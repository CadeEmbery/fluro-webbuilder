/////////////////////////////////////////////////////////////////////

console.log('Webbuilder V5 init');

/////////////////////////////////////////////////////////////////////


function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}

/////////////////////////////////////////////////////////////////////

var dependencies = [
    'ngResource',
    'ngTouch',
    'ngAnimate',
    'ui.tree',
    'ui.router',
    'ui.bootstrap',
    'fluro.config',
    'fluro.customer',
    'fluro.asset',
    'fluro.types',
    'fluro.socket',
    'fluro.util',
    'fluro.content',
    'fluro.access',
    'fluro.auth',
    'fluro.video',
    'fluro.interactions',
    'fluro.notifications',
    'ngStorage',
    'ui.ace',
    'ui.sortable',
    'fluro.lazyload',
    'ui.tree',
    'ui.bootstrap.fontawesome',
    'angular-loading-bar',
    'formly',
    'formlyBootstrap',
    '720kb.socialshare',
    'ng-currency',
    'angularFileUpload',
];

/////////////////////////////////////////////////////////////////////

var $parentScope;

/////////////////////////////////////////////////////////////////////

//Get all the dependencies
if (window.opener && window.opener.angular) {
    //Get the parent dependencies
    var element = window.opener.angular.element('#builder');

    if (element) {
        $parentScope = element.scope();
        if ($parentScope && $parentScope.item && $parentScope.item.dependencies && $parentScope.item.dependencies.length) {
            dependencies = dependencies.concat($parentScope.item.dependencies);
        }
    }
}

/////////////////////////////////////////////////////////////////////

console.log('Launch Dependencies')
var app = angular.module('fluro', dependencies);
var head = angular.element('head');

/////////////////////////////////////////////////////////////////////

//Bootstrap application
function initializeApplication() {

    //////////////////////////////////
    var initInjector = angular.injector(["ng"]);
    var $http = initInjector.get("$http");

    //////////////////////////////////
    //Get the location of our API
    var apiURL = getMetaKey('fluro_url');
    var token = getMetaKey('fluro_application_key');

    //Adjust to V2
    //apiURL = apiURL.replace('http://api', 'http://v2.api');

    //////////////////////////////////

    var config = {}
    if (token) {
        config.headers = {
            'Authorization': 'Bearer ' + token
        }
    } else {
        config.withCredentials = true;
    }

    /////////////////////////////////////////////////////////////////////

    //Now attach the dependency scripts
    if ($parentScope && $parentScope.item) {

        /////////////////////////////////////////////

        if ($parentScope.item.external && $parentScope.item.external.length) {
            _.each($parentScope.item.external, function(url) {
                    //Attach the Javascript dependencies
                    head.append('<script src="' + url + '" type="text/javascript"></script>');
                })
                //console.log('Attached external dependencies')
        }

        /////////////////////////////////////////////

        if ($parentScope.item.bower && $parentScope.item.bower.length) {
            //Vendor Urls
            var jsUrl = apiURL + '/get/site/vendor/' + $parentScope.item._id + '/js'; // + '?access_token=' + token;
            var cssUrl = apiURL + '/get/site/vendor/' + $parentScope.item._id + '/css'; // + '?access_token=' + token;

            if (token) {
                jsUrl += '?access_token=' + token;
                cssUrl += '?access_token=' + token;
            }

            /////////////////////////////////////////////////////////////////////

            //Attach the Javascript dependencies
            head.append('<script src="' + jsUrl + '" type="text/javascript"></script>');

            //Attach the CSS Straight away
            head.append('<link href="' + cssUrl + '" type="text/css" rel="stylesheet"/>');

            /////////////////////////////////////////////////////////////////////

            //Load the JS and wait til its finished
            var req = $http.get(jsUrl, config);

            req.success(function(res) {
                //console.log('Loaded Script Dependencies');
                startApplication();
            });

            req.error(function(res) {
                console.log('Bower Dependencies Error', res);
            });
        } else {
            startApplication();
        }
    } else {
        //Just Start the app
        startApplication();
    }

    /////////////////////////////////////////////////////////////////////

    function startApplication() {

        //Start the application with initial dependencies
        // console.log('Start application')

        /////////////////////////////////////////////////////////////////////

        //Request
        var req = $http.get(apiURL + "/session", config);

        req.success(function(res) {
            if (res) {
                app.constant("$initUser", res);


                //Bootstrap the application
                angular.element(document).ready(function() {
                    angular.bootstrap(document, ["fluro"]);
                });

            } else {
                console.log('Bootstrap error', res)
            }
        });

        req.error(function(res) {
            console.log('Bootstrap error', res)
        });
    }
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

//Start the application
initializeApplication();

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function(
    $controllerProvider,
    $compileProvider,
    $filterProvider,
    $provide,
    $stateProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider) {

    ///////////////////////////////////////////


    app.components = {}
    app.components.controller = $controllerProvider.register;
    app.components.directive = $compileProvider.directive;
    app.components.filter = $filterProvider.register;
    app.components.factory = $provide.factory;
    app.components.service = $provide.service;

    //Get the access token and the API URL
    var accessToken = getMetaKey('fluro_application_key');
    var apiURL = getMetaKey('fluro_url');


    //Create the initial config setup
    var initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: true, //Change this if you want to use local storage  or session storage
        backupToken: accessToken,
    }

    ///////////////////////////////////////////

    //Check if we are developing an app locally
    var appDevelopmentURL = getMetaKey('app_dev_url');

    //If an app development url is set then we know where to login etc
    if (appDevelopmentURL && appDevelopmentURL.length) {
        initialConfig.appDevelopmentURL = appDevelopmentURL;
    } else {
        //Set HTML5 mode to true when we deploy
        //otherwise live reloading will break in local dev environment
        $locationProvider.html5Mode(true);
    }

    ///////////////////////////////////////////

    //Set the initial config
    FluroProvider.set(initialConfig);

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!accessToken) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");



    //Define our pages

    $stateProvider.state('home', {
        url: '/',
        views: {
            'builder': {
                templateUrl: 'routes/home/home.html',
                controller: 'HomeController',
                resolve: {
                    items: function(FluroContent) {

                        console.log('Retrieve sites');
                        return FluroContent.resource('site').query().$promise;
                    }
                }
            }
        }
    })


    //Define our pages

    $stateProvider.state('versions', {
        url: '/versions/:id',
        views: {
            'builder': {
                templateUrl: 'routes/versions/versions.html',
                controller: 'VersionListController',
                resolve: {
                    item: function($stateParams, FluroContent) {
                        return FluroContent.endpoint('log').query({
                            item: $stateParams.id,
                            limit: 500,
                        }).$promise;
                    }
                }
            }
        }
    })

    ///////////////////////////////////////////


    //Define our pages

    $stateProvider.state('templates', {
        url: '/templates',
        views: {
            'builder': {
                templateUrl: 'routes/templates/templates.html',
                controller: 'TemplatesController',
                resolve: {
                    items: function(FluroContent) {
                        return FluroContent.endpoint('templates/site').query().$promise;
                    }
                },
            }
        }
    });

    ///////////////////////////////////////////

    $stateProvider.state('build', {
        url: '/build/:id',
        abstract: true,
        views: {
            'builder': {
                templateUrl: 'routes/build/build.html',
                controller: 'BuildController',
                resolve: {
                    item: function(FluroContent, $stateParams) {
                        if ($stateParams.id != 'new') {
                            return FluroContent.resource('site').get({
                                id: $stateParams.id
                            }).$promise;
                        } else {
                            return {}
                        }
                    },
                    snippets: function(FluroContent, $stateParams) {
                        return FluroContent.endpoint('my/snippets').query().$promise;
                    }
                },
            }
        }
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.default', {
        url: '',
        templateUrl: 'routes/build/pages/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.info', {
        url: '/info',
        templateUrl: 'routes/build/info/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.styles', {
        url: '/styles',
        templateUrl: 'routes/build/styles/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.dependencies', {
        url: '/dependencies',
        templateUrl: 'routes/build/dependencies/manager.html',
    })


    ///////////////////////////////////////////

    $stateProvider.state('build.components', {
        url: '/components',
        templateUrl: 'routes/build/components/manager.html',
        controller: 'ComponentManagerController',
    })



    ///////////////////////////////////////////

    $stateProvider.state('preview', {
        url: '/preview',
        views: {
            'builder': {
                templateUrl: 'routes/preview/preview.html',
                controller: 'PreviewController',
            }
        }
    })



    /*
   

    $stateProvider.state('build', {
        url: '/build/:id',
        templateUrl: 'routes/build.html',
        controller: 'BuildController',
        resolve: {
            item: function(FluroContent, $stateParams, Site) {
                if ($stateParams.id != 'new') {
                    return FluroContent.resource('site').get({
                        id: $stateParams.id
                    }).$promise;
                } else {
                    return Site.create()
                }
            },
            styles: function(FluroContent) {

                return FluroContent.resource('get/scripts/css').query().$promise;

            }
        }
    })




    $stateProvider.state('preview', {
        url: '/preview',
        templateUrl: 'views/preview.html',
        controller: 'PreviewController',
    })

    $stateProvider.state('outer', {
        url: '/outer/:id',
        templateUrl: 'views/outer.html',
        controller: 'OuterController',
        resolve: {
            item: function(FluroContent, $stateParams, Site) {
                if ($stateParams.id != 'new') {
                    return FluroContent.resource('site').get({
                        id: $stateParams.id
                    }).$promise;
                } else {
                    return Site.create()
                }
            },
        }
    })
    */




});

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.run(function($state, VideoTools, Fluro, FluroTokenService, FluroWindowService, $sessionStorage, $localStorage, $stateParams, $rootScope, FluroAccess, FluroMenuService, Notifications, FluroScrollService, DateTools, Asset, $initUser) {









    $rootScope.getTitle = function() {

        var title = 'WebBuilder';

        if ($rootScope.previewMode) {
            title = 'Preview';
        }

        if ($rootScope.site && $rootScope.site.title) {
            title = title + ' - ' + $rootScope.site.title;
        }

        return title;

    }

    //////////////////////////////////////////////

    var aceEditorOnload = function(editor) {
        //disables the annoying warning message
        editor.$blockScrolling = Infinity;
        // editor.getSession().setUseWorker(false);


        // Editor part
        var _session = editor.getSession();
        //var _renderer = _editor.renderer;

        //////////////////////////////////////////////////
        //////////////////////////////////////////////////

        /*
        editor.commands.addCommand({
            name: 'format',
            bindKey: {
                win: "Ctrl-Shift-F",
                mac: 'Command-Shift-F'
            },
            exec: function(editor) {

                var mode = editor.session.$modeId;

                mode = mode.substr(mode.lastIndexOf('/') + 1);
                var worker = new Worker('worker-jsbeautify.js');
                var options = {
                    mode: mode
                };

                worker.postMessage([editor.getValue(), options]);
                worker.addEventListener('message', function(e) {
                    console.log(e);
                    editor.setValue(e.data);
                }, false);
            }
        })
        /**/


        //////////////////////////////////////////////////

        //Set worker path
        // ace.config.set("workerPath", "/js/worker");

        ace.config.loadModule('ace/snippets/snippets', function() {

            //Snippet manager
            var snippetManager = ace.require('ace/snippets').snippetManager;


            switch (_session.$modeId) {
                case 'ace/mode/html':
                    ace.config.loadModule('ace/snippets/html', function(m) {
                        if (m && $rootScope.snippets.length) {

                            _.chain($rootScope.snippets)
                                .filter({
                                    syntax: 'html'
                                })
                                .each(function(snippet) {
                                    m.snippets.push({
                                        content: snippet.body,
                                        tabTrigger: String(snippet.account.title + ':' + snippet.title).toLowerCase(),
                                    });
                                })
                                .value();

                            snippetManager.register(m.snippets, m.scope);
                        }
                    });
                    break;
                case 'ace/mode/javascript':
                    ace.config.loadModule('ace/snippets/javascript', function(m) {
                        if (m && $rootScope.snippets.length) {

                            _.chain($rootScope.snippets)
                                .filter({
                                    syntax: 'js'
                                })
                                .each(function(snippet) {
                                    m.snippets.push({
                                        content: snippet.body,
                                        tabTrigger: String(snippet.account.title + ':' + snippet.title).toLowerCase(),
                                    });
                                })
                                .value();

                            snippetManager.register(m.snippets, m.scope);
                        }
                    });
                    break;
            }
        })



        /**
        //Load the snippets module
        ace.config.loadModule('ace/snippets/snippets', function() {

            //Snippet manager
            var snippetManager = ace.require('ace/snippets').snippetManager;



            ace.config.loadModule('ace/snippets/html', function(m) {
                if (m) {
                    m.snippets.push({
                        content: '${1:test}.this is custom snippet text!',
                        tabTrigger: 'CustomSnippet'
                    });
                    snippetManager.register(m.snippets, m.scope);
                }
            });

            ace.config.loadModule('ace/snippets/javacript', function(m) {
                if (m) {
                    m.snippets.push({
                        content: '${1:test}.this is custom snippet text!',
                        tabTrigger: 'CustomSnippet'
                    });
                    snippetManager.register(m.snippets, m.scope);
                }
            });
        });
        /**/



        /// var snippetManager = ace.require("ace/snippets").snippetManager;

        /**
        editor.commands.addCommand({
            name: 'format',
            bindKey: {win: "Ctrl-Shift-L", mac: 'Command-Shift-L'},
            exec: function (editor) {
                var mode = editor.session.$modeId;
                mode = mode.substr(mode.lastIndexOf('/') + 1);
                var worker = new Worker('worker-jsbeautify.js');
                var options = {
                    mode: mode
                };
                worker.postMessage([editor.getValue(), options]);
                worker.addEventListener('message', function (e) {
                    console.log(e);
                    editor.setValue(e.data);
                }, false);
            }
        })
        /**/
    };

    $rootScope.aceEditorOptions = {
        html: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'html',
            onLoad: aceEditorOnload

        },
        scss: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'scss',
            onLoad: aceEditorOnload
        },
        js: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'javascript',
            onLoad: aceEditorOnload
        }
    };


    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////



    /**
    function success(res) {

        console.log('SUCCESSFULLY AUTHENTICATED AS', res, Fluro);
        // $sessionStorage.session = res.data;
        // // Fluro.token = res.data.token;
        // // console.log('SIGN IN COMPLETE', res, Fluro, $sessionStorage);
        $rootScope.user = res.data;
        $rootScope.init = true;
    }

    function fail(res) {
        Notifications.error('Error creating token session');
    }

    //Store the session storage token
    Fluro.sessionStorage = true;

     console.log('CREATE STORAGE TOKEN', $initUser.account._id);

    FluroTokenService.getTokenForAccount($initUser.account._id).then(success, fail);

    /**/

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    //Add the user to the rootscope
    $rootScope.user = $initUser;
    $rootScope.init = true;

    //////////////////////////////////////////////////////////////////

    //Add Storage Options
    $rootScope.localStorage = $localStorage;
    $rootScope.sessionStorage = $sessionStorage;

    //////////////////////////////////////////////////////////////////

    //Add the window to the rootscope
    $rootScope.window = FluroWindowService;

    //////////////////////////////////////////////////////////////////

    //Enable Fluro Access on the rootscope
    $rootScope.access = FluroAccess;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.asset = Asset;

    //////////////////////////////////////////////////////

    $rootScope.getVideoImageUrl = VideoTools.getVideoThumbnail;
    $rootScope.getImageUrl = Asset.imageUrl;
    $rootScope.getThumbnailUrl = Asset.thumbnailUrl;
    $rootScope.getDownloadUrl = Asset.downloadUrl;
    $rootScope.getUrl = Asset.getUrl;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.menu = FluroMenuService;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.date = DateTools;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.notifications = Notifications;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.scroll = FluroScrollService;

    //////////////////////////////////////////////////////////////////

    //Add state params
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    //////////////////////////////////////////////////////////////////
    /*
    $rootScope.getImageUrl = Asset.imageUrl;
    $rootScope.getThumbnailUrl = Asset.thumbnailUrl;
    $rootScope.getDownloadUrl = Asset.downloadUrl;
    $rootScope.getUrl = Asset.getUrl;
    */

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
        if (to.redirectTo) {
            evt.preventDefault();
            $state.go(to.redirectTo, params)
        }
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        console.log('State change error')
        throw error;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.firstLoad = true;
    });

    //////////////////////////////////////////////////////////////////

    //Make touch devices more responsive
    FastClick.attach(document.body);

});
app.directive('contentBrowser', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            typeName: '=ngType',
            params: '=?params',
            done: '=ngDone',
        },
        // Replace the div with our template
        templateUrl: 'admin-content-browser/admin-content-browser.html',
        controller: 'ContentBrowserController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('ContentBrowserController', function($scope, TypeService, $rootScope, FluroContent, $http, $filter, Fluro, FluroStorage) {


    if ($scope.typeName) {
        $scope.type = TypeService.getTypeFromPath($scope.typeName);
        $scope.title = 'Select ' + $scope.type.plural;
    } else {
        $scope.title = 'Select content';
    }

    //////////////////////////////

    $scope.close = function() {
        if ($scope.done) {
            $scope.done();
        }
    }

    //////////////////////////////

    $scope.toggle = function(item) {
        if (_.some($scope.model, {
            _id: item._id
        })) {
            $scope.deselect(item)
        } else {
            $scope.select(item)
        }
    }

    //////////////////////////////

    $scope.select = function(item) {
        if (!_.some($scope.model, {
            _id: item._id
        })) {
            $scope.model.push(item)
        }


        // console.log('SELECT IS', $scope, $scope.model, item);

    }

    //////////////////////////////

    $scope.deselect = function(item) {
        var found = _.find($scope.model, {
            _id: item._id
        })

        if (found) {
            _.pull($scope.model, found);
        }
    }

    //////////////////////////////

    $scope.isSelected = function(item) {
        return _.some($scope.model, {
            _id: item._id
        })
    }


    //////////////////////////////

    if(!$scope.params) {
        $scope.params = {};
    }

    //////////////////////////////

    var local = FluroStorage.localStorage('content-browser.' + $scope.typeName)
    var session = FluroStorage.sessionStorage('content-browser.' + $scope.typeName)

    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {
                    status: ['active'],
                }
        }
    } else {
        $scope.search = session.search;
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize: 8,
        }
    } else {
        $scope.pager = session.pager;
    }




    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
    }

    //////////////////////////////

    $scope.$watch('search', updateFilters, true);

    //////////////////////////////

    $scope.refresh = function() {


        //If browsing for an account
        if($scope.type && $scope.type.path == 'account') {
            
            //Get all the options
            return FluroContent.resource('account', false, true).query().$promise.then(function(results) {
                //console.log('Got content results', results)
                $scope.items = results;
                updateFilters();
            });
        }

        ///////////////////////////////////////////


        var queryDetails = {}

        
        if ($scope.type) {
            queryDetails.type = $scope.type.path
        }

        if ($scope.params.searchInheritable) {
            queryDetails.searchInheritable = true;
        }


        if ($scope.params.syntax) {
            queryDetails.syntax = $scope.params.syntax;
        }




        //Get all the options
        FluroContent.endpoint('content', false, true).query(queryDetails).$promise.then(function(results) {
            //console.log('Got content results', results)
            $scope.items = results;
            updateFilters();
        });
    }

    //Load the library
    $scope.refresh();

    /*
    $http.get(Fluro.apiURL + '/node/browse', {
        ignoreLoadingBar: true,
    }).then(function(response) {
    	$scope.items = response.data;
    	updateFilters();
    })
*/


    $scope.removeFilter = function(key) {
        if ($scope.search.filters) {
            delete $scope.search.filters[key];
        }
    }

    $scope.$watch('search', updateFilters, true);


    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    function updateFilters() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);


            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;


            //Update the current page
            $scope.updatePage();

        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {


        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        var filteredItems = $scope.filteredItems;

        //Order by title
        //filteredItems = $filter('orderBy')(filteredItems, 'title');
        //filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);



        //Now break it up into pages
        var pageItems = $filter('startFrom')(filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }



    ///////////////////////////////////////////////

    /*
	///////////////////////////////////////

	$scope.$watch('search.terms', function(keywords) {
		return $http.get(Fluro.apiURL + '/search', {
            ignoreLoadingBar: true,
            params: {
                keys: keywords,
                //type: $scope.restrictType,
                limit: 100,
            }
        }).then(function(response) {
           $scope.items = response.data;
        });
	})
*/

    ///////////////////////////////////////


});
app.directive('filterBlock', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            model: '=ngModel',
            source: '=ngSource',
            getFilterKey: '&ngFilterKey',
            getFilterTitle: '&ngFilterTitle',
            persist: '@',
        },
        templateUrl: 'admin-content-filter-block/admin-content-filter-block.html',
        controller: 'ContentFilterBlock',
    };
});


app.controller('ContentFilterBlock', function($scope, $timeout, $filter, Tools) {

    var filterKey = $scope.getFilterKey();
    var filterTitle = $scope.getFilterTitle();

    $scope.filterKey = filterKey;
    $scope.filterTitle = filterTitle;

    if (!$scope.model) {
        $scope.model = {};
    }


    // console.log(filterKey, $scope.model, $scope.source)

    ///////////////////////////////////////////////

    $scope.filterIsActive = function(value) {
        return _.contains($scope.model[filterKey], value);
    }

    ///////////////////////////////////////////////

    $scope.filtersUsed = function() {
        return ($scope.model[filterKey] && $scope.model[filterKey].length)
    }

    ///////////////////////////////////////////////

    $scope.toggleFilter = function(value) {

        var active = _.contains($scope.model[filterKey], value);

        if ($scope.persist) {

            if (active) {
                //Always select
                $scope.model[filterKey] = [];
            } else {
                //Always select
                $scope.model[filterKey] = [value];
            }

        } else {

            if (active) {
                $scope.removeFilter(value);
            } else {
                $scope.addFilter(value);
            }
        }
    }

    ///////////////////////////////////////////////

    $scope.removeFilter = function(value) {
        //console.log('remove filter', key)
        if (!value) {
            delete $scope.model[filterKey];
        } else {
            _.pull($scope.model[filterKey], value);
        }
    }

    ///////////////////////////////////////////////

    $scope.addFilter = function(value) {

        if (!$scope.model[filterKey]) {
            $scope.model[filterKey] = [];
        }

        if (!_.contains($scope.model[filterKey], value)) {
            $scope.model[filterKey].push(value);
        }
    }

    ///////////////////////////////////////////////

    function addFilterOption(result, item, field_key, source) {

        if (!source) {
            return;
        }
        var key = source[field_key];
        var title;

        if (_.isString(source)) {
            title = key = source;
        } else {
            if (source.title) {
                title = source.title;
            } else {
                title = source.name;
            }
        }

        if (!result[key]) {
            result[key] = {
                title: title,
                key: key,
                //items: [item],
                count: 1,
            }
        } else {
            //result[key].items.push(item);
            result[key].count++;
        }
    }



    ///////////////////////////////////////////////

    $scope.$watch('source', function(items) {


        var filterOptions = _.chain(items)
            .filter(function(item) {
                var obj = Tools.getDeepProperty(item, filterKey);
                if (obj) {
                    return true;
                }
            })
            .reduce(function(result, item, key) {
                var obj = Tools.getDeepProperty(item, filterKey);

                if (_.isArray(obj)) {
                    _.each(obj, function(sub) {
                        addFilterOption(result, item, '_id', sub)
                    })
                } else {
                    if (_.isString(obj)) {
                        addFilterOption(result, item, filterKey, obj);
                    } else {
                        addFilterOption(result, item, '_id', obj);
                    }
                }
                return result;
            }, {})
            .value();

        $scope.filterOptions = filterOptions;
        //$scope.filterOptions = $filter('orderBy')(filterOptions, 'title');


        $scope.count = _.keys($scope.filterOptions).length;

    });

    ///////////////////////////////////////////////


});
app.directive('contentSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            ngParams: '&',
        },
        templateUrl: 'admin-content-select/admin-content-select.html',
        controller: 'ContentSelectController',
    };
});


//////////////////////////////////////////////////////////////////////////

app.controller('ContentSelectController', function($scope, $location, $window, Asset, $rootScope, FluroContent, Fluro, ModalService, TypeService, FluroAccess, $http) {

    /////////////////////////////////////

    if (!$scope.selectedItems) {
        $scope.selectedItems = {
            items:[]
        }
    }

    /////////////////////////////////////

    if (!$scope.params) {
        $scope.params = {};
    }

    /////////////////////////////////////

    $scope.asset = Asset;

    /////////////////////////////////////

    $scope.$watch(function() {
        return $scope.ngParams();
    }, function(p) {

        if (!p) {
            p = {}
        }
        $scope.params = p;


        if ($scope.params.canCreate) {
            $scope.canCreate = $scope.params.canCreate;
        }

        

        if ($scope.params.type) {


            $scope.type = TypeService.getTypeFromPath($scope.params.type)

            if(!$scope.type) {
                $scope.browseAccessDisabled = true;
            }

           // console.log('POARAMS TYPE', TypeService.definedTypes, $scope.params.type, $scope.type);
            //$scope.type = $scope.params.type;
        }

        if ($scope.params.hideBrowse) {
            $scope.hideBrowse = true;
        }

        //console.log('TESTING', $scope.params.disableBrowse);

        if ($scope.params.minimum) {
            $scope.minimum = $scope.params.minimum;
        }
        if ($scope.params.maximum) {
            $scope.maximum = $scope.params.maximum;
        }

        if ($scope.params.allowedValues) {
            $scope.allowedValues = $scope.params.allowedValues;
        }

        if ($scope.params.defaultValues) {
            $scope.defaultValues = $scope.params.defaultValues;

            //If theres nothing in the list yet
            if (!$scope.selectedItems.items.length) {
                //Loop through each default value and include it
                _.each($scope.defaultValues, function(item) {
                    $scope.selectedItems.items.push(item);
                })
            }
        }

    }, true)

    ///////////////////////////////////////////////

    $scope.$watch('model', function() {
        if ($scope.model) {
            if (_.isArray($scope.model)) {
                if ($scope.model.length) {
                    if ($scope.maximum) {
                        //take the maximum first items from the existing model
                        $scope.selectedItems.items = _.take($scope.model, $scope.maximum);
                    } else {
                        $scope.selectedItems.items = angular.copy($scope.model);
                    }
                } else {
                    $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues);
                }
            } else {
                $scope.selectedItems.items = [$scope.model];
            }
        } else {
            $scope.selectedItems.items = [];
        }
    }, true)

    ////////////////////////////////////////////////////

    $scope.$watch('selectedItems.items', function(items) {

       // console.log('Selected Items changed', items)
        if ($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {

            $scope.model = items;
        }

        //We need to nullify it otherwise the message doesnt get to the server
        if(!$scope.model) {
            $scope.model = null;
        }

    }, true)


    /////////////////////////////////////

    $scope.proposed = {}

    /////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/create-popover.html',
    };

    $scope.searchPlaceholder = 'Search for existing content';

    if ($scope.type) {
        $scope.searchPlaceholder = 'Search for ' + $scope.type.singular + ' items';
    }

    /////////////////////////////////////

    $scope.createEnabled = function() {
        if ($scope.type) {
            return FluroAccess.can('create', $scope.type.path);
        } else {
            return false;
        }
    }

    /////////////////////////////////////

    $scope.isSortable = function() {

        if ($scope.maximum) {
            return $scope.maximum > 1;
        }
        return true;
    }

    /////////////////////////////////////

    $scope.addEnabled = function() {
        if(!$scope.browseAccessDisabled) {
            if ($scope.maximum) {
                return ($scope.selectedItems.items.length < $scope.maximum)
            } else {
                return true;
            }
        }
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedItems.items.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.remove = function(item) {
        if (_.contains($scope.selectedItems.items, item)) {
            _.pull($scope.selectedItems.items, item);
        }
    }

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.view = function(item) {
        ModalService.view(item);
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////


    function getBaseDomain(string) {

        var split = string.split('.');
        return split.slice(Math.max(split.length - 2, 1)).join('.');

        // var length = split.length;
        // if (length > 2) {
        //     return string.substr(string.indexOf(".") + 1);
        // } else {
        //     return string;
        // }
    }


    $scope.editInAdmin = function(item) {

        var def = item._type;
        if(item.definition && item.definition.length) {
            def = item.definition;
        }

        var baseDomain = getBaseDomain(Fluro.apiURL);
        // console.log('BASE DOMAIN', baseDomain);
        var domain = '';

        switch(baseDomain)  {
            case 'fluro.dev:3000':
                domain = 'http://admin.fluro.dev:3000';
            break;
            case 'inyerpocket.com':
                domain = 'http://admin.inyerpocket.com';
            break;
            default:
                domain = 'http://admin.fluro.io';
            break;
        }

        var url =  domain + '/' + item._type + '/' + def + '/' + item._id + '/edit';

        $window.open(url, '_blank');
    }

    $scope.canEditAdmin = function(item) {
        var canEdit = $scope.canEdit(item);
        var currentDomain = $location.host();

        var notAdmin = !_.startsWith(currentDomain, 'admin');
        return canEdit && notAdmin;
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////

    $scope.canView = function(item) {
        return FluroAccess.canViewItem(item);
    }

    /////////////////////////////////////

    $scope.browse = function() {   
        if (!$scope.browseAccessDisabled) {

            var params = {};

            if ($scope.params.searchInheritable) {
                params.searchInheritable = $scope.params.searchInheritable = true;
            }

            if ($scope.params.syntax) {
                params.syntax = $scope.params.syntax;
            }
            
            if ($scope.type) {
                ModalService.browse($scope.type.path, $scope.selectedItems, params);
            } else {
                ModalService.browse(null, $scope.selectedItems, params);
            }
        }
    }

    /////////////////////////////////////

    $scope.createableTypes = TypeService.getAllCreateableTypes;

    /////////////////////////////////////

    $scope.create = function(typeToCreate) {

        var options = {};

        if($scope.params.template) {
            options.template = $scope.params.template;
        }

        if (!typeToCreate) {
            ModalService.create($scope.type.path, options, $scope.add)
        } else {
            ModalService.create(typeToCreate, options, $scope.add)
        }
    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {

        if ($scope.allowedValues && $scope.allowedValues.length) {
            return _.reduce($scope.allowedValues, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return $filter('filter')(filtered, val);
            }, [])
        } else {

            ////////////////////////

            //Create Search Url
            // var searchUrl = Fluro.apiURL + '/content';
            var searchUrl = 'content';
            if ($scope.type) {
                searchUrl += '/' + $scope.type.path;
            }
            searchUrl += '/search';

            ////////////////////////

            // return $http.get(searchUrl +'/'+ val, {
            //     ignoreLoadingBar: true,
            //     params: {
            //         limit: 10,
            //     }
            // })
            return FluroContent.endpoint(searchUrl +'/'+ val, true, true).query({
                limit: 15,
            })
            .$promise
            .then(function(results) {

                console.log('SEARCH', results, val)

                // var results = response.data;

                return _.reduce(results, function(filtered, item) {
                    var exists = _.some($scope.selectedItems.items, {
                        '_id': item._id
                    });
                    if (!exists) {
                        filtered.push(item);
                    }
                    return filtered;
                }, []);
                
            });
        }
    };

});

app.directive('newContentSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            config: '=',
        },
        templateUrl: 'admin-content-select/new-content-select.html',
        controller: 'NewContentSelectController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('NewContentSelectController', function($scope, Asset, $rootScope, Fluro, ModalService, TypeService, FluroAccess, $http) {


    /////////////////////////////////////

    if (!$scope.selectedItems) {
        $scope.selectedItems = {
            items: []
        }
    }

    /////////////////////////////////////

    if (!$scope.config) {
        $scope.config = {};
    }

    /////////////////////////////////////

    $scope.asset = Asset;

    /////////////////////////////////////

    /**/
    $scope.$watch('config', function(config) {

        //console.log('Config', config)
        /////////////////////////////////////////

        //Basics
        $scope.canCreate = config.canCreate;
        $scope.hideBrowse = config.hideBrowse;
        $scope.minimum = parseInt(config.minimum);
        $scope.maximum = parseInt(config.maximum);

        /////////////////////////////////////////


        if (config.type) {
            $scope.type = TypeService.getTypeFromPath(config.type)
            if (!$scope.type) {
                $scope.browseAccessDisabled = true;
            }
        }

        /////////////////////////////////////////

        if (config.allowedValues) {
            $scope.allowedValues = config.allowedValues;
        }

        /////////////////////////////////////////

        if (config.defaultValues) {
            $scope.defaultValues = config.defaultValues;

            //If theres nothing in the list yet
            if (!$scope.selectedItems.items.length) {
                //Loop through each default value and include it
                _.each($scope.defaultValues, function(item) {
                    $scope.selectedItems.items.push(item);
                });
            }
        }

        /////////////////////////////////////////

        if (config.type) {
            $scope.type = TypeService.getTypeFromPath(config.type)
            if (!$scope.type) {
                $scope.browseAccessDisabled = true;
            }
        }
    }, true);
    /**/


    ///////////////////////////////////////////////

    $scope.$watch('model', function() {


        //console.log('TEST MODEL', $scope.model);
        if ($scope.model) {
            if (_.isArray($scope.model)) {
                if ($scope.model.length) {
                    if ($scope.maximum) {
                        //take the maximum first items from the existing model
                        $scope.selectedItems.items = _.take($scope.model, $scope.maximum);
                    } else {
                        $scope.selectedItems.items = angular.copy($scope.model);
                    }
                } else {
                    $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues);
                }
            } else {
                $scope.selectedItems.items = [$scope.model];
            }
        } else {
            $scope.model = [];
        }
    }, true)

    ////////////////////////////////////////////////////

    $scope.$watch('selectedItems.items', function(items) {
        // console.log('Selected Items changed', items)
        if ($scope.maximum == 1) {
            $scope.model = _.first(items);
        } else {
            $scope.model = items;
        }
    }, true)


    /////////////////////////////////////

    $scope.proposed = {}

    /////////////////////////////////////

    $scope.dynamicPopover = {
        templateUrl: 'views/ui/create-popover.html',
    };

    $scope.searchPlaceholder = 'Search for existing content';

    if ($scope.type) {
        $scope.searchPlaceholder = 'Search for ' + $scope.type.singular + ' items';
    }

    /////////////////////////////////////

    $scope.createEnabled = function() {
        if ($scope.type) {
            return FluroAccess.can('create', $scope.type.path);
        } else {
            return false;
        }
    }

    /////////////////////////////////////

    $scope.isSortable = function() {
        if ($scope.maximum) {
            return $scope.maximum > 1;
        }
        return true;
    }

    /////////////////////////////////////

    $scope.addEnabled = function() {

        //console.log('Testing', $scope.maximum, $scope.selectedItems);
        if ($scope.browseAccessDisabled) {
            return false;
        }

        if (!$scope.maximum) {
            return true;
        }
        
        var selectedItems = $scope.selectedItems.items;

        return (selectedItems.length < $scope.maximum); 
    }

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.selectedItems.items.push(item);
        $scope.proposed = {};
    }

    /////////////////////////////////////

    $scope.remove = function(item) {
        if (_.contains($scope.selectedItems.items, item)) {
            _.pull($scope.selectedItems.items, item);
        }
    }

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////

    $scope.browse = function() {

        if (!$scope.browseAccessDisabled) {

            var params = {};

            if ($scope.config.searchInheritable) {
                params.searchInheritable = $scope.config.searchInheritable = true;
            }
            
            if ($scope.type) {
                ModalService.browse($scope.type.path, $scope.selectedItems, params);
            } else {
                ModalService.browse(null, $scope.selectedItems, params);
            }
        }
    }

    /////////////////////////////////////

    $scope.createableTypes = TypeService.getAllCreateableTypes;

    /////////////////////////////////////

    $scope.create = function(typeToCreate) {

        var options = {};

        if ($scope.config.template) {
            options.template = $scope.config.template;
        }

        if (!typeToCreate) {
            ModalService.create($scope.type.path, options, $scope.add)
        } else {
            ModalService.create(typeToCreate, options, $scope.add)
        }
    }

    /////////////////////////////////////

    $scope.getOptions = function(val) {

        if ($scope.allowedValues && $scope.allowedValues.length) {
            return _.reduce($scope.allowedValues, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    '_id': item._id
                });

                if (!exists) {
                    filtered.push(item);
                }
                return $filter('filter')(filtered, val);
            }, [])
        } else {

            ////////////////////////

            //Create Search Url
            var searchUrl = Fluro.apiURL + '/content';
            if ($scope.type) {
                searchUrl += '/' + $scope.type.path;
            }
            searchUrl += '/search';


            var searchParams = {
                limit: 10
            };


            if ($scope.config.searchInheritable) {
                searchParams.searchInheritable = true;
            }

            ////////////////////////

            return $http.get(searchUrl + '/' + val, {
                ignoreLoadingBar: true,
                params: searchParams,
            }).then(function(response) {

                var results = response.data;

                return _.reduce(results, function(filtered, item) {
                    var exists = _.some($scope.selectedItems.items, {
                        '_id': item._id
                    });
                    if (!exists) {
                        filtered.push(item);
                    }
                    return filtered;
                }, []);

            });
        }
    };

});
app.directive('fieldEdit', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-edit.html',
        controller: 'FieldEditController',
    };
});

app.controller('FieldEditController', function($scope, TypeService) {


    //console.log('Field Edit Controller')
    //$scope.$watch('model', function(model) {

    if (!$scope.model) {
        $scope.model = {};
    }

    $scope.referenceParams =
        $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;



    $scope.showOptions = function() {

        switch ($scope.model.directive) {
            case 'select':
            case 'button-select':
            case 'search-select':
            case 'order-select':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.isNumberType = function(type) {
        switch (type) {
            case 'number':
            case 'integer':
            case 'float':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.$watch('model', function(model) {
        if (!model.key || !model.key.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        });

    }

    /////////////////////////////////////////////

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })

    /////////////////////////////////////////////

    // $scope.$watch('model.type + model.directive + model.params.restrictType', function() {
    //     if($scope.model.type == 'reference' && $scope.model.directive == 'reference-create') {

    //         var definition = TypeService.getTypeFromPath($scope.model.params.restrictType);

    //         if(definition) {
    //             $scope.model.embeddedDefinition = definition;
    //         }
    //     } else {
    //         delete $scope.model.embeddedDefinition;
    //     }
    // })

    //});


});
app.directive('fieldEditor', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-editor.html',
        //templateUrl: 'views/ui/field-editor.html',
        controller: 'FieldEditorController',
    };
});




app.controller('FieldEditorController', function($scope) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.selected = {};

    $scope.selectField = function(node) {
        $scope.selected.field = node;
    }

    ///////////////////////////////////////////////

    $scope.hasSubFields = function(node) {
        return (node.type == 'group' || node.directive == 'embedded');
    }


    var newObject = {
        type: 'string',
        directive: 'input',
        minimum: 0,
        maximum: 1,
        fields: [],
    }

    var newGroup = {
        type: 'group',
        fields: [],
        collapsed: true,
    }

    //Create a new object
    $scope._new = angular.copy(newObject);
    $scope._newGroup = angular.copy(newGroup);

    $scope.sortableOptions = {
        handle: ' .handle'
        // items: ' .panel:not(.panel-heading)'
        // axis: 'y'
    }


    ////////////////////////////////////////////////////

    $scope.add = function(object) {
        if ($scope.contains(object)) {
            return;
        }

        //Add it in to the right place

        var parent = $scope.model;
        var selectedField = $scope.selected.field;

        if (selectedField) {
            parent = getParentArray(selectedField);

            var index = parent.indexOf(selectedField);
            if (index != -1) {

                parent.splice(index + 1, 0, object);
                return true;
            }


        }



        parent.push(object);
        return true;
    }



    //////////////////////////////////////////

    $scope.getFieldPath = function(target) {

        var trail = [];
        var result = {};

        // function callback(actualTrail) {
        //     return actualTrail;
        // }

        getFieldPathTrail($scope.model, target, trail, result);

        return 'interaction.' + result.trail.join('.');
    }


    function getFieldPathTrail(array, target, trail, result) {
        //return $scope.getTrail($scope.model, field, []);

        for (var key in array) {
            var field = array[key];

            if (field == target) {

                if(!field.asObject && field.directive != 'embedded') {
                    trail.push(field.key);
                } else {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }


                //trail.push(field.key);
                result.trail = trail.slice();
                return;

                //return callback(trail.slice());
            }

            if (field.fields && field.fields.length) {

                if (field.asObject || field.directive == 'embedded') {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }

                getFieldPathTrail(field.fields, target, trail, result);

                if (field.asObject || field.directive == 'embedded') {
                    trail.pop();
                }

            }
        }
    }

    /*
    $scope.getTrail = function(parentArray, child, trail) {

        //////////////////////////////////////////

        return _.chain(parentArray)
            .reduce(function(trailResult, field, key) {

                if(field == child) {
                    return trailResult;
                }

                if (field.fields && field.fields.length) {
                    trailResult.push(field.key);

                    //Recursive search
                    var subTrail = $scope.getTrail(field.fields, child, trail);
                    trailResult.pop();
                }

                return trailResult;

            }, [])
            .flatten()
            .compact()
            .value();

    }
    */

    //////////////////////////////////////////

    //Now we have nested routes we need to flatten
    function getFlattenedFields(array) {
        return _.chain(array)
            .reduce(function(result, field, key) {

                if (field.type == 'group' || field.directive) {
                    result.push(field);
                }

                if (field.fields && field.fields.length) {
                    //Recursive search
                    var folders = getFlattenedFields(field.fields);

                    if (folders.length) {
                        result.push(folders)
                    }
                }

                return result;

            }, [])
            .flatten()
            .compact()
            .value();

    }


    //////////////////////////////////////////

    function getParentArray(item) {

        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            return _.includes(field.fields, item);
        })

        //Remove it
        if (parent) {
            return parent.fields;
        }

        return $scope.model;
    }

    //////////////////////////////////////////

    $scope.remove = function(item) {

        /*
        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            
            return _.includes(field.fields, item);
        })
        */

        var parentArray = getParentArray(item);

        //Remove it
        if (parentArray) {
            _.pull(parentArray, item);
        }

        //Deselect
        delete $scope.selected.field;

    }

    /**
    $scope.removeRoute = function() {

        //Get the item
        var item = $scope.selection.item;

        //Get the flattened routes
        var folders = getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {

            var found = _.includes(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }


        $scope.selection.deselect();
    }
    /**/

    ////////////////////////////////////////////////////

    /**
    $scope.remove = function(object) {

        console.log('Remove Object', object);
        if ($scope.contains(object)) {
            delete $scope.selected.field;
            _.pull($scope.model, object);
        }
    }
    /**/

    ////////////////////////////////////////////////////

    $scope.contains = function(object) {
        return _.includes($scope.model, object)
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._new = angular.copy(newObject);
        }
    }

    ////////////////////////////////////////////////////

    $scope.duplicate = function() {
        if (!$scope.selected.field) {
            return;
        }

        var insert = angular.copy($scope.selected.field);
        insert.title = insert.title + ' Copy';
        insert.key = '';
        delete insert._id;


        var inserted = $scope.add(insert);

        if (inserted) {
            $scope.selected.field = insert;
            $scope._new = angular.copy(newObject);
        }

    }






    ////////////////////////////////////////////////////

    $scope.createGroup = function() {
        var insert = angular.copy($scope._newGroup);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._newGroup = angular.copy(newGroup);
        }
    }

    ////////////////////////////////////////////////////
    /*
    $scope.createGroup = function() {
        var newGroup = {
            title:'New Group',
            type:'group',
            fields:[]
        }

        $scope.add(newGroup); 
    }
    */

})


/////////////////////////////////////////////////

/////////////////////////////////////////////////

app.directive('fieldGroupEdit', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-group-edit.html',

        //templateUrl: 'views/ui/field-group-edit.html',
        controller: 'FieldGroupEditController',
    };
});

app.controller('FieldGroupEditController', function($scope, TypeService) {

    if (!$scope.model) {
        $scope.model = {};
    }


    //  $scope.referenceParams =
    // $scope.definedTypes = TypeService.definedTypes;
    // $scope.types = TypeService.types;

    ///////////////////////////////////////////////

    $scope.$watch('model.asObjectType', function(objectType) {
        if(objectType == 'contactdetail') {
            $scope.definedTypes = _.filter(TypeService.definedTypes, {'parentType':'contactdetail'});
        } else {
            $scope.definedTypes = null;
        }
    })

    ///////////////////////////////////////////////


    
    if (!$scope.model.key) {
        $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })
});
app.directive('keyContentSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-key-content-select/admin-key-content-select.html',
        controller: 'KeyContentSelectController',
    };
});





app.controller('KeyContentSelectController', function($scope) {


    
    if (!$scope.model || !_.isArray($scope.model)) {
        $scope.model = [];
    }
    
    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = [];
        }

        if(insert.key) {
            var keyExists = _.find($scope.model, {key:insert.key});
            
            if(!keyExists) {
                $scope.model.push(insert);
                $scope._new = {}
            }
        }
    }

})

app.directive('keyValueSelect', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-key-value-select/admin-key-value-select.html',
        controller: 'KeyValueSelectController',
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});



app.controller('KeyValueSelectController', function($scope) {


    if (!$scope.model || (_.isObject($scope.model) && _.isArray($scope.model))) {
        $scope.model = {};
    }


    //Create a new object
    $scope._new = {}

    ////////////////////////////////////////////////////

    $scope.remove = function(key) {
        delete $scope.model[key];
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        if(!$scope.model) {
            $scope.model = {}
        }
        if(insert.key && !$scope.model[insert.key]) {
            $scope.model[insert.key] = insert.value;
            $scope._new = {}
        }
    }

})



app.directive('multiValueField', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
            placeholder:'@',
            //type: '=ngType',
            //title: '=ngTitle',
            //minimum: '=ngMinimum',
            //maximum: '=ngMaximum',
        },
        templateUrl: 'admin-multi-value-field/multi-value-field.html',
        controller: 'MultiValueFieldController',
    };
});


app.controller('MultiValueFieldController', function($scope) {

    
    ////////////////////////////////////////////////////

    //Create a model if it doesn't exist
    if (!$scope.model) {
        $scope.model = [];
    } 

    ////////////////////////////////////////////////////

    $scope.add = function(value) {
        if (value && value.length) {
            if(!$scope.contains(value)) {
                 $scope.model.push(value);
            delete $scope._proposed;
            }
           
        }
    }

    ////////////////////////////////////////////////////

    $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }

    ////////////////////////////////////////////////////

    $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    }

   


})
app.directive('realmSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            definedName: '=ngType',
            restrictedRealms: '=ngRestrict',
            popoverDirection: '@',
        },
        templateUrl: 'admin-realm-select/admin-realm-select.html',
        controller: 'RealmSelectController',
    };
});





// app.controller('RealmSelectListController', function($scope) {


// });




app.directive('realmSelectItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-select/admin-realm-select-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + ' track by subRealm._id"></realm-select-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});


app.controller('RealmSelectController', function($scope, $rootScope, TypeService, FluroAccess, FluroSocket) {


    $scope.dynamicPopover = {
        templateUrl: 'admin-realm-select/admin-realm-popover.html',
    };

    if (!$scope.popoverDirection) {
        $scope.popoverDirection = 'bottom';
    }


    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    //////////////////////////////////////////////////

    FluroSocket.on('content.create', socketUpdate);

    //////////////////////////////////////////////////

    function socketUpdate(eventData) {
        //New realm was created
        console.log('New realm available')
        if (eventData.data._type == 'realm') {
            return reloadAvailableRealms();
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.create', socketUpdate);
    });

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    function reloadAvailableRealms() {

        if (!TypeService.definedTypes) {
            TypeService.refreshDefinedTypes().$promise.then(definedTypesLoaded);
        } else {
            //We need to wait until all the defined types have been loaded
            TypeService.definedTypes.$promise.then(definedTypesLoaded);
        }


        //////////////////////////////////////

        function definedTypesLoaded() {

            //If the parent definition exists get it otherwise get the primitive type
            var typeDefinition = TypeService.getTypeFromPath($scope.definedName);

            if (!typeDefinition) {
                return;
            }

            ////////////////////////////////////////////////////////

            var parentType = $scope.definedName;
            if (typeDefinition.parentType) {
                parentType = typeDefinition.parentType;
            }

            //Now we get the realm tree from FluroAccess service
            FluroAccess.retrieveSelectableRealms('create', $scope.definedName, parentType)
                .then(function(realmTree) {

                    //Add the tree to the scope
                    $scope.tree = realmTree;

                    if ($scope.restrictedRealms && $scope.restrictedRealms.length) {
                        console.log('Restricted Realms', $scope.restrictedRealms)

                        var restrictedRealmIDs = _.map($scope.restrictedRealms, function(realm) {
                            if (realm._id) {
                                return realm._id;
                            } else {
                                return realm;
                            }
                        });

                        ///////////////////////////////////////

                        function getFlatRealms(realm) {

                            if (!realm.children) {
                                return realm;
                            }

                            //Loop through each item
                            var children = _.map(realm.children, getFlatRealms);

                            children.push(realm);
                            return children;
                        }

                        ///////////////////////////////////////

                        var flattened = _.chain($scope.tree)
                            .map(getFlatRealms)
                            .flattenDeep()
                            .compact()
                            .uniq()
                            .value()

                        // console.log('FLATTENED', flattened);

                        ///////////////////////////////////////

                        $scope.tree = _.filter(flattened, function(realm) {
                            return _.includes(restrictedRealmIDs, realm._id);
                        });

                        ////////////////////////////////////////////////////////

                        //If only one realm is available select it by default
                        if ($scope.tree.length == 1 && !$scope.model.length) {
                            $scope.model = [$scope.tree[0]];
                        }

                    } else {
                        ////////////////////////////////////////////////////////

                        //If only one realm is available select it by default
                        if (realmTree.length == 1 && !$scope.model.length) {
                            //If there are no children select this only realm by default
                            if (!realmTree.children || !realmTree.children.length) {
                                $scope.model = [realmTree[0]];
                            }
                        }
                    }


                });
        }
    }

    //////////////////////////////////

    //One for good measure
    reloadAvailableRealms();

    //////////////////////////////////

    $scope.isSelectable = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////

        if ($scope.restrictedRealms && $scope.restrictedRealms.length) {

            //Check if the realm we are asking about is in the restricted realms
            return _.some($scope.restrictedRealms, function(restrictedRealmID) {
                if (restrictedRealmID._id) {
                    restrictedRealmID = restrictedRealmID._id;
                }
                return realmID == restrictedRealmID
            });

        } else {
            return true;
        }
    }

    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////

    $scope.contains = function(realm) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == realm._id;
            } else {
                return i == realm._id;
            }
        });
    }

    $scope.toggle = function(realm) {

        var matches = _.filter($scope.model, function(r) {
            var id = r;
            if (_.isObject(r)) {
                id = r._id;
            }

            return (id == realm._id);
        })

        ////////////////////////////////

        if (matches.length) {
            $scope.model = _.reject($scope.model, function(r) {
                var id = r;
                if (_.isObject(r)) {
                    id = r._id;
                }

                return (id == realm._id);
            });
        } else {
            $scope.model.push(realm);
        }

    }



});
app.directive('search', function() {

    return {
        restrict: 'E',
        replace:true,
        scope:{
        	model:'=ngModel'
        },
        templateUrl: 'admin-search/search.html',
        controller: 'SearchFormController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('SearchFormController', function($scope) {


	$scope.clear = function() {
		$scope.model = '';
	}

});

app.directive('tagSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '=ngType',
        },
        templateUrl: 'admin-tag-select/admin-tag-select.html',
        controller: 'TagSelectController',
    };
});

app.controller('TagSelectController', function($scope, $rootScope, CacheManager, Fluro, $http, FluroContent) {


    if (!$scope.model) {
        $scope.model = [];
    }
    
    $scope.dynamicPopover = {
         templateUrl: 'admin-tag-select/admin-tag-popover.html',
    };


    $scope.proposed = {};

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.model.push(item);
        $scope.proposed = {};
    }


    $scope.removeTag = function(tag) {
       _.pull($scope.model, tag);
    }

    
    $scope.getTags = function(val) {

        var url = Fluro.apiURL + '/content/tag/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };


    $scope.create = function() {

       
        var details = {
            title: $scope.proposed.value,
        }

        //Immediately create a tag   
        FluroContent.resource('tag').save(details, function(tag) {

            //$scope.tags.push(tag);
            $scope.model.push(tag);

            //We need to clear the tag cache now
            CacheManager.clear('tag');

            //Clear
            $scope.proposed = {
            }
        }, function(data) {
            console.log('Failed to create tag', data);
        });

    }
});
app.directive('application', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        template:'<div class="outer-application-wrapper" ui-view="builder" ng-if="$root.init"/>',
        //templateUrl: 'application/application.html',
        controller: 'ApplicationController',
    };
});

////////////////////////////////////////////////////////////////////////

app.controller('ApplicationController', function($scope) {


	//$scope.message ='hey';

});


(function() {
    
    Date.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    Date.longMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    Date.shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    
    // defining patterns
    var replaceChars = {
        // Day
        d: function() { return (this.getDate() < 10 ? '0' : '') + this.getDate(); },
        D: function() { return Date.shortDays[this.getDay()]; },
        j: function() { return this.getDate(); },
        l: function() { return Date.longDays[this.getDay()]; },
        N: function() { return (this.getDay() == 0 ? 7 : this.getDay()); },
        S: function() { return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th'))); },
        w: function() { return this.getDay(); },
        z: function() { var d = new Date(this.getFullYear(),0,1); return Math.ceil((this - d) / 86400000); }, // Fixed now
        // Week
        W: function() { 
            var target = new Date(this.valueOf());
            var dayNr = (this.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var firstThursday = target.valueOf();
            target.setMonth(0, 1);
            if (target.getDay() !== 4) {
                target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
            }
            return 1 + Math.ceil((firstThursday - target) / 604800000);
        },
        // Month
        F: function() { return Date.longMonths[this.getMonth()]; },
        m: function() { return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1); },
        M: function() { return Date.shortMonths[this.getMonth()]; },
        n: function() { return this.getMonth() + 1; },
        t: function() { var d = new Date(); return new Date(d.getFullYear(), d.getMonth(), 0).getDate() }, // Fixed now, gets #days of date
        // Year
        L: function() { var year = this.getFullYear(); return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)); },   // Fixed now
        o: function() { var d  = new Date(this.valueOf());  d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3); return d.getFullYear();}, //Fixed now
        Y: function() { return this.getFullYear(); },
        y: function() { return ('' + this.getFullYear()).substr(2); },
        // Time
        a: function() { return this.getHours() < 12 ? 'am' : 'pm'; },
        A: function() { return this.getHours() < 12 ? 'AM' : 'PM'; },
        B: function() { return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24); }, // Fixed now
        g: function() { return this.getHours() % 12 || 12; },
        G: function() { return this.getHours(); },
        h: function() { return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12); },
        H: function() { return (this.getHours() < 10 ? '0' : '') + this.getHours(); },
        i: function() { return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes(); },
        s: function() { return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds(); },
        u: function() { var m = this.getMilliseconds(); return (m < 10 ? '00' : (m < 100 ?
    '0' : '')) + m; },
        // Timezone
        e: function() { return "Not Yet Supported"; },
        I: function() {
            var DST = null;
                for (var i = 0; i < 12; ++i) {
                        var d = new Date(this.getFullYear(), i, 1);
                        var offset = d.getTimezoneOffset();
    
                        if (DST === null) DST = offset;
                        else if (offset < DST) { DST = offset; break; }                     else if (offset > DST) break;
                }
                return (this.getTimezoneOffset() == DST) | 0;
            },
        O: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00'; },
        P: function() { return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00'; }, // Fixed now
        T: function() { return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1'); },
        Z: function() { return -this.getTimezoneOffset() * 60; },
        // Full Date/Time
        c: function() { return this.format("Y-m-d\\TH:i:sP"); }, // Fixed now
        r: function() { return this.toString(); },
        U: function() { return this.getTime() / 1000; }
    };

    // Simulates PHP's date function
    Date.prototype.format = function(format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function(_, esc, chr) {
            return (esc === '' && replaceChars[chr]) ? replaceChars[chr].call(date) : chr;
        });
    };

}).call(this);

(function () {
	'use strict';
	
	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @version 1.0.3
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */
	
	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/
	
	
	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} options The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;
	
		options = options || {};
	
		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;
	
	
		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;
	
	
		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;
	
	
		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;
	
	
		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;
	
	
		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;
	
	
		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;
	
	
		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;
	
		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;
	
		if (FastClick.notNeeded(layer)) {
			return;
		}
	
		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}
	
	
		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}
	
		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}
	
		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);
	
		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};
	
			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}
	
		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {
	
			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}
	
	
	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0;
	
	
	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent);
	
	
	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);
	
	
	/**
	 * iOS 6.0(+?) requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS ([6-9]|\d{2})_\d/).test(navigator.userAgent);
	
	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;
	
	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {
	
		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}
	
			break;
		case 'input':
	
			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}
	
			break;
		case 'label':
		case 'video':
			return true;
		}
	
		return (/\bneedsclick\b/).test(target.className);
	};
	
	
	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}
	
			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};
	
	
	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;
	
		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}
	
		touch = event.changedTouches[0];
	
		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};
	
	FastClick.prototype.determineEventType = function(targetElement) {
	
		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}
	
		return 'click';
	};
	
	
	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;
	
		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};
	
	
	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;
	
		scrollParent = targetElement.fastClickScrollParent;
	
		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}
	
				parentElement = parentElement.parentElement;
			} while (parentElement);
		}
	
		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};
	
	
	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
	
		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}
	
		return eventTarget;
	};
	
	
	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;
	
		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}
	
		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];
	
		if (deviceIsIOS) {
	
			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}
	
			if (!deviceIsIOS4) {
	
				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}
	
				this.lastTouchIdentifier = touch.identifier;
	
				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}
	
		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;
	
		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;
	
		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}
	
		return true;
	};
	
	
	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;
	
		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}
	
		return false;
	};
	
	
	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}
	
		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}
	
		return true;
	};
	
	
	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {
	
		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}
	
		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}
	
		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};
	
	
	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
	
		if (!this.trackingClick) {
			return true;
		}
	
		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}
	
		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;
	
		this.lastClickTime = event.timeStamp;
	
		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;
	
		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];
	
			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}
	
		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}
	
				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {
	
			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}
	
			this.focus(targetElement);
			this.sendClick(targetElement, event);
	
			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}
	
			return false;
		}
	
		if (deviceIsIOS && !deviceIsIOS4) {
	
			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}
	
		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}
	
		return false;
	};
	
	
	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};
	
	
	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {
	
		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}
	
		if (event.forwardedTouchEvent) {
			return true;
		}
	
		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}
	
		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {
	
			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {
	
				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}
	
			// Cancel the event
			event.stopPropagation();
			event.preventDefault();
	
			return false;
		}
	
		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};
	
	
	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;
	
		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}
	
		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}
	
		permitted = this.onMouse(event);
	
		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}
	
		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};
	
	
	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;
	
		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}
	
		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};
	
	
	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
	
		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}
	
		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];
	
		if (chromeVersion) {
	
			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');
	
				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
	
			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}
	
		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);
	
			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');
	
				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}
	
		// IE10 with -ms-touch-action: none, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none') {
			return true;
		}
	
		return false;
	};
	
	
	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} options The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};
	
	
	if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
	
		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());

app.controller('ContentDeleteController', function($scope, type, definition, $state, FluroContent, Notifications, CacheManager, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;


    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.cancelPath = type.path;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.cancelPath = type.path + ".custom({definition:'" + definition.definitionName + "'})";
    }


    ///////////////////////////////////////////////////

    $scope.delete = function() {

        var definedName = type.path;
        if(definition && definition.definitionName) {
            definedName = definition.definitionName;
        }

		
		if($scope.item._id) {
			//Update existing article
			FluroContent.resource(definedName).delete({
                id: $scope.item._id,
            }, deleteSuccess, deleteFailed);
		}
	}

	//////////////////////////////////////////////////

	function deleteSuccess(data) {
        var title = data.title;
        if(data.name) {
            title = data.name;
        }

        Notifications.status(title + ' was deleted successfully')
		
        if(data.definition && data.definition != data._type) {
            CacheManager.clear(data.definition);
            $state.go(type.path + '.custom', {definition:data.definition});
        } else {
            CacheManager.clear(type.path);
            $state.go(type.path);
        }
	}

	function deleteFailed(data) {

        var title = $scope.item.title;
        if($scope.item.name) {
            title = $scope.item.name;
        }

		 Notifications.error('Failed to delete ' + title)
	}



})
app.controller('ContentFormController', function($rootScope, Fluro, ModalService, $scope, FluroStorage, extras, FluroSocket, $rootScope, $state, $controller, Asset, Notifications, CacheManager, TypeService, FluroContent, type, definition, item) {

    $scope.item = item;
    $scope.extras = extras;

    if ($scope.item._id) {
        $scope.saveText = 'Save';
        $scope.cancelText = 'Close';
    } else {
        $scope.saveText = 'Create';
        $scope.cancelText = 'Cancel';
    }

    //Force defaults for saving
    $scope.defaultSaveOptions = {};


    $scope.type = type;
    $scope.definition = definition;

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/form.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    

    //////////////////////////////

    //Cancel path

    if (definition && definition.definitionName) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
        $scope.icon = definition.parentType;
    } else {
        $scope.definedType = type.path;
        $scope.icon = type.path;
    }

    ///////////////////////////////////

    $scope.titleLabel = 'Title';
    $scope.bodyLabel = 'Body';

    if (definition && definition.definitionName) {
        if (definition.data) {
            if (definition.data.titleLabel && definition.data.titleLabel.length) {
                $scope.titleLabel = definition.data.titleLabel;
            }

            if (definition.data.bodyLabel && definition.data.bodyLabel.length) {
                $scope.bodyLabel = definition.data.bodyLabel;
            }
        }
    }

    //////////////////////////////

    //Setup Local storage
    var session;

    if ($scope.item._id) {
        session = FluroStorage.sessionStorage('form-' + $scope.item._id)
    }


    //////////////////////////////

    $scope.openAPIDocument = function() {
        var url = Fluro.apiURL + '/content/' + $scope.definedType + '/' + $scope.item._id + '?context=edit';

        if(Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        //Open in a new window
        window.open(url);
    }


    //////////////////////////////



    var socketEnabled = true;
    /**/


    FluroSocket.on('content.edit', socketUpdate);


    function socketUpdate(data) {

        if (socketEnabled) {

            //Same id content
            var sameId = (data.item._id == $scope.item._id);
            var newVersion = (data.item.updated != item.updated);


            console.log('Socket change', item.title, sameId, data.item.updated, item.updated);

            if (sameId && newVersion) {


                console.log('update this data ', data.item._id, $scope.item._id, data.item.title);

                //Notify the user
                //Notifications.warning('Changes to this ' + $scope.titleSingular + ' were made by ' + data.user.name);

                //Assign the new content
                socketEnabled = false;

                _.assign($scope.item, data.item);
                //$scope.item = data.item;
                socketEnabled = true;

                // console.log($rootScope.user._id, data.user._id)
                if ($rootScope.user._id != data.user._id) {
                    Notifications.warning('This content was just updated by ' + data.user.name);
                } else {
                    Notifications.status('You updated this in another window');
                }

            }
        }
    }

    /**/

    //////////////////////////////////////////

    /*
    $scope.$watch('item', function(newData, oldData) {

        if (socketEnabled) {
            //////////////////////////////////////////


            //newData.updated = new Date();

            //Broadcast the message
            var message = {
                event: 'content.live',
                item: newData,
                user: {
                    _id: $rootScope.user._id,
                    name: $rootScope.user.name,
                    account: {
                        _id: $rootScope.user.account._id
                    }
                }
            }


            //////////////////////////////////////////

            FluroSocket.emit('content', message, function(res) {
                console.log('FluroContent Emitted')
            })
        }

        //////////////////////////////////////////


    }, true)
*/

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.useInlineTitle = (type.path == 'article');

    //////////////////////////////////////////////////

    $scope.getAssetUrl = Asset.getUrl;
    $scope.getDownloadUrl = Asset.downloadUrl;


    //Cleanup Pasted HTML
    $scope.cleanupPastedHTML = $rootScope.cleanupPastedHTML;


    //////////////////////////////////////////////////

    $(document).on('keydown', keyDown);

    function keyDown(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    event.stopPropagation();
                    $scope.save();
                    break;
            }
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        $(document).off('keydown', keyDown);

        FluroSocket.off('content', socketUpdate);
    });


    //////////////////////////////////////////////////

    //////////////////////////////////////////////////

    //Save
    $scope.save = function(options) {

        //Disable Socket
        socketEnabled = false;

        console.log('Disable socket')

        if (!options) {
            options = $scope.defaultSaveOptions;
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        if (options.forceVersioning) {

            if (!$scope.item.version || $scope.item.version.length) {
                console.log('Forcing a version save');
                $scope.item.version = 'Changes made by ' + $rootScope.user.name;
            }
        }

        //////////////////////////////////////////

        console.log('Save item', $scope.item.applicationIcon, $scope.item.icon);

        //////////////////////////////////////////

        if ($scope.item._id) {


            console.log('Push item', $scope.item);
            
            //Edit existing content
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);


        } else {

            /*if (type.path == 'event') {
                FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccess, $scope.saveFail);
            } else {
                */
            FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessExit, $scope.saveFail);
            //}
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccess = function(result) {
        //Clear cache
        CacheManager.clear($scope.definedType);

        if (result._id) {
            //console.log('Updated to version', $scope.item.__v, result.__v);
            $scope.item.__v = result.__v;
        }

        if (result._type == 'definition') {
            TypeService.sideLoadDefinition(result);
        }


        if (result.name) {
            Notifications.status(result.name + ' saved successfully')
        } else {
            Notifications.status(result.title + ' saved successfully')
        }


        //Disable Socket
        socketEnabled = true;
        //console.log('Enable Socket')

        //////////////////////////////////////////////////

        //Able to close and keep moving
        if ($scope.closeOnSave) {
            console.log('Close on save')

            if ($scope.$close) {
                $scope.$close(result)
            }

            if (definition) {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                //Go to path
                $state.go(type.path)
            }

            return true;
        }


        //////////////////////////////////////////////////

        //Finish the save
        return $scope.saveFinished(result);
    }

    //////////////////////////////////////////////////

    $scope.saveFinished = function(result) {
        //console.log('Save Finished')
        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            return false;
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccessExit = function(result) {
        //console.log('Save Success Exit')
        var res = $scope.saveSuccess(result);

        if (!res) {
            if (definition) {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                //Go to path
                $state.go(type.path)
            }
        }
    }



    //////////////////////////////////////////////////

    $scope.saveFail = function(result) {

        if (!result) {
            return Notifications.error('Failed to save')
        }


        if (!result.data) {
            return Notifications.error('Failed to save')
        }

        if (result.data.errors) {
            _.each(result.data.errors, function(err) {
                if (_.isObject(err)) {
                    Notifications.error(err.message)
                } else {
                    Notifications.error(err)
                }
            });
            return;
        }

        if (result.data) {
            return Notifications.error(result.data)
        }

        return Notifications.error('Failed to save document')


    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }


        if ($scope.item._id == $rootScope.user._id) {
            return $state.go('home')
        }

        //Go to the normal path
        if (definition && definition.definitionName) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            $state.go(type.path)
        }
    }





    var realmSelectEnabled = true;
    var tagSelectEnabled = true;

    switch ($scope.type.path) {
        case 'realm':
        case 'account':
            realmSelectEnabled = false;
            tagSelectEnabled = false;
            break;
        case 'role':
            tagSelectEnabled = false;
            break;
        case 'tag':
            tagSelectEnabled = false;
            break;
        case 'event':
            $controller('EventFormController', {
                $scope: $scope
            });
            break;
        case 'integration':
            $controller('IntegrationFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'application':
            $controller('ApplicationFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'user':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('UserFormController', {
                $scope: $scope
            });
            break;
        case 'persona':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('PersonaFormController', {
                $scope: $scope
            });
            break;
        case 'contactdetail':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('ContactDetailFormController', {
                $scope: $scope
            });
            break;
        case 'family':


            $controller('FamilyFormController', {
                $scope: $scope
            });
            break;
        case 'query':
            $controller('QueryFormController', {
                $scope: $scope
            });
            break;
        case 'purchase':
            $controller('PurchaseFormController', {
                $scope: $scope
            });
            break;
        case 'checkin':
            $controller('CheckinFormController', {
                $scope: $scope
            });
            break;
        case 'asset':
        case 'video':
        case 'audio':
        case 'image':
            $controller('AssetFormController', {
                $scope: $scope
            });
            break;
    }

    //////////////////////////////////////////////////

    $scope.realmSelectEnabled = realmSelectEnabled;
    $scope.tagSelectEnabled = tagSelectEnabled;

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    // $scope.getExportedJSON = function() {
    //     //Create the json object
    //     var json = angular.toJson($scope.item);

    //     //JSON.stringify(json)
    //     var data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json));

    //     return data;
    // }

    //////////////////////////////////////////////////

/**
    $scope.import = function(object) {
       ModalService.import
    }

    //////////////////////////////////////////////////

    $scope.importDataObject = function(object) {
        $scope.item = object;
    }

    //////////////////////////////////////////////////

    $scope.export = function() {

        //Create the json object
        var json = angular.toJson($scope.item);

        //JSON.stringify(json)
        var data = "text/json;charset=utf-8," + encodeURIComponent(json);


        //Download data
        window.open('data:' + data, '_blank');
        //angular.element('<a href="data:' + data + '" download="'+$scope.item._id+'.json"><i class="fa fa-share-square-o"></i></a>');
        //.appendTo('.status-summary');


    }
    /**/


})
//////////////////////////////////////////////////////////////////////////

app.controller('ContentListController', function($scope, $filter, extras, Notifications, CacheManager, FluroSocket, $rootScope, $controller, $state, $filter, $compile, Fluro, FluroAccess, ModalService, Selection, FluroStorage, Tools, items, definition, definitions, type) {

    //////////////////////////////

    //Add the items to the scope
    $scope.items = items;
    $scope.definitions = definitions;
    $scope.type = type;
    $scope.definition = definition;

    $scope.extras = extras;


    $scope.templates = _.filter(items, function(item) {
        return item.status == 'template';
    })

    
    //var socketEnabled = true;

    FluroSocket.on('content', socketUpdate);


    function socketUpdate(data) {
        if (data.item._type == type.path) {

            if (data.item.definition) {
                CacheManager.clear(data.item.definition);
            } else {
                CacheManager.clear(type.path);
            }
            
            Notifications.warning('Some of this content was just updated by ' + data.user.name);

            //console.log('Refresh list because changes were made')
            //$state.reload();
            //refreshList();
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content', socketUpdate);
    });

    //////////////////////////////


    //////////////////////////////

    $scope.updatedTooltip = function(item) {
        if(item.updatedBy) {
            return 'By ' + item.updatedBy + ' ' + $filter('timeago')(item.updated);
        } else {
            return $filter('timeago')(item.updated);
        }

        //return $sce.trustAsHtml('<span ng-if="item.updatedBy">By ' +item.updatedBy+ '</span> {{item.updated | timeago}}');
    }

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.filtersActive = function() {
        return _.keys($scope.search.filters).length;
    }

    switch (type.path) {
        case 'event':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        case 'account':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        default:
            $scope.template = 'fluro-admin-content/types/list.html';
            break;
    }
    //////////////////////////////

    //Setup Local storage
    var local;
    var session;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;

        local = FluroStorage.localStorage(definition.definitionName)
        session = FluroStorage.sessionStorage(definition.definitionName)
    } else {
        local = FluroStorage.localStorage(type.path)
        session = FluroStorage.sessionStorage(type.path)

        $scope.definedType = type.path;
    }

    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {
                    status: ['active'],
                },
                reverse: false,
        }
    } else {
        $scope.search = session.search;
    }

    //////////////////////////////

    //////////////////////////////

    //Setup Search    
    if (!local.settings) {
        $scope.settings =
            local.settings = {}
    } else {
        $scope.settings = local.settings;
    }

    //////////////////////////////

    switch ($scope.type.path) {
        case 'asset':
        case 'image':
        case 'video':
        case 'audio':
            $scope.uploadType = true;

            $scope.uploadName = $scope.type.path;
            if(definition) {
                $scope.uploadName = definition.definitionName;
            }
            break;

    }
    //////////////////////////////

    $scope.currentViewMode = function(viewMode) {
        if (viewMode == 'default') {
            return (!$scope.settings.viewMode || $scope.settings.viewMode == '' || $scope.settings.viewMode == viewMode);
        } else {
            return $scope.settings.viewMode == viewMode;
        }
    }

    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
        $scope.updateFilters();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        $scope.updateFilters();
    }




    //////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize:8,
        }
    } else {
        $scope.pager = session.pager;
    }

    //////////////////////////////

    $scope.$watch('search', function() {
        $scope.updateFilters();
    }, true);

    ///////////////////////////////////////////////

    $scope.canSelect = function(item) {
        return true;
    }

    ///////////////////////////////////////////////

    $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect);

    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }

    

    ///////////////////////////////////////////////

    $scope.deselectFiltered = function() {
        $scope.selection.deselectMultiple($scope.filteredItems);
    }

    ///////////////////////////////////////////////

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    ///////////////////////////////////////////////

    $scope.viewInModal = function(item) {
        ModalService.view(item);
    }

    ///////////////////////////////////////////////

    $scope.editInModal = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {
        });
    }

    ///////////////////////////////////////////////

    $scope.createInModal = function() {
        ModalService.create($scope.type, function(result) {
            $state.reload();
        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////

    $scope.createFromTemplate = function(template) {

        ModalService.edit(template, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, false, true, $scope);


    }

    ///////////////////////////////////////////////

    $scope.copyItem = function(item) {

        ModalService.edit(item, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);


    }



    ///////////////////////////////////////////////

    $scope.canCreate = function() {
        var c = FluroAccess.can('create', type.path);

        if (definition) {
            c = FluroAccess.can('create', definition.definitionName);
        }

        return c;
    }


    ///////////////////////////////////////////////

    $scope.canCopy = function(item) {
        var typeName = $scope.type.path;
        if (item.definition) {
            typeName = item.definition;
        }

        return FluroAccess.can('create', typeName);
    }

    ///////////////////////////////////////////////

    
    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, (type.path == 'user'));
    }

    /*function(item) {

        if (!item) {
            return false;
        }

        var canEditAny = FluroAccess.can('edit any', type.path);
        var canEditOwn = FluroAccess.can('edit own', type.path);

        if (definition) {
            canEditAny = FluroAccess.can('edit any', definition.definitionName);
            canEditOwn = FluroAccess.can('edit own', definition.definitionName);
        }

        if (canEditAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canEditOwn;
            }
        }
    }
    */




    ///////////////////////////////////////////////

     $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item, (type.path == 'user'));
    }
    /*function(item) {

        if (!item) {
            return false;
        }

        var canDeleteAny = FluroAccess.can('delete any', type.path);
        var canDeleteOwn = FluroAccess.can('delete own', type.path);

        if (definition) {
            canDeleteAny = FluroAccess.can('delete any', definition.definitionName);
            canDeleteOwn = FluroAccess.can('delete own', definition.definitionName);
        }

        if (canDeleteAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canDeleteOwn;
            }
        }
    }
    */



    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;


            //Update the current page
            $scope.updatePage();
        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {

        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        //Now break it up into pages
        var pageItems = $filter('startFrom')($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }




    ////////////////////////////////////////////

    switch (type.path) {
        case 'event':
            $controller('EventListController', {
                $scope: $scope
            });
            break;
        case 'user':
            $controller('UserListController', {
                $scope: $scope
            });
            break;
        case 'persona':
            $controller('PersonaListController', {
                $scope: $scope
            });
            break;
        case 'account':
            $controller('AccountListController', {
                $scope: $scope
            });
            break;
    }


    $scope.updateFilters();

    ///////////////////////////////////////////////

    /*
	///////////////////////////////////////

	$scope.$watch('search.terms', function(keywords) {
		return $http.get(Fluro.apiURL + '/search', {
            ignoreLoadingBar: true,
            params: {
                keys: keywords,
                //type: $scope.restrictType,
                limit: 100,
            }
        }).then(function(response) {
           $scope.items = response.data;
        });
	})
*/

    ///////////////////////////////////////


});
app.controller('ContentTrashController', function($scope, $rootScope, $filter, $state, FluroContent, Notifications, CacheManager, Batch, FluroStorage, FluroAccess, Selection, items, definition, definitions, type) {

    //////////////////////////////

    //Add the items to the scope
    $scope.items = items;
    $scope.definitions = definitions;
    $scope.type = type;
    $scope.definition = definition;





    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    //////////////////////////////

    //Setup Local storage
    var local;
    var session;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;

        local = FluroStorage.localStorage(definition.definitionName + '-trash')
        session = FluroStorage.sessionStorage(definition.definitionName + '-trash')
    } else {
        local = FluroStorage.localStorage(type.path + '-trash')
        session = FluroStorage.sessionStorage(type.path + '-trash')
    }


    $scope.template = 'fluro-admin-content/types/trash.html';


    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {},
                reverse: false,
        }
    } else {
        $scope.search = session.search;

        if ($scope.search.filters.status) {
            delete $scope.search.filters.status;
        }
    }








    //////////////////////////////

    //Setup Search    
    if (!local.settings) {
        $scope.settings =
            local.settings = {}
    } else {
        $scope.settings = local.settings;
    }



    console.log($scope, 'SCOPE');
    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
        $scope.updateFilters();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        $scope.updateFilters();
    }


    //////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize:8,
        }
    } else {
        $scope.pager = session.pager;
    }

    //////////////////////////////

    $scope.$watch('search', function() {
        $scope.updateFilters();
    }, true);


    ///////////////////////////////////////////////

    $scope.canSelect = function(item) {
        return true;
    }

    ///////////////////////////////////////////////

    $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect);

    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    ///////////////////////////////////////////////

    $scope.canDestroy = function(item) {

        if (!item) {
            return false;
        }

        var canDestroyAny = FluroAccess.can('destroy any', type.path);
        var canDestroyOwn = FluroAccess.can('destroy own', type.path);

        if (definition) {
            canDestroyAny = FluroAccess.can('destroy any', definition.definitionName);
            canDestroyOwn = FluroAccess.can('destroy own', definition.definitionName);
        }

        if (canDestroyAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canDestroyOwn;
            }
        }
    }

    ///////////////////////////////////////////////

    $scope.canRestore = function(item) {

        if (!item) {
            return false;
        }

        var canRestoreAny = FluroAccess.can('restore any', type.path);
        var canRestoreOwn = FluroAccess.can('restore own', type.path);

        if (definition) {
            canRestoreAny = FluroAccess.can('restore any', definition.definitionName);
            canRestoreOwn = FluroAccess.can('restore own', definition.definitionName);
        }

        if (canRestoreAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canRestoreOwn;
            }
        }
    }

    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);


            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;





            //Update the current page
            $scope.updatePage();

        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {



        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        //Now break it up into pages
        var pageItems = $filter('startFrom')($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }



    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


    $scope.done = function(data) {

        ///////////////////////

        //Find all the caches we need to clear
        var caches = _.chain(data.results)
            .map(function(result) {
                return [result.definition, result._type];
            })
            .flatten()
            .value();

        ///////////////////////

        //Make sure we refresh the current view
        if ($scope.definition) {
            console.log('GET THE DEFINITION');
            caches.push($scope.definition.definitionName);
            caches.push($scope.definition.definitionName + '-trash');
        }

        caches.push($scope.type.path);
        caches.push($scope.type.path + '-trash');

        ///////////////////////

        console.log('Caches to clear', caches);

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            console.log('Clear cache', t);
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        //Notifications.status('Updated  ' + data.results.length + ' ' + $scope.type.plural);
    }

    ///////////////////////////////////////////////

    /**/
    $scope.restore = function(item) {
        //Edit existing user
        FluroContent.endpoint('content/' + $scope.type.path + '/'+item._id+'/restore').get().$promise.then(function(res) {

            Notifications.status('Restored ' + res.title);
            //Refresh
            $scope.done(res);
        });
    }
   /**

    ////////////////////////////////////////////

    $scope.restore = function(item) {
        console.log('Restore selected item')

        var id = item;
        if(_.isObject(item)) {
            id = item._id;
        }

        Batch.restore({
            ids: [id],
        }, function(err, res) {

            if (err) {
                console.log('DONE ERR', err)
                return Notifications.error(err);
            }

            Notifications.status('Restored ' + res.results.length + ' ' + $scope.type.plural);
            

            $scope.done(res);
        });
    }
    /**/



    ////////////////////////////////////////////

    $scope.destroy = function(item) {
        FluroContent.resource($scope.type.path).delete({
            id: item._id,
            destroy: true,
        }, function(res) {

            Notifications.status('Destroyed ' + res.title);
            //Refresh
            $scope.done(res);
        });
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    $scope.updateFilters();









});
app.service('ContentVersionService', function(FluroContent) {

	var controller = {};

	////////////////////////////////////

	controller.list = function(item) {

		var id = item._id;
		var type =  item._type;

		if(item.definition) {
			type = item.definition;
		}

		return FluroContent.endpoint('content/'+type+'/'+ id +'/versions').query();
	}

	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////

	return controller;
})
app.controller('ContentViewController', function($scope, extras, $rootScope, $controller, FluroSocket, Notifications, ModalService, $state, FluroAccess, type, definition, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;
    $scope.extras = extras;

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/view.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    //Cancel path

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
    } else {
        $scope.definedType = type.path;
    }


    $scope.canEdit = FluroAccess.canEditItem;


    //////////////////////////////



    var socketEnabled = true;
    /**/
    FluroSocket.on('content', function(data) {

        if (socketEnabled) {
            if (data.item._id == item._id) {
                Notifications.warning('Changes to this ' + $scope.titleSingular + ' were just made by ' + data.user.name);
                //$state.reload();
                if (angular.equals(data.item, item)) {
                    console.log('No change');
                } else {

                    socketEnabled = false;
                    //_.assign($scope.item, data.item);
                    $scope.item = data.item;
                    socketEnabled = true;

                    Notifications.warning('This content was just updated by ' + data.user.name);
                }
            }
        }
    });
    /**/



    //ui-sref="{{type.path}}.edit({id:item._id})"

    //////////////////////////////////////////////////

    $scope.editItem = function() {

        if ($scope.$dismiss) {
            ModalService.edit(item);
            $scope.$dismiss();
            /*
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

       
           
            */
        } else {
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

        }

    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }

        if ($scope.item._id == $rootScope.user._id) {
            return $state.go('home')
        }

        //Go to the normal path
        if (definition && definition.definitionName) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            $state.go(type.path)
        }
    }


    switch ($scope.item._type) {
        case 'contact':
            $controller('ContactViewController', {
                $scope: $scope
            });
            break;
        
    }





})
/////////////////////////////////////////////////////////////////

//WE NEED TO UPDATE THIS TO BE NICER (admin-content-select that is)
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'reference-select',
        templateUrl: 'fluro-admin-content/content-formly/reference-select.html',
        controller: function($scope) {


            //Get definition
            var definition = $scope.to.definition;

            if (!definition.params) {
                definition.params = {}
            }

            //Create a config object
            $scope.config = {
                canCreate: true,
                minimum: definition.minimum,
                maximum: definition.maximum,
                type: definition.params.restrictType,
                searchInheritable: definition.params.searchInheritable
            };
        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'date-select',
        templateUrl: 'fluro-admin-content/content-formly/date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'object-editor',
        templateUrl: 'fluro-admin-content/content-formly/object-editor.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'custom',
        templateUrl: 'fluro-admin-content/content-formly/unknown.html',
        wrapper: ['bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'terms',
        templateUrl: 'fluro-admin-content/content-formly/terms.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'code',
        templateUrl: 'fluro-admin-content/content-formly/code.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {

            var aceEditor;

            $scope.aceLoaded = function(_editor) {
                // Options
                //_editor.setReadOnly(true);
                aceEditor = _editor;

                var syntaxName;

                if ($scope.to.definition.params && $scope.to.definition.params.syntax) {
                    syntaxName = syntaxName = $scope.to.definition.params.syntax;
                }

                if (aceEditor && syntaxName) {
                    aceEditor.getSession().setMode("ace/mode/" + syntaxName);
                }
            }
        }
    });

    formlyConfig.setType({
        name: 'wysiwyg',
        templateUrl: 'fluro-admin-content/content-formly/wysiwyg.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    /**
// set templates here
    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-interaction-form/nested.html',
        controller: function($scope) {

            $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    console.log('Create it!')
                    $scope.model[$scope.options.key] = [];
                }

            })

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;

            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                console.log('Only 1!!!', $scope.options)
            } else {
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({});
                    });
                }
            }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        //defaultValue:[],
        //noFormControl: true,
        //template: '<formly-form model="model[options.key]" fields="options.data.fields"></formly-form>'
    });

    /**/

    // set templates here
    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-admin-content/content-formly/nested.html',
        // defaultOptions: {
        //    noFormControl: true,
        // },
        controller: function($scope) {
            $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    $scope.model[$scope.options.key] = [];
                }
            })

            $scope.settings = {};

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;

            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                //console.log('Only 1 Please!!!!', $scope.options)
            } else {

                //console.log('Fill er up')
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({});
                    });
                }
            }

            //////////////////////////////////////
            //////////////////////////////////////

            $scope.firstLine = function(item) {
                var keys = _.chain(item)
                    .keys()
                    .without('active')
                    .filter(function(key) {
                        var i = key.indexOf('$$');
                        if (i == -1) {
                            return true;
                        } else {
                            return false;
                        }
                    })
                    .slice(0, 3)
                    .value();

                return _.at(item, keys).join(',').slice(0, 150);
            }

            $scope.items = function() {
                return $scope.model[$scope.options.key];
            }




            //[{title:'one'},{title:'two'},{title:'three'}]

            ////////////////////////////////////

            //var minimum = $scope.to.definition.minimum;
            //var maximum = $scope.to.definition.maximum;

            // if (minimum && !$scope.model[$scope.options.key].length) {
            //     _.times(minimum, function() {
            //         $scope.model[$scope.options.key].push({});
            //     });
            // }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            $scope.remove = function(entry) {
                //model[options.key].splice($index, 1)
                console.log('Pull Entry', entry, $scope.model[$scope.options.key].indexOf(entry));
                _.pull($scope.model[$scope.options.key], entry);
            }

            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            ////////////////////////////////////

            if (maximum != 1 && $scope.model && $scope.model[$scope.options.key] && $scope.model[$scope.options.key].length) {
                $scope.settings.active = $scope.model[$scope.options.key][0];
            }
        }
    });




    formlyConfig.setType({
        name: 'list-select',
        templateUrl: 'fluro-admin-content/content-formly/list-select.html',
        controller: function($scope, FluroValidate) {



            //Get the definition
            var definition = $scope.to.definition;

            //Minimum and maximum
            var minimum = definition.minimum;
            var maximum = definition.maximum;

            /////////////////////////////////////////////////////////////////////////


            $scope.multiple = (maximum != 1);

            /////////////////////////////////////////////////////////////////////////

            var to = $scope.to;
            var opts = $scope.options;

            $scope.selection = {
                values: [],
                value: null,
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.contains = function(value) {
                if ($scope.multiple) {
                    //Check if the values are selected
                    return _.contains($scope.selection.values, value);
                } else {
                    return $scope.selection.value == value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.select = function(value) {
                //console.log('SELECTION', $scope.selection);
                if ($scope.multiple) {
                    $scope.selection.values.push(value);
                } else {
                    $scope.selection.value = value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.deselect = function(value) {
                if ($scope.multiple) {
                    _.pull($scope.selection.values, value);
                } else {
                    $scope.selection.value = null;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.toggle = function(reference) {
                if ($scope.contains(reference)) {
                    $scope.deselect(reference);
                } else {
                    $scope.select(reference);
                }

                //Update model
                setModel();
            }


            /////////////////////////////////////////////////////////////////////////

            // initialize the checkboxes check property
            $scope.$watch('model', function(newModelValue) {
                var modelValue;

                //If there is properties in the FORM model
                if (_.keys(newModelValue).length) {

                    //Get the model for this particular field
                    modelValue = newModelValue[opts.key];



                    //$scope.$watch('to.options', function(newOptionsValues) {
                    if ($scope.multiple) {
                        if (_.isArray(modelValue)) {
                            $scope.selection.values = angular.copy(modelValue);
                        } else {
                            if (modelValue) {
                                $scope.selection.values = [angular.copy(modelValue)];
                            }
                        }
                    } else {
                        if(modelValue) {
                           $scope.selection.value = angular.copy(modelValue);
                        }
                    }
                }
            }, true);


            /////////////////////////////////////////////////////////////////////////

            function checkValidity() {

                var validRequired;
                var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

                //Check if multiple
                if ($scope.multiple) {
                    if ($scope.to.required) {
                        validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
                    }
                } else {
                    if ($scope.to.required) {
                        if ($scope.model[opts.key]) {
                            validRequired = true;
                        }
                    }
                }

                $scope.fc.$setValidity('required', validRequired);
                $scope.fc.$setValidity('validInput', validInput);
            }

            /////////////////////////////////////////////////////////////////////////

            function setModel() {
                if ($scope.multiple) {
                    $scope.model[opts.key] = angular.copy($scope.selection.values);
                } else {
                    $scope.model[opts.key] = angular.copy($scope.selection.value);
                }

                $scope.fc.$setTouched();
                checkValidity();
            }

            /////////////////////////////////////////////////////////////////////////

            if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
                $scope.$watch(function() {
                    return $scope.to.required;
                }, function(newValue) {
                    checkValidity();
                });
            }

            /////////////////////////////////////////////////////////////////////////

            if ($scope.to.required) {
                var unwatchFormControl = $scope.$watch('fc', function(newValue) {
                    if (!newValue) {
                        return;
                    }
                    checkValidity();
                    unwatchFormControl();
                });
            }

        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });
});


////////////////////////////////////////////////////////////////////////


app.directive('contentFormly', function(FluroContent, FluroContentRetrieval, FluroValidate) {
    return {
        restrict: 'E',
        //replace: true,
        transclude: true,
        scope: {
            item: '=ngModel',
            definition: '=',
            agreements:'=?'
        },
        templateUrl: 'fluro-admin-content/content-formly/form.html',
        //controller: 'InteractionFormController',
        //templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {


            if (!$scope.item) {
                $scope.item = {}
            }


            /////////////////////////////////////////////////////////////////

            $scope.$watch('item', function(item) {


                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = item; //$scope.item;

                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = $scope.item;


                // An array of our form fields with configuration
                // and options set. We make reference to this in
                // the 'fields' attribute on the  element
                $scope.vm.fields = [];

                /////////////////////////////////////////////////////////////////

                //Keep track of the state of the form
                $scope.vm.state = 'ready';

                /////////////////////////////////////////////////////////////////

                //$scope.vm.onSubmit = submitInteraction;

                /////////////////////////////////////////////////////////////////

                function addFieldDefinition(array, fieldDefinition) {

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Create a new field
                    var newField = {};
                    newField.key = fieldDefinition.key;


                    /////////////////////////////

                    //Add the class name if applicable
                    if (fieldDefinition.className && !fieldDefinition.asObject) {
                        newField.className = fieldDefinition.className;
                    }

                    /////////////////////////////

                    //Template Options
                    var templateOptions = {};
                    templateOptions.type = 'text';
                    templateOptions.label = fieldDefinition.title;
                    templateOptions.description = fieldDefinition.description;

                    //Include the definition itself
                    templateOptions.definition = fieldDefinition;

                    /////////////////////////////

                    //Add a placeholder
                    if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                        templateOptions.placeholder = fieldDefinition.placeholder;
                    } else if (fieldDefinition.description && fieldDefinition.description.length) {
                        templateOptions.placeholder = fieldDefinition.description;
                    } else {
                        templateOptions.placeholder = fieldDefinition.title;
                    }

                    /////////////////////////////

                    //Require if minimum is greater than 1 and not a field group
                    templateOptions.required = (fieldDefinition.minimum > 0);

                    /////////////////////////////

                    //Directive or widget
                    switch (fieldDefinition.directive) {
                        case 'value-select':
                        case 'button-select':
                        case 'order-select':
                            newField.type = 'list-select';
                            break;
                        default:
                            newField.type = fieldDefinition.directive;
                            break;
                    }


                    /////////////////////////////

                    //Directive or widget
                    switch (fieldDefinition.type) {
                        case 'reference':
                            //Detour here if we are using a select
                            if (fieldDefinition.directive == 'select' && fieldDefinition.maximum == 1) {

                                //Map Reference objects to strings
                                if ($scope.vm.model[fieldDefinition.key] && $scope.vm.model[fieldDefinition.key]._id) {
                                    $scope.vm.model[fieldDefinition.key] = $scope.vm.model[fieldDefinition.key]._id;
                                }


                            } else {
                                newField.type = 'reference-select';
                            }





                            break;
                        case 'boolean':
                            //Detour here

                            // console.log('GOT PARAMS SORTED', fieldDefinition)
                            if(fieldDefinition.params && fieldDefinition.params.storeData) {
                                newField.type = 'terms';
                                newField.data = {
                                    agreements:$scope.agreements,
                                }
                            } else {
                                 newField.type = 'checkbox';
                            }
                           
                            break;
                        case 'object':
                            //Detour here
                            newField.type = 'object-editor';
                            break;
                        case 'date':
                            //Detour here
                            newField.type = 'date-select';
                            break;
                    }





                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Allowed Options

                    if (fieldDefinition.type == 'reference') {




                        //If we have allowed references specified
                        if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                            templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id,
                                }
                            });
                        } else {

                            //We want to load all the options from the server
                            templateOptions.options = [];

                            if (fieldDefinition.sourceQuery) {

                                //We use the query to find all the references we can find
                                var queryId = fieldDefinition.sourceQuery;
                                if (queryId._id) {
                                    queryId = queryId._id;
                                }


                                /////////////////////////

                                var options = {};

                                //If we need to template the query
                                if (fieldDefinition.queryTemplate) {
                                    options.template = fieldDefinition.queryTemplate;
                                    if (options.template._id) {
                                        options.template = options.template._id;
                                    }
                                }

                                /////////////////////////

                                //Now retrieve the query
                                var promise = FluroContentRetrieval.getQuery(queryId, options);


                                //var promise = FluroContentRetrieval.query(null, null, queryId, true);

                                //Now get the results from the query
                                promise.then(function(res) {

                                    templateOptions.options = _.map(res, function(ref) {
                                        return {
                                            name: ref.title,
                                            value: ref._id,
                                        }
                                    })
                                });


                            } else {

                                if (!fieldDefinition.params) {
                                    fieldDefinition.params = {};
                                }
                                //We want to load all the possible references we can select
                                FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                    templateOptions.options = _.map(res, function(ref) {
                                        return {
                                            name: ref.title,
                                            value: ref._id,
                                        }
                                    })
                                });
                            }
                        }
                    } else {
                        //Just list the options specified

                        if (fieldDefinition.options && fieldDefinition.options.length) {
                            templateOptions.options = fieldDefinition.options;
                        } else {
                            templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                                return {
                                    name: val,
                                    value: val
                                }
                            });
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //What kind of data type, override for things like checkbox
                    if (fieldDefinition.directive == 'input') {
                        switch (fieldDefinition.type) {
                            case 'boolean':
                                newField.type = 'checkbox';
                                break;
                                /*
                        case 'number':
                        case 'integer':
                        case 'float':
                            templateOptions.type = 'number';
                        break;
                        */
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Default Options

                    if (fieldDefinition.maximum == 1) {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]; //._id;
                                } else {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]._id;
                                }
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues[0];
                            }
                        }
                    } else {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences;
                                } else {
                                    newField.defaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                        return ref._id;
                                    });
                                }
                            } else {
                                newField.defaultValue = [];
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues;
                            }
                        }
                    }




                    /////////////////////////////

                    //Append the template options
                    newField.templateOptions = templateOptions;

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    newField.validators = {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            var valid = FluroValidate.validate(value, fieldDefinition);
                            //   console.log(scope.name, 'VALID?', value, valid, scope);
                            return valid;
                        }
                    }

                    /////////////////////////////

                    /////////////////////////////

                    //If there are sub items and this is just a group
                    if (fieldDefinition.directive != 'embedded' && fieldDefinition.fields && fieldDefinition.fields.length) {

                        var fieldContainer;

                        if (fieldDefinition.asObject) {
                            newField.type = 'nested';

                            //Check if it's an array or an object
                            newField.defaultValue = [];

                            if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                                newField.defaultValue = {};
                            }

                            newField.data = {
                                fields: [],
                               // className: fieldDefinition.className,
                            };

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                            newField.extras = {
                                skipNgModelAttrsManipulator: true,
                            }

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                        } else {
                            //Start again
                            newField = {
                                fieldGroup: [],
                                className: fieldDefinition.className,
                            }

                            //Link to the sub fields
                            fieldContainer = newField.fieldGroup;
                        }

                        //Loop through each sub field inside a group
                        _.each(fieldDefinition.fields, function(sub) {
                            addFieldDefinition(fieldContainer, sub);
                        });
                    }

                    /////////////////////////////

                    if (fieldDefinition.hideExpression) {
                        newField.hideExpression = fieldDefinition.hideExpression;
                    }

                    /////////////////////////////

                    if (newField.key != '_contact' && newField.type != 'value') {
                        //Push our new field into the array
                        array.push(newField);
                    }
                }

                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////

                if ($scope.definition) {
                    //Loop through each defined field and add it to our form
                    _.each($scope.definition.fields, function(fieldDefinition) {
                        addFieldDefinition($scope.vm.fields, fieldDefinition);
                    });
                }


            });



        },
    };
});
app.controller('AccountListController', function($scope, $rootScope, CacheManager, TypeService, Notifications, Fluro, FluroTokenService, FluroContent) {

    $scope.isActiveAccount = function(id) {

        return true;

        var found = _.find($rootScope.user.availableAccounts, {
            _id: id
        });
        if (found) {
            return true;
        }
    }

    ////////////////////////////////////////////////////////////

    $scope.isMyAccount = function(_id) {
        return ($rootScope.user.account._id == _id);
    }

    ////////////////////////////////////////////////////////////

    $scope.loginToAccount = function(id, asSession) {

        console.log('Login to account', asSession);

        if (asSession) {
            Fluro.sessionStorage = true;

            function success(res) {
                console.log('success now clear caches')
                CacheManager.clearAll();
                TypeService.refreshDefinedTypes();
                Notifications.status('Switched session to ' + res.data.account.title);
            }

            function fail(res) {
                Notifications.error('Error creating token session');
            }
           
            var request = FluroTokenService.getTokenForAccount(id);

            //Callback when the promise is complete
            request.then(success, fail);

        } else {

            // //Update the user
            // FluroContent.resource('user').update({
            //     id: $rootScope.user._id,
            // }, {
            //     account: id
            // }, loginSuccess, updateFailed);


            //Update the user
            FluroContent.endpoint('auth/switch/' + id).save({}).$promise.then(loginSuccess, updateFailed);
        }
    }

    ////////////////////////////////////////////////////////////

    $scope.toggleAccount = function(id) {
        if ($rootScope.user) {

            if (id == $rootScope.user.account._id) {
                Notifications.error('Can not deactivate active account');
                return;
            }

            //Get the current user
            var user = angular.copy($rootScope.user);
            var list = user.availableAccounts;

            var found = _.find(list, {
                _id: id
            })
            if (found) {
                _.pull(list, found);
            } else {
                list.push(id);
            }

            //Update the user
            FluroContent.resource('user').update({
                id: user._id,
            }, {
                availableAccounts: _.uniq(list)
            }, updateSuccess, updateFailed);
        }
    }

    ////////////////////////////////////////////////////////////

    function updateSuccess(data) {
        $rootScope.user = data;
        Notifications.status('Your account list has been updated');
        console.log('Update success', data);
    }

    function loginSuccess(data) {

        FluroTokenService.deleteSession();
        CacheManager.clearAll();
        $rootScope.user = data;
        Notifications.status('Logged in to ' + data.account.title);
    }

    function updateFailed(data) {
        Notifications.error('Failed to update account information');
        console.log('Update failed', data);
    }





});
app.controller('ApplicationFormController', function($scope, $rootScope, FluroAccess, Notifications) {


    $scope.timezones = moment.tz.names();

    /*
    $scope.$watch('item.permissionSets', function() {

        var filtered = _.filter($scope.item.permissionSets, function(set) {
            if (set && set.account) {
                return set.account._id == $rootScope.user.account._id;
            }
        });

        if (!filtered.length) {
            $scope.item.permissionSets = [{
                account: $rootScope.user.account,
                sets: [],
            }]
        }

        $scope.allowedPermissionSets = filtered;

    })
*/

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});
app.controller('AssetFormController', function($scope, Asset, Fluro, $state, $stateParams, Notifications, FileUploader) {


    if($scope.type.path == 'image') {
        if(!$scope.item.width) {
            $scope.needsWidth = true;
        }

        if(!$scope.item.height) {
            $scope.needsHeight = true;
        }
    }

    /////////////////////////////////

    $scope.uploader = new FileUploader({
        url: Fluro.apiURL + '/file/replace',
        //removeAfterUpload: true,
        withCredentials: true,
        formData: {},
    });


    var forceRefresh = false;

    $scope.saveAndUpload = function() {
        console.log('Save and Upload');

        if (!$scope.item || !$scope.item.realms || !$scope.item.realms.length) {
            return Notifications.warning('Please select a realm before attempting to upload');
        }

        $scope.uploader.uploadAll();

    }

    
    if (!$scope.item.assetType && $scope.type.path != 'video' && $scope.type.path != 'audio') {
        $scope.item.assetType = 'upload';
    }
    

    /////////////////////////////////

    function refreshAfterUpload(response) {
        if (response._id == $scope.item._id) {
            $scope.item = response;
        }
    }

    $scope.getImageURL = function() {
        return Asset.getUrl($scope.item._id) + '?w=600&v=' + $scope.item.updated;// + '&forceRefresh=true';
        
    }

    //////////////////////////////////////////////////

    $scope.$watch('item._id + item.assetType', function() {
        if (!$scope.item._id && $scope.item.assetType == 'upload') {
            $scope.uploadSave = true;
        } else {
            $scope.uploadSave = false;
        }
    })

    /////////////////////////////////

    $scope.uploader.onBeforeUploadItem = function(item) {

        // item.removeAfterUpload = false;
        var details = {};

        if ($scope.item._id) {
            details._id = $scope.item._id;
        } else {

            //Add the definition before we send the data
            if ($scope.definition) {
                $scope.item.definition = $scope.definition.definitionName;
            }

            $scope.item.type = $scope.type.path;
            //console.log('Create Item', $scope.type, $scope.item.type);
            details.json = angular.toJson($scope.item);
        }

        console.log('Send details', details)
        item.formData = [details];
    };




    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {

        _.each(response.errors, function(err) {
            if (err.message) {
                Notifications.error(err.message);
            } else {
                Notifications.error(err);
            }
        })

        //Reset File Item and add it back into the queue
        fileItem.isError = false;
        fileItem.isUploaded = false;
        $scope.uploader.queue.push(fileItem);
    };

    /////////////////////////////////

    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        switch (status) {
            case 200:
                if ($scope.item._id) {
                    $scope.saveSuccess(response);
                    refreshAfterUpload(response);
                } else {
                    $scope.saveSuccessExit(response);
                }
                break;
            default:
                //Failed
                $scope.saveFail(response);
                break;
        }
    };


});
app.directive('assetReplaceForm', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        templateUrl: 'fluro-admin-content/types/asset/asset-replace-form.html',
        controller: 'AssetReplaceController',
    };
});


app.controller('AssetReplaceController', function($scope, Fluro) {


    /////////////////////////////////

/*
    if (!$scope.item._id) {
        $scope.replaceDialogShowing = true;
    }
*/

    /////////////////////////////////

    $scope.showReplaceDialog = function() {
        $scope.replaceDialogShowing = true;
    }

    /////////////////////////////////

});
app.controller('CheckinFormController', function($scope, $http, Fluro, Notifications) {

	$scope.$watch('item.contact + item.title + item.firstName + item.lastName', function() {
		
		//Watch to see if the contact exists
		if($scope.item.contact) {
			//if(!$scope.item.title || !$scope.item.title.length) {
				$scope.item.title = $scope.item.contact.title;
			//}
		} else {

			var title = '';
			if($scope.item.firstName && $scope.item.firstName.length) {
				title += $scope.item.firstName;
			}

			if($scope.item.lastName && $scope.item.lastName.length) {
				title += ' ' + $scope.item.lastName;
			}

			$scope.item.title = title;
		}

	})
});

app.controller('CodeFormController', function($scope) {
       
       var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
        updateSyntax();
    };

    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    $scope.$watch('item.syntax', updateSyntax)

    $scope.aceChanged = function(e) {
        //
    };

});

app.controller('ComponentFormController', function($scope) {
       


      
    $scope.scssDisabled = false;
    $scope.htmlDisabled = false;
    $scope.jsDisabled = false;


       /*
       var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
       // updateSyntax();
    };

    /*
    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    $scope.$watch('item.syntax', updateSyntax)
    

    $scope.aceChanged = function(e) {
        //
    };
    */

});




app.controller('ContactFormController', function($scope, FluroContent, FluroAccess, TypeService, ModalService) {


    if(!$scope.details) {
        $scope.details = [];
    }
    
    /////////////////////////////////////

    $scope.dateOptions = {
        formatYear: 'yy',
        //startingDay: 1,
        showWeeks:false,
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

/*
    //Get all the defined types
    $scope.availableDetails = _.filter(TypeService.definedTypes, function(def) {
        
    });
*/

    /////////////////////////////////////

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.getCreateableDetails = function() {
        return _.filter(TypeService.definedTypes, function(def) {
            var correctType = def.parentType == 'contactdetail';
            var exists = _.some($scope.details, function(existing) {
                return existing.definition == def.definitionName;
            })

            return (correctType && !exists);
        });
    }

    /////////////////////////////////////

    $scope.createDetailSheet = function(sheet) {

        var options = {};
        options.template = {
            contact:$scope.item,
        }

        ModalService.create(sheet.definitionName, options, function(result) {
            $scope.details.push(result);
        })
        
    }

    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////


    if($scope.item._id) {
        FluroContent.endpoint('info/checkins').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {
            $scope.checkins = res;
        });


        FluroContent.endpoint('info/interactions').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {
            $scope.interactions = res;
        });


        FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
        .then(function(res) {
            $scope.details = res;
        });

        FluroContent.endpoint('info/assignments').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {

            var today = new Date();

            $scope.pastassignments = _.filter(res, function(event) {
                return (new Date(event.startDate) < today);
            });

            $scope.upcomingassignments = _.filter(res, function(event) {
                return (new Date(event.startDate) >= today);
            });

        });
    }
   
})
app.controller('ContactViewController', function($scope, FluroContent, FluroAccess, TypeService, ModalService) {

    FluroContent.endpoint('info/checkins').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.checkins = res;
    });


    FluroContent.endpoint('info/interactions').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.interactions = res;
    });


    FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
        .then(function(res) {
            $scope.details = res;
        });

    FluroContent.endpoint('info/assignments').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {

        var today = new Date();

        $scope.pastassignments = _.filter(res, function(event) {
            return (new Date(event.startDate) < today);
        });

        $scope.upcomingassignments = _.filter(res, function(event) {
            return (new Date(event.startDate) >= today);
        });

    });


    $scope.viewDetail = function(sheet) {
        ModalService.view(sheet);
    }


})
app.controller('ContactDetailFormController', function($scope, $filter) {

	$scope.$watch('item.contact + definition', function() {

        console.log('Test', $scope.definition);
        if(!$scope.item.contact) {
            return;
        }

        if(!$scope.definition) {
            return;
        }


        var name = $filter('capitalizename')($scope.item.contact.title);

        //Update the title
        var string = name + ' - ' + $scope.definition.title;
        $scope.item.title = string;
       
    })

})
app.controller('DefinitionFormController', function($scope) {

    if (!$scope.item.definitionName) {
        $scope.$watch('item.title', function(newValue) {
            if (newValue) {
                $scope.item.definitionName = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    $scope.$watch('item.definitionName', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.item.definitionName = $scope.item.definitionName.replace(regexp, '');
        }
    })

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    function getFlattenedFields(array, trail) {

        return _.chain(array).map(function(field, key) {
                if (field.type == 'group') {

                    if (field.asObject) {
                        //Push the field key
                        trail.push(field.key);
                    }

                    // console.log('Go down', field.key);
                    var fields = getFlattenedFields(field.fields, trail);

                    trail.pop();
                    //console.log('Go back up')
                    return fields;
                } else {

                    //Push the field key
                    trail.push(field.key);

                    field.trail = trail.slice();
                    trail.pop();

                    // console.log(field.title, trail);
                    return field;
                }
            })
            .flatten()
            .value();
    }

   

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    $scope.$watch('item.fields', function(fields) {

         //Get all the flattened fields
        var flattened = getFlattenedFields($scope.item.fields, []);


        $scope.availableFields = _.map(flattened, function(field) {
            return {
                title:field.title,
                path:field.trail.join('.'),
                type:field.type,
            }
        });
    })

    /////////////////////////////////////////////////

    $scope.addAlternativePaymentMethod = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.paymentMethods) {
            $scope.item.paymentDetails.paymentMethods = [];
        }

        $scope.item.paymentDetails.paymentMethods.push({
            title: 'New Payment Method'
        })
    }


    $scope.removeAlternativePaymentMethod = function(method) {
        _.pull($scope.item.paymentDetails.paymentMethods, method);
    }


    /////////////////////////////////////////////////

    $scope.addPaymentModifier = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.modifiers) {
            $scope.item.paymentDetails.modifiers = [];
        }

        $scope.item.paymentDetails.modifiers.push({})
    }


    $scope.removeModifier = function(entry) {
        _.pull($scope.item.paymentDetails.modifiers, entry);
    }

})
app.controller('EndpointFormController', function($scope, PermissionService, GenericPermissionService) {
    $scope.permissions = PermissionService.permissions;
    $scope.genericPermissions = GenericPermissionService.getPermissions();
});

app.controller('EventFormController', function($scope, $stateParams, ModalService, DateTools) {

    /*
	//////////////////////////////////////////

    //Check if the user provided an init date upon creation
    //var initDate = $stateParams.initDate;
    if (!$scope.item._id) {
        if ($scope.definition && $scope.definition.data) {
            if ($scope.definition.data.defaultAssignments) {
                $scope.item.assignments = angular.copy($scope.definition.data.defaultAssignments);
            }
        }
    }

    //////////////////////////////////////////

    */
   
    
    //////////////////////////////////////////

   $scope.confirmations = $scope.extras.confirmations;

    //////////////////////////////////////////

   var currentDate1;
   var currentDate2;
   
    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }


    if (currentDate1 && currentDate2 && (currentDate1.getTime() != currentDate2.getTime())) {
        $scope.useEndDate = true;
    }


    //Make sure the end date is never earlier than the start date
    $scope.$watch('item.startDate + useEndDate', function(data) {

        //Compare the dates
        var date1 = new Date($scope.item.startDate);
        var date2 = new Date($scope.item.endDate);

        //If the user does not want to use an end date set it the same as the start date
        if (!$scope.useEndDate) {
            $scope.item.endDate = $scope.item.startDate;
        }

        //Update the end time if its earlier than the start time
        if (date2.getTime() < date1.getTime()) {
            $scope.item.endDate = date1
        }

    });

    //////////////////////////////////////////
    //////////////////////////////////////////

    $scope.$watch('item.startDate +  item.endDate + item.checkinData.checkinStartOffset + item.checkinData.checkinEndOffset', function() {

        var startDate = $scope.item.startDate;
        var endDate = $scope.item.endDate;

        if(!$scope.item.checkinData) {
            $scope.item.checkinData = {
                checkinStartOffset:0,
                checkinEndOffset:0,
            }
        }

        $scope.checkinStartDate = moment(startDate).subtract($scope.item.checkinData.checkinStartOffset, 'minutes');
        $scope.checkinEndDate = moment(endDate).add($scope.item.checkinData.checkinEndOffset, 'minutes');
    })
    //////////////////////////////////////////


    /*

    //////////////////////////////////////////

    var today = new Date();

    var currentDate1 = new Date(today.getTime());
    var currentDate2 = new Date(today.getTime());

    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }

    if (currentDate1.getTime() != currentDate2.getTime()) {
        $scope.useEndDate = true;
    }


	//////////////////////////////////////////



    var initDate;

    //If there are modal params
    if ($stateParams.initDate) {
        initDate = $stateParams.initDate;
    }

    if (initDate) {
       
        if (_.isString(initDate)) {
            initDate = new Date(parseInt(initDate)); //Number(initDate);
        }

        var initStart = new Date(initDate);
        var initEnd = new Date(initDate);
        initStart.setMinutes(initStart.getMinutes() + 30);
        initStart.setMinutes(0);

        initEnd.setMinutes(initEnd.getMinutes() + 30);
        initEnd.setMinutes(0);

        $scope.item.startDate = initStart;
        $scope.item.endDate = initEnd;
    }

    //////////////////////////////////////////

    //Include the readable date range function
    $scope.readableDateRange = DateTools.readableDateRange;

/**/


    $scope.createPlanFromTemplate = function() {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addPlan,
            addPlan)

        function addPlan() {
           // console.log('Select Plans', $scope.item);
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!$scope.item.plans || !$scope.item.plans.length) {
                    $scope.item.plans = [];
                }

                //Get the template
                var template = selection.items[0];

               // console.log('Add Plan from template', template)


                  
                ModalService.edit(template, function(copyResult) {
                    if (copyResult) {
                        $scope.item.plans.push(copyResult)
                    }

                    //console.log('After', $scope.item)

                }, function(res) {
                    //console.log('Failed to copy', res)
                }, true, true, $scope);

            }
        }

    }

    /*
    //////////////////////////////////////////////////


    $scope.saveFinished = function(result) {

        if (!$scope.item._id) {
            if (result._id) {

                _.assign($scope.item, result);

                //$scope.item._id = result._id;

                $scope.saveText = 'Save';
                $scope.cancelText = 'Close';
            }

            $scope.closeOnSave = true;
            return true;
        } else {

            console.log('Close like normal')
            if ($scope.$close) {
                return $scope.$close(result)
            } else {
                return false;
            }
        }
    }

    /*
    //Save
    $scope.save = function(options) {

        //TODO Disable Socket     

        if (!options) {
            options = {}
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        //////////////////////////////////////////

        if ($scope.item._id) {
            //Edit existing user
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);
        } else {

            console.log('SAVE SUCCESS STAY');
            //Saving a new event so stay and dont disappear
            FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessStay, $scope.saveFail);
        }


    }


    //////////////////////////////////////////////////

    $scope.saveSuccessStay = function(result) {
        console.log('Stay please')
        //Clear cache
        CacheManager.clear($scope.definedType);

        if (result._id) {
            $scope.item.__v = result.__v;
            $scope.item._id = result._id;
        }


        //TODO Reenable Socket
        Notifications.status(result.title + ' saved successfully')
        
        return false;

       
        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            console.log('Return false modal')
            return false;
        }
        
    }

    */

    //////////////////////////////////////////


})
app.controller('EventListController', function($scope, $sce, $filter, ModalService, FluroStorage, DateTools) {

    //Setup Local storage
    var local;
    var session;

    if ($scope.definition) {
        local = FluroStorage.localStorage($scope.definition.definitionName)
        session = FluroStorage.sessionStorage($scope.definition.definitionName)
    } else {
        local = FluroStorage.localStorage($scope.type.path)
        session = FluroStorage.sessionStorage($scope.type.path)
    }

    //Today
    var today = new Date();

    $scope.changeDate = function() {
        $scope.updateFilters();
    }

    ///////////////////////////////////////////////

    $scope.getMarker = function(date, mode) {


        if (mode === 'day') {

            //Get the timestamp of our check date
            var checkTimestamp = new Date(date);
            checkTimestamp.setHours(0, 0, 0, 0);
            checkTimestamp = checkTimestamp.getTime();

            //Search through all items to see if they match any dates
            var match = _.some($scope.items, function(event) {

                var startDate;
                var endDate;

                //Get the start of the day
                if (event.startDate) {
                    startDate = new Date(event.startDate);
                    startDate.setHours(0, 0, 0, 0);
                }

                //Get the start of the day
                if (event.endDate) {
                    endDate = new Date(event.endDate);
                }

                //Get the end of the day
                endDate.setHours(23, 59, 59, 999);

                //Turn into integers
                var checkTimestamp = date.getTime();
                var startTimestamp = startDate.getTime();
                var endTimestamp = endDate.getTime();
               
                if(checkTimestamp >= startTimestamp && checkTimestamp <= endTimestamp) {
                    return true;//
                }
            })

            if(match) {
                return 'has-event';
            }
        }

        return '';

    };

    ///////////////////////////////////////////////

    $scope.resetToday = function() {
        session.search.browseDate = new Date();
    }

    //Set to today if not set previously
    if (!session.search.browseDate) {
        $scope.resetToday();
    }

    //Set to today if not set previously
    if (!session.search.order) {
        session.search.order = 'startDate';

    }


    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.getConfirmed = function(item) {
        return $scope.getAssigned(item, 'confirmed');
    }

    $scope.getDenied = function(item) {
        return $scope.getAssigned(item, 'denied');
    }

    $scope.getAssignedContacts = function(item) {
        var results = [];

        _.each(item.assignments, function(ass) {
            _.each(ass.contacts, function(contact) {
                results.push({
                    assignment: ass._id,
                    contact: contact._id,
                })
            });
        });

        return results;
    }

    $scope.getUnknown = function(item) {

        var contacts = $scope.getAssignedContacts(item);
        var results = [];

        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {

            //Get the confirmation list
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });

            if (eventConfirmationObject && eventConfirmationObject.confirmations) {

                //Get the confirmations
                var confirmations = eventConfirmationObject.confirmations;

                results = _.filter(contacts, function(contactAss) {
                    return !_.some(confirmations, function(conf) {
                        return conf.contact._id == contactAss.contact;
                    });
                });

            }
        }

        return results;

    }

    ///////////////////////////////////////////////

    $scope.getAssigned = function(item, status) {
        var results = [];

        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {
            //Get the confirmation list
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });

            if (eventConfirmationObject && eventConfirmationObject.confirmations) {
                if (status) {
                    results = _.filter(eventConfirmationObject.confirmations, function(conf) {
                        var statusMatch = (conf.status == status);

                        if (!statusMatch) {
                            return false;
                        }

                        var assignment = _.find(item.assignments, function(ass) {
                            var matchId = (ass._id == conf.assignment || ass.title == conf.title);
                            return matchId;
                        });

                        if (!assignment) {
                            return false;
                        }

                        return _.some(assignment.contacts, {
                            _id: conf.contact._id
                        });


                    });
                } else {
                    results = eventConfirmationObject.confirmations;
                }
            }
        }

        return results;
    }

    ///////////////////////////////////////////////

    $scope.getAssignmentTitle = function(item, assignmentId) {
        var ass = _.find(item.assignments, {
            _id: assignmentId
        });
        if (ass) {
            return ass.title;
        }
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    $scope.pagerEnabled = function() {

        var morePages = ($scope.filteredItems && $scope.filteredItems.length && ($scope.filteredItems.length > $scope.pager.limit));

        return morePages && ($scope.settings.viewMode != 'calendar' && $scope.settings.viewMode != 'cards');
    }

    ///////////////////////////////////////////////

    $scope.planTooltip = function(item) {

        var string = '';

        _.each(item.plans, function(plan) {
            string += plan.title + '<br/>';
        })


        string += '';

        return string;

    }

    $scope.viewPlans = function(event) {

        if (event.plans.length == 1) {
            $scope.viewInModal(event.plans[0])
        } else {
            $scope.viewInModal(event);
        }
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////

    $scope.assignmentTooltip = function(item) {

        var string = '<div class="text-left tooltip-block"><div class="row">';

        _.each(item.assignments, function(assignment) {
            if (assignment.contacts.length) {
                string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + '</strong><br/>';

                _.each(assignment.contacts, function(contact) {
                    string += $filter('capitalizename')(contact.title) + '<br/>';
                });

                string += '</div>';
            }
        })
        /*
        if(item.assignments.length) {
            return 'By ' + item.updatedBy + ' ' + $filter('timeago')(item.updated);
        } else {
            return $filter('timeago')(item.updated);
        }
        */

        string += '</div></div>';

        return string;

        //return $sce.trustAsHtml('<span ng-if="item.updatedBy">By ' +item.updatedBy+ '</span> {{item.updated | timeago}}');
    }

    ///////////////////////////////////////////////

    $scope.assignedTooltip = function(item) {
        /*
        //Get all the confirmed people
        
        var string = '<div class="text-left tooltip-block">';

        _.each(confirmations, function(confirmation) {
            //var assignmentTitle = $scope.getAssignmentTitle(item, confirmation.assignment);
            string += $filter('capitalizename')(confirmation.contact.title) + '<br/>';
        });
        string += '</div>';
        return string;
/**/

        //Get all the confirmations we know about
        var confirmations = $scope.getAssigned(item);

        //Create the HTML String
        var string = '<div class="text-left tooltip-block"><div class="row">';

        //Loop through each assignment
        _.each(item.assignments, function(assignment) {

            if (assignment.contacts.length) {

                string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + '</strong><br/>';

                _.each(assignment.contacts, function(contact) {

                    var findConfirmation = _.find(confirmations, function(conf) {
                        var correctContact = (conf.contact._id == contact._id);
                        var correctAssignment = (conf.assignment == assignment._id || conf.title == assignment.title);
                        return (correctContact && correctAssignment);
                    })
                    

                    var contactName = $filter('capitalizename')(contact.title);

                    if (findConfirmation) {
                        switch (findConfirmation.status) {
                            case 'confirmed':
                                string += ('<span class="brand-success"><i class="fa success fa-check"></i> ' + contactName + '</span><br/>');
                                break;
                            case 'denied':
                                string += ('<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + contactName + '</span><br/>');
                                break;
                            default:
                                string += ('<span><i class="text-muted fa warning fa-question"></i> ' + contactName + '</span><br/>');
                                break;
                        }

                    } else {
                        string += ('<span><i class="text-muted fa warning fa-question"></i> ' + contactName + '</span><br/>');
                    }

                });

                string += '</div>';
            }
        })


        //Loop through each assignment
        _.each(item.volunteers, function(slot) {

            //if (slot.contacts.length) {
                string += '<div class="col-xs-6 inline-column"><strong>' + slot.title + '</strong><br/>';

                if(slot.contacts && slot.contacts.length) {
                    _.each(slot.contacts, function(contact) {
                        // var contactName = $filter('capitalizename')(contact.title);
                        string += ('<span class="brand-success text-capitalize"><i class="fa success fa-check"></i> ' + contact.title + '</span><br/>');
                    });

                    if(slot.minimum && slot.contacts.length < slot.minimum){
                        string += ('<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + (slot.minimum - slot.contacts.length) + ' more required</span><br/>');
                    }
                } else {
                    if(slot.minimum){
                        string += ('<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + slot.minimum + ' required</span><br/>');
                    }
                }

                string += '</div>';
            //} 
        })

        string += '</div></div>';

        return string;




    }


    // function getVolunteerNumbers(slot) {
    //     if(!slot.minimum) {
    //         return '';
    //     }

    //     if(!slot.contacts || !slot.contacts.length) {
    //         return '<span>'+slot.minimum+' required</span>'
    //     }

    //     if(slot.contacts.length >= slot.minimum) {
    //         return '<i></i>';
    //     }

    //     var num = slot.minimum - slot.contacts.length;
    //     if(num > 0) {
    //         return '<span>('+slot.contacts.length +'/'+ slot.minimum+')</span>';
    //     } else {

    //     }

        
        
    // }


    ///////////////////////////////////////////////

    $scope.stringBrowseDate = function() {

        var itemDate = new Date(session.search.browseDate);
        itemDate.setHours(0);
        itemDate.setMinutes(0);
        itemDate.setSeconds(0);
        itemDate.setMilliseconds(0);
        return itemDate.getTime();




    }


    $scope.canCreatePlans = function() {
        return FluroAccess.can('create', 'plan');
    }


    ///////////////////////////////////////////////

    $scope.createPlanFromTemplate = function(event) {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addTemplatePlan,
            addTemplatePlan)

        function addTemplatePlan() {
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!event.plans || !event.plans.length) {
                    event.plans = [];
                }

                //Get the template
                var template = selection.items[0];

                //Link it to this event
                template.event = event;




                console.log('Add Plan from template', template)
                ModalService.edit(template, function(copyResult) {
                    if (copyResult) {
                        event.plans.push(copyResult)
                    }
                    //console.log('After', $scope.item)
                }, function(res) {
                    //console.log('Failed to copy', res)
                }, true, true, $scope);
            }
        }

    }

    ///////////////////////////////////////////////

    $scope.addPlan = function(event) {

        ModalService.create('plan', {
            template: {
                event: event,
                realms: event.realms,
                startDate: event.startDate,
            }
        }, function(result) {
            if (result.definition) {
                CacheManager.clear(result.definition);
            } else {
                CacheManager.clear(result._type);
            }

            $state.reload();

        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////

    if (!session.search.calendarMode) {
        session.search.calendarMode = 'month';
    }

    $scope.calendarOpen = function(item) {
        $scope.viewInModal(item.event)
    }


    $scope.calendar = {}

    ///////////////////////////////////////////////


    $scope.$watch('filteredItems', function(data) {

        $scope.dateGroups = _.groupBy(data, function(item) {
            var itemDate = new Date(item.startDate);
            itemDate.setHours(0);
            itemDate.setMinutes(0);
            itemDate.setSeconds(0);
            itemDate.setMilliseconds(0);
            return itemDate.getTime();
        });


        $scope.calendar.events = _.map(data, function(item) {
            return {
                event: item,
                title: item.title, // The title of the event
                type: 'primary', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                startsAt: new Date(item.startDate), // A javascript date object for when the event starts
                endsAt: new Date(item.endDate), // Optional - a javascript date object for when the event ends
                editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable. 
                //If set to false will also prevent the event from being dragged and dropped.
                deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                //recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
                //cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
            }
        })
    }, true);

    ///////////////////////////////////////////////

    $scope.readableDate = DateTools.readableDateRange;

    $scope.setShowAll = function() {
        session.search.restrictToDate = false;
        $scope.updateFilters();
    }

    $scope.setShowDate = function() {
        session.search.restrictToDate = true;
        $scope.updateFilters();
    }


    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////

            if (session.search.browseDate) {
                // console.log('Have browse date')
                var browseType = 'upcoming';
                if (session.search.restrictToDate) {
                    browseType = 'specific';
                }

                // console.log('Filter all', browseType);
                filteredItems = $filter('matchDate')(filteredItems, session.search.browseDate, browseType);
            }

            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;

            //Update the current page
            $scope.updatePage();

        }
    }


});
app.controller('EventViewController', function($scope, ModalService) {



    $scope.viewEvent = function() {
        ModalService.view(item.event);
    }

    /////////////////////////////////////////////

    $scope.contexts = [];

    /////////////////////////////////////////////

   
    if($scope.extras) {
        $scope.confirmations = $scope.extras.confirmations;
        $scope.unavailableList = _.filter($scope.confirmations, function(con) {
            return con.status == 'denied';
        })

        $scope.confirmedList = _.filter($scope.confirmations, function(con) {
            return con.status == 'confirmed';
        })


    }
    /////////////////////////////////////////////

    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {

        
        return _.contains($scope.contexts, val);
    }

    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })

           
            var laterTime = new Date(startDate);



            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }


})


app.controller('FamilyFormController', function($scope, ModalService) {

    console.log('FAMILY CONTROLLER')

    //$scope.openContact = ModalService.edit(item);


    $scope.addContact = function() {

        console.log('Add Contact', $scope.item);

        if ($scope.item && $scope.item._id) {
            var params = {}
            params.template = {
                family: $scope.item,
                lastName:$scope.item.title,
            }

            ModalService.create('contact', params, function(res) {
                $scope.item.items.push(res);
            })

        }

    }
});
app.controller('IntegrationFormController', function($scope, $http, Fluro, Notifications) {



	$scope.authorizeInstagram = function() {


		if(!$scope.item._id || !$scope.item._id.length) {
			Notifications.warning('Please save before attempting to authorize this integration')
		}

		var clientID = $scope.item.publicDetails.clientID;

		var redirectURI = Fluro.apiURL + '/integrate/instagram/' + $scope.item._id;

		if(Fluro.token) {
			redirectURI+= '?access_token=' +Fluro.token;
		}



		if(!clientID || !clientID.length) {
			Notifications.warning('Invalid or missing client ID')
		}

		if(!redirectURI || !redirectURI.length) {
			Notifications.warning('Invalid or missing Redirect URI')
		}

		var url = 'https://api.instagram.com/oauth/authorize/?client_id='+clientID+'&redirect_uri='+redirectURI+ '&response_type=code'

		if(Fluro.token) {
			url+= '&access_token=' +Fluro.token;
		}

		console.log('authorize', url);
		window.open(url);


	}


});

app.controller('InteractionFormController', function($scope) {





	//////////////////////////////////////

	$scope.amountPaid = function() {
		var totalPaid = 0;

		if($scope.item.transactions && $scope.item.transactions.length) {
			totalPaid += _.chain($scope.item.transactions)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		if($scope.item.manualPayments && $scope.item.manualPayments.length) {
			totalPaid += _.chain($scope.item.manualPayments)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		return totalPaid;
	}

	//////////////////////////////////////

	$scope.amountDue = function() {

		var originalAmount = $scope.item.amount;
		var amountPaid = $scope.amountPaid();
		
		return originalAmount - amountPaid;
		
	}

});
app.controller('LocationFormController', function($scope) {

    console.log('TESTING', $scope.item);
    $scope.itemAsArray = [$scope.item];

    /*
    var map;


    $scope.$on('mapInitialized', function(event, evtMap) {


        






        /*
        console.log('WOOT')
        map = evtMap;


        $scope.addMarker = function(event) {
            var ll = event.latLng;
            $scope.positions.push({
                lat: ll.lat(),
                lng: ll.lng()
            });
        }

        $scope.deleteMarkers = function() {
            $scope.positions = [];
        };

        $scope.mapClick = function(e) {



            map.markers = {};

            var marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });


            map.panTo(e.latLng);
        }
        

});
*/
});
app.controller('PersonaFormController', function($scope, Session, $state, Fluro, FluroContent, $http, $rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {


    $scope.resendState = 'ready';


    $scope.sendResetEmail = function() {

        $scope.resendState = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;


        if ($scope.item.user) {
            email = $scope.item.user.email;
        }

        /////////////////////////////////////////////////

        // var request = $http.post(Fluro.apiURL + '/user/reinvite', {
        //     email: email
        // });

        var request = $http.post(Fluro.apiURL + '/user/reinvite/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.error(function(err) {
            console.log('ERROR', err);

            $scope.resendState = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending password instructions');
            }
        });

        /////////////////////////////////////////////////

        request.success(function(result) {
            console.log('Send', result);
            $scope.resendState = 'ready';
            Notifications.status('Instructions on how to reset password have been sent to ' + email);
        });




    }


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});
app.controller('PersonaListController', function($scope, $rootScope, $state, Fluro, FluroTokenService, CacheManager, Notifications, TypeService) {



    $scope.canImpersonate = function(item) {
        if(!item) {
            return $rootScope.user.accountType == 'administrator';
        } else {
            return $rootScope.user.accountType == 'administrator' && !item.managed;
        }

    }

    ////////////////////////////////////////////////////////////

    $scope.signInAsPersona = function(personaID) {

        Fluro.sessionStorage = true;
        console.log('Sign in as persona', personaID);


        var request = FluroTokenService.signInAsPersona(personaID);

        function signInSuccess(res) {
            console.log('Signed in as persona', res)
            CacheManager.clearAll();
            TypeService.refreshDefinedTypes();
            Notifications.status('Switched this session to ' + res.data.account.title);

            $state.reload();
        }

        function signInFail(res) {
            console.log('Error signing in as persona', res)
        }

        //Listen for the promise resolution
        request.then(signInSuccess, signInFail);

        /**
        if (asSession) {
            Fluro.sessionStorage = true;

            function success(res) {
                 CacheManager.clearAll();
                 TypeService.refreshDefinedTypes();
                Notifications.status('Switched this session to ' + res.account.title);
            }

            function fail(res) {
                Notifications.error('Error creating token session');
            }
           
            FluroTokenService.getTokenForAccount(id, success, fail);
        } else {

            // //Update the user
            // FluroContent.resource('user').update({
            //     id: $rootScope.user._id,
            // }, {
            //     account: id
            // }, loginSuccess, updateFailed);





            //Update the user
            FluroContent.endpoint('auth/switch/' + id).save({}).$promise.then(loginSuccess, updateFailed);
        }
        /**/
    }



});
app.controller('PlanFormController', function($scope, $stateParams, DateTools) {
/*
    //////////////////////////////////////////




	//////////////////////////////////////////
*/

})
app.directive('adminPlanView', function(ModalService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=ngModel',
            event: '=ngEvent',
            confirmations:'=ngConfirmations',
        },
        templateUrl: 'fluro-admin-content/types/plan/view.html',
        link:function($scope, $element, $attributes) {
            $scope.canEditPlan = true;

            $scope.editPlan = function(plan) {
                console.log('Plan', plan)
                ModalService.edit($scope.item);
            }
        }


        //controller: 'PlanViewController',
    };
});



app.controller('AdminPlanViewController', function($scope) {

    if($scope.item.event) {
        $scope.event = $scope.item.event;
    }

    $scope.item.showTeamOnSide = true;
    
    /////////////////////////////////////////////

    $scope.contexts = []


    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {


        return _.contains($scope.contexts, val);
    }


    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date(startDate);

            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }



    $scope.isBlankColumn = function(team) {


        var hasContent = _.chain($scope.item.schedules)
            .any(function(schedule) {
                if(!schedule) {
                    return false;
                }

                if (!schedule.notes) {
                    return false;
                }
                if (schedule.notes[team] && schedule.notes[team].length) {
                    return true;
                } else {
                    return false;
                }
            })
            .value();

        return !hasContent;
    }
});
app.controller('PolicyFormController', function($scope) {



    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});
app.controller('PurchaseFormController', function($scope, $http, Fluro, Notifications) {


    //Save the original email
    $scope.originalEmail = String($scope.item.collectionEmail);



	$scope.$watch('item.product', function(product) {
		$scope.item.title = product.title;
	})


     $scope.originalEmail = String($scope.item.collectionEmail);


     

	//////////////////////////////////////////////

	$scope.resendStatus = 'ready';


	$scope.resendPurchaseInvitation = function() {


		if(!$scope.item || !$scope.item._id) {
			return;
		}

		if(!$scope.item.collectionEmail || !$scope.item.collectionEmail.length) {
			return;
		}



        $scope.resendStatus = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;

        /////////////////////////////////////////////////

        var request = $http.post(Fluro.apiURL + '/purchase/resend/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.error(function(err) {
            $scope.resendStatus = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending purchase notification');
            }
        });

        /////////////////////////////////////////////////

        request.success(function(result) {
            $scope.resendStatus = 'ready';
            Notifications.status('Purchase notification sent ' + email);
        });




    }




});
app.directive('json', function() {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModelCtrl) {

            var lastValid;

            // push() if faster than unshift(), and avail. in IE8 and earlier (unshift isn't)
            ngModelCtrl.$parsers.push(fromUser);
            ngModelCtrl.$formatters.push(toUser);

            // clear any invalid changes on blur
            element.bind('blur', function() {
                element.val(toUser(scope.$eval(attrs.ngModel)));
            });

            // $watch(attrs.ngModel) wouldn't work if this directive created a new scope;
            // see http://stackoverflow.com/questions/14693052/watch-ngmodel-from-inside-directive-using-isolate-scope how to do it then
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                lastValid = lastValid || newValue;

                if (newValue != oldValue) {
                    ngModelCtrl.$setViewValue(toUser(newValue));

                    // TODO avoid this causing the focus of the input to be lost..
                    ngModelCtrl.$render();
                }
            }, true); // MUST use objectEquality (true) here, for some reason..

            function fromUser(text) {
                // Beware: trim() is not available in old browsers
                if (!text || text.trim() === '') {
                    return {};
                } else {
                    try {
                        lastValid = angular.fromJson(text);
                        ngModelCtrl.$setValidity('invalidJson', true);
                    } catch (e) {
                        ngModelCtrl.$setValidity('invalidJson', false);
                    }
                    return lastValid;
                }
            }

            function toUser(object) {
                // better than JSON.stringify(), because it formats + filters $$hashKey etc.
                return angular.toJson(object, true);
            }
        }
    };
});




app.controller('QueryFormController', function($scope, Fluro) {
    if($scope.item._id) {
        $scope.previewLink = Fluro.apiURL + '/content/_query/' + $scope.item._id + '?noCache=true';
    }
    /*
    if (!$scope.item.data) {
        $scope.item.data = {}
    }

    $scope.getObjectAsText = function() {

        if ($scope.item.data) {
            $scope.textAreaModel = JSON.stringify($scope.item.data);
        } else {
            $scope.textAreaModel = '{}';
        }
    };


    $scope.$watch('textAreaModel', function(n) {

        if (n) {
            try {
                $scope.item.data = JSON.parse($scope.textAreaModel);
            } catch (err) {
                //Exception handler
                console.log('Exception', err)
            }
        }
    });

    /*
    var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
        updateSyntax();
    };

    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    $scope.$watch('item.syntax', updateSyntax)

    $scope.aceChanged = function(e) {
        //
    };
    */

});
app.controller('RoleFormController', function($scope, PermissionService, GenericPermissionService) {
        


        $scope.permissions = PermissionService.permissions;



        $scope.genericPermissions = GenericPermissionService.getPermissions();

});

app.controller('SiteFormController', function($scope, $http, FluroAccess, ObjectSelection, ContentVersionService) {


/**
    if (!$scope.item.routes) {
        $scope.item.routes = [];
    }

    $scope.selected = {}


    if (!$scope.item.menus) {
        $scope.item.menus = [];
    }

    $scope.addRoute = function() {
        $scope.item.routes.push({})
    }

    $scope.removeRoute = function(route) {
        console.log('Remove route', route)
        _.remove($scope.item.routes, route);
    }


    $scope.addMenu = function() {
        $scope.item.menus.push({title:'Menu'})
    }

    $scope.removeMenu = function() {
        _.remove($scope.item.menus, $scope.selected.menu);
        $scope.selected.menu = null;

    }

    $scope.context = {
        showAll: true
    }

    $scope.isActive = function(route) {
        return $scope.context.activeRoute == route;
    }

    /**/




    $scope.selection = new ObjectSelection();
    $scope.selection.multiple = false;


    $scope.$watch('item.routes', function(routes) {
        $scope.flattenedRoutes = getFlattenedRoutes(routes);
    })


    $scope.$watch('item._id', function(id) {
        if(id) {
            $scope.versions = ContentVersionService.list($scope.item);
        }
    })


    //////////////////////////////////

    $scope.hasSlug = function(route) {
        return _.contains(route.url, ':slug');
    }


    $scope.defaultSaveOptions.forceVersioning = true;


    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }


    /*
    if (!$scope.item.branches || !$scope.item.branches.length) {
        $scope.item.branches = [{
            title: 'Home',
            _type: 'container',
        }]
    }


    $scope.search = {}

    //////////////////////////////////////////////////////////

    $scope.$watch('search.terms', function(data) {

        console.log('Searching', data)

        $http.get('/api/search', {
            ignoreLoadingBar: true,
            params: {
                keys: data,
                limit: 10,
            }
        }).then(function(response) {
            $scope.contentOptions = response.data;
        });

        



    })

    //////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////

    $scope.toolbox = [{
        title: 'Container',
        _type:'container',
    }, ]
*/



});

app.controller('UserFormController', function($scope, Session, $state, Fluro, FluroContent, $http,$rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {


    //////////////////////////////////////////////////

    /*
    //Save
    $scope.save = function(options) {

        if (!options) {
            options = {}
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        //////////////////////////////////////////

        if ($scope.item._id) {
            //Edit existing user
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);
        } else {

            //Creating a new user so we need to deviate from the usual course here
            FluroContent.endpoint('invite').save($scope.item, $scope.saveSuccessExit, $scope.saveFail);
        }
    }
    */




    $scope.$watch('item.permissionSets', function() {

        var filtered = _.filter($scope.item.permissionSets, function(set) {
            if (set && set.account) {
                return set.account._id == $rootScope.user.account._id;
            }
        });

        if (!filtered.length) {
            $scope.item.permissionSets = [{
                account: $rootScope.user.account,
                sets: [],
            }]
        }

        $scope.allowedPermissionSets = filtered;

    })


    //////////////////////////////////////////////////

    $scope.saveSuccess = function(result) {


        if (result._id) {
            console.log('Updated to version', $scope.item.__v, result.__v);
            $scope.item.__v = result.__v;
        }

        //Update the current user
        if ($rootScope.user._id == result._id) {
           // CacheManager.clearAll();
            //$rootScope.user = result;
            Session.refresh();
        } else {
            console.log('Clear USER')
            CacheManager.clear('user');
        }


        Notifications.status(result.name + ' was saved successfully')

        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            if ($rootScope.user._id == result._id) {
                $state.reload();
            }
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccessExit = function(result) {
        $scope.saveSuccess(result);
        $state.go('user');
    }


 
    //Creating a new user
    if (!$scope.item._id) {
        //Populate with some defaults
        var account_id = $rootScope.user.account._id;
        $scope.item.account = account_id;
        $scope.item.availableAccounts = [account_id];
    }

    //Check if current user
    $scope.me = ($rootScope.user._id == $scope.item._id);

    $scope.allowAccountChange = ($scope.me && $scope.item.availableAccounts.length > 1);
    $scope.canEditAuthDetails = (($scope.me || FluroAccess.isFluroAdmin()) || !$scope.item._id);



    //Allow changes to roles and realms
    $scope.allowRoleChange = FluroAccess.can('assign', 'role');
    $scope.allowRealmChange = FluroAccess.can('assign', 'realm');




    $scope.resendEmail = function() {

        $scope.resendState = 'processing';

        /////////////////////////////////////////////////

        var request = $http.post(Fluro.apiURL + '/auth/resend', {
            email: $scope.item.email
        });
    
        /////////////////////////////////////////////////

        request.error(function(err) {
            $scope.resendState = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending password instructions');
            }
        });

        /////////////////////////////////////////////////

        request.success(function(result) {
            $scope.resendState = 'ready';
            Notifications.status('Instructions on how to reset password have been sent to ' + $scope.item.email);
        });

    }




    /*
    //Save
    if ($scope.me) {
        $scope.save = function() {
            Content.resource('user').update({
                id: $scope.item._id,
            }, $scope.item, selfSaveSuccess);
        }
    }

    function selfSaveSuccess(data) {

        //CLEAR ALL CACHES
        console.log('NEED TO CHANGE CACHE STUFF AND REFRESH SESSION')
        //AdminCacheManager.clearAll();
        Notifications.status('Updates to your account were made successfully');
        //Session.create(data);


        if (FluroAccess.canAccess('user')) {
            $state.go('app.user.default');
        } else {
            $state.go('app.home');

        }
        
    }

    */




    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});
app.controller('UserListController', function($scope, ModalService) {



});
app.controller('BowerManagerController', function($scope, Fluro, $http) {


    $scope.bowerStatus = 'ready';


    $scope.install = function() {
        $scope.bowerStatus = 'progress';

        $http.post(Fluro.apiURL + '/site/bower/' + $scope.item._id, {}).then(function(res) {
            console.log('BOWER RES', res.data);
            $scope.bowerStatus = 'ready';
        }, function(res) {
            console.log('BOWER FAIL', res.data);
            $scope.bowerStatus = 'ready';
        });
    }
})



app.directive('fluroComponentLoader', function(Fluro, $compile) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        template: '<div class="component-loader"></div>',
        link: function($scope, $element, $attr) {

            //String
            var watchString = 'model.components + model.scripts + model.bower.length';

            /////////////////////////////////////

            $scope.$watch(watchString, function() {

                //Clear out old scripts
                $element.empty();

                if (!$scope.model) {
                    return;
                }

                //Write our script
                var script = '';
                //var styles = '';
                //var sassString = '';

                ///////////////////////////////////////


                if ($scope.model.components && $scope.model.components.length) {

                    console.log('Load components')
                    
                    _.each($scope.model.components, function(component) {
                        // script += component.js;
                        script += '<script type="text/lazy">';
                        script += 'console.log("Init Component ' + component.title + '");\n';
                         script += component.compiled;
                         script += '</script>';
                        // var scriptString = component.js;

                        // //Replace the template placeholder with our html
                        // if (String(component.html) && String(component.html).length) {
                        //     scriptString = scriptString.replace('[TEMPLATE]', String(component.html).replace(/\n|\t/g, ' '));
                        // }


                        //Add the CSS if its available
                        //sassString += component.css;

                        //Add the compiled js
                        // script += scriptString;

                    })
                    
                }

                ///////////////////////////////////////

                //Include Inline Script
                if ($scope.model.scripts && $scope.model.scripts.length) {
                    console.log('Load inline scripts');


                    // script += '<script type="text/lazy">\n';
                    _.each($scope.model.scripts, function(s) {
                        script += '<script type="text/lazy">\n';
                        script += 'console.log("Init ' +s.title + '");\n';
                        script += s.body + ';\n';
                        script += '</script>';
                    })


                    // script += '</script>';
                }

                ///////////////////////////////////////

                //If there are bower components aswell then include the vendor scripts
                if ($scope.model.bower && $scope.model.bower.length) {
                    console.log('Load Bower Scripts');
                    var vendorJSUrl = Fluro.apiURL + '/get/site/' + $scope.model._id + '/js';
                    script += '<script type="text/lazy" src="' + vendorJSUrl + '"></script>';

                    /**
                    var vendorCSSUrl = Fluro.apiURL + '/get/site/' + $scope.model._id + '/css';
                    styles += '<link href="' + vendorCSSUrl + '" rel="stylesheet" type="text/css"/>';
                    /**/
                }

                ///////////////////////////////////////

                /**
                if (sassString && sassString.length) {
                    //Compile the SASS
                    Sass.compile(sassString, function(result) {
                        styles += '<style type="text/css">' + result.text + '</style>';
                    })
                }


                ///////////////////////////////////////

                if(styles && styles.length) {
                    $element.append(styles);
                }
                /**/

                ///////////////////////////////////////

                if (script && script.length) {
                    //Do the magic!
                    $element.append(script);
                    $compile($element.contents())($scope);
                }


            }, true);
        },
    };
});


app.directive('fluroComponentPreview', function(Fluro, $compile) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        templateUrl: 'fluro-component-preview/fluro-component-preview.html',
        link: function($scope, $element, $attr) {

            $scope.$watch('model', function(component) {
                $element.empty();

                if(component) {

                    var directiveName = component.directive;
                    var script = '<script type="text/lazy">' + component.compiled + '</script>';
                    var template = '<' + directiveName + '></' + directiveName + '>';

                    $element.append(script);
                    $compile($element.contents())($scope);

                    $element.append(template);
                    $compile($element.contents())($scope);
                }
            });

            /*
            $scope.$watch('model.length', function() {


                $element.empty();

                var components = $scope.model;

                if (components.length) {

                    var template = '';
                    var script = '';

                    _.each(components, function(component) {

                        var directiveName = component.directive;
                        //script += component.js;
                        //var html = component.html;

                        //Add the html string
                        template += '<' + directiveName + '></' + directiveName + '>';

                        //var url = Fluro.apiURL + '/get/scripts/component/' + component._id;
                        script += '<script type="text/lazy">' + component.compiled + '</script>';

                        //$ocLazyLoad.inject('textAngular');

                    })

                    //script += '</script>';


                    

                    //console.log(compiledTemplate);

                    $element.append(script);
                    $compile($element.contents())($scope);

                    $element.append(template);
                    $compile($element.contents())($scope);


                    /*
                    var string = '';

                    var list = _.map(components, function(component) {

                        string += '<div class="panel panel-default"><' + component.directive + '><' + component.directive + '></div>';

                        //Get the script location
                        var url = Fluro.apiURL + '/get/scripts/component/' + component._id;

                        //Return the details to lazyload
                        return {
                            type: 'js',
                            path: url,
                        }
                    });


                    console.log('Lazy load', list);

                    $ocLazyLoad.load(list).then(function() {

                        
                        


                        var template = string;
                        var compiled = $compile(template)($scope);

                        $element.append(compiled);

                    }, function() {
                        console.log('Failed to load components')
                    });

                }
            });
*/
        },
    };
});
app.directive('fluroComponentSelect', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            preview: '=ngPreview',
        },
        // Replace the div with our template
        templateUrl: 'fluro-component-select/fluro-component-select.html',
        controller: 'FluroComponentSelectController',
    };
});


app.controller('FluroComponentSelectController', function($scope, FluroContent) {

    if (!$scope.model) {
        $scope.model = [];
    }

    //Load the components
    //$scope.items = FluroContent.endpoint('my/components', false, true).query();

   

    $scope.previewComponent = function(component) {
        if(!$scope.preview) {
            $scope.preview = {}
        }

        $scope.preview.component = component;
    }


    $scope.refreshStatus = ''

    //////////////////////////////////////////


    $scope.reloadComponents = function() {
        $scope.refreshStatus = 'loading';
        FluroContent.endpoint('my/components', false, true).query().$promise.then(function(res) {
            console.log('RES', res)
            $scope.items = res;
            $scope.refreshStatus = 'complete';
        });
    }

    //One for good measure
     $scope.reloadComponents();

    //////////////////////////////////////////

    $scope.reloadComponent = function(component) {
        console.log('Reload Component', component.title)
        var index = _.findIndex($scope.model, { '_id': component._id});

        if(index != -1) {
            console.log('Replace component');
            $scope.model[index] = component;
        } else {
            console.log('Insert Component');
            $scope.model.push(component);
        }
    }


    //////////////////////////////////////////

    $scope.isSelected = function(item) {

        var checkId = item;
        if (item._id) {
            checkId = item._id;
        }

        //Check if the item is activated
        return _.some($scope.model, function(active) {
            if (active._id) {
                return active._id == checkId;
            } else {
                return active == checkId;
            }
        });

    }

    //////////////////////////////////////////

    $scope.deselect = function(item) {

        var checkId = item;
        if(item._id) {
            checkId = item._id;
        }

        _.remove($scope.model, function(search) {
            if(search._id) {
                return search._id == checkId;
            } else {
                return search == checkId;
            }
        })
    }

    //////////////////////////////////////////

    $scope.select = function(item) {
        console.log('Select', item)
        $scope.model.push(item);
    }

    //////////////////////////////////////////

    $scope.toggle = function(item) {
        var active = $scope.isSelected(item);
        if (active) {
            $scope.deselect(item);
        } else {
            $scope.select(item);
        }

    }

    //////////////////////////////////////////

})
app.factory('dateutils', function() {
    // validate if entered values are a real date
    function validateDate(date) {


        // store as a UTC date as we do not want changes with timezones
        var d = new Date(Date.UTC(date.year, date.month, date.day));
        var result = d && (d.getMonth() === date.month && d.getDate() === Number(date.day));

        // console.log('Valid date', result, date);
        return result;
    }

    // reduce the day count if not a valid date (e.g. 30 february)
    function changeDate(date) {
        if (date.day > 28) {
            date.day--;
            return date;
        }
        // this case should not exist with a restricted input
        // if a month larger than 11 is entered
        else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    }

    var self = this;
    this.days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    this.months = [{
        value: 0,
        name: 'January'
    }, {
        value: 1,
        name: 'February'
    }, {
        value: 2,
        name: 'March'
    }, {
        value: 3,
        name: 'April'
    }, {
        value: 4,
        name: 'May'
    }, {
        value: 5,
        name: 'June'
    }, {
        value: 6,
        name: 'July'
    }, {
        value: 7,
        name: 'August'
    }, {
        value: 8,
        name: 'September'
    }, {
        value: 9,
        name: 'October'
    }, {
        value: 10,
        name: 'November'
    }, {
        value: 11,
        name: 'December'
    }];

    return {
        checkDate: function(date) {

            if (!date.day) {
                return false;
            }

            if (date.month === 0) {

            } else {
                if (!date.month) {
                    return false;
                }
            }

            if (!date.year) {
                return false;
            }

            if (validateDate(date)) {
                // update the model when the date is correct
                return date;
            } else {
                // change the date on the scope and try again if invalid
                return this.checkDate(changeDate(date));
            }
        },
        get: function(name) {
            return self[name];
        }
    };
});

app.directive('dobSelect', ['dateutils',
    function(dateutils) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                hideAge: '=?',
                hideDates: '=?',
            },
            controller: ['$scope', 'dateutils',
                function($scope, dateutils) {


                    // set up arrays of values
                    $scope.days = dateutils.get('days');

                    $scope.months = dateutils.get('months');

                    /////////////////////////////////////////////////////

                    if (!$scope.model) {
                        //$scope.model = new Date();
                        console.log('No way man')
                    } else {

                        console.log('TESTING', $scope.model, _.isDate($scope.model))
                        if (!_.isDate($scope.model)) {
                            $scope.model = new Date($scope.model);
                        }
                    }

                    /////////////////////////////////////////////////////

                    // split the current date into sections
                    var dateFields = {};

                    if ($scope.model) {
                        //Update the date fields
                        dateFields.day = new Date($scope.model).getDate();
                        dateFields.month = new Date($scope.model).getMonth();
                        dateFields.year = new Date($scope.model).getFullYear();

                        $scope.dateFields = dateFields;


                        /////////////////////////////////////////////////////

                        //Age
                        $scope.age = moment().diff($scope.model, 'years');

                    }

                    /////////////////////////////////////////////////////

                    // validate that the date selected is accurate
                    $scope.checkDate = function() {
                        // update the date or return false if not all date fields entered.
                        var date = dateutils.checkDate($scope.dateFields);
                        if (date) {
                            $scope.dateFields = date;
                        }
                    };

                    /////////////////////////////////////////////////////

                    function dateFieldsChanged(newDate, oldDate) {

                        //console.log('Date fields Changed', newDate);
                        if (newDate == oldDate) {
                            return;
                        }

                        //Stop Watching Age
                        stopWatchingAge();


                        /////////////////////////////////

                        if (!$scope.dateFields) {
                            return console.log('No datefields object');
                        }

                        if (!$scope.dateFields.year) {
                            return console.log('No Year');
                        }

                        /////////////////////////////////

                        if ($scope.dateFields.month === 0) {

                        } else {
                            if (!$scope.dateFields.month) {
                                return console.log('No Month');
                            }
                        }

                        /////////////////////////////////

                        if (!$scope.dateFields.day) {
                            return console.log('No day');
                        }

                        /////////////////////////////////

                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0);

                        if (!$scope.model) {
                            $scope.model = new Date();
                        }


                        //Set the models details
                        $scope.model.setDate(date.getDate());
                        $scope.model.setMonth(date.getMonth());
                        $scope.model.setFullYear(date.getFullYear());

                        //Get the difference
                        var diff = moment().diff($scope.model, 'years');

                        //Update the age
                        $scope.age = parseInt(diff);


                        //Start Watching Age
                        startWatchingAge();
                    }


                    //Age was changed
                    function ageChanged(newAge, oldAge) {

                        if (newAge == oldAge) {
                            return;
                        }
                        console.log('Age changed', newAge, oldAge);

                        var today = new Date();
                        today.setHours(0, 0, 0, 0);

                        var currentYear = today.getFullYear();
                        var diff = parseInt(currentYear) - parseInt(newAge);

                        if (diff) {

                            //Stop Watching the Date
                            stopWatchingDate();

                            if (!$scope.dateFields) {

                                // if(!$scope.model) {
                                //     $scope.model = new Date();
                                // }

                                var dateFields = {};
                                //Update the date fields
                                //dateFields.day = new Date($scope.model).getDate();
                                //dateFields.month = new Date($scope.model).getMonth();
                                dateFields.year = diff; //new Date($scope.model).getFullYear();
                                $scope.dateFields = dateFields;
                            } else {
                                $scope.dateFields.year = diff;
                            }

                            //Set the year
                            // if ($scope.dateFields.year != diff) {
                            //     $scope.dateFields.year = diff;
                            // }

                            //Start Watching the date again
                            startWatchingDate();


                        } else {
                            console.log('NOPE', newAge, currentYear, diff);
                        }


                    }

                    /////////////////////////////////////////////////////

                    var ageWatcher;

                    function startWatchingAge() {
                        ageWatcher = $scope.$watch('age', ageChanged);
                    }

                    function stopWatchingAge() {
                        if (ageWatcher) {
                            ageWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////

                    var dateWatcher;

                    function startWatchingDate() {
                        dateWatcher = $scope.$watch('dateFields', dateFieldsChanged, true);
                    }

                    function stopWatchingDate() {
                        if (dateWatcher) {
                            dateWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////


                    //Start watching for changes to either
                    startWatchingAge();
                    startWatchingDate();
                    /*
                    /////////////////////////////////////////////////////

                    $scope.updateAge = function() {
                        console.log('Change Age');
                        var today = new Date();
                       
                        var currentYear = today.getFullYear();
                        var diff = currentYear - age;
                        console.log('AGE', age, diff, currentYear);

                        //Set the year
                        $scope.dateFields.year = diff
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateDate = function() {
                        console.log('Change Date');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setDate(date.getDate());
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateMonth = function() {
                        console.log('Change Month');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setMonth(date.getMonth());
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateYear = function() {
                        console.log('Change Year');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setFullYear(date.getFullYear());
                    }

                    /////////////////////////////////////////////////////

                    $scope.$watch('model', function(newModel, oldModel) {


                        console.log('Model Change');
                        $scope.dateFields.day = new Date(newModel).getUTCDate();
                        $scope.dateFields.month = new Date(newModel).getUTCMonth();
                        $scope.dateFields.year = new Date(newModel).getUTCFullYear();

                    })
*/

                    /*
                $scope.$watch('dateFields', function() {

                    //Update our date fields
                    $scope.dateFields.day = new Date($scope.model).getUTCDate();
                    $scope.dateFields.month = new Date($scope.model).getUTCMonth();
                    $scope.dateFields.year = new Date($scope.model).getUTCFullYear();

                    //Get the actual Date
                    $scope.model.
                    //var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);

                    //console.log('tESTING', $scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day, date);
                    $scope.age = moment().diff(date, 'years');
                    $scope.model = date;

                }, true);

                $scope.updateAge = function() {
                    if (newAge != oldAge) {

                        var today = new Date();
                        var age = Number(newAge);
                        var currentYear = today.getFullYear();
                        var diff = currentYear - age;
                        console.log('AGE', age, diff, currentYear);
                        //Set the year
                        $scope.dateFields.year = diff
                    }

                });
*/




                }
            ],
            templateUrl: 'fluro-dob-select/fluro-dob-select.html',
            link: function(scope, element, attrs, ctrl) {
                if (attrs.yearText) {
                    scope.yearText = true;
                }
                // allow overwriting of the
                if (attrs.dayDivClass) {
                    angular.element(element[0].children[0]).removeClass('form-group col-xs-3');
                    angular.element(element[0].children[0]).addClass(attrs.dayDivClass);
                }
                if (attrs.dayClass) {
                    angular.element(element[0].children[0].children[0]).removeClass('form-control');
                    angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass);
                }
                if (attrs.monthDivClass) {
                    angular.element(element[0].children[1]).removeClass('form-group col-xs-5');
                    angular.element(element[0].children[1]).addClass(attrs.monthDivClass);
                }
                if (attrs.monthClass) {
                    angular.element(element[0].children[1].children[0]).removeClass('form-control');
                    angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass);
                }
                if (attrs.yearDivClass) {
                    angular.element(element[0].children[2]).removeClass('form-group col-xs-4');
                    angular.element(element[0].children[2]).addClass(attrs.yearDivClass);
                }
                if (attrs.yearClass) {
                    angular.element(element[0].children[2].children[0]).removeClass('form-control');
                    angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass);
                }

                // set the years drop down from attributes or defaults
                var currentYear = parseInt(attrs.startingYear, 10) || new Date().getFullYear();
                var numYears = parseInt(attrs.numYears, 10) || 100;
                var oldestYear = currentYear - numYears;

                scope.years = [];
                for (var i = currentYear; i >= oldestYear; i--) {
                    scope.years.push(i);
                }



                // pass down the ng-disabled property
                scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                    scope.disableFields = newVal;
                });

                /*
      // ensure that fields are entered
      var required = attrs.required.split(' ');
      ctrl.$parsers.push(function(value) {
        angular.forEach(required, function (elem) {
          if(!angular.isNumber(elem)) {
            ctrl.$setValidity('required', false);
          }
        });
        ctrl.$setValidity('required', true);
      });
/**/
                // var validator = function(){
                //   var valid = true;
                //   // all fields entered
                //   angular.forEach(required, function (elem) {
                //     if(!angular.isNumber(elem)) {
                //       valid = false;
                //     }
                //   });
                //   // if(isNaN(scope.dateFields.day) && isNaN(scope.dateFields.month) && isNaN(scope.dateFields.year)){
                //   //   valid = true;
                //   // }
                //   // else if(!isNaN(scope.dateFields.day) && !isNaN(scope.dateFields.month) && !isNaN(scope.dateFields.year)){
                //   //   valid = true;
                //   // }
                //   // else valid = false;

                //   ctrl.$setValidity('rsmdatedropdowns', valid);
                // };

                // scope.$watch('dateFields.day', function(){
                //   validator();
                // });

                // scope.$watch('dateFields.month', function(){
                //   validator();
                // });

                // scope.$watch('dateFields.year', function(){
                //   validator();
                // });
            }
        };
    }
]);



/*
app.directive('dobSelect', function($document) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'fluro-dob-select/fluro-dob-select.html',
        scope: {
            model: '=ngModel',
        },
        link: function($scope, element, attr) {

            /////////////////////////////////////

            $scope.dateObject = {};

            /////////////////////////////////////

            $scope.settings = {}

            /////////////////////////////////////

            function elementClick(event) {
                //Clicked inside
                event.stopPropagation();
            }

            function documentClick(event) {
                //Clicked outside
                $scope.$apply(function() {
                    $scope.open = false;
                });
            }

            /////////////////////////////////////

            if (!$scope.model) {
                $scope.model = new Date();
            }

            /////////////////////////////////////

            $scope.$watch('model', function(data) {
                if ($scope.model) {
                    var date = new Date($scope.model);
                    $scope.dateObject.year = date.getFullYear();
                    $scope.dateObject.month = date.getMonth();
                    $scope.dateObject.day = date.getDay();
                }
            })

            /////////////////////////////////////

            $scope.$watch('dateObject', function(newValue, oldValue) {
                if (newValue != oldValue) {
                    var data = newValue;
                    $scope.model = new Date(data.year, data.month, data.day);
                }

                console.log($scope.model);
            }, true);

            /*
            /////////////////////////////////////

            $scope.$watch('model', function(newValue, oldValue) {
                if(newValue != oldValue) {
                   
                   console.log($scope.model.getYear(), $scope.model.getMonth(), $scope.model.getDate());

                   $scope.dateObject.year = Number($scope.model.getYear());
                   $scope.dateObject.month = Number($scope.model.getMonth());
                   $scope.dateObject.day = Number($scope.model.getDay());
                   
                }

            }, true);


            /////////////////////////////////////
            /*
            $scope.$watch('model', function() {
                if (!$scope.model) {
                    $scope.model = new Date();
                }

                $scope.dateObject.year = $scope.model.getYear();
                $scope.dateObject.month = $scope.model.getYear();
                $scope.dateObject.day = $scope.model.getYear();
            })



            //Listen for when this date select is open
            $scope.$watch('settings.open', function(bol) {
                if (bol) {
                    element.on('click', elementClick);
                    $document.on('click', documentClick);
                } else {
                    element.off('click', elementClick);
                    $document.off('click', documentClick);
                }
            })

        },
        controller: function($scope) {






        }

    };

});
/**/
app.controller('FluroInteractionButtonSelectController', function($scope, FluroValidate) {


     ////console.log('FluroInteractionButtonSelectController')
    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    $scope.selection = {
        values: [],
        value: null,
    }


    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if(!minimum) {
        minimum = 0;
    }

    if(!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);


    /////////////////////////////////////////////////////////////////////////

    $scope.dragControlListeners = {
        //accept: function (sourceItemHandleScope, destSortableScope) {return boolean}//override to determine drag is allowed or not. default is true.
        //itemMoved: function (event) {//Do what you want},
        orderChanged: function(event) {
            //Do what you want
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        },
        //containment: '#board'//optional param.
        //clone: true //optional param for clone feature.
        //allowDuplicates: false //optional param allows duplicates to be dropped.
    };


    /////////////////////////////////////////////////////////////////////////

    $scope.selectBox = {}

    $scope.selectUpdate = function() {
        if(!$scope.selectBox.item) {
            return;
        }
        $scope.selection.values.push($scope.selectBox.item);
        $scope.model[opts.key] = angular.copy($scope.selection.values);
    }

    /////////////////////////////////////////////////////////////////////////




    $scope.canAddMore = function() {

        if(!maximum) {
            return true;
        }
       
        if($scope.multiple) {
            return ($scope.selection.values.length < maximum);
        } else {
            if(!$scope.selection.value) {
                return true;
            }
        }
        
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.contains = function(value) {
        if ($scope.multiple) {
            //Check if the values are selected
            return _.includes($scope.selection.values, value);
        } else {
            return $scope.selection.value == value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.select = function(value) {

        if ($scope.multiple) {
            if (!$scope.canAddMore()) {
                return;
            }
            $scope.selection.values.push(value);
        } else {
            $scope.selection.value = value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.deselect = function(value) {
        if ($scope.multiple) {
            _.pull($scope.selection.values, value);
        } else {
            $scope.selection.value = null;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.toggle = function(reference) {

        if ($scope.contains(reference)) {
            $scope.deselect(reference);
        } else {
            $scope.select(reference);
        }

        //Update model
        setModel();
    }


    /////////////////////////////////////////////////////////////////////////

    // initialize the checkboxes check property
    $scope.$watch('model', function(newModelValue, oldModelValue) {


        if (newModelValue != oldModelValue) {
            var modelValue;

            //If there is properties in the FORM model
            if (_.keys(newModelValue).length) {

                //Get the model for this particular field
                modelValue = newModelValue[opts.key];

                if ($scope.multiple) {
                    if (modelValue && _.isArray(modelValue)) {
                        $scope.selection.values = angular.copy(modelValue);
                    } else {
                        $scope.selection.values = [];
                    }
                } else {
                    $scope.selection.value = angular.copy(modelValue);
                }
            }
        }
    }, true);


    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {

        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    function setModel() {
        if ($scope.multiple) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        } else {
            $scope.model[opts.key] = angular.copy($scope.selection.value);
        }

        if ($scope.fc) {
            $scope.fc.$setTouched();
        }

        //////console.log('Model set!', $scope.model[opts.key]);
        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }

    /////////////////////////////////////////////////////////////////////////
})
////////////////////////////////////////////////////////////////////////

app.directive('fluroDateSelect', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'fluro-interaction-form/date-select/date-selector.html',
        controller: ['$scope', 'dateutils', function($scope, dateutils) {



            //Get days and months
            $scope.days = dateutils.get('days');
            $scope.months = dateutils.get('months');

            /////////////////////////////////////////////////////

            // split the current date into sections
            var dateFields = {};

            /////////////////////////////////////////////////////

            $scope.$watch('dateFields', dateFieldsChanged, true);
            $scope.$watch('model', modelChanged);

            /////////////////////////////////////////////////////

            function modelChanged() {
                if ($scope.model) {
                    if (!_.isDate($scope.model)) {
                        $scope.model = new Date($scope.model);
                    }

                    //Update the date fields
                    dateFields.day = new Date($scope.model).getDate();
                    dateFields.month = new Date($scope.model).getMonth();
                    dateFields.year = new Date($scope.model).getFullYear();
                    $scope.dateFields = dateFields;
                }
            }

            /////////////////////////////////////////////////////

            function dateFieldsChanged(newDate, oldDate) {

                //////console.log('Date fields Changed', newDate);
                if (newDate == oldDate) {
                    return;
                }

                /////////////////////////////////

                if (!$scope.dateFields) {
                    return ////console.log('No datefields object');
                }

                if (!$scope.dateFields.year) {
                    return ////console.log('No Year');
                }

                /////////////////////////////////

                if ($scope.dateFields.month === 0) {

                } else {
                    if (!$scope.dateFields.month) {
                        return ////console.log('No Month');
                    }
                }

                /////////////////////////////////

                if (!$scope.dateFields.day) {
                    return ////console.log('No day');
                }

                /////////////////////////////////

                var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                date.setHours(0, 0, 0, 0);

                if (!$scope.model) {
                    $scope.model = new Date();
                }


                //Set the models details
                $scope.model.setDate(date.getDate());
                $scope.model.setMonth(date.getMonth());
                $scope.model.setFullYear(date.getFullYear());
            }

            /////////////////////////////////////////////////////

            // validate that the date selected is accurate
            $scope.checkDate = function() {
                // update the date or return false if not all date fields entered.
                var date = dateutils.checkDate($scope.dateFields);
                if (date) {
                    $scope.dateFields = date;
                }
            };


            

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            
            /**
             ////console.log('FluroDateSelectController')
            $scope.today = function() {
                $scope.model[$scope.options.key] = new Date();
            };


            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.opened = true;
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd/MM/yyyy'];
            $scope.format = $scope.formats[0];

            /**/
        }]
    }

}]);
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'dob-select',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/dob-select/fluro-dob-select.html',
        //controller: 'FluroInteractionDobSelectController',
        wrapper: ['bootstrapHasError'],
    });


     ////console.log('fluro-dob-select')

});

app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'embedded',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/embedded/fluro-embedded.html',
        controller: 'FluroInteractionNestedController',
        wrapper: ['bootstrapHasError'],
    });

    ////console.log('fluro-embedded')

});

/**

app.controller('FluroEmbeddedDefinitionController', function($scope, $http, Fluro, $filter, FluroValidate) {


})

/**/
app.directive('interactionForm', function($compile) {

    //////console.log('interaction form controller directive')
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            callback: '=?callback',
        },
        transclude: true,
        controller: 'InteractionFormController',
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };


});

////////////////////////////////////////////////////////////////////////

app.directive('webform', function($compile) {

    //////console.log('web form controller directive')

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback: '=?callback',
            linkedEvent: '=?linkedEvent',
        },
        transclude: true,
        controller: 'InteractionFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
                // console.log('Transclude content', clone)
                //$element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };
});


app.config(function(formlyConfigProvider) {


    formlyConfigProvider.setType({
        name: 'currency',
        extends: 'input',
        overwriteOk: true,
        controller: ['$scope', function($scope) {
            /*
            ////////console.log('CURRENCY SCOPE', $scope);

            $scope.$watch('model[options.key]', function(val) {

                if(!$scope.model[$scope.options.key] && $scope.model[$scope.options.key] != 0 ) {
                    ////////console.log('Set!')
                    $scope.model[$scope.options.key] = 0;
                }
            })
            /**/
        }],

        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: 'ng-currency'
                },
                fractionValue: {
                    attribute: 'fraction',
                    bound: 'fraction'
                },
                minimum: {
                    attribute: 'min',
                    bound: 'min'
                },
                maximum: {
                    attribute: 'max',
                    bound: 'max'
                }
            },
            templateOptions: {
                customAttrVal: '',
                required: true,
                fraction: 2,
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {

                        var numericValue = Number($modelValue);

                        if (isNaN(numericValue)) {
                            return false;
                        }

                        //Get Minimum and Maximum Amounts
                        var minimumAmount = scope.options.data.minimumAmount;
                        var maximumAmount = scope.options.data.maximumAmount;

                        if (minimumAmount && numericValue < minimumAmount) {
                            return false;
                        }

                        if (maximumAmount && numericValue > maximumAmount) {
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    });


})

////////////////////////////////////////////////////////////////////////

app.run(function(formlyConfig, $templateCache) {

    //////console.log('web form run')
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {
        var fluroErrorTemplate = $templateCache.get('fluro-interaction-form/field-errors.html');
        return '<div>' + template + fluroErrorTemplate + '</div>';
    });

    //////////////////////////////////

    formlyConfig.setType({
        name: 'upload',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/upload/upload.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: 'FluroInteractionFormUploadController',
    });


    //////////////////////////////////

    formlyConfig.setType({
        name: 'multiInput',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/multi.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: ['$scope', function($scope) {
            $scope.copyItemOptions = copyItemOptions;

            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
        }]
    });

    //////////////////////////////////

    // formlyConfig.setType({
    //     name: 'payment',
    //     overwriteOk: true,
    //     templateUrl: 'fluro-interaction-form/payment/payment.html',
    //     //controller: 'FluroPaymentController',
    //     defaultOptions: {
    //         noFormControl: true,
    //     },
    // });

    formlyConfig.setType({
        name: 'custom',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/custom.html',
        controller: 'CustomInteractionFieldController',
        wrapper: ['bootstrapHasError']
    });

    formlyConfig.setType({
        name: 'button-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/button-select/fluro-button-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });



    formlyConfig.setType({
        name: 'date-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/date-select/fluro-date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });



    formlyConfig.setType({
        name: 'terms',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/fluro-terms.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'order-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/order-select/fluro-order-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    /**/

});

////////////////////////////////////////////////////////////////////////

app.controller('CustomInteractionFieldController', function($scope, FluroValidate) {
    $scope.$watch('model[options.key]', function(value) {
        if (value) {
            if ($scope.fc) {
                $scope.fc.$setTouched();
            }
        }
    }, true);
});

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

app.controller('InteractionFormController', function($scope, $q, $timeout, $rootScope, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {




    // console.log($scope.integration);

    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }

    /////////////////////////////////////////////////////////////////

    //Keep this available to all field scopes
    //this is used so that other fields can get calculated total
    //not just the payment summary field
    $formScope = $scope;

    /////////////////////////////////////////////////////////////////

    function debugLog() {
        if ($scope.debugMode) {
            console.log(arguments);
        }
    }


    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';


    /////////////////////////////////////////////////////////////////

    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


    $scope.scrollToField = function(field) {
        var element = _.get(field, 'formControl.$$element[0]');
        if (element && element.scrollIntoView) {
            element.scrollIntoView();
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    // $scope.$watch('vm.modelForm.$invalid', updateErrorList)
    $scope.$watch('vm.modelFields', validateAllFields, true)
    $scope.$watch('vm.model', validateAllFields, true)
        // $scope.$watch('vm.modelForm.$touched', updateErrorList)
        // $scope.$watch('vm.modelForm.$error.required', updateErrorList)
        // $scope.$watch('vm.modelForm.$error.validInput', updateErrorList)

    function validateAllFields() {


        $timeout(function() {

            function setValid(ready) {
                $scope.readyToSubmit = ready;
            }

            /////////////////////////////////////

            function mapRecursiveField(field) {
                if (!field) {
                    return;
                }

                var output = [field];


                if (field.fields && field.fields.length) {
                    output.push(_.map(field.fields, mapRecursiveField));
                }

                if (field.fieldGroup && field.fieldGroup.length) {
                    output.push(_.map(field.fieldGroup, mapRecursiveField));
                }

                if (field.data && field.data.replicatedFields && field.data.replicatedFields.length) {

                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                } else {
                    if (field.data && field.data.fields && field.data.fields.length) {
                        output.push(_.map(field.data.fields, mapRecursiveField));
                    }
                }

                return output;
            }

            ////////////////////////////////////////////

            $scope.errorList = _.chain($scope.vm.modelFields)
                .map(mapRecursiveField)
                .flattenDeep()
                .compact()
                .filter(function(field) {
                    var required = _.get(field, 'formControl.$error.required');
                    var invalid = _.get(field, 'formControl.$error.validInput');
                    var uploadInProgress = _.get(field, 'formControl.$error.uploadInProgress');


                    return required || invalid || uploadInProgress;
                })
                .value();

            ////////////////////////////////////////////

            //Interaction Form
            var interactionForm = $scope.vm.modelForm;

            if (!interactionForm) {
                // console.log('Invalid no form')
                return setValid(false);
            }

            if (interactionForm.$invalid) {
                // console.log('Invalid because its invalid', interactionForm.$invalid, interactionForm);
                return setValid(false);
            }

            if (interactionForm.$error) {

                if (interactionForm.$error.required && interactionForm.$error.required.length) {
                    // console.log('required input not provided');
                    return setValid(false);
                }

                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {
                    // console.log('valid input not provided');
                    return setValid(false);
                }

                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) {
                    // console.log('Upload has not finished');
                    return setValid(false);
                }
            }

            // ////////console.log('Form should be good to go')

            //It all worked so set to true
            setValid(true);
        })

    }

    /**
     /////////////////////////////////////////////////////////////////

    $scope.$watch('vm.modelFields', function(fields) {
        //////////console.log('Interaction Fields changed')
        $scope.errorList = getAllErrorFields(fields);

        console.log('Error List', $scope.errorList);
    }, true)

    /**/


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }


    formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is still processing';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            //console.log('Reset the default model')
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();


        //Act as if a new definition was changed
        definitionModelChanged();




        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';


        //Reset after state change
        ////////console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    // //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    $scope.$watch('model', definitionModelChanged);


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function definitionModelChanged() {


        // ////////console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'interaction') {
            return console.log('Model is not an interaction'); //$scope.model = {};
        }

        /////////////////////////////////////////////////////////////////

        //check if we have the correct permissions



        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitInteraction;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        if (!interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) {
            interactionFormSettings.requireFirstName = true;
            interactionFormSettings.requireLastName = true;
            interactionFormSettings.requireGender = true;
            interactionFormSettings.requireEmail = true;

            switch (interactionFormSettings.identifier) {
                case 'both':
                    interactionFormSettings.requireEmail =
                        interactionFormSettings.requirePhone = true;
                    break;
                case 'email':
                    interactionFormSettings.requireEmail = true;
                    break;
                case 'phone':
                    interactionFormSettings.requirePhone = true;
                    break;
                case 'either':
                    interactionFormSettings.askPhone = true;
                    interactionFormSettings.askEmail = true;
                    break;
            }
        }


        /////////////////////////////////////////////////////////////////

        var firstNameField;
        var lastNameField;
        var genderField;

        /////////////////////////////////////////////////////////////////

        //Gender
        if (interactionFormSettings.askGender || interactionFormSettings.requireGender) {
            genderField = {
                    key: '_gender',
                    type: 'select',
                    templateOptions: {
                        type: 'email',
                        label: 'Title',
                        placeholder: 'Please select a title',
                        options: [{
                            name: 'Mr',
                            value: 'male'
                        }, {
                            name: 'Ms / Mrs',
                            value: 'female'
                        }],
                        required: interactionFormSettings.requireGender,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return (value == 'male' || value == 'female');
                        }
                    }
                }
                //$scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //First Name
        if (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) {
            firstNameField = {
                key: '_firstName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'First Name',
                    placeholder: 'Please enter your first name',
                    required: interactionFormSettings.requireFirstName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////

        //Last Name
        if (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) {
            lastNameField = {
                key: '_lastName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Last Name',
                    placeholder: 'Please enter your last name',
                    required: interactionFormSettings.requireLastName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        if (genderField && firstNameField && lastNameField) {

            genderField.className = 'col-sm-2';

            firstNameField.className =
                lastNameField.className = 'col-sm-5';

            $scope.vm.modelFields.push({
                fieldGroup: [genderField, firstNameField, lastNameField],
                className: 'row'
            });
        } else if (firstNameField && lastNameField && !genderField) {
            firstNameField.className =
                lastNameField.className = 'col-sm-6';

            $scope.vm.modelFields.push({
                fieldGroup: [firstNameField, lastNameField],
                className: 'row'
            });
        } else {
            if (genderField) {
                $scope.vm.modelFields.push(genderField);
            }

            if (firstNameField) {
                $scope.vm.modelFields.push(firstNameField);
            }

            if (lastNameField) {
                $scope.vm.modelFields.push(lastNameField);
            }
        }



        /////////////////////////////////////////////////////////////////

        //Email Address
        if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
            var newField = {
                key: '_email',
                type: 'input',
                templateOptions: {
                    type: 'email',
                    label: 'Email Address',
                    placeholder: 'Please enter a valid email address',
                    required: interactionFormSettings.requireEmail,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return validator.isEmail(value);
                    }
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._phoneNumber || !scope.model._phoneNumber.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }

            $scope.vm.modelFields.push(newField);
        }


        /////////////////////////////////////////////////////////////////

        //Ask Phone Number
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: '_phoneNumber',
                type: 'input',
                templateOptions: {
                    type: 'tel',
                    label: 'Contact Phone Number',
                    placeholder: 'Please enter a contact phone number',
                    required: interactionFormSettings.requirePhone,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._email || !scope.model._email.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            }


            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //Age / Date of birth
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: '_dob',
                type: 'dob-select',
                templateOptions: {
                    label: 'Date of birth',
                    placeholder: 'Please provide your date of birth',
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                    // params:{
                    //     hideAge:true,
                    // }

                }
            }

            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {


            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;

            /////////////////////////////

            //Ensure integers for min/max
            fieldDefinition.minimum = parseInt(fieldDefinition.minimum);
            fieldDefinition.maximum = parseInt(fieldDefinition.maximum);

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;

            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            } else if (fieldDefinition.description && fieldDefinition.description.length) {
                templateOptions.placeholder = fieldDefinition.description;
            } else {
                templateOptions.placeholder = fieldDefinition.title;
            }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';

            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':
                case 'value-select':
                    //Detour here
                    newField.type = 'button-select';
                    break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive;
                    break;
            }


            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options


            switch (fieldDefinition.type) {
                case 'reference':


                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //////////console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute: key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom') {
                switch (fieldDefinition.type) {
                    case 'boolean':
                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        //Only do this if its an integer cos iOS SUCKS!
                        if (fieldDefinition.type == 'integer') {
                            // ////////console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            //Force numeric keyboard
                            newField.ngModelAttrs.customAttrpattern = {
                                attribute: 'pattern',
                            }

                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard
                            templateOptions.customAttrpattern = "[0-9]*";
                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // ////////console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    if (!value) {
                        return true;
                    }

                    var valid = FluroValidate.validate(value, fieldDefinition);
                    return valid;
                }
            }

            /**
            if (fieldDefinition.directive == 'upload') {
                newField.validators.uploadInProgress = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    var isArray = _.isArray(value);

                    if (isArray) {
                        var values = value;
                        var hasErrors = _.includes(values, 'error');
                        var hasProcessing = _.includes(values, 'processing');

                        if (hasErrors || hasProcessing) {
                            console.log('VALLLLLL', hasErrors, hasProcessing)
                            return false;
                        } else {
                            console.log('VAL GOOD');
                            return true;
                        }
                    } else {
                        return (value == 'error' || value == 'processing');
                    }
                }
            }
            /**/

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////




            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //////////console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //////////console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    replicatedFields: []
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;

                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }


                        ////////console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);

                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }



            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {


                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // ////////console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field


            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {

                //Include Expression Properties
                // if (!newField.expressionProperties) {
                //     newField.expressionProperties = {};
                // }

                //////////////////////////////////////////

                //Add the hide expression if added through another method
                if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                    fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                }

                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.values(fieldDefinition.expressions).join('+');

                //////////////////////////////////////////

                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {


                        var watchScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                        }

                        //Return the result
                        var val = $parse(allExpressions)(watchScope);

                        return val;

                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        debugLog('Expression Changed');

                        //Create a new scope object
                        var checkScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(checkScope);

                            debugLog('expression:', retrievedValue, checkScope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = retrievedValue;
                                    break;
                                case 'hide':
                                    return field.hide = retrievedValue;
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }


                //Replace expression
                //var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');



                /*
                    //Add the expression properties
                    newField.expressionProperties[key] = function($viewValue, $modelValue, scope) {


                        //Replace expression
                        var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');

           


                       // var retrievedValue = $parse(replaceExpression)($scope);
                        var retrievedValue = _.get($scope, replaceExpression);

                         ////////console.log('Testing retrieved value from GET', retrievedValue, replaceExpression);

                        ////////////////////////////////////////

                        

                        return retrievedValue;
                    }
                    /**/
                //});
            }

            /////////////////////////////

            if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                newField.hideExpression = function($viewValue, $modelValue, scope) {

                    //Create a new scope object
                    var checkScope = {
                        model: scope.model,
                        data: $scope.vm.model,
                        interaction: $scope.vm.model,
                    }

                    var parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);

                    // console.log('hide expression parsed value', fieldDefinition.hideExpression, checkScope, parsedValue);
                    return parsedValue;
                }


            }

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details

        if (!$scope.model.paymentDetails) {
            $scope.model.paymentDetails = {};
        }

        var paymentSettings = $scope.model.paymentDetails;


        // //console.log('SCOPE MODEL', $scope.model);

        /////////////////////////////////////////////////////////////////

        //Credit Card Details
        if (paymentSettings.required || paymentSettings.allow) {

            //Setup the wrapper fields
            var paymentWrapperFields = [];
            var paymentCardFields = [];


            //console.log('PAYMENT SETTINGS REQUIRED', paymentSettings.required)

            // paymentWrapperFields.push({
            //     template: '<h4><i class="fa fa-credit-card"></i> Payment details</h4>'
            // });


            // ////////console.log('BEFORE SCOPE', $scope);

            if (paymentSettings.required) {



                debugLog('-- DEBUG -- Payment Settings Required')



                //Add the payment summary
                paymentWrapperFields.push({
                    templateUrl: 'fluro-interaction-form/payment/payment-summary.html',
                    controller: ['$scope', '$parse', function($scope, $parse) {

                        //Add the payment details to the scope
                        $scope.paymentDetails = paymentSettings;


                        //Start with the required amount
                        var requiredAmount = paymentSettings.amount;

                        //Store the calculatedAmount on the scope
                        $formScope.vm.model.calculatedTotal = requiredAmount;
                        $scope.calculatedTotal = requiredAmount;

                        /////////////////////////////////////////////////////

                        var watchString = '';

                        ////////////////////////////////////////
                        ////////////////////////////////////////

                        //Map each modifier to a property string and combine them all at once
                        var modelVariables = _.chain(paymentSettings.modifiers)
                            .map(function(paymentModifier) {
                                var string = '(' + paymentModifier.expression + ') + (' + paymentModifier.condition + ')';
                                return string;
                            })
                            .flatten()
                            .compact()
                            .uniq()
                            .value();


                        if (modelVariables.length) {
                            watchString = modelVariables.join(' + "a" + ');
                        }

                        ////////////////////////////////////////

                        if (watchString.length) {
                            debugLog('Watching changes', watchString);


                            $scope.$watch(watchString, calculateTotal);
                        } else {

                            // debugLog('No watch string set', paymentSettings);

                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;
                            $scope.modifications = [];
                        }

                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////

                        function calculateTotal(watchString) {

                            // //console.log('calculate please');

                            debugLog('Recalculate total', watchString);


                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;

                            $scope.modifications = [];


                            if (!paymentSettings.modifiers || !paymentSettings.modifiers.length) {

                                debugLog('No payment modifiers set');


                                return;
                            }

                            //Loop through each modifier
                            _.each(paymentSettings.modifiers, function(modifier) {

                                var parsedValue = $parse(modifier.expression)($scope);
                                parsedValue = Number(parsedValue);

                                if (isNaN(parsedValue)) {

                                    debugLog('Payment modifier error', modifier.title, parsedValue);

                                    //throw Error('Invalid or non-numeric pricing modifier ' + modifier.title);
                                    return;
                                }

                                /////////////////////////////////////////

                                var parsedCondition = true;

                                if (modifier.condition && String(modifier.condition).length) {
                                    parsedCondition = $parse(modifier.condition)($scope);
                                }

                                //If the condition returns false then just stop here and go to the next modifier
                                if (!parsedCondition) {

                                    debugLog('inactive', modifier.title, modifier, $scope);

                                    return
                                }

                                /////////////////////////////////////////

                                var operator = '';
                                var operatingValue = '$' + parseFloat(parsedValue / 100).toFixed(2);

                                switch (modifier.operation) {
                                    case 'add':
                                        operator = '+';
                                        $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                        break;
                                    case 'subtract':
                                        operator = '-';
                                        $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                        break;
                                    case 'divide':
                                        operator = '÷';
                                        operatingValue = parsedValue;
                                        $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                        break;
                                    case 'multiply':
                                        operator = '×';
                                        operatingValue = parsedValue;
                                        $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                        break;
                                    case 'set':
                                        $scope.calculatedTotal = parsedValue;
                                        break;
                                }

                                //Let the front end know that this modification was made
                                $scope.modifications.push({
                                    title: modifier.title,
                                    total: $scope.calculatedTotal,
                                    description: operator + ' ' + operatingValue,
                                    operation: modifier.operation,
                                });
                            })


                            //If the modifiers change the price below 0 then change the total back to 0
                            if (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) {
                                $scope.calculatedTotal = 0;
                            }

                            //Add the calculated total to the rootscope

                            $timeout(function() {
                                //console.log('Set calculated total', $scope.calculatedTotal)
                                $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                            })

                        }

                        /**/
                    }],
                });
            } else {

                var amountDescription = 'Please enter an amount (' + String(paymentSettings.currency).toUpperCase() + ')';


                //Limits of amount
                var minimum = paymentSettings.minAmount;
                var maximum = paymentSettings.maxAmount;
                var defaultAmount = paymentSettings.amount;

                ///////////////////////////////////////////

                var paymentErrorMessage = 'Invalid amount';

                ///////////////////////////////////////////

                if (minimum) {
                    minimum = (parseInt(minimum) / 100);
                    paymentErrorMessage = 'Amount must be a number at least ' + $filter('currency')(minimum, '$');

                    amountDescription += 'Enter at least ' + $filter('currency')(minimum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();
                }

                if (maximum) {
                    maximum = (parseInt(maximum) / 100);
                    paymentErrorMessage = 'Amount must be a number less than ' + $filter('currency')(maximum, '$');

                    amountDescription += 'Enter up to ' + $filter('currency')(maximum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();;
                }


                if (minimum && maximum) {
                    amountDescription = 'Enter a numeric amount between ' + $filter('currency')(minimum) + ' and  ' + $filter('currency')(maximum) + ' ' + String(paymentSettings.currency).toUpperCase();;
                    paymentErrorMessage = 'Amount must be a number between ' + $filter('currency')(minimum) + ' and ' + $filter('currency')(maximum);
                }

                ///////////////////////////////////////////

                //Add the option for putting in a custom amount of money
                var fieldConfig = {
                    key: '_paymentAmount',
                    type: 'currency',
                    //defaultValue: 'Cade Embery',
                    templateOptions: {
                        type: 'text',
                        label: 'Amount',
                        description: amountDescription,
                        placeholder: '0.00',
                        required: true,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum,
                    },
                };

                if (minimum) {
                    fieldConfig.defaultValue = minimum;
                }

                paymentWrapperFields.push({
                    'template': '<hr/><h3>Payment Details</h3>'
                });
                paymentWrapperFields.push(fieldConfig);
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Setup debug card details
            var defaultCardName;
            var defaultCardNumber;
            var defaultCardExpMonth;
            var defaultCardExpYear;
            var defaultCardCVN;

            //If testing mode
            if ($scope.debugMode) {
                defaultCardName = 'John Citizen';
                defaultCardNumber = '4242424242424242';
                defaultCardExpMonth = '05';
                defaultCardExpYear = '2020';
                defaultCardCVN = '123';
            }

            //////////////////////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardName',
                type: 'input',
                defaultValue: defaultCardName,
                templateOptions: {
                    type: 'text',
                    label: 'Name on Card',
                    placeholder: 'eg. (John Smith)',
                    required: paymentSettings.required,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
            });




            /////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardNumber',
                type: 'input',
                defaultValue: defaultCardNumber,
                templateOptions: {
                    type: 'text',
                    label: 'Card Number',
                    placeholder: 'Card Number (No dashes or spaces)',
                    required: paymentSettings.required,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {

                        /////////////////////////////////////////////
                        var luhnChk = function(a) {
                            return function(c) {

                                if (!c) {
                                    return false;
                                }
                                for (var l = c.length, b = 1, s = 0, v; l;) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10
                            }
                        }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

                        /////////////////////////////////////////////

                        var value = $modelValue || $viewValue;
                        var valid = luhnChk(value);
                        return valid;
                    }
                },
            });

            paymentCardFields.push({
                className: 'row clearfix',
                fieldGroup: [{
                    key: '_paymentCardExpMonth',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Month',
                        placeholder: 'MM',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }, {
                    key: '_paymentCardExpYear',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Year',
                        placeholder: 'YYYY',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }, {
                    key: '_paymentCardCVN',
                    className: "col-xs-4 col-sm-2",
                    type: 'input',
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: 'text',
                        label: 'CVN',
                        placeholder: 'CVN',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }]
            });



            //////////////////////////////////////////////////////////

            //If the form is set to allow anonymous or has been set to ask for a specific receipt
            //email then prompt the submitter to type it in
            if (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) {
                paymentCardFields.push({
                    key: '_paymentEmail',
                    type: 'input',
                    templateOptions: {
                        type: 'email',
                        label: 'Receipt Email Address',
                        placeholder: 'Enter an email address for transaction receipt',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                });
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Create the credit card field group
            var cardDetailsField = {
                className: "payment-details",
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {

                    //If the calculated total is exactly 0
                    if ($formScope.vm.model.calculatedTotal === 0) {
                        return true;
                    }
                }
            }

            //////////////////////////////////////////////////////////

            if (paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length) {

                //Create a method selection widget
                var methodSelection = {
                    className: "payment-method-select",
                    //defaultValue:{},
                    //key:'methods',
                    // fieldGroup:cardDetailsField,
                    data: {
                        fields: [cardDetailsField],
                        settings: paymentSettings,
                    },
                    controller: ['$scope', function($scope) {

                        //Payment Settings on scope
                        $scope.settings = paymentSettings;

                        //Options
                        $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        });

                        //Add card at the start
                        $scope.methodOptions.unshift({
                            title: 'Pay with Card',
                            key: 'card',
                        });

                        ////////////////////////////////////////

                        if (!$scope.model._paymentMethod) {
                            $scope.model._paymentMethod = 'card';
                        }

                        //Select the first method by default
                        $scope.selected = {
                            method: $scope.methodOptions[0]
                        };

                        $scope.selectMethod = function(method) {
                            $scope.settings.showOptions = false;
                            $scope.selected.method = method;
                            $scope.model._paymentMethod = method.key;
                        }
                    }],
                    templateUrl: 'fluro-interaction-form/payment/payment-method.html',
                    hideExpression: function($viewValue, $modelValue, scope) {

                        //If the calculated total is exactly 0
                        if ($formScope.vm.model.calculatedTotal === 0) {
                            return true;
                        }
                    }
                };

                paymentWrapperFields.push(methodSelection);

            } else {
                //Push the card details
                paymentWrapperFields.push(cardDetailsField);
            }

            //////////////////////////////////////////////////////////

            // $scope.vm.modelFields = $scope.vm.modelFields.concat(paymentWrapperFields);

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields,

            });
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Wait for all async promises to resolve

        if (!$scope.promises.length) {
            // console.log('NO PROMISES');
            $scope.promisesResolved = true;
        } else {

            $scope.promisesResolved = false;

            $q.all($scope.promises).then(function() {
                // console.log('Promises have been resolved');
                $scope.promisesResolved = true;

                //updateErrorList();
                //Update the error list
                // $scope.errorList = getAllErrorFields($scope.vm.modelFields);
                // ////////console.log('All promises resolved', $scope.errorList);

                // _.each($scope.errorList, function(field) {
                //     ////////console.log('FIELD', field.templateOptions.label, field.formControl)
                // })

            });
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function getAllErrorFields(array) {


        // field.formControl.$invalid


        return _.chain(array)
            .map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {
                    //Get the error fields in a field group
                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.replicatedFields && field.data.replicatedFields))) {

                    var combined = [];
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields);
                    combined = _.compact(combined);

                    //Get the error fields in the combined extra data
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }



    /////////////////////////////////////////////////////////////////


    function submitInteraction() {


        //Sending
        $scope.vm.state = 'sending';
        // console.log('state.sending');

        var interactionKey = $scope.model.definitionName;
        var interactionDetails = angular.copy($scope.vm.model);


        //console.log('____________________________________');
        //console.log('SUBMIT', interactionKey, interactionDetails)

        /////////////////////////////////////////////////////////

        //Asking for Payment
        var requiresPayment;
        var allowsPayment;

        /////////////////////////////////////////////////////////


        var paymentConfiguration = $scope.model.paymentDetails;


        //Check if we have supplied payment details
        if (paymentConfiguration) {
            requiresPayment = paymentConfiguration.required;
            allowsPayment = paymentConfiguration.allow;
        }

        // ////////console.log('PAYMENT CONFIG', paymentConfiguration)
        // ////////console.log('PAYMENT ALLOWED REQUIRED', requiresPayment, allowsPayment)

        /////////////////////////////////////////////////////////

        //Check if we need a payment
        if (requiresPayment || allowsPayment) {

            //console.log('PAYMENT DETAILS', requiresPayment, allowsPayment)


            ////////////////////////////////////

            var paymentDetails = {};

            ////////////////////////////////////

            //Check if we can use alternative payment methods
            if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {

                var selectedMethod = interactionDetails._paymentMethod;
                //If the user chose an alternative payment
                if (selectedMethod && selectedMethod != 'card') {

                    //Mark which method we are using as an alternative method
                    paymentDetails.method = selectedMethod;

                    //console.log('ALTERNATIVE METHOD SO PROCESS REQUEST')
                    //////console.log('Have selected payment method other than card')
                    //Skip straight through to process the request
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Only stop here if we REQUIRE payment
            if (requiresPayment) {
                ////////console.log('TESTING REQUIRES PAYMENT', $formScope);
                //If payment modifiers have removed the need for charging a payment
                if (!$formScope.vm.model.calculatedTotal) {

                    //console.log('NO AMOUNT DUE SO PROCESS REQUEST')
                    //////console.log('Calculated total is ', 0);
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Get the payment integration 
            var paymentIntegration = $scope.integration;


            if (!paymentIntegration || !paymentIntegration.publicDetails) {

                // if (paymentConfiguration.required) {
                //     //////console.log('No payment integration was supplied for this interaction but payment is required');
                // } else {
                //     //////console.log('No payment integration was supplied for this interaction but payments are set to be allowed');
                // }

                alert('This form has not been configured properly. Please notify the administrator of this website immediately.')
                $scope.vm.state = 'ready';
                // console.log('state.ready');
                return;
            }

            /////////////////////////////////////////////////////////

            //var paymentDetails = {};

            //Ensure we tell the server which integration to use to process payment
            paymentDetails.integration = paymentIntegration._id;

            //Now get the required details for making the transaction
            switch (paymentIntegration.module) {
                case 'eway':

                    //console.log('EWAY PAYMENT')

                    if (!window.eCrypt) {
                        console.log('ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from eWay
                    //var liveUrl = 'https://api.ewaypayments.com/DirectPayment.json';
                    //var sandboxUrl = 'https://api.sandbox.ewaypayments.com/DirectPayment.json';

                    /////////////////////////////////////////////

                    //Get the Public Encryption Key
                    var key = paymentIntegration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var cardDetails = {};
                    cardDetails.name = interactionDetails._paymentCardName;
                    cardDetails.number = eCrypt.encryptValue(interactionDetails._paymentCardNumber, key);
                    cardDetails.cvc = eCrypt.encryptValue(interactionDetails._paymentCardCVN, key);

                    var expiryMonth = String(interactionDetails._paymentCardExpMonth);
                    var expiryYear = String(interactionDetails._paymentCardExpYear);

                    if (expiryMonth.length < 1) {
                        expiryMonth = '0' + expiryMonth;
                    }
                    cardDetails.exp_month = expiryMonth;
                    cardDetails.exp_year = expiryYear.slice(-2);

                    //Send encrypted details to the server
                    paymentDetails.details = cardDetails;

                    //Process the request
                    return processRequest();

                    break;
                case 'stripe':

                    //console.log('STRIPE PAYMENT')

                    if (!window.Stripe) {
                        console.log('ERROR: Stripe is selected for payment but the Stripe API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from Stripe
                    var liveKey = paymentIntegration.publicDetails.livePublicKey;
                    var sandboxKey = paymentIntegration.publicDetails.testPublicKey;

                    var key = liveKey;

                    /////////////////////////////////////////////

                    if (paymentIntegration.publicDetails.sandbox) {
                        key = sandboxKey;
                    }

                    /////////////////////////////////////////////

                    //Set the stripe key
                    Stripe.setPublishableKey(key);

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var cardDetails = {};
                    cardDetails.name = interactionDetails._paymentCardName;
                    cardDetails.number = interactionDetails._paymentCardNumber;
                    cardDetails.cvc = interactionDetails._paymentCardCVN;
                    cardDetails.exp_month = interactionDetails._paymentCardExpMonth;
                    cardDetails.exp_year = interactionDetails._paymentCardExpYear;

                    /////////////////////////////////////////////

                    Stripe.card.createToken(cardDetails, function(status, response) {

                        $timeout(function() {
                            // console.log('TESTING', status, response);
                            if (response.error) {
                                //Error creating token
                                // Notifications.error(response.error);
                                console.log('Stripe token error', response);
                                $scope.processErrorMessages = [response.error.message];




                                $scope.vm.state = 'error';
                                // console.log('state.error');


                            } else {
                                //Include the payment details
                                paymentDetails.details = response;
                                return processRequest();
                            }

                        })
                    });
                    break;
            }
        } else {
            return processRequest();
        }


        ///////////////////////////////////////////////////////////////////////

        function processRequest() {
            //console.log('PROCESS INTERACTION REQUEST')

            /////////////////////////////////////////////////////////

            //Delete payment details (we don't send these to fluro)
            delete interactionDetails._paymentCardCVN;
            delete interactionDetails._paymentCardExpMonth;
            delete interactionDetails._paymentCardExpYear;
            delete interactionDetails._paymentCardName;
            delete interactionDetails._paymentCardNumber;


            /////////////////////////////////////////////////////////

            //Log the request
            //////////console.log('Process request', interactionKey, interactionDetails, paymentDetails);

            /////////////////////////////////////////////////////////

            //Allow user specified payment
            if (interactionDetails._paymentAmount) {
                paymentDetails.amount = (parseFloat(interactionDetails._paymentAmount) * 100);
            }


            /////////////////////////////////////////////////////////

            //Add the transaction email  details
            if (interactionDetails._paymentEmail) {
                paymentDetails.email = interactionDetails._paymentEmail;
            }

            delete interactionDetails._paymentEmail;

            /////////////////////////////////////////////////////////

            //Explicitly pass through the account we want to use the definition for
            var definitionID = $scope.model._id;
            if (definitionID._id) {
                //ensure its just a simple string and not an object
                definitionID = definitionID._id;
            }

            /////////////////////////////////////////////////////////

            //Query string parameters
            var params = {
                definition: definitionID,
            }

            //Attempt to send information to interact endpoint
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);


            //////////////////////////////////

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////

            function submissionSuccess(res) {

                //////console.log('submission success');
                /**
                //TESTING
                $scope.vm.state = 'ready';
                return ////////console.log('RES TEST', res);
                /**/

                ///////////////////////////////////

                if ($scope.callback) {
                    $scope.callback(res);
                }

                ///////////////////////////////////

                //Reset
                if ($scope.vm.defaultModel) {
                    $scope.vm.model = angular.copy($scope.vm.defaultModel);
                } else {
                    $scope.vm.model = {};
                }


                $scope.vm.modelForm.$setPristine();
                $scope.vm.options.resetModel();

                //Reset the form scope
                $formScope = $scope;

                // $scope.vm.model = {}
                // $scope.vm.modelForm.$setPristine();
                // $scope.vm.options.resetModel();

                //Response from server incase we want to use it on the thank you page
                $scope.response = res;

                //Change state
                $scope.vm.state = 'complete';
                // console.log('state.complete');
            }

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////


            function submissionFail(res) {

                //////console.log('submission failed', res);
                ////////console.log('Interaction Failed', res);
                // Notifications.error(res.data);

                $scope.vm.state = 'error';
                // console.log('state.error');

                if (!res.data) {
                    return $scope.processErrorMessages = ['Error: ' + res];
                }

                if (res.data.error) {
                    if (res.data.error.message) {
                        return $scope.processErrorMessages = [res.error.message];
                    } else {
                        return $scope.processErrorMessages = [res.error];
                    }
                }

                if (res.data.errors) {
                    return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                        return error.message;
                    });
                }

                if (_.isArray(res.data)) {
                    return $scope.processErrorMessages = res.data;
                } else {
                    $scope.processErrorMessages = [res.data];
                }



                //$scope.vm.state = 'ready';
            }


        }
    }

});
////////////////////////////////////////////////////////////////////////

app.directive('postForm', function($compile) {
    return {
        restrict: 'E',
        //replace: true,
        scope: {
            model: '=ngModel',
            host: '=hostId',
            reply: '=?reply',
            thread: '=?thread',
            userStore: '=?user',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback:'=?callback',
        },
        transclude: true,
        controller: 'PostFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        },
    };
});


app.directive('recaptchaRender', function($window) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs, $ctrl) {

            //Check if we need to use recaptcha
            if ($scope.model.data && $scope.model.data.recaptcha) {

                //Recaptcha
                var element = $element[0];

                ////////////////////////////////////////////////

                var cancelWatch;

                //If recaptcha hasn't loaded yet wait for it to load
                if (window.grecaptcha) {
                    activateRecaptcha(window.grecaptcha)
                } else {

                    //Listen for when recaptcha exists
                    cancelWatch = $scope.$watch(function() {
                        return window.grecaptcha;
                    }, activateRecaptcha);
                }

                ////////////////////////////////////////////////

                function activateRecaptcha(recaptcha) {

                    console.log('Activate recaptcha!!');
                    if (cancelWatch) {
                        cancelWatch();
                    }

                    if (recaptcha) {
                        $scope.vm.recaptchaID = recaptcha.render(element, {
                            sitekey: '6LelOyUTAAAAADSACojokFPhb9AIzvrbGXyd-33z'
                        });
                    }
                }
            }

            ////////////////////////////////////////////////

        },
    };
});

app.controller('PostFormController', function($scope, $rootScope, $q, $http, Fluro, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {





    /////////////////////////////////////////////////////////////////

    if (!$scope.thread) {
        $scope.thread = [];
    }

    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }
    /////////////////////////////////////////////////////////////////
    //Attach unique ID of this forms scope
    // $scope.vm.formScopeID = $scope.$id;




    //Resolve promises by default
    $scope.promisesResolved = true;
    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';


    /////////////////////////////////////////////////////////////////

    //  $scope.$watch('vm.modelForm', function(form) {
    //     console.log('Form Validation', form);
    // }, true)

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    $scope.$watch('vm.modelForm.$invalid + vm.modelForm.$error', function() {


        // $scope.readyToSubmit = true;
        // return;

        //Interaction Form
        var interactionForm = $scope.vm.modelForm;

        if (!interactionForm) {
            // console.log('Invalid no form')
            return $scope.readyToSubmit = false;
        }

        if (interactionForm.$invalid) {
            // console.log('Invalid because its invalid', interactionForm);
            return $scope.readyToSubmit = false;
        }

        if (interactionForm.$error) {

            // console.log('Has an error', interactionForm.$error);

            if (interactionForm.$error.required && interactionForm.$error.required.length) {
                // console.log('required input not provided');
                return $scope.readyToSubmit = false;
            }

            if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {
                // console.log('valid input not provided');
                return $scope.readyToSubmit = false;
            }
        }

        // console.log('Form should be good to go')

        //It all worked so set to true
        $scope.readyToSubmit = true;

    }, true)

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    /*
    formlyValidationMessages.messages.required = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is required';
    }
    */

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function resetCaptcha() {


        //Recaptcha ID
        var recaptchaID = $scope.vm.recaptchaID;

        console.log('Reset Captcha', recaptchaID);

        if (window.grecaptcha && recaptchaID) {
            window.grecaptcha.reset(recaptchaID);
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();

        //Reset the captcha
        resetCaptcha();

        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';

        //Reset after state change
        console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    // /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    $scope.$watch('model', function(newData, oldData) {

        // console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'post') {
            return; //$scope.model = {};
        }

        /////////////////////////////////////////////////////////////////

        //check if we have the correct permissions
        // checkPermissions();

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitPost;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        /////////////////////////////////////////////////////////////////
        /**/
        //Email Address
        // // if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
        // var newField = {
        //     key: 'body',
        //     type: 'textarea',
        //     templateOptions: {
        //         // type: 'email',
        //         label: 'Body',
        //         placeholder: 'Enter your comment here',
        //         required: true,
        //         // required: interactionFormSettings.requireEmail,
        //         onBlur: 'to.focused=false',
        //         onFocus: 'to.focused=true',
        //         rows: 4,
        //         cols: 15
        //     },
        // }

        //Push the body
        // $scope.vm.modelFields.push(newField);
        // }
        /**/

        /////////////////////////////////////////////////////////////////

        //Push the extra data object
        // var dataObject = {
        //     key: 'data',
        //     type:'nested',
        //     fieldGroup: [],
        //     // templateOptions:{
        //     //     baseDefaultValue:{}
        //     // }
        // }


        // $scope.vm.modelFields.push(dataObject);

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {

            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;

            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            } else if (fieldDefinition.description && fieldDefinition.description.length) {
                templateOptions.placeholder = fieldDefinition.description;
            } else {
                templateOptions.placeholder = fieldDefinition.title;
            }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';

            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':
                case 'value-select':
                    //Detour here
                    newField.type = 'button-select';
                    break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive;
                    break;
            }


            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options

            switch (fieldDefinition.type) {

                case 'reference':
                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if(fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute:key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom') {
                switch (fieldDefinition.type) {
                    case 'boolean':
                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        //Only do this if its an integer cos iOS SUCKS!
                        if (fieldDefinition.type == 'integer') {
                            // console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            //Force numeric keyboard
                            newField.ngModelAttrs.customAttrpattern = {
                                attribute: 'pattern',
                            }

                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard
                            templateOptions.customAttrpattern = "[0-9]*";
                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    if (!value) {
                        return true;
                    }


                    var valid = FluroValidate.validate(value, fieldDefinition);

                    if (!valid) {
                        //console.log('Check validation', fieldDefinition.title, value)
                    }
                    return valid;
                }
            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    replicatedFields: []
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;


                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }

                        // console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);
                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }
            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field


            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {

                //Include Expression Properties
                // if (!newField.expressionProperties) {
                //     newField.expressionProperties = {};
                // }

                //////////////////////////////////////////

                //Add the hide expression if added through another method
                if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                    fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                }

                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.values(fieldDefinition.expressions).join('+');

                //////////////////////////////////////////

                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {
                        //Return the result
                        return $parse(allExpressions)(scope);
                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        //Parse the expression on the root scope vm
                        if (!scope.interaction) {
                            scope.interaction = $scope.vm.model;
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(scope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = retrievedValue;
                                    break;
                                case 'hide':
                                    return field.hide = retrievedValue;
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }


                //Replace expression
                //var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');



                /*
                    //Add the expression properties
                    newField.expressionProperties[key] = function($viewValue, $modelValue, scope) {


                        //Replace expression
                        var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');

           


                       // var retrievedValue = $parse(replaceExpression)($scope);
                        var retrievedValue = _.get($scope, replaceExpression);

                         console.log('Testing retrieved value from GET', retrievedValue, replaceExpression);

                        ////////////////////////////////////////

                        

                        return retrievedValue;
                    }
                    /**/
                //});
            }

            /////////////////////////////

            if (fieldDefinition.hideExpression) {
                newField.hideExpression = fieldDefinition.hideExpression;
            }

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });
    });

    /////////////////////////////////////////////////////////////////

    function getAllErrorFields(array) {
        return _.chain(array).map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {

                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.replicatedFields && field.data.replicatedFields))) {
                    var combined = [];
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields);
                    combined = _.compact(combined);
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }

    /////////////////////////////////////////////////////////////////

    $scope.$watch('vm.modelFields', function(fields) {
        //console.log('Interaction Fields changed')
        $scope.errorList = getAllErrorFields(fields);

        //console.log('Error List', $scope.errorList);
    }, true)



    /////////////////////////////////////////////////////////////////

    //Submit the
    function submitPost() {

        //Sending
        $scope.vm.state = 'sending';

        var submissionKey = $scope.model.definitionName;
        var submissionModel = {
            data: angular.copy($scope.vm.model)
        }

        /////////////////////////////////////////////////////////

        var hostID = $scope.host;

        /////////////////////////////////////////////////////////

        //If its a reply then mark it as such
        if ($scope.reply) {
            submissionModel.reply = $scope.reply;
        }

        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////

        //If we have a recaptcha id present then use it
        if (typeof $scope.vm.recaptchaID !== 'undefined') {
            var response = window.grecaptcha.getResponse($scope.vm.recaptchaID);
            submissionModel['g-recaptcha-response'] = response;
        }

        /////////////////////////////////////////////////////////


        var request;

        //If a user store has been provided
        if ($scope.userStore) {

            //Get the required config
            $scope.userStore.config().then(function(config) {


                var postURL = Fluro.apiURL + '/post/' + hostID + '/' + submissionKey;

                //Make the request using the user stores configuration
                request = $http.post(postURL, submissionModel, config)

                //When the promise results fire the callbacks
                request.then(function(res) {
                    return submissionSuccess(res.data);
                }, function(res) {
                    return submissionFail(res.data);
                })
            });

        } else {

            //Attempt to send information to post endpoint
            request = FluroContent.endpoint('post/' + hostID + '/' + submissionKey).save(submissionModel).$promise;

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)
        }

        //////////////////////////////////        

        function submissionSuccess(res) {
            //Reset
            if ($scope.vm.defaultModel) {
                $scope.vm.model = angular.copy($scope.vm.defaultModel);
            } else {
                $scope.vm.model = {
                    data: {}
                };
            }
            $scope.vm.modelForm.$setPristine();
            $scope.vm.options.resetModel();

            //Reset the captcha
            resetCaptcha();

            // $scope.vm.model = {}
            // $scope.vm.modelForm.$setPristine();
            // $scope.vm.options.resetModel();

            //Response from server incase we want to use it on the thank you page
            $scope.response = res;

            //If there is a thread push this into it
            if ($scope.thread) {
                $scope.thread.push(res);
            }

            //Change state
            $scope.vm.state = 'complete';
        }

        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////
        //////////////////////////////////

        function submissionFail(res) {
            $scope.vm.state = 'error';

            if (!res.data) {
                return $scope.processErrorMessages = ['Error: ' + res];
            }

            if (res.data.error) {
                if (res.data.error.message) {
                    return $scope.processErrorMessages = [res.error.message];
                } else {
                    return $scope.processErrorMessages = [res.error];
                }
            }

            if (res.data.errors) {
                return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                    return error.message;
                });
            }

            if (_.isArray(res.data)) {
                return $scope.processErrorMessages = res.data;
            } else {
                $scope.processErrorMessages = [res.data];
            }
        }
    }

});
app.directive('postThread', function() {
    return {
        restrict: 'E',
        transclude:true,
        scope:{
          definitionName:"=?type",
          host:"=?hostId",
          thread:"=?thread",
        },
        // template:'<div class="post-thread" ng-transclude></div>',
        link:function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function (clone, $scope) {
                $element.replaceWith(clone); // <-- will transclude it's own scope
            });
        },
        controller:function($scope, $filter, $rootScope, FluroContent) {

        	$scope.outer = $scope.$parent;

            if(!$scope.thread) {
                $scope.thread = [];
            }

            //////////////////////////////////////////////////

            var hostID;
            var definitionName;
            
            //////////////////////////////////////////////////

            function reloadThread() {
                console.log('Reload thread', 'post.'+hostID+'.' + definitionName);
                return FluroContent.endpoint('post/' + hostID + '/' + definitionName, true, true)
                .query()
                .$promise
                .then(threadLoaded, threadError);
            }

            //////////////////////////////////////////////////

            function threadLoaded(res) {
                console.log('Thread reloaded', res);
                //All the posts
                var allPosts = res;

                //Break it up into nested threads
                $scope.thread = _.chain(res)
                .map(function(post) {
                    //Find all replies to this post
                    post.thread = _.filter(allPosts, function(sub) {
                        return (sub.reply == post._id);
                    });
                    //If it's a top level post then send it back
                    if(!post.reply) {
                        return post;
                    }
                })
                .compact()
                .value();
            }

            //////////////////////////////////////////////////

            function threadError(err) {
                console.log('Thread Error', err);
                $scope.thread = []
            }

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            //Watch for when the host and the definition name is changed/set
        	$scope.$watch('host + definitionName', function() {

                hostID = $scope.host;
                definitionName = $scope.definitionName;

                if(!hostID || !definitionName) {
                    return;
                } 

                ////////////////////////////////////////////////////////////////////////

                //Get the thread refresh event
                var threadRefreshEvent = 'post.'+hostID+'.' + definitionName;

                console.log('LISTENING FOR', threadRefreshEvent);
                //When it's broadcast
                $rootScope.$on(threadRefreshEvent, reloadThread);

                ////////////////////////////////////////////////////////////////////////
                
                return reloadThread();

                ////////////////////////////////////////////////////////////////////////

                
        	})
        },
    }
});
/**/
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'nested',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/nested/fluro-nested.html',
        controller: 'FluroInteractionNestedController',
    });

    //console.log('fluro-nested')

});

//////////////////////////////////////////////////////////

app.controller('FluroInteractionNestedController', function($scope) {

    //console.log('FluroInteractionNestedController', $scope);
    // $scope.parentForm = $scope.$parent.form

    //Definition
    var def = $scope.to.definition;

    ////////////////////////////////////

    var minimum = def.minimum;
    var maximum = def.maximum;

    ////////////////////////////////////

    $scope.$watch('model[options.key]', function(model) {
        if (!model) {
            ////console.log('Reset Model cos no value!')
            resetDefaultValue();
        }
    });

    ////////////////////////////////////


    function resetDefaultValue() {
        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        if(!$scope.model) {
            //console.log('NO RESET Reset Model Values', $scope.options.key, defaultValue);
        }
        $scope.model[$scope.options.key] = defaultValue;
    }

    ////////////////////////////////////

    //Listen for a reset event
    $scope.$on('form-reset', resetDefaultValue);

    ////////////////////////////////////

    $scope.addAnother = function() {

        //console.log('Add another')
        $scope.model[$scope.options.key].push({});
    }

    ////////////////////////////////////

    $scope.canRemove = function() {
        if (minimum) {
            if ($scope.model[$scope.options.key].length > minimum) {
                return true;
            }
        } else {
            return true;
        }
    }

    ////////////////////////////////////

    $scope.canAdd = function() {
        if (maximum) {
            if ($scope.model[$scope.options.key].length < maximum) {
                return true;
            }
        } else {
            return true;
        }
    }


    $scope.remove = function($index) {
        //console.log('Remove entry')
        $scope.model[$scope.options.key].splice($index, 1);
        $scope.options.data.replicatedFields.splice($index, 1);
    }

    $scope.copyFields = function() {

        var copiedFields = angular.copy($scope.options.data.fields);
        $scope.options.data.replicatedFields.push(copiedFields);

        return copiedFields;
    }

    $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        $scope.options.data.replicatedFields.push(copiedFields);
        return copiedFields;
    }
});
/**/
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'search-select',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/search-select/fluro-search-select.html',
        controller: 'FluroSearchSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    ////console.log('FluroSearchSelectController formlyconfig')

});

app.controller('FluroSearchSelectController', function($scope, $http, Fluro, $filter, FluroValidate) {

        ////console.log('FluroSearchSelectController')

    /////////////////////////////////////////////////////////////////////////

    //Search Object
    $scope.search = {};

    //Proposed value
    $scope.proposed = {}

    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    //Selection Object
    $scope.selection = {};

    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;


    /////////////////////////////////////////////////////////////////////////

    if (!definition.params) {
        definition.params = {};
    }

    /////////////////////////////////////////////////////////////////////////

    var restrictType = definition.params.restrictType;

    //Add maximum search results
    var searchLimit = definition.params.searchLimit;
    if (!searchLimit) {
        searchLimit = 10;
    }

    /////////////////////////////////////////////////////////////////////////

    //////console.log('DEFINITION', definition);

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if (!minimum) {
        minimum = 0;
    }

    if (!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);

    if($scope.multiple) {
        if($scope.model[opts.key] && _.isArray($scope.model[opts.key])) {
            $scope.selection.values = angular.copy($scope.model[opts.key]);
        }
    } else {
        if($scope.model[opts.key]) {
            $scope.selection.value = $scope.model[opts.key];
        }
    }

    /////////////////////////////////////////////////////////////////////////


    $scope.canAddMore = function() {

        if (!maximum) {
            return true;
        }

        if ($scope.multiple) {
            return ($scope.selection.values.length < maximum);
        } else {
            if (!$scope.selection.value) {
                return true;
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////

    $scope.contains = function(value) {
        if ($scope.multiple) {
            //Check if the values are selected
            return _.includes($scope.selection.values, value);
        } else {
            return $scope.selection.value == value;
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.$watch('model', function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {

            var modelValue;


            //If there is properties in the FORM model
            if (_.keys(newModelValue).length) {

                //Get the model for this particular field
                modelValue = newModelValue[opts.key];


               

                if ($scope.multiple) {
                    if (modelValue && _.isArray(modelValue)) {
                        $scope.selection.values = angular.copy(modelValue);
                    } else {
                        $scope.selection.values = [];
                    }
                } else {
                    $scope.selection.value = angular.copy(modelValue);
                }
            }
        }
    }, true);

    /////////////////////////////////////////////////////////////////////////

    function setModel() {

        if ($scope.multiple) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        } else {
            $scope.model[opts.key] = angular.copy($scope.selection.value);
        }

        if ($scope.fc) {
            $scope.fc.$setTouched();
        }


        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {


        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        if ($scope.fc) {

            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.select = function(value) {

        //////console.log('SELECT', value)

        if ($scope.multiple) {
            if (!$scope.canAddMore()) {
                return;
            }
            $scope.selection.values.push(value);


        } else {
            $scope.selection.value = value;

        }

        //Clear proposed item
        $scope.proposed = {};

        //Set the model
        setModel();


    }

    /////////////////////////////////////////////////////////

    $scope.retrieveReferenceOptions = function(val) {


        ////////////////////////

        //Create Search Url
        var searchUrl = Fluro.apiURL + '/content';
        if (restrictType) {
            searchUrl += '/' + restrictType;
        }
        searchUrl += '/search';

        ////////////////////////

        return $http.get(searchUrl + '/' + val, {
            ignoreLoadingBar: true,
            params: {
                limit: searchLimit,
            }
        }).then(function(response) {

            //Got the results
            var results = response.data;

            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selection.values, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, []);

        });

    }

    ////////////////////////////////////////////////////////////

    $scope.getValueLabel = function(value) {
        if(definition.options && definition.options.length) {
            var match = _.find(definition.options, {value:value});
            if(match && match.name) {
                return match.name;
            }
        }

        return value;
    }

    ////////////////////////////////////////////////////////////

    $scope.retrieveValueOptions = function(val) {

        if (definition.options && definition.options.length) {

            var options = _.reduce(definition.options, function(results, item) {

                var exists;

                if ($scope.multiple) {
                    exists = _.includes($scope.selection.values, item.value);
                } else {
                    exists = $scope.selection.value == item.value;
                }

                if (!exists) {
                    results.push({
                        name:item.name,
                        value:item.value,
                    });
                }

                return results;
            }, []);


            return $filter('filter')(options, val);

        } else if (definition.allowedValues && definition.allowedValues.length) {

            var options = _.reduce(definition.allowedValues, function(results, allowedValue) {

                var exists;

                if ($scope.multiple) {
                    exists = _.includes($scope.selection.values, allowedValue);
                } else {
                    exists = $scope.selection.value == allowedValue;
                }

                if (!exists) {
                    results.push({
                        name:allowedValue,
                        value:allowedValue,
                    });
                }

                return results;
            }, []);

            ////console.log('Options', options)

            return $filter('filter')(options, val);
        }
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.deselect = function(value) {
        if ($scope.multiple) {
            _.pull($scope.selection.values, value);
        } else {
            delete $scope.selection.value;
        }

        setModel();
    }

    /////////////////////////////////////////////////////////////////////////

    $scope.toggle = function(reference) {
        if ($scope.contains(reference)) {
            $scope.deselect(reference);
        } else {
            $scope.select(reference);
        }

        //Update model
        //setModel();
    }

})
app.controller('FluroInteractionFormUploadController', function($scope, $http, FluroValidate) {

    $scope.ctrl = {
        fileArray: [],
    };

    /////////////////////////////////////////////////////////////////////////

    var to = $scope.to;
    var opts = $scope.options;

    /////////////////////////////////////////////////////////////////////////

    //Get the definition
    var definition = $scope.to.definition;

    //Minimum and maximum
    var minimum = definition.minimum;
    var maximum = definition.maximum;

    if (!minimum) {
        minimum = 0;
    }

    if (!maximum) {
        maximim = 0;
    }

    $scope.multiple = (maximum != 1);

    //////////////////////////////

    var definition = _.get($scope, 'to.definition');

    /////////////////////////////////////////////////////////////////////////

    $scope.remove = function(item) {
        _.pull($scope.ctrl.fileArray, item);

        if (!$scope.ctrl.fileArray || !$scope.ctrl.fileArray.length) {
            var fileElement = angular.element('#' + $scope.options.id + ' input');
            angular.element(fileElement).val(null);
        }

        $scope.ctrl.refresh();
    }

    //////////////////////////////

    //Helper function to refresh and check validity
    $scope.ctrl.refresh = function() {

        /////////////////////////////////////

        var maximum = definition.maximum;

        /////////////////////////////////////

        if (maximum == 1) {


            if ($scope.ctrl.fileArray.length) {

                var firstFile = $scope.ctrl.fileArray[0];
                if (firstFile.state == 'complete') {
                    $scope.model[$scope.options.key] = firstFile.attachmentID;
                } else {
                    $scope.model[$scope.options.key] = firstFile.state;
                }
            } else {
                $scope.model[$scope.options.key] = null;
            }
        } else {

            $scope.model[$scope.options.key] = _.map($scope.ctrl.fileArray, function(fileItem) {

                if (fileItem.state == 'complete') {
                    return fileItem.attachmentID;
                }
                return fileItem.state;
            })
        }


        if ($scope.fc) {
            $scope.fc.$setTouched();
        }

        checkValidity();
    }

    /////////////////////////////////////////////////////////////////////////

    if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
        $scope.$watch(function() {
            return $scope.to.required;
        }, function(newValue) {
            checkValidity();
        });
    }

    /////////////////////////////////////////////////////////////////////////

    if ($scope.to.required) {
        var unwatchFormControl = $scope.$watch('fc', function(newValue) {
            if (!newValue) {
                return;
            }
            checkValidity();
            unwatchFormControl();
        });
    }
    
    //////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////

    function checkValidity() {

        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        //Check if multiple
        if ($scope.multiple) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        // ///////////////////////////////////////////////////

        // var values = $scope.model[opts.key];
        // var hasErrors = _.includes(values, 'error');
        // var hasProcessing = _.includes(values, 'processing');

        // if (hasErrors || hasProcessing) {
        //     validInput = false;
        // }

        ///////////////////////////////////////////////////


        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }



        /**
    function checkValidity() {



        var validRequired;
        var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

        /////////////////////////////////////

        var values = $scope.model[$scope.options.key];

        var hasErrors = _.some(values, 'error');
        var hasProcessing = _.some(values, 'processing');

        if (hasErrors || hasProcessing) {
            validInput = false;
        }

        /////////////////////////////////////

        //Check if multiple
        if (definition.maximum != 1) {
            if ($scope.to.required) {
                validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
            }
        } else {
            if ($scope.to.required) {
                if ($scope.model[opts.key]) {
                    validRequired = true;
                }
            }
        }

        /////////////////////////////////////

        if ($scope.fc) {
            $scope.fc.$setValidity('required', validRequired);
            $scope.fc.$setValidity('validInput', validInput);
        }
    }
    /**/

    //////////////////////////////

    $scope.filesize = function(fileSizeInBytes) {
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };
});

/////////////////////////////////////

app.directive("fluroFileInput", function(Fluro, $http) {
    return {
        scope: {
            model: "=fileArray",
            definition: '=',
            callback: '=',
        },
        link: function($scope, $element, $attrs) {



            if (!$scope.model) {
                $scope.model = [];
            }

            ///////////////////////////////////////

            $element.bind('change', filesSelected)

            ///////////////////////////////////////

            function filesSelected(changeEvent) {



                //Get files that were selected
                var files = changeEvent.target.files;
                var realmID = _.get($scope.definition, 'params.realm');
                var uploadURL = Fluro.apiURL + '/file/attach/' + realmID;

                ///////////////////////////////

                // $scope.model = _.map(files, function(file) {
                //     return {
                //         lastModified: file.lastModified,
                //         lastModifiedDate: file.lastModifiedDate,
                //         name: file.name,
                //         size: file.size,
                //         type: file.type,
                //     }

                // });


                // if (datas.length === files.length) {
                //         scope.$apply(function() {
                //             scope.fileread = datas;
                //         });
                //     }

                ///////////////////////////////

                $scope.model = _.map(files, function(file) {

                    var output = {}

                    ////////////////////////////////

                    output.state = 'processing';

                    ////////////////////////////////

                    //Create the form data
                    var formData = new FormData();
                    formData.append('file', file);

                    ////////////////////////////////

                    var request = $http({
                        url: uploadURL,
                        method: "POST",
                        data: formData,
                        headers: {
                            'Content-Type': undefined
                        },
                        uploadEventHandlers: {
                            progress: function(e) {
                                $scope.callback();
                                if (e.lengthComputable) {
                                    output.progress = Math.round((e.loaded / e.total) * 100);
                                }
                            }
                        },
                    });

                    ////////////////////////////////

                    output.file = file;
                    output.request = request;

                    ////////////////////////////////

                    request.then(function(res) {
                        output.state = 'complete';
                        output.attachmentID = res.data._id;
                        $scope.callback();
                    }, function(err) {
                        output.state = 'error';

                        $scope.callback();
                    })

                    ////////////////////////////////

                    return output;
                })

                ///////////////////////////////

                $scope.callback();

            }

            ///////////////////////////////////////


            /**
            element.bind("change", function(changeEvent) {

                // console.log('FILE', changeEvent);

                var readers = [];
                var files = changeEvent.target.files;
                var datas = [];

                /////////////////////////////////////////

                if (!files.length) {
                    scope.$apply(function() {
                        scope.fileread = [];
                    });
                    return;
                }


                for (var i = 0; i < files.length; i++) {
                    readers[i] = new FileReader();
                    readers[i].index = i;

                    readers[i].onload = function(loadEvent) {

                        var index = loadEvent.target.index;

                        datas.push({
                            lastModified: files[index].lastModified,
                            lastModifiedDate: files[index].lastModifiedDate,
                            name: files[index].name,
                            size: files[index].size,
                            type: files[index].type,
                            data: loadEvent.target.result
                        });

                        if (datas.length === files.length) {
                            scope.$apply(function() {
                                scope.fileread = datas;
                            });
                        }

                    };
                    readers[i].readAsDataURL(files[i]);
                }
            });
            /**/

        }
    }
});
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'value',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/value/value.html',
        //controller: 'FluroInteractionDobSelectController',
        wrapper: ['bootstrapHasError'],
    });

        ////console.log('value.js formlyconfig')


});

app.directive('menuManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel'
        },
        // Replace the div with our template
        templateUrl: 'fluro-menu-manager/fluro-menu-manager.html',
        controller: 'MenuManagerController',
    };
});


app.controller('MenuManagerController', function($scope, SiteTools, $rootScope, ObjectSelection) {

    $scope.selection = new ObjectSelection();
    $scope.selection.multiple = false;


    if (!$scope.model) {
        $scope.model = [];
    } else {
        $scope.selection.select($scope.model[0]);
    }

    //////////////////////////////////////////

    $scope.addAllMenuItems = function(array) {

        //Get flattened site routes
        var siteRoutes = SiteTools.getFlattenedRoutes($rootScope.site.routes);


        _.each(siteRoutes, function(route) {

            var newMenuItem = {
                title:route.title,
                items:[],
                state:route.state,
                type:'state',
            };

            array.push(newMenuItem);

        })
    }

    //////////////////////////////////////////

    $scope.availablePages = function() {

        if($rootScope.site) {

            return SiteTools.getFlattenedRoutes($rootScope.site.routes);
            //return $rootScope.site.routes;
        }
    }

    //////////////////////////////////////////

    $scope.removeMenu = function() {
        _.pull($scope.model, $scope.selection.item);
        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.addMenuItem = function(array) {
        var newMenuItem = {
            title:'New Menu Item',
            items:[],
            type:'state',
        };

        array.push(newMenuItem);
    }

    //////////////////////////////////////////

    $scope.addMenu = function() {

        var newMenu = {
            title:'New Menu',
            items:[],
        };

        $scope.model.push(newMenu);
        $scope.selection.select(newMenu);
    }

})
app.service('FluroMenuService', function($rootScope) {

    var controller ={}
    
    //////////////////////////////////////////

    controller.get = function(string, id) {
        
    	var menu;
    	var result;

        if($rootScope.site) {
           menu = _.find($rootScope.site.menus, {key:string});
        }

        if(menu && id) {
        	result = _.find(menu.items, {id:id});
        } else {
        	result = menu;
        }

        return result;
    }

    return controller;

})
app.directive('routeManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-route-manager/fluro-route-manager.html',
        controller: 'RouteManagerController',
    };
});


app.controller('RouteManagerController', function($scope, BuildSelectionService) {

    $scope.settings = {};
/*
    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    //Now we have nested routes we need to flatten
    function getFlattenedFolders(array) {
        return _.chain(array)
        .reduce(function(result, route, key) {

            if (route.type == 'folder') {
                result.push(route);
            }

            if (route.routes && route.routes.length) {
                //Recursive search
                var folders = getFlattenedFolders(route.routes);

                if (folders.length) {
                    result.push(folders)
                }
            }

            return result;

        }, [])
        .flatten()
        .compact()
        .value();

    }
    /**/



    // $scope.$watch(function() {

    //     var item = $scope.selection.item;


    //     return _.includes($scope.model, item);
    // }, function(breakout) {
    //     $scope.breakout = breakout;
    // })

    $scope.containsDot = function(string) {
        return _.contains(string, '.');
    }


    $scope.hasSlug = function(route) {
        if(route) {
            //Check if the path has a slug or an id
            return (_.includes(route.url, ':slug') || _.includes(route.url, ':_id'));
        }
    }




    $scope.hasIssue = function(route) {
        if(!route.state || !route.state.length) {
            return 'No state name defined';
        } else {

             var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model);

            var dupe =_.some(flattenedRoutes, function(r) {
                return (route.state == r.state && route != r);
            }) 

            if(dupe) {
                return 'Conflicting state name with existing page (' + route.state + ')';
            }
        }

        if(!route.url || !route.url.length) {
            return 'No URL path defined';
        }

        if($scope.hasSlug(route)) {
            if(!route.seo) {
                return 'No slug title';
            }

            if(!route.seo.slugTitle || !route.seo.slugTitle.length) {
                return 'No slug title';
            }

            if(!route.seo.slugDescription || !route.seo.slugDescription.length) {
                return 'No slug description';
            }

            if(!route.seo.slugImage || !route.seo.slugImage.length) {
                return 'No slug share image';
            }
        }

        if(!route.sections || !route.sections.length) {
            return 'No sections created for route';
        }

        return false;
    }


    if (!$scope.model) {
        $scope.model = [];
    } else {

        //Flattened routes
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model);

        //First route
        var firstRoute = _.find(flattenedRoutes, function(route) {
            return (!route.type || route.type != 'folder');
        });

        //Select first route
        $scope.selection.select(firstRoute);
    }

    // _.each($scope.model, function(route) {
    //     if(!route.routes) {
    //         route.routes = [];
    //     }
    // })

    //////////////////////////////////////////
    /*
    $scope.removeFolder = function(item) {

        //Get the flattened routes
        var folders = getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {
            var found = _.contains(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }
    }
    */
   
    //////////////////////////////////////////

    $scope.removeRoute = function() {

        //Get the item
        var item = $scope.selection.item;

        //Get the flattened routes
        var folders = BuildSelectionService.getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {

            var found = _.contains(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }


        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.duplicate = function(route, parent) {

        if (!route) {
            return;
        }

        var index = $scope.model.indexOf(route);



        var newRoute = angular.copy(route);
        newRoute.state = '';
        newRoute.title = newRoute.title + ' copy';
        newRoute.url = '';

        if (index != -1) {
            $scope.model.splice(index + 1, 0, newRoute);
        } else {
            $scope.model.push(newRoute);
        }


        $scope.selection.select(newRoute);
    }


    $scope.editFolderName = function(node, event) {
        event.preventDefault();
        event.stopPropagation();

        $scope.$targetFolder = node;
    }

    //////////////////////////////////////////

    $scope.addRoute = function() {

        var newRoute = {
            title: 'New page',
            //includeInMenu:true,
        };

        $scope.model.push(newRoute);
        $scope.selection.select(newRoute);
    }

    //////////////////////////////////////////

    $scope.addFolder = function() {

        var newFolder = {
            title: 'New Folder',
            type: 'folder',
            routes: []
                //includeInMenu:true,
        };

        $scope.model.push(newFolder);
    }

})
app.directive('routeSectionManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        templateUrl: 'fluro-route-section-manager/fluro-route-section-manager.html',
        controller: 'RouteSectionManager',
    };
});




app.controller('RouteSectionManager', function($scope, $rootScope, $modal, ObjectSelection, FluroContent) {

    //Create the template array if doesnt exist
    if(!$rootScope.site.templates) {
        $rootScope.site.templates = [];
    }

    $scope.getTemplateSource = function(key) {
        return _.find($rootScope.site.templates, {key:key});
    }


   



    /*
     $scope.aceLoaded = function(_editor){

        console.log('SET OPTIONS', _editor);
        _editor.setOptions({
            useWrapMode : false, 
            showGutter: true, 
            theme:'tomorrow_night_eighties', 
            mode: 'html', 
            enableBasicAutocompletion: true, 
            enableSnippets: true,
            enableLiveAutocompletion: true
        })
    }
*/
    $scope.selectExisting = function() {

        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            templateUrl: 'fluro-route-section-manager/section-modal.html',
            controller: 'SectionModalController',
            size: 'md',
            resolve:{
                templates:function(FluroContent, $q) {

                    var deferred = $q.defer();
                    FluroContent.endpoint('content').query({includePublic:true, searchInheritable:true, type:'siteblock'}).$promise.then(deferred.resolve, function(err) {
                        return deferred.resolve([]);
                    });

                    return deferred.promise;
                }
            }
        });

        modalInstance.result.then(function(sections) {

            if (sections && sections.length) {
                console.log('Modal Selected', sections)
                    //$scope.selected = selectedItem;

                _.each(sections, function(section) {

                    var copy = angular.copy(section);
                    if(section._id) {
                        copy.exportID = section._id;
                    }

                    $scope.model.push(copy);
                });

                $scope.selection.select($scope.model[$scope.model.length - 1]);
            }
        });
    }

    /////////////////////////////////////////////////////

    $scope.exportTemplate = function(section) {
        if(section.title && section.title.length && section.html && section.html.length) {
            //Create new template
            var newTemplate = {
                title: section.title,
                key:_.camelCase(section.title),
                html:section.html,
            };

            //Push the html into a new template
            $rootScope.site.templates.push(newTemplate);

            //Use the template for this section
            section.template = newTemplate.key;
        }
    }

    /////////////////////////////////////////////////////

    $scope.replicateTemplate = function(section) {

        //Check if it's got a template
        if(section.template) {

            //Get the actual template object
            var template = $scope.getTemplateSource(section.template);

            //If we have found one
            if(template) {
                //copy the html and append it to the section
                section.html = template.html;

                //Remove the link to the template
                section.template = null;
            }
        }
    }


    /////////////////////////////////////////////////////

    $scope.$watch('model', function(model) {
        if (!model) {
            $scope.model = [];
        } else {
            $scope.model = model;
        }

        var selection = new ObjectSelection();
        selection.multiple = false;
        $scope.selection = selection;

        ////////////////////////////////////

        
      



        /*
        
        ////////////////////////////////////
        $scope.addExistingSection = function(section) {
            var copy = angular.copy(section);
            $scope.model.push(copy);
            $scope.selection.select(copy);
            $scope.dynamic.showExisting = false;
        }

       $scope.dynamic = {}

        ////////////////////////////////////

        //Now we have nested routes we need to flatten
        function getFlattenedRoutes(array) {
            return _.chain(array).map(function(route) {
                    if (route.type == 'folder') {
                        return getFlattenedRoutes(route.routes);
                    } else {
                        return route;
                    }
                })
                .flatten()
                .compact()
                .value();
        }

        ////////////////////////////////////
        $scope.$watch(function() {
            if ($rootScope.site && $rootScope.site.routes) {
                return $rootScope.site.routes;
            }
        }, function() {

            var routes = getFlattenedRoutes($rootScope.site.routes);

            if (routes) {
                $scope.existingSections = _.chain(routes).map(function(route) {
                        if (route.sections && route.sections.length) {
                            return {
                                pageName: route.title,
                                sections: route.sections
                            };
                        }
                    })
                    .compact()
                    .value();
            }
        })
        */
       
        $scope.addSection = function() {

            var newSection = {};
            $scope.model.push(newSection);
            $scope.selection.select(newSection);
            //$scope.dynamic.showExisting = false;

        }

        $scope.removeSection = function(section) {
            _.pull($scope.model, section);
            $scope.selection.deselect(section);
        }


        $scope.exportSection = function(section) {
            
            section.exporting = true;

            var newData = _.clone(section);
            newData.realms = $rootScope.site.realms;


            console.log('NEW DATA', newData, $scope.model);

            FluroContent.resource('siteblock').save(newData).$promise.then(function(res) {
                console.log('Site block was saved', res);
                section.exporting = false;

                section.exportID = res._id;
            }, function(err) {
                console.log('Error exporting site block', err);
                section.exporting = false;
            });
        }

    })

})
app.controller('SectionModalController', function($rootScope, $scope, ObjectSelection, templates) {


	$scope.selection = new ObjectSelection();
	$scope.selection.multiple = true;

	$scope.settings = {};


    $scope.templates = templates;

    ////////////////////////////////////


    $scope.toggleGroup = function(group) {
    	if($scope.settings.selectedGroup == group) {
    		$scope.settings.selectedGroup = null;
    	} else {
    		$scope.settings.selectedGroup = group;
    	}
    }
    ////////////////////////////////////
    ////////////////////////////////////

    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.$watch(function() {
        if ($rootScope.site && $rootScope.site.routes) {
            return $rootScope.site.routes;
        }
    }, function() {

        var routes = getFlattenedRoutes($rootScope.site.routes);

        if (routes) {
            $scope.sections = _.chain(routes).map(function(route) {
                    if (route.sections && route.sections.length) {
                        return {
                            pageName: route.title,
                            route:route,
                            sections: route.sections
                        };
                    }
                })
                .compact()
                .value();
        }
    });

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////





});
app.directive('scriptManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
           //selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-script-manager/fluro-script-manager.html',
        controller: 'ScriptManagerController',
    };
});


app.controller('ScriptManagerController', function($scope) {

    /////////////////////////////////////////////

    $scope.selected = {};


    if (!$scope.model) {
        $scope.model = [];
    } else {
        //Select first route
        $scope.selected.script = $scope.model[0];
    }

    //////////////////////////////////////////

    $scope.removeScript = function() {
        _.pull($scope.model, $scope.selected.script);
        $scope.selected = {};
    }

    // //////////////////////////////////////////

    // $scope.duplicate = function(template) {

    //     if (!template) {
    //         return;
    //     }

    //     var index = $scope.model.indexOf(template);

    //     var newTemplate = angular.copy(template);
    //     newTemplate.title = newTemplate.title + ' copy';

    //     if(index != -1) {
    //         $scope.model.splice(index+1, 0, newTemplate);
    //     } else {
    //         $scope.model.push(newTemplate);
    //     }

    //     $scope.selection.select(newTemplate);
    // }

    //////////////////////////////////////////

    $scope.addScript = function() {

        var newScript = {
            title: 'New Script',
            body:''
            //includeInMenu:true,
        };

        $scope.model.push(newScript);
        $scope.selected.script = newScript;
    }

})
app.directive('scrollActive', function($compile, $timeout, $window, FluroScrollService) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attrs) {

            ////////////////////////////////////////////////

            var onActive;
            var onBefore;
            var onAfter;
            var onAnchor;


            if(!$scope.scrollActiveStartOffset) {
                $scope.scrollActiveStartOffset = 100;
            }

            ////////////////////////////////////////////////

            var currentContext = '';
            var anchored;

            ////////////////////////////////////////////////

            if ($attrs.onActive) {
                onActive = function() {
                    $scope.$eval($attrs.onActive);
                }
            }

            if ($attrs.onAnchor) {
                onAnchor = function() {
                    $scope.$eval($attrs.onAnchor);
                }
            }

            if ($attrs.onAfter) {
                onAfter = function() {
                    $scope.$eval($attrs.onAfter);
                }
            }

            if ($attrs.onBefore) {
                onBefore = function() {
                    $scope.$eval($attrs.onBefore);
                }
            }


            ////////////////////////////////////////////////


            //Check if there is a parent we should be looking at instead of the body
            var parent = $element.closest('[scroll-active-parent]');
            var body = angular.element('body');

            ////////////////////////////////////////////////
            ////////////////////////////////////////////////

            if (parent.length) {
                //Listen for the parent scroll value
                parent.bind("scroll", updateParentScroll);
                $timeout(updateParentScroll, 10);
            } else {
                //Watch for changes to the main scroll value
                $scope.$watch(function() {
                    return FluroScrollService.getScroll();
                }, updateFromMainScroll);

                //Fire one for good measure
                $timeout(updateFromMainScroll, 10);
            }

            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////
            ////////////////////////////////////////////////

            function setScrollContext(context) {
                if (currentContext != context) {
                    currentContext = context;

                    $timeout(function() {

                        switch (context) {
                            case 'active':
                                $element.removeClass('scroll-after');
                                $element.removeClass('scroll-before');
                                $element.addClass('scroll-active');
                                $scope.scrollActive = true;
                                $scope.scrollBefore = false;
                                $scope.scrollAfter = false;

                                if (onActive) {
                                    onActive();
                                }

                                break;
                            case 'before':
                                $element.removeClass('scroll-after');
                                $element.addClass('scroll-before');
                                $element.removeClass('scroll-active');
                                $scope.scrollActive = false;
                                $scope.scrollBefore = true;
                                $scope.scrollAfter = false;

                                if (onBefore) {
                                    onBefore();
                                }

                                break;
                            case 'after':
                                $element.addClass('scroll-after');
                                $element.removeClass('scroll-before');
                                $element.removeClass('scroll-active');
                                $scope.scrollActive = false;
                                $scope.scrollBefore = false;
                                $scope.scrollAfter = true;

                                if (onAfter) {
                                    onAfter();
                                }

                                break;
                        }
                    })
                }
            }



            //////////////////////////////////////////////
            //////////////////////////////////////////////

            function updateParentScroll() {


                //Get the scroll value
                var scrollValue = parent.scrollTop();

                /////////////////////////

                //constants
                var viewportHeight = parent.height();
                var contentHeight = parent.get(0).scrollHeight;

                //////////////////////////////////////////////

                //Limits and markers
                var viewportHalf = (viewportHeight / 2);
                var maxScroll = contentHeight - viewportHeight;

                //Scroll
                var startView = 0;
                var endView = startView + viewportHeight;
                var halfView = endView - (viewportHeight / 2);


                /////////////////////////

                //Element Dimensions
                var elementHeight = $element.outerHeight();
                var elementStart = $element.position().top;
                var elementEnd = elementStart + elementHeight;
                var elementHalf = elementStart + (elementHeight / 4);

                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //If an anchor callback has been specified
                if (onAnchor) {
                    var start = parseInt(startView);
                    var rangeStart = parseInt(elementStart);
                    var rangeEnd = parseInt(elementHalf);
                    
                    // console.log(rangeStart, start, rangeEnd);
                    
                    if (start >= rangeStart && start < rangeEnd) {
                        if(!anchored) {
                            anchored = true;
                            if (anchored) {
                                onAnchor();
                            }
                        }
                    } else {
                        anchored = false;
                    }
                }


                ///////////////////////////////////////////////////

                //Check if the entire element is viewable on screen
                var entirelyViewable = (elementStart >= startView) && (elementEnd <= endView);

                ///////////////////////////////////////////////////

                //console.log('Scroll Value', entirelyViewable, scrollValue, halfView);
                if (entirelyViewable) {
                    return setScrollContext('active');
                }


                

                //Scrolled past the content so set to after
                if (halfView >= elementEnd) {
                    return setScrollContext('after');
                }

                //If the point is past the start of the new element
                //Then set the element to active
                if (halfView >= elementStart) {
                    return setScrollContext('active');
                }

                //If we reach the end of the page
                if (startView >= (maxScroll - 200)) {
                    return setScrollContext('active');
                }

                //Otherwise we havent reached the element yet
                return setScrollContext('before');



            }

            //////////////////////////////////////////////
            //////////////////////////////////////////////

            function updateFromMainScroll(scrollValue) {


                //constants
                var windowHeight = $window.innerHeight;
                var documentHeight = body.height();

                //////////////////////////////////////////////

                //Limits and markers
                var windowHalf = (windowHeight / 2);
                var maxScroll = documentHeight - windowHeight;

                //Scroll
                var startView = scrollValue;
                if(!startView) {
                    startView = 0;
                }
                var endView = startView + windowHeight;
                var halfView = endView - (windowHeight / 2)

                ///////////////////////////////////////////////////

                //Element
                var elementHeight = $element.outerHeight();
                var elementStart = $element.offset().top;
                var elementEnd = elementStart + elementHeight;
                var elementHalf = elementStart + (elementHeight / 4);

                ///////////////////////////////////////////////////
                ///////////////////////////////////////////////////

                //If an anchor callback has been specified
                if (onAnchor) {
                    var start = parseInt(startView);
                    var rangeStart = parseInt(elementStart);
                    var rangeEnd = parseInt(elementHalf);

                    console.log(rangeStart, start, rangeEnd);
                    
                    if (start >= rangeStart && start < rangeEnd) {
                        if(!anchored) {
                            anchored = true;
                            if (anchored) {
                                onAnchor();
                            }
                        }
                    } else {
                        anchored = false;
                    }
                }
                

                ///////////////////////////////////////////////////

                //Check if the entire element is viewable on screen
                var entirelyViewable = (elementStart >= startView) && (elementEnd <= endView);

                ///////////////////////////////////////////////////

                
                if (entirelyViewable) {
                    return setScrollContext('active');
                }

                //Scrolled past the content so set to after
                if (halfView >= elementEnd) {
                    return setScrollContext('after');
                }

                //If element reaches half of the screen viewport
                if (halfView >= elementStart) {
                    return setScrollContext('active');
                }

                //If we reach the end of the page
                if (startView >= (maxScroll - 200)) {
                    return setScrollContext('active');
                }


                //Otherwise we havent reached the element yet
                return setScrollContext('before');

            }

        }
    };
});
app.service('FluroScrollService', function($window, $location, $timeout) {

    var controller = {};
    
    /////////////////////////////////////
    
    controller.cache = {}
    
    /////////////////////////////////////

    controller.direction = 'down';

    /////////////////////////////////////

    var _value = 0;
    var body = angular.element('html,body');

    /////////////////////////////////////

    controller.setAnchor = function(id) {
        $location.hash('jump-to-' + id);
    }


    /////////////////////////////////////

    controller.getAnchor = function() {
        var hash = $location.hash();
        if (_.startsWith(hash, 'jump-to-')) {
            return hash.substring(8);
        } else {
            return hash;
        }
    }

    /////////////////////////////////////

    function updateScroll() {
        var v = this.pageYOffset;

        if (_value != this.pageYOffset) {
            if (v < _value) {
                controller.direction = 'up';
            } else {
                controller.direction = 'down';
            }


            $timeout(function() {
                _value = this.pageYOffset;
            });
        }
    }

    /////////////////////////////////////

    controller.scrollToID =
        controller.scrollToId = function(id, speed, selector, offset) {

            if (!speed) {
                speed = 'fast';
            }

            var $target = angular.element('#' + id);

            if ($target && $target.offset && $target.offset()) {
                if (!selector) {
                    selector = 'body,html';
                }


                var pos = $target.offset().top;

                //If theres an offset
                if(offset) {
                    pos += Number(offset);
                }

                angular.element(selector).animate({
                    scrollTop: pos
                }, speed);
            }

    }

    /////////////////////////////////////

    controller.scrollToPosition =
        controller.scrollTo = function(pos, speed, selector, offset) {

            if (!speed) {
                speed = 'fast';
            }

            if (!selector) {
                selector = 'body,html';
            }


             //If theres an offset
            if(offset) {
                pos += Number(offset);
            }

            angular.element(selector).animate({
                scrollTop: pos
            }, speed);
    }

    /////////////////////////////////////

    controller.get =
    controller.getScroll = function() {
        return _value;
    }

    /////////////////////////////////////

    controller.getMax = function(selector) {

        if (!selector) {
            selector = 'body,html';
        }

        var bodyHeight = angular.element(selector).height();
        var windowHeight = $window.innerHeight;

        return (bodyHeight - windowHeight);
    }


    controller.getHalfPoint = function() {
        return ($window.innerHeight / 2);
    }

    controller.getWindowHeight = function() {
        return $window.innerHeight;
    }

    /////////////////////////////////////

    angular.element($window).bind("scroll", updateScroll);

    //Update the scroll on init
    updateScroll();

    /////////////////////////////////////

    return controller;

});
app.directive('fluroStylesheetLoader', function(Fluro, $compile, FluroSocket) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        template: '<div class="stylesheet-loader"></div>',
        link: function($scope, $element, $attr) {

            //////////////////////////////////////////////////////////////////////////////////////////

            //Watch for any changes
            $scope.$watch('model.stylesheets.length + model.afterStylesheets.length + model.bower.length + model.components.length', reloadStylesheets);

            //////////////////////////////////////////////////////////////////////////////////////////

            function reloadStylesheets() {
                console.log('Refresh Styles')

                //Clear the element
                $element.empty();

                //////////////////////////

                //If there is a site
                if (!$scope.model) {
                    return;
                }

                ///////////////////////////////////////

                var script = '';

                ///////////////////////////////////////

                // THIS IS HANDLED IN Configuration.js now
                // //If there are bower components aswell then include the vendor scripts
                // if ($scope.model.bower && $scope.model.bower.length) {
                //     console.log('Load Bower CSS');
                //     var vendorUrl = Fluro.apiURL + '/get/site/vendor/' + $scope.model._id + '/css';
                //     script += '<link href="' + vendorUrl + '" rel="stylesheet" type="text/css"/>';
                // }

                ///////////////////////////////////////
                ///////////////////////////////////////

                //Keep track of all the stylesheet ids
                var ids = [];

                ///////////////////////////////////////

                if ($scope.model.stylesheets && $scope.model.stylesheets.length) {
                    var stylesheetIds = _.reduce($scope.model.stylesheets, function(result, include) {
                        if (include._id) {
                            result.push(include._id);
                        } else {
                            result.push(include);
                        }

                        return result;

                    }, []);

                    ids = ids.concat(stylesheetIds);

                }

                ///////////////////////////////////////

                //If there are components with styles
                if ($scope.model.components && $scope.model.components.length) {
                    var componentIds = _.reduce($scope.model.components, function(result, include) {
                        //console.log('Component', include.title, include);
                        if (include.css && include.css.length) {
                            if (include._id) {
                                result.push(include._id);
                            } else {
                                result.push(include);
                            }
                        }
                        return result;
                    }, []);

                    ///////////////////////////////////////
                    //  console.log('Load the components', $scope.model.components.length, componentIds);
                    if (componentIds.length) {

                        ids = ids.concat(componentIds);
                    }


                }

                ///////////////////////////////////////

                if ($scope.model.afterStylesheets && $scope.model.afterStylesheets.length) {
                    var afterStyles = _.reduce($scope.model.afterStylesheets, function(result, include) {
                        if (include._id) {
                            result.push(include._id);
                        } else {
                            result.push(include);
                        }
                        return result;
                    }, []);

                    ids = ids.concat(afterStyles);
                }

                ///////////////////////////////////////

                if (ids.length) {
                    //Compiled SASS Url
                    var url = Fluro.apiURL + '/get/compiled/scss?ids=' + ids.join(',');

                    if (Fluro.token) {
                        url += '&access_token=' + Fluro.token;
                    }

                    //Include as a link tag
                    script += '<link href="' + url + '" rel="stylesheet" type="text/css"/>';
                }

                ///////////////////////////////////////

                if (script && script.length) {
                    //Do the magic!
                    $element.append(script);
                    $compile($element.contents())($scope);

                    //return reloadStylesheets();
                }

            }

            //////////////////////////////////////////////////////////////////////////////////////////

            //Listen for any socket events
            FluroSocket.on('content.edit', function(socketUpdate) {               


                //Get the updated item id
                var itemID = socketUpdate.item;
                if(itemID._id) {
                    itemID = itemID._id;
                }

                

                //////////////////////////////////////////////////////////////////////////////////////////

                var combinedDependencies = [].concat($scope.model.components, $scope.model.stylesheets, $scope.model.afterStylesheets)
                
                console.log('CSS UPDATE', socketUpdate);

                var requireUpdate = _.chain(combinedDependencies)
                .compact()
                .some(function(item) {

                    if(!item) {
                        return false;
                    } 

                    if (item._id) {
                    
                        return (item._id == itemID);
                    } else {
                        // console.log('Check ITEM', item, itemID, item == itemID);
                        return (item == itemID);
                    }

                })
                .value();

                //////////////////////////////////////////////////////////////////////////////////////////

                if (requireUpdate) {
                    reloadStylesheets();
                }

            });

            //////////////////////////////////////////////////////////////////////////////////////////



        },
    };
});
app.directive('templateManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
           //selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-template-manager/fluro-template-manager.html',
        controller: 'TemplateManagerController',
    };
});


app.controller('TemplateEditorController', function($scope) {


    /////////////////////////////////////////////

    $scope.$watch('selection.item', function(item) {
        if (!item.key || !item.key.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('selection.item.title', function(newValue) {
            if (newValue) {
                $scope.selection.item.key = _.camelCase(newValue); //.toLowerCase();
            }
        });
    }

    /////////////////////////////////////////////

    $scope.$watch('selection.item.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.selection.item.key = $scope.selection.item.key.replace(regexp, '');
        }
    });

})

app.controller('TemplateManagerController', function($scope, ObjectSelection) {


    //Setup one route selected at a time
    var selection = new ObjectSelection();
    selection.multiple = false;
    $scope.selection = selection;



    if (!$scope.model) {
        $scope.model = [];
    } else {
        //Select first route
        $scope.selection.select($scope.model[0]);
    }

    //////////////////////////////////////////

    $scope.removeTemplate = function() {
        _.pull($scope.model, $scope.selection.item);
        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.duplicate = function(template) {

        if (!template) {
            return;
        }

        var index = $scope.model.indexOf(template);

        var newTemplate = angular.copy(template);
        newTemplate.title = newTemplate.title + ' copy';

        if(index != -1) {
            $scope.model.splice(index+1, 0, newTemplate);
        } else {
            $scope.model.push(newTemplate);
        }

        $scope.selection.select(newTemplate);
    }

    //////////////////////////////////////////

    $scope.addTemplate = function() {

        var newTemplate = {
            title: '',
            //includeInMenu:true,
        };

        $scope.model.push(newTemplate);
        $scope.selection.select(newTemplate);
    }

})
app.service('FluroWindowService', function($window, $timeout) {

    var controller = {};
    controller.breakpoint = 'xs';

    /////////////////////////////////////

    function updateSize() {
        controller.width = $window.innerWidth;
        controller.height = $window.innerHeight;

        if(controller.width < 768) {
            return controller.breakpoint = 'xs';
        }

        if(controller.width < 992) {
            return controller.breakpoint = 'sm';
        }

        if(controller.width < 1200) {
            return controller.breakpoint = 'md';
        }
        
        return controller.breakpoint = 'lg';
    }

    /////////////////////////////////////

    angular.element($window).bind("resize", updateSize);
    updateSize();

    /////////////////////////////////////

    return controller;

});
app.directive('compileHtml', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.$watch(function() {
                return scope.$eval(attrs.compileHtml);
            }, function(value) {



                element.html(value);
                $compile(element.contents())(scope);
            });
        }
    };
});
app.directive('editable', function($compile, $timeout) {
    return {
        restrict: 'A',
        scope: {
            model: '=ngModel',
        },
        link: function(scope, element, attrs) {

            //Active
            scope.active = false;

            //Normal
            var normal = angular.element('<span class="editable-text" ng-hide="active" ng-dblclick="setActive($event)">{{model}}</span>');
            var editor = angular.element('<input class="editable-input" ng-show="active" ng-model="model" ng-blur="active = false"/>');

            $compile(normal)(scope);
            $compile(editor)(scope);

            //Add the elements
            element.append(normal);
            element.append(editor);


            scope.setActive = function(event) {
                scope.active = true;
                $timeout(function() {
                    editor.select();
                }, 10);
            }

            //Listen for keypress
            element.bind("keydown keypress", function(event) {
                console.log('Enter pressed')
                if (event.which === 13) {
                    event.preventDefault();
                    //Set active
                    $timeout(function() {
                        scope.active = false;
                    });
                }

            })



        }
    }
});


// app.directive('selectOnFocus', function($timeout) {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             var focusedElement = null;

//             element.on('focus', function() {
//                 var self = this;
//                 if (focusedElement != self) {
//                     focusedElement = self;
//                     $timeout(function() {
//                         self.select();
//                     }, 10);
//                 }
//             });

//             element.on('blur', function() {
//                 focusedElement = null;
//             });
//         }
//     }
// });
app.directive('handle', function() {

    return {
        restrict: 'E',
        replace:true,
        template:'<div class="btn-handle"><i class="fa fa-arrows"></i></div>'
    };
});

angular.module('fluro.lazyload', []).directive('script', function() {
    return {
        restrict: 'E',
        scope: false,
        link: function(scope, elem, attr) {
            if (attr.type === 'text/lazy') {
                var code = elem.text();
                var f = new Function(code);
                f();
            }
        }
    };
});



app.directive('machineName', function($parse) {
    return {
        restrict: 'A',
        require: '^ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$viewChangeListeners.push(function() {
                /*Set model value differently based on the viewvalue entered*/
                var regexp = /[^a-zA-Z0-9-_]+/g;

                var val = ngModelCtrl.$viewValue.replace(regexp, '-');
                var cameled = _.camelCase(val);


                //$parse(attrs.ngModel).assign(scope, ngModelCtrl.$viewValue.replace(regexp, ''));
                $parse(attrs.ngModel).assign(scope, cameled);
                

            });
        }
    };
});

app.directive('notifications', function() {
    return {
        restrict: 'E',
        replace: true,
        controller: 'NotificationsController',
        template: '<div class="notifications"><div ng-if="lastMessage" class="notification {{lastMessage.type}}"><i class="fa fa-notification-{{lastMessage.type}}"></i><span>{{lastMessage.text}}</span></div></div>',
    };
})


/////////////////////////////////////////////////////

app.controller('NotificationsController', function($scope, Notifications) {
    //$scope.messages = Notifications.list;

    $scope.$watch(function() {
    	return Notifications.list.length;
    }, function() {
    	$scope.lastMessage = _.last(Notifications.list);
    })
})
app.filter('age', function(){
  return function(date){
      return Number(moment().diff(date, 'years'));
  };
});

app.filter('comma', function() {
    return function(array, key, characterLimit) {


        if(!array) {
            return '';
        }


        if (key) {
            var strings = _.map(array, function(item) {
                return item[key];
            })
        } else {
            strings = array;
        }

        if(characterLimit) {
            return strings.join(", ").substring(0, characterLimit);
        } else {
            return strings.join(", ")
        }
    };
});






app.filter('simple', function() {
    return function(data) {


        if(_.isString(data)) {
            return data;
        }

        if(_.isArray(data)) {

            var array = _.map(data, function(item) {
                if(_.isObject(item)) {
                    return item.title;
                } 

                if(_.isString(item)) {
                    return item;
                }
            })

            return array.join(', ');
        }
    };
});
app.filter('kebabcase', function() {
        return function(text) {
           return _.kebabCase(text);
        };
    });
app.filter("reference", function() {
    return function(items, fieldName, id) {


        
        function getProperty(obj, prop) {
            var parts = prop.split('.'),
                last = parts.pop(),
                l = parts.length,
                i = 1,
                current = parts[0];

            if(!parts.length) {
                return obj[prop];
            }

            while ((obj = obj[current]) && i < l) {
                current = parts[i];
                i++;
            }

            if (obj) {
                return obj[last];
            }
        }
       /* */

        //Find where item author is the id specified
        var results = _.filter(items, function(item) {

            var key = getProperty(item, fieldName);

            if (key) {
                if(_.isArray(key)) {
                    var array = key;
                    return _.some(array, function(e) {
                        if(_.isObject(e)) {
                            if(e._id) {
                                return e._id == id;
                            }
                        } else {
                            return e == id;
                        }
                    })
                } else {
                    return (key._id == id || key == id);
                }
            }



            /*
            if (item[fieldName]) {
            	if(_.isArray(item[fieldName])) {

            		var array = item[fieldName];
            		return _.some(array, {'_id': id});
            	} else {
                	return (item[fieldName]._id == id || item[fieldName] == id);
            	}
            }
            */
        });

        return results;



    };
});
app.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.filter('timeago', function(){
  return function(date){
    return moment(date).fromNow();
  };
});

app.service('FluroStorage', function($rootScope, $localStorage, $sessionStorage) {


    /////////////////////////////////////////////////////

    var controller = {}

    /////////////////////////////////////////////////////
    
    controller.resetSessionStorage = function() {
        if($rootScope.user) {
            $sessionStorage[$rootScope.user._id] = {};
        }
    }

    /////////////////////////////////////////////////////
    
    controller.resetLocalStorage = function() {
        if($rootScope.user) {
            $localStorage[$rootScope.user._id] = {};
        }
    }

    /////////////////////////////////////////////////////

    controller.sessionStorage = function(key) {
        if($rootScope.user) {
            if(!$sessionStorage[$rootScope.user._id]) {
                $sessionStorage[$rootScope.user._id] = {}
            }

            if(!$sessionStorage[$rootScope.user._id][key]) {
                $sessionStorage[$rootScope.user._id][key] = {}
            }

            return $sessionStorage[$rootScope.user._id][key];
        }
    }

    controller.localStorage = function(key) {
        if($rootScope.user) {
            if(!$localStorage[$rootScope.user._id]) {
                $localStorage[$rootScope.user._id] = {}
            }

            if(!$localStorage[$rootScope.user._id][key]) {
                $localStorage[$rootScope.user._id][key] = {}
            }

            return $localStorage[$rootScope.user._id][key];
        }
    }

    /////////////////////////////////////////////////////

    return controller;
});
app.service('ModalService', function($modal, $rootScope, Fluro, FluroContent, TypeService) {

    var controller = {}


    /////////////////////////////////////////////////////////////////////

    controller.message = function(ids, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance = $modal.open({
            templateUrl: 'fluro-message-center/main.html',
            controller: 'FluroMessageCenterController',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                /**
                    access: function(FluroAccess) {
                        var canCreate;
                        if (definition) {
                            canCreate = FluroAccess.can('create', definition.definitionName);
                        } else {
                            canCreate = FluroAccess.can('create', type.path);
                        }

                        return FluroAccess.resolveIf(canCreate);
                    },
                    /**/
                contacts: function($q) {
                    return FluroContent.endpoint('message/contacts').query({
                        ids: ids,
                    }).$promise;
                }
            }
        });

        modalInstance.result.then(successCallback, cancelCallback);
    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    controller.create = function(typeName, params, successCallback, cancelCallback) {

        //console.log('Create In Modal', params);
        //////////////////////*/

        if (!params) {
            params = {};
        }

        var type = TypeService.getTypeFromPath(typeName);


        if (type.parentType) {

            //Resource
            FluroContent.endpoint('defined/' + type.path).get({}, createModal);

        } else {
            createModal();
        }

        function createModal(definition) {

            ///////////////////////////////

            var modalInstance = $modal.open({
                templateUrl: 'fluro-admin-content/types/form.html',
                controller: 'ContentFormController',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    //Get the definition
                    definition: function() {
                        return definition;
                    },
                    type: function() {
                        if (definition) {
                            return TypeService.getTypeFromPath(definition.parentType);
                        } else {
                            return type;
                        }
                    },
                    access: function(FluroAccess) {
                        var canCreate;
                        if (definition) {
                            canCreate = FluroAccess.can('create', definition.definitionName);
                        } else {
                            canCreate = FluroAccess.can('create', type.path);
                        }

                        return FluroAccess.resolveIf(canCreate);
                    },
                    item: function($q) {
                        var deferred = $q.defer();

                        var newItem = {};

                        //Check if we received any starting data
                        if (params.template) {
                            newItem = angular.copy(params.template);
                        }

                        //console.log('Create new item', newItem);

                        if (definition) {
                            //If the definition exists
                            if (definition.definitionName) {
                                //Send back a new item with the definition set
                                newItem.definition = definition.definitionName;
                                deferred.resolve(newItem);
                            } else {
                                //Just don't resolve
                                deferred.reject();
                            }

                            //return deferred.promise;
                        } else {

                            deferred.resolve(newItem);
                            //return newItem;
                        }

                        return deferred.promise;
                    },
                    extras: function() {
                        return {};
                    }
                }
            });

            modalInstance.result.then(successCallback, cancelCallback);
        }
    }


    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.view = function(itemObject, successCallback, cancelCallback) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        //Load the items
        FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {

                var modalInstance = $modal.open({
                    templateUrl: 'fluro-admin-content/types/view.html',
                    controller: 'ContentViewController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            return item;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }
                            var canView = FluroAccess.canViewItem(item, (definitionName == 'user'));
                            return FluroAccess.resolveIf(canView);
                        },
                        extras: function(FluroContent, $q) {

                            var deferred = $q.defer();

                            //Data Object
                            var data = {};

                            switch (type.path) {
                                // case 'process':
                                //     return FluroContent.endpoint('process/' + item._id + '/progress', null, true).query().$promise;
                                //     break;
                                case 'event':
                                case 'plan':

                                    var id;
                                    if (item.event) {
                                        if (item.event._id) {
                                            id = item.event._id;
                                        } else {
                                            id = item.event;
                                        }
                                    } else {
                                        id = item._id;
                                    }

                                    if (id) {
                                        FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(function(res) {
                                            data.confirmations = res;
                                            //console.log('GOT CONFIRMATIONS', res);
                                            deferred.resolve(data);
                                        });
                                    } else {
                                        //console.log('NO ID')
                                        deferred.resolve(data);
                                    }
                                    break;
                                default:
                                    deferred.resolve(data);
                                    break;
                            }

                            return deferred.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }

    controller.edit = function(itemObject, successCallback, cancelCallback, createCopy, createFromTemplate, $scope) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        //Load the items
        FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {

                var modalInstance = $modal.open({
                    templateUrl: 'fluro-admin-content/types/form.html',
                    controller: 'ContentFormController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {

                            var newItem = item;

                            if (createCopy || createFromTemplate) {
                                newItem = angular.copy(item);

                                delete newItem._id;
                                delete newItem.slug;
                                delete newItem.author;

                                //Delete API Key
                                delete newItem.apikey;

                                //Clear plan details
                                if (newItem._type == 'event') {
                                    newItem.plans = [];
                                    newItem.assignments = [];
                                }


                                if (newItem._type == 'event' || newItem._type == 'plan') {
                                    var initDate = new Date();

                                    if ($scope) {
                                        if ($scope.stringBrowseDate) {
                                            initDate = $scope.stringBrowseDate();
                                        }

                                        if ($scope.item && $scope.item.startDate) {
                                            initDate = $scope.item.startDate;
                                            //console.log('Use start date of item we are creating for');
                                        }
                                    }

                                    //Reset Dates
                                    newItem.useEndDate = false;
                                    newItem.endDate = initDate;
                                    newItem.startDate = initDate;
                                }

                                //Link a new plan to an event if none provided
                                if (newItem._type == 'plan' && !newItem.event) {
                                    if ($scope.item && $scope.item._id) {
                                        newItem.event = $scope.item;
                                    }
                                }
                            }

                            ////////////////////////////////////////////////

                            //Create Copy
                            if (createCopy) {
                                newItem.title = newItem.title + ' Copy';
                            }

                            ////////////////////////////////////////////////

                            if (createFromTemplate) {
                                //Update the status
                                newItem.status = 'active';

                            }

                            ////////////////////////////////////////////////

                            //Cheat here
                            if (itemObject.event) {
                                newItem.event = itemObject.event;
                                if (newItem._type == 'plan') {
                                    newItem.startDate = newItem.event.startDate;
                                }
                            }

                            ////////////////////////////////////////////////

                            return newItem;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess, $rootScope) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }

                            //Allow the user to modify themselves
                            var author = false;

                            //If wanting to edit a user
                            if (type.path == 'user') {
                                author = ($rootScope.user._id == item._id);
                                if (author) {
                                    return true;
                                }
                            } else {
                                //Only allow if author of the content
                                if (_.isObject(item.author)) {
                                    author = (item.author._id == $rootScope.user._id);
                                } else {
                                    author = (item.author == $rootScope.user._id);
                                }
                            }

                            /////////////////////////////////////

                            //Check if we can edit
                            var canEdit = FluroAccess.canEditItem(item, (definitionName == 'user'));
                            var canCreate = FluroAccess.can('create', definitionName);

                            if (createCopy) {
                                return FluroAccess.resolveIf(canCreate && canEdit);
                            } else {
                                return FluroAccess.resolveIf(canEdit);
                            }
                        },
                        extras: function(FluroContent, $q) {
                            var deferred = $q.defer();

                            //Data Object
                            var data = {};

                            switch (type.path) {
                                case 'event':
                                    if (itemObject._id && !createCopy && !createFromTemplate) {
                                        FluroContent.endpoint('confirmations/event/' + itemObject._id).query().$promise.then(function(res) {
                                            data.confirmations = res;
                                            //console.log('GOT CONFIRMATIONS', res);
                                            deferred.resolve(data);
                                        });
                                    } else {
                                        //console.log('NO ID')
                                        deferred.resolve(data);
                                    }
                                    break;
                                default:
                                    deferred.resolve(data);
                                    break;
                            }

                            return deferred.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }


    /////////////////////////////////////////////////////////////////////////

    controller.browse = function(type, modelSource, params) {

        if (!params) {
            params = {}
        }

        /////////////////////////////////////////

        //We need an object for our source
        if (!modelSource) {
            modelSource = {};
        }

        /////////////////////////////////////////

        //We need an array to append the items to
        if (!modelSource.items) {
            modelSource.items = [];
        }

        /////////////////////////////////////////

        //Launch the modal
        var modalDetails = {
            template: '<content-browser ng-model="model.items" ng-done="$dismiss" ng-type="type" params="params"></content-browser>',
            controller: function($scope) {
                $scope.type = type;
                $scope.model = modelSource;
                $scope.params = params;
            },
            size: 'lg',
        };
        /*
        if(params.scope) {
            //Isolate scope
            console.log('Isolate Scope');
            modalDetails.scope = params.scope;
        }
        */

        var modalInstance = $modal.open(modalDetails);

        return modalInstance;

        //modalInstance.result.then(successCallback, cancelCallback);

    }

    controller.batch = function(type, definition, ids, callback) {

        //var $scope = $rootScope.$new();

        var modalInstance = $modal.open({
            template: '<batch-editor></batch-editor>',
            backdrop: 'static',
            controller: function($scope) {
                $scope.type = type;
                $scope.definition = definition;
                $scope.ids = ids;
            },
            size: 'lg',
        });

        //modalInstance.result.then(successCallback, cancelCallback);

    }

    ///////////////////////

    return controller;
});
app.service('SiteTools', function() {

    var controller = {}

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
   
    controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
            if (route.type == 'folder') {
                return controller.getFlattenedRoutes(route.routes);
            } else {
                return route;
            }
        }).flatten().compact().value();
    }


    /////////////////////////////////////////////////////

    return controller;
});
app.service('Tools', function() {

    var controller = {}

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
   
    controller.getDeepProperty = function(obj, prop) {
        var parts = prop.split('.'),
            last = parts.pop(),
            l = parts.length,
            i = 1,
            current = parts[0];



        if(!parts.length) {
            return obj[prop];
        }


        while ((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }
        
        if (obj) {
            return obj[last];
        }
    }


    /////////////////////////////////////////////////////

    return controller;
});




var redactorOptions = {};

//Setup Basic Options
app.constant('redactorOptions', redactorOptions);


//Setup the directive
app.directive('redactor', function($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        controller: function($scope, $modal) {
            $scope.modal = $modal;

        },
        link: function(scope, element, attrs, ngModel) {

            ////////////////////////////////////////////

            // Expose scope var with loaded state of Redactor
            scope.redactorLoaded = false;

            var updateModel = function updateModel(value) {
                // $timeout to avoid $digest collision
                $timeout(function() {
                    scope.$apply(function() {
                        ngModel.$setViewValue(value);
                    });
                });
            };

            var options = {
                changeCallback: updateModel
            }


            /////////////////////////////////

            var additionalOptions = attrs.redactor ? scope.$eval(attrs.redactor) : {};

            /////////////////////////////////

            if(!additionalOptions.buttons) {
            	additionalOptions.buttons = [
            		'html',
            		'formatting', 
            		'bold', 
            		'underline', 
            		'italic', 
					'unorderedlist', 
					'orderedlist', 
					'insertImage', 
					'video', 
					'table', 
					'link', 
					'indent', 
					'outdent', 
					'horizontalrule',
					]
            }

            /////////////////////////////////

            //Format Options
            additionalOptions.formatting = [];
            additionalOptions.formattingAdd = [];

            additionalOptions.formattingAdd.push({
            	tag:'p',
            	title:'Normal text',
            	clear: true,
            });

            additionalOptions.formattingAdd.push({
            	tag:'p',
            	title:'Lead text',
            	class:'lead',
            	clear: true,
            });

            additionalOptions.formattingAdd.push({
            	tag:'p',
            	title:'Small text',
            	class:'small',
            	clear: true,
            });

            additionalOptions.formattingAdd.push({
            	tag:'h1',
            	title:'Heading 1',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h2',
            	title:'Heading 2',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h3',
            	title:'Heading 3',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h4',
            	title:'Heading 4',
            });

            additionalOptions.formattingAdd.push({
            	tag:'h5',
            	title:'Heading 5',
            });

            additionalOptions.formattingAdd.push({
            	tag:'blockquote',
            	title:'Quote',
            });

            

            additionalOptions.formattingAdd.push({
            	tag:'code',
            	title:'Code',
            });


            /////////////////////////////////

            //Add allowed attributes
            additionalOptions.allowedAttr = [
                ['p', 'class'],
                ['img', ['src', 'alt', 'title', 'class', 'ng-src']],
                ['fluro-video', 'ng-model'],
                ['iframe', ['src','style', 'allowfullscreen', 'frameborder']],
                ['tr', ['class']],
                ['td', ['class', 'colspan']],
                ['table', ['class']],
                ['div', ['class']],
                ['a', '*'],
                ['span', ['class', 'rel', 'data-verified']],
                ['iframe', '*'],
                ['video', '*'],
                ['audio', '*'],
                ['embed', '*'],
                ['object', '*'],
                ['param', '*'],
                ['source', '*']
            ];

            /////////////////////////////////

            additionalOptions.replaceTags = [
                ['strike', 'del'],
                //['i', 'em'],
                ['b', 'strong'],
            ]

            /////////////////////////////////

            additionalOptions.replaceDivs = false;

            /////////////////////////////////

            if(!additionalOptions.plugins) {
                //Settings
                additionalOptions.plugins = [
                'insertImage',
                'video',
                'table',
                'undoAction',
                'redoAction'
                ];
            }

            if(!additionalOptions.toolbarExternal) {
                additionalOptions.toolbarExternal = '#article-toolbar';
            }
           

            /////////////////////////////////

            var editor;

            /////////////////////////////////

            angular.extend(options, redactorOptions, additionalOptions);

            /////////////////////////////////

            // prevent collision with the constant values on ChangeCallback
            var changeCallback = additionalOptions.changeCallback || redactorOptions.changeCallback;
            if (changeCallback) {
                options.changeCallback = function(value) {
                    updateModel.call(this, value);
                    changeCallback.call(this, value);
                }
            }

            /////////////////////////////////

            // put in timeout to avoid $digest collision.  call render() to
            // set the initial value.
            $timeout(function() {
                editor = element.redactor(options);
                ngModel.$render();
                element.on('remove', function() {
                    element.off('remove');
                    element.redactor('core.destroy');
                });
            });

            ngModel.$render = function() {
                if (angular.isDefined(editor)) {
                    $timeout(function() {
                        element.redactor('code.set', ngModel.$viewValue || '');
                        element.redactor('placeholder.toggle');
                        scope.redactorLoaded = true;
                    });
                }
            };
        }
    };
});
;/*
	Redactor 10.2.3
	Updated: August 15, 2015

	http://imperavi.com/redactor/

	Copyright (c) 2009-2015, Imperavi LLC.
	License: http://imperavi.com/redactor/license/

	Usage: $('#content').redactor();
*/

(function($)
{

	'use strict';

	if (!Function.prototype.bind)
	{
		Function.prototype.bind = function(scope)
		{
			var fn = this;
			return function()
			{
				return fn.apply(scope);
			};
		};
	}

	var uuid = 0;

	// Plugin
	$.fn.redactor = function(options)
	{
		var val = [];
		var args = Array.prototype.slice.call(arguments, 1);

		if (typeof options === 'string')
		{
			this.each(function()
			{
				var instance = $.data(this, 'redactor');
				var func;

				if (options.search(/\./) != '-1')
				{
					func = options.split('.');
					if (typeof instance[func[0]] != 'undefined')
					{
						func = instance[func[0]][func[1]];
					}
				}
				else
				{
					func = instance[options];
				}

				if (typeof instance !== 'undefined' && $.isFunction(func))
				{
					var methodVal = func.apply(instance, args);
					if (methodVal !== undefined && methodVal !== instance)
					{
						val.push(methodVal);
					}
				}
				else
				{
					$.error('No such method "' + options + '" for Redactor');
				}
			});
		}
		else
		{
			this.each(function()
			{
				$.data(this, 'redactor', {});
				$.data(this, 'redactor', Redactor(this, options));
			});
		}

		if (val.length === 0) return this;
		else if (val.length === 1) return val[0];
		else return val;

	};

	// Initialization
	function Redactor(el, options)
	{
		return new Redactor.prototype.init(el, options);
	}

	// Functionality
	$.Redactor = Redactor;
	$.Redactor.VERSION = '10.2.3';
	$.Redactor.modules = ['alignment', 'autosave', 'block', 'buffer', 'build', 'button',
						  'caret', 'clean', 'code', 'core', 'dropdown', 'file', 'focus',
						  'image', 'indent', 'inline', 'insert', 'keydown', 'keyup',
						  'lang', 'line', 'link', 'linkify', 'list', 'modal', 'observe', 'paragraphize',
						  'paste', 'placeholder', 'progress', 'selection', 'shortcuts',
						  'tabifier', 'tidy',  'toolbar', 'upload', 'utils'];

	$.Redactor.opts = {

		// settings
		lang: 'en',
		direction: 'ltr', // ltr or rtl

		plugins: false, // array

		focus: false,
		focusEnd: false,

		placeholder: false,

		visual: true,
		tabindex: false,

		minHeight: false,
		maxHeight: false,

		linebreaks: false,
		replaceDivs: true,
		paragraphize: true,
		cleanStyleOnEnter: false,
		enterKey: true,

		cleanOnPaste: true,
		cleanSpaces: true,
		pastePlainText: false,

		autosave: false, // false or url
		autosaveName: false,
		autosaveInterval: 60, // seconds
		autosaveOnChange: false,
		autosaveFields: false,

		linkTooltip: true,
		linkProtocol: 'http',
		linkNofollow: false,
		linkSize: 50,

		imageEditable: true,
		imageLink: true,
		imagePosition: true,
		imageFloatMargin: '10px',
		imageResizable: true,

		imageUpload: null,
		imageUploadParam: 'file',

		uploadImageField: false,

		dragImageUpload: true,

		fileUpload: null,
		fileUploadParam: 'file',

		dragFileUpload: true,

		s3: false,

		convertLinks: true,
		convertUrlLinks: true,
		convertImageLinks: true,
		convertVideoLinks: true,

		preSpaces: 4, // or false
		tabAsSpaces: false, // true or number of spaces
		tabKey: true,

		scrollTarget: false,

		toolbar: true,
		toolbarFixed: true,
		toolbarFixedTarget: document,
		toolbarFixedTopOffset: 0, // pixels
		toolbarExternal: false, // ID selector
		toolbarOverflow: false,

		source: true,
		buttons: ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist',
				  'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule'], // + 'underline'

		buttonsHide: [],
		buttonsHideOnMobile: [],

		formatting: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
		formattingAdd: false,

		tabifier: true,

		deniedTags: ['script', 'style'],
		allowedTags: false, // or array

		paragraphizeBlocks: ['table', 'div', 'pre', 'form', 'ul', 'ol', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'dl', 'blockquote', 'figcaption',
							'address', 'section', 'header', 'footer', 'aside', 'article', 'object', 'style', 'script', 'iframe', 'select', 'input', 'textarea',
							'button', 'option', 'map', 'area', 'math', 'hr', 'fieldset', 'legend', 'hgroup', 'nav', 'figure', 'details', 'menu', 'summary', 'p'],

		removeComments: false,
		replaceTags: [
			['strike', 'del'],
			['b', 'strong']
		],
		replaceStyles: [
            ['font-weight:\\s?bold', "strong"],
            ['font-style:\\s?italic', "em"],
            ['text-decoration:\\s?underline', "u"],
            ['text-decoration:\\s?line-through', 'del']
        ],
        removeDataAttr: false,

		removeAttr: false, // or multi array
		allowedAttr: false, // or multi array

		removeWithoutAttr: ['span'], // or false
		removeEmpty: ['p'], // or false;

		activeButtons: ['deleted', 'italic', 'bold', 'underline', 'unorderedlist', 'orderedlist',
						'alignleft', 'aligncenter', 'alignright', 'justify'],
		activeButtonsStates: {
			b: 'bold',
			strong: 'bold',
			i: 'italic',
			em: 'italic',
			del: 'deleted',
			strike: 'deleted',
			ul: 'unorderedlist',
			ol: 'orderedlist',
			u: 'underline'
		},

		shortcuts: {
			'ctrl+shift+m, meta+shift+m': { func: 'inline.removeFormat' },
			'ctrl+b, meta+b': { func: 'inline.format', params: ['bold'] },
			'ctrl+i, meta+i': { func: 'inline.format', params: ['italic'] },
			'ctrl+h, meta+h': { func: 'inline.format', params: ['superscript'] },
			'ctrl+l, meta+l': { func: 'inline.format', params: ['subscript'] },
			'ctrl+k, meta+k': { func: 'link.show' },
			'ctrl+shift+7':   { func: 'list.toggle', params: ['orderedlist'] },
			'ctrl+shift+8':   { func: 'list.toggle', params: ['unorderedlist'] }
		},
		shortcutsAdd: false,

		// private
		buffer: [],
		rebuffer: [],
		emptyHtml: '<p>&#x200b;</p>',
		invisibleSpace: '&#x200b;',
		imageTypes: ['image/png', 'image/jpeg', 'image/gif'],
		indentValue: 20,
		verifiedTags: 		['a', 'img', 'b', 'strong', 'sub', 'sup', 'i', 'em', 'u', 'small', 'strike', 'del', 'cite', 'ul', 'ol', 'li'], // and for span tag special rule
		inlineTags: 		['strong', 'b', 'u', 'em', 'i', 'code', 'del', 'ins', 'samp', 'kbd', 'sup', 'sub', 'mark', 'var', 'cite', 'small'],
		alignmentTags: 		['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6',  'DL', 'DT', 'DD', 'DIV', 'TD', 'BLOCKQUOTE', 'OUTPUT', 'FIGCAPTION', 'ADDRESS', 'SECTION', 'HEADER', 'FOOTER', 'ASIDE', 'ARTICLE'],
		blockLevelElements: ['PRE', 'UL', 'OL', 'LI'],
		highContrast: false,
		observe: {
			dropdowns: []
		},

		// lang
		langs: {
			en: {
				html: 'HTML',
				video: 'Insert Video',
				image: 'Insert Image',
				table: 'Table',
				link: 'Link',
				link_insert: 'Insert link',
				link_edit: 'Edit link',
				unlink: 'Unlink',
				formatting: 'Formatting',
				paragraph: 'Normal text',
				quote: 'Quote',
				code: 'Code',
				header1: 'Header 1',
				header2: 'Header 2',
				header3: 'Header 3',
				header4: 'Header 4',
				header5: 'Header 5',
				bold: 'Bold',
				italic: 'Italic',
				fontcolor: 'Font Color',
				backcolor: 'Back Color',
				unorderedlist: 'Unordered List',
				orderedlist: 'Ordered List',
				outdent: 'Outdent',
				indent: 'Indent',
				cancel: 'Cancel',
				insert: 'Insert',
				save: 'Save',
				_delete: 'Delete',
				insert_table: 'Insert Table',
				insert_row_above: 'Add Row Above',
				insert_row_below: 'Add Row Below',
				insert_column_left: 'Add Column Left',
				insert_column_right: 'Add Column Right',
				delete_column: 'Delete Column',
				delete_row: 'Delete Row',
				delete_table: 'Delete Table',
				rows: 'Rows',
				columns: 'Columns',
				add_head: 'Add Head',
				delete_head: 'Delete Head',
				title: 'Title',
				image_position: 'Position',
				none: 'None',
				left: 'Left',
				right: 'Right',
				center: 'Center',
				image_web_link: 'Image Web Link',
				text: 'Text',
				mailto: 'Email',
				web: 'URL',
				video_html_code: 'Video Embed Code or Youtube/Vimeo Link',
				file: 'Insert File',
				upload: 'Upload',
				download: 'Download',
				choose: 'Choose',
				or_choose: 'Or choose',
				drop_file_here: 'Drop file here',
				align_left: 'Align text to the left',
				align_center: 'Center text',
				align_right: 'Align text to the right',
				align_justify: 'Justify text',
				horizontalrule: 'Insert Horizontal Rule',
				deleted: 'Deleted',
				anchor: 'Anchor',
				link_new_tab: 'Open link in new tab',
				underline: 'Underline',
				alignment: 'Alignment',
				filename: 'Name (optional)',
				edit: 'Edit',
				upload_label: 'Drop file here or '
			}
		},

		linkify: {
			regexps: {
				youtube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.\-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig,
				vimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
				image: /((https?|www)[^\s]+\.)(jpe?g|png|gif)(\?[^\s-]+)?/ig,
				url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/ig,
			}
		},

		codemirror: false
	};

	// Functionality
	Redactor.fn = $.Redactor.prototype = {

		keyCode: {
			BACKSPACE: 8,
			DELETE: 46,
			UP: 38,
			DOWN: 40,
			ENTER: 13,
			SPACE: 32,
			ESC: 27,
			TAB: 9,
			CTRL: 17,
			META: 91,
			SHIFT: 16,
			ALT: 18,
			RIGHT: 39,
			LEFT: 37,
			LEFT_WIN: 91
		},

		// Initialization
		init: function(el, options)
		{
			this.$element = $(el);
			this.uuid = uuid++;

			// if paste event detected = true
			this.rtePaste = false;
			this.$pasteBox = false;

			this.loadOptions(options);
			this.loadModules();

			// formatting storage
			this.formatting = {};

			// block level tags
			$.merge(this.opts.blockLevelElements, this.opts.alignmentTags);
			this.reIsBlock = new RegExp('^(' + this.opts.blockLevelElements.join('|' ) + ')$', 'i');

			// setup allowed and denied tags
			this.tidy.setupAllowed();

			// setup denied tags
			if (this.opts.deniedTags !== false)
			{
				var tags = ['html', 'head', 'link', 'body', 'meta', 'applet'];
				for (var i = 0; i < tags.length; i++)
				{
					this.opts.deniedTags.push(tags[i]);
				}
			}

			// load lang
			this.lang.load();

			// extend shortcuts
			$.extend(this.opts.shortcuts, this.opts.shortcutsAdd);

			// start callback
			this.core.setCallback('start');

			// build
			this.start = true;
			this.build.run();
		},

		loadOptions: function(options)
		{
			this.opts = $.extend(
				{},
				$.extend(true, {}, $.Redactor.opts),
				this.$element.data(),
				options
			);
		},
		getModuleMethods: function(object)
		{
			return Object.getOwnPropertyNames(object).filter(function(property)
			{
				return typeof object[property] == 'function';
			});
		},
		loadModules: function()
		{
			var len = $.Redactor.modules.length;
			for (var i = 0; i < len; i++)
			{
				this.bindModuleMethods($.Redactor.modules[i]);
			}
		},
		bindModuleMethods: function(module)
		{
			if (typeof this[module] == 'undefined') return;

			// init module
			this[module] = this[module]();

			var methods = this.getModuleMethods(this[module]);
			var len = methods.length;

			// bind methods
			for (var z = 0; z < len; z++)
			{
				this[module][methods[z]] = this[module][methods[z]].bind(this);
			}
		},
		alignment: function()
		{
			return {
				left: function()
				{
					this.alignment.set('');
				},
				right: function()
				{
					this.alignment.set('right');
				},
				center: function()
				{
					this.alignment.set('center');
				},
				justify: function()
				{
					this.alignment.set('justify');
				},
				set: function(type)
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					// get blocks
					this.alignment.blocks = this.selection.getBlocks();
					this.alignment.type = type;

					// set alignment
					if (this.alignment.isLinebreaksOrNoBlocks())
					{
						this.alignment.setText();
					}
					else
					{
						this.alignment.setBlocks();
					}

					// sync
					this.selection.restore();
					this.code.sync();
				},
				setText: function()
				{
					var wrapper = this.selection.wrap('div');
					$(wrapper).attr('data-tagblock', 'redactor').css('text-align', this.alignment.type);
				},
				setBlocks: function()
				{
					$.each(this.alignment.blocks, $.proxy(function(i, el)
					{
						var $el = this.utils.getAlignmentElement(el);
						if (!$el) return;

						if (this.alignment.isNeedReplaceElement($el))
						{
							this.alignment.replaceElement($el);
						}
						else
						{
							this.alignment.alignElement($el);
						}

					}, this));
				},
				isLinebreaksOrNoBlocks: function()
				{
					return (this.opts.linebreaks && this.alignment.blocks[0] === false);
				},
				isNeedReplaceElement: function($el)
				{
					return (this.alignment.type === '' && typeof($el.data('tagblock')) !== 'undefined');
				},
				replaceElement: function($el)
				{
					$el.replaceWith($el.html());
				},
				alignElement: function($el)
				{
					$el.css('text-align', this.alignment.type);
					this.utils.removeEmptyAttr($el, 'style');
				}
			};
		},
		autosave: function()
		{
			return {
				html: false,
				enable: function()
				{
					if (!this.opts.autosave) return;

					this.autosave.name = (this.opts.autosaveName) ? this.opts.autosaveName : this.$textarea.attr('name');

					if (this.opts.autosaveOnChange) return;
					this.autosaveInterval = setInterval(this.autosave.load, this.opts.autosaveInterval * 1000);
				},
				onChange: function()
				{
					if (!this.opts.autosaveOnChange) return;
					this.autosave.load();
				},
				load: function()
				{
					if (!this.opts.autosave) return;

					this.autosave.source = this.code.get();

					if (this.autosave.html === this.autosave.source) return;

					// data
					var data = {};
					data['name'] = this.autosave.name;
					data[this.autosave.name] = this.autosave.source;
					data = this.autosave.getHiddenFields(data);

					// ajax
					var jsxhr = $.ajax({
						url: this.opts.autosave,
						type: 'post',
						data: data
					});

					jsxhr.done(this.autosave.success);
				},
				getHiddenFields: function(data)
				{
					if (this.opts.autosaveFields === false || typeof this.opts.autosaveFields !== 'object')
					{
						return data;
					}

					$.each(this.opts.autosaveFields, $.proxy(function(k, v)
					{
						if (v !== null && v.toString().indexOf('#') === 0) v = $(v).val();
						data[k] = v;

					}, this));

					return data;

				},
				success: function(data)
				{
					var json;
					try
					{
						json = $.parseJSON(data);
					}
					catch(e)
					{
						//data has already been parsed
						json = data;
					}

					var callbackName = (typeof json.error == 'undefined') ? 'autosave' :  'autosaveError';

					this.core.setCallback(callbackName, this.autosave.name, json);
					this.autosave.html = this.autosave.source;
				},
				disable: function()
				{
					clearInterval(this.autosaveInterval);
				}
			};
		},
		block: function()
		{
			return {
				formatting: function(name)
				{
					this.block.clearStyle = false;
					var type, value;

					if (typeof this.formatting[name].data != 'undefined') type = 'data';
					else if (typeof this.formatting[name].attr != 'undefined') type = 'attr';
					else if (typeof this.formatting[name]['class'] != 'undefined') type = 'class';

					if (typeof this.formatting[name].clear != 'undefined')
					{
						this.block.clearStyle = true;
					}

					if (type) value = this.formatting[name][type];

					this.block.format(this.formatting[name].tag, type, value);

				},
				format: function(tag, type, value)
				{
					if (tag == 'quote') tag = 'blockquote';

					var formatTags = ['p', 'pre', 'blockquote', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
					if ($.inArray(tag, formatTags) == -1) return;

					this.block.isRemoveInline = (tag == 'pre' || tag.search(/h[1-6]/i) != -1);

					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					var html = $.trim(this.$editor.html());
					this.block.isEmpty = this.utils.isEmpty(html);

					// FF focus
					if (this.utils.browser('mozilla') && !this.focus.isFocused())
					{
						if (this.block.isEmpty)
						{
							var $first;
							if (!this.opts.linebreaks)
							{
								$first = this.$editor.children().first();
								this.caret.setEnd($first);
							}
						}
					}

					this.block.blocks = this.selection.getBlocks();

					this.block.blocksSize = this.block.blocks.length;
					this.block.type = type;
					this.block.value = value;

					this.buffer.set();
					this.selection.save();

					this.block.set(tag);

					this.selection.restore();
					this.code.sync();
					this.observe.load();

				},
				set: function(tag)
				{

					this.selection.get();
					this.block.containerTag = this.range.commonAncestorContainer.tagName;

					if (this.range.collapsed)
					{
						this.block.setCollapsed(tag);
					}
					else
					{
						this.block.setMultiple(tag);
					}
				},
				setCollapsed: function(tag)
				{
					if (this.opts.linebreaks && this.block.isEmpty && tag != 'p')
					{
						var node = document.createElement(tag);
						this.$editor.html(node);
						this.caret.setEnd(node);

						return;
					}


					var block = this.block.blocks[0];
					if (block === false) return;

					if (block.tagName == 'LI')
					{
						if (tag != 'blockquote') return;

						this.block.formatListToBlockquote();
						return;
					}

					var isContainerTable = (this.block.containerTag  == 'TD' || this.block.containerTag  == 'TH');
					if (isContainerTable && !this.opts.linebreaks)
					{
						document.execCommand('formatblock', false, '<' + tag + '>');

						block = this.selection.getBlock();
						this.block.toggle($(block));

					}
					else if (block.tagName.toLowerCase() != tag)
					{
						if (this.opts.linebreaks && tag == 'p')
						{
							$(block).append('<br>');
							this.utils.replaceWithContents(block);
						}
						else
						{
							var $formatted = this.utils.replaceToTag(block, tag);

							this.block.toggle($formatted);

							if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();
							if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
							if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();

							this.block.formatTableWrapping($formatted);
						}
					}
					else if (tag == 'blockquote' && block.tagName.toLowerCase() == tag)
					{
						// blockquote off
						if (this.opts.linebreaks)
						{
							$(block).append('<br>');
							this.utils.replaceWithContents(block);
						}
						else
						{
							var $el = this.utils.replaceToTag(block, 'p');
							this.block.toggle($el);
						}
					}
					else if (block.tagName.toLowerCase() == tag)
					{
						this.block.toggle($(block));
					}


					if (typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
					{
						$(block).removeAttr('class').removeAttr('style');
					}
				},
				setMultiple: function(tag)
				{
					var block = this.block.blocks[0];
					var isContainerTable = (this.block.containerTag  == 'TD' || this.block.containerTag  == 'TH');

					if (block !== false && this.block.blocksSize === 1)
					{
						if (block.tagName.toLowerCase() == tag &&  tag == 'blockquote')
						{
							// blockquote off
							if (this.opts.linebreaks)
							{
								$(block).append('<br>');
								this.utils.replaceWithContents(block);
							}
							else
							{
								var $el = this.utils.replaceToTag(block, 'p');
								this.block.toggle($el);
							}
						}
						else if (block.tagName == 'LI')
						{
							if (tag != 'blockquote') return;

							this.block.formatListToBlockquote();
						}
						else if (this.block.containerTag == 'BLOCKQUOTE')
						{
							this.block.formatBlockquote(tag);
						}
						else if (this.opts.linebreaks && ((isContainerTable) || (this.range.commonAncestorContainer != block)))
						{
							this.block.formatWrap(tag);
						}
						else
						{
							if (this.opts.linebreaks && tag == 'p')
							{
								$(block).prepend('<br>').append('<br>');
								this.utils.replaceWithContents(block);
							}
							else if (block.tagName === 'TD')
							{
								this.block.formatWrap(tag);
							}
							else
							{
								var $formatted = this.utils.replaceToTag(block, tag);

								this.block.toggle($formatted);

								if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
								if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();
							}
						}
					}
					else
					{
						if (this.opts.linebreaks || tag != 'p')
						{
							if (tag == 'blockquote')
							{
								var count = 0;
								for (var i = 0; i < this.block.blocksSize; i++)
								{
									if (this.block.blocks[i].tagName == 'BLOCKQUOTE') count++;
								}

								// only blockquote selected
								if (count == this.block.blocksSize)
								{
									$.each(this.block.blocks, $.proxy(function(i,s)
									{
										var $formatted = false;
										if (this.opts.linebreaks)
										{
											$(s).prepend('<br>').append('<br>');
											$formatted = this.utils.replaceWithContents(s);
										}
										else
										{
											$formatted = this.utils.replaceToTag(s, 'p');
										}

										if ($formatted && typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
										{
											$formatted.removeAttr('class').removeAttr('style');
										}

									}, this));

									return;
								}

							}

							this.block.formatWrap(tag);
						}
						else
						{
							var classSize = 0;
							var toggleType = false;
							if (this.block.type == 'class')
							{
								toggleType = 'toggle';
								classSize = $(this.block.blocks).filter('.' + this.block.value).length;

								if (this.block.blocksSize == classSize) toggleType = 'toggle';
								else if (this.block.blocksSize > classSize) toggleType = 'set';
								else if (classSize === 0) toggleType = 'set';

							}

							var exceptTags = ['ul', 'ol', 'li', 'td', 'th', 'dl', 'dt', 'dd'];
							$.each(this.block.blocks, $.proxy(function(i,s)
							{
								if ($.inArray(s.tagName.toLowerCase(), exceptTags) != -1) return;

								var $formatted = this.utils.replaceToTag(s, tag);

								if (toggleType)
								{
									if (toggleType == 'toggle') this.block.toggle($formatted);
									else if (toggleType == 'remove') this.block.remove($formatted);
									else if (toggleType == 'set') this.block.setForce($formatted);
								}
								else this.block.toggle($formatted);

								if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();
								if (this.block.isRemoveInline) this.utils.removeInlineTags($formatted);
								if (tag == 'p' || this.block.headTag) $formatted.find('p').contents().unwrap();

								if (typeof this.block.type == 'undefined' && typeof this.block.value == 'undefined')
								{
									$formatted.removeAttr('class').removeAttr('style');
								}


							}, this));
						}
					}
				},
				setForce: function($el)
				{
					// remove style and class if the specified setting
					if (this.block.clearStyle)
					{
						$el.removeAttr('class').removeAttr('style');
					}

					if (this.block.type == 'class')
					{
						$el.addClass(this.block.value);
						return;
					}
					else if (this.block.type == 'attr' || this.block.type == 'data')
					{
						$el.attr(this.block.value.name, this.block.value.value);
						return;
					}
				},
				toggle: function($el)
				{
					// remove style and class if the specified setting
					if (this.block.clearStyle)
					{
						$el.removeAttr('class').removeAttr('style');
					}

					if (this.block.type == 'class')
					{
						$el.toggleClass(this.block.value);
						return;
					}
					else if (this.block.type == 'attr' || this.block.type == 'data')
					{
						if ($el.attr(this.block.value.name) == this.block.value.value)
						{
							$el.removeAttr(this.block.value.name);
						}
						else
						{
							$el.attr(this.block.value.name, this.block.value.value);
						}

						return;
					}
					else
					{
						$el.removeAttr('style class');
						return;
					}
				},
				remove: function($el)
				{
					$el.removeClass(this.block.value);
				},
				formatListToBlockquote: function()
				{
					var block = $(this.block.blocks[0]).closest('ul, ol', this.$editor[0]);

					$(block).find('ul, ol').contents().unwrap();
					$(block).find('li').append($('<br>')).contents().unwrap();

					var $el = this.utils.replaceToTag(block, 'blockquote');
					this.block.toggle($el);
				},
				formatBlockquote: function(tag)
				{
					document.execCommand('outdent');
					document.execCommand('formatblock', false, tag);

					this.clean.clearUnverified();
					this.$editor.find('p:empty').remove();

					var formatted = this.selection.getBlock();

					if (tag != 'p')
					{
						$(formatted).find('img').remove();
					}

					if (!this.opts.linebreaks)
					{
						this.block.toggle($(formatted));
					}

					this.$editor.find('ul, ol, tr, blockquote, p').each($.proxy(this.utils.removeEmpty, this));

					if (this.opts.linebreaks && tag == 'p')
					{
						this.utils.replaceWithContents(formatted);
					}

				},
				formatWrap: function(tag)
				{
					if (this.block.containerTag == 'UL' || this.block.containerTag == 'OL')
					{
						if (tag == 'blockquote')
						{
							this.block.formatListToBlockquote();
						}
						else
						{
							return;
						}
					}

					var formatted = this.selection.wrap(tag);
					if (formatted === false) return;

					var $formatted = $(formatted);

					this.block.formatTableWrapping($formatted);

					var $elements = $formatted.find(this.opts.blockLevelElements.join(',') + ', td, table, thead, tbody, tfoot, th, tr');

					$elements.contents().unwrap();

					if (tag != 'p' && tag != 'blockquote') $formatted.find('img').remove();

					$.each(this.block.blocks, $.proxy(this.utils.removeEmpty, this));

					$formatted.append(this.selection.getMarker(2));

					if (!this.opts.linebreaks)
					{
						this.block.toggle($formatted);
					}

					this.$editor.find('ul, ol, tr, blockquote, p').each($.proxy(this.utils.removeEmpty, this));
					$formatted.find('blockquote:empty').remove();

					if (this.block.isRemoveInline)
					{
						this.utils.removeInlineTags($formatted);
					}

					if (this.opts.linebreaks && tag == 'p')
					{
						this.utils.replaceWithContents($formatted);
					}

					if (this.opts.linebreaks)
					{
						var $next = $formatted.next().next();
						if ($next.size() != 0 && $next[0].tagName === 'BR')
						{
							$next.remove();
						}
					}



				},
				formatTableWrapping: function($formatted)
				{
					if ($formatted.closest('table', this.$editor[0]).length === 0) return;

					if ($formatted.closest('tr', this.$editor[0]).length === 0) $formatted.wrap('<tr>');
					if ($formatted.closest('td', this.$editor[0]).length === 0 && $formatted.closest('th').length === 0)
					{
						$formatted.wrap('<td>');
					}
				},
				removeData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeAttr('data-' + name);

					this.code.sync();
				},
				setData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).attr('data-' + name, value);

					this.code.sync();
				},
				toggleData: function(name, value)
				{
					var blocks = this.selection.getBlocks();
					$.each(blocks, function()
					{
						if ($(this).attr('data-' + name))
						{
							$(this).removeAttr('data-' + name);
						}
						else
						{
							$(this).attr('data-' + name, value);
						}
					});
				},
				removeAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeAttr(attr);

					this.code.sync();
				},
				setAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).attr(attr, value);

					this.code.sync();
				},
				toggleAttr: function(attr, value)
				{
					var blocks = this.selection.getBlocks();
					$.each(blocks, function()
					{
						if ($(this).attr(name))
						{
							$(this).removeAttr(name);
						}
						else
						{
							$(this).attr(name, value);
						}
					});
				},
				removeClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).removeClass(className);

					this.utils.removeEmptyAttr(blocks, 'class');

					this.code.sync();
				},
				setClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).addClass(className);

					this.code.sync();
				},
				toggleClass: function(className)
				{
					var blocks = this.selection.getBlocks();
					$(blocks).toggleClass(className);

					this.code.sync();
				}
			};
		},
		buffer: function()
		{
			return {
				set: function(type)
				{
					if (typeof type == 'undefined' || type == 'undo')
					{
						this.buffer.setUndo();
					}
					else
					{
						this.buffer.setRedo();
					}
				},
				setUndo: function()
				{
					this.selection.save();
					this.opts.buffer.push(this.$editor.html());
					this.selection.restore();
				},
				setRedo: function()
				{
					this.selection.save();
					this.opts.rebuffer.push(this.$editor.html());
					this.selection.restore();
				},
				getUndo: function()
				{
					this.$editor.html(this.opts.buffer.pop());
				},
				getRedo: function()
				{
					this.$editor.html(this.opts.rebuffer.pop());
				},
				add: function()
				{
					this.opts.buffer.push(this.$editor.html());
				},
				undo: function()
				{
					if (this.opts.buffer.length === 0) return;

					this.buffer.set('redo');
					this.buffer.getUndo();

					this.selection.restore();

					setTimeout($.proxy(this.observe.load, this), 50);
				},
				redo: function()
				{
					if (this.opts.rebuffer.length === 0) return;

					this.buffer.set('undo');
					this.buffer.getRedo();

					this.selection.restore();

					setTimeout($.proxy(this.observe.load, this), 50);
				}
			};
		},
		build: function()
		{
			return {
				run: function()
				{
					this.build.createContainerBox();
					this.build.loadContent();
					this.build.loadEditor();
					this.build.enableEditor();
					this.build.setCodeAndCall();
				},
				isTextarea: function()
				{
					return (this.$element[0].tagName === 'TEXTAREA');
				},
				createContainerBox: function()
				{
					this.$box = $('<div class="redactor-box" role="application" />');
				},
				createTextarea: function()
				{
					this.$textarea = $('<textarea />').attr('name', this.build.getTextareaName());
				},
				getTextareaName: function()
				{
					return ((typeof(name) == 'undefined')) ? 'content-' + this.uuid : this.$element.attr('id');
				},
				loadContent: function()
				{
					var func = (this.build.isTextarea()) ? 'val' : 'html';
					this.content = $.trim(this.$element[func]());
				},
				enableEditor: function()
				{
					this.$editor.attr({ 'contenteditable': true, 'dir': this.opts.direction });
				},
				loadEditor: function()
				{
					var func = (this.build.isTextarea()) ? 'fromTextarea' : 'fromElement';
					this.build[func]();
				},
				fromTextarea: function()
				{
					this.$editor = $('<div />');
					this.$textarea = this.$element;
					this.$box.insertAfter(this.$element).append(this.$editor).append(this.$element);
					this.$editor.addClass('redactor-editor');

					this.$element.hide();
				},
				fromElement: function()
				{
					this.$editor = this.$element;
					this.build.createTextarea();
					this.$box.insertAfter(this.$editor).append(this.$editor).append(this.$textarea);
					this.$editor.addClass('redactor-editor');

					this.$textarea.hide();
				},
				setCodeAndCall: function()
				{
					// set code
					this.code.set(this.content);

					this.build.setOptions();
					this.build.callEditor();

					// code mode
					if (this.opts.visual) return;
					setTimeout($.proxy(this.code.showCode, this), 200);
				},
				callEditor: function()
				{
					this.build.disableMozillaEditing();
					this.build.disableIeLinks();
					this.build.setEvents();
					this.build.setHelpers();

					// load toolbar
					if (this.opts.toolbar)
					{
						this.opts.toolbar = this.toolbar.init();
						this.toolbar.build();
					}

					// modal templates init
					this.modal.loadTemplates();

					// plugins
					this.build.plugins();

					// observers
					setTimeout($.proxy(this.observe.load, this), 4);

					// init callback
					this.core.setCallback('init');
				},
				setOptions: function()
				{
					// textarea direction
					$(this.$textarea).attr('dir', this.opts.direction);

					if (this.opts.linebreaks) this.$editor.addClass('redactor-linebreaks');

					if (this.opts.tabindex) this.$editor.attr('tabindex', this.opts.tabindex);

					if (this.opts.minHeight) this.$editor.css('minHeight', this.opts.minHeight);
					if (this.opts.maxHeight) this.$editor.css('maxHeight', this.opts.maxHeight);

				},
				setEventDropUpload: function(e)
				{
					e.preventDefault();

					if (!this.opts.dragImageUpload || !this.opts.dragFileUpload) return;

					var files = e.dataTransfer.files;
					this.upload.directUpload(files[0], e);
				},
				setEventDrop: function(e)
				{
					this.code.sync();
					setTimeout(this.clean.clearUnverified, 1);
					this.core.setCallback('drop', e);
				},
				setEvents: function()
				{
					// drop
					this.$editor.on('drop.redactor', $.proxy(function(e)
					{
						e = e.originalEvent || e;

						if (window.FormData === undefined || !e.dataTransfer) return true;

						if (e.dataTransfer.files.length === 0)
						{
							return this.build.setEventDrop(e);
						}
						else
						{
							this.build.setEventDropUpload(e);
						}

						setTimeout(this.clean.clearUnverified, 1);
						this.core.setCallback('drop', e);

					}, this));


					// click
					this.$editor.on('click.redactor', $.proxy(function(e)
					{
						var event = this.core.getEvent();
						var type = (event == 'click' || event == 'arrow') ? false : 'click';

						this.core.addEvent(type);
						this.utils.disableSelectAll();
						this.core.setCallback('click', e);

					}, this));

					// paste
					this.$editor.on('paste.redactor', $.proxy(this.paste.init, this));

					// cut
					this.$editor.on('cut.redactor', $.proxy(this.code.sync, this));

					// keydown
					this.$editor.on('keydown.redactor', $.proxy(this.keydown.init, this));

					// keyup
					this.$editor.on('keyup.redactor', $.proxy(this.keyup.init, this));

					// textarea keydown
					if ($.isFunction(this.opts.codeKeydownCallback))
					{
						this.$textarea.on('keydown.redactor-textarea', $.proxy(this.opts.codeKeydownCallback, this));
					}

					// textarea keyup
					if ($.isFunction(this.opts.codeKeyupCallback))
					{
						this.$textarea.on('keyup.redactor-textarea', $.proxy(this.opts.codeKeyupCallback, this));
					}

					// focus
					this.$editor.on('focus.redactor', $.proxy(function(e)
					{
						if ($.isFunction(this.opts.focusCallback)) this.core.setCallback('focus', e);

						if (this.selection.getCurrent() === false)
						{
							this.selection.get();
							this.range.setStart(this.$editor[0], 0);
							this.range.setEnd(this.$editor[0], 0);
							this.selection.addRange();
						}


					}, this));


					// blur
					$(document).on('mousedown.redactor-blur.' + this.uuid, $.proxy(function(e)
					{
						if (this.start) return;
						if (this.rtePaste) return;

						if ($(e.target).closest('.redactor-editor, .redactor-toolbar, .redactor-dropdown').size() !== 0)
						{
							return;
						}

						this.utils.disableSelectAll();
						if ($.isFunction(this.opts.blurCallback)) this.core.setCallback('blur', e);

					}, this));

				},
				setHelpers: function()
				{
					// linkify
					if (this.linkify.isEnabled())
					{
						this.linkify.format();
					}

					// placeholder
					this.placeholder.enable();

					// focus
					if (this.opts.focus) setTimeout(this.focus.setStart, 100);
					if (this.opts.focusEnd) setTimeout(this.focus.setEnd, 100);

				},
				plugins: function()
				{
					if (!this.opts.plugins) return;

					$.each(this.opts.plugins, $.proxy(function(i, s)
					{
						var func = (typeof RedactorPlugins !== 'undefined' && typeof RedactorPlugins[s] !== 'undefined') ? RedactorPlugins : Redactor.fn;

						if (!$.isFunction(func[s]))
						{
							return;
						}

						this[s] = func[s]();

						// get methods
						var methods = this.getModuleMethods(this[s]);
						var len = methods.length;

						// bind methods
						for (var z = 0; z < len; z++)
						{
							this[s][methods[z]] = this[s][methods[z]].bind(this);
						}

						if ($.isFunction(this[s].init))
						{
							this[s].init();
						}


					}, this));

				},
				disableMozillaEditing: function()
				{
					if (!this.utils.browser('mozilla')) return;

					// FF fix
					try {
						document.execCommand('enableObjectResizing', false, false);
						document.execCommand('enableInlineTableEditing', false, false);
					} catch (e) {}
				},
				disableIeLinks: function()
				{
					if (!this.utils.browser('msie')) return;

					// IE prevent converting links
					document.execCommand("AutoUrlDetect", false, false);
				}
			};
		},
		button: function()
		{
			return {
				build: function(btnName, btnObject)
				{
					var $button = $('<a href="#" class="re-icon re-' + btnName + '" rel="' + btnName + '" />').attr({'role': 'button', 'aria-label': btnObject.title, 'tabindex': '-1'});

					// click
					if (btnObject.func || btnObject.command || btnObject.dropdown)
					{
						this.button.setEvent($button, btnName, btnObject);
					}

					// dropdown
					if (btnObject.dropdown)
					{
						$button.addClass('redactor-toolbar-link-dropdown').attr('aria-haspopup', true);

						var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + ' redactor-dropdown-box-' + btnName + '" style="display: none;">');
						$button.data('dropdown', $dropdown);
						this.dropdown.build(btnName, $dropdown, btnObject.dropdown);
					}

					// tooltip
					if (this.utils.isDesktop())
					{
						this.button.createTooltip($button, btnName, btnObject.title);
					}

					return $button;
				},
				setEvent: function($button, btnName, btnObject)
				{
					$button.on('touchstart click', $.proxy(function(e)
					{
						if ($button.hasClass('redactor-button-disabled')) return false;

						var type = 'func';
						var callback = btnObject.func;

						if (btnObject.command)
						{
							type = 'command';
							callback = btnObject.command;
						}
						else if (btnObject.dropdown)
						{
							type = 'dropdown';
							callback = false;
						}

						this.button.onClick(e, btnName, type, callback);

					}, this));
				},
				createTooltip: function($button, name, title)
				{
					var $tooltip = $('<span>').addClass('redactor-toolbar-tooltip redactor-toolbar-tooltip-' + this.uuid + ' redactor-toolbar-tooltip-' + name).hide().html(title);
					$tooltip.appendTo('body');

					$button.on('mouseover', function()
					{
						if ($(this).hasClass('redactor-button-disabled')) return;

						var pos = $button.offset();

						$tooltip.show();
						$tooltip.css({
							top: (pos.top + $button.innerHeight()) + 'px',
							left: (pos.left + $button.innerWidth()/2 - $tooltip.innerWidth()/2) + 'px'
						});
					});

					$button.on('mouseout', function()
					{
						$tooltip.hide();
					});

				},
				onClick: function(e, btnName, type, callback)
				{
					this.button.caretOffset = this.caret.getOffset();

					e.preventDefault();

					$(document).find('.redactor-toolbar-tooltip').hide();

					if (this.utils.browser('msie')) e.returnValue = false;

					if (type == 'command') this.inline.format(callback);
					else if (type == 'dropdown') this.dropdown.show(e, btnName);
					else this.button.onClickCallback(e, callback, btnName);
				},
				onClickCallback: function(e, callback, btnName)
				{
					var func;

					if ($.isFunction(callback)) callback.call(this, btnName);
					else if (callback.search(/\./) != '-1')
					{
						func = callback.split('.');
						if (typeof this[func[0]] == 'undefined') return;

						this[func[0]][func[1]](btnName);
					}
					else this[callback](btnName);

					this.observe.buttons(e, btnName);
				},
				get: function(key)
				{
					return this.$toolbar.find('a.re-' + key);
				},
				setActive: function(key)
				{
					this.button.get(key).addClass('redactor-act');
				},
				setInactive: function(key)
				{
					this.button.get(key).removeClass('redactor-act');
				},
				setInactiveAll: function(key)
				{
					if (typeof key === 'undefined')
					{
						this.$toolbar.find('a.re-icon').removeClass('redactor-act');
					}
					else
					{
						this.$toolbar.find('a.re-icon').not('.re-' + key).removeClass('redactor-act');
					}
				},
				setActiveInVisual: function()
				{
					this.$toolbar.find('a.re-icon').not('a.re-html, a.re-fullscreen').removeClass('redactor-button-disabled');
				},
				setInactiveInCode: function()
				{
					this.$toolbar.find('a.re-icon').not('a.re-html, a.re-fullscreen').addClass('redactor-button-disabled');
				},
				changeIcon: function(key, classname)
				{
					this.button.get(key).addClass('re-' + classname);
				},
				removeIcon: function(key, classname)
				{
					this.button.get(key).removeClass('re-' + classname);
				},
				setAwesome: function(key, name)
				{
					var $button = this.button.get(key);
					$button.removeClass('redactor-btn-image').addClass('fa-redactor-btn');
					$button.html('<i class="fa ' + name + '"></i>');
				},
				addCallback: function($btn, callback)
				{
					if ($btn == "buffer") return;

					var type = (callback == 'dropdown') ? 'dropdown' : 'func';
					var key = $btn.attr('rel');
					$btn.on('touchstart click', $.proxy(function(e)
					{
						if ($btn.hasClass('redactor-button-disabled')) return false;
						this.button.onClick(e, key, type, callback);

					}, this));
				},
				addDropdown: function($btn, dropdown)
				{
					$btn.addClass('redactor-toolbar-link-dropdown').attr('aria-haspopup', true);

					var key = $btn.attr('rel');
					this.button.addCallback($btn, 'dropdown');

					var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + ' redactor-dropdown-box-' + key + '" style="display: none;">');
					$btn.data('dropdown', $dropdown);

					// build dropdown
					if (dropdown) this.dropdown.build(key, $dropdown, dropdown);

					return $dropdown;
				},
				add: function(key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');

					this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				addFirst: function(key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					this.$toolbar.prepend($('<li>').append(btn));

					return btn;
				},
				addAfter: function(afterkey, key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					var $btn = this.button.get(afterkey);

					if ($btn.length !== 0) $btn.parent().after($('<li>').append(btn));
					else this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				addBefore: function(beforekey, key, title)
				{
					if (!this.opts.toolbar) return;

					if (this.button.isMobileUndoRedo(key)) return "buffer";

					var btn = this.button.build(key, { title: title });
					btn.addClass('redactor-btn-image');
					var $btn = this.button.get(beforekey);

					if ($btn.length !== 0) $btn.parent().before($('<li>').append(btn));
					else this.$toolbar.append($('<li>').append(btn));

					return btn;
				},
				remove: function(key)
				{
					this.button.get(key).remove();
				},
				isMobileUndoRedo: function(key)
				{
					return (key == "undo" || key == "redo") && !this.utils.isDesktop();
				}
			};
		},
		caret: function()
		{
			return {
				setStart: function(node)
				{
					// inline tag
					if (!this.utils.isBlock(node))
					{
						var space = this.utils.createSpaceElement();

						$(node).prepend(space);
						this.caret.setEnd(space);
					}
					else
					{
						this.caret.set(node, 0, node, 0);
					}
				},
				setEnd: function(node)
				{
					node = node[0] || node;
					if (node.lastChild.nodeType == 1)
					{
						return this.caret.setAfter(node.lastChild);
					}

					this.caret.set(node, 1, node, 1);

				},
				set: function(orgn, orgo, focn, foco)
				{
					// focus
					// disabled in 10.0.7
					// if (!this.utils.browser('msie')) this.$editor.focus();

					orgn = orgn[0] || orgn;
					focn = focn[0] || focn;

					if (this.utils.isBlockTag(orgn.tagName) && orgn.innerHTML === '')
					{
						orgn.innerHTML = this.opts.invisibleSpace;
					}

					if (orgn.tagName == 'BR' && this.opts.linebreaks === false)
					{
						var parent = $(this.opts.emptyHtml)[0];
						$(orgn).replaceWith(parent);
						orgn = parent;
						focn = orgn;
					}

					this.selection.get();

					try
					{
						this.range.setStart(orgn, orgo);
						this.range.setEnd(focn, foco);
					}
					catch (e) {}

					this.selection.addRange();
				},
				setAfter: function(node)
				{
					try
					{
						var tag = $(node)[0].tagName;

						// inline tag
						if (tag != 'BR' && !this.utils.isBlock(node))
						{
							var space = this.utils.createSpaceElement();

							$(node).after(space);
							this.caret.setEnd(space);
						}
						else
						{
							if (tag != 'BR' && this.utils.browser('msie'))
							{
								this.caret.setStart($(node).next());
							}
							else
							{
								this.caret.setAfterOrBefore(node, 'after');
							}
						}
					}
					catch (e)
					{
						var space = this.utils.createSpaceElement();
						$(node).after(space);
						this.caret.setEnd(space);
					}
				},
				setBefore: function(node)
				{
					// block tag
					if (this.utils.isBlock(node))
					{
						this.caret.setEnd($(node).prev());
					}
					else
					{
						this.caret.setAfterOrBefore(node, 'before');
					}
				},
				setAfterOrBefore: function(node, type)
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					node = node[0] || node;

					this.selection.get();

					if (type == 'after')
					{
						try {

							this.range.setStartAfter(node);
							this.range.setEndAfter(node);
						}
						catch (e) {}
					}
					else
					{
						try {
							this.range.setStartBefore(node);
							this.range.setEndBefore(node);
						}
						catch (e) {}
					}


					this.range.collapse(false);
					this.selection.addRange();
				},
				getOffsetOfElement: function(node)
				{
					node = node[0] || node;

					this.selection.get();

					var cloned = this.range.cloneRange();
					cloned.selectNodeContents(node);
					cloned.setEnd(this.range.endContainer, this.range.endOffset);

					return $.trim(cloned.toString()).length;
				},
				getOffset: function()
				{
					var offset = 0;
				    var sel = window.getSelection();

				    if (sel.rangeCount > 0)
				    {
				        var range = window.getSelection().getRangeAt(0);
				        var caretRange = range.cloneRange();
				        caretRange.selectNodeContents(this.$editor[0]);
				        caretRange.setEnd(range.endContainer, range.endOffset);
				        offset = caretRange.toString().length;
				    }

					return offset;
				},
				setOffset: function(start, end)
				{
					if (typeof end == 'undefined') end = start;
					if (!this.focus.isFocused()) this.focus.setStart();

					var sel = this.selection.get();
					var node, offset = 0;
					var walker = document.createTreeWalker(this.$editor[0], NodeFilter.SHOW_TEXT, null, null);

					while (node == walker.nextNode())
					{
						offset += node.nodeValue.length;
						if (offset > start)
						{
							this.range.setStart(node, node.nodeValue.length + start - offset);
							start = Infinity;
						}

						if (offset >= end)
						{
							this.range.setEnd(node, node.nodeValue.length + end - offset);
							break;
						}
					}

					this.range.collapse(false);
					this.selection.addRange();
				},
				// deprecated
				setToPoint: function(start, end)
				{
					this.caret.setOffset(start, end);
				},
				getCoords: function()
				{
					return this.caret.getOffset();
				}
			};
		},
		clean: function()
		{
			return {
				onSet: function(html)
				{
					html = this.clean.savePreCode(html);

					// convert script tag
					html = html.replace(/<script(.*?[^>]?)>([\w\W]*?)<\/script>/gi, '<pre class="redactor-script-tag" style="display: none;" $1>$2</pre>');

					// replace dollar sign to entity
					html = html.replace(/\$/g, '&#36;');

					// replace special characters in links
					html = html.replace(/<a href="(.*?[^>]?)®(.*?[^>]?)">/gi, '<a href="$1&reg$2">');

					if (this.opts.replaceDivs && !this.opts.linebreaks) html = this.clean.replaceDivs(html);
					if (this.opts.linebreaks)  html = this.clean.replaceParagraphsToBr(html);

					// save form tag
					html = this.clean.saveFormTags(html);

					// convert font tag to span
					var $div = $('<div>');
					$div.html(html);
					var fonts = $div.find('font[style]');
					if (fonts.length !== 0)
					{
						fonts.replaceWith(function()
						{
							var $el = $(this);
							var $span = $('<span>').attr('style', $el.attr('style'));
							return $span.append($el.contents());
						});

						html = $div.html();
					}

					$div.remove();

					// remove font tag
					html = html.replace(/<font(.*?)>/gi, '');
					html = html.replace(/<\/font>/gi, '');

					// tidy html
					html = this.tidy.load(html);

					// paragraphize
					if (this.opts.paragraphize) html = this.paragraphize.load(html);

					// verified
					html = this.clean.setVerified(html);

					// convert inline tags
					html = this.clean.convertInline(html);

					html = html.replace(/&amp;/g, '&');

					return html;
				},
				onSync: function(html)
				{
					// remove spaces
					html = html.replace(/\u200B/g, '');
					html = html.replace(/&#x200b;/gi, '');

					if (this.opts.cleanSpaces)
					{
						html = html.replace(/&nbsp;/gi, ' ');
					}

					if (html.search(/^<p>(||\s||<br\s?\/?>||&nbsp;)<\/p>$/i) != -1)
					{
						return '';
					}

					// reconvert script tag
					html = html.replace(/<pre class="redactor-script-tag" style="display: none;"(.*?[^>]?)>([\w\W]*?)<\/pre>/gi, '<script$1>$2</script>');

					// restore form tag
					html = this.clean.restoreFormTags(html);

					var chars = {
						'\u2122': '&trade;',
						'\u00a9': '&copy;',
						'\u2026': '&hellip;',
						'\u2014': '&mdash;',
						'\u2010': '&dash;'
					};
					// replace special characters
					$.each(chars, function(i,s)
					{
						html = html.replace(new RegExp(i, 'g'), s);
					});

					// remove last br in FF
					if (this.utils.browser('mozilla'))
					{
						html = html.replace(/<br\s?\/?>$/gi, '');
					}

					// remove br in|of li tags
					html = html.replace(new RegExp('<br\\s?/?></li>', 'gi'), '</li>');
					html = html.replace(new RegExp('</li><br\\s?/?>', 'gi'), '</li>');

					// remove empty attributes
					html = html.replace(/<(.*?)rel="\s*?"(.*?[^>]?)>/gi, '<$1$2">');
					html = html.replace(/<(.*?)style="\s*?"(.*?[^>]?)>/gi, '<$1$2">');
					html = html.replace(/="">/gi, '>');
					html = html.replace(/""">/gi, '">');
					html = html.replace(/"">/gi, '">');

					// remove verified
					html = html.replace(/<div(.*?)data-tagblock="redactor"(.*?[^>])>/gi, '<div$1$2>');
					html = html.replace(/<(.*?) data-verified="redactor"(.*?[^>])>/gi, '<$1$2>');

					var $div = $("<div/>").html($.parseHTML(html, document, true));
					$div.find("span").removeAttr("rel");

					$div.find('pre .redactor-invisible-space').each(function()
					{
						$(this).contents().unwrap();
					});

					html = $div.html();

					// remove rel attribute from img
					html = html.replace(/<img(.*?[^>])rel="(.*?[^>])"(.*?[^>])>/gi, '<img$1$3>');
					html = html.replace(/<span class="redactor-invisible-space">(.*?)<\/span>/gi, '$1');

					html = html.replace(/ data-save-url="(.*?[^>])"/gi, '');

					// remove image resize
					html = html.replace(/<span(.*?)id="redactor-image-box"(.*?[^>])>([\w\W]*?)<img(.*?)><\/span>/gi, '$3<img$4>');
					html = html.replace(/<span(.*?)id="redactor-image-resizer"(.*?[^>])>(.*?)<\/span>/gi, '');
					html = html.replace(/<span(.*?)id="redactor-image-editter"(.*?[^>])>(.*?)<\/span>/gi, '');

					// remove font tag
					html = html.replace(/<font(.*?)>/gi, '');
					html = html.replace(/<\/font>/gi, '');

					// tidy html
					html = this.tidy.load(html);

					// link nofollow
					if (this.opts.linkNofollow)
					{
						html = html.replace(/<a(.*?)rel="nofollow"(.*?[^>])>/gi, '<a$1$2>');
						html = html.replace(/<a(.*?[^>])>/gi, '<a$1 rel="nofollow">');
					}

					// reconvert inline
					html = html.replace(/\sdata-redactor-(tag|class|style)="(.*?[^>])"/gi, '');
					html = html.replace(new RegExp('<(.*?) data-verified="redactor"(.*?[^>])>', 'gi'), '<$1$2>');
					html = html.replace(new RegExp('<(.*?) data-verified="redactor">', 'gi'), '<$1>');

					html = html.replace(/&amp;/g, '&');

					return html;
				},
				onPaste: function(html, setMode)
				{
					html = $.trim(html);
					html = html.replace(/\$/g, '&#36;');

					// convert dirty spaces
					html = html.replace(/<span class="s[0-9]">/gi, '<span>');
					html = html.replace(/<span class="Apple-converted-space">&nbsp;<\/span>/gi, ' ');
					html = html.replace(/<span class="Apple-tab-span"[^>]*>\t<\/span>/gi, '\t');
					html = html.replace(/<span[^>]*>(\s|&nbsp;)<\/span>/gi, ' ');

					if (this.opts.pastePlainText)
					{
						return this.clean.getPlainText(html);
					}

					if (!this.utils.isSelectAll() && typeof setMode == 'undefined')
					{
						if (this.utils.isCurrentOrParent(['FIGCAPTION', 'A']))
						{
							return this.clean.getPlainText(html, false);
						}

						if (this.utils.isCurrentOrParent('PRE'))
						{
							html = html.replace(/”/g, '"');
							html = html.replace(/“/g, '"');
							html = html.replace(/‘/g, '\'');
							html = html.replace(/’/g, '\'');

							return this.clean.getPreCode(html);
						}

						if (this.utils.isCurrentOrParent(['BLOCKQUOTE', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6']))
						{
							html = this.clean.getOnlyImages(html);

							if (!this.utils.browser('msie'))
							{
								var block = this.selection.getBlock();
								if (block && block.tagName == 'P')
								{
									html = html.replace(/<img(.*?)>/gi, '<p><img$1></p>');
								}
							}

							return html;
						}

						if (this.utils.isCurrentOrParent(['TD']))
						{
							html = this.clean.onPasteTidy(html, 'td');

							if (this.opts.linebreaks) html = this.clean.replaceParagraphsToBr(html);

							html = this.clean.replaceDivsToBr(html);

							return html;
						}


						if (this.utils.isCurrentOrParent(['LI']))
						{
							return this.clean.onPasteTidy(html, 'li');
						}
					}


					html = this.clean.isSingleLine(html, setMode);

					if (!this.clean.singleLine)
					{
						if (this.opts.linebreaks)  html = this.clean.replaceParagraphsToBr(html);
						if (this.opts.replaceDivs) html = this.clean.replaceDivs(html);

						html = this.clean.saveFormTags(html);
					}


					html = this.clean.onPasteWord(html);
					html = this.clean.onPasteExtra(html);

					html = this.clean.onPasteTidy(html, 'all');


					// paragraphize
					if (!this.clean.singleLine && this.opts.paragraphize)
					{
						html = this.paragraphize.load(html);
					}

					html = this.clean.removeDirtyStyles(html);
					html = this.clean.onPasteRemoveSpans(html);
					html = this.clean.onPasteRemoveEmpty(html);


					html = this.clean.convertInline(html);

					return html;
				},
				onPasteWord: function(html)
				{
					// comments
					html = html.replace(/<!--[\s\S]*?-->/gi, '');

					// style
					html = html.replace(/<style[^>]*>[\s\S]*?<\/style>/gi, '');

					// op
					html = html.replace(/<o\:p[^>]*>[\s\S]*?<\/o\:p>/gi, '');

					if (html.match(/class="?Mso|style="[^"]*\bmso-|style='[^'']*\bmso-|w:WordDocument/i))
					{
						// comments
						html = html.replace(/<!--[\s\S]+?-->/gi, '');

						// scripts
						html = html.replace(/<(!|script[^>]*>.*?<\/script(?=[>\s])|\/?(\?xml(:\w+)?|img|meta|link|style|\w:\w+)(?=[\s\/>]))[^>]*>/gi, '');

						// Convert <s> into <strike>
						html = html.replace(/<(\/?)s>/gi, "<$1strike>");

						// Replace nbsp entites to char since it's easier to handle
						html = html.replace(/ /gi, ' ');

						// Convert <span style="mso-spacerun:yes">___</span> to string of alternating
						// breaking/non-breaking spaces of same length
						html = html.replace(/<span\s+style\s*=\s*"\s*mso-spacerun\s*:\s*yes\s*;?\s*"\s*>([\s\u00a0]*)<\/span>/gi, function(str, spaces) {
							return (spaces.length > 0) ? spaces.replace(/./, " ").slice(Math.floor(spaces.length/2)).split("").join("\u00a0") : '';
						});

						html = this.clean.onPasteIeFixLinks(html);

						// shapes
						html = html.replace(/<img(.*?)v:shapes=(.*?)>/gi, '');
						html = html.replace(/src="file\:\/\/(.*?)"/, 'src=""');

						// lists
						var $div = $("<div/>").html(html);

						var lastList = false;
						var lastLevel = 1;
						var listsIds = [];

						$div.find("p[style]").each(function()
						{
							var matches = $(this).attr('style').match(/mso\-list\:l([0-9]+)\slevel([0-9]+)/);

							if (matches)
							{
								var currentList = parseInt(matches[1]);
								var currentLevel = parseInt(matches[2]);
								var listType = $(this).html().match(/^[\w]+\./) ? "ol" : "ul";

								var $li = $("<li/>").html($(this).html());

								$li.html($li.html().replace(/^([\w\.]+)</, '<'));
								$li.find("span:first").remove();

								if (currentLevel == 1 && $.inArray(currentList, listsIds) == -1)
								{
									var $list = $("<" + listType + "/>").attr({"data-level": currentLevel,
																			   "data-list": currentList})
																	  .html($li);

									$(this).replaceWith($list);

									lastList = currentList;
									listsIds.push(currentList);
								}
								else
								{
									if (currentLevel > lastLevel)
									{
										var $prevList = $div.find('[data-level="' + lastLevel + '"][data-list="' + lastList + '"]');

										var $lastList = $prevList;

										for(var i = lastLevel; i < currentLevel; i++)
										{
											$list = $("<" + listType + "/>");

											$list.appendTo($lastList.find("li").last());

											$lastList = $list;
										}

										$lastList.attr({"data-level": currentLevel,
														"data-list": currentList})
												 .html($li);

									}
									else
									{
										var $prevList = $div.find('[data-level="' + currentLevel + '"][data-list="' + currentList + '"]').last();

										$prevList.append($li);
									}

									lastLevel = currentLevel;
									lastList = currentList;

									$(this).remove();
								}
							}
						});

						$div.find('[data-level][data-list]').removeAttr('data-level data-list');
						html = $div.html();

						// remove ms word's bullet
						html = html.replace(/·/g, '');
						html = html.replace(/<p class="Mso(.*?)"/gi, '<p');

						// classes
						html = html.replace(/ class=\"(mso[^\"]*)\"/gi,	"");
						html = html.replace(/ class=(mso\w+)/gi, "");

						// remove ms word tags
						html = html.replace(/<o:p(.*?)>([\w\W]*?)<\/o:p>/gi, '$2');

						// ms word break lines
						html = html.replace(/\n/g, ' ');

						// ms word lists break lines
						html = html.replace(/<p>\n?<li>/gi, '<li>');
					}

					return html;
				},
				onPasteExtra: function(html)
				{
					// remove google docs markers
					html = html.replace(/<b\sid="internal-source-marker(.*?)">([\w\W]*?)<\/b>/gi, "$2");
					html = html.replace(/<b(.*?)id="docs-internal-guid(.*?)">([\w\W]*?)<\/b>/gi, "$3");

					// google docs styles
			 		html = html.replace(/<span[^>]*(font-style: italic; font-weight: bold|font-weight: bold; font-style: italic)[^>]*>/gi, '<span style="font-weight: bold;"><span style="font-style: italic;">');
			 		html = html.replace(/<span[^>]*font-style: italic[^>]*>/gi, '<span style="font-style: italic;">');
					html = html.replace(/<span[^>]*font-weight: bold[^>]*>/gi, '<span style="font-weight: bold;">');
					html = html.replace(/<span[^>]*text-decoration: underline[^>]*>/gi, '<span style="text-decoration: underline;">');

					html = html.replace(/<img>/gi, '');
					html = html.replace(/\n{3,}/gi, '\n');
					html = html.replace(/<font(.*?)>([\w\W]*?)<\/font>/gi, '$2');

					// remove dirty p
					html = html.replace(/<p><p>/gi, '<p>');
					html = html.replace(/<\/p><\/p>/gi, '</p>');
					html = html.replace(/<li>(\s*|\t*|\n*)<p>/gi, '<li>');
					html = html.replace(/<\/p>(\s*|\t*|\n*)<\/li>/gi, '</li>');

					// remove space between paragraphs
					html = html.replace(/<\/p>\s<p/gi, '<\/p><p');

					// remove safari local images
					html = html.replace(/<img src="webkit-fake-url\:\/\/(.*?)"(.*?)>/gi, '');

					// bullets
					html = html.replace(/<p>•([\w\W]*?)<\/p>/gi, '<li>$1</li>');

					// FF fix
					if (this.utils.browser('mozilla'))
					{
						html = html.replace(/<br\s?\/?>$/gi, '');
					}

					return html;
				},
				onPasteTidy: function(html, type)
				{
					// remove all tags except these
					var tags = ['span', 'a', 'pre', 'blockquote', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'address', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del',
								'ol', 'ul', 'li', 'dl', 'dt', 'dd', 'p', 'br', 'video', 'audio', 'iframe', 'embed', 'param', 'object', 'img', 'table',
								'td', 'th', 'tr', 'tbody', 'tfoot', 'thead', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
					var tagsEmpty = false;
					var attrAllowed =  [
							['a', '*'],
							['img', ['src', 'alt']],
							['span', ['class', 'rel', 'data-verified']],
							['iframe', '*'],
							['video', '*'],
							['audio', '*'],
							['embed', '*'],
							['object', '*'],
							['param', '*'],
							['source', '*']
						];

					if (type == 'all')
					{
						tagsEmpty = ['p', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
						attrAllowed =  [
							['table', 'class'],
							['td', ['colspan', 'rowspan']],
							['a', '*'],
							['img', ['src', 'alt', 'data-redactor-inserted-image']],
							['span', ['class', 'rel', 'data-verified']],
							['iframe', '*'],
							['video', '*'],
							['audio', '*'],
							['embed', '*'],
							['object', '*'],
							['param', '*'],
							['source', '*']
						];
					}
					else if (type == 'td')
					{
						// remove all tags except these and remove all table tags: tr, td etc
						tags = ['ul', 'ol', 'li', 'span', 'a', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del',
								'ol', 'ul', 'li', 'dl', 'dt', 'dd', 'br', 'iframe', 'video', 'audio', 'embed', 'param', 'object', 'img', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

					}
					else if (type == 'li')
					{
						// only inline tags and ul, ol, li
						tags = ['ul', 'ol', 'li', 'span', 'a', 'small', 'em', 'strong', 'code', 'kbd', 'mark', 'cite', 'var', 'samp', 'dfn', 'sup', 'sub', 'b', 'i', 'u', 'del', 'br',
								'iframe', 'video', 'audio', 'embed', 'param', 'object', 'img'];
					}

					var options = {
						deniedTags: (this.opts.deniedTags) ? this.opts.deniedTags : false,
						allowedTags: (this.opts.allowedTags) ? this.opts.allowedTags : tags,
						removeComments: true,
						removePhp: true,
						removeAttr: (this.opts.removeAttr) ? this.opts.removeAttr : false,
						allowedAttr: (this.opts.allowedAttr) ? this.opts.allowedAttr : attrAllowed,
						removeEmpty: tagsEmpty
					};

					return this.tidy.load(html, options);
				},
				onPasteRemoveEmpty: function(html)
				{
					html = html.replace(/<(p|h[1-6])>(|\s|\n|\t|<br\s?\/?>)<\/(p|h[1-6])>/gi, '');

					// remove br in the end
					if (!this.opts.linebreaks) html = html.replace(/<br>$/i, '');

					return html;
				},
				onPasteRemoveSpans: function(html)
				{
					html = html.replace(/<span>(.*?)<\/span>/gi, '$1');
					html = html.replace(/<span[^>]*>\s|&nbsp;<\/span>/gi, ' ');

					return html;
				},
				onPasteIeFixLinks: function(html)
				{
					if (!this.utils.browser('msie')) return html;

					var tmp = $.trim(html);
					if (tmp.search(/^<a(.*?)>(.*?)<\/a>$/i) === 0)
					{
						html = html.replace(/^<a(.*?)>(.*?)<\/a>$/i, "$2");
					}

					return html;
				},
				isSingleLine: function(html, setMode)
				{
					this.clean.singleLine = false;

					if (!this.utils.isSelectAll() && typeof setMode == 'undefined')
					{
						var blocks = this.opts.blockLevelElements.join('|').replace('P|', '').replace('DIV|', '');

						var matchBlocks = html.match(new RegExp('</(' + blocks + ')>', 'gi'));
						var matchContainers = html.match(/<\/(p|div)>/gi);

						if (!matchBlocks && (matchContainers === null || (matchContainers && matchContainers.length <= 1)))
						{
							var matchBR = html.match(/<br\s?\/?>/gi);
							//var matchIMG = html.match(/<img(.*?[^>])>/gi);
							if (!matchBR)
							{
								this.clean.singleLine = true;
								html = html.replace(/<\/?(p|div)(.*?)>/gi, '');
							}
						}
					}

					return html;
				},
				stripTags: function(input, allowed)
				{
				    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
				    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;

				    return input.replace(tags, function ($0, $1) {
				        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
				    });
				},
				savePreCode: function(html)
				{
					html = this.clean.savePreFormatting(html);
					html = this.clean.saveCodeFormatting(html);

					html = this.clean.restoreSelectionMarker(html);

					return html;
				},
				savePreFormatting: function(html)
				{
					var pre = html.match(/<pre(.*?)>([\w\W]*?)<\/pre>/gi);

					if (pre !== null)
					{
						$.each(pre, $.proxy(function(i,s)
						{
							var arr = s.match(/<pre(.*?)>([\w\W]*?)<\/pre>/i);

							arr[2] = arr[2].replace(/<br\s?\/?>/g, '\n');
							arr[2] = arr[2].replace(/&nbsp;/g, ' ');

							if (this.opts.preSpaces)
							{
								arr[2] = arr[2].replace(/\t/g, Array(this.opts.preSpaces + 1).join(' '));
							}

							arr[2] = this.clean.encodeEntities(arr[2]);

							// $ fix
							arr[2] = arr[2].replace(/\$/g, '&#36;');

							html = html.replace(s, '<pre' + arr[1] + '>' + arr[2] + '</pre>');

						}, this));
					}

					return html;
				},
				saveCodeFormatting: function(html)
				{
					var code = html.match(/<code(.*?)>([\w\W]*?)<\/code>/gi);

					if (code !== null)
					{
						$.each(code, $.proxy(function(i,s)
						{
							var arr = s.match(/<code(.*?)>([\w\W]*?)<\/code>/i);

							arr[2] = arr[2].replace(/&nbsp;/g, ' ');
							arr[2] = this.clean.encodeEntities(arr[2]);
							arr[2] = arr[2].replace(/\$/g, '&#36;');

							html = html.replace(s, '<code' + arr[1] + '>' + arr[2] + '</code>');
						}, this));
					}

					return html;
				},
				restoreSelectionMarker: function(html)
				{
					html = html.replace(/&lt;span id=&quot;selection-marker-([0-9])&quot; class=&quot;redactor-selection-marker&quot; data-verified=&quot;redactor&quot;&gt;​&lt;\/span&gt;/g, '<span id="selection-marker-$1" class="redactor-selection-marker" data-verified="redactor">​</span>');

					return html;
				},
				getTextFromHtml: function(html)
				{
					html = html.replace(/<br\s?\/?>|<\/H[1-6]>|<\/p>|<\/div>|<\/li>|<\/td>/gi, '\n');

					var tmp = document.createElement('div');
					tmp.innerHTML = html;
					html = tmp.textContent || tmp.innerText;

					return $.trim(html);
				},
				getPlainText: function(html, paragraphize)
				{
					html = this.clean.getTextFromHtml(html);
					html = html.replace(/\n/g, '<br />');

					if (this.opts.paragraphize && typeof paragraphize == 'undefined' && !this.utils.browser('mozilla'))
					{
						html = this.paragraphize.load(html);
					}

					return html;
				},
				getPreCode: function(html)
				{
					html = html.replace(/<img(.*?) style="(.*?)"(.*?[^>])>/gi, '<img$1$3>');
					html = html.replace(/<img(.*?)>/gi, '&lt;img$1&gt;');
					html = this.clean.getTextFromHtml(html);

					if (this.opts.preSpaces)
					{
						html = html.replace(/\t/g, Array(this.opts.preSpaces + 1).join(' '));
					}

					html = this.clean.encodeEntities(html);

					return html;
				},
				getOnlyImages: function(html)
				{
					html = html.replace(/<img(.*?)>/gi, '[img$1]');

					// remove all tags
					html = html.replace(/<([Ss]*?)>/gi, '');

					html = html.replace(/\[img(.*?)\]/gi, '<img$1>');

					return html;
				},
				getOnlyLinksAndImages: function(html)
				{
					html = html.replace(/<a(.*?)href="(.*?)"(.*?)>([\w\W]*?)<\/a>/gi, '[a href="$2"]$4[/a]');
					html = html.replace(/<img(.*?)>/gi, '[img$1]');

					// remove all tags
					html = html.replace(/<(.*?)>/gi, '');

					html = html.replace(/\[a href="(.*?)"\]([\w\W]*?)\[\/a\]/gi, '<a href="$1">$2</a>');
					html = html.replace(/\[img(.*?)\]/gi, '<img$1>');

					return html;
				},
				encodeEntities: function(str)
				{
					str = String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
					return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
				},
				removeDirtyStyles: function(html)
				{
					if (this.utils.browser('msie')) return html;

					var div = document.createElement('div');
					div.innerHTML = html;

					this.clean.clearUnverifiedRemove($(div));

					html = div.innerHTML;
					$(div).remove();

					return html;
				},
				clearUnverified: function()
				{
					if (this.utils.browser('msie')) return;

					this.clean.clearUnverifiedRemove(this.$editor);

					var headers = this.$editor.find('h1, h2, h3, h4, h5, h6');
					headers.find('span').removeAttr('style');
					headers.find(this.opts.verifiedTags.join(', ')).removeAttr('style');

					this.code.sync();
				},
				clearUnverifiedRemove: function($editor)
				{
					$editor.find(this.opts.verifiedTags.join(', ')).removeAttr('style');
					$editor.find('span').not('[data-verified="redactor"]').removeAttr('style');

					$editor.find('span[data-verified="redactor"], img[data-verified="redactor"]').each(function(i, s)
					{
						var $s = $(s);
						$s.attr('style', $s.attr('rel'));
					});

				},
				cleanEmptyParagraph: function()
				{

				},
				setVerified: function(html)
				{
					if (this.utils.browser('msie')) return html;

					html = html.replace(new RegExp('<img(.*?[^>])>', 'gi'), '<img$1 data-verified="redactor">');
					html = html.replace(new RegExp('<span(.*?[^>])>', 'gi'), '<span$1 data-verified="redactor">');

					var matches = html.match(new RegExp('<(span|img)(.*?)style="(.*?)"(.*?[^>])>', 'gi'));

					if (matches)
					{
						var len = matches.length;
						for (var i = 0; i < len; i++)
						{
							try {

								var newTag = matches[i].replace(/style="(.*?)"/i, 'style="$1" rel="$1"');
								html = html.replace(matches[i], newTag);

							}
							catch (e) {}
						}
					}

					return html;
				},
				convertInline: function(html)
				{
					var $div = $('<div />').html(html);

					var tags = this.opts.inlineTags;
					tags.push('span');

					$div.find(tags.join(',')).each(function()
					{
						var $el = $(this);
						var tag = this.tagName.toLowerCase();
						$el.attr('data-redactor-tag', tag);

						if (tag == 'span')
						{
							if ($el.attr('style')) $el.attr('data-redactor-style', $el.attr('style'));
							else if ($el.attr('class')) $el.attr('data-redactor-class', $el.attr('class'));
						}

					});

					html = $div.html();
					$div.remove();

					return html;
				},
				normalizeLists: function()
				{
					this.$editor.find('li').each(function(i,s)
					{
						var $next = $(s).next();
						if ($next.length !== 0 && ($next[0].tagName == 'UL' || $next[0].tagName == 'OL'))
						{
							$(s).append($next);
						}

					});
				},
				removeSpaces: function(html)
				{
					html = html.replace(/\n/g, '');
					html = html.replace(/[\t]*/g, '');
					html = html.replace(/\n\s*\n/g, "\n");
					html = html.replace(/^[\s\n]*/g, ' ');
					html = html.replace(/[\s\n]*$/g, ' ');
					html = html.replace( />\s{2,}</g, '> <'); // between inline tags can be only one space
					html = html.replace(/\n\n/g, "\n");
					html = html.replace(/\u200B/g, '');

					return html;
				},
				replaceDivs: function(html)
				{
					if (this.opts.linebreaks)
					{
						html = html.replace(/<div><br\s?\/?><\/div>/gi, '<br />');
						html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, '$2<br />');
					}
					else
					{
						html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, '<p$1>$2</p>');
					}

					html = html.replace(/<div(.*?[^>])>/gi, '');
					html = html.replace(/<\/div>/gi, '');

					return html;
				},
				replaceDivsToBr: function(html)
				{
					html = html.replace(/<div\s(.*?)>/gi, '<p>');
					html = html.replace(/<div><br\s?\/?><\/div>/gi, '<br /><br />');
					html = html.replace(/<div>([\w\W]*?)<\/div>/gi, '$1<br /><br />');

					return html;
				},
				replaceParagraphsToBr: function(html)
				{
					html = html.replace(/<p\s(.*?)>/gi, '<p>');
					html = html.replace(/<p><br\s?\/?><\/p>/gi, '<br />');
					html = html.replace(/<p>([\w\W]*?)<\/p>/gi, '$1<br /><br />');
					html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, '</blockquote>');

					return html;
				},
				saveFormTags: function(html)
				{
					return html.replace(/<form(.*?)>([\w\W]*?)<\/form>/gi, '<section$1 rel="redactor-form-tag">$2</section>');
				},
				restoreFormTags: function(html)
				{
					return html.replace(/<section(.*?) rel="redactor-form-tag"(.*?)>([\w\W]*?)<\/section>/gi, '<form$1$2>$3</form>');
				}
			};
		},
		code: function()
		{
			return {
				set: function(html)
				{
					html = $.trim(html.toString());

					// clean
					html = this.clean.onSet(html);


					if (this.utils.browser('msie'))
					{
						html = html.replace(/<span(.*?)id="selection-marker-(1|2)"(.*?)><\/span>/gi, '');
					}

					this.$editor.html(html);
					this.code.sync();

					if (html !== '') this.placeholder.remove();

					setTimeout($.proxy(this.buffer.add, this), 15);
					if (this.start === false) this.observe.load();

				},
				get: function()
				{
					var code = this.$textarea.val();

					if (this.opts.replaceDivs) code = this.clean.replaceDivs(code);
					if (this.opts.linebreaks) code = this.clean.replaceParagraphsToBr(code);

					// indent code
					code = this.tabifier.get(code);

					return code;
				},
				sync: function()
				{
					setTimeout($.proxy(this.code.startSync, this), 10);
				},
				startSync: function()
				{
					var html = this.$editor.html();

					// is there a need to synchronize
					if (this.code.syncCode && this.code.syncCode == html)
					{
						// do not sync
						return;
					}

					// save code
					this.code.syncCode = html;

					// before clean callback
					html = this.core.setCallback('syncBefore', html);

					// clean
					html = this.clean.onSync(html);

					// set code
					this.$textarea.val(html);

					// after sync callback
					this.core.setCallback('sync', html);

					if (this.start === false)
					{
						this.core.setCallback('change', html);
					}

					this.start = false;

					if (this.autosave.html == false)
					{
						this.autosave.html = this.code.get();
					}

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							el.CodeMirror.setValue(html);
						});
					}

					//autosave
					this.autosave.onChange();
					this.autosave.enable();
				},
				toggle: function()
				{
					if (this.opts.visual)
					{
						this.code.showCode();
					}
					else
					{
						this.code.showVisual();
					}
				},
				showCode: function()
				{
					this.selection.save();

					this.code.offset = this.caret.getOffset();
					var scroll = $(window).scrollTop();

					var	width = this.$editor.innerWidth(),
						height = this.$editor.innerHeight();

					this.$editor.hide();

					var html = this.$textarea.val();

					this.modified = this.clean.removeSpaces(html);

					// indent code
					html = this.tabifier.get(html);

					// caret position sync
					var start = 0, end = 0;
					var $editorDiv = $("<div/>").append($.parseHTML(this.clean.onSync(this.$editor.html()), document, true));
					var $selectionMarkers = $editorDiv.find("span.redactor-selection-marker");

					if ($selectionMarkers.length > 0)
					{
						var editorHtml = this.tabifier.get($editorDiv.html()).replace(/&amp;/g, '&');

						if ($selectionMarkers.length == 1)
						{
							start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML"));
							end   = start;
						}
						else if ($selectionMarkers.length == 2)
						{
							start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML"));
							end	 = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-2").prop("outerHTML")) - $editorDiv.find("#selection-marker-1").prop("outerHTML").toString().length;
						}
					}

					this.selection.removeMarkers();
					this.$textarea.val(html);

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							$(el).show();
							el.CodeMirror.setValue(html);
							el.CodeMirror.setSize('100%', height);
							el.CodeMirror.refresh();

							if (start == end)
							{
								el.CodeMirror.setCursor(el.CodeMirror.posFromIndex(start).line, el.CodeMirror.posFromIndex(end).ch);
							}
							else
							{
								el.CodeMirror.setSelection({line: el.CodeMirror.posFromIndex(start).line,
															ch: el.CodeMirror.posFromIndex(start).ch},
														  {line: el.CodeMirror.posFromIndex(end).line,
														   ch:  el.CodeMirror.posFromIndex(end).ch});
							}

							el.CodeMirror.focus();
						});
					}
					else
					{
						this.$textarea.height(height).show().focus();
						this.$textarea.on('keydown.redactor-textarea-indenting', this.code.textareaIndenting);

						$(window).scrollTop(scroll);

						if (this.$textarea[0].setSelectionRange)
						{
							this.$textarea[0].setSelectionRange(start, end);
						}

						this.$textarea[0].scrollTop = 0;
					}

					this.opts.visual = false;

					this.button.setInactiveInCode();
					this.button.setActive('html');
					this.core.setCallback('source', html);
				},
				showVisual: function()
				{
					var html;

					if (this.opts.visual) return;

					var start = 0, end = 0;

					if (this.opts.codemirror)
					{
						var selection;

						this.$textarea.next('.CodeMirror').each(function(i, el)
						{
							selection = el.CodeMirror.listSelections();

							start = el.CodeMirror.indexFromPos(selection[0].anchor);
							end = el.CodeMirror.indexFromPos(selection[0].head);

							html = el.CodeMirror.getValue();
						});
					}
					else
					{
						start = this.$textarea.get(0).selectionStart;
						end = this.$textarea.get(0).selectionEnd;

						html = this.$textarea.hide().val();
					}

					// if selection starts from end
					if (start > end && end > 0)
					{
						var tempStart = end;
						var tempEnd = start;

						start = tempStart;
						end = tempEnd;
					}

					start = this.code.enlargeOffset(html, start);
					end = this.code.enlargeOffset(html, end);

					html = html.substr(0, start) + this.selection.getMarkerAsHtml(1) + html.substr(start);

					if (end > start)
					{
						var markerLength = this.selection.getMarkerAsHtml(1).toString().length;

						html = html.substr(0, end + markerLength) + this.selection.getMarkerAsHtml(2) + html.substr(end + markerLength);
					}



					if (this.modified !== this.clean.removeSpaces(html))
					{
						this.code.set(html);

					}

					if (this.opts.codemirror)
					{
						this.$textarea.next('.CodeMirror').hide();
					}

					this.$editor.show();

					if (!this.utils.isEmpty(html))
					{
						this.placeholder.remove();
					}

					this.selection.restore();

					this.$textarea.off('keydown.redactor-textarea-indenting');

					this.button.setActiveInVisual();
					this.button.setInactive('html');
					this.observe.load();
					this.opts.visual = true;
					this.core.setCallback('visual', html);
				},
				textareaIndenting: function(e)
				{
					if (e.keyCode !== 9) return true;

					var $el = this.$textarea;
					var start = $el.get(0).selectionStart;
					$el.val($el.val().substring(0, start) + "\t" + $el.val().substring($el.get(0).selectionEnd));
					$el.get(0).selectionStart = $el.get(0).selectionEnd = start + 1;

					return false;
				},
				enlargeOffset: function(html, offset)
				{
					var htmlLength = html.length;
					var c = 0;

					if (html[offset] == '>')
					{
						c++;
					}
					else
					{
						for(var i = offset; i <= htmlLength; i++)
						{
							c++;

							if (html[i] == '>')
							{
								break;
							}
							else if (html[i] == '<' || i == htmlLength)
							{
								c = 0;
								break;
							}
						}
					}

					return offset + c;
				}
			};
		},
		core: function()
		{
			return {
				getObject: function()
				{
					return $.extend({}, this);
				},
				getEditor: function()
				{
					return this.$editor;
				},
				getBox: function()
				{
					return this.$box;
				},
				getElement: function()
				{
					return this.$element;
				},
				getTextarea: function()
				{
					return this.$textarea;
				},
				getToolbar: function()
				{
					return (this.$toolbar) ? this.$toolbar : false;
				},
				addEvent: function(name)
				{
					this.core.event = name;
				},
				getEvent: function()
				{
					return this.core.event;
				},
				setCallback: function(type, e, data)
				{
					var eventName = type + 'Callback';
					var eventNamespace = 'redactor';
					var callback = this.opts[eventName];

					if (this.$textarea)
					{
						var returnValue = false;
						var events = $._data(this.$textarea[0], 'events');

						if (typeof events != 'undefined' && typeof events[eventName] != 'undefined')
						{
							$.each(events[eventName], $.proxy(function(key, value)
							{
								if (value['namespace'] == eventNamespace)
								{
									var data = (typeof data == 'undefined') ? [e] : [e, data];

									returnValue = (typeof data == 'undefined') ? value.handler.call(this, e) : value.handler.call(this, e, data);
								}
							}, this));
						}

						if (returnValue) return returnValue;
					}

					if ($.isFunction(callback))
					{
						return (typeof data == 'undefined') ? callback.call(this, e) : callback.call(this, e, data);
					}
					else
					{
						return (typeof data == 'undefined') ? e : data;
					}
				},
				destroy: function()
				{
					this.opts.destroyed = true;

					this.core.setCallback('destroy');

					// off events and remove data
					this.$element.off('.redactor').removeData('redactor');
					this.$editor.off('.redactor');

					$(document).off('mousedown.redactor-blur.' + this.uuid);
					$(document).off('mousedown.redactor.' + this.uuid);
					$(document).off('click.redactor-image-delete.' + this.uuid);
					$(document).off('click.redactor-image-resize-hide.' + this.uuid);
					$(document).off('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid);
					$("body").off('scroll.redactor.' + this.uuid);
					$(this.opts.toolbarFixedTarget).off('scroll.redactor.' + this.uuid);

					// common
					this.$editor.removeClass('redactor-editor redactor-linebreaks redactor-placeholder');
					this.$editor.removeAttr('contenteditable');

					var html = this.code.get();

					if (this.opts.toolbar)
					{
						// dropdowns off
						this.$toolbar.find('a').each(function()
						{
							var $el = $(this);
							if ($el.data('dropdown'))
							{
								$el.data('dropdown').remove();
								$el.data('dropdown', {});
							}
						});
					}

					if (this.build.isTextarea())
					{
						this.$box.after(this.$element);
						this.$box.remove();
						this.$element.val(html).show();
					}
					else
					{
						this.$box.after(this.$editor);
						this.$box.remove();
						this.$element.html(html).show();
					}

					// paste box
					if (this.$pasteBox) this.$pasteBox.remove();

					// modal
					if (this.$modalBox) this.$modalBox.remove();
					if (this.$modalOverlay) this.$modalOverlay.remove();

					// buttons tooltip
					$('.redactor-toolbar-tooltip-' + this.uuid).remove();

					// autosave
					clearInterval(this.autosaveInterval);
				}
			};
		},
		dropdown: function()
		{
			return {
				build: function(name, $dropdown, dropdownObject)
				{
					if (name == 'formatting' && this.opts.formattingAdd)
					{
						$.each(this.opts.formattingAdd, $.proxy(function(i,s)
						{
							var name = s.tag,
								func;

							if (typeof s['class'] != 'undefined')
							{
								name = name + '-' + s['class'];
							}

							s.type = (this.utils.isBlockTag(s.tag)) ? 'block' : 'inline';

							if (typeof s.func !== "undefined")
							{
								func = s.func;
							}
							else
							{
								func = (s.type == 'inline') ? 'inline.formatting' : 'block.formatting';
							}

							if (this.opts.linebreaks && s.type == 'block' && s.tag == 'p') return;

							this.formatting[name] = {
								tag: s.tag,
								style: s.style,
								'class': s['class'],
								attr: s.attr,
								data: s.data,
								clear: s.clear
							};

							dropdownObject[name] = {
								func: func,
								title: s.title
							};

						}, this));
					}

					$.each(dropdownObject, $.proxy(function(btnName, btnObject)
					{
						var $item = $('<a href="#" class="redactor-dropdown-' + btnName + '" role="button">' + btnObject.title + '</a>');
						if (name == 'formatting') $item.addClass('redactor-formatting-' + btnName);

						$item.on('click', $.proxy(function(e)
						{
							e.preventDefault();

							var type = 'func';
							var callback = btnObject.func;
							if (btnObject.command)
							{
								type = 'command';
								callback = btnObject.command;
							}
							else if (btnObject.dropdown)
							{
								type = 'dropdown';
								callback = btnObject.dropdown;
							}

							if ($(e.target).hasClass('redactor-dropdown-link-inactive')) return;

							this.button.onClick(e, btnName, type, callback);
							this.dropdown.hideAll();

						}, this));

						this.observe.addDropdown($item, btnName, btnObject);

						$dropdown.append($item);

					}, this));
				},
				show: function(e, key)
				{
					if (!this.opts.visual)
					{
						e.preventDefault();
						return false;
					}

					var $button = this.button.get(key);

					// Always re-append it to the end of <body> so it always has the highest sub-z-index.
					var $dropdown = $button.data('dropdown').appendTo(document.body);

					if (this.opts.highContrast)
					{
						$dropdown.addClass("redactor-dropdown-contrast");
					}

					if ($button.hasClass('dropact'))
					{
						this.dropdown.hideAll();
					}
					else
					{
						this.dropdown.hideAll();
						this.observe.dropdowns();

						this.core.setCallback('dropdownShow', { dropdown: $dropdown, key: key, button: $button });

						this.button.setActive(key);

						$button.addClass('dropact');

						var keyPosition = $button.offset();

						// fix right placement
						var dropdownWidth = $dropdown.width();
						if ((keyPosition.left + dropdownWidth) > $(document).width())
						{
							keyPosition.left = Math.max(0, keyPosition.left - dropdownWidth);
						}

						var left = keyPosition.left + 'px';
						if (this.$toolbar.hasClass('toolbar-fixed-box'))
						{
							var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset;
							var position = 'fixed';
							if (this.opts.toolbarFixedTarget !== document)
							{
								top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top) + this.opts.toolbarFixedTopOffset;
								position = 'absolute';
							}

							$dropdown.css({ position: position, left: left, top: top + 'px' }).show();
						}
						else
						{
							var top = ($button.innerHeight() + keyPosition.top) + 'px';

							$dropdown.css({ position: 'absolute', left: left, top: top }).show();
						}

						this.core.setCallback('dropdownShown', { dropdown: $dropdown, key: key, button: $button });

						this.$dropdown = $dropdown;
					}


					$(document).one('click.redactor-dropdown', $.proxy(this.dropdown.hide, this));
					this.$editor.one('click.redactor-dropdown', $.proxy(this.dropdown.hide, this));
					$(document).one('keyup.redactor-dropdown', $.proxy(this.dropdown.closeHandler, this));

					// disable scroll whan dropdown scroll
					$dropdown.on('mouseover.redactor-dropdown', $.proxy(this.utils.disableBodyScroll, this)).on('mouseout.redactor-dropdown', $.proxy(this.utils.enableBodyScroll, this));


					e.stopPropagation();
				},
				closeHandler: function(e)
				{
					if (e.which != this.keyCode.ESC) return;

					this.dropdown.hideAll();
					this.$editor.focus();
				},
				hideAll: function()
				{
					this.$toolbar.find('a.dropact').removeClass('redactor-act').removeClass('dropact');

					this.utils.enableBodyScroll();

					$('.redactor-dropdown-' + this.uuid).hide();
					$('.redactor-dropdown-link-selected').removeClass('redactor-dropdown-link-selected');


					if (this.$dropdown)
					{
						this.$dropdown.off('.redactor-dropdown');
						this.core.setCallback('dropdownHide', this.$dropdown);

						this.$dropdown = false;
					}
				},
				hide: function (e)
				{
					var $dropdown = $(e.target);

					if (!$dropdown.hasClass('dropact') && !$dropdown.hasClass('redactor-dropdown-link-inactive'))
					{
						$dropdown.removeClass('dropact');
						$dropdown.off('mouseover mouseout');

						this.dropdown.hideAll();
					}
				}
			};
		},
		file: function()
		{
			return {
				show: function()
				{
					this.modal.load('file', this.lang.get('file'), 700);
					this.upload.init('#redactor-modal-file-upload', this.opts.fileUpload, this.file.insert);

					this.selection.save();

					this.selection.get();
					var text = this.sel.toString();

					$('#redactor-filename').val(text);

					this.modal.show();
				},
				insert: function(json, direct, e)
				{
					// error callback
					if (typeof json.error != 'undefined')
					{
						this.modal.close();
						this.selection.restore();
						this.core.setCallback('fileUploadError', json);
						return;
					}

					var link;
					if (typeof json == 'string')
					{
						link = json;
					}
					else
					{
						var text = $('#redactor-filename').val();
						if (typeof text == 'undefined' || text === '') text = json.filename;

						link = '<a href="' + json.filelink + '" id="filelink-marker">' + text + '</a>';
					}

					if (direct)
					{
						this.selection.removeMarkers();
						var marker = this.selection.getMarker();
						this.insert.nodeToCaretPositionFromPoint(e, marker);
					}
					else
					{
						this.modal.close();
					}

					this.selection.restore();
					this.buffer.set();

					this.insert.htmlWithoutClean(link);

					if (typeof json == 'string') return;

					var linkmarker = $(this.$editor.find('a#filelink-marker'));
					if (linkmarker.length !== 0)
					{
						linkmarker.removeAttr('id').removeAttr('style');
					}
					else linkmarker = false;

					this.core.setCallback('fileUpload', linkmarker, json);

				}
			};
		},
		focus: function()
		{
			return {
				setStart: function()
				{
					this.$editor.focus();

					var first = this.$editor.children().first();

					if (first.length === 0) return;
					if (first[0].length === 0 || first[0].tagName == 'BR' || first[0].nodeType == 3)
					{
						return;
					}

					if (first[0].tagName == 'UL' || first[0].tagName == 'OL')
					{
						var child = first.find('li').first();
						if (!this.utils.isBlock(child) && child.text() === '')
						{
							// empty inline tag in li
							this.caret.setStart(child);
							return;
						}
					}

					if (this.opts.linebreaks && !this.utils.isBlockTag(first[0].tagName))
					{
						this.selection.get();
						this.range.setStart(this.$editor[0], 0);
						this.range.setEnd(this.$editor[0], 0);
						this.selection.addRange();

						return;
					}

					// if node is tag
					this.caret.setStart(first);
				},
				setEnd: function()
				{
					var last = this.$editor.children().last();
					this.$editor.focus();

					if (last.size() === 0) return;
					if (this.utils.isEmpty(this.$editor.html()))
					{

						this.selection.get();
						this.range.collapse(true);
						this.range.setStartAfter(last[0]);
						this.range.setEnd(last[0], 0);
						this.selection.addRange();
					}
					else
					{
						this.selection.get();
						this.range.selectNodeContents(last[0]);
						this.range.collapse(false);
						this.selection.addRange();

					}
				},
				isFocused: function()
				{
					var focusNode = document.getSelection().focusNode;
					if (focusNode === null) return false;

					if (this.opts.linebreaks && $(focusNode.parentNode).hasClass('redactor-linebreaks')) return true;
					else if (!this.utils.isRedactorParent(focusNode.parentNode)) return false;

					return this.$editor.is(':focus');
				}
			};
		},
		image: function()
		{
			return {
				show: function()
				{
					this.modal.load('image', this.lang.get('image'), 700);
					this.upload.init('#redactor-modal-image-droparea', this.opts.imageUpload, this.image.insert);

					this.selection.save();
					this.modal.show();

				},
				showEdit: function($image)
				{
					var $link = $image.closest('a', this.$editor[0]);

					this.modal.load('imageEdit', this.lang.get('edit'), 705);

					this.modal.createCancelButton();
					this.image.buttonDelete = this.modal.createDeleteButton(this.lang.get('_delete'));
					this.image.buttonSave = this.modal.createActionButton(this.lang.get('save'));

					this.image.buttonDelete.on('click', $.proxy(function()
					{
						this.image.remove($image);

					}, this));

					this.image.buttonSave.on('click', $.proxy(function()
					{
						this.image.update($image);

					}, this));

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					$('#redactor-image-title').val($image.attr('alt'));

					if (!this.opts.imageLink) $('.redactor-image-link-option').hide();
					else
					{
						var $redactorImageLink = $('#redactor-image-link');

						$redactorImageLink.attr('href', $image.attr('src'));
						if ($link.length !== 0)
						{
							$redactorImageLink.val($link.attr('href'));
							if ($link.attr('target') == '_blank') $('#redactor-image-link-blank').prop('checked', true);
						}
					}

					if (!this.opts.imagePosition) $('.redactor-image-position-option').hide();
					else
					{
						var floatValue = ($image.css('display') == 'block' && $image.css('float') == 'none') ? 'center' : $image.css('float');
						$('#redactor-image-align').val(floatValue);
					}

					this.modal.show();
					$('#redactor-image-title').focus();

				},
				setFloating: function($image)
				{
					var floating = $('#redactor-image-align').val();

					var imageFloat = '';
					var imageDisplay = '';
					var imageMargin = '';

					switch (floating)
					{
						case 'left':
							imageFloat = 'left';
							imageMargin = '0 ' + this.opts.imageFloatMargin + ' ' + this.opts.imageFloatMargin + ' 0';
						break;
						case 'right':
							imageFloat = 'right';
							imageMargin = '0 0 ' + this.opts.imageFloatMargin + ' ' + this.opts.imageFloatMargin;
						break;
						case 'center':
							imageDisplay = 'block';
							imageMargin = 'auto';
						break;
					}

					$image.css({ 'float': imageFloat, display: imageDisplay, margin: imageMargin });
					$image.attr('rel', $image.attr('style'));
				},
				update: function($image)
				{
					this.image.hideResize();
					this.buffer.set();

					var $link = $image.closest('a', this.$editor[0]);

					var title = $('#redactor-image-title').val().replace(/(<([^>]+)>)/ig,"");
					$image.attr('alt', title);

					this.image.setFloating($image);

					// as link
					var link = $.trim($('#redactor-image-link').val());
					var link = link.replace(/(<([^>]+)>)/ig,"");
					if (link !== '')
					{
						// test url (add protocol)
						var pattern = '((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}';
						var re = new RegExp('^(http|ftp|https)://' + pattern, 'i');
						var re2 = new RegExp('^' + pattern, 'i');

						if (link.search(re) == -1 && link.search(re2) === 0 && this.opts.linkProtocol)
						{
							link = this.opts.linkProtocol + '://' + link;
						}

						var target = ($('#redactor-image-link-blank').prop('checked')) ? true : false;

						if ($link.length === 0)
						{
							var a = $('<a href="' + link + '">' + this.utils.getOuterHtml($image) + '</a>');
							if (target) a.attr('target', '_blank');

							$image.replaceWith(a);
						}
						else
						{
							$link.attr('href', link);
							if (target)
							{
								$link.attr('target', '_blank');
							}
							else
							{
								$link.removeAttr('target');
							}
						}
					}
					else if ($link.length !== 0)
					{
						$link.replaceWith(this.utils.getOuterHtml($image));

					}

					this.modal.close();
					this.observe.images();
					this.code.sync();


				},
				setEditable: function($image)
				{
					if (this.opts.imageEditable)
					{
						$image.on('dragstart', $.proxy(this.image.onDrag, this));
					}

					var handler = $.proxy(function(e)
					{

						this.observe.image = $image;

						this.image.resizer = this.image.loadEditableControls($image);

						$(document).on('mousedown.redactor-image-resize-hide.' + this.uuid, $.proxy(this.image.hideResize, this));

						// resize
						if (!this.opts.imageResizable) return;

						this.image.resizer.on('mousedown.redactor touchstart.redactor', $.proxy(function(e)
						{
							this.image.setResizable(e, $image);
						}, this));

					}, this);


					$image.off('mousedown.redactor').on('mousedown.redactor', $.proxy(this.image.hideResize, this));
					$image.off('click.redactor touchstart.redactor').on('click.redactor touchstart.redactor', handler);
				},
				setResizable: function(e, $image)
				{
					e.preventDefault();

				    this.image.resizeHandle = {
				        x : e.pageX,
				        y : e.pageY,
				        el : $image,
				        ratio: $image.width() / $image.height(),
				        h: $image.height()
				    };

				    e = e.originalEvent || e;

				    if (e.targetTouches)
				    {
				         this.image.resizeHandle.x = e.targetTouches[0].pageX;
				         this.image.resizeHandle.y = e.targetTouches[0].pageY;
				    }

					this.image.startResize();


				},
				startResize: function()
				{
					$(document).on('mousemove.redactor-image-resize touchmove.redactor-image-resize', $.proxy(this.image.moveResize, this));
					$(document).on('mouseup.redactor-image-resize touchend.redactor-image-resize', $.proxy(this.image.stopResize, this));
				},
				moveResize: function(e)
				{
					e.preventDefault();

					e = e.originalEvent || e;

					var height = this.image.resizeHandle.h;

		            if (e.targetTouches) height += (e.targetTouches[0].pageY -  this.image.resizeHandle.y);
		            else height += (e.pageY -  this.image.resizeHandle.y);

					var width = Math.round(height * this.image.resizeHandle.ratio);

					if (height < 50 || width < 100) return;

					var height = Math.round(this.image.resizeHandle.el.width() / this.image.resizeHandle.ratio);

					this.image.resizeHandle.el.attr({width: width, height: height});
		            this.image.resizeHandle.el.width(width);
		            this.image.resizeHandle.el.height(height);

		            this.code.sync();
				},
				stopResize: function()
				{
					this.handle = false;
					$(document).off('.redactor-image-resize');

					this.image.hideResize();
				},
				onDrag: function(e)
				{
					if (this.$editor.find('#redactor-image-box').length !== 0)
					{
						e.preventDefault();
						return false;
					}

					this.$editor.on('drop.redactor-image-inside-drop', $.proxy(function()
					{
						setTimeout($.proxy(this.image.onDrop, this), 1);

					}, this));
				},
				onDrop: function()
				{
					this.image.fixImageSourceAfterDrop();
					this.observe.images();
					this.$editor.off('drop.redactor-image-inside-drop');
					this.clean.clearUnverified();
					this.code.sync();
				},
				fixImageSourceAfterDrop: function()
				{
					this.$editor.find('img[data-save-url]').each(function()
					{
						var $el = $(this);
						$el.attr('src', $el.attr('data-save-url'));
						$el.removeAttr('data-save-url');
					});
				},
				hideResize: function(e)
				{
					if (e && $(e.target).closest('#redactor-image-box', this.$editor[0]).length !== 0) return;
					if (e && e.target.tagName == 'IMG')
					{
						var $image = $(e.target);
						$image.attr('data-save-url', $image.attr('src'));
					}

					var imageBox = this.$editor.find('#redactor-image-box');
					if (imageBox.length === 0) return;

					$('#redactor-image-editter').remove();
					$('#redactor-image-resizer').remove();

					imageBox.find('img').css({
						marginTop: imageBox[0].style.marginTop,
						marginBottom: imageBox[0].style.marginBottom,
						marginLeft: imageBox[0].style.marginLeft,
						marginRight: imageBox[0].style.marginRight
					});

					imageBox.css('margin', '');
					imageBox.find('img').css('opacity', '');
					imageBox.replaceWith(function()
					{
						return $(this).contents();
					});

					$(document).off('mousedown.redactor-image-resize-hide.' + this.uuid);


					if (typeof this.image.resizeHandle !== 'undefined')
					{
						this.image.resizeHandle.el.attr('rel', this.image.resizeHandle.el.attr('style'));
					}

					this.code.sync();

				},
				loadResizableControls: function($image, imageBox)
				{
					if (this.opts.imageResizable && !this.utils.isMobile())
					{
						var imageResizer = $('<span id="redactor-image-resizer" data-redactor="verified"></span>');

						if (!this.utils.isDesktop())
						{
							imageResizer.css({ width: '15px', height: '15px' });
						}

						imageResizer.attr('contenteditable', false);
						imageBox.append(imageResizer);
						imageBox.append($image);

						return imageResizer;
					}
					else
					{
						imageBox.append($image);
						return false;
					}
				},
				loadEditableControls: function($image)
				{
					var imageBox = $('<span id="redactor-image-box" data-redactor="verified">');
					imageBox.css('float', $image.css('float')).attr('contenteditable', false);

					if ($image[0].style.margin != 'auto')
					{
						imageBox.css({
							marginTop: $image[0].style.marginTop,
							marginBottom: $image[0].style.marginBottom,
							marginLeft: $image[0].style.marginLeft,
							marginRight: $image[0].style.marginRight
						});

						$image.css('margin', '');
					}
					else
					{
						imageBox.css({ 'display': 'block', 'margin': 'auto' });
					}

					$image.css('opacity', '.5').after(imageBox);


					if (this.opts.imageEditable)
					{
						// editter
						this.image.editter = $('<span id="redactor-image-editter" data-redactor="verified">' + this.lang.get('edit') + '</span>');
						this.image.editter.attr('contenteditable', false);
						this.image.editter.on('click', $.proxy(function()
						{
							this.image.showEdit($image);
						}, this));

						imageBox.append(this.image.editter);

						// position correction
						var editerWidth = this.image.editter.innerWidth();
						this.image.editter.css('margin-left', '-' + editerWidth/2 + 'px');
					}

					return this.image.loadResizableControls($image, imageBox);

				},
				remove: function(image)
				{
					var $image = $(image);
					var $link = $image.closest('a', this.$editor[0]);
					var $figure = $image.closest('figure', this.$editor[0]);
					var $parent = $image.parent();
					if ($('#redactor-image-box').length !== 0)
					{
						$parent = $('#redactor-image-box').parent();
					}

					var $next;
					if ($figure.length !== 0)
					{
						$next = $figure.next();
						$figure.remove();
					}
					else if ($link.length !== 0)
					{
						$parent = $link.parent();
						$link.remove();
					}
					else
					{
						$image.remove();
					}

					$('#redactor-image-box').remove();

					if ($figure.length !== 0)
					{
						this.caret.setStart($next);
					}
					else
					{
						this.caret.setStart($parent);
					}

					// delete callback
					this.core.setCallback('imageDelete', $image[0].src, $image);

					this.modal.close();
					this.code.sync();
				},
				insert: function(json, direct, e)
				{
					// error callback
					if (typeof json.error != 'undefined')
					{
						this.modal.close();
						this.selection.restore();
						this.core.setCallback('imageUploadError', json);
						return;
					}

					var $img;
					if (typeof json == 'string')
					{
						$img = $(json).attr('data-redactor-inserted-image', 'true');
					}
					else
					{
						$img = $('<img>');
						$img.attr('src', json.filelink).attr('data-redactor-inserted-image', 'true');
					}


					var node = $img;
					var isP = this.utils.isCurrentOrParent('P');
					if (isP)
					{
						// will replace
						node = $('<blockquote />').append($img);
					}

					if (direct)
					{
						this.selection.removeMarkers();
						var marker = this.selection.getMarker();
						this.insert.nodeToCaretPositionFromPoint(e, marker);
					}
					else
					{
						this.modal.close();
					}

					this.selection.restore();
					this.buffer.set();

					this.insert.html(this.utils.getOuterHtml(node), false);

					var $image = this.$editor.find('img[data-redactor-inserted-image=true]').removeAttr('data-redactor-inserted-image');

					if (isP)
					{
						$image.parent().contents().unwrap().wrap('<p />');
					}
					else if (this.opts.linebreaks)
					{
						if (!this.utils.isEmpty(this.code.get()))
						{
							$image.before('<br>');
						}

						$image.after('<br>');
					}

					if (typeof json == 'string') return;

					this.core.setCallback('imageUpload', $image, json);

				}
			};
		},
		indent: function()
		{
			return {
				increase: function()
				{
					// focus
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					var block = this.selection.getBlock();

					if (block && block.tagName == 'LI')
					{
						this.indent.increaseLists();
					}
					else if (block === false && this.opts.linebreaks)
					{
						this.indent.increaseText();
					}
					else
					{
						this.indent.increaseBlocks();
					}

					this.selection.restore();
					this.code.sync();
				},
				increaseLists: function()
				{
					document.execCommand('indent');

					this.indent.fixEmptyIndent();
					this.clean.normalizeLists();
					this.clean.clearUnverified();
				},
				increaseBlocks: function()
				{
					$.each(this.selection.getBlocks(), $.proxy(function(i, elem)
					{
						if (elem.tagName === 'TD' || elem.tagName === 'TH') return;

						var $el = this.utils.getAlignmentElement(elem);

						var left = this.utils.normalize($el.css('margin-left')) + this.opts.indentValue;
						$el.css('margin-left', left + 'px');

					}, this));
				},
				increaseText: function()
				{
					var wrapper = this.selection.wrap('div');
					$(wrapper).attr('data-tagblock', 'redactor');
					$(wrapper).css('margin-left', this.opts.indentValue + 'px');
				},
				decrease: function()
				{
					this.buffer.set();
					this.selection.save();

					var block = this.selection.getBlock();
					if (block && block.tagName == 'LI')
					{
						this.indent.decreaseLists();
					}
					else
					{
						this.indent.decreaseBlocks();
					}

					this.selection.restore();
					this.code.sync();
				},
				decreaseLists: function()
				{
					document.execCommand('outdent');

					var current = this.selection.getCurrent();
					var $item = $(current).closest('li', this.$editor[0]);

					this.indent.fixEmptyIndent();

					if (!this.opts.linebreaks && $item.length === 0)
					{
						document.execCommand('formatblock', false, 'p');
						this.$editor.find('ul, ol, blockquote, p').each($.proxy(this.utils.removeEmpty, this));
					}

					this.clean.clearUnverified();
				},
				decreaseBlocks: function()
				{
					$.each(this.selection.getBlocks(), $.proxy(function(i, elem)
					{
						var $el = this.utils.getAlignmentElement(elem);
						var left = this.utils.normalize($el.css('margin-left')) - this.opts.indentValue;

						if (left <= 0)
						{
							if (this.opts.linebreaks && typeof($el.data('tagblock')) !== 'undefined')
							{
								$el.replaceWith($el.html() + '<br />');
							}
							else
							{
								$el.css('margin-left', '');
								this.utils.removeEmptyAttr($el, 'style');
							}
						}
						else
						{
							$el.css('margin-left', left + 'px');
						}

					}, this));
				},
				fixEmptyIndent: function()
				{
					var block = this.selection.getBlock();

					if (this.range.collapsed && block && block.tagName == 'LI' && this.utils.isEmpty($(block).text()))
					{
						var $block = $(block);
						$block.find('span').not('.redactor-selection-marker').contents().unwrap();
						$block.append('<br>');
					}
				}
			};
		},
		inline: function()
		{
			return {
				formatting: function(name)
				{
					var type, value;

					if (typeof this.formatting[name].style != 'undefined') type = 'style';
					else if (typeof this.formatting[name]['class'] != 'undefined') type = 'class';

					if (type) value = this.formatting[name][type];

					this.inline.format(this.formatting[name].tag, type, value);

				},
				format: function(tag, type, value)
				{
					var current = this.selection.getCurrent();
					if (current && current.tagName === 'TR') return;

					// Stop formatting pre and headers
					if (this.utils.isCurrentOrParent('PRE') || this.utils.isCurrentOrParentHeader()) return;

					var tags = ['b', 'bold', 'i', 'italic', 'underline', 'strikethrough', 'deleted', 'superscript', 'subscript'];
					var replaced = ['strong', 'strong', 'em', 'em', 'u', 'del', 'del', 'sup', 'sub'];

					for (var i = 0; i < tags.length; i++)
					{
						if (tag == tags[i]) tag = replaced[i];
					}


					if (this.opts.allowedTags)
					{
						if ($.inArray(tag, this.opts.allowedTags) == -1) return;
					}
					else
					{
						if ($.inArray(tag, this.opts.deniedTags) !== -1) return;
					}

					this.inline.type = type || false;
					this.inline.value = value || false;

					this.buffer.set();

					if (!this.utils.browser('msie') && !this.opts.linebreaks)
					{
						this.$editor.focus();
					}

					this.selection.get();

					if (this.range.collapsed)
					{
						this.inline.formatCollapsed(tag);
					}
					else
					{
						this.inline.formatMultiple(tag);
					}
				},
				formatCollapsed: function(tag)
				{
					var current = this.selection.getCurrent();
					var $parent = $(current).closest(tag + '[data-redactor-tag=' + tag + ']', this.$editor[0]);

					// inline there is
					if ($parent.length !== 0 && (this.inline.type != 'style' && $parent[0].tagName != 'SPAN'))
					{
						// remove empty
						if (this.utils.isEmpty($parent.text()))
						{
							this.caret.setAfter($parent[0]);

							$parent.remove();
							this.code.sync();
						}
						else if (this.utils.isEndOfElement($parent))
						{
							this.caret.setAfter($parent[0]);
						}

						return;
					}

					// create empty inline
					var node = $('<' + tag + '>').attr('data-verified', 'redactor').attr('data-redactor-tag', tag);
					node.html(this.opts.invisibleSpace);

					node = this.inline.setFormat(node);

					var node = this.insert.node(node);
					this.caret.setEnd(node);

					this.code.sync();
				},
				formatMultiple: function(tag)
				{
					this.inline.formatConvert(tag);

					this.selection.save();
					document.execCommand('strikethrough');

					this.$editor.find('strike').each($.proxy(function(i,s)
					{
						var $el = $(s);

						this.inline.formatRemoveSameChildren($el, tag);

						var $span;
						if (this.inline.type)
						{
							$span = $('<span>').attr('data-redactor-tag', tag).attr('data-verified', 'redactor');
							$span = this.inline.setFormat($span);
						}
						else
						{
							$span = $('<' + tag + '>').attr('data-redactor-tag', tag).attr('data-verified', 'redactor');
						}

						$el.replaceWith($span.html($el.contents()));
						var $parent = $span.parent();

						// remove U tag if selected link + node
						if ($span[0].tagName === 'A' && $parent && $parent[0].tagName === 'U')
						{
							$span.parent().replaceWith($span);
						}

						if (tag == 'span')
						{
							if ($parent && $parent[0].tagName === 'SPAN' && this.inline.type === 'style')
							{
								var arr = this.inline.value.split(';');

								for (var z = 0; z < arr.length; z++)
								{
									if (arr[z] === '') return;
									var style = arr[z].split(':');
									$parent.css(style[0], '');

									if (this.utils.removeEmptyAttr($parent, 'style'))
									{
										$parent.replaceWith($parent.contents());
									}

								}

							}
						}

					}, this));

					// clear text decoration
					if (tag != 'span')
					{
						this.$editor.find(this.opts.inlineTags.join(', ')).each($.proxy(function(i,s)
						{
							var $el = $(s);


							if (s.tagName === 'U' && s.attributes.length === 0)
							{
								$el.replaceWith($el.contents());
								return;
							}

							var property = $el.css('text-decoration');
							if (property === 'line-through')
							{
								$el.css('text-decoration', '');
								this.utils.removeEmptyAttr($el, 'style');
							}
						}, this));
					}

					if (tag != 'del')
					{
						var _this = this;
						this.$editor.find('inline').each(function(i,s)
						{
							_this.utils.replaceToTag(s, 'del');
						});
					}

					this.selection.restore();
					this.code.sync();

				},
				formatRemoveSameChildren: function($el, tag)
				{
					var self = this;
					$el.children(tag).each(function()
					{
						var $child = $(this);

						if (!$child.hasClass('redactor-selection-marker'))
						{
							if (self.inline.type == 'style')
							{
								var arr = self.inline.value.split(';');

								for (var z = 0; z < arr.length; z++)
								{
									if (arr[z] === '') return;

									var style = arr[z].split(':');
									$child.css(style[0], '');

									if (self.utils.removeEmptyAttr($child , 'style'))
									{
										$child.replaceWith($child.contents());
									}

								}
							}
							else
							{
								$child.contents().unwrap();
							}
						}

					});
				},
				formatConvert: function(tag)
				{
					this.selection.save();

					var find = '';
					if (this.inline.type == 'class') find = '[data-redactor-class=' + this.inline.value + ']';
					else if (this.inline.type == 'style')
					{
						find = '[data-redactor-style="' + this.inline.value + '"]';
					}

					var self = this;
					if (tag != 'del')
					{
						this.$editor.find('del').each(function(i,s)
						{
							self.utils.replaceToTag(s, 'inline');
						});
					}

					if (tag != 'span')
					{
						this.$editor.find(tag).each(function()
						{
							var $el = $(this);
							$el.replaceWith($('<strike />').html($el.contents()));

						});
					}

					this.$editor.find('[data-redactor-tag="' + tag + '"]' + find).each(function()
					{
						if (find === '' && tag == 'span' && this.tagName.toLowerCase() == tag) return;

						var $el = $(this);
						$el.replaceWith($('<strike />').html($el.contents()));

					});

					this.selection.restore();
				},
				setFormat: function(node)
				{
					switch (this.inline.type)
					{
						case 'class':

							if (node.hasClass(this.inline.value))
							{
								node.removeClass(this.inline.value);
								node.removeAttr('data-redactor-class');
							}
							else
							{
								node.addClass(this.inline.value);
								node.attr('data-redactor-class', this.inline.value);
							}


						break;
						case 'style':

							node[0].style.cssText = this.inline.value;
							node.attr('data-redactor-style', this.inline.value);

						break;
					}

					return node;
				},
				removeStyle: function()
				{
					this.buffer.set();
					var current = this.selection.getCurrent();
					var nodes = this.selection.getInlines();

					this.selection.save();

					if (current && current.tagName === 'SPAN')
					{
						var $s = $(current);

						$s.removeAttr('style');
						if ($s[0].attributes.length === 0)
						{
							$s.replaceWith($s.contents());
						}
					}

					$.each(nodes, $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.removeAttr('style');
							if ($s[0].attributes.length === 0)
							{
								$s.replaceWith($s.contents());
							}
						}
					}, this));

					this.selection.restore();
					this.code.sync();

				},
				removeStyleRule: function(name)
				{
					this.buffer.set();
					var parent = this.selection.getParent();
					var nodes = this.selection.getInlines();

					this.selection.save();

					if (parent && parent.tagName === 'SPAN')
					{
						var $s = $(parent);

						$s.css(name, '');
						this.utils.removeEmptyAttr($s, 'style');
						if ($s[0].attributes.length === 0)
						{
							$s.replaceWith($s.contents());
						}
					}

					$.each(nodes, $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.css(name, '');
							this.utils.removeEmptyAttr($s, 'style');
							if ($s[0].attributes.length === 0)
							{
								$s.replaceWith($s.contents());
							}
						}
					}, this));

					this.selection.restore();
					this.code.sync();
				},
				removeFormat: function()
				{
					this.buffer.set();
					var current = this.selection.getCurrent();

					this.selection.save();

					document.execCommand('removeFormat');

					if (current && current.tagName === 'SPAN')
					{
						$(current).replaceWith($(current).contents());
					}


					$.each(this.selection.getNodes(), $.proxy(function(i,s)
					{
						var $s = $(s);
						if ($.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) != -1 && !$s.hasClass('redactor-selection-marker'))
						{
							$s.replaceWith($s.contents());
						}
					}, this));

					this.selection.restore();
					this.code.sync();

				},
				toggleClass: function(className)
				{
					this.inline.format('span', 'class', className);
				},
				toggleStyle: function(value)
				{
					this.inline.format('span', 'style', value);
				}
			};
		},
		insert: function()
		{
			return {
				set: function(html, clean)
				{
					this.placeholder.remove();

					html = this.clean.setVerified(html);

					if (typeof clean == 'undefined')
					{
						html = this.clean.onPaste(html, false);
					}

					this.$editor.html(html);
					this.selection.remove();
					this.focus.setEnd();
					this.clean.normalizeLists();
					this.code.sync();
					this.observe.load();

					if (typeof clean == 'undefined')
					{
						setTimeout($.proxy(this.clean.clearUnverified, this), 10);
					}
				},
				text: function(text)
				{
					this.placeholder.remove();

					text = text.toString();
					text = $.trim(text);
					text = this.clean.getPlainText(text, false);

					this.$editor.focus();

					if (this.utils.browser('msie'))
					{
						this.insert.htmlIe(text);
					}
					else
					{
						this.selection.get();

						this.range.deleteContents();
						var el = document.createElement("div");
						el.innerHTML = text;
						var frag = document.createDocumentFragment(), node, lastNode;
						while ((node = el.firstChild))
						{
							lastNode = frag.appendChild(node);
						}

						this.range.insertNode(frag);

						if (lastNode)
						{
							var range = this.range.cloneRange();
							range.setStartAfter(lastNode);
							range.collapse(true);
							this.sel.removeAllRanges();
							this.sel.addRange(range);
						}
					}

					this.code.sync();
					this.clean.clearUnverified();
				},
				htmlWithoutClean: function(html)
				{
					this.insert.html(html, false);
				},
				html: function(html, clean)
				{
					this.placeholder.remove();

					if (typeof clean == 'undefined') clean = true;

					if (!this.opts.linebreaks)
					{
						this.$editor.focus();
					}

					html = this.clean.setVerified(html);

					if (clean)
					{
						html = this.clean.onPaste(html);
					}

					if (this.utils.browser('msie'))
					{
						this.insert.htmlIe(html);
					}
					else
					{
						if (this.clean.singleLine) this.insert.execHtml(html);
						else document.execCommand('insertHTML', false, html);

						this.insert.htmlFixMozilla();

					}

					this.clean.normalizeLists();

					// remove empty paragraphs finaly
					if (!this.opts.linebreaks)
					{
						this.$editor.find('p').each($.proxy(this.utils.removeEmpty, this));
					}

					this.code.sync();
					this.observe.load();

					if (clean)
					{
						this.clean.clearUnverified();
					}

				},
				htmlFixMozilla: function()
				{
					// FF inserts empty p when content was selected dblclick
					if (!this.utils.browser('mozilla')) return;

					var $next = $(this.selection.getBlock()).next();
					if ($next.length > 0 && $next[0].tagName == 'P' && $next.html() === '')
					{
						$next.remove();
					}

				},
				htmlIe: function(html)
				{
					if (this.utils.isIe11())
					{
						var parent = this.utils.isCurrentOrParent('P');
						var $html = $('<div>').append(html);
						var blocksMatch = $html.contents().is('p, :header, dl, ul, ol, div, table, td, blockquote, pre, address, section, header, footer, aside, article');

						if (parent && blocksMatch) this.insert.ie11FixInserting(parent, html);
						else this.insert.ie11PasteFrag(html);

						return;
					}

					document.selection.createRange().pasteHTML(html);

				},
				execHtml: function(html)
				{
					html = this.clean.setVerified(html);

					this.selection.get();

					this.range.deleteContents();

					var el = document.createElement('div');
					el.innerHTML = html;

					var frag = document.createDocumentFragment(), node, lastNode;
					while ((node = el.firstChild))
					{
						lastNode = frag.appendChild(node);
					}

					this.range.insertNode(frag);

					this.range.collapse(true);
					this.caret.setAfter(lastNode);

				},
				node: function(node, deleteContents)
				{
					node = node[0] || node;

					var html = this.utils.getOuterHtml(node);
					html = this.clean.setVerified(html);

					if (html.match(/</g) !== null)
					{
						node = $(html)[0];
					}

					this.selection.get();

					if (deleteContents !== false)
					{
						this.range.deleteContents();
					}

					this.range.insertNode(node);
					this.range.collapse(false);
					this.selection.addRange();

					return node;
				},
				nodeToPoint: function(node, x, y)
				{
					node = node[0] || node;

					this.selection.get();

					var range;
					if (document.caretPositionFromPoint)
					{
					    var pos = document.caretPositionFromPoint(x, y);

					    this.range.setStart(pos.offsetNode, pos.offset);
					    this.range.collapse(true);
					    this.range.insertNode(node);
					}
					else if (document.caretRangeFromPoint)
					{
					    range = document.caretRangeFromPoint(x, y);
					    range.insertNode(node);
					}
					else if (typeof document.body.createTextRange != "undefined")
					{
				        range = document.body.createTextRange();
				        range.moveToPoint(x, y);
				        var endRange = range.duplicate();
				        endRange.moveToPoint(x, y);
				        range.setEndPoint("EndToEnd", endRange);
				        range.select();
					}
				},
				nodeToCaretPositionFromPoint: function(e, node)
				{
					node = node[0] || node;

					var range;
					var x = e.clientX, y = e.clientY;
					if (document.caretPositionFromPoint)
					{
					    var pos = document.caretPositionFromPoint(x, y);
					    var sel = document.getSelection();
					    range = sel.getRangeAt(0);
					    range.setStart(pos.offsetNode, pos.offset);
					    range.collapse(true);
					    range.insertNode(node);
					}
					else if (document.caretRangeFromPoint)
					{
					    range = document.caretRangeFromPoint(x, y);
					    range.insertNode(node);
					}
					else if (typeof document.body.createTextRange != "undefined")
					{
				        range = document.body.createTextRange();
				        range.moveToPoint(x, y);
				        var endRange = range.duplicate();
				        endRange.moveToPoint(x, y);
				        range.setEndPoint("EndToEnd", endRange);
				        range.select();
					}

				},
				ie11FixInserting: function(parent, html)
				{
					var node = document.createElement('span');
					node.className = 'redactor-ie-paste';
					this.insert.node(node);

					var parHtml = $(parent).html();

					parHtml = '<p>' + parHtml.replace(/<span class="redactor-ie-paste"><\/span>/gi, '</p>' + html + '<p>') + '</p>';
					parHtml = parHtml.replace(/<p><\/p>/gi, '');
					$(parent).replaceWith(parHtml);
				},
				ie11PasteFrag: function(html)
				{
					this.selection.get();
					this.range.deleteContents();

					var el = document.createElement("div");
					el.innerHTML = html;

					var frag = document.createDocumentFragment(), node, lastNode;
					while ((node = el.firstChild))
					{
						lastNode = frag.appendChild(node);
					}

					this.range.insertNode(frag);
					this.range.collapse(false);
					this.selection.addRange();
				}
			};
		},
		keydown: function()
		{
			return {
				init: function(e)
				{
					if (this.rtePaste) return;

					var key = e.which;
					var arrow = (key >= 37 && key <= 40);

					this.keydown.ctrl = e.ctrlKey || e.metaKey;
					this.keydown.current = this.selection.getCurrent();
					this.keydown.parent = this.selection.getParent();
					this.keydown.block = this.selection.getBlock();

			        // detect tags
					this.keydown.pre = this.utils.isTag(this.keydown.current, 'pre');
					this.keydown.blockquote = this.utils.isTag(this.keydown.current, 'blockquote');
					this.keydown.figcaption = this.utils.isTag(this.keydown.current, 'figcaption');

					// shortcuts setup
					this.shortcuts.init(e, key);

					if (this.utils.isDesktop())
					{
						this.keydown.checkEvents(arrow, key);
						this.keydown.setupBuffer(e, key);
					}

					this.keydown.addArrowsEvent(arrow);
					this.keydown.setupSelectAll(e, key);

					// callback
					var keydownStop = this.core.setCallback('keydown', e);
					if (keydownStop === false)
					{
						e.preventDefault();
						return false;
					}

					// ie and ff exit from table
					if (this.opts.enterKey && (this.utils.browser('msie') || this.utils.browser('mozilla')) && (key === this.keyCode.DOWN || key === this.keyCode.RIGHT))
					{
						var isEndOfTable = false;
						var $table = false;
						if (this.keydown.block && this.keydown.block.tagName === 'TD')
						{
							$table = $(this.keydown.block).closest('table', this.$editor[0]);
						}

						if ($table && $table.find('td').last()[0] === this.keydown.block)
						{
							isEndOfTable = true;
						}

						if (this.utils.isEndOfElement() && isEndOfTable)
						{
							var node = $(this.opts.emptyHtml);
							$table.after(node);
							this.caret.setStart(node);
						}
					}

					// down
					if (this.opts.enterKey && key === this.keyCode.DOWN)
					{
						this.keydown.onArrowDown();
					}

					// turn off enter key
					if (!this.opts.enterKey && key === this.keyCode.ENTER)
					{
						e.preventDefault();
						// remove selected
						if (!this.range.collapsed) this.range.deleteContents();
						return;
					}

					// on enter
					if (key == this.keyCode.ENTER && !e.shiftKey && !e.ctrlKey && !e.metaKey)
					{
						var stop = this.core.setCallback('enter', e);
						if (stop === false)
						{
							e.preventDefault();
							return false;
						}

						if (this.keydown.blockquote && this.keydown.exitFromBlockquote(e) === true)
						{
							return false;
						}

						var current, $next;
						if (this.keydown.pre)
						{
							return this.keydown.insertNewLine(e);
						}
						else if (this.keydown.blockquote || this.keydown.figcaption)
						{
							current = this.selection.getCurrent();
							$next = $(current).next();

							if ($next.length !== 0 && $next[0].tagName == 'BR')
							{
								return this.keydown.insertBreakLine(e);
							}
							else if (this.utils.isEndOfElement() && (current && current != 'SPAN'))
							{
								return this.keydown.insertDblBreakLine(e);
							}
							else
							{
								return this.keydown.insertBreakLine(e);
							}
						}
						else if (this.opts.linebreaks && !this.keydown.block)
						{
							current = this.selection.getCurrent();
							$next = $(this.keydown.current).next();

							if ($next.length !== 0 && $next[0].tagName == 'BR')
							{
								return this.keydown.insertBreakLine(e);
							}
							else if (current !== false && $(current).hasClass('redactor-invisible-space'))
							{
								this.caret.setAfter(current);
								$(current).contents().unwrap();

								return this.keydown.insertDblBreakLine(e);
							}
							else
							{
								if (this.utils.isEndOfEditor())
								{
									return this.keydown.insertDblBreakLine(e);
								}
								else if ($next.length === 0 && current === false && typeof $next.context != 'undefined')
								{
									return this.keydown.insertBreakLine(e);
								}

								return this.keydown.insertBreakLine(e);
							}

						}
						else if (this.opts.linebreaks && this.keydown.block)
						{
							setTimeout($.proxy(this.keydown.replaceDivToBreakLine, this), 1);
						}
						// paragraphs
						else if (!this.opts.linebreaks && this.keydown.block)
						{
							setTimeout($.proxy(this.keydown.replaceDivToParagraph, this), 1);

							if (this.keydown.block.tagName === 'LI')
							{
								current = this.selection.getCurrent();
								var $parent = $(current).closest('li', this.$editor[0]);
								var $list = $parent.closest('ul,ol', this.$editor[0]);

								if ($parent.length !== 0 && this.utils.isEmpty($parent.html()) && $list.next().length === 0 && this.utils.isEmpty($list.find("li").last().html()))
								{
									$list.find("li").last().remove();

									var node = $(this.opts.emptyHtml);
									$list.after(node);
									this.caret.setStart(node);

									return false;
								}
							}
						}
						else if (!this.opts.linebreaks && !this.keydown.block)
						{
							return this.keydown.insertParagraph(e);
						}
					}

					// Shift+Enter or Ctrl+Enter
					if (key === this.keyCode.ENTER && (e.ctrlKey || e.shiftKey))
					{
						return this.keydown.onShiftEnter(e);
					}


					// tab or cmd + [
					if (key === this.keyCode.TAB || e.metaKey && key === 221 || e.metaKey && key === 219)
					{
						return this.keydown.onTab(e, key);
					}

					// image delete and backspace
					if (key === this.keyCode.BACKSPACE || key === this.keyCode.DELETE)
					{
						var nodes = this.selection.getNodes();

						if (nodes)
						{
							var len = nodes.length;
							var last;
							for (var i = 0; i < len; i++)
							{
								var children = $(nodes[i]).children('img');
								if (children.length !== 0)
								{
									var self = this;
									$.each(children, function(z,s)
									{
										var $s = $(s);
										if ($s.css('float') != 'none') return;

										// image delete callback
										self.core.setCallback('imageDelete', s.src, $s);
										last = s;
									});
								}
								else if (nodes[i].tagName == 'IMG')
								{
									if (last != nodes[i])
									{
										// image delete callback
										this.core.setCallback('imageDelete', nodes[i].src, $(nodes[i]));
										last = nodes[i];
									}
								}
							}
						}
					}

					// backspace
					if (key === this.keyCode.BACKSPACE)
					{
						// backspace as outdent
						var block = this.selection.getBlock();
						var indented = ($(block).css('margin-left') !== '0px');
						if (block && indented && this.range.collapsed && this.utils.isStartOfElement())
						{
							this.indent.decrease();
							e.preventDefault();
							return;
						}

						// remove hr in FF
						if (this.utils.browser('mozilla'))
						{
							var prev = this.selection.getPrev();
							var prev2 = $(prev).prev()[0];
							if (prev && prev.tagName === 'HR') $(prev).remove();
							if (prev2 && prev2.tagName === 'HR') $(prev2).remove();
						}

						this.keydown.removeInvisibleSpace();
						this.keydown.removeEmptyListInTable(e);
					}

					this.code.sync();
				},
				checkEvents: function(arrow, key)
				{
					if (!arrow && (this.core.getEvent() == 'click' || this.core.getEvent() == 'arrow'))
					{
						this.core.addEvent(false);

						if (this.keydown.checkKeyEvents(key))
						{
							this.buffer.set();
						}
					}
				},
				checkKeyEvents: function(key)
				{
					var k = this.keyCode;
					var keys = [k.BACKSPACE, k.DELETE, k.ENTER, k.ESC, k.TAB, k.CTRL, k.META, k.ALT, k.SHIFT];

					return ($.inArray(key, keys) == -1) ? true : false;

				},
				addArrowsEvent: function(arrow)
				{
					if (!arrow) return;

					if ((this.core.getEvent() == 'click' || this.core.getEvent() == 'arrow'))
					{
						this.core.addEvent(false);
						return;
					}

				    this.core.addEvent('arrow');
				},
				setupBuffer: function(e, key)
				{
					if (this.keydown.ctrl && key === 90 && !e.shiftKey && !e.altKey && this.opts.buffer.length) // z key
					{
						e.preventDefault();
						this.buffer.undo();
						return;
					}
					// undo
					else if (this.keydown.ctrl && key === 90 && e.shiftKey && !e.altKey && this.opts.rebuffer.length !== 0)
					{
						e.preventDefault();
						this.buffer.redo();
						return;
					}
					else if (!this.keydown.ctrl)
					{
						if (key == this.keyCode.BACKSPACE || key == this.keyCode.DELETE || (key == this.keyCode.ENTER && !e.ctrlKey && !e.shiftKey))
						{
							this.buffer.set();
						}
					}
				},
				setupSelectAll: function(e, key)
				{
					if (this.keydown.ctrl && key === 65)
					{
						this.utils.enableSelectAll();
					}
					else if (key != this.keyCode.LEFT_WIN && !this.keydown.ctrl)
					{
						this.utils.disableSelectAll();
					}
				},
				onArrowDown: function()
				{
					var tags = [this.keydown.blockquote, this.keydown.pre, this.keydown.figcaption];

					for (var i = 0; i < tags.length; i++)
					{
						if (tags[i])
						{
							this.keydown.insertAfterLastElement(tags[i]);
							return false;
						}
					}
				},
				onShiftEnter: function(e)
				{
					this.buffer.set();

					if (this.utils.isEndOfElement())
					{
						return this.keydown.insertDblBreakLine(e);
					}

					return this.keydown.insertBreakLine(e);
				},
				onTab: function(e, key)
				{
					if (!this.opts.tabKey) return true;
					if (this.utils.isEmpty(this.code.get()) && this.opts.tabAsSpaces === false) return true;

					e.preventDefault();

					var node;
					if (this.keydown.pre && !e.shiftKey)
					{
						node = (this.opts.preSpaces) ? document.createTextNode(Array(this.opts.preSpaces + 1).join('\u00a0')) : document.createTextNode('\t');
						this.insert.node(node);
						this.code.sync();
					}
					else if (this.opts.tabAsSpaces !== false)
					{
						node = document.createTextNode(Array(this.opts.tabAsSpaces + 1).join('\u00a0'));
						this.insert.node(node);
						this.code.sync();
					}
					else
					{
						if (e.metaKey && key === 219) this.indent.decrease();
						else if (e.metaKey && key === 221) this.indent.increase();
						else if (!e.shiftKey) this.indent.increase();
						else this.indent.decrease();
					}

					return false;
				},
				replaceDivToBreakLine: function()
				{
					var blockElem = this.selection.getBlock();
					var blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, '');
					if ((blockElem.tagName === 'DIV' || blockElem.tagName === 'P') && blockHtml === '' && !$(blockElem).hasClass('redactor-editor'))
					{
						var br = document.createElement('br');

						$(blockElem).replaceWith(br);
						this.caret.setBefore(br);

						this.code.sync();

						return false;
					}
				},
				replaceDivToParagraph: function()
				{
					var blockElem = this.selection.getBlock();
					var blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, '');
					if (blockElem.tagName === 'DIV' && this.utils.isEmpty(blockHtml) && !$(blockElem).hasClass('redactor-editor'))
					{
						var p = document.createElement('p');
						p.innerHTML = this.opts.invisibleSpace;

						$(blockElem).replaceWith(p);
						this.caret.setStart(p);

						this.code.sync();

						return false;
					}
					else if (this.opts.cleanStyleOnEnter && blockElem.tagName == 'P')
					{
						$(blockElem).removeAttr('class').removeAttr('style');
					}
				},
				insertParagraph: function(e)
				{
					e.preventDefault();

					this.selection.get();

					var p = document.createElement('p');
					p.innerHTML = this.opts.invisibleSpace;

					this.range.deleteContents();
					this.range.insertNode(p);

					this.caret.setStart(p);

					this.code.sync();

					return false;
				},
				exitFromBlockquote: function(e)
				{
					if (!this.utils.isEndOfElement()) return;

					var tmp = $.trim($(this.keydown.block).html());
					if (tmp.search(/(<br\s?\/?>){2}$/i) != -1)
					{
						e.preventDefault();

						if (this.opts.linebreaks)
						{
							var br = document.createElement('br');
							$(this.keydown.blockquote).after(br);

							this.caret.setBefore(br);
							$(this.keydown.block).html(tmp.replace(/<br\s?\/?>$/i, ''));
						}
						else
						{
							var node = $(this.opts.emptyHtml);
							$(this.keydown.blockquote).after(node);
							this.caret.setStart(node);
						}

						return true;

					}

					return;

				},
				insertAfterLastElement: function(element)
				{
					if (!this.utils.isEndOfElement()) return;

					this.buffer.set();

					if (this.opts.linebreaks)
					{
						var contents = $('<div>').append($.trim(this.$editor.html())).contents();
						var last = contents.last()[0];
						if (last.tagName == 'SPAN' && last.innerHTML === '')
						{
							last = contents.prev()[0];
						}

						if (this.utils.getOuterHtml(last) != this.utils.getOuterHtml(element)) return;

						var br = document.createElement('br');
						$(element).after(br);
						this.caret.setAfter(br);

					}
					else
					{
						if (this.$editor.contents().last()[0] !== element) return;

						var node = $(this.opts.emptyHtml);
						$(element).after(node);
						this.caret.setStart(node);
					}
				},
				insertNewLine: function(e)
				{
					e.preventDefault();

					var node = document.createTextNode('\n');

					this.selection.get();

					this.range.deleteContents();
					this.range.insertNode(node);

					this.caret.setAfter(node);

					this.code.sync();

					return false;
				},
				insertBreakLine: function(e)
				{
					return this.keydown.insertBreakLineProcessing(e);
				},
				insertDblBreakLine: function(e)
				{
					return this.keydown.insertBreakLineProcessing(e, true);
				},
				insertBreakLineProcessing: function(e, dbl)
				{
					e.stopPropagation();

					this.selection.get();

					var br1 = document.createElement('br');

					if (this.utils.browser('msie'))
					{
						this.range.collapse(false);
						this.range.setEnd(this.range.endContainer, this.range.endOffset);
					}
					else
					{
						this.range.deleteContents();
					}

					this.range.insertNode(br1);

					// move br outside A tag
					var $parentA = $(br1).parent("a");

					if ($parentA.length > 0)
					{
						$parentA.find(br1).remove();
						$parentA.after(br1);
					}

					if (dbl === true)
					{
						var $next = $(br1).next();
						if ($next.length !== 0 && $next[0].tagName === 'BR' && this.utils.isEndOfEditor())
						{
							this.caret.setAfter(br1);
							this.code.sync();
							return false;
						}

						var br2 = document.createElement('br');

						this.range.insertNode(br2);
						this.caret.setAfter(br2);
					}
					else
					{
						// caret does not move after the br visual
						if (this.utils.browser('msie'))
						{
							var space = document.createElement('span');
							space.innerHTML = '&#x200b;';

							$(br1).after(space);
							this.caret.setAfter(space);
							$(space).remove();
						}
						else
						{
							var range = document.createRange();
							range.setStartAfter(br1);
							range.collapse(true);
							var selection = window.getSelection();
							selection.removeAllRanges();
							selection.addRange(range);

						}
					}

					this.code.sync();
					return false;
				},
				removeInvisibleSpace: function()
				{
					var $current = $(this.keydown.current);
					if ($current.text().search(/^\u200B$/g) === 0)
					{
						$current.remove();
					}
				},
				removeEmptyListInTable: function(e)
				{
					var $current = $(this.keydown.current);
					var $parent = $(this.keydown.parent);
					var td = $current.closest('td', this.$editor[0]);

					if (td.length !== 0 && $current.closest('li', this.$editor[0]) && $parent.children('li').length === 1)
					{
						if (!this.utils.isEmpty($current.text())) return;

						e.preventDefault();

						$current.remove();
						$parent.remove();

						this.caret.setStart(td);
					}
				}
			};
		},
		keyup: function()
		{
			return {
				init: function(e)
				{

					if (this.rtePaste) return;

					var key = e.which;

					this.keyup.current = this.selection.getCurrent();
					this.keyup.parent = this.selection.getParent();
					var $parent = this.utils.isRedactorParent($(this.keyup.parent).parent());

					// callback
					var keyupStop = this.core.setCallback('keyup', e);
					if (keyupStop === false)
					{
						e.preventDefault();
						return false;
					}

					// replace to p before / after the table or body
					if (!this.opts.linebreaks && this.keyup.current.nodeType === 3 && this.keyup.current.length <= 1 && (this.keyup.parent === false || this.keyup.parent.tagName == 'BODY'))
					{
						this.keyup.replaceToParagraph();
					}

					// replace div after lists
					if (!this.opts.linebreaks && this.utils.isRedactorParent(this.keyup.current) && this.keyup.current.tagName === 'DIV')
					{
						this.keyup.replaceToParagraph(false);
					}


					if (!this.opts.linebreaks && $(this.keyup.parent).hasClass('redactor-invisible-space') && ($parent === false || $parent[0].tagName == 'BODY'))
					{
						$(this.keyup.parent).contents().unwrap();
						this.keyup.replaceToParagraph();
					}

					// linkify
					if (this.linkify.isEnabled() && this.linkify.isKey(key)) this.linkify.format();

					if (key === this.keyCode.DELETE || key === this.keyCode.BACKSPACE)
					{
						if (this.utils.browser('mozilla'))
						{
							var td = $(this.keydown.current).closest('td', this.$editor[0]);
							if (td.size() !== 0 && td.text() !== '')
							{
								e.preventDefault();
								return false;
							}
						}

						// clear unverified
						this.clean.clearUnverified();

						if (this.observe.image)
						{
							e.preventDefault();

							this.image.hideResize();

							this.buffer.set();
							this.image.remove(this.observe.image);
							this.observe.image = false;

							return false;
						}

						// remove empty paragraphs
						this.$editor.find('p').each($.proxy(function(i, s)
						{
							this.utils.removeEmpty(i, $(s).html());
						}, this));

						// remove invisible space
						if (this.opts.linebreaks && this.keyup.current && this.keyup.current.tagName == 'DIV' && this.utils.isEmpty(this.keyup.current.innerHTML))
						{
							$(this.keyup.current).after(this.selection.getMarkerAsHtml());
							this.selection.restore();
							$(this.keyup.current).remove();
						}

						// if empty
						return this.keyup.formatEmpty(e);
					}
				},
				replaceToParagraph: function(clone)
				{
					var $current = $(this.keyup.current);

					var node;
					if (clone === false)
					{
						node = $('<p>').append($current.html());
					}
					else
					{
						node = $('<p>').append($current.clone());
					}

					$current.replaceWith(node);
					var next = $(node).next();
					if (typeof(next[0]) !== 'undefined' && next[0].tagName == 'BR')
					{
						next.remove();
					}

					this.caret.setEnd(node);
				},
				formatEmpty: function(e)
				{
					var html = $.trim(this.$editor.html());

					if (!this.utils.isEmpty(html)) return;

					e.preventDefault();

					if (this.opts.linebreaks)
					{
						this.$editor.html(this.selection.getMarkerAsHtml());
						this.selection.restore();
					}
					else
					{
						this.$editor.html(this.opts.emptyHtml);
						this.focus.setStart();
					}

					this.code.sync();

					return false;
				}
			};
		},
		lang: function()
		{
			return {
				load: function()
				{
					this.opts.curLang = this.opts.langs[this.opts.lang];
				},
				get: function(name)
				{
					return (typeof this.opts.curLang[name] != 'undefined') ? this.opts.curLang[name] : '';
				}
			};
		},
		line: function()
		{
			return {
				insert: function()
				{
					this.buffer.set();

					var blocks = this.selection.getBlocks();
 					if (blocks[0] !== false && this.line.isExceptLastOrFirst(blocks))
	 				{
	 					if (!this.utils.browser('msie')) this.$editor.focus();
	 					return;
					}

					if (this.utils.browser('msie'))
					{
						this.line.insertInIe();
					}
					else
					{
						this.line.insertInOthersBrowsers();
					}
				},
				isExceptLastOrFirst: function(blocks)
				{
					var exceptTags = ['li', 'td', 'th', 'blockquote', 'figcaption', 'pre', 'dl', 'dt', 'dd'];

					var first = blocks[0].tagName.toLowerCase();
					var last = this.selection.getLastBlock();

					last = (typeof last == 'undefined') ? first : last.tagName.toLowerCase();

					var firstFound = $.inArray(first, exceptTags) != -1;
					var lastFound = $.inArray(last, exceptTags) != -1;

					if ((firstFound && lastFound) || firstFound)
					{
						return true;
					}
				},
				insertInIe: function()
				{
					this.utils.saveScroll();
					this.buffer.set();

					this.insert.node(document.createElement('hr'));

					this.utils.restoreScroll();
					this.code.sync();
				},
				insertInOthersBrowsers: function()
				{
					this.buffer.set();

					var extra = '<p id="redactor-insert-line"><br /></p>';
					if (this.opts.linebreaks) extra = '<br id="redactor-insert-line">';

					document.execCommand('insertHtml', false, '<hr>' + extra);

					this.line.setFocus();
					this.code.sync();
				},
				setFocus: function()
				{
					var node = this.$editor.find('#redactor-insert-line');
					var next = $(node).next()[0];
					var target = next;
					if (this.utils.browser('mozilla') && next && next.innerHTML === '')
					{
						target = $(next).next()[0];
						$(next).remove();
					}

					if (target)
					{
						node.remove();

						if (!this.opts.linebreaks)
						{
							this.$editor.focus();
							this.line.setStart(target);
						}

					}
					else
					{

						node.removeAttr('id');
						this.line.setStart(node[0]);
					}
				},
				setStart: function(node)
				{
					if (typeof node === 'undefined') return;

					var textNode = document.createTextNode('\u200B');

					this.selection.get();
					this.range.setStart(node, 0);
					this.range.insertNode(textNode);
					this.range.collapse(true);
					this.selection.addRange();

				}
			};
		},
		link: function()
		{
			return {
				show: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault) e.preventDefault();

					if (!this.observe.isCurrent('a'))
					{
						this.modal.load('link', this.lang.get('link_insert'), 600);
					}
					else
					{
						this.modal.load('link', this.lang.get('link_edit'), 600);
					}

					this.modal.createCancelButton();

					var buttonText = !this.observe.isCurrent('a') ? this.lang.get('insert') : this.lang.get('edit');

					this.link.buttonInsert = this.modal.createActionButton(buttonText);

					this.selection.get();

					this.link.getData();
					this.link.cleanUrl();

					if (this.link.target == '_blank') $('#redactor-link-blank').prop('checked', true);

					this.link.$inputUrl = $('#redactor-link-url');
					this.link.$inputText = $('#redactor-link-url-text');

					this.link.$inputText.val(this.link.text);
					this.link.$inputUrl.val(this.link.url);

					this.link.buttonInsert.on('click', $.proxy(this.link.insert, this));

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					// show modal
					this.selection.save();
					this.modal.show();
					this.link.$inputUrl.focus();
				},
				cleanUrl: function()
				{
					var thref = self.location.href.replace(/\/$/i, '');

					if (typeof this.link.url !== "undefined")
					{
						this.link.url = this.link.url.replace(thref, '');
						this.link.url = this.link.url.replace(/^\/#/, '#');
						this.link.url = this.link.url.replace('mailto:', '');

						// remove host from href
						if (!this.opts.linkProtocol)
						{
							var re = new RegExp('^(http|ftp|https)://' + self.location.host, 'i');
							this.link.url = this.link.url.replace(re, '');
						}
					}
				},
				getData: function()
				{
					this.link.$node = false;

					var $el = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
					if ($el.length !== 0 && $el[0].tagName === 'A')
					{
						this.link.$node = $el;

						this.link.url = $el.attr('href');
						this.link.text = $el.text();
						this.link.target = $el.attr('target');
					}
					else
					{
						this.link.text = this.sel.toString();
						this.link.url = '';
						this.link.target = '';
					}

				},
				insert: function()
				{
					this.placeholder.remove();

					var target = '';
					var link = this.link.$inputUrl.val();
					var text = this.link.$inputText.val().replace(/(<([^>]+)>)/ig,"");

					if ($.trim(link) === '')
					{
						this.link.$inputUrl.addClass('redactor-input-error').on('keyup', function()
						{
							$(this).removeClass('redactor-input-error');
							$(this).off('keyup');

						});

						return;
					}

					// mailto
					if (link.search('@') != -1 && /(http|ftp|https):\/\//i.test(link) === false)
					{
						link = 'mailto:' + link;
					}
					// url, not anchor
					else if (link.search('#') !== 0)
					{
						if ($('#redactor-link-blank').prop('checked'))
						{
							target = '_blank';
						}

						// test url (add protocol)
						var pattern = '((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}';
						var re = new RegExp('^(http|ftp|https)://' + pattern, 'i');
						var re2 = new RegExp('^' + pattern, 'i');
						var re3 = new RegExp('\.(html|php)$', 'i');
						if (link.search(re) == -1 && link.search(re3) == -1 && link.search(re2) === 0 && this.opts.linkProtocol)
						{
							link = this.opts.linkProtocol + '://' + link;
						}
					}

					this.link.set(text, link, target);
					this.modal.close();
				},
				set: function(text, link, target)
				{
					text = $.trim(text.replace(/<|>/g, ''));

					this.selection.restore();
					var blocks = this.selection.getBlocks();

					if (text === '' && link === '') return;
					if (text === '' && link !== '') text = link;

					if (this.link.$node)
					{
						this.buffer.set();

						var $link = this.link.$node,
							$el   = $link.children();

						if ($el.length > 0)
						{
							while ($el.length)
							{
								$el = $el.children();
							}

							$el = $el.end();
						}
						else
						{
							$el = $link;
						}

						$link.attr('href', link);
						$el.text(text);

						if (target !== '')
						{
							$link.attr('target', target);
						}
						else
						{
							$link.removeAttr('target');
						}

						this.selection.selectElement($link);

						this.code.sync();
					}
					else
					{
						if (this.utils.browser('mozilla') && this.link.text === '')
						{
							var $a = $('<a />').attr('href', link).text(text);
							if (target !== '') $a.attr('target', target);

							$a = $(this.insert.node($a));

							if (this.opts.linebreaks)
							{
								$a.after('&nbsp;');
							}

							this.selection.selectElement($a);
						}
						else
						{
							var $a;
							if (this.utils.browser('msie'))
							{
								$a = $('<a href="' + link + '">').text(text);
								if (target !== '') $a.attr('target', target);

								$a = $(this.insert.node($a));

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								this.selection.selectElement($a);
							}
							else
							{
								document.execCommand('createLink', false, link);

								$a = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
								if (this.utils.browser('mozilla'))
								{
									$a = $('a[_moz_dirty=""]');
								}

								if (target !== '') $a.attr('target', target);
								$a.removeAttr('style').removeAttr('_moz_dirty');

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								if (this.link.text !== '' || this.link.text != text)
								{
									if (!this.opts.linebreaks && blocks && blocks.length <= 1)
									{
										$a.text(text);
									}
									else if (this.opts.linebreaks)
									{
										$a.text(text);
									}

									this.selection.selectElement($a);
								}
							}
						}

						this.code.sync();
						this.core.setCallback('insertedLink', $a);

					}

					// link tooltip
					setTimeout($.proxy(function()
					{
						this.observe.links();

					}, this), 5);
				},
				unlink: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault)
					{
						e.preventDefault();
					}

					var nodes = this.selection.getNodes();
					if (!nodes) return;

					this.buffer.set();

					var len = nodes.length;
					var links = [];
					for (var i = 0; i < len; i++)
					{
						if (nodes[i].tagName === 'A')
						{
							links.push(nodes[i]);
						}

						var $node = $(nodes[i]).closest('a', this.$editor[0]);
						$node.replaceWith($node.contents());
					}

					this.core.setCallback('deletedLink', links);

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					this.code.sync();

				},
				toggleClass: function(className)
				{
					this.link.setClass(className, 'toggleClass');
				},
				addClass: function(className)
				{
					this.link.setClass(className, 'addClass');
				},
				removeClass: function(className)
				{
					this.link.setClass(className, 'removeClass');
				},
				setClass: function(className, func)
				{
					var links = this.selection.getInlinesTags(['a']);
					if (links === false) return;

					$.each(links, function()
					{
						$(this)[func](className);
					});
				}
			};
		},
		linkify: function()
		{
			return {
				isKey: function(key)
				{
					return key == this.keyCode.ENTER || key == this.keyCode.SPACE;
				},
				isEnabled: function()
				{
					return this.opts.convertLinks && (this.opts.convertUrlLinks || this.opts.convertImageLinks || this.opts.convertVideoLinks) && !this.utils.isCurrentOrParent('PRE');
				},
				format: function()
				{
					var linkify = this.linkify,
						opts    = this.opts;

					this.$editor
						.find(":not(iframe,img,a,pre)")
						.addBack()
						.contents()
						.filter(function()
						{
							return this.nodeType === 3 && $.trim(this.nodeValue) != "" && !$(this).parent().is("pre") && (this.nodeValue.match(opts.linkify.regexps.youtube) || this.nodeValue.match(opts.linkify.regexps.vimeo) || this.nodeValue.match(opts.linkify.regexps.image) || this.nodeValue.match(opts.linkify.regexps.url));
						})
						.each(function()
						{
							var text = $(this).text(),
								html = text;

							if (opts.convertVideoLinks && (html.match(opts.linkify.regexps.youtube) || html.match(opts.linkify.regexps.vimeo)) )
							{
								html = linkify.convertVideoLinks(html);
							}
							else if (opts.convertImageLinks && html.match(opts.linkify.regexps.image))
							{
								html = linkify.convertImages(html);
							}
							else if (opts.convertUrlLinks)
							{
								html = linkify.convertLinks(html);
							}

							$(this).before(text.replace(text, html))
								   .remove();
						});


					var objects = this.$editor.find('.redactor-linkify-object').each(function()
					{
						var $el = $(this);
						$el.removeClass('redactor-linkify-object');
						if ($el.attr('class') === '') $el.removeAttr('class');

						return $el[0];

					});

					// callback
					setTimeout($.proxy(function()
					{
						this.observe.load();
						this.core.setCallback('linkify', objects);
					}, this), 100);

					// sync
					this.code.sync();
				},
				convertVideoLinks: function(html)
				{
					var iframeStart = '<iframe class="redactor-linkify-object" width="500" height="281" src="',
						iframeEnd = '" frameborder="0" allowfullscreen></iframe>';

					if (html.match(this.opts.linkify.regexps.youtube))
					{
						html = html.replace(this.opts.linkify.regexps.youtube, iframeStart + '//www.youtube.com/embed/$1' + iframeEnd);
					}

					if (html.match(this.opts.linkify.regexps.vimeo))
					{
						html = html.replace(this.opts.linkify.regexps.vimeo, iframeStart + '//player.vimeo.com/video/$2' + iframeEnd);
					}

					return html;
				},
				convertImages: function(html)
				{
					var matches = html.match(this.opts.linkify.regexps.image);

					if (matches)
					{
						html = html.replace(html, '<img src="' + matches + '" class="redactor-linkify-object" />');

						if (this.opts.linebreaks)
						{
							if (!this.utils.isEmpty(this.code.get()))
							{
								html = '<br>' + html;
							}
						}

						html += '<br>';
					}

					return html;
				},
				convertLinks: function(html)
				{
					var matches = html.match(this.opts.linkify.regexps.url);

					if (matches)
					{
						matches = $.grep(matches, function(v, k) { return $.inArray(v, matches) === k; });

						var length = matches.length;

						for (var i = 0; i < length; i++)
						{
							var href = matches[i],
								text = href,
								linkProtocol = this.opts.linkProtocol + '://';

							if (href.match(/(https?|ftp):\/\//i) !== null)
							{
								linkProtocol = "";
							}

							if (text.length > this.opts.linkSize)
							{
								text = text.substring(0, this.opts.linkSize) + '...';
							}

							if (text.search('%') === -1)
							{
								text = decodeURIComponent(text);
							}

							var regexB = "\\b";

							if ($.inArray(href.slice(-1), ["/", "&", "="]) != -1)
							{
								regexB = "";
							}

							// escaping url
							var regexp = new RegExp('(' + href.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + regexB + ')', 'g');

							html = html.replace(regexp, '<a href="' + linkProtocol + $.trim(href) + '" class="redactor-linkify-object">' + $.trim(text) + '</a>');
						}
					}

					return html;
				}
			};
		},
		list: function()
		{
			return {
				toggle: function(cmd)
				{
					this.placeholder.remove();
					if (!this.utils.browser('msie')) this.$editor.focus();

					this.buffer.set();
					this.selection.save();

					var parent = this.selection.getParent();
					var $list = $(parent).closest('ol, ul', this.$editor[0]);

					if (!this.utils.isRedactorParent($list) && $list.length !== 0)
					{
						$list = false;
					}

					var isUnorderedCmdOrdered, isOrderedCmdUnordered;
					var remove = false;
					if ($list && $list.length)
					{
						remove = true;
						var listTag = $list[0].tagName;

						isUnorderedCmdOrdered = (cmd === 'orderedlist' && listTag === 'UL');
						isOrderedCmdUnordered = (cmd === 'unorderedlist' && listTag === 'OL');
					}

					if (isUnorderedCmdOrdered)
					{
						this.utils.replaceToTag($list, 'ol');
					}
					else if (isOrderedCmdUnordered)
					{
						this.utils.replaceToTag($list, 'ul');
					}
					else
					{
						if (remove)
						{
							this.list.remove(cmd, $list);
						}
						else
						{
							this.list.insert(cmd);
						}
					}

					this.selection.restore();
					this.code.sync();

				},
				insert: function(cmd)
				{
					var current = this.selection.getCurrent();
					var $td = $(current).closest('td, th', this.$editor[0]);

					if (this.utils.browser('msie') && this.opts.linebreaks)
					{
						this.list.insertInIe(cmd);
					}
					else
					{
						document.execCommand('insert' + cmd);
					}

					var parent = this.selection.getParent();
					var $list = $(parent).closest('ol, ul', this.$editor[0]);
					if ($td.length !== 0)
					{
						var newTd = $td.clone();
						$td.after(newTd).remove('');
					}


					if (this.utils.isEmpty($list.find('li').text()))
					{
						var $children = $list.children('li');
						$children.find('br').remove();
						$children.append(this.selection.getMarkerAsHtml());

						if (this.opts.linebreaks && this.utils.browser('mozilla') && $children.size() == 2 && this.utils.isEmpty($children.eq(1).text()))
						{
							$children.eq(1).remove();
						}
					}

					if ($list.length)
					{
						// remove block-element list wrapper
						var $listParent = $list.parent();
						if (this.utils.isRedactorParent($listParent) && $listParent[0].tagName != 'LI' && this.utils.isBlock($listParent[0]))
						{
							$listParent.replaceWith($listParent.contents());
						}
					}

					if (!this.utils.browser('msie'))
					{
						this.$editor.focus();
					}


					this.clean.clearUnverified();
				},
				insertInIe: function(cmd)
				{
					var wrapper = this.selection.wrap('div');
					var wrapperHtml = $(wrapper).html();

					var tmpList = (cmd == 'orderedlist') ? $('<ol>') : $('<ul>');
					var tmpLi = $('<li>');

					if ($.trim(wrapperHtml) === '')
					{
						tmpLi.append(this.selection.getMarkerAsHtml());
						tmpList.append(tmpLi);
						this.$editor.find('#selection-marker-1').replaceWith(tmpList);
					}
					else
					{
						var items = wrapperHtml.split(/<br\s?\/?>/gi);
						if (items)
						{
							for (var i = 0; i < items.length; i++)
							{
								if ($.trim(items[i]) !== '')
								{
									tmpList.append($('<li>').html(items[i]));
								}
							}
						}
						else
						{
							tmpLi.append(wrapperHtml);
							tmpList.append(tmpLi);
						}

						$(wrapper).replaceWith(tmpList);
					}
				},
				remove: function(cmd, $list)
				{
					if ($.inArray('ul', this.selection.getBlocks())) cmd = 'unorderedlist';

					document.execCommand('insert' + cmd);

					var $current = $(this.selection.getCurrent());
					this.indent.fixEmptyIndent();

					if (!this.opts.linebreaks && $current.closest('li, th, td', this.$editor[0]).length === 0)
					{
						document.execCommand('formatblock', false, 'p');
						this.$editor.find('ul, ol, blockquote').each($.proxy(this.utils.removeEmpty, this));
					}

					var $table = $(this.selection.getCurrent()).closest('table', this.$editor[0]);
					var $prev = $table.prev();
					if (!this.opts.linebreaks && $table.length !== 0 && $prev.length !== 0 && $prev[0].tagName == 'BR')
					{
						$prev.remove();
					}

					this.clean.clearUnverified();


				}
			};
		},
		modal: function()
		{
			return {
				callbacks: {},
				loadTemplates: function()
				{
					this.opts.modal = {
						imageEdit: String()
						+ '<section id="redactor-modal-image-edit">'
							+ '<label>' + this.lang.get('title') + '</label>'
							+ '<input class="form-control" type="text" id="redactor-image-title" />'
							+ '<label class="redactor-image-link-option">' + this.lang.get('link') + '</label>'
							+ '<input class="form-control" type="text" id="redactor-image-link" class="redactor-image-link-option" aria-label="' + this.lang.get('link') + '" />'
							+ '<label class="redactor-image-link-option"><input class="form-control" type="checkbox" id="redactor-image-link-blank" aria-label="' + this.lang.get('link_new_tab') + '"> ' + this.lang.get('link_new_tab') + '</label>'
							+ '<label class="redactor-image-position-option">' + this.lang.get('image_position') + '</label>'
							+ '<select class="redactor-image-position-option" id="redactor-image-align" aria-label="' + this.lang.get('image_position') + '">'
								+ '<option value="none">' + this.lang.get('none') + '</option>'
								+ '<option value="left">' + this.lang.get('left') + '</option>'
								+ '<option value="center">' + this.lang.get('center') + '</option>'
								+ '<option value="right">' + this.lang.get('right') + '</option>'
							+ '</select>'
						+ '</section>',

						image: String()
						+ '<section id="redactor-modal-image-insert">'
							+ '<div id="redactor-modal-image-droparea"></div>'
 						+ '</section>',

						file: String()
						+ '<section id="redactor-modal-file-insert">'
							+ '<div id="redactor-modal-file-upload-box">'
								+ '<label>' + this.lang.get('filename') + '</label>'
								+ '<input class="form-control" type="text" id="redactor-filename" aria-label="' + this.lang.get('filename') + '" /><br><br>'
								+ '<div id="redactor-modal-file-upload"></div>'
							+ '</div>'
						+ '</section>',

						link: String()
						+ '<section id="redactor-modal-link-insert">'
							+ '<label>URL</label>'
							+ '<input class="form-control" type="url" id="redactor-link-url" aria-label="URL" />'
							+ '<label>' + this.lang.get('text') + '</label>'
							+ '<input class="form-control" type="text" id="redactor-link-url-text" aria-label="' + this.lang.get('text') + '" />'
							+ '<label><input class="form-control" type="checkbox" id="redactor-link-blank"> ' + this.lang.get('link_new_tab') + '</label>'
						+ '</section>'
					};


					$.extend(this.opts, this.opts.modal);

				},
				addCallback: function(name, callback)
				{
					this.modal.callbacks[name] = callback;
				},
				createTabber: function($modal)
				{
					this.modal.$tabber = $('<div>').attr('id', 'redactor-modal-tabber');

					$modal.prepend(this.modal.$tabber);
				},
				addTab: function(id, name, active)
				{
					var $tab = $('<a href="#" rel="tab' + id + '">').text(name);
					if (active)
					{
						$tab.addClass('active');
					}

					var self = this;
					$tab.on('click', function(e)
					{
						e.preventDefault();
						$('.redactor-tab').hide();
						$('.redactor-' + $(this).attr('rel')).show();

						self.modal.$tabber.find('a').removeClass('active');
						$(this).addClass('active');

					});

					this.modal.$tabber.append($tab);
				},
				addTemplate: function(name, template)
				{
					this.opts.modal[name] = template;
				},
				getTemplate: function(name)
				{
					return this.opts.modal[name];
				},
				getModal: function()
				{
					return this.$modalBody.find('section');
				},
				load: function(templateName, title, width)
				{
					this.modal.templateName = templateName;
					this.modal.width = width;

					this.modal.build();
					this.modal.enableEvents();
					this.modal.setTitle(title);
					this.modal.setDraggable();
					this.modal.setContent();

					// callbacks
					if (typeof this.modal.callbacks[templateName] != 'undefined')
					{
						this.modal.callbacks[templateName].call(this);
					}

				},
				show: function()
				{
					this.utils.disableBodyScroll();

					if (this.utils.isMobile())
					{
						this.modal.showOnMobile();
					}
					else
					{
						this.modal.showOnDesktop();
					}

					if (this.opts.highContrast)
					{
						this.$modalBox.addClass("redactor-modal-contrast");
					}

					this.$modalOverlay.show();
					this.$modalBox.show();

					this.$modal.attr('tabindex', '-1');
					this.$modal.focus();

					this.modal.setButtonsWidth();

					this.utils.saveScroll();

					// resize
					if (!this.utils.isMobile())
					{
						setTimeout($.proxy(this.modal.showOnDesktop, this), 0);
						$(window).on('resize.redactor-modal', $.proxy(this.modal.resize, this));
					}

					// modal shown callback
					this.core.setCallback('modalOpened', this.modal.templateName, this.$modal);

					// fix bootstrap modal focus
					$(document).off('focusin.modal');

					// enter
					this.$modal.find('input[type=text],input[type=url],input[type=email]').on('keydown.redactor-modal', $.proxy(this.modal.setEnter, this));
				},
				showOnDesktop: function()
				{
					var height = this.$modal.outerHeight();
					var windowHeight = $(window).height();
					var windowWidth = $(window).width();

					if (this.modal.width > windowWidth)
					{
						this.$modal.css({
							width: '96%',
							marginTop: (windowHeight/2 - height/2) + 'px'
						});
						return;
					}

					if (height > windowHeight)
					{
						this.$modal.css({
							width: this.modal.width + 'px',
							marginTop: '20px'
						});
					}
					else
					{
						this.$modal.css({
							width: this.modal.width + 'px',
							marginTop: (windowHeight/2 - height/2) + 'px'
						});
					}
				},
				showOnMobile: function()
				{
					this.$modal.css({
						width: '96%',
						marginTop: '2%'
					});

				},
				resize: function()
				{
					if (this.utils.isMobile())
					{
						this.modal.showOnMobile();
					}
					else
					{
						this.modal.showOnDesktop();
					}
				},
				setTitle: function(title)
				{
					this.$modalHeader.html(title);
				},
				setContent: function()
				{
					this.$modalBody.html(this.modal.getTemplate(this.modal.templateName));
				},
				setDraggable: function()
				{
					if (typeof $.fn.draggable === 'undefined') return;

					this.$modal.draggable({ handle: this.$modalHeader });
					this.$modalHeader.css('cursor', 'move');
				},
				setEnter: function(e)
				{
					if (e.which != 13) return;

					e.preventDefault();
					this.$modal.find('button.redactor-modal-action-btn').click();
				},
				createCancelButton: function()
				{
					var button = $('<button>').addClass('redactor-modal-btn redactor-modal-close-btn').html(this.lang.get('cancel'));
					button.on('click', $.proxy(this.modal.close, this));

					this.$modalFooter.append(button);
				},
				createDeleteButton: function(label)
				{
					return this.modal.createButton(label, 'delete');
				},
				createActionButton: function(label)
				{
					return this.modal.createButton(label, 'action');
				},
				createButton: function(label, className)
				{
					var button = $('<button>').addClass('redactor-modal-btn').addClass('redactor-modal-' + className + '-btn').html(label);
					this.$modalFooter.append(button);

					return button;
				},
				setButtonsWidth: function()
				{
					var buttons = this.$modalFooter.find('button');
					var buttonsSize = buttons.length;
					if (buttonsSize === 0) return;

					buttons.css('width', (100/buttonsSize) + '%');
				},
				build: function()
				{
					this.modal.buildOverlay();

					this.$modalBox = $('<div id="redactor-modal-box"/>').hide();
					this.$modal = $('<div id="redactor-modal" role="dialog" aria-labelledby="redactor-modal-header" />');
					this.$modalHeader = $('<header id="redactor-modal-header"/>');
					this.$modalClose = $('<button type="button" id="redactor-modal-close" tabindex="1" aria-label="Close" />').html('&times;');
					this.$modalBody = $('<div id="redactor-modal-body" />');
					this.$modalFooter = $('<footer />');

					this.$modal.append(this.$modalHeader);
					this.$modal.append(this.$modalClose);
					this.$modal.append(this.$modalBody);
					this.$modal.append(this.$modalFooter);
					this.$modalBox.append(this.$modal);
					this.$modalBox.appendTo(document.body);
				},
				buildOverlay: function()
				{
					this.$modalOverlay = $('<div id="redactor-modal-overlay">').hide();
					$('body').prepend(this.$modalOverlay);
				},
				enableEvents: function()
				{
					this.$modalClose.on('click.redactor-modal', $.proxy(this.modal.close, this));
					$(document).on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
					this.$editor.on('keyup.redactor-modal', $.proxy(this.modal.closeHandler, this));
					this.$modalBox.on('click.redactor-modal', $.proxy(this.modal.close, this));
				},
				disableEvents: function()
				{
					this.$modalClose.off('click.redactor-modal');
					$(document).off('keyup.redactor-modal');
					this.$editor.off('keyup.redactor-modal');
					this.$modalBox.off('click.redactor-modal');
					$(window).off('resize.redactor-modal');
				},
				closeHandler: function(e)
				{
					if (e.which != this.keyCode.ESC) return;

					this.modal.close(false);
				},
				close: function(e)
				{
					if (e)
					{
						if (!$(e.target).hasClass('redactor-modal-close-btn') && e.target != this.$modalClose[0] && e.target != this.$modalBox[0])
						{
							return;
						}

						e.preventDefault();
					}

					if (!this.$modalBox) return;

					this.modal.disableEvents();
					this.utils.enableBodyScroll();

					this.$modalOverlay.remove();

					this.$modalBox.fadeOut('fast', $.proxy(function()
					{
						this.$modalBox.remove();

						setTimeout($.proxy(this.utils.restoreScroll, this), 0);

						if (e !== undefined) this.selection.restore();

						$(document.body).css('overflow', this.modal.bodyOveflow);
						this.core.setCallback('modalClosed', this.modal.templateName);

					}, this));

				}
			};
		},
		observe: function()
		{
			return {
				load: function()
				{
					if (typeof this.opts.destroyed != "undefined") return;

					if (this.utils.browser('msie'))
					{
						var self = this;
						this.$editor.find('pre, code').on('mouseover',function()
						{
							self.$editor.attr('contenteditable', false);
							$(this).attr('contenteditable', true);

						}).on('mouseout',function()
						{
							self.$editor.attr('contenteditable', true);
							$(this).removeAttr('contenteditable');

						});
					}

					this.observe.images();
					this.observe.links();
				},
				toolbar: function(e, btnName)
				{
					this.observe.buttons(e, btnName);
					this.observe.dropdowns();
				},
				isCurrent: function($el, $current)
				{
					if (typeof $current == 'undefined')
					{
						var $current = $(this.selection.getCurrent());
					}

					return $current.is($el) || $current.parents($el).length > 0;
				},
				dropdowns: function()
				{
					var $current = $(this.selection.getCurrent());

					$.each(this.opts.observe.dropdowns, $.proxy(function(key, value)
					{
						var observe = value.observe,
							element = observe.element,
							$item   = value.item,
							inValues = typeof observe['in'] != 'undefined' ? observe['in'] : false,
							outValues = typeof observe['out'] != 'undefined' ? observe['out'] : false;

						if ($current.closest(element).size() > 0)
						{
							this.observe.setDropdownProperties($item, inValues, outValues);
						}
						else
						{
							this.observe.setDropdownProperties($item, outValues, inValues);
						}
					}, this));
				},
				setDropdownProperties: function($item, addProperties, deleteProperties)
				{
					if (deleteProperties && typeof deleteProperties['attr'] != 'undefined')
					{
						this.observe.setDropdownAttr($item, deleteProperties.attr, true);
					}

					if (typeof addProperties['attr'] != 'undefined')
					{
						this.observe.setDropdownAttr($item, addProperties.attr);
					}

					if (typeof addProperties['title'] != 'undefined')
					{
						$item.text(addProperties['title']);
					}
				},
				setDropdownAttr: function($item, properties, isDelete)
				{
					$.each(properties, function(key, value)
					{
						if (key == 'class')
						{
							if (!isDelete)
							{
								$item.addClass(value);
							}
							else
							{
								$item.removeClass(value);
							}
						}
						else
						{
							if (!isDelete)
							{
								$item.attr(key, value);
							}
							else
							{
								$item.removeAttr(key);
							}
						}
					});
				},
				addDropdown: function($item, btnName, btnObject)
				{
					if (typeof btnObject.observe == "undefined") return;

					btnObject.item = $item;

					this.opts.observe.dropdowns.push(btnObject);
				},
				buttons: function(e, btnName)
				{
					var current = this.selection.getCurrent();
					var parent = this.selection.getParent();

					if (e !== false)
					{
						this.button.setInactiveAll();
					}
					else
					{
						this.button.setInactiveAll(btnName);
					}

					if (e === false && btnName !== 'html')
					{
						if ($.inArray(btnName, this.opts.activeButtons) != -1) this.button.toggleActive(btnName);
						return;
					}

					//var linkButtonName = (this.utils.isCurrentOrParent('A')) ? this.lang.get('link_edit') : this.lang.get('link_insert');
					//$('body').find('a.redactor-dropdown-link').text(linkButtonName);

					$.each(this.opts.activeButtonsStates, $.proxy(function(key, value)
					{
						var parentEl = $(parent).closest(key, this.$editor[0]);
						var currentEl = $(current).closest(key, this.$editor[0]);

						if (parentEl.length !== 0 && !this.utils.isRedactorParent(parentEl)) return;
						if (!this.utils.isRedactorParent(currentEl)) return;
						if (parentEl.length !== 0 || currentEl.closest(key, this.$editor[0]).length !== 0)
						{
							this.button.setActive(value);
						}

					}, this));

					var $parent = $(parent).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
					if (this.utils.isRedactorParent(parent) && $parent.length)
					{
						var align = ($parent.css('text-align') === '') ? 'left' : $parent.css('text-align');
						this.button.setActive('align' + align);
					}
				},
				addButton: function(tag, btnName)
				{
					this.opts.activeButtons.push(btnName);
					this.opts.activeButtonsStates[tag] = btnName;
				},
				images: function()
				{
					this.$editor.find('img').each($.proxy(function(i, img)
					{
						var $img = $(img);

						// IE fix (when we clicked on an image and then press backspace IE does goes to image's url)
						$img.closest('a', this.$editor[0]).on('click', function(e) { e.preventDefault(); });

						if (this.utils.browser('msie')) $img.attr('unselectable', 'on');

						this.image.setEditable($img);

					}, this));

					$(document).on('click.redactor-image-delete.' + this.uuid, $.proxy(function(e)
					{
						this.observe.image = false;
						if (e.target.tagName == 'IMG' && this.utils.isRedactorParent(e.target))
						{
							this.observe.image = (this.observe.image && this.observe.image == e.target) ? false : e.target;
						}

					}, this));

				},
				links: function()
				{
					if (!this.opts.linkTooltip) return;

					this.$editor.find('a').on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.showTooltip, this));
					this.$editor.on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.closeTooltip, this));
					$(document).on('touchstart.redactor.' + this.uuid + ' click.redactor.' + this.uuid, $.proxy(this.observe.closeTooltip, this));
				},
				getTooltipPosition: function($link)
				{
					return $link.offset();
				},
				showTooltip: function(e)
				{
					var $el = $(e.target);

					if ($el[0].tagName == 'IMG')
						return;

					if ($el[0].tagName !== 'A')
						$el = $el.closest('a', this.$editor[0]);

					if ($el[0].tagName !== 'A')
						return;

					var $link = $el;

					var pos = this.observe.getTooltipPosition($link);
					var tooltip = $('<span class="redactor-link-tooltip"></span>');

					var href = $link.attr('href');
					if (href === undefined)
					{
						href = '';
					}

					if (href.length > 24) href = href.substring(0, 24) + '...';

					var aLink = $('<a href="' + $link.attr('href') + '" target="_blank" />').html(href).addClass('redactor-link-tooltip-action');
					var aEdit = $('<a href="#" />').html(this.lang.get('edit')).on('click', $.proxy(this.link.show, this)).addClass('redactor-link-tooltip-action');
					var aUnlink = $('<a href="#" />').html(this.lang.get('unlink')).on('click', $.proxy(this.link.unlink, this)).addClass('redactor-link-tooltip-action');

					tooltip.append(aLink).append(' | ').append(aEdit).append(' | ').append(aUnlink);
					tooltip.css({
						top: (pos.top + parseInt($link.css('line-height'), 10)) + 'px',
						left: pos.left + 'px'
					});

					$('.redactor-link-tooltip').remove();
					$('body').append(tooltip);
				},
				closeTooltip: function(e)
				{
					e = e.originalEvent || e;

					var target = e.target;
					var $parent = $(target).closest('a', this.$editor[0]);
					if ($parent.length !== 0 && $parent[0].tagName === 'A' && target.tagName !== 'A')
					{
						return;
					}
					else if ((target.tagName === 'A' && this.utils.isRedactorParent(target)) || $(target).hasClass('redactor-link-tooltip-action'))
					{
						return;
					}

					$('.redactor-link-tooltip').remove();
				}

			};
		},
		paragraphize: function()
		{
			return {
				load: function(html)
				{
					if (this.opts.linebreaks) return html;
					if (html === '' || html === '<p></p>') return this.opts.emptyHtml;

					html = html + "\n";

					this.paragraphize.safes = [];
					this.paragraphize.z = 0;

					html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, '</blockquote>');

					html = this.paragraphize.getSafes(html);
					html = this.paragraphize.getSafesComments(html);
					html = this.paragraphize.replaceBreaksToNewLines(html);
					html = this.paragraphize.replaceBreaksToParagraphs(html);
					html = this.paragraphize.clear(html);
					html = this.paragraphize.restoreSafes(html);

					html = html.replace(new RegExp('<br\\s?/?>\n?<(' + this.opts.paragraphizeBlocks.join('|') + ')(.*?[^>])>', 'gi'), '<p><br /></p>\n<$1$2>');

					return $.trim(html);
				},
				getSafes: function(html)
				{
					var $div = $('<div />').append(html);

					// remove paragraphs in blockquotes
					$div.find('blockquote p').replaceWith(function()
					{
						return $(this).append('<br />').contents();
					});

					html = $div.html();

					$div.find(this.opts.paragraphizeBlocks.join(', ')).each($.proxy(function(i,s)
					{
						this.paragraphize.z++;
						this.paragraphize.safes[this.paragraphize.z] = s.outerHTML;
						html = html.replace(s.outerHTML, '\n{replace' + this.paragraphize.z + '}');

					}, this));

					return html;
				},
				getSafesComments: function(html)
				{
					var commentsMatches = html.match(/<!--([\w\W]*?)-->/gi);

					if (!commentsMatches) return html;

					$.each(commentsMatches, $.proxy(function(i,s)
					{
						this.paragraphize.z++;
						this.paragraphize.safes[this.paragraphize.z] = s;
						html = html.replace(s, '\n{replace' + this.paragraphize.z + '}');
					}, this));

					return html;
				},
				restoreSafes: function(html)
				{
					$.each(this.paragraphize.safes, function(i,s)
					{
						s = (typeof s !== 'undefined') ? s.replace(/\$/g, '&#36;') : s;
						html = html.replace('{replace' + i + '}', s);

					});

					return html;
				},
				replaceBreaksToParagraphs: function(html)
				{
					var htmls = html.split(new RegExp('\n', 'g'), -1);

					html = '';
					if (htmls)
					{
						var len = htmls.length;
						for (var i = 0; i < len; i++)
						{
							if (!htmls.hasOwnProperty(i)) return;

							if (htmls[i].search('{replace') == -1)
							{
								htmls[i] = htmls[i].replace(/<p>\n\t?<\/p>/gi, '');
								htmls[i] = htmls[i].replace(/<p><\/p>/gi, '');

								if (htmls[i] !== '')
								{
									html += '<p>' +  htmls[i].replace(/^\n+|\n+$/g, "") + "</p>";
								}
							}
							else html += htmls[i];
						}
					}

					return html;
				},
				replaceBreaksToNewLines: function(html)
				{
					html = html.replace(/<br \/>\s*<br \/>/gi, "\n\n");
					html = html.replace(/<br\s?\/?>\n?<br\s?\/?>/gi, "\n<br /><br />");

					html = html.replace(new RegExp("\r\n", 'g'), "\n");
					html = html.replace(new RegExp("\r", 'g'), "\n");
					html = html.replace(new RegExp("/\n\n+/"), 'g', "\n\n");

					return html;
				},
				clear: function(html)
				{
					html = html.replace(new RegExp('</blockquote></p>', 'gi'), '</blockquote>');
					html = html.replace(new RegExp('<p></blockquote>', 'gi'), '</blockquote>');
					html = html.replace(new RegExp('<p><blockquote>', 'gi'), '<blockquote>');
					html = html.replace(new RegExp('<blockquote></p>', 'gi'), '<blockquote>');

					html = html.replace(new RegExp('<p><p ', 'gi'), '<p ');
					html = html.replace(new RegExp('<p><p>', 'gi'), '<p>');
					html = html.replace(new RegExp('</p></p>', 'gi'), '</p>');
					html = html.replace(new RegExp('<p>\\s?</p>', 'gi'), '');
					html = html.replace(new RegExp("\n</p>", 'gi'), '</p>');
					html = html.replace(new RegExp('<p>\t?\t?\n?<p>', 'gi'), '<p>');
					html = html.replace(new RegExp('<p>\t*</p>', 'gi'), '');

					return html;
				}
			};
		},
		paste: function()
		{
			return {
				init: function(e)
				{
					if (!this.opts.cleanOnPaste)
					{
						setTimeout($.proxy(this.code.sync, this), 1);
						return;
					}

					this.rtePaste = true;

					this.buffer.set();
					this.selection.save();
					this.utils.saveScroll();

					this.paste.createPasteBox();

					$(window).on('scroll.redactor-freeze', $.proxy(function()
					{
						$(window).scrollTop(this.saveBodyScroll);

					}, this));

					setTimeout($.proxy(function()
					{
						var html = this.$pasteBox.html();

						this.$pasteBox.remove();

						this.selection.restore();
						this.utils.restoreScroll();

						this.paste.insert(html);

						$(window).off('scroll.redactor-freeze');

						if (this.linkify.isEnabled())
						{
							this.linkify.format();
						}

					}, this), 1);
				},
				createPasteBox: function()
				{
					this.$pasteBox = $('<div>').html('').attr('contenteditable', 'true').css({ position: 'fixed', width: 0, top: 0, left: '-9999px' });

					if (this.utils.browser('msie'))
					{
						this.$box.append(this.$pasteBox);
					}
					else
					{
						// bootstrap modal
						if ($('.modal-body').length > 0)
						{

							$('.modal.in .modal-body').append(this.$pasteBox);
						}
						else
						{
							$('body').append(this.$pasteBox);
						}

					}

					this.$pasteBox.focus();
				},
				insert: function(html)
				{
					html = this.core.setCallback('pasteBefore', html);

					// clean
					html = (this.utils.isSelectAll()) ? this.clean.onPaste(html, false) : this.clean.onPaste(html);

					html = this.core.setCallback('paste', html);

					if (this.utils.isSelectAll())
					{
						this.insert.set(html, false);
					}
					else
					{
						this.insert.html(html, false);
					}

					this.utils.disableSelectAll();
					this.rtePaste = false;

					setTimeout($.proxy(this.clean.clearUnverified, this), 10);

					// clean empty spans
					setTimeout($.proxy(function()
					{
						var spans = this.$editor.find('span');
						$.each(spans, function(i,s)
						{
							var html = s.innerHTML.replace(/\u200B/, '');
							if (html === '' && s.attributes.length === 0) $(s).remove();

						});

					}, this), 10);

				}
			};
		},
		placeholder: function()
		{
			return {
				enable: function()
				{
					if (!this.placeholder.is()) return;

					this.$editor.attr('placeholder', this.$element.attr('placeholder'));

					this.placeholder.toggle();
					this.$editor.on('keydown.redactor-placeholder', $.proxy(this.placeholder.toggle, this));
				},
				toggle: function()
				{
					setTimeout($.proxy(function()
					{
						var func = this.utils.isEmpty(this.$editor.html(), false) ? 'addClass' : 'removeClass';
						this.$editor[func]('redactor-placeholder');

					}, this), 5);
				},
				remove: function()
				{
					this.$editor.removeClass('redactor-placeholder');
				},
				is: function()
				{
					if (this.opts.placeholder)
					{
						return this.$element.attr('placeholder', this.opts.placeholder);
					}
					else
					{
						return !(typeof this.$element.attr('placeholder') == 'undefined' || this.$element.attr('placeholder') === '');
					}
				}
			};
		},
		progress: function()
		{
			return {
				show: function()
				{
					$(document.body).append($('<div id="redactor-progress"><span></span></div>'));
					$('#redactor-progress').fadeIn();
				},
				hide: function()
				{
					$('#redactor-progress').fadeOut(1500, function()
					{
						$(this).remove();
					});
				}

			};
		},
		selection: function()
		{
			return {
				get: function()
				{
					this.sel = document.getSelection();

					if (document.getSelection && this.sel.getRangeAt && this.sel.rangeCount)
					{
						this.range = this.sel.getRangeAt(0);
					}
					else
					{
						this.range = document.createRange();
					}
				},
				addRange: function()
				{
					try {
						this.sel.removeAllRanges();
					} catch (e) {}

					this.sel.addRange(this.range);
				},
				getCurrent: function()
				{
					var el = false;

					this.selection.get();

					if (this.sel && this.sel.rangeCount > 0)
					{
						el = this.sel.getRangeAt(0).startContainer;
					}

					return this.utils.isRedactorParent(el);
				},
				getParent: function(elem)
				{
					elem = elem || this.selection.getCurrent();
					if (elem)
					{
						return this.utils.isRedactorParent($(elem).parent()[0]);
					}

					return false;
				},
				getPrev: function()
				{
					return  window.getSelection().anchorNode.previousSibling;
				},
				getNext: function()
				{
					return window.getSelection().anchorNode.nextSibling;
				},
				getBlock: function(node)
				{
					node = node || this.selection.getCurrent();

					while (node)
					{
						if (this.utils.isBlockTag(node.tagName))
						{
							return ($(node).hasClass('redactor-editor')) ? false : node;
						}

						node = node.parentNode;
					}

					return false;
				},
				getInlines: function(nodes, tags)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return false;
					}

					var inlines = [];
					nodes = (typeof nodes == 'undefined' || nodes === false) ? this.selection.getNodes() : nodes;
					var inlineTags = this.opts.inlineTags;
					inlineTags.push('span');

					if (typeof tags !== 'undefined')
					{
						for (var i = 0; i < tags.length; i++)
						{
							inlineTags.push(tags[i]);
						}
					}

					$.each(nodes, $.proxy(function(i,node)
					{
						if ($.inArray(node.tagName.toLowerCase(), inlineTags) != -1)
						{
							inlines.push(node);
						}

					}, this));

					return (inlines.length === 0) ? false : inlines;
				},
				getInlinesTags: function(tags)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return false;
					}

					var inlines = [];
					var nodes =  this.selection.getNodes();
					$.each(nodes, $.proxy(function(i,node)
					{
						if ($.inArray(node.tagName.toLowerCase(), tags) != -1)
						{
							inlines.push(node);
						}

					}, this));

					return (inlines.length === 0) ? false : inlines;
				},
				getBlocks: function(nodes)
				{
					this.selection.get();

					if (this.range && this.range.collapsed)
					{
						return [this.selection.getBlock()];
					}

					var blocks = [];
					nodes = (typeof nodes == 'undefined') ? this.selection.getNodes() : nodes;
					$.each(nodes, $.proxy(function(i,node)
					{
						if (this.utils.isBlock(node))
						{
							this.selection.lastBlock = node;
							blocks.push(node);
						}

					}, this));

					return (blocks.length === 0) ? [this.selection.getBlock()] : blocks;
				},
				getLastBlock: function()
				{
					return this.selection.lastBlock;
				},
				getNodes: function()
				{
					this.selection.get();

					var startNode = this.selection.getNodesMarker(1);
					var endNode = this.selection.getNodesMarker(2);

					if (this.range.collapsed === false)
					{
					   if (window.getSelection) {
					        var sel = window.getSelection();
					        if (sel.rangeCount > 0) {

					            var range = sel.getRangeAt(0);
					            var startPointNode = range.startContainer, startOffset = range.startOffset;

					            var boundaryRange = range.cloneRange();
					            boundaryRange.collapse(false);
					            boundaryRange.insertNode(endNode);
					            boundaryRange.setStart(startPointNode, startOffset);
					            boundaryRange.collapse(true);
					            boundaryRange.insertNode(startNode);

					            // Reselect the original text
					            range.setStartAfter(startNode);
					            range.setEndBefore(endNode);
					            sel.removeAllRanges();
					            sel.addRange(range);
					        }
					    }
					}
					else
					{
						this.selection.setNodesMarker(this.range, startNode, true);
						endNode = startNode;
					}

					var nodes = [];
					var counter = 0;

					var self = this;
					this.$editor.find('*').each(function()
					{
						if (this == startNode)
						{
							var parent = $(this).parent();
							if (parent.length !== 0 && parent[0].tagName != 'BODY' && self.utils.isRedactorParent(parent[0]))
							{
								nodes.push(parent[0]);
							}

							nodes.push(this);
							counter = 1;
						}
						else
						{
							if (counter > 0)
							{
								nodes.push(this);
								counter = counter + 1;
							}
						}

						if (this == endNode)
						{
							return false;
						}

					});

					var finalNodes = [];
					var len = nodes.length;
					for (var i = 0; i < len; i++)
					{
						if (nodes[i].id != 'nodes-marker-1' && nodes[i].id != 'nodes-marker-2')
						{
							finalNodes.push(nodes[i]);
						}
					}

					this.selection.removeNodesMarkers();

					return finalNodes;

				},
				getNodesMarker: function(num)
				{
					return $('<span id="nodes-marker-' + num + '" class="redactor-nodes-marker" data-verified="redactor">' + this.opts.invisibleSpace + '</span>')[0];
				},
				setNodesMarker: function(range, node, type)
				{
					var range = range.cloneRange();

					try {
						range.collapse(type);
						range.insertNode(node);
					}
					catch (e) {}
				},
				removeNodesMarkers: function()
				{
					$(document).find('span.redactor-nodes-marker').remove();
					this.$editor.find('span.redactor-nodes-marker').remove();
				},
				fromPoint: function(start, end)
				{
					this.caret.setOffset(start, end);
				},
				wrap: function(tag)
				{
					this.selection.get();

					if (this.range.collapsed) return false;

					var wrapper = document.createElement(tag);
					wrapper.appendChild(this.range.extractContents());
					this.range.insertNode(wrapper);

					return wrapper;
				},
				selectElement: function(node)
				{
					if (this.utils.browser('mozilla'))
					{
						node = node[0] || node;

						var range = document.createRange();
						range.selectNodeContents(node);
					}
					else
					{
						this.caret.set(node, 0, node, 1);
					}
				},
				selectAll: function()
				{
					this.selection.get();
					this.range.selectNodeContents(this.$editor[0]);
					this.selection.addRange();
				},
				remove: function()
				{
					this.selection.get();
					this.sel.removeAllRanges();
				},
				save: function()
				{
					this.selection.createMarkers();
				},
				createMarkers: function()
				{
					this.selection.get();

					var node1 = this.selection.getMarker(1);

					this.selection.setMarker(this.range, node1, true);
					if (this.range.collapsed === false)
					{
						var node2 = this.selection.getMarker(2);
						this.selection.setMarker(this.range, node2, false);
					}

					this.savedSel = this.$editor.html();
				},
				getMarker: function(num)
				{
					if (typeof num == 'undefined') num = 1;

					return $('<span id="selection-marker-' + num + '" class="redactor-selection-marker"  data-verified="redactor">' + this.opts.invisibleSpace + '</span>')[0];
				},
				getMarkerAsHtml: function(num)
				{
					return this.utils.getOuterHtml(this.selection.getMarker(num));
				},
				setMarker: function(range, node, type)
				{
					range = range.cloneRange();

					try {
						range.collapse(type);
						range.insertNode(node);

					}
					catch (e)
					{
						this.focus.setStart();
					}

				},
				restore: function()
				{
					var node1 = this.$editor.find('span#selection-marker-1');
					var node2 = this.$editor.find('span#selection-marker-2');

					if (this.utils.browser('mozilla'))
					{
						this.$editor.focus();
					}

					if (node1.length !== 0 && node2.length !== 0)
					{
						this.caret.set(node1, 0, node2, 0);
					}
					else if (node1.length !== 0)
					{
						this.caret.set(node1, 0, node1, 0);
					}
					else
					{
						this.$editor.focus();
					}

					this.selection.removeMarkers();
					this.savedSel = false;

				},
				removeMarkers: function()
				{
					this.$editor.find('span.redactor-selection-marker').each(function(i,s)
					{
						var text = $(s).text().replace(/\u200B/g, '');
						if (text === '') $(s).remove();
						else $(s).replaceWith(function() { return $(this).contents(); });
					});
				},
				getText: function()
				{
					this.selection.get();

					return this.sel.toString();
				},
				getHtml: function()
				{
					var html = '';

					this.selection.get();
					if (this.sel.rangeCount)
					{
						var container = document.createElement('div');
						var len = this.sel.rangeCount;
						for (var i = 0; i < len; ++i)
						{
							container.appendChild(this.sel.getRangeAt(i).cloneContents());
						}

						html = container.innerHTML;
					}

					return this.clean.onSync(html);
				},
				replaceSelection: function(html)
				{
					this.selection.get();
					this.range.deleteContents();
					var div = document.createElement("div");
					div.innerHTML = html;
					var frag = document.createDocumentFragment(), child;
					while ((child = div.firstChild)) {
						frag.appendChild(child);
					}

					this.range.insertNode(frag);
				},
				replaceWithHtml: function(html)
				{
					html = this.selection.getMarkerAsHtml(1) + html + this.selection.getMarkerAsHtml(2);

					this.selection.get();

					if (window.getSelection && window.getSelection().getRangeAt)
					{
						this.selection.replaceSelection(html);
					}
					else if (document.selection && document.selection.createRange)
					{
						this.range.pasteHTML(html);
					}

					this.selection.restore();
					this.code.sync();
				}
			};
		},
		shortcuts: function()
		{
			return {
				init: function(e, key)
				{
					// disable browser's hot keys for bold and italic
					if (!this.opts.shortcuts)
					{
						if ((e.ctrlKey || e.metaKey) && (key === 66 || key === 73)) e.preventDefault();
						return false;
					}

					$.each(this.opts.shortcuts, $.proxy(function(str, command)
					{
						var keys = str.split(',');
						var len = keys.length;
						for (var i = 0; i < len; i++)
						{
							if (typeof keys[i] === 'string')
							{
								this.shortcuts.handler(e, $.trim(keys[i]), $.proxy(function()
								{
									var func;
									if (command.func.search(/\./) != '-1')
									{
										func = command.func.split('.');
										if (typeof this[func[0]] != 'undefined')
										{
											this[func[0]][func[1]].apply(this, command.params);
										}
									}
									else
									{
										this[command.func].apply(this, command.params);
									}

								}, this));
							}

						}

					}, this));
				},
				handler: function(e, keys, origHandler)
				{
					// based on https://github.com/jeresig/jquery.hotkeys
					var hotkeysSpecialKeys =
					{
						8: "backspace", 9: "tab", 10: "return", 13: "return", 16: "shift", 17: "ctrl", 18: "alt", 19: "pause",
						20: "capslock", 27: "esc", 32: "space", 33: "pageup", 34: "pagedown", 35: "end", 36: "home",
						37: "left", 38: "up", 39: "right", 40: "down", 45: "insert", 46: "del", 59: ";", 61: "=",
						96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7",
						104: "8", 105: "9", 106: "*", 107: "+", 109: "-", 110: ".", 111 : "/",
						112: "f1", 113: "f2", 114: "f3", 115: "f4", 116: "f5", 117: "f6", 118: "f7", 119: "f8",
						120: "f9", 121: "f10", 122: "f11", 123: "f12", 144: "numlock", 145: "scroll", 173: "-", 186: ";", 187: "=",
						188: ",", 189: "-", 190: ".", 191: "/", 192: "`", 219: "[", 220: "\\", 221: "]", 222: "'"
					};


					var hotkeysShiftNums =
					{
						"`": "~", "1": "!", "2": "@", "3": "#", "4": "$", "5": "%", "6": "^", "7": "&",
						"8": "*", "9": "(", "0": ")", "-": "_", "=": "+", ";": ": ", "'": "\"", ",": "<",
						".": ">",  "/": "?",  "\\": "|"
					};

					keys = keys.toLowerCase().split(" ");
					var special = hotkeysSpecialKeys[e.keyCode],
						character = String.fromCharCode( e.which ).toLowerCase(),
						modif = "", possible = {};

					$.each([ "alt", "ctrl", "meta", "shift"], function(index, specialKey)
					{
						if (e[specialKey + 'Key'] && special !== specialKey)
						{
							modif += specialKey + '+';
						}
					});


					if (special) possible[modif + special] = true;
					if (character)
					{
						possible[modif + character] = true;
						possible[modif + hotkeysShiftNums[character]] = true;

						// "$" can be triggered as "Shift+4" or "Shift+$" or just "$"
						if (modif === "shift+")
						{
							possible[hotkeysShiftNums[character]] = true;
						}
					}

					for (var i = 0, len = keys.length; i < len; i++)
					{
						if (possible[keys[i]])
						{
							e.preventDefault();
							return origHandler.apply(this, arguments);
						}
					}
				}
			};
		},
		tabifier: function()
		{
			return {
				get: function(code)
				{
					if (!this.opts.tabifier) return code;

					// clean setup
					var ownLine = ['area', 'body', 'head', 'hr', 'i?frame', 'link', 'meta', 'noscript', 'style', 'script', 'table', 'tbody', 'thead', 'tfoot'];
					var contOwnLine = ['li', 'dt', 'dt', 'h[1-6]', 'option', 'script'];
					var newLevel = ['p', 'blockquote', 'div', 'dl', 'fieldset', 'form', 'frameset', 'map', 'ol', 'pre', 'select', 'td', 'th', 'tr', 'ul'];

					this.tabifier.lineBefore = new RegExp('^<(/?' + ownLine.join('|/?' ) + '|' + contOwnLine.join('|') + ')[ >]');
					this.tabifier.lineAfter = new RegExp('^<(br|/?' + ownLine.join('|/?' ) + '|/' + contOwnLine.join('|/') + ')[ >]');
					this.tabifier.newLevel = new RegExp('^</?(' + newLevel.join('|' ) + ')[ >]');

					var i = 0,
					codeLength = code.length,
					point = 0,
					start = null,
					end = null,
					tag = '',
					out = '',
					cont = '';

					this.tabifier.cleanlevel = 0;

					for (; i < codeLength; i++)
					{
						point = i;

						// if no more tags, copy and exit
						if (-1 == code.substr(i).indexOf( '<' ))
						{
							out += code.substr(i);

							return this.tabifier.finish(out);
						}

						// copy verbatim until a tag
						while (point < codeLength && code.charAt(point) != '<')
						{
							point++;
						}

						if (i != point)
						{
							cont = code.substr(i, point - i);
							if (!cont.match(/^\s{2,}$/g))
							{
								if ('\n' == out.charAt(out.length - 1)) out += this.tabifier.getTabs();
								else if ('\n' == cont.charAt(0))
								{
									out += '\n' + this.tabifier.getTabs();
									cont = cont.replace(/^\s+/, '');
								}

								out += cont;
							}

							if (cont.match(/\n/)) out += '\n' + this.tabifier.getTabs();
						}

						start = point;

						// find the end of the tag
						while (point < codeLength && '>' != code.charAt(point))
						{
							point++;
						}

						tag = code.substr(start, point - start);
						i = point;

						var t;

						if ('!--' == tag.substr(1, 3))
						{
							if (!tag.match(/--$/))
							{
								while ('-->' != code.substr(point, 3))
								{
									point++;
								}
								point += 2;
								tag = code.substr(start, point - start);
								i = point;
							}

							if ('\n' != out.charAt(out.length - 1)) out += '\n';

							out += this.tabifier.getTabs();
							out += tag + '>\n';
						}
						else if ('!' == tag[1])
						{
							out = this.tabifier.placeTag(tag + '>', out);
						}
						else if ('?' == tag[1])
						{
							out += tag + '>\n';
						}
						else if (t = tag.match(/^<(script|style|pre)/i))
						{
							t[1] = t[1].toLowerCase();
							tag = this.tabifier.cleanTag(tag);
							out = this.tabifier.placeTag(tag, out);
							end = String(code.substr(i + 1)).toLowerCase().indexOf('</' + t[1]);

							if (end)
							{
								cont = code.substr(i + 1, end);
								i += end;
								out += cont;
							}
						}
						else
						{
							tag = this.tabifier.cleanTag(tag);
							out = this.tabifier.placeTag(tag, out);
						}
					}

					return this.tabifier.finish(out);
				},
				getTabs: function()
				{
					var s = '';
					for ( var j = 0; j < this.tabifier.cleanlevel; j++ )
					{
						s += '\t';
					}

					return s;
				},
				finish: function(code)
				{
					code = code.replace(/\n\s*\n/g, '\n');
					code = code.replace(/^[\s\n]*/, '');
					code = code.replace(/[\s\n]*$/, '');
					code = code.replace(/<script(.*?)>\n<\/script>/gi, '<script$1></script>');

					this.tabifier.cleanlevel = 0;

					return code;
				},
				cleanTag: function (tag)
				{
					var tagout = '';
					tag = tag.replace(/\n/g, ' ');
					tag = tag.replace(/\s{2,}/g, ' ');
					tag = tag.replace(/^\s+|\s+$/g, ' ');

					var suffix = '';
					if (tag.match(/\/$/))
					{
						suffix = '/';
						tag = tag.replace(/\/+$/, '');
					}

					var m;
					while (m = /\s*([^= ]+)(?:=((['"']).*?\3|[^ ]+))?/.exec(tag))
					{
						if (m[2]) tagout += m[1].toLowerCase() + '=' + m[2];
						else if (m[1]) tagout += m[1].toLowerCase();

						tagout += ' ';
						tag = tag.substr(m[0].length);
					}

					return tagout.replace(/\s*$/, '') + suffix + '>';
				},
				placeTag: function (tag, out)
				{
					var nl = tag.match(this.tabifier.newLevel);

					if (tag.match(this.tabifier.lineBefore) || nl)
					{
						out = out.replace(/\s*$/, '');
						out += '\n';
					}

					if (nl && '/' == tag.charAt(1)) this.tabifier.cleanlevel--;
					if ('\n' == out.charAt(out.length - 1)) out += this.tabifier.getTabs();
					if (nl && '/' != tag.charAt(1)) this.tabifier.cleanlevel++;

					out += tag;

					if (tag.match(this.tabifier.lineAfter) || tag.match(this.tabifier.newLevel))
					{
						out = out.replace(/ *$/, '');
						//out += '\n';
					}

					return out;
				}
			};
		},
		tidy: function()
		{
			return {
				setupAllowed: function()
				{
					var index = $.inArray('span', this.opts.removeEmpty);
					if (index !== -1)
					{
						this.opts.removeEmpty.splice(index, 1);
					}

					if (this.opts.allowedTags) this.opts.deniedTags = false;
					if (this.opts.allowedAttr) this.opts.removeAttr = false;

					if (this.opts.linebreaks) return;

					var tags = ['p', 'section'];
					if (this.opts.allowedTags) this.tidy.addToAllowed(tags);
					if (this.opts.deniedTags) this.tidy.removeFromDenied(tags);

				},
				addToAllowed: function(tags)
				{
					var len = tags.length;
					for (var i = 0; i < len; i++)
					{
						if ($.inArray(tags[i], this.opts.allowedTags) == -1)
						{
							this.opts.allowedTags.push(tags[i]);
						}
					}
				},
				removeFromDenied: function(tags)
				{
					var len = tags.length;
					for (var i = 0; i < len; i++)
					{
						var pos = $.inArray(tags[i], this.opts.deniedTags);
						if (pos != -1)
						{
							this.opts.deniedTags.splice(pos, 1);
						}
					}
				},
				load: function(html, options)
				{
					this.tidy.settings = {
						deniedTags: this.opts.deniedTags,
						allowedTags: this.opts.allowedTags,
						removeComments: this.opts.removeComments,
						replaceTags: this.opts.replaceTags,
						replaceStyles: this.opts.replaceStyles,
						removeDataAttr: this.opts.removeDataAttr,
						removeAttr: this.opts.removeAttr,
						allowedAttr: this.opts.allowedAttr,
						removeWithoutAttr: this.opts.removeWithoutAttr,
						removeEmpty: this.opts.removeEmpty
					};

					$.extend(this.tidy.settings, options);

					html = this.tidy.removeComments(html);

					// create container
					this.tidy.$div = $('<div />').append(html);

					// clean
					this.tidy.replaceTags();
					this.tidy.replaceStyles();
					this.tidy.removeTags();

					this.tidy.removeAttr();
					this.tidy.removeEmpty();
					this.tidy.removeParagraphsInLists();
					this.tidy.removeDataAttr();
					this.tidy.removeWithoutAttr();

					html = this.tidy.$div.html();
					this.tidy.$div.remove();

					return html;
				},
				removeComments: function(html)
				{
					if (!this.tidy.settings.removeComments) return html;

					return html.replace(/<!--[\s\S]*?-->/gi, '');
				},
				replaceTags: function(html)
				{
					if (!this.tidy.settings.replaceTags) return html;

					var len = this.tidy.settings.replaceTags.length;
					var replacement = [], rTags = [];
					for (var i = 0; i < len; i++)
					{
						rTags.push(this.tidy.settings.replaceTags[i][1]);
						replacement.push(this.tidy.settings.replaceTags[i][0]);
					}

					$.each(replacement, $.proxy(function(key, value)
					{
						this.tidy.$div.find(value).replaceWith(function()
						{
							return $("<" + rTags[key] + " />", {html: $(this).html()});
						});
					}, this));
				},
				replaceStyles: function()
				{
					if (!this.tidy.settings.replaceStyles) return;

					var len = this.tidy.settings.replaceStyles.length;
					this.tidy.$div.find('span').each($.proxy(function(n,s)
					{
						var $el = $(s);
						var style = $el.attr('style');
						for (var i = 0; i < len; i++)
						{
							if (style && style.match(new RegExp('^' + this.tidy.settings.replaceStyles[i][0], 'i')))
							{
								var tagName = this.tidy.settings.replaceStyles[i][1];
								$el.replaceWith(function()
								{
									var tag = document.createElement(tagName);
									return $(tag).append($(this).contents());
								});
							}
						}

					}, this));

				},
				removeTags: function()
				{
					if (!this.tidy.settings.deniedTags && this.tidy.settings.allowedTags)
					{
						this.tidy.$div.find('*').not(this.tidy.settings.allowedTags.join(',')).each(function(i, s)
						{
							if (s.innerHTML === '') $(s).remove();
							else $(s).contents().unwrap();
						});
					}

					if (this.tidy.settings.deniedTags)
					{
						this.tidy.$div.find(this.tidy.settings.deniedTags.join(',')).each(function(i, s)
						{
							if ($(s).hasClass('redactor-script-tag') || $(s).hasClass('redactor-selection-marker')) return;

							if (s.innerHTML === '') $(s).remove();
							else $(s).contents().unwrap();
						});
					}
				},
				removeAttr: function()
				{
					var len;
					if (!this.tidy.settings.removeAttr && this.tidy.settings.allowedAttr)
					{

						var allowedAttrTags = [], allowedAttrData = [];
						len = this.tidy.settings.allowedAttr.length;
						for (var i = 0; i < len; i++)
						{
							allowedAttrTags.push(this.tidy.settings.allowedAttr[i][0]);
							allowedAttrData.push(this.tidy.settings.allowedAttr[i][1]);
						}


						this.tidy.$div.find('*').each($.proxy(function(n,s)
						{
							var $el = $(s);
							var pos = $.inArray($el[0].tagName.toLowerCase(), allowedAttrTags);
							var attributesRemove = this.tidy.removeAttrGetRemoves(pos, allowedAttrData, $el);

							if (attributesRemove)
							{
								$.each(attributesRemove, function(z,f) {
									$el.removeAttr(f);
								});
							}
						}, this));
					}

					if (this.tidy.settings.removeAttr)
					{
						len = this.tidy.settings.removeAttr.length;
						for (var i = 0; i < len; i++)
						{
							var attrs = this.tidy.settings.removeAttr[i][1];
							if ($.isArray(attrs)) attrs = attrs.join(' ');

							this.tidy.$div.find(this.tidy.settings.removeAttr[i][0]).removeAttr(attrs);
						}
					}

				},
				removeAttrGetRemoves: function(pos, allowed, $el)
				{
					var attributesRemove = [];

					// remove all attrs
					if (pos == -1)
					{
						$.each($el[0].attributes, function(i, item)
						{
							attributesRemove.push(item.name);
						});

					}
					// allow all attrs
					else if (allowed[pos] == '*')
					{
						attributesRemove = [];
					}
					// allow specific attrs
					else
					{
						$.each($el[0].attributes, function(i, item)
						{
							if ($.isArray(allowed[pos]))
							{
								if ($.inArray(item.name, allowed[pos]) == -1)
								{
									attributesRemove.push(item.name);
								}
							}
							else if (allowed[pos] != item.name)
							{
								attributesRemove.push(item.name);
							}

						});
					}

					return attributesRemove;
				},
				removeAttrs: function (el, regex)
				{
					regex = new RegExp(regex, "g");
					return el.each(function()
					{
						var self = $(this);
						var len = this.attributes.length - 1;
						for (var i = len; i >= 0; i--)
						{
							var item = this.attributes[i];
							if (item && item.specified && item.name.search(regex)>=0)
							{
								self.removeAttr(item.name);
							}
						}
					});
				},
				removeEmpty: function()
				{
					if (!this.tidy.settings.removeEmpty) return;

					this.tidy.$div.find(this.tidy.settings.removeEmpty.join(',')).each(function()
					{
						var $el = $(this);
						var text = $el.text();
						text = text.replace(/\u200B/g, '');
						text = text.replace(/&nbsp;/gi, '');
						text = text.replace(/\s/g, '');

		    	    	if (text === '' && $el.children().length === 0)
		    	    	{
			    	    	$el.remove();
		    	    	}
					});
				},
				removeParagraphsInLists: function()
				{
					this.tidy.$div.find('li p').contents().unwrap();
				},
				removeDataAttr: function()
				{
					if (!this.tidy.settings.removeDataAttr) return;

					var tags = this.tidy.settings.removeDataAttr;
					if ($.isArray(this.tidy.settings.removeDataAttr)) tags = this.tidy.settings.removeDataAttr.join(',');

					this.tidy.removeAttrs(this.tidy.$div.find(tags), '^(data-)');

				},
				removeWithoutAttr: function()
				{
					if (!this.tidy.settings.removeWithoutAttr) return;

					this.tidy.$div.find(this.tidy.settings.removeWithoutAttr.join(',')).each(function()
					{
						if (this.attributes.length === 0)
						{
							$(this).contents().unwrap();
						}
					});
				}
			};
		},
		toolbar: function()
		{
			return {
				init: function()
				{
					return {
						html:
						{
							title: this.lang.get('html'),
							func: 'code.toggle'
						},
						formatting:
						{
							title: this.lang.get('formatting'),
							dropdown:
							{
								p:
								{
									title: this.lang.get('paragraph'),
									func: 'block.format'
								},
								blockquote:
								{
									title: this.lang.get('quote'),
									func: 'block.format'
								},
								pre:
								{
									title: this.lang.get('code'),
									func: 'block.format'
								},
								h1:
								{
									title: this.lang.get('header1'),
									func: 'block.format'
								},
								h2:
								{
									title: this.lang.get('header2'),
									func: 'block.format'
								},
								h3:
								{
									title: this.lang.get('header3'),
									func: 'block.format'
								},
								h4:
								{
									title: this.lang.get('header4'),
									func: 'block.format'
								},
								h5:
								{
									title: this.lang.get('header5'),
									func: 'block.format'
								}
							}
						},
						bold:
						{
							title: this.lang.get('bold'),
							func: 'inline.format'
						},
						italic:
						{
							title: this.lang.get('italic'),
							func: 'inline.format'
						},
						deleted:
						{
							title: this.lang.get('deleted'),
							func: 'inline.format'
						},
						underline:
						{
							title: this.lang.get('underline'),
							func: 'inline.format'
						},
						unorderedlist:
						{
							title: '&bull; ' + this.lang.get('unorderedlist'),
							func: 'list.toggle'
						},
						orderedlist:
						{
							title: '1. ' + this.lang.get('orderedlist'),
							func: 'list.toggle'
						},
						outdent:
						{
							title: '< ' + this.lang.get('outdent'),
							func: 'indent.decrease'
						},
						indent:
						{
							title: '> ' + this.lang.get('indent'),
							func: 'indent.increase'
						},
						image:
						{
							title: this.lang.get('image'),
							func: 'image.show'
						},
						file:
						{
							title: this.lang.get('file'),
							func: 'file.show'
						},
						link:
						{
							title: this.lang.get('link'),
							dropdown:
							{
								link:
								{
									title: this.lang.get('link_insert'),
									func: 'link.show',
									observe: {
										element: 'a',
										in: {
											title: this.lang.get('link_edit'),
										},
										out: {
											title: this.lang.get('link_insert')
										}
									}
								},
								unlink:
								{
									title: this.lang.get('unlink'),
									func: 'link.unlink',
									observe: {
										element: 'a',
										out: {
											attr: {
												'class': 'redactor-dropdown-link-inactive',
												'aria-disabled': true
											}
										}
									}
								}
							}
						},
						alignment:
						{
							title: this.lang.get('alignment'),
							dropdown:
							{
								left:
								{
									title: this.lang.get('align_left'),
									func: 'alignment.left'
								},
								center:
								{
									title: this.lang.get('align_center'),
									func: 'alignment.center'
								},
								right:
								{
									title: this.lang.get('align_right'),
									func: 'alignment.right'
								},
								justify:
								{
									title: this.lang.get('align_justify'),
									func: 'alignment.justify'
								}
							}
						},
						horizontalrule:
						{
							title: this.lang.get('horizontalrule'),
							func: 'line.insert'
						}
					};
				},
				build: function()
				{
					this.toolbar.hideButtons();
					this.toolbar.hideButtonsOnMobile();
					this.toolbar.isButtonSourceNeeded();

					if (this.opts.buttons.length === 0) return;

					this.$toolbar = this.toolbar.createContainer();

					this.toolbar.setOverflow();
					this.toolbar.append();
					this.toolbar.setFormattingTags();
					this.toolbar.loadButtons();
					this.toolbar.setFixed();

					// buttons response
					if (this.opts.activeButtons)
					{
						this.$editor.on('mouseup.redactor keyup.redactor focus.redactor', $.proxy(this.observe.toolbar, this));
					}

				},
				createContainer: function()
				{
					return $('<ul>').addClass('redactor-toolbar').attr({'id': 'redactor-toolbar-' + this.uuid, 'role': 'toolbar'});
				},
				setFormattingTags: function()
				{
					$.each(this.opts.toolbar.formatting.dropdown, $.proxy(function (i, s)
					{
						if ($.inArray(i, this.opts.formatting) == -1) delete this.opts.toolbar.formatting.dropdown[i];
					}, this));

				},
				loadButtons: function()
				{
					$.each(this.opts.buttons, $.proxy(function(i, btnName)
					{
						if (!this.opts.toolbar[btnName]) return;

						if (btnName === 'file')
						{
							 if (this.opts.fileUpload === false) return;
							 else if (!this.opts.fileUpload && this.opts.s3 === false) return;
						}

						if (btnName === 'image')
						{
							 if (this.opts.imageUpload === false) return;
							 else if (!this.opts.imageUpload && this.opts.s3 === false) return;
						}

						var btnObject = this.opts.toolbar[btnName];
						this.$toolbar.append($('<li>').append(this.button.build(btnName, btnObject)));

					}, this));
				},
				append: function()
				{
					if (this.opts.toolbarExternal)
					{
						this.$toolbar.addClass('redactor-toolbar-external');
						$(this.opts.toolbarExternal).html(this.$toolbar);
					}
					else
					{
						this.$box.prepend(this.$toolbar);
					}
				},
				setFixed: function()
				{
					if (!this.utils.isDesktop()) return;
					if (this.opts.toolbarExternal) return;
					if (!this.opts.toolbarFixed) return;

					this.toolbar.observeScroll();
					$(this.opts.toolbarFixedTarget).on('scroll.redactor.' + this.uuid, $.proxy(this.toolbar.observeScroll, this));

				},
				setOverflow: function()
				{
					if (this.utils.isMobile() && this.opts.toolbarOverflow)
					{
						this.$toolbar.addClass('redactor-toolbar-overflow');
					}
				},
				isButtonSourceNeeded: function()
				{
					if (this.opts.source) return;

					var index = this.opts.buttons.indexOf('html');
					if (index !== -1)
					{
						this.opts.buttons.splice(index, 1);
					}
				},
				hideButtons: function()
				{
					if (this.opts.buttonsHide.length === 0) return;

					$.each(this.opts.buttonsHide, $.proxy(function(i, s)
					{
						var index = this.opts.buttons.indexOf(s);
						this.opts.buttons.splice(index, 1);

					}, this));
				},
				hideButtonsOnMobile: function()
				{
					if (!this.utils.isMobile() || this.opts.buttonsHideOnMobile.length === 0) return;

					$.each(this.opts.buttonsHideOnMobile, $.proxy(function(i, s)
					{
						var index = this.opts.buttons.indexOf(s);
						this.opts.buttons.splice(index, 1);

					}, this));
				},
				observeScroll: function()
				{
					var scrollTop = $(this.opts.toolbarFixedTarget).scrollTop();
					var boxTop = 1;

					if (this.opts.toolbarFixedTarget === document)
					{
						boxTop = this.$box.offset().top;
					}

					if ((scrollTop + this.opts.toolbarFixedTopOffset) > boxTop)
					{
						this.toolbar.observeScrollEnable(scrollTop, boxTop);
					}
					else
					{
						this.toolbar.observeScrollDisable();
					}
				},
				observeScrollEnable: function(scrollTop, boxTop)
				{
					var top = this.opts.toolbarFixedTopOffset + scrollTop - boxTop;
					var left = 0;
					var end = boxTop + this.$box.height() - 32;
					var width = this.$box.innerWidth();

					this.$toolbar.addClass('toolbar-fixed-box');
					this.$toolbar.css({
						position: 'absolute',
						width: width,
						top: top + 'px',
						left: left
					});

					if (scrollTop > end)
						$('.redactor-dropdown-' + this.uuid + ':visible').hide();

					this.toolbar.setDropdownsFixed();
					this.$toolbar.css('visibility', (scrollTop < end) ? 'visible' : 'hidden');
				},
				observeScrollDisable: function()
				{
					this.$toolbar.css({
						position: 'relative',
						width: 'auto',
						top: 0,
						left: 0,
						visibility: 'visible'
					});

					this.toolbar.unsetDropdownsFixed();
					this.$toolbar.removeClass('toolbar-fixed-box');
				},
				setDropdownsFixed: function()
				{
					var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset;
					var position = 'fixed';
					if (this.opts.toolbarFixedTarget !== document)
					{
						top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top) + this.opts.toolbarFixedTopOffset;
						position = 'absolute';
					}

					$('.redactor-dropdown-' + this.uuid).each(function()
					{
						$(this).css({ position: position, top: top + 'px' });
					});
				},
				unsetDropdownsFixed: function()
				{
					var top = (this.$toolbar.innerHeight() + this.$toolbar.offset().top);
					$('.redactor-dropdown-' + this.uuid).each(function()
					{
						$(this).css({ position: 'absolute', top: top + 'px' });
					});
				}
			};
		},
		upload: function()
		{
			return {
				init: function(id, url, callback)
				{
					this.upload.direct = false;
					this.upload.callback = callback;
					this.upload.url = url;
					this.upload.$el = $(id);
					this.upload.$droparea = $('<div id="redactor-droparea" />');

					this.upload.$placeholdler = $('<div id="redactor-droparea-placeholder" />').text(this.lang.get('upload_label'));
					this.upload.$input = $('<input class="form-control" type="file" name="file" />');

					this.upload.$placeholdler.append(this.upload.$input);
					this.upload.$droparea.append(this.upload.$placeholdler);
					this.upload.$el.append(this.upload.$droparea);

					this.upload.$droparea.off('redactor.upload');
					this.upload.$input.off('redactor.upload');

					this.upload.$droparea.on('dragover.redactor.upload', $.proxy(this.upload.onDrag, this));
					this.upload.$droparea.on('dragleave.redactor.upload', $.proxy(this.upload.onDragLeave, this));

					// change
					this.upload.$input.on('change.redactor.upload', $.proxy(function(e)
					{
						e = e.originalEvent || e;
						this.upload.traverseFile(this.upload.$input[0].files[0], e);
					}, this));

					// drop
					this.upload.$droparea.on('drop.redactor.upload', $.proxy(function(e)
					{
						e.preventDefault();

						this.upload.$droparea.removeClass('drag-hover').addClass('drag-drop');
						this.upload.onDrop(e);

					}, this));
				},
				directUpload: function(file, e)
				{
					this.upload.direct = true;
					this.upload.traverseFile(file, e);
				},
				onDrop: function(e)
				{
					e = e.originalEvent || e;
					var files = e.dataTransfer.files;

					this.upload.traverseFile(files[0], e);
				},
				traverseFile: function(file, e)
				{
					if (this.opts.s3)
					{
						this.upload.setConfig(file);
						this.upload.s3uploadFile(file);
						return;
					}

					var formData = !!window.FormData ? new FormData() : null;
					if (window.FormData)
					{
						this.upload.setConfig(file);

						var name = (this.upload.type == 'image') ? this.opts.imageUploadParam : this.opts.fileUploadParam;
						formData.append(name, file);
					}

					this.progress.show();
					this.core.setCallback('uploadStart', e, formData);
					this.upload.sendData(formData, e);
				},
				setConfig: function(file)
				{
					this.upload.getType(file);

					if (this.upload.direct)
					{
						this.upload.url = (this.upload.type == 'image') ? this.opts.imageUpload : this.opts.fileUpload;
						this.upload.callback = (this.upload.type == 'image') ? this.image.insert : this.file.insert;
					}
				},
				getType: function(file)
				{
					this.upload.type = 'image';
					if (this.opts.imageTypes.indexOf(file.type) == -1)
					{
						this.upload.type = 'file';
					}
				},
				getHiddenFields: function(obj, fd)
				{
					if (obj === false || typeof obj !== 'object') return fd;

					$.each(obj, $.proxy(function(k, v)
					{
						if (v !== null && v.toString().indexOf('#') === 0) v = $(v).val();
						fd.append(k, v);

					}, this));

					return fd;

				},
				sendData: function(formData, e)
				{
					// append hidden fields
					if (this.upload.type == 'image')
					{
						formData = this.upload.getHiddenFields(this.opts.uploadImageFields, formData);
						formData = this.upload.getHiddenFields(this.upload.imageFields, formData);
					}
					else
					{
						formData = this.upload.getHiddenFields(this.opts.uploadFileFields, formData);
						formData = this.upload.getHiddenFields(this.upload.fileFields, formData);
					}

					var xhr = new XMLHttpRequest();
					xhr.open('POST', this.upload.url);
					xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

					// complete
					xhr.onreadystatechange = $.proxy(function()
					{
					    if (xhr.readyState == 4)
					    {
					        var data = xhr.responseText;

							data = data.replace(/^\[/, '');
							data = data.replace(/\]$/, '');

							var json;
							try
							{
								json = (typeof data === 'string' ? $.parseJSON(data) : data);
							}
							catch(err)
							{
								json = {
									error: true
								};
							}


							this.progress.hide();

							if (!this.upload.direct)
							{
								this.upload.$droparea.removeClass('drag-drop');
							}

							this.upload.callback(json, this.upload.direct, e);
					    }
					}, this);


					/*
					xhr.upload.onprogress = $.proxy(function(e)
					{
						if (e.lengthComputable)
						{
							var complete = (e.loaded / e.total * 100 | 0);
							//progress.value = progress.innerHTML = complete;
						}

					}, this);
					*/


					xhr.send(formData);
				},
				onDrag: function(e)
				{
					e.preventDefault();
					this.upload.$droparea.addClass('drag-hover');
				},
				onDragLeave: function(e)
				{
					e.preventDefault();
					this.upload.$droparea.removeClass('drag-hover');
				},
				clearImageFields: function()
				{
					this.upload.imageFields = {};
				},
				addImageFields: function(name, value)
				{
					this.upload.imageFields[name] = value;
				},
				removeImageFields: function(name)
				{
					delete this.upload.imageFields[name];
				},
				clearFileFields: function()
				{
					this.upload.fileFields = {};
				},
				addFileFields: function(name, value)
				{
					this.upload.fileFields[name] = value;
				},
				removeFileFields: function(name)
				{
					delete this.upload.fileFields[name];
				},


				// S3
				s3uploadFile: function(file)
				{
					this.upload.s3executeOnSignedUrl(file, $.proxy(function(signedURL)
					{
						this.upload.s3uploadToS3(file, signedURL);
					}, this));
				},
				s3executeOnSignedUrl: function(file, callback)
				{
					var xhr = new XMLHttpRequest();
					var mark = (this.opts.s3.search(/\?/) !== '-1') ? '?' : '&';

					xhr.open('GET', this.opts.s3 + mark + 'name=' + file.name + '&type=' + file.type, true);

					// Hack to pass bytes through unprocessed.
					if (xhr.overrideMimeType) xhr.overrideMimeType('text/plain; charset=x-user-defined');

					var that = this;
					xhr.onreadystatechange = function(e)
					{
						if (this.readyState == 4 && this.status == 200)
						{
							that.progress.show();
							callback(decodeURIComponent(this.responseText));
						}
						else if (this.readyState == 4 && this.status != 200)
						{
							//setProgress(0, 'Could not contact signing script. Status = ' + this.status);
						}
					};

					xhr.send();
				},
				s3createCORSRequest: function(method, url)
				{
					var xhr = new XMLHttpRequest();
					if ("withCredentials" in xhr)
					{
						xhr.open(method, url, true);
					}
					else if (typeof XDomainRequest != "undefined")
					{
						xhr = new XDomainRequest();
						xhr.open(method, url);
					}
					else
					{
						xhr = null;
					}

					return xhr;
				},
				s3uploadToS3: function(file, url)
				{
					var xhr = this.upload.s3createCORSRequest('PUT', url);
					if (!xhr)
					{
						//setProgress(0, 'CORS not supported');
					}
					else
					{
						xhr.onload = $.proxy(function()
						{
							if (xhr.status == 200)
							{
								//setProgress(100, 'Upload completed.');

								this.progress.hide();

								var s3file = url.split('?');

								if (!s3file[0])
								{
									 // url parsing is fail
									 return false;
								}


								if (!this.upload.direct)
								{
									this.upload.$droparea.removeClass('drag-drop');
								}

								var json = { filelink: s3file[0] };
								if (this.upload.type == 'file')
								{
									var arr = s3file[0].split('/');
									json.filename = arr[arr.length-1];
								}

								this.upload.callback(json, this.upload.direct, false);


							}
							else
							{
								//setProgress(0, 'Upload error: ' + xhr.status);
							}
						}, this);

						xhr.onerror = function() {};

						xhr.upload.onprogress = function(e) {};

						xhr.setRequestHeader('Content-Type', file.type);
						xhr.setRequestHeader('x-amz-acl', 'public-read');

						xhr.send(file);
					}
				}
			};
		},
		utils: function()
		{
			return {
				isMobile: function()
				{
					return /(iPhone|iPod|BlackBerry|Android)/.test(navigator.userAgent);
				},
				isDesktop: function()
				{
					return !/(iPhone|iPod|iPad|BlackBerry|Android)/.test(navigator.userAgent);
				},
				isString: function(obj)
				{
					return Object.prototype.toString.call(obj) == '[object String]';
				},
				isEmpty: function(html, removeEmptyTags)
				{
					html = html.replace(/[\u200B-\u200D\uFEFF]/g, '');
					html = html.replace(/&nbsp;/gi, '');
					html = html.replace(/<\/?br\s?\/?>/g, '');
					html = html.replace(/\s/g, '');
					html = html.replace(/^<p>[^\W\w\D\d]*?<\/p>$/i, '');
					html = html.replace(/<iframe(.*?[^>])>$/i, 'iframe');
					html = html.replace(/<source(.*?[^>])>$/i, 'source');

					// remove empty tags
					if (removeEmptyTags !== false)
					{
						html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, '');
						html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, '');
					}

					html = $.trim(html);

					return html === '';
				},
				normalize: function(str)
				{
					if (typeof(str) === 'undefined') return 0;
					return parseInt(str.replace('px',''), 10);
				},
				hexToRgb: function(hex)
				{
					if (typeof hex == 'undefined') return;
					if (hex.search(/^#/) == -1) return hex;

					var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
					hex = hex.replace(shorthandRegex, function(m, r, g, b)
					{
						return r + r + g + g + b + b;
					});

					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
					return 'rgb(' + parseInt(result[1], 16) + ', ' + parseInt(result[2], 16) + ', ' + parseInt(result[3], 16) + ')';
				},
				getOuterHtml: function(el)
				{
					return $('<div>').append($(el).eq(0).clone()).html();
				},
				getAlignmentElement: function(el)
				{
					if ($.inArray(el.tagName, this.opts.alignmentTags) !== -1)
					{
						return $(el);
					}
					else
					{
						return $(el).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
					}
				},
				removeEmptyAttr: function(el, attr)
				{
					var $el = $(el);
					if (typeof $el.attr(attr) == 'undefined')
					{
						return true;
					}

					if ($el.attr(attr) === '')
					{
						$el.removeAttr(attr);
						return true;
					}

					return false;
				},
				removeEmpty: function(i, s)
				{
					var $s = $($.parseHTML(s));

					$s.find('.redactor-invisible-space').removeAttr('style').removeAttr('class');

					if ($s.find('hr, br, img, iframe, source').length !== 0) return;
					var text = $.trim($s.text());

					if (this.utils.isEmpty(text, false))
					{
						$s.remove();
					}
				},

				// save and restore scroll
				saveScroll: function()
				{
					this.saveEditorScroll = this.$editor.scrollTop();
					this.saveBodyScroll = $(window).scrollTop();

					if (this.opts.scrollTarget) this.saveTargetScroll = $(this.opts.scrollTarget).scrollTop();
				},
				restoreScroll: function()
				{
					if (typeof this.saveScroll === 'undefined' && typeof this.saveBodyScroll === 'undefined') return;

					$(window).scrollTop(this.saveBodyScroll);
					this.$editor.scrollTop(this.saveEditorScroll);

					if (this.opts.scrollTarget) $(this.opts.scrollTarget).scrollTop(this.saveTargetScroll);
				},

				// get invisible space element
				createSpaceElement: function()
				{
					var space = document.createElement('span');
					space.className = 'redactor-invisible-space';
					space.innerHTML = this.opts.invisibleSpace;

					return space;
				},

				// replace
				removeInlineTags: function(node)
				{
					var tags = this.opts.inlineTags;
					tags.push('span');

					if (node.tagName == 'PRE') tags.push('a');

					$(node).find(tags.join(',')).not('span.redactor-selection-marker').contents().unwrap();
				},
				replaceWithContents: function(node, removeInlineTags)
				{
					var self = this;
					$(node).replaceWith(function()
					{
						if (removeInlineTags === true) self.utils.removeInlineTags(this);

						return $(this).contents();
					});

					return $(node);
				},
				replaceToTag: function(node, tag, removeInlineTags)
				{
					var replacement;
					var self = this;
					$(node).replaceWith(function()
					{
						replacement = $('<' + tag + ' />').append($(this).contents());

						for (var i = 0; i < this.attributes.length; i++)
						{
							replacement.attr(this.attributes[i].name, this.attributes[i].value);
						}

						if (removeInlineTags === true) self.utils.removeInlineTags(replacement);

						return replacement;
					});

					return replacement;
				},

				// start and end of element
				isStartOfElement: function()
				{
					var block = this.selection.getBlock();
					if (!block) return false;

					var offset = this.caret.getOffsetOfElement(block);

					return (offset === 0) ? true : false;
				},
				isEndOfElement: function(element)
				{
					if (typeof element == 'undefined')
					{
						var element = this.selection.getBlock();
						if (!element) return false;
					}

					var offset = this.caret.getOffsetOfElement(element);
					var text = $.trim($(element).text()).replace(/\n\r\n/g, '');

					return (offset == text.length) ? true : false;
				},
				isStartOfEditor: function()
				{
					var offset = this.caret.getOffsetOfElement(this.$editor[0]);

					return (offset === 0) ? true : false;
				},
				isEndOfEditor: function()
				{
					var block = this.$editor[0];

					var offset = this.caret.getOffsetOfElement(block);
					var text = $.trim($(block).html().replace(/(<([^>]+)>)/gi,''));

					return (offset == text.length) ? true : false;
				},

				// test blocks
				isBlock: function(block)
				{
					block = block[0] || block;

					return block && this.utils.isBlockTag(block.tagName);
				},
				isBlockTag: function(tag)
				{
					if (typeof tag == 'undefined') return false;

					return this.reIsBlock.test(tag);
				},

				// tag detection
				isTag: function(current, tag)
				{
					var element = $(current).closest(tag, this.$editor[0]);
					if (element.length == 1)
					{
						return element[0];
					}

					return false;
				},

				// select all
				isSelectAll: function()
				{
					return this.selectAll;
				},
				enableSelectAll: function()
				{
					this.selectAll = true;
				},
				disableSelectAll: function()
				{
					this.selectAll = false;
				},

				// parents detection
				isRedactorParent: function(el)
				{
					if (!el)
					{
						return false;
					}

					if ($(el).parents('.redactor-editor').length === 0 || $(el).hasClass('redactor-editor'))
					{
						return false;
					}

					return el;
				},
				isCurrentOrParentHeader: function()
				{
					return this.utils.isCurrentOrParent(['H1', 'H2', 'H3', 'H4', 'H5', 'H6']);
				},
				isCurrentOrParent: function(tagName)
				{
					var parent = this.selection.getParent();
					var current = this.selection.getCurrent();

					if ($.isArray(tagName))
					{
						var matched = 0;
						$.each(tagName, $.proxy(function(i, s)
						{
							if (this.utils.isCurrentOrParentOne(current, parent, s))
							{
								matched++;
							}
						}, this));

						return (matched === 0) ? false : true;
					}
					else
					{
						return this.utils.isCurrentOrParentOne(current, parent, tagName);
					}
				},
				isCurrentOrParentOne: function(current, parent, tagName)
				{
					tagName = tagName.toUpperCase();

					return parent && parent.tagName === tagName ? parent : current && current.tagName === tagName ? current : false;
				},


				// browsers detection
				isOldIe: function()
				{
					return (this.utils.browser('msie') && parseInt(this.utils.browser('version'), 10) < 9) ? true : false;
				},
				isLessIe10: function()
				{
					return (this.utils.browser('msie') && parseInt(this.utils.browser('version'), 10) < 10) ? true : false;
				},
				isIe11: function()
				{
					return !!navigator.userAgent.match(/Trident\/7\./);
				},
				browser: function(browser)
				{
					var ua = navigator.userAgent.toLowerCase();
					var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
		            /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		            /(webkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
		            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		            /(msie) ([\w.]+)/.exec( ua ) ||
		            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
		            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		            [];

					if (browser == 'safari') return (typeof match[3] != 'undefined') ? match[3] == 'safari' : false;
					if (browser == 'version') return match[2];
					if (browser == 'webkit') return (match[1] == 'chrome' || match[1] == 'opr' || match[1] == 'webkit');
					if (match[1] == 'rv') return browser == 'msie';
					if (match[1] == 'opr') return browser == 'webkit';

					return browser == match[1];
				},
				strpos: function(haystack, needle, offset)
				{
					var i = haystack.indexOf(needle, offset);
					return i >= 0 ? i : false;
				},
				disableBodyScroll: function()
				{

					var $body = $('html');
					var windowWidth = window.innerWidth;
					if (!windowWidth)
					{
						var documentElementRect = document.documentElement.getBoundingClientRect();
						windowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
					}

					var isOverflowing = document.body.clientWidth < windowWidth;
					var scrollbarWidth = this.utils.measureScrollbar();

					$body.css('overflow', 'hidden');
					if (isOverflowing) $body.css('padding-right', scrollbarWidth);


				},
				measureScrollbar: function()
				{
					var $body = $('body');
					var scrollDiv = document.createElement('div');
					scrollDiv.className = 'redactor-scrollbar-measure';

					$body.append(scrollDiv);
					var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
					$body[0].removeChild(scrollDiv);
					return scrollbarWidth;
				},
				enableBodyScroll: function()
				{
					$('html').css({ 'overflow': '', 'padding-right': '' });
					$('body').remove('redactor-scrollbar-measure');
				}
			};
		}
	};

	$(window).on('load.tools.redactor', function()
	{
		$('[data-tools="redactor"]').redactor();
	});

	// constructor
	Redactor.prototype.init.prototype = Redactor.prototype;

})(jQuery);
/*$.Redactor.prototype.insertButton = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertButton', 'Insert button');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertButton', ' fa-hand-pointer-o');
    }

    ////////////////////////////////////

    controller.select = function() {

    }

    /////////////////////////////////////////////////////////

    controller.insert = function(images) {
        var self = this;

        //Restore the selection
        self.selection.restore();

        //Only proceed if we actually selected some images
        if (images.length) {
            var str = '';
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
            })

            //Insert
            self.insert.html(str);
            self.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};




link: function()
		{
			return {
				show: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault) e.preventDefault();

					if (!this.observe.isCurrent('a'))
					{
						this.modal.load('link', this.lang.get('link_insert'), 600);
					}
					else
					{
						this.modal.load('link', this.lang.get('link_edit'), 600);
					}

					this.modal.createCancelButton();

					var buttonText = !this.observe.isCurrent('a') ? this.lang.get('insert') : this.lang.get('edit');

					this.link.buttonInsert = this.modal.createActionButton(buttonText);

					this.selection.get();

					this.link.getData();
					this.link.cleanUrl();

					if (this.link.target == '_blank') $('#redactor-link-blank').prop('checked', true);

					this.link.$inputUrl = $('#redactor-link-url');
					this.link.$inputText = $('#redactor-link-url-text');

					this.link.$inputText.val(this.link.text);
					this.link.$inputUrl.val(this.link.url);

					this.link.buttonInsert.on('click', $.proxy(this.link.insert, this));

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					// show modal
					this.selection.save();
					this.modal.show();
					this.link.$inputUrl.focus();
				},
				cleanUrl: function()
				{
					var thref = self.location.href.replace(/\/$/i, '');

					if (typeof this.link.url !== "undefined")
					{
						this.link.url = this.link.url.replace(thref, '');
						this.link.url = this.link.url.replace(/^\/#/, '#');
						this.link.url = this.link.url.replace('mailto:', '');

						// remove host from href
						if (!this.opts.linkProtocol)
						{
							var re = new RegExp('^(http|ftp|https)://' + self.location.host, 'i');
							this.link.url = this.link.url.replace(re, '');
						}
					}
				},
				getData: function()
				{
					this.link.$node = false;

					var $el = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
					if ($el.length !== 0 && $el[0].tagName === 'A')
					{
						this.link.$node = $el;

						this.link.url = $el.attr('href');
						this.link.text = $el.text();
						this.link.target = $el.attr('target');
					}
					else
					{
						this.link.text = this.sel.toString();
						this.link.url = '';
						this.link.target = '';
					}

				},
				insert: function()
				{
					this.placeholder.remove();

					var target = '';
					var link = this.link.$inputUrl.val();
					var text = this.link.$inputText.val().replace(/(<([^>]+)>)/ig,"");

					if ($.trim(link) === '')
					{
						this.link.$inputUrl.addClass('redactor-input-error').on('keyup', function()
						{
							$(this).removeClass('redactor-input-error');
							$(this).off('keyup');

						});

						return;
					}

					// mailto
					if (link.search('@') != -1 && /(http|ftp|https):\/\//i.test(link) === false)
					{
						link = 'mailto:' + link;
					}
					// url, not anchor
					else if (link.search('#') !== 0)
					{
						if ($('#redactor-link-blank').prop('checked'))
						{
							target = '_blank';
						}

						// test url (add protocol)
						var pattern = '((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}';
						var re = new RegExp('^(http|ftp|https)://' + pattern, 'i');
						var re2 = new RegExp('^' + pattern, 'i');
						var re3 = new RegExp('\.(html|php)$', 'i');
						if (link.search(re) == -1 && link.search(re3) == -1 && link.search(re2) === 0 && this.opts.linkProtocol)
						{
							link = this.opts.linkProtocol + '://' + link;
						}
					}

					this.link.set(text, link, target);
					this.modal.close();
				},
				set: function(text, link, target)
				{
					text = $.trim(text.replace(/<|>/g, ''));

					this.selection.restore();
					var blocks = this.selection.getBlocks();

					if (text === '' && link === '') return;
					if (text === '' && link !== '') text = link;

					if (this.link.$node)
					{
						this.buffer.set();

						var $link = this.link.$node,
							$el   = $link.children();

						if ($el.length > 0)
						{
							while ($el.length)
							{
								$el = $el.children();
							}

							$el = $el.end();
						}
						else
						{
							$el = $link;
						}

						$link.attr('href', link);
						$el.text(text);

						if (target !== '')
						{
							$link.attr('target', target);
						}
						else
						{
							$link.removeAttr('target');
						}

						this.selection.selectElement($link);

						this.code.sync();
					}
					else
					{
						if (this.utils.browser('mozilla') && this.link.text === '')
						{
							var $a = $('<a />').attr('href', link).text(text);
							if (target !== '') $a.attr('target', target);

							$a = $(this.insert.node($a));

							if (this.opts.linebreaks)
							{
								$a.after('&nbsp;');
							}

							this.selection.selectElement($a);
						}
						else
						{
							var $a;
							if (this.utils.browser('msie'))
							{
								$a = $('<a href="' + link + '">').text(text);
								if (target !== '') $a.attr('target', target);

								$a = $(this.insert.node($a));

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								this.selection.selectElement($a);
							}
							else
							{
								document.execCommand('createLink', false, link);

								$a = $(this.selection.getCurrent()).closest('a', this.$editor[0]);
								if (this.utils.browser('mozilla'))
								{
									$a = $('a[_moz_dirty=""]');
								}

								if (target !== '') $a.attr('target', target);
								$a.removeAttr('style').removeAttr('_moz_dirty');

								if (this.selection.getText().match(/\s$/))
								{
									$a.after(" ");
								}

								if (this.link.text !== '' || this.link.text != text)
								{
									if (!this.opts.linebreaks && blocks && blocks.length <= 1)
									{
										$a.text(text);
									}
									else if (this.opts.linebreaks)
									{
										$a.text(text);
									}

									this.selection.selectElement($a);
								}
							}
						}

						this.code.sync();
						this.core.setCallback('insertedLink', $a);

					}

					// link tooltip
					setTimeout($.proxy(function()
					{
						this.observe.links();

					}, this), 5);
				},
				unlink: function(e)
				{
					if (typeof e != 'undefined' && e.preventDefault)
					{
						e.preventDefault();
					}

					var nodes = this.selection.getNodes();
					if (!nodes) return;

					this.buffer.set();

					var len = nodes.length;
					var links = [];
					for (var i = 0; i < len; i++)
					{
						if (nodes[i].tagName === 'A')
						{
							links.push(nodes[i]);
						}

						var $node = $(nodes[i]).closest('a', this.$editor[0]);
						$node.replaceWith($node.contents());
					}

					this.core.setCallback('deletedLink', links);

					// hide link's tooltip
					$('.redactor-link-tooltip').remove();

					this.code.sync();

				},
				toggleClass: function(className)
				{
					this.link.setClass(className, 'toggleClass');
				},
				addClass: function(className)
				{
					this.link.setClass(className, 'addClass');
				},
				removeClass: function(className)
				{
					this.link.setClass(className, 'removeClass');
				},
				setClass: function(className, func)
				{
					var links = this.selection.getInlinesTags(['a']);
					if (links === false) return;

					$.each(links, function()
					{
						$(this)[func](className);
					});
				}
			};
		},


		*/
$.Redactor.prototype.insertImage = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertImage', 'Insert image');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertImage', 'fa-image');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();
        this.buffer.set();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $scope = angular.element(this.$element).scope();

        /////////////////////////////////////////////////////////

        var modalInstance = $scope.modal.open({
            template: '<content-browser ng-model="images" ng-done="submit" ng-type="' + "'image'" + '"></content-browser>',
            controller: function($modalInstance, $rootScope, $scope) {
                $scope.images = [];
                $scope.submit = function() {

                    //Map each image to id and url             
                    var mapped = _.map($scope.images, function(img) {
                        return {
                            _id: img._id,
                            url: $rootScope.asset.imageUrl(img._id)
                        }
                    });

                    //Insert all the images into the text area
                    controller.insert(mapped);

                    //Close the modal
                    $modalInstance.close($scope.images);
                }
            }
        });
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(images) {
        var self = this;

        //Restore the selection
        self.selection.restore();

        //Only proceed if we actually selected some images
        if (images.length) {
            var str = '';
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
            })

            //Insert
            self.insert.html(str);
            self.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};
$.Redactor.prototype.video = function() {
    return {
        reUrlYoutube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig,
        reUrlVimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
        getTemplate: function() {
            return String() + '<section id="redactor-modal-video-insert">' + '<label>' + this.lang.get('video_html_code') + '</label>' + '<textarea class="form-control" id="redactor-insert-video-area" style="height: 160px;"></textarea>' + '</section>';
        },
        init: function() {
            var button = this.button.addAfter('image', 'video', 'Embed Video');
            this.button.addCallback(button, this.video.show);
        },
        show: function() {
            this.modal.addTemplate('video', this.video.getTemplate());

            this.modal.load('video', this.lang.get('video'), 700);
            this.modal.createCancelButton();

            var button = this.modal.createActionButton(this.lang.get('insert'));
            button.on('click', this.video.insert);

            this.selection.save();
            this.modal.show();

            $('#redactor-insert-video-area').focus();

        },
        insert: function() {
            var data = $('#redactor-insert-video-area').val();

            if (!data.match(/<iframe|<video/gi)) {
                data = this.clean.stripTags(data);

                // parse if it is link on youtube & vimeo
               /* var iframeStart = '<iframe style="width: 500px; height: 281px;" src="',
                    iframeEnd = '" frameborder="0" allowfullscreen></iframe>';
*/

                   var iframeStart = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="';
                   var iframeEnd ='" frameborder="0" allowfullscreen></iframe></div>';



                if (data.match(this.video.reUrlYoutube)) {
                    data = data.replace(this.video.reUrlYoutube, iframeStart + '//www.youtube.com/embed/$1' + iframeEnd);
                } else if (data.match(this.video.reUrlVimeo)) {
                    data = data.replace(this.video.reUrlVimeo, iframeStart + '//player.vimeo.com/video/$2' + iframeEnd);
                }
            }

            this.selection.restore();
            this.modal.close();

            var current = this.selection.getBlock() || this.selection.getCurrent();

            if (current) $(current).after(data);
            else {
                this.insert.html(data);
            }

            this.code.sync();
        }
    };
}




/*$.Redactor.prototype.insertVideo = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertVideo', 'Insert video');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertVideo', 'fa-video');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();
        this.buffer.set();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $scope = angular.element(this.$element).scope();

        /////////////////////////////////////////////////////////

        var modalInstance = $scope.modal.open({
            template: '<content-browser ng-model="videos" ng-done="submit" ng-type="' + "'video'" + '"></content-browser>',
            controller: function($modalInstance, $rootScope, $scope) {
                $scope.videos = [];
                $scope.submit = function() {

                    //Map each video to id and url             
                    var mapped = _.map($scope.videos, function(img) {
                        return {
                            _id: img._id,
                            url: $rootScope.asset.videoUrl(img._id)
                        }
                    });

                    //Insert all the videos into the text area
                    controller.insert(mapped);

                    //Close the modal
                    $modalInstance.close($scope.videos);
                }
            }
        });
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(videos) {
        var self = this;

        //Restore the selection
        self.selection.restore();

        //Only proceed if we actually selected some videos
        if (videos.length) {
            var str = '';
            _.each(videos, function(video) {
                str += '<fluro-video src="' + video.url + '" ng-src="{{$root.asset.videoUrl(' + "'" + video._id + "'" + ')}}"/>';
            })

            //Insert
            self.insert.html(str);
            self.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};
*/
$.Redactor.prototype.table = function() {
    return {
        getTemplate: function() {
            return String() + '<section id="redactor-modal-table-insert">' + '<label>' + this.lang.get('rows') + '</label>' + '<input class="form-control" type="text" size="5" value="2" id="redactor-table-rows" />' + '<label>' + this.lang.get('columns') + '</label>' + '<input class="form-control" type="text" size="5" value="3" id="redactor-table-columns" />' + '</section>';
        },
        init: function() {
            var dropdown = {};

            dropdown.insert_table = {
                title: this.lang.get('insert_table'),
                func: this.table.show,
                observe: {
                    element: 'table',
                    in : {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.insert_row_above = {
                title: this.lang.get('insert_row_above'),
                func: this.table.addRowAbove,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.insert_row_below = {
                title: this.lang.get('insert_row_below'),
                func: this.table.addRowBelow,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.insert_column_left = {
                title: this.lang.get('insert_column_left'),
                func: this.table.addColumnLeft,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.insert_column_right = {
                title: this.lang.get('insert_column_right'),
                func: this.table.addColumnRight,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.add_head = {
                title: this.lang.get('add_head'),
                func: this.table.addHead,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.delete_head = {
                title: this.lang.get('delete_head'),
                func: this.table.deleteHead,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.delete_column = {
                title: this.lang.get('delete_column'),
                func: this.table.deleteColumn,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.delete_row = {
                title: this.lang.get('delete_row'),
                func: this.table.deleteRow,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            dropdown.delete_table = {
                title: this.lang.get('delete_table'),
                func: this.table.deleteTable,
                observe: {
                    element: 'table',
                    out: {
                        attr: {
                            'class': 'redactor-dropdown-link-inactive',
                            'aria-disabled': true,
                        }
                    }
                }
            };

            this.observe.addButton('td', 'table');
            this.observe.addButton('th', 'table');

            var button = this.button.addBefore('link', 'table', this.lang.get('table'));
            this.button.addDropdown(button, dropdown);
        },
        show: function() {
            this.modal.addTemplate('table', this.table.getTemplate());

            this.modal.load('table', this.lang.get('insert_table'), 300);
            this.modal.createCancelButton();

            var button = this.modal.createActionButton(this.lang.get('insert'));
            button.on('click', this.table.insert);

            this.selection.save();
            this.modal.show();

            $('#redactor-table-rows').focus();

        },
        insert: function() {
            this.placeholder.remove();

            var rows = $('#redactor-table-rows').val(),
                columns = $('#redactor-table-columns').val(),
                $tableBox = $('<div>'),
                tableId = Math.floor(Math.random() * 99999),
                $table = $('<table id="table' + tableId + '" class="table table-striped table-bordered"><tbody></tbody></table>'),
                i, $row, z, $column;

            for (i = 0; i < rows; i++) {
                $row = $('<tr>');

                for (z = 0; z < columns; z++) {
                    $column = $('<td>' + this.opts.invisibleSpace + '</td>');

                    // set the focus to the first td
                    if (i === 0 && z === 0) {
                        $column.append(this.selection.getMarker());
                    }

                    $($row).append($column);
                }

                $table.append($row);
            }

            $tableBox.append($table);
            var html = $tableBox.html();

            this.modal.close();
            this.selection.restore();

            if (this.table.getTable()) return;

            this.buffer.set();

            var current = this.selection.getBlock() || this.selection.getCurrent();
            if (current && current.tagName != 'BODY') {
                if (current.tagName == 'LI') current = $(current).closest('ul, ol');
                $(current).after(html);
            } else {
                this.insert.html(html, false);
            }

            this.selection.restore();

            var table = this.$editor.find('#table' + tableId);

            var p = table.prev("p");

            if (p.length > 0 && this.utils.isEmpty(p.html())) {
                p.remove();
            }

            if (!this.opts.linebreaks && (this.utils.browser('mozilla') || this.utils.browser('msie'))) {
                var $next = table.next();
                if ($next.length === 0) {
                    table.after(this.opts.emptyHtml);
                }
            }

            this.observe.buttons();

            table.find('span.redactor-selection-marker').remove();
            table.removeAttr('id');

            this.code.sync();
            this.core.setCallback('insertedTable', table);
        },
        getTable: function() {
            var $table = $(this.selection.getParent()).closest('table');

            if (!this.utils.isRedactorParent($table)) return false;
            if ($table.size() === 0) return false;

            return $table;
        },
        restoreAfterDelete: function($table) {
            this.selection.restore();
            $table.find('span.redactor-selection-marker').remove();
            this.code.sync();
        },
        deleteTable: function() {
            var $table = this.table.getTable();
            if (!$table) return;

            this.buffer.set();


            var $next = $table.next();
            if (!this.opts.linebreaks && $next.length !== 0) {
                this.caret.setStart($next);
            } else {
                this.caret.setAfter($table);
            }


            $table.remove();

            this.code.sync();
        },
        deleteRow: function() {
            var $table = this.table.getTable();
            if (!$table) return;

            var $current = $(this.selection.getCurrent());

            this.buffer.set();

            var $current_tr = $current.closest('tr');
            var $focus_tr = $current_tr.prev().length ? $current_tr.prev() : $current_tr.next();
            if ($focus_tr.length) {
                var $focus_td = $focus_tr.children('td, th').first();
                if ($focus_td.length) $focus_td.prepend(this.selection.getMarker());
            }

            $current_tr.remove();
            this.table.restoreAfterDelete($table);
        },
        deleteColumn: function() {
            var $table = this.table.getTable();
            if (!$table) return;

            this.buffer.set();

            var $current = $(this.selection.getCurrent());
            var $current_td = $current.closest('td, th');
            var index = $current_td[0].cellIndex;

            $table.find('tr').each($.proxy(function(i, elem) {
                var $elem = $(elem);
                var focusIndex = index - 1 < 0 ? index + 1 : index - 1;
                if (i === 0) $elem.find('td, th').eq(focusIndex).prepend(this.selection.getMarker());

                $elem.find('td, th').eq(index).remove();

            }, this));

            this.table.restoreAfterDelete($table);
        },
        addHead: function() {
            var $table = this.table.getTable();
            if (!$table) return;

            this.buffer.set();

            if ($table.find('thead').size() !== 0) {
                this.table.deleteHead();
                return;
            }

            var tr = $table.find('tr').first().clone();
            tr.find('td').replaceWith($.proxy(function() {
                return $('<th>').html(this.opts.invisibleSpace);
            }, this));

            $thead = $('<thead></thead>').append(tr);
            $table.prepend($thead);

            this.code.sync();

        },
        deleteHead: function() {
            var $table = this.table.getTable();
            if (!$table) return;

            var $thead = $table.find('thead');
            if ($thead.size() === 0) return;

            this.buffer.set();

            $thead.remove();
            this.code.sync();
        },
        addRowAbove: function() {
            this.table.addRow('before');
        },
        addRowBelow: function() {
            this.table.addRow('after');
        },
        addColumnLeft: function() {
            this.table.addColumn('before');
        },
        addColumnRight: function() {
            this.table.addColumn('after');
        },
        addRow: function(type) {
            var $table = this.table.getTable();
            if (!$table) return;

            this.buffer.set();

            var $current = $(this.selection.getCurrent());
            var $current_tr = $current.closest('tr');
            var new_tr = $current_tr.clone();

            new_tr.find('th').replaceWith(function() {
                var $td = $('<td>');
                $td[0].attributes = this.attributes;

                return $td.append($(this).contents());
            });

            new_tr.find('td').html(this.opts.invisibleSpace);

            if (type == 'after') {
                $current_tr.after(new_tr);
            } else {
                $current_tr.before(new_tr);
            }

            this.code.sync();
        },
        addColumn: function(type) {
            var $table = this.table.getTable();
            if (!$table) return;

            var index = 0;
            var current = $(this.selection.getCurrent());

            this.buffer.set();

            var $current_tr = current.closest('tr');
            var $current_td = current.closest('td, th');

            $current_tr.find('td, th').each($.proxy(function(i, elem) {
                if ($(elem)[0] === $current_td[0]) index = i;

            }, this));

            $table.find('tr').each($.proxy(function(i, elem) {
                var $current = $(elem).find('td, th').eq(index);

                var td = $current.clone();
                td.html(this.opts.invisibleSpace);

                if (type == 'after') {
                    $current.after(td);
                } else {
                    $current.before(td);
                }

            }, this));

            this.code.sync();
        }
    };
};
$.Redactor.prototype.undoAction = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('undoAction', 'Undo');
        this.button.addCallback(button, this.buffer.undo);
        this.button.setAwesome('undoAction', 'fa-undo');
    }

    /////////////////////////////////////////////////////////

    return controller;
};


/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

$.Redactor.prototype.redoAction = function() {


    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('redoAction', 'Redo');
        this.button.addCallback(button, this.buffer.redo);
        this.button.setAwesome('redoAction', 'fa-repeat');
    }

    /////////////////////////////////////////////////////////

    return controller;
};
app.directive('route', function($compile, $parse, $rootScope, FluroContentRetrieval) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            route: '=ngModel',
        },
        template: '<div></div>',
        controller: function($scope, $controller) {

            //Disable Caching for webbuilder
            var noCache = true;

            //////////////////////////////////////////

            // var controller = this;

            ///////////////////////////////////////////////////////

            $scope.$watch('route.testSlug', function(slug) {


                // var appendPosts = $scope.route.appendPosts;
                var appendPostCount = $scope.route.appendPostCount;
                var appendPosts = $scope.route.appendPosts;
                var appendContactDetail = $scope.route.appendContactDetail;
                var appendProcess = $scope.route.appendProcess;
                var appendForms = $scope.route.appendForms;
                var appendTeams = $scope.route.appendTeams;
                var appendAssignments = $scope.route.appendAssignments;
                var includePublicSearchResults = $scope.route.includePublic;

                ////////////////////////////////////////////////////

                //May as well make this standard
                if(!appendPostCount || !appendPostCount.length) {
                    appendPostCount = 'all';
                }

                if(!appendProcess || !appendProcess.length) {
                    appendProcess = 'all';
                }

                if(!appendForms || !appendForms.length) {
                    appendForms = 'all';
                }

                // console.log('APPEND FORMS', appendForms)

                ////////////////////////////////////////////////////

                if (slug) {
                    FluroContentRetrieval.get([slug._id], noCache, {
                        searchInheritable:true,
                        includePublic:includePublicSearchResults,
                        appendPostCount:appendPostCount,
                        appendPosts:appendPosts,
                        appendProcess:appendProcess,
                        appendContactDetail:appendContactDetail,
                        appendForms:appendForms,
                        appendAssignments:appendAssignments,
                        appendTeams:appendTeams,
                    }).then(function(res) {
                        if (res && res.length) {
                            // console.log('Got route slug!!', res[0]);
                            $scope.slug = res[0];
                        }
                    });
                } else {
                    delete $scope.slug;
                }
            })


            //////////////////////////////////////////

            $scope.$watch('route.content', function(nodes) {

                //console.log('POPULATE', nodes);
                if (nodes && nodes.length) {
                    FluroContentRetrieval.populate(nodes, noCache, {
                        searchInheritable: true
                    }).then(function(res) {
                        $scope.items = res;
                    });
                } else {
                    $scope.items = [];
                }

            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('route.keys', function(keyGroups) {

                /////////////////////////////////

                var nids = _.chain(keyGroups)
                    .map(function(group) {
                        return group.content;
                    })
                    .flatten()
                    .compact()
                    .value();

                /////////////////////////////////

                if (nids && nids.length) {
                    FluroContentRetrieval.populate(nids, noCache, {
                        searchInheritable: true
                    }).then(function(res) {

                        $scope.content = _.reduce(keyGroups, function(result, keyGroup) {

                            result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                                if (nid._id) {
                                    nid = nid._id;
                                }
                                return _.find(res, {
                                    _id: nid
                                });;
                            })

                            return result;

                        }, {})
                    });
                } else {
                    $scope.content = {};
                }


            }, true)


            ///////////////////////////////////////////////////////

            //Add Parameters to the scope
            if($scope.route.params) {
                $scope.params = $scope.route.params;
            } else {
                $scope.params = {};
            }
            

            ///////////////////////////////////////////////////////

            $scope.$watch('route.collections', function(nodes) {

                //Reset
                $scope.collections = [];
                $scope.collectionResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var collectionIds = _.chain(nodes)
                        .map(function(collection) {
                            if (collection._id) {
                                return collection._id;
                            } else {
                                return collection;
                            }
                        })
                        .compact()
                        .value();


                    //Get the collections
                    FluroContentRetrieval.getMultipleCollections(collectionIds, true)
                        .then(function(res) {


                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            //Add the collections to scope
                            $scope.collections = res;

                            //Add all of the items to the collection results
                            $scope.collectionResults = _.chain(res)
                                .map(function(collection) {
                                    return collection.items;
                                })
                                .flatten()
                                .uniq()
                                .value();
                        });
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('route.queries + route.queryVariables + slug', function() {

                var nodes = $scope.route.queries;

                $scope.results = [];
                $scope.queryResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var queryIds = _.chain(nodes)
                        .map(function(query) {
                            if (query._id) {
                                return query._id;
                            } else {
                                return query;
                            }
                        })
                        .compact()
                        .value();

                    ///////////////////////////////////////////////////

                    //Check if we need to go further
                    if (queryIds && queryIds.length) {

                        var variables;

                        if ($scope.route.queryVariables) {

                            //Parse the variables against the current $scope
                            variables = _.mapValues($scope.route.queryVariables, function(value) {
                                var val = $parse(value)($scope);
                                return val;
                            })

                            //Only send through variables that exist
                            variables = _.pick(variables, _.identity);
                        }

                        //Now query the ids
                        FluroContentRetrieval.queryMultiple(queryIds, true, variables).then(function(res) {

                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            $scope.queryResults = [];

                            ///////////////////////////////////////////////////

                            //Loop through each query and pull out results
                            $scope.results = _.reduce(queryIds, function(result, queryId) {

                                var array = res[queryId];

                                $scope.queryResults.push(array);

                                if (array && array.length) {

                                    //Add each item that matches
                                    _.each(array, function(item) {
                                        result.push(item);
                                    })
                                }

                                return result;

                            }, []);

                            ///////////////////////////////////////////////////


                        });
                    }
                }
            }, true);

            ///////////////////////////////////////////////////////////

            this.page = $scope;

            ///////////////////////////////////////////////////////////

            //Bootstrap our custom controller
            if ($scope.route.controllerName) {
                $controller($scope.route.controllerName, {
                    $scope: $scope
                });
            }

            


        },
        link: function($scope, $element, $attr, $routeController) {

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            $scope.$watch('route.html', function(html) {
                $element.empty();
                if (html && html.length) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                } else {
                    //List the sections
                    var template = $compile('<route-section ng-model="section" ng-route="route" ng-repeat="section in route.sections track by $index" ng-if="!section.disabled"></route-section>')($scope);
                    $element.append(template);
                }
            });
        },
    };
});
///////////////////////////////////////////////////////

app.directive('routeSection', function($compile, $rootScope, $templateCache, $timeout, $parse, FluroContentRetrieval) {
    return {
        restrict: 'E',
        replace: true,
        require: '^route',
        scope: {
            model: '=ngModel',
            route: '=ngRoute',
        },
        template: '<section></section>',
        link: function($scope, $element, $attr, $ctrl) {

            //Disable Caching for webbuilder
            var noCache = true;

            //Add the controller to the scope
            $scope.page = $ctrl.page;

            ///////////////////////////////////////////////////////

            //Inherit the slug from the route
            $scope.$watch(function() {
                return $scope.page.slug;
            }, function(slug) {
                $scope.slug = slug;
            });


            ///////////////////////////////////////////////////////

            $scope.$watch('model.temp.highlight', function(highlight) {
                if (highlight) {
                    $element.addClass('_highlight');
                } else {
                    $element.removeClass('_highlight');
                }
            })

            // ///////////////////////////////////////////////////////

            // $scope.$watch('route.testSlug', function(slug) {


            //     // var appendPosts = $scope.route.appendPosts;
            //     var appendPostCount = $scope.route.appendPostCount;
            //     var appendContactDetail = $scope.route.appendContactDetail;
            //     var appendProcess = $scope.route.appendProcess;

            //     ////////////////////////////////////////////////////

            //     if(!appendPostCount || !appendPostCount.length) {
            //         appendPostCount = 'all';
            //     }

            //     if(!appendProcess || !appendProcess.length) {
            //         appendProcess = 'all';
            //     }

            //     ////////////////////////////////////////////////////

            //     if (slug) {
            //         FluroContentRetrieval.get([slug._id], noCache, {
            //             searchInheritable:true,
            //             appendPostCount:appendPostCount,
            //             // appendPosts:appendPosts,
            //             appendProcess:appendProcess,
            //             appendContactDetail:appendContactDetail,
            //         }).then(function(res) {
            //             if (res && res.length) {
            //                 console.log('Got slug!!');
            //                 $scope.slug = res[0];
            //             }
            //         });
            //     } else {
            //         delete $scope.slug;
            //     }
            // })

            ///////////////////////////////////////////////////////

            $scope.$watch('model.content', function(nodes) {

                if ($scope.model.select && $scope.model.select.length) {
                    // console.log('Partial Populate');
                    //console.log('POPULATE', nodes);
                    if (nodes && nodes.length) {
                        FluroContentRetrieval.populatePartial(nodes, $scope.model.select, noCache, true).then(function(res) {
                            $scope.items = res;
                        });
                    } else {
                        delete $scope.items;
                    }
                } else {
                    //console.log('POPULATE', nodes);
                    if (nodes && nodes.length) {
                        FluroContentRetrieval.populate(nodes, noCache, {
                            searchInheritable: true
                        }).then(function(res) {
                            $scope.items = res;
                        });
                    } else {
                        delete $scope.items;
                    }
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('model.keys', function(keyGroups) {

                /////////////////////////////////

                var nids = _.chain(keyGroups)
                    .map(function(group) {
                        return group.content;
                    })
                    .flatten()
                    .compact()
                    .value();

                /////////////////////////////////

                if (nids && nids.length) {
                    FluroContentRetrieval.populate(nids, noCache, {
                        searchInheritable: true
                    }).then(function(res) {

                        $scope.content = _.reduce(keyGroups, function(result, keyGroup) {

                            result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                                if (nid._id) {
                                    nid = nid._id;
                                }
                                return _.find(res, {
                                    _id: nid
                                });;
                            })

                            return result;

                        }, {})
                    });
                } else {
                    delete $scope.content;
                }


            }, true)


            ///////////////////////////////////////////////////////

            $scope.$watch('model.params', function(params) {
                //Add Parameters to the scope
                $scope.params = params;
            });

            ///////////////////////////////////////////////////////

            $scope.$watch('model.collections', function(nodes) {

                //Reset
                $scope.collections = [];
                $scope.collectionResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var collectionIds = _.chain(nodes)
                        .map(function(collection) {
                            if (collection._id) {
                                return collection._id;
                            } else {
                                return collection;
                            }
                        })
                        .compact()
                        .value();


                    //Get the collections
                    FluroContentRetrieval.getMultipleCollections(collectionIds, true).then(function(res) {


                        //If nothing then stop here
                        if (!res) {
                            return;
                        }

                        ///////////////////////////////////////////////////

                        //Add the collections to scope
                        $scope.collections = res;

                        //Add all of the items to the collection results
                        $scope.collectionResults = _.chain(res)
                            .map(function(collection) {
                                return collection.items;
                            })
                            .flatten()
                            .uniq()
                            .value();
                    });
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('model.queries + model.queryVariables + slug', function() {

                var nodes = $scope.model.queries;

                $scope.results = [];
                $scope.queryResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var queryIds = _.chain(nodes)
                        .map(function(query) {
                            if (query._id) {
                                return query._id;
                            } else {
                                return query;
                            }
                        })
                        .compact()
                        .value();

                    ///////////////////////////////////////////////////

                    //Check if we need to go further
                    if (queryIds && queryIds.length) {

                        var variables;

                        if ($scope.model.queryVariables) {

                            //Parse the variables against the current $scope
                            variables = _.mapValues($scope.model.queryVariables, function(value) {
                                var val = $parse(value)($scope);
                                return val;
                            })

                            //Only send through variables that exist
                            variables = _.pick(variables, _.identity);
                        }

                        //Now query the ids
                        FluroContentRetrieval.queryMultiple(queryIds, true, variables).then(function(res) {

                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            $scope.queryResults = [];

                            ///////////////////////////////////////////////////

                            //Loop through each query and pull out results
                            $scope.results = _.reduce(queryIds, function(result, queryId) {

                                var array = res[queryId];

                                $scope.queryResults.push(array);

                                if (array && array.length) {

                                    //Add each item that matches
                                    _.each(array, function(item) {
                                        result.push(item);
                                    })
                                }

                                return result;

                            }, []);

                            ///////////////////////////////////////////////////


                        });
                    }
                }
            }, true)


            var existingTimeout;

            ///////////////////////////////////////////////////////

            $scope.$watch(function() {

                var html;

                if ($scope.model) {
                    if ($scope.model.template) {
                        html = $templateCache.get($scope.model.template);
                    } else {
                        html = $scope.model.html;
                    }
                }

                return html;

            }, function() {

                var html = $scope.model.html;

                if ($scope.model.template) {
                    html = $templateCache.get($scope.model.template);
                }
                $element.empty();
                if (html) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                    //$element.replaceWith(template);




                    /**
                    if(existingTimeout) {
                        $timeout.cancel(existingTimeout);
                    }



                    //Rasterise to canvas
                    existingTimeout = $timeout(function() {


                        var originalWidth = $element.outerWidth();
                        var originalHeight = $element.outerHeight();



                        var canvasWidth = 100;
                        var canvasHeight = Math.floor((originalHeight / originalWidth) * 100);
                        //canvas.width = canvas.height * (canvas.clientWidth / canvas.clientHeight);

                        console.log(originalWidth + 'x' + originalHeight, canvasWidth + 'x' + canvasHeight);

                        html2canvas($element, {
                            useCORS: true,
                            // width: canvasWidth,
                            // height: canvasHeight,
                            // allowTaint:true,
                            // logging:true,
                            onrendered: function(canvas) {


                                // canvas.width = canvas.height * (canvas.clientWidth / canvas.clientHeight);

                                // var extra_canvas = document.createElement("canvas");
                                // // extra_canvas.setAttribute('width',70);
                                // // extra_canvas.setAttribute('height',70);
                                // var ctx = extra_canvas.getContext('2d');
                                // ctx.drawImage(canvas,0,0, canvas.width, canvas.height, 0,0,70,70);
                                var dataURL = canvas.toDataURL();


                                // console.log('GOT CANVAS', canvas)

                                $scope.model.thumbnail = dataURL; //canvas.toDataURL();



                                // canvas is the final rendered <canvas> element
                            }
                        });
                    }, 1000);

                    /**/

                }
            })


        },
    };
})
app.service('BuildSelectionService', function() {

    var controller = {};

    //Now we have nested routes we need to flatten
    controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return controller.getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    //Now we have nested routes we need to flatten
    controller.getFlattenedFolders = function(array) {
        return _.chain(array)
            .reduce(function(result, route, key) {

                if (route.type == 'folder') {
                    result.push(route);
                }

                if (route.routes && route.routes.length) {
                    //Recursive search
                    var folders = controller.getFlattenedFolders(route.routes);

                    if (folders.length) {
                        result.push(folders)
                    }
                }

                return result;

            }, [])
            .flatten()
            .compact()
            .value();

    }

    return controller;


});


//////////////////////////////////////////

app.service('BuildService', function(ObjectSelection) {

    var controller = {};

    //////////////////////////////////////////

    controller.selection = new ObjectSelection();;
    controller.selection.multiple = false;


    //////////////////////////////////////////

    return controller;
})

//////////////////////////////////////////

app.controller('BuildController', function($scope, $state, FluroSocket, $timeout, BuildService, $interval, $rootScope, $modal, snippets, $rootScope, item, BuildSelectionService, $state, Fluro, Notifications, FluroContent, $window, $compile) {






    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////


    //Listen to the content.edit event
    FluroSocket.on('content.edit', socketUpdate);

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', socketUpdate);
    });

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    var socketEnabled = true;

    function socketUpdate(socketEvent) {

        if (socketEnabled) {
            //Get the item that was updated
            var socketItemID = socketEvent.item;

            if (!socketItemID) {
                return;
            }

            //////////////////////////////////////////////

            //Simplify the id
            if (socketItemID._id) {
                socketItemID = socketItemID._id;
            }

            //////////////////////////////////////////////

            //Same id as the current site that we are working on
            var sameId = (socketItemID == item._id);

            //////////////////////////////////////////////

            //Check if the update is newer than the current item we are working on
            var newVersion = (socketEvent.data.updated != item.updated);

            //Make sure we only update if we weren't the originating socket
            var differentSocket = (socketEvent.ignoreSocket != FluroSocket.getSocketID());

            //////////////////////////////////////////////

            console.log('SOCKET', sameId, newVersion, differentSocket, FluroSocket.getSocketID(), socketEvent.ignoreSocket);

            //If the same id, and its a new version and a different socket that cause the request
            if (sameId && newVersion && differentSocket) {

                //Then apply
                $timeout(function() {
                    //Assign the new content
                    socketEnabled = false;

                    // console.log('APPLY PATCH', socketEvent)
                    //Patch the differences
                    jsondiffpatch.patch(item, socketEvent.data.diff);

                    //Renable the socket
                    socketEnabled = true;

                    // //console.log($rootScope.user._id, data.user._id)
                    if (socketEvent.user) {
                        if ($rootScope.user._id != socketEvent.user._id) {
                            Notifications.warning('This content was just updated by ' + socketEvent.user.name);
                        } else {
                            // if(socketEvent.user.name) {
                            //     Notifications.status('This content was updated by '+ $rootScope.user.name +' in another window');
                            // } else {
                                Notifications.warning('This content was updated in another window');
                            // }
                        }
                    } else {
                        Notifications.warning('This content was updated by another user');
                    }

                    // FluroContent.resource('site/' + item._id, true, true).get().$promise.then(function(site) {
                    //     console.log('Got NEW SITE', site);
                    //     _.assign($scope.item, site);
                    // })

                    // $state.reload();
                })
            }
        }
    }

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////



    $rootScope.snippets = snippets;

    $scope.openPreview = function() {
        $window.open('/preview');
    }

    /////////////////////////////////////////

    window.buildService = BuildService;

    /////////////////////////////////////////

    $scope.buildService = BuildService;

    /////////////////////////////////////////

    //Add the item to the scope
    $scope.item = item;

    //Anytime the item changes
    $scope.$watch('item', function(item) {

        //
        $rootScope.site = item;
        BuildService.site = item;
    })

    /////////////////////////////////////////

    //Setup one route selected at a time
    // var routeSelection = new ObjectSelection();
    // routeSelection.multiple = false;
    // $scope.routeSelection = routeSelection;




    //Select first route
    if (item.routes) {
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes(item.routes);
        BuildService.selection.select(flattenedRoutes[0]);
    }


    //$scope.selection.select(firstRoute);


    //////////////////////////////////////////////////





    /**
    function(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    alert('ctrl-s');
                    break;
                case 'f':
                    event.preventDefault();
                    alert('ctrl-f');
                    break;
                case 'g':
                    event.preventDefault();
                    alert('ctrl-g');
                    break;
            }
        }
    });



    $(window).on('keydown', keyDown);
    /**/

    ///////////////////////////////
    ///////////////////////////////

    $(window).on('keydown', keyDown);
    $(window).on('keyup', keyUp);

    ///////////////////////////////

    var keySaveDown;

    ///////////////////////////////

    function keyUp(event) {
        if (String(event.which) == '91') {
            keySaveDown = false;
        }
    }
    ///////////////////////////////



    function keyDown(event) {

        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    //event.stopPropagation();

                    if (keySaveDown) {
                        return;
                    }

                    keySaveDown = true;
                    $scope.save();
                    break;
            }
        }
    }

    $scope.$on("$destroy", function() {
        $(window).off('keydown', keyDown);
        $(window).off('keyup', keyUp);
    });

    /////////////////////////////////////////////


    var versionMessage = '';

    /////////////////////////////////////////////

    $scope.saveVersion = function() {


        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            templateUrl: 'routes/build/save-modal.html',
            controller: 'SaveModalController',
            size: 'sm',
        });

        modalInstance.result.then(function(message) {

            //If we wrote a message then just keep going
            if (message && message.length) {
                $scope.save({
                    version: message,
                });
            } else {
                $scope.save();
            }
        });
    }

    //////////////////////////////////////////////////

    //Save
    $scope.save = function(options) {

        if ($scope.isSaving) {
            console.log('SStop')
            return;
        }



        $scope.isSaving = true;

        ///////////////////////////////////

        if (!options) {
            options = {};
        }

        //Remove the version information
        delete $scope.item.version;

        //Check if we need to save a new version
        if (options.version) {
            //Check if our message has changed since last time
            if (versionMessage != options.version) {
                console.log('Saving with new message', options.version);
                //If it has then append the version message
                versionMessage = options.version;
                $scope.item.version = options.version;
                $scope.item.milestone = true;
            }
        }

        if (!$scope.item.title) {
            Notifications.warning('Please provide a title before saving');
            return;
        }

        Notifications.status('Saving');


        //////////////////////////////////////////////////////////

        //Duplicate the item and append the ignore socket attribute
        var saveData = angular.copy($scope.item);
        saveData.ignoreSocket = FluroSocket.getSocketID();

        //////////////////////////////////////////////////////////

        if ($scope.item._id) {
            //Updating existing content
            FluroContent.resource('site').update({
                id: $scope.item._id,
            }, saveData, saveSuccess, saveFail);
        } else {
            console.log('Save New')
                //If we are creating new content
            FluroContent.resource('site', true).save(saveData, saveSuccess, saveFail);
        }
    }


    function saveSuccess(res) {

        if (res.status == 500) {
            //delete $scope.item.__v;
            console.log('VERSION ERROR!!', res, $scope.item)
            return Notifications.error('Version Error, Please try again');
        }

        Notifications.status('Site saved successfully')
        console.log('SAVE SUCCESS', res);

        if (res._id) {

            if (!$scope.item._id) {
                $scope.item._id = res._id;

                console.log('Move to the State please!');
                //Move to the actual book instead of the create form
                $state.go('build.default', {
                    id: res._id
                })

            } else {
                $scope.item._id = res._id;
                $scope.item.__v = res.__v;
            }
        }



        $scope.isSaving = false;
    }

    function saveFail(res) {
        console.log('SAVE FAILED', res);

        if (res.data) {
            if (res.data.errors) {
                _.each(res.data.errors, function(err) {
                    Notifications.error(err)
                })
            } else {
                Notifications.error('Site failed to save ' + res.data);
            }
        } else {
            Notifications.error(res);
        }

        delete $scope.item.__v;


        $scope.isSaving = false;
    }


});
app.controller('SaveModalController', function($scope) {

})
app.controller('AvailableComponentController', function($scope, FluroContent) {


	if (!$scope.item.components) {
        $scope.item.components = [];
    }



	$scope.componentUsed = function(component) {


		var match  = _.find($scope.item.components, {_id:component._id});

		if(match) {
			return true;
		}
	}



	function upsert(arr, key, newval) {
	    
	    var match = _.find(arr, key);

	    if(match){
	        var index = _.indexOf(arr, _.find(arr, key));
	        arr.splice(index, 1, newval);
	    } else {
	        arr.push(newval);
	    }
	}



	$scope.updateComponent = function(component) {

		var cleanedUp = angular.fromJson(angular.toJson(component));

		upsert($scope.item.components, {_id:cleanedUp._id}, cleanedUp);
	}

	$scope.useComponent = function(component) {
		if($scope.componentUsed(component)) {
			return;
		}

		$scope.item.components.push(component);
	}


 })
app.controller('ComponentManagerController', function($scope, $rootScope, ModalService, FluroContent, $sessionStorage) {


    $scope.selected = {};
    $scope.settings = {};
    $scope.search = {};

    if(!$scope.item.components) {
        $scope.item.components = [];
    }

    /////////////////////////////////////////

    $scope.sessionStorage = $sessionStorage;

    /////////////////////////////////////////

    $scope.refreshAvailableComponents = function() {
        console.log('Refresh available components')
        $scope.availableComponents = FluroContent.resource('component', false, true).query();
    }

    $scope.refreshAvailableComponents();

    ////////////////////////////////////

    $scope.refreshMarketComponents = function() {
        console.log('Refresh market components')
        $scope.marketComponents = FluroContent.endpoint('my/components', false, true).query();
    }

    ////////////////////////////////////

    /**/
    $scope.sameAccount = function(component) {
        var accountId = component.account;

        if(!accountId) {
            return;
        }
        if (accountId._id) {
            accountId = accountId._id;
        }

        return (accountId == $rootScope.user.account._id);
    }
    /**/

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.addComponent = function() {

        var startScript = "app.components.directive('[DIRECTIVENAME]', function() {\n    return {\n        restrict: 'E', \n        replace:true, \n        template:'[FLURO_TEMPLATE]', \n        link: function($scope, $elem, $attr) {\n        } \n    } \n});";

        var newScript = {
            title: 'New Component',
            js: startScript,
            html: '<div></div>',
            realms: []
        };

        //$scope.item.components.push(newScript)
        //$scope.selected.component = newScript;

        /**/
        var params = {};
        params.template = newScript;

        ModalService.create('component', params, function(res) {

            $scope.item.components.push(res);
            $scope.selected.component = res;
            $scope.refreshAvailableComponents();
        });
        /**/
    }

    ////////////////////////////////////

    $scope.editComponent = function() {
        ModalService.edit($scope.selected.component, function(res) {

            $scope.replaceComponent(res);

            //$scope.item.components.push(res);
            $scope.refreshAvailableComponents();
        });
        /**/
    }

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.removeComponent = function() {
        if ($scope.selected.component) {

            _.pull($scope.item.components, $scope.selected.component);
            $scope.selected.component = null;
        }
    }

    ////////////////////////////////////

    $scope.pullComponent = function(component) {

        console.log('Pull Component', component);
        if (component) {

            var match = _.find($scope.item.components, {_id:component._id});
            if(match) {
                _.pull($scope.item.components, match);
                $scope.selected.component = null;
            }
        }
    }


    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.replaceComponent = function(component) {
        var cleanedUp = angular.fromJson(angular.toJson(component));
        _.assign($scope.selected.component, cleanedUp);
        console.log('Updated component', $scope.selected.component);
    }

    ////////////////////////////////////

    $scope.updateComponent = function(component) {
        $scope.settings.status = 'processing';

        function success(res) {
            $scope.settings.status = 'ready';
            $scope.replaceComponent(res);
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to update component', res)
        }

        FluroContent.resource('component').get({
            id: component._id
        }).$promise.then(success, failed)
    }

    ////////////////////////////////////

    $scope.publishComponent = function(component) {
        if (!component._id) {
            if (!component.realms && !component.realms.length) {
                return;
            }
        }

        $scope.settings.status = 'processing';
        //delete component._id;

        function success(res) {
            $scope.settings.status = 'ready';
            component._id = res._id;
            console.log('Saved component', res)

            $scope.refreshAvailableComponents();
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to save component', res)
        }

        if (component._id) {
            FluroContent.resource('component').update({
                id: component._id,
            }, component, success, failed);

        } else {
            FluroContent.resource('component').save(component).$promise.then(success, failed)
        }
    }


    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////


    /*
    if (!$scope.item.components) {
        $scope.item.components = [];
    }

    $scope.scriptSelection = {};


    $scope.settings = {};


    ////////////////////////////////////


    $scope.publishScript = function(component) {


        


        if (!component._id) {
            if (!component.realms && !component.realms.length) {
                return;
            }
        }

        $scope.settings.status = 'processing';
        //delete component._id;

        function success(res) {
            $scope.settings.status = 'ready';
            component._id = res._id;
            console.log('Saved component', res)
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to save component', res)
        }

        if (component._id) {

                FluroContent.resource('component').update({
                id: component._id,
            }, component, success, failed);

        } else {
            FluroContent.resource('component').save(component).$promise.then(success, failed)
        }
    }


    ////////////////////////////////////


     $scope.removeScriptID = function(component) {
        delete component._id;
     }

    ////////////////////////////////////

    $scope.updateScript = function(component) {
        $scope.settings.status = 'processing';

        function success(res) {
            $scope.settings.status = 'ready';


            var cleanedUp = angular.fromJson(angular.toJson(res));
            _.assign(component, cleanedUp);
            console.log('Updated component', $scope.selected.component);
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to update component', res)
        }

        FluroContent.resource('component').get({
            id: component._id
        }).$promise.then(success, failed)
    }

    ////////////////////////////////////

    $scope.addScript = function() {


        var startScript = "app.components.directive('[DIRECTIVENAME]', function() {\n    return {\n        restrict: 'E', \n        replace:true, \n        template:'[TEMPLATE]', \n        link: function($scope, $elem, $attr) {\n        } \n    } \n});";

        //var startScript = "app.components.directive('[DIRECTIVENAME]', function() { \n return { \n restrict: 'E', \n replace:true, \n template:'[TEMPLATE]', \n link: function($scope, $elem, $attr) { \n \n } \n } \n });";

        var newScript = {
            title: 'New Script',
            js: startScript,
            html: '<div></div>',
        };


        $scope.item.components.push(newScript)
        $scope.selected.component = newScript;


    }



*/





});
app.controller('ComponentMarketController', function($scope, $modal, FluroContent) {


	if (!$scope.item.components) {
        $scope.item.components = [];
    }

    $scope.refreshMarketComponents();

	$scope.componentUsed = function(component) {


		var match  = _.find($scope.item.components, {_id:component._id});

		if(match) {
			return true;
		}
	}


	$scope.viewReadme = function(component) {
		 var modalInstance = $modal.open({
            template: '<div class="panel panel-default"><div class="panel-body"><h2>{{model.title}}</h2><div compile-html="model.readme"/></div></div>',
            controller:function($scope) {
            	$scope.model = component;
            }
        });
        
        // modalInstance.result.then(function() {
        //     CheckinService.refresh();
        // });
	}

	function upsert(arr, key, newval) {
	    
	    var match = _.find(arr, key);

	    if(match){
	        var index = _.indexOf(arr, _.find(arr, key));
	        arr.splice(index, 1, newval);
	    } else {
	        arr.push(newval);
	    }
	}



	$scope.updateComponent = function(component) {

		var cleanedUp = angular.fromJson(angular.toJson(component));

		upsert($scope.item.components, {_id:cleanedUp._id}, cleanedUp);
	}

	$scope.useComponent = function(component) {
		if($scope.componentUsed(component)) {
			return;
		}

		$scope.item.components.push(component);
	}


 })
app.controller('HomeController', function($scope, items) {


	console.log('Home Controller', items);
	$scope.items = items;
});
app.controller('PreviewController', function($scope, $rootScope, $templateCache, $timeout, $compile, Fluro, $interval) {


    //Set Preview Mode
    $rootScope.previewMode = true;

    /////////////////////////////////////////////////

    //Get the site element
    var $element = angular.element('.site');

    /////////////////////////////////////////////////
    /////////////////////////////////////////////////

    //Store the cached template
    var cachedTemplate;
    var defaultTemplate = '<fluro-header/><fluro-content/><fluro-footer/>';

    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////

    function refreshPreview() {

        //Start off with nothing
        var newTemplate = '';

        /////////////////////////////////////////


        if(window.opener && window.opener.buildService) {
            var buildService = window.opener.buildService;

            $rootScope.buildService = buildService;
            $rootScope.site =
            $scope.site = buildService.site;
            $rootScope.currentRoute = buildService.selection.item;
            $rootScope.bodyClasses = 'route-' + _.kebabCase(buildService.selection.item.state);


            //Check if there is an outer html template
            if ($scope.site.outerHTML && $scope.site.outerHTML.length) {
                newTemplate = buildService.site.outerHTML;
            }


            ///////////////////////////////////

            //Add each template to the template cache
            _.each(buildService.site.templates, function(sectionTemplate) {
                $templateCache.put(sectionTemplate.key, sectionTemplate.html);
            })
        } else {
            return console.log('window opener not found'); 
        }


       /**


        //Get the scope from the opening window
        var $parentScope = window.opener.angular.element('#builder').scope();

        if ($parentScope) {
            $rootScope.builder =
                $scope.builder = $parentScope;
            $scope.site = $scope.builder.item;
            $rootScope.site = $scope.builder.item;
            $rootScope.currentRoute = $scope.builder.routeSelection.item;

            //Set route class
            $rootScope.bodyClasses = 'route-' + _.kebabCase($rootScope.currentRoute.state);

            //Check if there is an outer html template
            if ($scope.site.outerHTML && $scope.site.outerHTML.length) {
                newTemplate = $scope.site.outerHTML;
            }

            ///////////////////////////////////

        } else {
            $scope.builder = null;
            $scope.site = null;
        }
        /**/



        

        /////////////////////////////////////////

        //No need to change anything
        if (cachedTemplate == newTemplate) {
            return;
        }

        /////////////////////////////////////////

        //Set the template to our new template
        cachedTemplate = newTemplate;

        /////////////////////////////////////////

        //Create our actual template
        var template = cachedTemplate;

        /////////////////////////////////////////

        //Check if we should use the default template
        if (!template || !template.length) {
            template = defaultTemplate;
        }

        /////////////////////////////////////////

        //Replace elements
        template = template.replace(/<fluro-header\/>/g, '<route class="site-header" ng-if="!$root.currentRoute.disableHeader" ng-model="buildService.site.header"></route>');
        template = template.replace(/<fluro-content\/>/g, '<div class="site-page-container"><route class="site-page" ng-model="buildService.selection.item"></route></div>');
        template = template.replace(/<fluro-footer\/>/g, '<route class="site-footer" ng-if="!$root.currentRoute.disableFooter" ng-model="buildService.site.footer"></route>');

        /////////////////////////////////////////

        //Clear the old
        $element.empty();


        $timeout(function() {
            //In with the new
            var compiled = $compile(template)($scope);
            $element.append(compiled);
        }, 1000);



    }

    /////////////////////////////////////////////////

    //Refresh every second
    $interval(refreshPreview, 1500);

    /////////////////////////////////////////////////

    /*

        $scope.$watch('builder.routeSelection.item', function(route) {
        	$scope.route = route;
        })

        /*
            if ($scope.item.components.length) {
                var script = '';

                _.each(components, function(component) {

                    var directiveName = component.directive;
                    //script += component.js;
                    //var html = component.html;

                    //Add the html string
                    template += '<' + directiveName + '></' + directiveName + '>';

                    //var url = Fluro.apiURL + '/get/scripts/component/' + component._id;
                    script += '<script type="text/lazy">' + component.compiled + '</script>';

                    //$ocLazyLoad.inject('textAngular');

                })

                //script += '</script>';




                //console.log(compiledTemplate);

                $element.append(script);
                $compile($element.contents())($scope);

                $element.append(template);
                $compile($element.contents())($scope);


                /*
                        var string = '';

                        var list = _.map(components, function(component) {

                            string += '<div class="panel panel-default"><' + component.directive + '><' + component.directive + '></div>';

                            //Get the script location
                            var url = Fluro.apiURL + '/get/scripts/component/' + component._id;

                            //Return the details to lazyload
                            return {
                                type: 'js',
                                path: url,
                            }
                        });


                        console.log('Lazy load', list);

                        $ocLazyLoad.load(list).then(function() {

                            
                            


                            var template = string;
                            var compiled = $compile(template)($scope);

                            $element.append(compiled);

                        }, function() {
                            console.log('Failed to load components')
                        });
    */
    //  }




    /*
		console.log(window.opener);
		var main = window.opener.angular.element('#outer').scope();

		$scope.item = main.item;
		$scope.currentRoute = main.currentRoute;
	

    /*
	 $scope.getStylesheetURL = function(css) {
         var url = Fluro.apiURL + '/get/scripts/css/' + css._id +'?v='+ css.updated;
         return url;
    }




	/*
	window.setData = function(data) {
		console.log('SET DATA', data)

      $scope.$apply(function(){
        $scope.item = data;
      });
    };
    */

});
app.controller('TemplatesController', function($scope, items, $state, $modal, FluroContent) {



    $scope.items = items;

    ///////////////////////////////////////////////////////////

    $scope.search = {}


    ///////////////////////////////////////////////////////////

    $scope.useTemplate = function(item) {

        //Open a modal to ask for more details
        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            backdrop:'static',
            templateUrl: 'routes/templates/template-copy-modal.html',
            controller: function($scope) {
                $scope.template = {
                    title: item.title,
                    _id: item._id,
                    extras: []
                }

                $scope.status = 'ready'


                $scope.create = function() {

                    $scope.status = 'processing';

                    var data = $scope.template;
                    /**/
                    FluroContent.endpoint('templates/use').save(data).$promise
                    .then(function(site) {

                    	$scope.$close();

                        $state.go('build.default', {
                            id: site._id
                        });
                    }, function(res) {
                    	console.log('Failed');
                    	$scope.status = 'error'
                    })
/**/
                }

                /////////////////////////////////////

                $scope.isSelected = function(typeName) {
                    var includes = $scope.template.extras;
                    return _.contains(includes, typeName);
                }

                /////////////////////////////////////

                $scope.toggle = function(typeName) {

                    //Check if it's activated already

                    if ($scope.isSelected(typeName)) {
                        _.pull($scope.template.extras, typeName);
                    } else {
                        $scope.template.extras.push(typeName);
                    }

                }

                /////////////////////////////////////

                $scope.types = [];

                // $scope.types.push({
                //     title: 'Stylesheets & Scripts',
                //     type: 'code',
                // });

                $scope.types.push({
                    title: 'Definitions',
                    type: 'definitions',
                    description: 'Include all definitions from the parent account',
                });

                // $scope.types.push({
                //     title: 'Queries',
                //     type: 'query',

                // });

                // $scope.types.push({
                //     title: 'Articles',
                //     type: 'article',
                // });

                // $scope.types.push({
                //     title: 'Locations',
                //     type: 'location',
                // });

                $scope.types.push({
                    title: 'Roles and Permission Sets',
                    type: 'roles',
                    description: 'Recreate the roles and permission sets defined in the parent account',
                });

                $scope.types.push({
                    title: 'Setup new application',
                    type: 'application',
                    description: 'Setup a new "Website" application and include this site model',
                });


                // $scope.template.includes =  _.map($scope.types, function(type) {
                // 	return type.type;
                // });

            },
            size: 'md',
        });

        ////////////////////////////////////////////////////////

        //Modal was closed with a result
        // modalInstance.result.then(function(id) {

        // });
    }

});
app.controller('VersionListController', function($scope, logs) {



	
})
app.factory('ObjectSelection', function() {

    var ObjectSelection = function() {

        var controller = {
        	items:[],
        	item:null,
        }
       

        //////////////////////////////////
        controller.multiple = true;

        //////////////////////////////////

        controller.select = function(item) {
            if (controller.multiple) {
                if (!_.contains(controller.items, item)) {
                    controller.items.push(item);
                }
            } else {
                controller.item = item;
            }
        }

        //////////////////////////////////

        controller.deselect = function(item) {
            if (controller.multiple) {
                _.pull(controller.items, item);
            } else {
            	controller.item = null;
            }
        }

        //////////////////////////////////

        controller.toggle = function(item) {
        	if (controller.multiple) {
	            if (_.contains(controller.items, item)) {
	                controller.deselect(item);
	            } else {
	                controller.select(item);
	            }
	        } else {
	        	if(controller.item == item) {
	        		controller.item = null;
	        	} else {
	        		controller.item = item;
	        	}
	        }
        }

        //////////////////////////////////

        controller.contains = function(item) {
        	if(controller.multiple) {
            	return _.contains(controller.items, item)
        	} else {
            	return controller.item == item;
        	}
        }

        //////////////////////////////////

        return controller;
    }

    return ObjectSelection;

});
app.directive('compileTemplate', function($compile, $rootScope, $templateCache) {
    return {
        restrict: 'A',
        // scope: {
        //     templateName: '@templateName',
        // },
        link: function($scope, $element, $attr) {

            var templateName;

            //Watch to see if the template name changes
            $attr.$observe('compileTemplate', function(newTemplateName) {

                //Clear out the element
                $element.empty();

                templateName = newTemplateName;
            });

            //////////////////////////////////

            $scope.$watch(function() {

                if (templateName) {
                    return $templateCache.get(templateName);
                } else {
                    return;
                }

            }, function(html) {

                //Clear out the element
                $element.empty();

                //If theres a template
                if (html && html.length) {
                    //Compile it and append it
                    var template = $compile(html)($scope);
                    $element.append(template);
                }

            }, true);
        },
    };
});