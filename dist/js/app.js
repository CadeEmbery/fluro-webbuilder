function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName("meta");
    for (i = 0; i < metas.length; i++) if (metas[i].getAttribute("property") == stringKey) return metas[i].getAttribute("content");
    return "";
}

function initializeApplication() {
    function startApplication() {
        var req = $http.get(apiURL + "/session", config);
        req.success(function(res) {
            res ? (app.constant("$initUser", res), angular.element(document).ready(function() {
                angular.bootstrap(document, [ "fluro" ]);
            })) : console.log("Bootstrap error", res);
        }), req.error(function(res) {
            console.log("Bootstrap error", res);
        });
    }
    var initInjector = angular.injector([ "ng" ]), $http = initInjector.get("$http"), apiURL = getMetaKey("fluro_url"), token = getMetaKey("fluro_application_key"), config = {};
    if (token ? config.headers = {
        Authorization: "Bearer " + token
    } : config.withCredentials = !0, $parentScope && $parentScope.item) if ($parentScope.item.external && $parentScope.item.external.length && _.each($parentScope.item.external, function(url) {
        head.append('<script src="' + url + '" type="text/javascript"></script>');
    }), $parentScope.item.bower && $parentScope.item.bower.length) {
        var jsUrl = apiURL + "/get/site/vendor/" + $parentScope.item._id + "/js", cssUrl = apiURL + "/get/site/vendor/" + $parentScope.item._id + "/css";
        token && (jsUrl += "?access_token=" + token, cssUrl += "?access_token=" + token), 
        head.append('<script src="' + jsUrl + '" type="text/javascript"></script>'), head.append('<link href="' + cssUrl + '" type="text/css" rel="stylesheet"/>');
        var req = $http.get(jsUrl, config);
        req.success(function(res) {
            startApplication();
        }), req.error(function(res) {
            console.log("Bower Dependencies Error", res);
        });
    } else startApplication(); else startApplication();
}

console.log("Webbuilder V5 init");

var dependencies = [ "ngResource", "ngTouch", "ngAnimate", "ui.tree", "ui.router", "ui.bootstrap", "fluro.config", "fluro.customer", "fluro.asset", "fluro.types", "fluro.socket", "fluro.util", "fluro.content", "fluro.access", "fluro.auth", "fluro.video", "fluro.interactions", "fluro.notifications", "ngStorage", "ui.ace", "ui.sortable", "fluro.lazyload", "ui.tree", "ui.bootstrap.fontawesome", "angular-loading-bar", "formly", "formlyBootstrap", "720kb.socialshare", "ng-currency", "angularFileUpload" ], $parentScope;

if (window.opener && window.opener.angular) {
    var element = window.opener.angular.element("#builder");
    element && ($parentScope = element.scope(), $parentScope && $parentScope.item && $parentScope.item.dependencies && $parentScope.item.dependencies.length && (dependencies = dependencies.concat($parentScope.item.dependencies)));
}

console.log("Launch Dependencies");

var app = angular.module("fluro", dependencies), head = angular.element("head");

initializeApplication(), app.config(function($controllerProvider, $compileProvider, $filterProvider, $provide, $stateProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider) {
    app.components = {}, app.components.controller = $controllerProvider.register, app.components.directive = $compileProvider.directive, 
    app.components.filter = $filterProvider.register, app.components.factory = $provide.factory, 
    app.components.service = $provide.service;
    var accessToken = getMetaKey("fluro_application_key"), apiURL = getMetaKey("fluro_url"), initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: !0,
        backupToken: accessToken
    }, appDevelopmentURL = getMetaKey("app_dev_url");
    appDevelopmentURL && appDevelopmentURL.length ? initialConfig.appDevelopmentURL = appDevelopmentURL : $locationProvider.html5Mode(!0), 
    FluroProvider.set(initialConfig), accessToken || ($httpProvider.defaults.withCredentials = !0), 
    $httpProvider.interceptors.push("FluroAuthentication"), $urlRouterProvider.otherwise("/"), 
    $stateProvider.state("home", {
        url: "/",
        views: {
            builder: {
                templateUrl: "routes/home/home.html",
                controller: "HomeController",
                resolve: {
                    items: function(FluroContent) {
                        return console.log("Retrieve sites"), FluroContent.resource("site").query().$promise;
                    }
                }
            }
        }
    }), $stateProvider.state("versions", {
        url: "/versions/:id",
        views: {
            builder: {
                templateUrl: "routes/versions/versions.html",
                controller: "VersionListController",
                resolve: {
                    item: function($stateParams, FluroContent) {
                        return FluroContent.endpoint("log").query({
                            item: $stateParams.id,
                            limit: 500
                        }).$promise;
                    }
                }
            }
        }
    }), $stateProvider.state("templates", {
        url: "/templates",
        views: {
            builder: {
                templateUrl: "routes/templates/templates.html",
                controller: "TemplatesController",
                resolve: {
                    items: function(FluroContent) {
                        return FluroContent.endpoint("templates/site").query().$promise;
                    }
                }
            }
        }
    }), $stateProvider.state("build", {
        url: "/build/:id",
        "abstract": !0,
        views: {
            builder: {
                templateUrl: "routes/build/build.html",
                controller: "BuildController",
                resolve: {
                    item: function(FluroContent, $stateParams) {
                        return "new" != $stateParams.id ? FluroContent.resource("site").get({
                            id: $stateParams.id
                        }).$promise : {};
                    },
                    snippets: function(FluroContent, $stateParams) {
                        return FluroContent.endpoint("my/snippets").query().$promise;
                    }
                }
            }
        }
    }), $stateProvider.state("build.default", {
        url: "",
        templateUrl: "routes/build/pages/manager.html"
    }), $stateProvider.state("build.info", {
        url: "/info",
        templateUrl: "routes/build/info/manager.html"
    }), $stateProvider.state("build.styles", {
        url: "/styles",
        templateUrl: "routes/build/styles/manager.html"
    }), $stateProvider.state("build.dependencies", {
        url: "/dependencies",
        templateUrl: "routes/build/dependencies/manager.html"
    }), $stateProvider.state("build.components", {
        url: "/components",
        templateUrl: "routes/build/components/manager.html",
        controller: "ComponentManagerController"
    }), $stateProvider.state("preview", {
        url: "/preview",
        views: {
            builder: {
                templateUrl: "routes/preview/preview.html",
                controller: "PreviewController"
            }
        }
    });
}), app.run(function($state, VideoTools, Fluro, FluroTokenService, FluroWindowService, $sessionStorage, $localStorage, $stateParams, $rootScope, FluroAccess, FluroMenuService, Notifications, FluroScrollService, DateTools, Asset, $initUser) {
    $rootScope.getTitle = function() {
        var title = "WebBuilder";
        return $rootScope.previewMode && (title = "Preview"), $rootScope.site && $rootScope.site.title && (title = title + " - " + $rootScope.site.title), 
        title;
    };
    var aceEditorOnload = function(editor) {
        editor.$blockScrolling = 1 / 0;
        var _session = editor.getSession();
        ace.config.loadModule("ace/snippets/snippets", function() {
            var snippetManager = ace.require("ace/snippets").snippetManager;
            switch (_session.$modeId) {
              case "ace/mode/html":
                ace.config.loadModule("ace/snippets/html", function(m) {
                    m && $rootScope.snippets.length && (_.chain($rootScope.snippets).filter({
                        syntax: "html"
                    }).each(function(snippet) {
                        m.snippets.push({
                            content: snippet.body,
                            tabTrigger: String(snippet.account.title + ":" + snippet.title).toLowerCase()
                        });
                    }).value(), snippetManager.register(m.snippets, m.scope));
                });
                break;

              case "ace/mode/javascript":
                ace.config.loadModule("ace/snippets/javascript", function(m) {
                    m && $rootScope.snippets.length && (_.chain($rootScope.snippets).filter({
                        syntax: "js"
                    }).each(function(snippet) {
                        m.snippets.push({
                            content: snippet.body,
                            tabTrigger: String(snippet.account.title + ":" + snippet.title).toLowerCase()
                        });
                    }).value(), snippetManager.register(m.snippets, m.scope));
                });
            }
        });
    };
    $rootScope.aceEditorOptions = {
        html: {
            require: [ "ace/ext/language_tools" ],
            advanced: {
                enableSnippets: !0,
                enableBasicAutocompletion: !0,
                enableLiveAutocompletion: !0
            },
            useWrapMode: !1,
            showGutter: !0,
            theme: "tomorrow_night_eighties",
            mode: "html",
            onLoad: aceEditorOnload
        },
        scss: {
            require: [ "ace/ext/language_tools" ],
            advanced: {
                enableSnippets: !0,
                enableBasicAutocompletion: !0,
                enableLiveAutocompletion: !0
            },
            useWrapMode: !1,
            showGutter: !0,
            theme: "tomorrow_night_eighties",
            mode: "scss",
            onLoad: aceEditorOnload
        },
        js: {
            require: [ "ace/ext/language_tools" ],
            advanced: {
                enableSnippets: !0,
                enableBasicAutocompletion: !0,
                enableLiveAutocompletion: !0
            },
            useWrapMode: !1,
            showGutter: !0,
            theme: "tomorrow_night_eighties",
            mode: "javascript",
            onLoad: aceEditorOnload
        }
    }, $rootScope.user = $initUser, $rootScope.init = !0, $rootScope.localStorage = $localStorage, 
    $rootScope.sessionStorage = $sessionStorage, $rootScope.window = FluroWindowService, 
    $rootScope.access = FluroAccess, $rootScope.asset = Asset, $rootScope.getVideoImageUrl = VideoTools.getVideoThumbnail, 
    $rootScope.getImageUrl = Asset.imageUrl, $rootScope.getThumbnailUrl = Asset.thumbnailUrl, 
    $rootScope.getDownloadUrl = Asset.downloadUrl, $rootScope.getUrl = Asset.getUrl, 
    $rootScope.menu = FluroMenuService, $rootScope.date = DateTools, $rootScope.notifications = Notifications, 
    $rootScope.scroll = FluroScrollService, $rootScope.$state = $state, $rootScope.$stateParams = $stateParams, 
    $rootScope.$on("$stateChangeStart", function(evt, to, params) {
        to.redirectTo && (evt.preventDefault(), $state.go(to.redirectTo, params));
    }), $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        throw console.log("State change error"), error;
    }), $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.firstLoad = !0;
    }), FastClick.attach(document.body);
}), app.directive("contentBrowser", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            typeName: "=ngType",
            params: "=?params",
            done: "=ngDone"
        },
        templateUrl: "admin-content-browser/admin-content-browser.html",
        controller: "ContentBrowserController"
    };
}), app.controller("ContentBrowserController", function($scope, TypeService, $rootScope, FluroContent, $http, $filter, Fluro, FluroStorage) {
    function updateFilters() {
        if ($scope.items) {
            var filteredItems = $scope.items;
            filteredItems = $filter("filter")(filteredItems, $scope.search.terms), filteredItems = $filter("orderBy")(filteredItems, $scope.search.order, $scope.search.reverse), 
            $scope.search.filters && _.forOwn($scope.search.filters, function(value, key) {
                _.isArray(value) ? _.forOwn(value, function(value) {
                    filteredItems = $filter("reference")(filteredItems, key, value);
                }) : filteredItems = $filter("reference")(filteredItems, key, value);
            }), $scope.filteredItems = filteredItems, $scope.updatePage();
        }
    }
    $scope.typeName ? ($scope.type = TypeService.getTypeFromPath($scope.typeName), $scope.title = "Select " + $scope.type.plural) : $scope.title = "Select content", 
    $scope.close = function() {
        $scope.done && $scope.done();
    }, $scope.toggle = function(item) {
        _.some($scope.model, {
            _id: item._id
        }) ? $scope.deselect(item) : $scope.select(item);
    }, $scope.select = function(item) {
        _.some($scope.model, {
            _id: item._id
        }) || $scope.model.push(item);
    }, $scope.deselect = function(item) {
        var found = _.find($scope.model, {
            _id: item._id
        });
        found && _.pull($scope.model, found);
    }, $scope.isSelected = function(item) {
        return _.some($scope.model, {
            _id: item._id
        });
    }, $scope.params || ($scope.params = {});
    var session = (FluroStorage.localStorage("content-browser." + $scope.typeName), 
    FluroStorage.sessionStorage("content-browser." + $scope.typeName));
    session.search ? $scope.search = session.search : $scope.search = session.search = {
        filters: {
            status: [ "active" ]
        }
    }, session.pager ? $scope.pager = session.pager : $scope.pager = session.pager = {
        limit: 20,
        maxSize: 8
    }, $scope.setOrder = function(string) {
        $scope.search.order = string;
    }, $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
    }, $scope.$watch("search", updateFilters, !0), $scope.refresh = function() {
        if ($scope.type && "account" == $scope.type.path) return FluroContent.resource("account", !1, !0).query().$promise.then(function(results) {
            $scope.items = results, updateFilters();
        });
        var queryDetails = {};
        $scope.type && (queryDetails.type = $scope.type.path), $scope.params.searchInheritable && (queryDetails.searchInheritable = !0), 
        $scope.params.syntax && (queryDetails.syntax = $scope.params.syntax), FluroContent.endpoint("content", !1, !0).query(queryDetails).$promise.then(function(results) {
            $scope.items = results, updateFilters();
        });
    }, $scope.refresh(), $scope.removeFilter = function(key) {
        $scope.search.filters && delete $scope.search.filters[key];
    }, $scope.$watch("search", updateFilters, !0), $scope.updatePage = function() {
        $scope.filteredItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 1);
        var filteredItems = $scope.filteredItems, pageItems = $filter("startFrom")(filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter("limitTo")(pageItems, $scope.pager.limit), $scope.pageItems = pageItems;
    };
}), app.directive("filterBlock", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            source: "=ngSource",
            getFilterKey: "&ngFilterKey",
            getFilterTitle: "&ngFilterTitle",
            persist: "@"
        },
        templateUrl: "admin-content-filter-block/admin-content-filter-block.html",
        controller: "ContentFilterBlock"
    };
}), app.controller("ContentFilterBlock", function($scope, $timeout, $filter, Tools) {
    function addFilterOption(result, item, field_key, source) {
        if (source) {
            var title, key = source[field_key];
            title = _.isString(source) ? key = source : source.title ? source.title : source.name, 
            result[key] ? result[key].count++ : result[key] = {
                title: title,
                key: key,
                count: 1
            };
        }
    }
    var filterKey = $scope.getFilterKey(), filterTitle = $scope.getFilterTitle();
    $scope.filterKey = filterKey, $scope.filterTitle = filterTitle, $scope.model || ($scope.model = {}), 
    $scope.filterIsActive = function(value) {
        return _.contains($scope.model[filterKey], value);
    }, $scope.filtersUsed = function() {
        return $scope.model[filterKey] && $scope.model[filterKey].length;
    }, $scope.toggleFilter = function(value) {
        var active = _.contains($scope.model[filterKey], value);
        $scope.persist ? active ? $scope.model[filterKey] = [] : $scope.model[filterKey] = [ value ] : active ? $scope.removeFilter(value) : $scope.addFilter(value);
    }, $scope.removeFilter = function(value) {
        value ? _.pull($scope.model[filterKey], value) : delete $scope.model[filterKey];
    }, $scope.addFilter = function(value) {
        $scope.model[filterKey] || ($scope.model[filterKey] = []), _.contains($scope.model[filterKey], value) || $scope.model[filterKey].push(value);
    }, $scope.$watch("source", function(items) {
        var filterOptions = _.chain(items).filter(function(item) {
            var obj = Tools.getDeepProperty(item, filterKey);
            return obj ? !0 : void 0;
        }).reduce(function(result, item, key) {
            var obj = Tools.getDeepProperty(item, filterKey);
            return _.isArray(obj) ? _.each(obj, function(sub) {
                addFilterOption(result, item, "_id", sub);
            }) : _.isString(obj) ? addFilterOption(result, item, filterKey, obj) : addFilterOption(result, item, "_id", obj), 
            result;
        }, {}).value();
        $scope.filterOptions = filterOptions, $scope.count = _.keys($scope.filterOptions).length;
    });
}), app.directive("contentSelect", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            ngParams: "&"
        },
        templateUrl: "admin-content-select/admin-content-select.html",
        controller: "ContentSelectController"
    };
}), app.controller("ContentSelectController", function($scope, $location, $window, Asset, $rootScope, FluroContent, Fluro, ModalService, TypeService, FluroAccess, $http) {
    function getBaseDomain(string) {
        var split = string.split(".");
        return split.slice(Math.max(split.length - 2, 1)).join(".");
    }
    $scope.selectedItems || ($scope.selectedItems = {
        items: []
    }), $scope.params || ($scope.params = {}), $scope.asset = Asset, $scope.$watch(function() {
        return $scope.ngParams();
    }, function(p) {
        p || (p = {}), $scope.params = p, $scope.params.canCreate && ($scope.canCreate = $scope.params.canCreate), 
        $scope.params.type && ($scope.type = TypeService.getTypeFromPath($scope.params.type), 
        $scope.type || ($scope.browseAccessDisabled = !0)), $scope.params.hideBrowse && ($scope.hideBrowse = !0), 
        $scope.params.minimum && ($scope.minimum = $scope.params.minimum), $scope.params.maximum && ($scope.maximum = $scope.params.maximum), 
        $scope.params.allowedValues && ($scope.allowedValues = $scope.params.allowedValues), 
        $scope.params.defaultValues && ($scope.defaultValues = $scope.params.defaultValues, 
        $scope.selectedItems.items.length || _.each($scope.defaultValues, function(item) {
            $scope.selectedItems.items.push(item);
        }));
    }, !0), $scope.$watch("model", function() {
        $scope.model ? _.isArray($scope.model) ? $scope.model.length ? $scope.maximum ? $scope.selectedItems.items = _.take($scope.model, $scope.maximum) : $scope.selectedItems.items = angular.copy($scope.model) : $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues) : $scope.selectedItems.items = [ $scope.model ] : $scope.selectedItems.items = [];
    }, !0), $scope.$watch("selectedItems.items", function(items) {
        1 == $scope.maximum ? $scope.model = _.first(items) : $scope.model = items, $scope.model || ($scope.model = null);
    }, !0), $scope.proposed = {}, $scope.dynamicPopover = {
        templateUrl: "views/ui/create-popover.html"
    }, $scope.searchPlaceholder = "Search for existing content", $scope.type && ($scope.searchPlaceholder = "Search for " + $scope.type.singular + " items"), 
    $scope.createEnabled = function() {
        return $scope.type ? FluroAccess.can("create", $scope.type.path) : !1;
    }, $scope.isSortable = function() {
        return $scope.maximum ? $scope.maximum > 1 : !0;
    }, $scope.addEnabled = function() {
        return $scope.browseAccessDisabled ? void 0 : $scope.maximum ? $scope.selectedItems.items.length < $scope.maximum : !0;
    }, $scope.add = function(item) {
        $scope.selectedItems.items.push(item), $scope.proposed = {};
    }, $scope.remove = function(item) {
        _.contains($scope.selectedItems.items, item) && _.pull($scope.selectedItems.items, item);
    }, $scope.edit = function(item) {
        ModalService.edit(item);
    }, $scope.view = function(item) {
        ModalService.view(item);
    }, $scope.editInAdmin = function(item) {
        var def = item._type;
        item.definition && item.definition.length && (def = item.definition);
        var baseDomain = getBaseDomain(Fluro.apiURL), domain = "";
        switch (baseDomain) {
          case "fluro.dev:3000":
            domain = "http://admin.fluro.dev:3000";
            break;

          case "inyerpocket.com":
            domain = "http://admin.inyerpocket.com";
            break;

          default:
            domain = "http://admin.fluro.io";
        }
        var url = domain + "/" + item._type + "/" + def + "/" + item._id + "/edit";
        $window.open(url, "_blank");
    }, $scope.canEditAdmin = function(item) {
        var canEdit = $scope.canEdit(item), currentDomain = $location.host(), notAdmin = !_.startsWith(currentDomain, "admin");
        return canEdit && notAdmin;
    }, $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }, $scope.canView = function(item) {
        return FluroAccess.canViewItem(item);
    }, $scope.browse = function() {
        if (!$scope.browseAccessDisabled) {
            var params = {};
            $scope.params.searchInheritable && (params.searchInheritable = $scope.params.searchInheritable = !0), 
            $scope.params.syntax && (params.syntax = $scope.params.syntax), $scope.type ? ModalService.browse($scope.type.path, $scope.selectedItems, params) : ModalService.browse(null, $scope.selectedItems, params);
        }
    }, $scope.createableTypes = TypeService.getAllCreateableTypes, $scope.create = function(typeToCreate) {
        var options = {};
        $scope.params.template && (options.template = $scope.params.template), typeToCreate ? ModalService.create(typeToCreate, options, $scope.add) : ModalService.create($scope.type.path, options, $scope.add);
    }, $scope.getOptions = function(val) {
        if ($scope.allowedValues && $scope.allowedValues.length) return _.reduce($scope.allowedValues, function(filtered, item) {
            var exists = _.some($scope.selectedItems.items, {
                _id: item._id
            });
            return exists || filtered.push(item), $filter("filter")(filtered, val);
        }, []);
        var searchUrl = "content";
        return $scope.type && (searchUrl += "/" + $scope.type.path), searchUrl += "/search", 
        FluroContent.endpoint(searchUrl + "/" + val, !0, !0).query({
            limit: 15
        }).$promise.then(function(results) {
            return console.log("SEARCH", results, val), _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    };
}), app.directive("newContentSelect", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            config: "="
        },
        templateUrl: "admin-content-select/new-content-select.html",
        controller: "NewContentSelectController"
    };
}), app.controller("NewContentSelectController", function($scope, Asset, $rootScope, Fluro, ModalService, TypeService, FluroAccess, $http) {
    $scope.selectedItems || ($scope.selectedItems = {
        items: []
    }), $scope.config || ($scope.config = {}), $scope.asset = Asset, $scope.$watch("config", function(config) {
        $scope.canCreate = config.canCreate, $scope.hideBrowse = config.hideBrowse, $scope.minimum = parseInt(config.minimum), 
        $scope.maximum = parseInt(config.maximum), config.type && ($scope.type = TypeService.getTypeFromPath(config.type), 
        $scope.type || ($scope.browseAccessDisabled = !0)), config.allowedValues && ($scope.allowedValues = config.allowedValues), 
        config.defaultValues && ($scope.defaultValues = config.defaultValues, $scope.selectedItems.items.length || _.each($scope.defaultValues, function(item) {
            $scope.selectedItems.items.push(item);
        })), config.type && ($scope.type = TypeService.getTypeFromPath(config.type), $scope.type || ($scope.browseAccessDisabled = !0));
    }, !0), $scope.$watch("model", function() {
        $scope.model ? _.isArray($scope.model) ? $scope.model.length ? $scope.maximum ? $scope.selectedItems.items = _.take($scope.model, $scope.maximum) : $scope.selectedItems.items = angular.copy($scope.model) : $scope.selectedItems.items = _.union($scope.model, $scope.defaultValues) : $scope.selectedItems.items = [ $scope.model ] : $scope.model = [];
    }, !0), $scope.$watch("selectedItems.items", function(items) {
        1 == $scope.maximum ? $scope.model = _.first(items) : $scope.model = items;
    }, !0), $scope.proposed = {}, $scope.dynamicPopover = {
        templateUrl: "views/ui/create-popover.html"
    }, $scope.searchPlaceholder = "Search for existing content", $scope.type && ($scope.searchPlaceholder = "Search for " + $scope.type.singular + " items"), 
    $scope.createEnabled = function() {
        return $scope.type ? FluroAccess.can("create", $scope.type.path) : !1;
    }, $scope.isSortable = function() {
        return $scope.maximum ? $scope.maximum > 1 : !0;
    }, $scope.addEnabled = function() {
        if ($scope.browseAccessDisabled) return !1;
        if (!$scope.maximum) return !0;
        var selectedItems = $scope.selectedItems.items;
        return selectedItems.length < $scope.maximum;
    }, $scope.add = function(item) {
        $scope.selectedItems.items.push(item), $scope.proposed = {};
    }, $scope.remove = function(item) {
        _.contains($scope.selectedItems.items, item) && _.pull($scope.selectedItems.items, item);
    }, $scope.edit = function(item) {
        ModalService.edit(item);
    }, $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }, $scope.browse = function() {
        if (!$scope.browseAccessDisabled) {
            var params = {};
            $scope.config.searchInheritable && (params.searchInheritable = $scope.config.searchInheritable = !0), 
            $scope.type ? ModalService.browse($scope.type.path, $scope.selectedItems, params) : ModalService.browse(null, $scope.selectedItems, params);
        }
    }, $scope.createableTypes = TypeService.getAllCreateableTypes, $scope.create = function(typeToCreate) {
        var options = {};
        $scope.config.template && (options.template = $scope.config.template), typeToCreate ? ModalService.create(typeToCreate, options, $scope.add) : ModalService.create($scope.type.path, options, $scope.add);
    }, $scope.getOptions = function(val) {
        if ($scope.allowedValues && $scope.allowedValues.length) return _.reduce($scope.allowedValues, function(filtered, item) {
            var exists = _.some($scope.selectedItems.items, {
                _id: item._id
            });
            return exists || filtered.push(item), $filter("filter")(filtered, val);
        }, []);
        var searchUrl = Fluro.apiURL + "/content";
        $scope.type && (searchUrl += "/" + $scope.type.path), searchUrl += "/search";
        var searchParams = {
            limit: 10
        };
        return $scope.config.searchInheritable && (searchParams.searchInheritable = !0), 
        $http.get(searchUrl + "/" + val, {
            ignoreLoadingBar: !0,
            params: searchParams
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selectedItems.items, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    };
}), app.directive("fieldEdit", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-field-edit/admin-field-edit.html",
        controller: "FieldEditController"
    };
}), app.controller("FieldEditController", function($scope, TypeService) {
    function stopWatchingTitle() {
        watchTitle && watchTitle();
    }
    function startWatchingTitle() {
        watchTitle && watchTitle(), watchTitle = $scope.$watch("model.title", function(newValue) {
            newValue && ($scope.model.key = _.camelCase(newValue));
        });
    }
    $scope.model || ($scope.model = {}), $scope.referenceParams = $scope.definedTypes = TypeService.definedTypes, 
    $scope.types = TypeService.types, $scope.showOptions = function() {
        switch ($scope.model.directive) {
          case "select":
          case "button-select":
          case "search-select":
          case "order-select":
            return !0;
        }
    }, $scope.isNumberType = function(type) {
        switch (type) {
          case "number":
          case "integer":
          case "float":
            return !0;
        }
    }, $scope.$watch("model", function(model) {
        model.key && model.key.length ? stopWatchingTitle() : startWatchingTitle();
    });
    var watchTitle;
    $scope.$watch("model.key", function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, "");
        }
    });
}), app.directive("fieldEditor", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-field-edit/admin-field-editor.html",
        controller: "FieldEditorController"
    };
}), app.controller("FieldEditorController", function($scope) {
    function getFieldPathTrail(array, target, trail, result) {
        for (var key in array) {
            var field = array[key];
            if (field == target) return field.asObject || "embedded" == field.directive ? field.minimum == field.maximum == 1 ? trail.push(field.key) : trail.push(field.key + "[i]") : trail.push(field.key), 
            void (result.trail = trail.slice());
            field.fields && field.fields.length && ((field.asObject || "embedded" == field.directive) && (field.minimum == field.maximum == 1 ? trail.push(field.key) : trail.push(field.key + "[i]")), 
            getFieldPathTrail(field.fields, target, trail, result), (field.asObject || "embedded" == field.directive) && trail.pop());
        }
    }
    function getFlattenedFields(array) {
        return _.chain(array).reduce(function(result, field, key) {
            if (("group" == field.type || field.directive) && result.push(field), field.fields && field.fields.length) {
                var folders = getFlattenedFields(field.fields);
                folders.length && result.push(folders);
            }
            return result;
        }, []).flatten().compact().value();
    }
    function getParentArray(item) {
        var fieldFolders = getFlattenedFields($scope.model), parent = _.find(fieldFolders, function(field) {
            return _.includes(field.fields, item);
        });
        return parent ? parent.fields : $scope.model;
    }
    $scope.model || ($scope.model = []), $scope.selected = {}, $scope.selectField = function(node) {
        $scope.selected.field = node;
    }, $scope.hasSubFields = function(node) {
        return "group" == node.type || "embedded" == node.directive;
    };
    var newObject = {
        type: "string",
        directive: "input",
        minimum: 0,
        maximum: 1,
        fields: []
    }, newGroup = {
        type: "group",
        fields: [],
        collapsed: !0
    };
    $scope._new = angular.copy(newObject), $scope._newGroup = angular.copy(newGroup), 
    $scope.sortableOptions = {
        handle: " .handle"
    }, $scope.add = function(object) {
        if (!$scope.contains(object)) {
            var parent = $scope.model, selectedField = $scope.selected.field;
            if (selectedField) {
                parent = getParentArray(selectedField);
                var index = parent.indexOf(selectedField);
                if (-1 != index) return parent.splice(index + 1, 0, object), !0;
            }
            return parent.push(object), !0;
        }
    }, $scope.getFieldPath = function(target) {
        var trail = [], result = {};
        return getFieldPathTrail($scope.model, target, trail, result), "interaction." + result.trail.join(".");
    }, $scope.remove = function(item) {
        var parentArray = getParentArray(item);
        parentArray && _.pull(parentArray, item), delete $scope.selected.field;
    }, $scope.contains = function(object) {
        return _.includes($scope.model, object);
    }, $scope.create = function() {
        var insert = angular.copy($scope._new), inserted = $scope.add(insert);
        $scope.selected.field = insert, inserted && ($scope._new = angular.copy(newObject));
    }, $scope.duplicate = function() {
        if ($scope.selected.field) {
            var insert = angular.copy($scope.selected.field);
            insert.title = insert.title + " Copy", insert.key = "", delete insert._id;
            var inserted = $scope.add(insert);
            inserted && ($scope.selected.field = insert, $scope._new = angular.copy(newObject));
        }
    }, $scope.createGroup = function() {
        var insert = angular.copy($scope._newGroup), inserted = $scope.add(insert);
        $scope.selected.field = insert, inserted && ($scope._newGroup = angular.copy(newGroup));
    };
}), app.directive("fieldGroupEdit", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-field-edit/admin-field-group-edit.html",
        controller: "FieldGroupEditController"
    };
}), app.controller("FieldGroupEditController", function($scope, TypeService) {
    $scope.model || ($scope.model = {}), $scope.$watch("model.asObjectType", function(objectType) {
        "contactdetail" == objectType ? $scope.definedTypes = _.filter(TypeService.definedTypes, {
            parentType: "contactdetail"
        }) : $scope.definedTypes = null;
    }), $scope.model.key || $scope.$watch("model.title", function(newValue) {
        newValue && ($scope.model.key = _.camelCase(newValue));
    }), $scope.$watch("model.key", function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, "");
        }
    });
}), app.directive("keyContentSelect", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-key-content-select/admin-key-content-select.html",
        controller: "KeyContentSelectController"
    };
}), app.controller("KeyContentSelectController", function($scope) {
    $scope.model && _.isArray($scope.model) || ($scope.model = []), $scope._new = {}, 
    $scope.remove = function(item) {
        _.pull($scope.model, item);
    }, $scope.create = function() {
        var insert = angular.copy($scope._new);
        if ($scope.model || ($scope.model = []), insert.key) {
            var keyExists = _.find($scope.model, {
                key: insert.key
            });
            keyExists || ($scope.model.push(insert), $scope._new = {});
        }
    };
}), app.directive("keyValueSelect", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-key-value-select/admin-key-value-select.html",
        controller: "KeyValueSelectController"
    };
}), app.directive("ngEnter", function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            13 === event.which && (scope.$apply(function() {
                scope.$eval(attrs.ngEnter);
            }), event.preventDefault());
        });
    };
}), app.controller("KeyValueSelectController", function($scope) {
    (!$scope.model || _.isObject($scope.model) && _.isArray($scope.model)) && ($scope.model = {}), 
    $scope._new = {}, $scope.remove = function(key) {
        delete $scope.model[key];
    }, $scope.create = function() {
        var insert = angular.copy($scope._new);
        $scope.model || ($scope.model = {}), insert.key && !$scope.model[insert.key] && ($scope.model[insert.key] = insert.value, 
        $scope._new = {});
    };
}), app.directive("multiValueField", function() {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            placeholder: "@"
        },
        templateUrl: "admin-multi-value-field/multi-value-field.html",
        controller: "MultiValueFieldController"
    };
}), app.controller("MultiValueFieldController", function($scope) {
    $scope.model || ($scope.model = []), $scope.add = function(value) {
        value && value.length && ($scope.contains(value) || ($scope.model.push(value), delete $scope._proposed));
    }, $scope.remove = function(entry) {
        _.pull($scope.model, entry);
    }, $scope.contains = function(entry) {
        return _.contains($scope.model, entry);
    };
}), app.directive("realmSelect", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            definedName: "=ngType",
            restrictedRealms: "=ngRestrict",
            popoverDirection: "@"
        },
        templateUrl: "admin-realm-select/admin-realm-select.html",
        controller: "RealmSelectController"
    };
}), app.directive("realmSelectItem", function($compile, $templateCache) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            realm: "=ngModel",
            selection: "=ngSelection",
            searchTerms: "=ngSearchTerms"
        },
        templateUrl: "admin-realm-select/admin-realm-select-item.html",
        link: function($scope, $element, $attrs) {
            function hasActiveChild(realm) {
                var active = $scope.contains(realm);
                return active ? !0 : realm.children && realm.children.length ? _.some(realm.children, hasActiveChild) : void 0;
            }
            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    return _.isObject(i) ? i._id == realm._id : i == realm._id;
                });
            }, $scope.toggle = function(realm) {
                var matches = _.filter($scope.selection, function(existingRealmID) {
                    var searchRealmID = realm._id;
                    return existingRealmID._id && (existingRealmID = existingRealmID._id), searchRealmID == existingRealmID;
                });
                matches.length ? _.each(matches, function(match) {
                    _.pull($scope.selection, match);
                }) : $scope.selection.push(realm);
            }, $scope.isExpanded = function(realm) {
                return realm.expanded ? realm.expanded : $scope.activeTrail(realm);
            }, $scope.activeTrail = function(realm) {
                return realm.children ? _.some(realm.children, hasActiveChild) : void 0;
            };
            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:\'title\' track by subRealm._id"></realm-select-item>', newElement = angular.element(template);
            $compile(newElement)($scope), $element.find(".realm-select-children").append(newElement);
        }
    };
}), app.controller("RealmSelectController", function($scope, $rootScope, TypeService, FluroAccess, FluroSocket) {
    function socketUpdate(eventData) {
        return console.log("New realm available"), "realm" == eventData.data._type ? reloadAvailableRealms() : void 0;
    }
    function reloadAvailableRealms() {
        function definedTypesLoaded() {
            var typeDefinition = TypeService.getTypeFromPath($scope.definedName);
            if (typeDefinition) {
                var parentType = $scope.definedName;
                typeDefinition.parentType && (parentType = typeDefinition.parentType), FluroAccess.retrieveSelectableRealms("create", $scope.definedName, parentType).then(function(realmTree) {
                    function getFlatRealms(realm) {
                        if (!realm.children) return realm;
                        var children = _.map(realm.children, getFlatRealms);
                        return children.push(realm), children;
                    }
                    if ($scope.tree = realmTree, $scope.restrictedRealms && $scope.restrictedRealms.length) {
                        console.log("Restricted Realms", $scope.restrictedRealms);
                        var restrictedRealmIDs = _.map($scope.restrictedRealms, function(realm) {
                            return realm._id ? realm._id : realm;
                        }), flattened = _.chain($scope.tree).map(getFlatRealms).flattenDeep().compact().uniq().value();
                        $scope.tree = _.filter(flattened, function(realm) {
                            return _.includes(restrictedRealmIDs, realm._id);
                        }), 1 != $scope.tree.length || $scope.model.length || ($scope.model = [ $scope.tree[0] ]);
                    } else 1 != realmTree.length || $scope.model.length || realmTree.children && realmTree.children.length || ($scope.model = [ realmTree[0] ]);
                });
            }
        }
        TypeService.definedTypes ? TypeService.definedTypes.$promise.then(definedTypesLoaded) : TypeService.refreshDefinedTypes().$promise.then(definedTypesLoaded);
    }
    $scope.dynamicPopover = {
        templateUrl: "admin-realm-select/admin-realm-popover.html"
    }, $scope.popoverDirection || ($scope.popoverDirection = "bottom"), $scope.model || ($scope.model = []), 
    FluroSocket.on("content.create", socketUpdate), $scope.$on("$destroy", function() {
        FluroSocket.off("content.create", socketUpdate);
    }), reloadAvailableRealms(), $scope.isSelectable = function(realm) {
        var realmID = realm;
        return realmID._id && (realmID = realmID._id), $scope.restrictedRealms && $scope.restrictedRealms.length ? _.some($scope.restrictedRealms, function(restrictedRealmID) {
            return restrictedRealmID._id && (restrictedRealmID = restrictedRealmID._id), realmID == restrictedRealmID;
        }) : !0;
    }, $scope.contains = function(realm) {
        return _.some($scope.model, function(i) {
            return _.isObject(i) ? i._id == realm._id : i == realm._id;
        });
    }, $scope.toggle = function(realm) {
        var matches = _.filter($scope.model, function(r) {
            var id = r;
            return _.isObject(r) && (id = r._id), id == realm._id;
        });
        matches.length ? $scope.model = _.reject($scope.model, function(r) {
            var id = r;
            return _.isObject(r) && (id = r._id), id == realm._id;
        }) : $scope.model.push(realm);
    };
}), app.directive("search", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "admin-search/search.html",
        controller: "SearchFormController"
    };
}), app.controller("SearchFormController", function($scope) {
    $scope.clear = function() {
        $scope.model = "";
    };
}), app.directive("tagSelect", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            type: "=ngType"
        },
        templateUrl: "admin-tag-select/admin-tag-select.html",
        controller: "TagSelectController"
    };
}), app.controller("TagSelectController", function($scope, $rootScope, CacheManager, Fluro, $http, FluroContent) {
    $scope.model || ($scope.model = []), $scope.dynamicPopover = {
        templateUrl: "admin-tag-select/admin-tag-popover.html"
    }, $scope.proposed = {}, $scope.add = function(item) {
        $scope.model.push(item), $scope.proposed = {};
    }, $scope.removeTag = function(tag) {
        _.pull($scope.model, tag);
    }, $scope.getTags = function(val) {
        var url = Fluro.apiURL + "/content/tag/search/" + val;
        return $http.get(url, {
            ignoreLoadingBar: !0,
            params: {
                limit: 20
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    }, $scope.create = function() {
        var details = {
            title: $scope.proposed.value
        };
        FluroContent.resource("tag").save(details, function(tag) {
            $scope.model.push(tag), CacheManager.clear("tag"), $scope.proposed = {};
        }, function(data) {
            console.log("Failed to create tag", data);
        });
    };
}), app.directive("application", function() {
    return {
        restrict: "E",
        replace: !0,
        template: '<div class="outer-application-wrapper" ui-view="builder" ng-if="$root.init"/>',
        controller: "ApplicationController"
    };
}), app.controller("ApplicationController", function($scope) {}), function() {
    Date.shortMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], 
    Date.longMonths = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ], 
    Date.shortDays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], Date.longDays = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    var replaceChars = {
        d: function() {
            return (this.getDate() < 10 ? "0" : "") + this.getDate();
        },
        D: function() {
            return Date.shortDays[this.getDay()];
        },
        j: function() {
            return this.getDate();
        },
        l: function() {
            return Date.longDays[this.getDay()];
        },
        N: function() {
            return 0 == this.getDay() ? 7 : this.getDay();
        },
        S: function() {
            return this.getDate() % 10 == 1 && 11 != this.getDate() ? "st" : this.getDate() % 10 == 2 && 12 != this.getDate() ? "nd" : this.getDate() % 10 == 3 && 13 != this.getDate() ? "rd" : "th";
        },
        w: function() {
            return this.getDay();
        },
        z: function() {
            var d = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((this - d) / 864e5);
        },
        W: function() {
            var target = new Date(this.valueOf()), dayNr = (this.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var firstThursday = target.valueOf();
            return target.setMonth(0, 1), 4 !== target.getDay() && target.setMonth(0, 1 + (4 - target.getDay() + 7) % 7), 
            1 + Math.ceil((firstThursday - target) / 6048e5);
        },
        F: function() {
            return Date.longMonths[this.getMonth()];
        },
        m: function() {
            return (this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1);
        },
        M: function() {
            return Date.shortMonths[this.getMonth()];
        },
        n: function() {
            return this.getMonth() + 1;
        },
        t: function() {
            var d = new Date();
            return new Date(d.getFullYear(), d.getMonth(), 0).getDate();
        },
        L: function() {
            var year = this.getFullYear();
            return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;
        },
        o: function() {
            var d = new Date(this.valueOf());
            return d.setDate(d.getDate() - (this.getDay() + 6) % 7 + 3), d.getFullYear();
        },
        Y: function() {
            return this.getFullYear();
        },
        y: function() {
            return ("" + this.getFullYear()).substr(2);
        },
        a: function() {
            return this.getHours() < 12 ? "am" : "pm";
        },
        A: function() {
            return this.getHours() < 12 ? "AM" : "PM";
        },
        B: function() {
            return Math.floor(1e3 * ((this.getUTCHours() + 1) % 24 + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) / 24);
        },
        g: function() {
            return this.getHours() % 12 || 12;
        },
        G: function() {
            return this.getHours();
        },
        h: function() {
            return ((this.getHours() % 12 || 12) < 10 ? "0" : "") + (this.getHours() % 12 || 12);
        },
        H: function() {
            return (this.getHours() < 10 ? "0" : "") + this.getHours();
        },
        i: function() {
            return (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
        },
        s: function() {
            return (this.getSeconds() < 10 ? "0" : "") + this.getSeconds();
        },
        u: function() {
            var m = this.getMilliseconds();
            return (10 > m ? "00" : 100 > m ? "0" : "") + m;
        },
        e: function() {
            return "Not Yet Supported";
        },
        I: function() {
            for (var DST = null, i = 0; 12 > i; ++i) {
                var d = new Date(this.getFullYear(), i, 1), offset = d.getTimezoneOffset();
                if (null === DST) DST = offset; else {
                    if (DST > offset) {
                        DST = offset;
                        break;
                    }
                    if (offset > DST) break;
                }
            }
            return this.getTimezoneOffset() == DST | 0;
        },
        O: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + "00";
        },
        P: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + ":00";
        },
        T: function() {
            return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
        },
        Z: function() {
            return 60 * -this.getTimezoneOffset();
        },
        c: function() {
            return this.format("Y-m-d\\TH:i:sP");
        },
        r: function() {
            return this.toString();
        },
        U: function() {
            return this.getTime() / 1e3;
        }
    };
    Date.prototype.format = function(format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function(_, esc, chr) {
            return "" === esc && replaceChars[chr] ? replaceChars[chr].call(date) : chr;
        });
    };
}.call(this), function() {
    "use strict";
    function FastClick(layer, options) {
        function bind(method, context) {
            return function() {
                return method.apply(context, arguments);
            };
        }
        var oldOnClick;
        if (options = options || {}, this.trackingClick = !1, this.trackingClickStart = 0, 
        this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, 
        this.touchBoundary = options.touchBoundary || 10, this.layer = layer, this.tapDelay = options.tapDelay || 200, 
        !FastClick.notNeeded(layer)) {
            for (var methods = [ "onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel" ], context = this, i = 0, l = methods.length; l > i; i++) context[methods[i]] = bind(context[methods[i]], context);
            deviceIsAndroid && (layer.addEventListener("mouseover", this.onMouse, !0), layer.addEventListener("mousedown", this.onMouse, !0), 
            layer.addEventListener("mouseup", this.onMouse, !0)), layer.addEventListener("click", this.onClick, !0), 
            layer.addEventListener("touchstart", this.onTouchStart, !1), layer.addEventListener("touchmove", this.onTouchMove, !1), 
            layer.addEventListener("touchend", this.onTouchEnd, !1), layer.addEventListener("touchcancel", this.onTouchCancel, !1), 
            Event.prototype.stopImmediatePropagation || (layer.removeEventListener = function(type, callback, capture) {
                var rmv = Node.prototype.removeEventListener;
                "click" === type ? rmv.call(layer, type, callback.hijacked || callback, capture) : rmv.call(layer, type, callback, capture);
            }, layer.addEventListener = function(type, callback, capture) {
                var adv = Node.prototype.addEventListener;
                "click" === type ? adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
                    event.propagationStopped || callback(event);
                }), capture) : adv.call(layer, type, callback, capture);
            }), "function" == typeof layer.onclick && (oldOnClick = layer.onclick, layer.addEventListener("click", function(event) {
                oldOnClick(event);
            }, !1), layer.onclick = null);
        }
    }
    var deviceIsAndroid = navigator.userAgent.indexOf("Android") > 0, deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent), deviceIsIOS4 = deviceIsIOS && /OS 4_\d(_\d)?/.test(navigator.userAgent), deviceIsIOSWithBadTarget = deviceIsIOS && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), deviceIsBlackBerry10 = navigator.userAgent.indexOf("BB10") > 0;
    FastClick.prototype.needsClick = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "button":
          case "select":
          case "textarea":
            if (target.disabled) return !0;
            break;

          case "input":
            if (deviceIsIOS && "file" === target.type || target.disabled) return !0;
            break;

          case "label":
          case "video":
            return !0;
        }
        return /\bneedsclick\b/.test(target.className);
    }, FastClick.prototype.needsFocus = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "textarea":
            return !0;

          case "select":
            return !deviceIsAndroid;

          case "input":
            switch (target.type) {
              case "button":
              case "checkbox":
              case "file":
              case "image":
              case "radio":
              case "submit":
                return !1;
            }
            return !target.disabled && !target.readOnly;

          default:
            return /\bneedsfocus\b/.test(target.className);
        }
    }, FastClick.prototype.sendClick = function(targetElement, event) {
        var clickEvent, touch;
        document.activeElement && document.activeElement !== targetElement && document.activeElement.blur(), 
        touch = event.changedTouches[0], clickEvent = document.createEvent("MouseEvents"), 
        clickEvent.initMouseEvent(this.determineEventType(targetElement), !0, !0, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, !1, !1, !1, !1, 0, null), 
        clickEvent.forwardedTouchEvent = !0, targetElement.dispatchEvent(clickEvent);
    }, FastClick.prototype.determineEventType = function(targetElement) {
        return deviceIsAndroid && "select" === targetElement.tagName.toLowerCase() ? "mousedown" : "click";
    }, FastClick.prototype.focus = function(targetElement) {
        var length;
        deviceIsIOS && targetElement.setSelectionRange && 0 !== targetElement.type.indexOf("date") && "time" !== targetElement.type && "month" !== targetElement.type ? (length = targetElement.value.length, 
        targetElement.setSelectionRange(length, length)) : targetElement.focus();
    }, FastClick.prototype.updateScrollParent = function(targetElement) {
        var scrollParent, parentElement;
        if (scrollParent = targetElement.fastClickScrollParent, !scrollParent || !scrollParent.contains(targetElement)) {
            parentElement = targetElement;
            do {
                if (parentElement.scrollHeight > parentElement.offsetHeight) {
                    scrollParent = parentElement, targetElement.fastClickScrollParent = parentElement;
                    break;
                }
                parentElement = parentElement.parentElement;
            } while (parentElement);
        }
        scrollParent && (scrollParent.fastClickLastScrollTop = scrollParent.scrollTop);
    }, FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
        return eventTarget.nodeType === Node.TEXT_NODE ? eventTarget.parentNode : eventTarget;
    }, FastClick.prototype.onTouchStart = function(event) {
        var targetElement, touch, selection;
        if (event.targetTouches.length > 1) return !0;
        if (targetElement = this.getTargetElementFromEventTarget(event.target), touch = event.targetTouches[0], 
        deviceIsIOS) {
            if (selection = window.getSelection(), selection.rangeCount && !selection.isCollapsed) return !0;
            if (!deviceIsIOS4) {
                if (touch.identifier && touch.identifier === this.lastTouchIdentifier) return event.preventDefault(), 
                !1;
                this.lastTouchIdentifier = touch.identifier, this.updateScrollParent(targetElement);
            }
        }
        return this.trackingClick = !0, this.trackingClickStart = event.timeStamp, this.targetElement = targetElement, 
        this.touchStartX = touch.pageX, this.touchStartY = touch.pageY, event.timeStamp - this.lastClickTime < this.tapDelay && event.preventDefault(), 
        !0;
    }, FastClick.prototype.touchHasMoved = function(event) {
        var touch = event.changedTouches[0], boundary = this.touchBoundary;
        return Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary ? !0 : !1;
    }, FastClick.prototype.onTouchMove = function(event) {
        return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) && (this.trackingClick = !1, 
        this.targetElement = null), !0) : !0;
    }, FastClick.prototype.findControl = function(labelElement) {
        return void 0 !== labelElement.control ? labelElement.control : labelElement.htmlFor ? document.getElementById(labelElement.htmlFor) : labelElement.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea");
    }, FastClick.prototype.onTouchEnd = function(event) {
        var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
        if (!this.trackingClick) return !0;
        if (event.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, 
        !0;
        if (this.cancelNextClick = !1, this.lastClickTime = event.timeStamp, trackingClickStart = this.trackingClickStart, 
        this.trackingClick = !1, this.trackingClickStart = 0, deviceIsIOSWithBadTarget && (touch = event.changedTouches[0], 
        targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement, 
        targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent), 
        targetTagName = targetElement.tagName.toLowerCase(), "label" === targetTagName) {
            if (forElement = this.findControl(targetElement)) {
                if (this.focus(targetElement), deviceIsAndroid) return !1;
                targetElement = forElement;
            }
        } else if (this.needsFocus(targetElement)) return event.timeStamp - trackingClickStart > 100 || deviceIsIOS && window.top !== window && "input" === targetTagName ? (this.targetElement = null, 
        !1) : (this.focus(targetElement), this.sendClick(targetElement, event), deviceIsIOS && "select" === targetTagName || (this.targetElement = null, 
        event.preventDefault()), !1);
        return deviceIsIOS && !deviceIsIOS4 && (scrollParent = targetElement.fastClickScrollParent, 
        scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) ? !0 : (this.needsClick(targetElement) || (event.preventDefault(), 
        this.sendClick(targetElement, event)), !1);
    }, FastClick.prototype.onTouchCancel = function() {
        this.trackingClick = !1, this.targetElement = null;
    }, FastClick.prototype.onMouse = function(event) {
        return this.targetElement ? event.forwardedTouchEvent ? !0 : event.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (event.stopImmediatePropagation ? event.stopImmediatePropagation() : event.propagationStopped = !0, 
        event.stopPropagation(), event.preventDefault(), !1) : !0 : !0;
    }, FastClick.prototype.onClick = function(event) {
        var permitted;
        return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, 
        !0) : "submit" === event.target.type && 0 === event.detail ? !0 : (permitted = this.onMouse(event), 
        permitted || (this.targetElement = null), permitted);
    }, FastClick.prototype.destroy = function() {
        var layer = this.layer;
        deviceIsAndroid && (layer.removeEventListener("mouseover", this.onMouse, !0), layer.removeEventListener("mousedown", this.onMouse, !0), 
        layer.removeEventListener("mouseup", this.onMouse, !0)), layer.removeEventListener("click", this.onClick, !0), 
        layer.removeEventListener("touchstart", this.onTouchStart, !1), layer.removeEventListener("touchmove", this.onTouchMove, !1), 
        layer.removeEventListener("touchend", this.onTouchEnd, !1), layer.removeEventListener("touchcancel", this.onTouchCancel, !1);
    }, FastClick.notNeeded = function(layer) {
        var metaViewport, chromeVersion, blackberryVersion;
        if ("undefined" == typeof window.ontouchstart) return !0;
        if (chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [ , 0 ])[1]) {
            if (!deviceIsAndroid) return !0;
            if (metaViewport = document.querySelector("meta[name=viewport]")) {
                if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
                if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0;
            }
        }
        if (deviceIsBlackBerry10 && (blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), 
        blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3 && (metaViewport = document.querySelector("meta[name=viewport]")))) {
            if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
            if (document.documentElement.scrollWidth <= window.outerWidth) return !0;
        }
        return "none" === layer.style.msTouchAction ? !0 : !1;
    }, FastClick.attach = function(layer, options) {
        return new FastClick(layer, options);
    }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
        return FastClick;
    }) : "undefined" != typeof module && module.exports ? (module.exports = FastClick.attach, 
    module.exports.FastClick = FastClick) : window.FastClick = FastClick;
}(), app.controller("ContentDeleteController", function($scope, type, definition, $state, FluroContent, Notifications, CacheManager, item) {
    function deleteSuccess(data) {
        var title = data.title;
        data.name && (title = data.name), Notifications.status(title + " was deleted successfully"), 
        data.definition && data.definition != data._type ? (CacheManager.clear(data.definition), 
        $state.go(type.path + ".custom", {
            definition: data.definition
        })) : (CacheManager.clear(type.path), $state.go(type.path));
    }
    function deleteFailed(data) {
        var title = $scope.item.title;
        $scope.item.name && (title = $scope.item.name), Notifications.error("Failed to delete " + title);
    }
    $scope.item = item, $scope.type = type, $scope.definition = definition, $scope.titleSingular = type.singular, 
    $scope.titlePlural = type.plural, $scope.cancelPath = type.path, definition && ($scope.titleSingular = definition.title, 
    $scope.titlePlural = definition.plural, $scope.cancelPath = type.path + ".custom({definition:'" + definition.definitionName + "'})"), 
    $scope["delete"] = function() {
        var definedName = type.path;
        definition && definition.definitionName && (definedName = definition.definitionName), 
        $scope.item._id && FluroContent.resource(definedName)["delete"]({
            id: $scope.item._id
        }, deleteSuccess, deleteFailed);
    };
}), app.controller("ContentFormController", function($rootScope, Fluro, ModalService, $scope, FluroStorage, extras, FluroSocket, $rootScope, $state, $controller, Asset, Notifications, CacheManager, TypeService, FluroContent, type, definition, item) {
    function socketUpdate(data) {
        if (socketEnabled) {
            var sameId = data.item._id == $scope.item._id, newVersion = data.item.updated != item.updated;
            console.log("Socket change", item.title, sameId, data.item.updated, item.updated), 
            sameId && newVersion && (console.log("update this data ", data.item._id, $scope.item._id, data.item.title), 
            socketEnabled = !1, _.assign($scope.item, data.item), socketEnabled = !0, $rootScope.user._id != data.user._id ? Notifications.warning("This content was just updated by " + data.user.name) : Notifications.status("You updated this in another window"));
        }
    }
    function keyDown(event) {
        if (event.ctrlKey || event.metaKey) switch (String.fromCharCode(event.which).toLowerCase()) {
          case "s":
            event.preventDefault(), event.stopPropagation(), $scope.save();
        }
    }
    $scope.item = item, $scope.extras = extras, $scope.item._id ? ($scope.saveText = "Save", 
    $scope.cancelText = "Close") : ($scope.saveText = "Create", $scope.cancelText = "Cancel"), 
    $scope.defaultSaveOptions = {}, $scope.type = type, $scope.definition = definition, 
    $scope.template = "fluro-admin-content/types/" + type.path + "/form.html", $scope.titleSingular = type.singular, 
    $scope.titlePlural = type.plural, definition && definition.definitionName ? ($scope.titleSingular = definition.title, 
    $scope.titlePlural = definition.plural, $scope.definedType = definition.definitionName, 
    $scope.icon = definition.parentType) : ($scope.definedType = type.path, $scope.icon = type.path), 
    $scope.titleLabel = "Title", $scope.bodyLabel = "Body", definition && definition.definitionName && definition.data && (definition.data.titleLabel && definition.data.titleLabel.length && ($scope.titleLabel = definition.data.titleLabel), 
    definition.data.bodyLabel && definition.data.bodyLabel.length && ($scope.bodyLabel = definition.data.bodyLabel));
    var session;
    $scope.item._id && (session = FluroStorage.sessionStorage("form-" + $scope.item._id)), 
    $scope.openAPIDocument = function() {
        var url = Fluro.apiURL + "/content/" + $scope.definedType + "/" + $scope.item._id + "?context=edit";
        Fluro.token && (url += "&access_token=" + Fluro.token), window.open(url);
    };
    var socketEnabled = !0;
    FluroSocket.on("content.edit", socketUpdate), $scope.useInlineTitle = "article" == type.path, 
    $scope.getAssetUrl = Asset.getUrl, $scope.getDownloadUrl = Asset.downloadUrl, $scope.cleanupPastedHTML = $rootScope.cleanupPastedHTML, 
    $(document).on("keydown", keyDown), $scope.$on("$destroy", function() {
        $(document).off("keydown", keyDown), FluroSocket.off("content", socketUpdate);
    }), $scope.save = function(options) {
        socketEnabled = !1, console.log("Disable socket"), options || (options = $scope.defaultSaveOptions), 
        options.status && (status = options.status), options.exitOnSave && (saveSuccessCallback = exitAfterSave), 
        options.forceVersioning && (!$scope.item.version || $scope.item.version.length) && (console.log("Forcing a version save"), 
        $scope.item.version = "Changes made by " + $rootScope.user.name), console.log("Save item", $scope.item.applicationIcon, $scope.item.icon), 
        $scope.item._id ? (console.log("Push item", $scope.item), FluroContent.resource($scope.definedType).update({
            id: $scope.item._id
        }, $scope.item, $scope.saveSuccess, $scope.saveFail)) : FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessExit, $scope.saveFail);
    }, $scope.saveSuccess = function(result) {
        return CacheManager.clear($scope.definedType), result._id && ($scope.item.__v = result.__v), 
        "definition" == result._type && TypeService.sideLoadDefinition(result), result.name ? Notifications.status(result.name + " saved successfully") : Notifications.status(result.title + " saved successfully"), 
        socketEnabled = !0, $scope.closeOnSave ? (console.log("Close on save"), $scope.$close && $scope.$close(result), 
        definition ? $state.go(type.path + ".custom", {
            definition: definition.definitionName
        }) : $state.go(type.path), !0) : $scope.saveFinished(result);
    }, $scope.saveFinished = function(result) {
        return $scope.$close ? $scope.$close(result) : !1;
    }, $scope.saveSuccessExit = function(result) {
        var res = $scope.saveSuccess(result);
        res || (definition ? $state.go(type.path + ".custom", {
            definition: definition.definitionName
        }) : $state.go(type.path));
    }, $scope.saveFail = function(result) {
        return result && result.data ? result.data.errors ? void _.each(result.data.errors, function(err) {
            _.isObject(err) ? Notifications.error(err.message) : Notifications.error(err);
        }) : result.data ? Notifications.error(result.data) : Notifications.error("Failed to save document") : Notifications.error("Failed to save");
    }, $scope.cancel = function() {
        return $scope.$dismiss ? $scope.$dismiss() : $scope.item._id == $rootScope.user._id ? $state.go("home") : void (definition && definition.definitionName ? $state.go(type.path + ".custom", {
            definition: definition.definitionName
        }) : $state.go(type.path));
    };
    var realmSelectEnabled = !0, tagSelectEnabled = !0;
    switch ($scope.type.path) {
      case "realm":
      case "account":
        realmSelectEnabled = !1, tagSelectEnabled = !1;
        break;

      case "role":
        tagSelectEnabled = !1;
        break;

      case "tag":
        tagSelectEnabled = !1;
        break;

      case "event":
        $controller("EventFormController", {
            $scope: $scope
        });
        break;

      case "integration":
        $controller("IntegrationFormController", {
            $scope: $scope
        });
        break;

      case "application":
        $controller("ApplicationFormController", {
            $scope: $scope
        });
        break;

      case "user":
        tagSelectEnabled = !1, $controller("UserFormController", {
            $scope: $scope
        });
        break;

      case "persona":
        tagSelectEnabled = !1, $controller("PersonaFormController", {
            $scope: $scope
        });
        break;

      case "contactdetail":
        tagSelectEnabled = !1, $controller("ContactDetailFormController", {
            $scope: $scope
        });
        break;

      case "family":
        $controller("FamilyFormController", {
            $scope: $scope
        });
        break;

      case "query":
        $controller("QueryFormController", {
            $scope: $scope
        });
        break;

      case "purchase":
        $controller("PurchaseFormController", {
            $scope: $scope
        });
        break;

      case "checkin":
        $controller("CheckinFormController", {
            $scope: $scope
        });
        break;

      case "asset":
      case "video":
      case "audio":
      case "image":
        $controller("AssetFormController", {
            $scope: $scope
        });
    }
    $scope.realmSelectEnabled = realmSelectEnabled, $scope.tagSelectEnabled = tagSelectEnabled;
}), app.controller("ContentListController", function($scope, $filter, extras, Notifications, CacheManager, FluroSocket, $rootScope, $controller, $state, $filter, $compile, Fluro, FluroAccess, ModalService, Selection, FluroStorage, Tools, items, definition, definitions, type) {
    function socketUpdate(data) {
        data.item._type == type.path && (data.item.definition ? CacheManager.clear(data.item.definition) : CacheManager.clear(type.path), 
        Notifications.warning("Some of this content was just updated by " + data.user.name));
    }
    switch ($scope.items = items, $scope.definitions = definitions, $scope.type = type, 
    $scope.definition = definition, $scope.extras = extras, $scope.templates = _.filter(items, function(item) {
        return "template" == item.status;
    }), FluroSocket.on("content", socketUpdate), $scope.$on("$destroy", function() {
        FluroSocket.off("content", socketUpdate);
    }), $scope.updatedTooltip = function(item) {
        return item.updatedBy ? "By " + item.updatedBy + " " + $filter("timeago")(item.updated) : $filter("timeago")(item.updated);
    }, $scope.titleSingular = type.singular, $scope.titlePlural = type.plural, $scope.filtersActive = function() {
        return _.keys($scope.search.filters).length;
    }, type.path) {
      case "event":
        $scope.template = "fluro-admin-content/types/" + type.path + "/list.html";
        break;

      case "account":
        $scope.template = "fluro-admin-content/types/" + type.path + "/list.html";
        break;

      default:
        $scope.template = "fluro-admin-content/types/list.html";
    }
    var local, session;
    switch (definition ? ($scope.titleSingular = definition.title, $scope.titlePlural = definition.plural, 
    $scope.definedType = definition.definitionName, local = FluroStorage.localStorage(definition.definitionName), 
    session = FluroStorage.sessionStorage(definition.definitionName)) : (local = FluroStorage.localStorage(type.path), 
    session = FluroStorage.sessionStorage(type.path), $scope.definedType = type.path), 
    session.search ? $scope.search = session.search : $scope.search = session.search = {
        filters: {
            status: [ "active" ]
        },
        reverse: !1
    }, local.settings ? $scope.settings = local.settings : $scope.settings = local.settings = {}, 
    $scope.type.path) {
      case "asset":
      case "image":
      case "video":
      case "audio":
        $scope.uploadType = !0, $scope.uploadName = $scope.type.path, definition && ($scope.uploadName = definition.definitionName);
    }
    switch ($scope.currentViewMode = function(viewMode) {
        return "default" == viewMode ? !$scope.settings.viewMode || "" == $scope.settings.viewMode || $scope.settings.viewMode == viewMode : $scope.settings.viewMode == viewMode;
    }, $scope.setOrder = function(string) {
        $scope.search.order = string, $scope.updateFilters();
    }, $scope.setReverse = function(bol) {
        $scope.search.reverse = bol, $scope.updateFilters();
    }, session.selection || (session.selection = []), session.pager ? $scope.pager = session.pager : $scope.pager = session.pager = {
        limit: 20,
        maxSize: 8
    }, $scope.$watch("search", function() {
        $scope.updateFilters();
    }, !0), $scope.canSelect = function(item) {
        return !0;
    }, $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect), 
    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }, $scope.deselectFiltered = function() {
        $scope.selection.deselectMultiple($scope.filteredItems);
    }, $scope.togglePage = function() {
        $scope.pageIsSelected() ? $scope.deselectPage() : $scope.selectPage();
    }, $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }, $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }, $scope.viewInModal = function(item) {
        ModalService.view(item);
    }, $scope.editInModal = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {});
    }, $scope.createInModal = function() {
        ModalService.create($scope.type, function(result) {
            $state.reload();
        }, function(result) {});
    }, $scope.createFromTemplate = function(template) {
        ModalService.edit(template, function(result) {
            $state.reload();
        }, function(result) {}, !1, !0, $scope);
    }, $scope.copyItem = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {}, !0);
    }, $scope.canCreate = function() {
        var c = FluroAccess.can("create", type.path);
        return definition && (c = FluroAccess.can("create", definition.definitionName)), 
        c;
    }, $scope.canCopy = function(item) {
        var typeName = $scope.type.path;
        return item.definition && (typeName = item.definition), FluroAccess.can("create", typeName);
    }, $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, "user" == type.path);
    }, $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item, "user" == type.path);
    }, $scope.updateFilters = function() {
        if ($scope.items) {
            var filteredItems = $scope.items;
            filteredItems = $filter("filter")(filteredItems, $scope.search.terms), filteredItems = $filter("orderBy")(filteredItems, $scope.search.order, $scope.search.reverse), 
            $scope.search.filters && _.forOwn($scope.search.filters, function(value, key) {
                _.isArray(value) ? _.forOwn(value, function(value) {
                    filteredItems = $filter("reference")(filteredItems, key, value);
                }) : filteredItems = $filter("reference")(filteredItems, key, value);
            }), $scope.filteredItems = filteredItems, $scope.updatePage();
        }
    }, $scope.updatePage = function() {
        $scope.filteredItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 1);
        var pageItems = $filter("startFrom")($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter("limitTo")(pageItems, $scope.pager.limit), $scope.pageItems = pageItems;
    }, type.path) {
      case "event":
        $controller("EventListController", {
            $scope: $scope
        });
        break;

      case "user":
        $controller("UserListController", {
            $scope: $scope
        });
        break;

      case "persona":
        $controller("PersonaListController", {
            $scope: $scope
        });
        break;

      case "account":
        $controller("AccountListController", {
            $scope: $scope
        });
    }
    $scope.updateFilters();
}), app.controller("ContentTrashController", function($scope, $rootScope, $filter, $state, FluroContent, Notifications, CacheManager, Batch, FluroStorage, FluroAccess, Selection, items, definition, definitions, type) {
    $scope.items = items, $scope.definitions = definitions, $scope.type = type, $scope.definition = definition, 
    $scope.titleSingular = type.singular, $scope.titlePlural = type.plural;
    var local, session;
    definition ? ($scope.titleSingular = definition.title, $scope.titlePlural = definition.plural, 
    local = FluroStorage.localStorage(definition.definitionName + "-trash"), session = FluroStorage.sessionStorage(definition.definitionName + "-trash")) : (local = FluroStorage.localStorage(type.path + "-trash"), 
    session = FluroStorage.sessionStorage(type.path + "-trash")), $scope.template = "fluro-admin-content/types/trash.html", 
    session.search ? ($scope.search = session.search, $scope.search.filters.status && delete $scope.search.filters.status) : $scope.search = session.search = {
        filters: {},
        reverse: !1
    }, local.settings ? $scope.settings = local.settings : $scope.settings = local.settings = {}, 
    console.log($scope, "SCOPE"), $scope.setOrder = function(string) {
        $scope.search.order = string, $scope.updateFilters();
    }, $scope.setReverse = function(bol) {
        $scope.search.reverse = bol, $scope.updateFilters();
    }, session.selection || (session.selection = []), session.pager ? $scope.pager = session.pager : $scope.pager = session.pager = {
        limit: 20,
        maxSize: 8
    }, $scope.$watch("search", function() {
        $scope.updateFilters();
    }, !0), $scope.canSelect = function(item) {
        return !0;
    }, $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect), 
    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }, $scope.togglePage = function() {
        $scope.pageIsSelected() ? $scope.deselectPage() : $scope.selectPage();
    }, $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }, $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }, $scope.canDestroy = function(item) {
        if (!item) return !1;
        var canDestroyAny = FluroAccess.can("destroy any", type.path), canDestroyOwn = FluroAccess.can("destroy own", type.path);
        if (definition && (canDestroyAny = FluroAccess.can("destroy any", definition.definitionName), 
        canDestroyOwn = FluroAccess.can("destroy own", definition.definitionName)), canDestroyAny) return !0;
        if (item.author) {
            var authorID;
            if (authorID = _.isObject(item.author) ? item.author._id : item.author, authorID == $rootScope.user._id) return canDestroyOwn;
        }
    }, $scope.canRestore = function(item) {
        if (!item) return !1;
        var canRestoreAny = FluroAccess.can("restore any", type.path), canRestoreOwn = FluroAccess.can("restore own", type.path);
        if (definition && (canRestoreAny = FluroAccess.can("restore any", definition.definitionName), 
        canRestoreOwn = FluroAccess.can("restore own", definition.definitionName)), canRestoreAny) return !0;
        if (item.author) {
            var authorID;
            if (authorID = _.isObject(item.author) ? item.author._id : item.author, authorID == $rootScope.user._id) return canRestoreOwn;
        }
    }, $scope.updateFilters = function() {
        if ($scope.items) {
            var filteredItems = $scope.items;
            filteredItems = $filter("filter")(filteredItems, $scope.search.terms), filteredItems = $filter("orderBy")(filteredItems, $scope.search.order, $scope.search.reverse), 
            $scope.search.filters && _.forOwn($scope.search.filters, function(value, key) {
                _.isArray(value) ? _.forOwn(value, function(value) {
                    filteredItems = $filter("reference")(filteredItems, key, value);
                }) : filteredItems = $filter("reference")(filteredItems, key, value);
            }), $scope.filteredItems = filteredItems, $scope.updatePage();
        }
    }, $scope.updatePage = function() {
        $scope.filteredItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 1);
        var pageItems = $filter("startFrom")($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter("limitTo")(pageItems, $scope.pager.limit), $scope.pageItems = pageItems;
    }, $scope.done = function(data) {
        var caches = _.chain(data.results).map(function(result) {
            return [ result.definition, result._type ];
        }).flatten().value();
        $scope.definition && (console.log("GET THE DEFINITION"), caches.push($scope.definition.definitionName), 
        caches.push($scope.definition.definitionName + "-trash")), caches.push($scope.type.path), 
        caches.push($scope.type.path + "-trash"), console.log("Caches to clear", caches), 
        caches = _.uniq(caches), caches = _.compact(caches), _.each(caches, function(t) {
            console.log("Clear cache", t), CacheManager.clear(t);
        }), $state.reload(), $scope.selection.deselectAll();
    }, $scope.restore = function(item) {
        FluroContent.endpoint("content/" + $scope.type.path + "/" + item._id + "/restore").get().$promise.then(function(res) {
            Notifications.status("Restored " + res.title), $scope.done(res);
        });
    }, $scope.destroy = function(item) {
        FluroContent.resource($scope.type.path)["delete"]({
            id: item._id,
            destroy: !0
        }, function(res) {
            Notifications.status("Destroyed " + res.title), $scope.done(res);
        });
    }, $scope.updateFilters();
}), app.service("ContentVersionService", function(FluroContent) {
    var controller = {};
    return controller.list = function(item) {
        var id = item._id, type = item._type;
        return item.definition && (type = item.definition), FluroContent.endpoint("content/" + type + "/" + id + "/versions").query();
    }, controller;
}), app.controller("ContentViewController", function($scope, extras, $rootScope, $controller, FluroSocket, Notifications, ModalService, $state, FluroAccess, type, definition, item) {
    $scope.item = item, $scope.type = type, $scope.definition = definition, $scope.extras = extras, 
    $scope.template = "fluro-admin-content/types/" + type.path + "/view.html", $scope.titleSingular = type.singular, 
    $scope.titlePlural = type.plural, definition ? ($scope.titleSingular = definition.title, 
    $scope.titlePlural = definition.plural, $scope.definedType = definition.definitionName) : $scope.definedType = type.path, 
    $scope.canEdit = FluroAccess.canEditItem;
    var socketEnabled = !0;
    switch (FluroSocket.on("content", function(data) {
        socketEnabled && data.item._id == item._id && (Notifications.warning("Changes to this " + $scope.titleSingular + " were just made by " + data.user.name), 
        angular.equals(data.item, item) ? console.log("No change") : (socketEnabled = !1, 
        $scope.item = data.item, socketEnabled = !0, Notifications.warning("This content was just updated by " + data.user.name)));
    }), $scope.editItem = function() {
        $scope.$dismiss ? (ModalService.edit(item), $scope.$dismiss()) : $state.go($scope.item._type + ".edit", {
            id: $scope.item._id,
            definitionName: $scope.definedType
        });
    }, $scope.cancel = function() {
        return $scope.$dismiss ? $scope.$dismiss() : $scope.item._id == $rootScope.user._id ? $state.go("home") : void (definition && definition.definitionName ? $state.go(type.path + ".custom", {
            definition: definition.definitionName
        }) : $state.go(type.path));
    }, $scope.item._type) {
      case "contact":
        $controller("ContactViewController", {
            $scope: $scope
        });
    }
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "reference-select",
        templateUrl: "fluro-admin-content/content-formly/reference-select.html",
        controller: function($scope) {
            var definition = $scope.to.definition;
            definition.params || (definition.params = {}), $scope.config = {
                canCreate: !0,
                minimum: definition.minimum,
                maximum: definition.maximum,
                type: definition.params.restrictType,
                searchInheritable: definition.params.searchInheritable
            };
        },
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "date-select",
        templateUrl: "fluro-admin-content/content-formly/date-select.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "object-editor",
        templateUrl: "fluro-admin-content/content-formly/object-editor.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "custom",
        templateUrl: "fluro-admin-content/content-formly/unknown.html",
        wrapper: [ "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "terms",
        templateUrl: "fluro-admin-content/content-formly/terms.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "code",
        templateUrl: "fluro-admin-content/content-formly/code.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
        controller: function($scope) {
            var aceEditor;
            $scope.aceLoaded = function(_editor) {
                aceEditor = _editor;
                var syntaxName;
                $scope.to.definition.params && $scope.to.definition.params.syntax && (syntaxName = syntaxName = $scope.to.definition.params.syntax), 
                aceEditor && syntaxName && aceEditor.getSession().setMode("ace/mode/" + syntaxName);
            };
        }
    }), formlyConfig.setType({
        name: "wysiwyg",
        templateUrl: "fluro-admin-content/content-formly/wysiwyg.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "nested",
        templateUrl: "fluro-admin-content/content-formly/nested.html",
        controller: function($scope) {
            $scope.$watch("model[options.key]", function(keyModel) {
                keyModel || ($scope.model[$scope.options.key] = []);
            }), $scope.settings = {};
            var def = $scope.to.definition, minimum = def.minimum, maximum = def.maximum, askCount = def.askCount;
            minimum || (minimum = 0), maximum || (maximum = 0), askCount || (askCount = 0), 
            minimum && minimum > askCount && (askCount = minimum), maximum && askCount > maximum && (askCount = maximum), 
            1 == maximum && 1 == minimum && $scope.options.key || askCount && !$scope.model[$scope.options.key].length && _.times(askCount, function() {
                $scope.model[$scope.options.key].push({});
            }), $scope.firstLine = function(item) {
                var keys = _.chain(item).keys().without("active").filter(function(key) {
                    var i = key.indexOf("$$");
                    return -1 == i ? !0 : !1;
                }).slice(0, 3).value();
                return _.at(item, keys).join(",").slice(0, 150);
            }, $scope.items = function() {
                return $scope.model[$scope.options.key];
            }, $scope.canRemove = function() {
                return minimum ? $scope.model[$scope.options.key].length > minimum ? !0 : void 0 : !0;
            }, $scope.remove = function(entry) {
                console.log("Pull Entry", entry, $scope.model[$scope.options.key].indexOf(entry)), 
                _.pull($scope.model[$scope.options.key], entry);
            }, $scope.canAdd = function() {
                return maximum ? $scope.model[$scope.options.key].length < maximum ? !0 : void 0 : !0;
            }, 1 != maximum && $scope.model && $scope.model[$scope.options.key] && $scope.model[$scope.options.key].length && ($scope.settings.active = $scope.model[$scope.options.key][0]);
        }
    }), formlyConfig.setType({
        name: "list-select",
        templateUrl: "fluro-admin-content/content-formly/list-select.html",
        controller: function($scope, FluroValidate) {
            function checkValidity() {
                var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
                $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
                $scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput);
            }
            function setModel() {
                $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
                $scope.fc.$setTouched(), checkValidity();
            }
            var definition = $scope.to.definition, maximum = (definition.minimum, definition.maximum);
            $scope.multiple = 1 != maximum;
            var opts = ($scope.to, $scope.options);
            if ($scope.selection = {
                values: [],
                value: null
            }, $scope.contains = function(value) {
                return $scope.multiple ? _.contains($scope.selection.values, value) : $scope.selection.value == value;
            }, $scope.select = function(value) {
                $scope.multiple ? $scope.selection.values.push(value) : $scope.selection.value = value;
            }, $scope.deselect = function(value) {
                $scope.multiple ? _.pull($scope.selection.values, value) : $scope.selection.value = null;
            }, $scope.toggle = function(reference) {
                $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference), 
                setModel();
            }, $scope.$watch("model", function(newModelValue) {
                var modelValue;
                _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : modelValue && ($scope.selection.values = [ angular.copy(modelValue) ]) : modelValue && ($scope.selection.value = angular.copy(modelValue)));
            }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
                return $scope.to.required;
            }, function(newValue) {
                checkValidity();
            }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
                newValue && (checkValidity(), unwatchFormControl());
            });
        },
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.directive("contentFormly", function(FluroContent, FluroContentRetrieval, FluroValidate) {
    return {
        restrict: "E",
        transclude: !0,
        scope: {
            item: "=ngModel",
            definition: "=",
            agreements: "=?"
        },
        templateUrl: "fluro-admin-content/content-formly/form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $scope.item || ($scope.item = {}), $scope.$watch("item", function(item) {
                function addFieldDefinition(array, fieldDefinition) {
                    var newField = {};
                    newField.key = fieldDefinition.key, fieldDefinition.className && !fieldDefinition.asObject && (newField.className = fieldDefinition.className);
                    var templateOptions = {};
                    switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                    templateOptions.description = fieldDefinition.description, templateOptions.definition = fieldDefinition, 
                    fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                    templateOptions.required = fieldDefinition.minimum > 0, fieldDefinition.directive) {
                      case "value-select":
                      case "button-select":
                      case "order-select":
                        newField.type = "list-select";
                        break;

                      default:
                        newField.type = fieldDefinition.directive;
                    }
                    switch (fieldDefinition.type) {
                      case "reference":
                        "select" == fieldDefinition.directive && 1 == fieldDefinition.maximum ? $scope.vm.model[fieldDefinition.key] && $scope.vm.model[fieldDefinition.key]._id && ($scope.vm.model[fieldDefinition.key] = $scope.vm.model[fieldDefinition.key]._id) : newField.type = "reference-select";
                        break;

                      case "boolean":
                        fieldDefinition.params && fieldDefinition.params.storeData ? (newField.type = "terms", 
                        newField.data = {
                            agreements: $scope.agreements
                        }) : newField.type = "checkbox";
                        break;

                      case "object":
                        newField.type = "object-editor";
                        break;

                      case "date":
                        newField.type = "date-select";
                    }
                    if ("reference" == fieldDefinition.type) if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else fieldDefinition.params || (fieldDefinition.params = {}), FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    }); else fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                    if ("input" == fieldDefinition.directive) switch (fieldDefinition.type) {
                      case "boolean":
                        newField.type = "checkbox";
                    }
                    if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("reference-select" == newField.type ? newField.defaultValue = fieldDefinition.defaultReferences[0] : newField.defaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && (newField.defaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "reference-select" == newField.type ? newField.defaultValue = fieldDefinition.defaultReferences : newField.defaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                        return ref._id;
                    }) : newField.defaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && (newField.defaultValue = fieldDefinition.defaultValues), 
                    newField.templateOptions = templateOptions, newField.validators = {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue, valid = FluroValidate.validate(value, fieldDefinition);
                            return valid;
                        }
                    }, "embedded" != fieldDefinition.directive && fieldDefinition.fields && fieldDefinition.fields.length) {
                        var fieldContainer;
                        fieldDefinition.asObject ? (newField.type = "nested", newField.defaultValue = [], 
                        fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum && (newField.defaultValue = {}), 
                        newField.data = {
                            fields: []
                        }, fieldContainer = newField.data.fields, newField.extras = {
                            skipNgModelAttrsManipulator: !0
                        }, fieldContainer = newField.data.fields) : (newField = {
                            fieldGroup: [],
                            className: fieldDefinition.className
                        }, fieldContainer = newField.fieldGroup), _.each(fieldDefinition.fields, function(sub) {
                            addFieldDefinition(fieldContainer, sub);
                        });
                    }
                    fieldDefinition.hideExpression && (newField.hideExpression = fieldDefinition.hideExpression), 
                    "_contact" != newField.key && "value" != newField.type && array.push(newField);
                }
                $scope.vm.model = item, $scope.vm.model = $scope.item, $scope.vm.fields = [], $scope.vm.state = "ready", 
                $scope.definition && _.each($scope.definition.fields, function(fieldDefinition) {
                    addFieldDefinition($scope.vm.fields, fieldDefinition);
                });
            });
        }
    };
}), app.controller("AccountListController", function($scope, $rootScope, CacheManager, TypeService, Notifications, Fluro, FluroTokenService, FluroContent) {
    function updateSuccess(data) {
        $rootScope.user = data, Notifications.status("Your account list has been updated"), 
        console.log("Update success", data);
    }
    function loginSuccess(data) {
        FluroTokenService.deleteSession(), CacheManager.clearAll(), $rootScope.user = data, 
        Notifications.status("Logged in to " + data.account.title);
    }
    function updateFailed(data) {
        Notifications.error("Failed to update account information"), console.log("Update failed", data);
    }
    $scope.isActiveAccount = function(id) {
        return !0;
    }, $scope.isMyAccount = function(_id) {
        return $rootScope.user.account._id == _id;
    }, $scope.loginToAccount = function(id, asSession) {
        function success(res) {
            console.log("success now clear caches"), CacheManager.clearAll(), TypeService.refreshDefinedTypes(), 
            Notifications.status("Switched session to " + res.data.account.title);
        }
        function fail(res) {
            Notifications.error("Error creating token session");
        }
        if (console.log("Login to account", asSession), asSession) {
            Fluro.sessionStorage = !0;
            var request = FluroTokenService.getTokenForAccount(id);
            request.then(success, fail);
        } else FluroContent.endpoint("auth/switch/" + id).save({}).$promise.then(loginSuccess, updateFailed);
    }, $scope.toggleAccount = function(id) {
        if ($rootScope.user) {
            if (id == $rootScope.user.account._id) return void Notifications.error("Can not deactivate active account");
            var user = angular.copy($rootScope.user), list = user.availableAccounts, found = _.find(list, {
                _id: id
            });
            found ? _.pull(list, found) : list.push(id), FluroContent.resource("user").update({
                id: user._id
            }, {
                availableAccounts: _.uniq(list)
            }, updateSuccess, updateFailed);
        }
    };
}), app.controller("ApplicationFormController", function($scope, $rootScope, FluroAccess, Notifications) {
    $scope.timezones = moment.tz.names();
}), app.controller("AssetFormController", function($scope, Asset, Fluro, $state, $stateParams, Notifications, FileUploader) {
    function refreshAfterUpload(response) {
        response._id == $scope.item._id && ($scope.item = response);
    }
    "image" == $scope.type.path && ($scope.item.width || ($scope.needsWidth = !0), $scope.item.height || ($scope.needsHeight = !0)), 
    $scope.uploader = new FileUploader({
        url: Fluro.apiURL + "/file/replace",
        withCredentials: !0,
        formData: {}
    });
    $scope.saveAndUpload = function() {
        return console.log("Save and Upload"), $scope.item && $scope.item.realms && $scope.item.realms.length ? void $scope.uploader.uploadAll() : Notifications.warning("Please select a realm before attempting to upload");
    }, $scope.item.assetType || "video" == $scope.type.path || "audio" == $scope.type.path || ($scope.item.assetType = "upload"), 
    $scope.getImageURL = function() {
        return Asset.getUrl($scope.item._id) + "?w=600&v=" + $scope.item.updated;
    }, $scope.$watch("item._id + item.assetType", function() {
        $scope.item._id || "upload" != $scope.item.assetType ? $scope.uploadSave = !1 : $scope.uploadSave = !0;
    }), $scope.uploader.onBeforeUploadItem = function(item) {
        var details = {};
        $scope.item._id ? details._id = $scope.item._id : ($scope.definition && ($scope.item.definition = $scope.definition.definitionName), 
        $scope.item.type = $scope.type.path, details.json = angular.toJson($scope.item)), 
        console.log("Send details", details), item.formData = [ details ];
    }, $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
        _.each(response.errors, function(err) {
            err.message ? Notifications.error(err.message) : Notifications.error(err);
        }), fileItem.isError = !1, fileItem.isUploaded = !1, $scope.uploader.queue.push(fileItem);
    }, $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        switch (status) {
          case 200:
            $scope.item._id ? ($scope.saveSuccess(response), refreshAfterUpload(response)) : $scope.saveSuccessExit(response);
            break;

          default:
            $scope.saveFail(response);
        }
    };
}), app.directive("assetReplaceForm", function() {
    return {
        restrict: "E",
        templateUrl: "fluro-admin-content/types/asset/asset-replace-form.html",
        controller: "AssetReplaceController"
    };
}), app.controller("AssetReplaceController", function($scope, Fluro) {
    $scope.showReplaceDialog = function() {
        $scope.replaceDialogShowing = !0;
    };
}), app.controller("CheckinFormController", function($scope, $http, Fluro, Notifications) {
    $scope.$watch("item.contact + item.title + item.firstName + item.lastName", function() {
        if ($scope.item.contact) $scope.item.title = $scope.item.contact.title; else {
            var title = "";
            $scope.item.firstName && $scope.item.firstName.length && (title += $scope.item.firstName), 
            $scope.item.lastName && $scope.item.lastName.length && (title += " " + $scope.item.lastName), 
            $scope.item.title = title;
        }
    });
}), app.controller("CodeFormController", function($scope) {
    function updateSyntax() {
        if (aceEditor) {
            var syntaxName;
            switch ($scope.item.syntax) {
              case "js":
                syntaxName = "javascript";
                break;

              default:
                syntaxName = $scope.item.syntax;
            }
            syntaxName && aceEditor.getSession().setMode("ace/mode/" + syntaxName);
        }
    }
    var aceEditor;
    $scope.aceLoaded = function(_editor) {
        aceEditor = _editor, aceEditor.setTheme("ace/theme/tomorrow_night_eighties"), updateSyntax();
    }, $scope.$watch("item.syntax", updateSyntax), $scope.aceChanged = function(e) {};
}), app.controller("ComponentFormController", function($scope) {
    $scope.scssDisabled = !1, $scope.htmlDisabled = !1, $scope.jsDisabled = !1;
}), app.controller("ContactFormController", function($scope, FluroContent, FluroAccess, TypeService, ModalService) {
    $scope.details || ($scope.details = []), $scope.dateOptions = {
        formatYear: "yy",
        showWeeks: !1
    }, $scope.open = function($event) {
        $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0;
    }, $scope.edit = function(item) {
        ModalService.edit(item);
    }, $scope.getCreateableDetails = function() {
        return _.filter(TypeService.definedTypes, function(def) {
            var correctType = "contactdetail" == def.parentType, exists = _.some($scope.details, function(existing) {
                return existing.definition == def.definitionName;
            });
            return correctType && !exists;
        });
    }, $scope.createDetailSheet = function(sheet) {
        var options = {};
        options.template = {
            contact: $scope.item
        }, ModalService.create(sheet.definitionName, options, function(result) {
            $scope.details.push(result);
        });
    }, $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }, $scope.item._id && (FluroContent.endpoint("info/checkins").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.checkins = res;
    }), FluroContent.endpoint("info/interactions").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.interactions = res;
    }), FluroContent.endpoint("contact/details/" + $scope.item._id, !0, !0).query().$promise.then(function(res) {
        $scope.details = res;
    }), FluroContent.endpoint("info/assignments").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        var today = new Date();
        $scope.pastassignments = _.filter(res, function(event) {
            return new Date(event.startDate) < today;
        }), $scope.upcomingassignments = _.filter(res, function(event) {
            return new Date(event.startDate) >= today;
        });
    }));
}), app.controller("ContactViewController", function($scope, FluroContent, FluroAccess, TypeService, ModalService) {
    FluroContent.endpoint("info/checkins").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.checkins = res;
    }), FluroContent.endpoint("info/interactions").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.interactions = res;
    }), FluroContent.endpoint("contact/details/" + $scope.item._id, !0, !0).query().$promise.then(function(res) {
        $scope.details = res;
    }), FluroContent.endpoint("info/assignments").query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        var today = new Date();
        $scope.pastassignments = _.filter(res, function(event) {
            return new Date(event.startDate) < today;
        }), $scope.upcomingassignments = _.filter(res, function(event) {
            return new Date(event.startDate) >= today;
        });
    }), $scope.viewDetail = function(sheet) {
        ModalService.view(sheet);
    };
}), app.controller("ContactDetailFormController", function($scope, $filter) {
    $scope.$watch("item.contact + definition", function() {
        if (console.log("Test", $scope.definition), $scope.item.contact && $scope.definition) {
            var name = $filter("capitalizename")($scope.item.contact.title), string = name + " - " + $scope.definition.title;
            $scope.item.title = string;
        }
    });
}), app.controller("DefinitionFormController", function($scope) {
    function getFlattenedFields(array, trail) {
        return _.chain(array).map(function(field, key) {
            if ("group" == field.type) {
                field.asObject && trail.push(field.key);
                var fields = getFlattenedFields(field.fields, trail);
                return trail.pop(), fields;
            }
            return trail.push(field.key), field.trail = trail.slice(), trail.pop(), field;
        }).flatten().value();
    }
    $scope.item.definitionName || $scope.$watch("item.title", function(newValue) {
        newValue && ($scope.item.definitionName = _.camelCase(newValue));
    }), $scope.$watch("item.definitionName", function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.item.definitionName = $scope.item.definitionName.replace(regexp, "");
        }
    }), $scope.$watch("item.fields", function(fields) {
        var flattened = getFlattenedFields($scope.item.fields, []);
        $scope.availableFields = _.map(flattened, function(field) {
            return {
                title: field.title,
                path: field.trail.join("."),
                type: field.type
            };
        });
    }), $scope.addAlternativePaymentMethod = function() {
        $scope.item.paymentDetails || ($scope.item.paymentDetails = {}), $scope.item.paymentDetails.paymentMethods || ($scope.item.paymentDetails.paymentMethods = []), 
        $scope.item.paymentDetails.paymentMethods.push({
            title: "New Payment Method"
        });
    }, $scope.removeAlternativePaymentMethod = function(method) {
        _.pull($scope.item.paymentDetails.paymentMethods, method);
    }, $scope.addPaymentModifier = function() {
        $scope.item.paymentDetails || ($scope.item.paymentDetails = {}), $scope.item.paymentDetails.modifiers || ($scope.item.paymentDetails.modifiers = []), 
        $scope.item.paymentDetails.modifiers.push({});
    }, $scope.removeModifier = function(entry) {
        _.pull($scope.item.paymentDetails.modifiers, entry);
    };
}), app.controller("EndpointFormController", function($scope, PermissionService, GenericPermissionService) {
    $scope.permissions = PermissionService.permissions, $scope.genericPermissions = GenericPermissionService.getPermissions();
}), app.controller("EventFormController", function($scope, $stateParams, ModalService, DateTools) {
    $scope.confirmations = $scope.extras.confirmations;
    var currentDate1, currentDate2;
    $scope.item.startDate && (currentDate1 = new Date($scope.item.startDate), currentDate2 = new Date($scope.item.endDate)), 
    currentDate1 && currentDate2 && currentDate1.getTime() != currentDate2.getTime() && ($scope.useEndDate = !0), 
    $scope.$watch("item.startDate + useEndDate", function(data) {
        var date1 = new Date($scope.item.startDate), date2 = new Date($scope.item.endDate);
        $scope.useEndDate || ($scope.item.endDate = $scope.item.startDate), date2.getTime() < date1.getTime() && ($scope.item.endDate = date1);
    }), $scope.$watch("item.startDate +  item.endDate + item.checkinData.checkinStartOffset + item.checkinData.checkinEndOffset", function() {
        var startDate = $scope.item.startDate, endDate = $scope.item.endDate;
        $scope.item.checkinData || ($scope.item.checkinData = {
            checkinStartOffset: 0,
            checkinEndOffset: 0
        }), $scope.checkinStartDate = moment(startDate).subtract($scope.item.checkinData.checkinStartOffset, "minutes"), 
        $scope.checkinEndDate = moment(endDate).add($scope.item.checkinData.checkinEndOffset, "minutes");
    }), $scope.createPlanFromTemplate = function() {
        function addPlan() {
            if (selection.items && selection.items.length) {
                $scope.item.plans && $scope.item.plans.length || ($scope.item.plans = []);
                var template = selection.items[0];
                ModalService.edit(template, function(copyResult) {
                    copyResult && $scope.item.plans.push(copyResult);
                }, function(res) {}, !0, !0, $scope);
            }
        }
        var selection = {}, modal = ModalService.browse("plan", selection);
        modal.result.then(addPlan, addPlan);
    };
}), app.controller("EventListController", function($scope, $sce, $filter, ModalService, FluroStorage, DateTools) {
    var local, session;
    $scope.definition ? (local = FluroStorage.localStorage($scope.definition.definitionName), 
    session = FluroStorage.sessionStorage($scope.definition.definitionName)) : (local = FluroStorage.localStorage($scope.type.path), 
    session = FluroStorage.sessionStorage($scope.type.path));
    new Date();
    $scope.changeDate = function() {
        $scope.updateFilters();
    }, $scope.getMarker = function(date, mode) {
        if ("day" === mode) {
            var checkTimestamp = new Date(date);
            checkTimestamp.setHours(0, 0, 0, 0), checkTimestamp = checkTimestamp.getTime();
            var match = _.some($scope.items, function(event) {
                var startDate, endDate;
                event.startDate && (startDate = new Date(event.startDate), startDate.setHours(0, 0, 0, 0)), 
                event.endDate && (endDate = new Date(event.endDate)), endDate.setHours(23, 59, 59, 999);
                var checkTimestamp = date.getTime(), startTimestamp = startDate.getTime(), endTimestamp = endDate.getTime();
                return checkTimestamp >= startTimestamp && endTimestamp >= checkTimestamp ? !0 : void 0;
            });
            if (match) return "has-event";
        }
        return "";
    }, $scope.resetToday = function() {
        session.search.browseDate = new Date();
    }, session.search.browseDate || $scope.resetToday(), session.search.order || (session.search.order = "startDate"), 
    $scope.getConfirmed = function(item) {
        return $scope.getAssigned(item, "confirmed");
    }, $scope.getDenied = function(item) {
        return $scope.getAssigned(item, "denied");
    }, $scope.getAssignedContacts = function(item) {
        var results = [];
        return _.each(item.assignments, function(ass) {
            _.each(ass.contacts, function(contact) {
                results.push({
                    assignment: ass._id,
                    contact: contact._id
                });
            });
        }), results;
    }, $scope.getUnknown = function(item) {
        var contacts = $scope.getAssignedContacts(item), results = [];
        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });
            if (eventConfirmationObject && eventConfirmationObject.confirmations) {
                var confirmations = eventConfirmationObject.confirmations;
                results = _.filter(contacts, function(contactAss) {
                    return !_.some(confirmations, function(conf) {
                        return conf.contact._id == contactAss.contact;
                    });
                });
            }
        }
        return results;
    }, $scope.getAssigned = function(item, status) {
        var results = [];
        if ($scope.extras && $scope.extras.eventConfirmations && $scope.extras.eventConfirmations.length) {
            var eventConfirmationObject = _.find($scope.extras.eventConfirmations, {
                _id: item._id
            });
            eventConfirmationObject && eventConfirmationObject.confirmations && (results = status ? _.filter(eventConfirmationObject.confirmations, function(conf) {
                var statusMatch = conf.status == status;
                if (!statusMatch) return !1;
                var assignment = _.find(item.assignments, function(ass) {
                    var matchId = ass._id == conf.assignment || ass.title == conf.title;
                    return matchId;
                });
                return assignment ? _.some(assignment.contacts, {
                    _id: conf.contact._id
                }) : !1;
            }) : eventConfirmationObject.confirmations);
        }
        return results;
    }, $scope.getAssignmentTitle = function(item, assignmentId) {
        var ass = _.find(item.assignments, {
            _id: assignmentId
        });
        return ass ? ass.title : void 0;
    }, $scope.pagerEnabled = function() {
        var morePages = $scope.filteredItems && $scope.filteredItems.length && $scope.filteredItems.length > $scope.pager.limit;
        return morePages && "calendar" != $scope.settings.viewMode && "cards" != $scope.settings.viewMode;
    }, $scope.planTooltip = function(item) {
        var string = "";
        return _.each(item.plans, function(plan) {
            string += plan.title + "<br/>";
        }), string += "";
    }, $scope.viewPlans = function(event) {
        1 == event.plans.length ? $scope.viewInModal(event.plans[0]) : $scope.viewInModal(event);
    }, $scope.assignmentTooltip = function(item) {
        var string = '<div class="text-left tooltip-block"><div class="row">';
        return _.each(item.assignments, function(assignment) {
            assignment.contacts.length && (string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + "</strong><br/>", 
            _.each(assignment.contacts, function(contact) {
                string += $filter("capitalizename")(contact.title) + "<br/>";
            }), string += "</div>");
        }), string += "</div></div>";
    }, $scope.assignedTooltip = function(item) {
        var confirmations = $scope.getAssigned(item), string = '<div class="text-left tooltip-block"><div class="row">';
        return _.each(item.assignments, function(assignment) {
            assignment.contacts.length && (string += '<div class="col-xs-6 inline-column"><strong>' + assignment.title + "</strong><br/>", 
            _.each(assignment.contacts, function(contact) {
                var findConfirmation = _.find(confirmations, function(conf) {
                    var correctContact = conf.contact._id == contact._id, correctAssignment = conf.assignment == assignment._id || conf.title == assignment.title;
                    return correctContact && correctAssignment;
                }), contactName = $filter("capitalizename")(contact.title);
                if (findConfirmation) switch (findConfirmation.status) {
                  case "confirmed":
                    string += '<span class="brand-success"><i class="fa success fa-check"></i> ' + contactName + "</span><br/>";
                    break;

                  case "denied":
                    string += '<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + contactName + "</span><br/>";
                    break;

                  default:
                    string += '<span><i class="text-muted fa warning fa-question"></i> ' + contactName + "</span><br/>";
                } else string += '<span><i class="text-muted fa warning fa-question"></i> ' + contactName + "</span><br/>";
            }), string += "</div>");
        }), _.each(item.volunteers, function(slot) {
            string += '<div class="col-xs-6 inline-column"><strong>' + slot.title + "</strong><br/>", 
            slot.contacts && slot.contacts.length ? (_.each(slot.contacts, function(contact) {
                string += '<span class="brand-success text-capitalize"><i class="fa success fa-check"></i> ' + contact.title + "</span><br/>";
            }), slot.minimum && slot.contacts.length < slot.minimum && (string += '<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + (slot.minimum - slot.contacts.length) + " more required</span><br/>")) : slot.minimum && (string += '<span class="brand-danger"><i class="fa fa-exclamation"></i> ' + slot.minimum + " required</span><br/>"), 
            string += "</div>";
        }), string += "</div></div>";
    }, $scope.stringBrowseDate = function() {
        var itemDate = new Date(session.search.browseDate);
        return itemDate.setHours(0), itemDate.setMinutes(0), itemDate.setSeconds(0), itemDate.setMilliseconds(0), 
        itemDate.getTime();
    }, $scope.canCreatePlans = function() {
        return FluroAccess.can("create", "plan");
    }, $scope.createPlanFromTemplate = function(event) {
        function addTemplatePlan() {
            if (selection.items && selection.items.length) {
                event.plans && event.plans.length || (event.plans = []);
                var template = selection.items[0];
                template.event = event, console.log("Add Plan from template", template), ModalService.edit(template, function(copyResult) {
                    copyResult && event.plans.push(copyResult);
                }, function(res) {}, !0, !0, $scope);
            }
        }
        var selection = {}, modal = ModalService.browse("plan", selection);
        modal.result.then(addTemplatePlan, addTemplatePlan);
    }, $scope.addPlan = function(event) {
        ModalService.create("plan", {
            template: {
                event: event,
                realms: event.realms,
                startDate: event.startDate
            }
        }, function(result) {
            result.definition ? CacheManager.clear(result.definition) : CacheManager.clear(result._type), 
            $state.reload();
        }, function(result) {});
    }, session.search.calendarMode || (session.search.calendarMode = "month"), $scope.calendarOpen = function(item) {
        $scope.viewInModal(item.event);
    }, $scope.calendar = {}, $scope.$watch("filteredItems", function(data) {
        $scope.dateGroups = _.groupBy(data, function(item) {
            var itemDate = new Date(item.startDate);
            return itemDate.setHours(0), itemDate.setMinutes(0), itemDate.setSeconds(0), itemDate.setMilliseconds(0), 
            itemDate.getTime();
        }), $scope.calendar.events = _.map(data, function(item) {
            return {
                event: item,
                title: item.title,
                type: "primary",
                startsAt: new Date(item.startDate),
                endsAt: new Date(item.endDate),
                editable: !1,
                deletable: !1,
                incrementsBadgeTotal: !0
            };
        });
    }, !0), $scope.readableDate = DateTools.readableDateRange, $scope.setShowAll = function() {
        session.search.restrictToDate = !1, $scope.updateFilters();
    }, $scope.setShowDate = function() {
        session.search.restrictToDate = !0, $scope.updateFilters();
    }, $scope.updateFilters = function() {
        if ($scope.items) {
            var filteredItems = $scope.items;
            if (filteredItems = $filter("filter")(filteredItems, $scope.search.terms), session.search.browseDate) {
                var browseType = "upcoming";
                session.search.restrictToDate && (browseType = "specific"), filteredItems = $filter("matchDate")(filteredItems, session.search.browseDate, browseType);
            }
            filteredItems = $filter("orderBy")(filteredItems, $scope.search.order, $scope.search.reverse), 
            $scope.search.filters && _.forOwn($scope.search.filters, function(value, key) {
                _.isArray(value) ? _.forOwn(value, function(value) {
                    filteredItems = $filter("reference")(filteredItems, key, value);
                }) : filteredItems = $filter("reference")(filteredItems, key, value);
            }), $scope.filteredItems = filteredItems, $scope.updatePage();
        }
    };
}), app.controller("EventViewController", function($scope, ModalService) {
    $scope.viewEvent = function() {
        ModalService.view(item.event);
    }, $scope.contexts = [], $scope.extras && ($scope.confirmations = $scope.extras.confirmations, 
    $scope.unavailableList = _.filter($scope.confirmations, function(con) {
        return "denied" == con.status;
    }), $scope.confirmedList = _.filter($scope.confirmations, function(con) {
        return "confirmed" == con.status;
    })), $scope.toggleContext = function(val) {
        _.contains($scope.contexts, val) ? _.pull($scope.contexts, val) : $scope.contexts.push(val);
    }, $scope.contextIsActive = function(val) {
        return _.contains($scope.contexts, val);
    }, $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = assignment._id == confirmation.assignment || assignment.title == confirmation.title, correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && "confirmed" == confirmation.status;
        });
    }, $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = assignment._id == confirmation.assignment || assignment.title == confirmation.title, correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && "denied" == confirmation.status;
        });
    }, $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }, $scope.estimateTime = function(startDate, schedules, target) {
        if (target) {
            var total = 0, foundAmount = 0, index = schedules.indexOf(target);
            _.every(schedules, function(item, key) {
                return item && item.duration ? (total += parseInt(item.duration), foundAmount = total - item.duration) : foundAmount = total, 
                index != key;
            });
            var laterTime = new Date(startDate), milliseconds = 60 * foundAmount * 1e3;
            return laterTime.setTime(laterTime.getTime() + milliseconds), laterTime;
        }
    };
}), app.controller("FamilyFormController", function($scope, ModalService) {
    console.log("FAMILY CONTROLLER"), $scope.addContact = function() {
        if (console.log("Add Contact", $scope.item), $scope.item && $scope.item._id) {
            var params = {};
            params.template = {
                family: $scope.item,
                lastName: $scope.item.title
            }, ModalService.create("contact", params, function(res) {
                $scope.item.items.push(res);
            });
        }
    };
}), app.controller("IntegrationFormController", function($scope, $http, Fluro, Notifications) {
    $scope.authorizeInstagram = function() {
        $scope.item._id && $scope.item._id.length || Notifications.warning("Please save before attempting to authorize this integration");
        var clientID = $scope.item.publicDetails.clientID, redirectURI = Fluro.apiURL + "/integrate/instagram/" + $scope.item._id;
        Fluro.token && (redirectURI += "?access_token=" + Fluro.token), clientID && clientID.length || Notifications.warning("Invalid or missing client ID"), 
        redirectURI && redirectURI.length || Notifications.warning("Invalid or missing Redirect URI");
        var url = "https://api.instagram.com/oauth/authorize/?client_id=" + clientID + "&redirect_uri=" + redirectURI + "&response_type=code";
        Fluro.token && (url += "&access_token=" + Fluro.token), console.log("authorize", url), 
        window.open(url);
    };
}), app.controller("InteractionFormController", function($scope) {
    $scope.amountPaid = function() {
        var totalPaid = 0;
        return $scope.item.transactions && $scope.item.transactions.length && (totalPaid += _.chain($scope.item.transactions).map(function(transaction) {
            return transaction.amount;
        }).sum().value()), $scope.item.manualPayments && $scope.item.manualPayments.length && (totalPaid += _.chain($scope.item.manualPayments).map(function(transaction) {
            return transaction.amount;
        }).sum().value()), totalPaid;
    }, $scope.amountDue = function() {
        var originalAmount = $scope.item.amount, amountPaid = $scope.amountPaid();
        return originalAmount - amountPaid;
    };
}), app.controller("LocationFormController", function($scope) {
    console.log("TESTING", $scope.item), $scope.itemAsArray = [ $scope.item ];
}), app.controller("PersonaFormController", function($scope, Session, $state, Fluro, FluroContent, $http, $rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {
    $scope.resendState = "ready", $scope.sendResetEmail = function() {
        $scope.resendState = "processing";
        var email = $scope.item.collectionEmail;
        $scope.item.user && (email = $scope.item.user.email);
        var request = $http.post(Fluro.apiURL + "/user/reinvite/" + $scope.item._id);
        request.error(function(err) {
            console.log("ERROR", err), $scope.resendState = "ready", err.error ? Notifications.error(err.error) : Notifications.error("There was an error sending password instructions");
        }), request.success(function(result) {
            console.log("Send", result), $scope.resendState = "ready", Notifications.status("Instructions on how to reset password have been sent to " + email);
        });
    };
}), app.controller("PersonaListController", function($scope, $rootScope, $state, Fluro, FluroTokenService, CacheManager, Notifications, TypeService) {
    $scope.canImpersonate = function(item) {
        return item ? "administrator" == $rootScope.user.accountType && !item.managed : "administrator" == $rootScope.user.accountType;
    }, $scope.signInAsPersona = function(personaID) {
        function signInSuccess(res) {
            console.log("Signed in as persona", res), CacheManager.clearAll(), TypeService.refreshDefinedTypes(), 
            Notifications.status("Switched this session to " + res.data.account.title), $state.reload();
        }
        function signInFail(res) {
            console.log("Error signing in as persona", res);
        }
        Fluro.sessionStorage = !0, console.log("Sign in as persona", personaID);
        var request = FluroTokenService.signInAsPersona(personaID);
        request.then(signInSuccess, signInFail);
    };
}), app.controller("PlanFormController", function($scope, $stateParams, DateTools) {}), 
app.directive("adminPlanView", function(ModalService) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            item: "=ngModel",
            event: "=ngEvent",
            confirmations: "=ngConfirmations"
        },
        templateUrl: "fluro-admin-content/types/plan/view.html",
        link: function($scope, $element, $attributes) {
            $scope.canEditPlan = !0, $scope.editPlan = function(plan) {
                console.log("Plan", plan), ModalService.edit($scope.item);
            };
        }
    };
}), app.controller("AdminPlanViewController", function($scope) {
    $scope.item.event && ($scope.event = $scope.item.event), $scope.item.showTeamOnSide = !0, 
    $scope.contexts = [], $scope.toggleContext = function(val) {
        _.contains($scope.contexts, val) ? _.pull($scope.contexts, val) : $scope.contexts.push(val);
    }, $scope.contextIsActive = function(val) {
        return _.contains($scope.contexts, val);
    }, $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = assignment._id == confirmation.assignment || assignment.title == confirmation.title, correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && "confirmed" == confirmation.status;
        });
    }, $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = assignment._id == confirmation.assignment || assignment.title == confirmation.title, correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && "denied" == confirmation.status;
        });
    }, $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }, $scope.estimateTime = function(startDate, schedules, target) {
        if (target) {
            var total = 0, foundAmount = 0, index = schedules.indexOf(target);
            _.every(schedules, function(item, key) {
                return item && item.duration ? (total += parseInt(item.duration), foundAmount = total - item.duration) : foundAmount = total, 
                index != key;
            });
            var laterTime = new Date(startDate), milliseconds = 60 * foundAmount * 1e3;
            return laterTime.setTime(laterTime.getTime() + milliseconds), laterTime;
        }
    }, $scope.isBlankColumn = function(team) {
        var hasContent = _.chain($scope.item.schedules).any(function(schedule) {
            return schedule && schedule.notes && schedule.notes[team] && schedule.notes[team].length ? !0 : !1;
        }).value();
        return !hasContent;
    };
}), app.controller("PolicyFormController", function($scope) {}), app.controller("PurchaseFormController", function($scope, $http, Fluro, Notifications) {
    $scope.originalEmail = String($scope.item.collectionEmail), $scope.$watch("item.product", function(product) {
        $scope.item.title = product.title;
    }), $scope.originalEmail = String($scope.item.collectionEmail), $scope.resendStatus = "ready", 
    $scope.resendPurchaseInvitation = function() {
        if ($scope.item && $scope.item._id && $scope.item.collectionEmail && $scope.item.collectionEmail.length) {
            $scope.resendStatus = "processing";
            var email = $scope.item.collectionEmail, request = $http.post(Fluro.apiURL + "/purchase/resend/" + $scope.item._id);
            request.error(function(err) {
                $scope.resendStatus = "ready", err.error ? Notifications.error(err.error) : Notifications.error("There was an error sending purchase notification");
            }), request.success(function(result) {
                $scope.resendStatus = "ready", Notifications.status("Purchase notification sent " + email);
            });
        }
    };
}), app.directive("json", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, element, attrs, ngModelCtrl) {
            function fromUser(text) {
                if (text && "" !== text.trim()) {
                    try {
                        lastValid = angular.fromJson(text), ngModelCtrl.$setValidity("invalidJson", !0);
                    } catch (e) {
                        ngModelCtrl.$setValidity("invalidJson", !1);
                    }
                    return lastValid;
                }
                return {};
            }
            function toUser(object) {
                return angular.toJson(object, !0);
            }
            var lastValid;
            ngModelCtrl.$parsers.push(fromUser), ngModelCtrl.$formatters.push(toUser), element.bind("blur", function() {
                element.val(toUser(scope.$eval(attrs.ngModel)));
            }), scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                lastValid = lastValid || newValue, newValue != oldValue && (ngModelCtrl.$setViewValue(toUser(newValue)), 
                ngModelCtrl.$render());
            }, !0);
        }
    };
}), app.controller("QueryFormController", function($scope, Fluro) {
    $scope.item._id && ($scope.previewLink = Fluro.apiURL + "/content/_query/" + $scope.item._id + "?noCache=true");
}), app.controller("RoleFormController", function($scope, PermissionService, GenericPermissionService) {
    $scope.permissions = PermissionService.permissions, $scope.genericPermissions = GenericPermissionService.getPermissions();
}), app.controller("SiteFormController", function($scope, $http, FluroAccess, ObjectSelection, ContentVersionService) {
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
            return "folder" == route.type ? getFlattenedRoutes(route.routes) : route;
        }).flatten().compact().value();
    }
    $scope.selection = new ObjectSelection(), $scope.selection.multiple = !1, $scope.$watch("item.routes", function(routes) {
        $scope.flattenedRoutes = getFlattenedRoutes(routes);
    }), $scope.$watch("item._id", function(id) {
        id && ($scope.versions = ContentVersionService.list($scope.item));
    }), $scope.hasSlug = function(route) {
        return _.contains(route.url, ":slug");
    }, $scope.defaultSaveOptions.forceVersioning = !0;
}), app.controller("UserFormController", function($scope, Session, $state, Fluro, FluroContent, $http, $rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {
    if ($scope.$watch("item.permissionSets", function() {
        var filtered = _.filter($scope.item.permissionSets, function(set) {
            return set && set.account ? set.account._id == $rootScope.user.account._id : void 0;
        });
        filtered.length || ($scope.item.permissionSets = [ {
            account: $rootScope.user.account,
            sets: []
        } ]), $scope.allowedPermissionSets = filtered;
    }), $scope.saveSuccess = function(result) {
        return result._id && (console.log("Updated to version", $scope.item.__v, result.__v), 
        $scope.item.__v = result.__v), $rootScope.user._id == result._id ? Session.refresh() : (console.log("Clear USER"), 
        CacheManager.clear("user")), Notifications.status(result.name + " was saved successfully"), 
        $scope.$close ? $scope.$close(result) : void ($rootScope.user._id == result._id && $state.reload());
    }, $scope.saveSuccessExit = function(result) {
        $scope.saveSuccess(result), $state.go("user");
    }, !$scope.item._id) {
        var account_id = $rootScope.user.account._id;
        $scope.item.account = account_id, $scope.item.availableAccounts = [ account_id ];
    }
    $scope.me = $rootScope.user._id == $scope.item._id, $scope.allowAccountChange = $scope.me && $scope.item.availableAccounts.length > 1, 
    $scope.canEditAuthDetails = $scope.me || FluroAccess.isFluroAdmin() || !$scope.item._id, 
    $scope.allowRoleChange = FluroAccess.can("assign", "role"), $scope.allowRealmChange = FluroAccess.can("assign", "realm"), 
    $scope.resendEmail = function() {
        $scope.resendState = "processing";
        var request = $http.post(Fluro.apiURL + "/auth/resend", {
            email: $scope.item.email
        });
        request.error(function(err) {
            $scope.resendState = "ready", err.error ? Notifications.error(err.error) : Notifications.error("There was an error sending password instructions");
        }), request.success(function(result) {
            $scope.resendState = "ready", Notifications.status("Instructions on how to reset password have been sent to " + $scope.item.email);
        });
    };
}), app.controller("UserListController", function($scope, ModalService) {}), app.controller("BowerManagerController", function($scope, Fluro, $http) {
    $scope.bowerStatus = "ready", $scope.install = function() {
        $scope.bowerStatus = "progress", $http.post(Fluro.apiURL + "/site/bower/" + $scope.item._id, {}).then(function(res) {
            console.log("BOWER RES", res.data), $scope.bowerStatus = "ready";
        }, function(res) {
            console.log("BOWER FAIL", res.data), $scope.bowerStatus = "ready";
        });
    };
}), app.directive("fluroComponentLoader", function(Fluro, $compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        template: '<div class="component-loader"></div>',
        link: function($scope, $element, $attr) {
            var watchString = "model.components + model.scripts + model.bower.length";
            $scope.$watch(watchString, function() {
                if ($element.empty(), $scope.model) {
                    var script = "";
                    if ($scope.model.components && $scope.model.components.length && (console.log("Load components"), 
                    _.each($scope.model.components, function(component) {
                        script += '<script type="text/lazy">', script += 'console.log("Init Component ' + component.title + '");\n', 
                        script += component.compiled, script += "</script>";
                    })), $scope.model.scripts && $scope.model.scripts.length && (console.log("Load inline scripts"), 
                    _.each($scope.model.scripts, function(s) {
                        script += '<script type="text/lazy">\n', script += 'console.log("Init ' + s.title + '");\n', 
                        script += s.body + ";\n", script += "</script>";
                    })), $scope.model.bower && $scope.model.bower.length) {
                        console.log("Load Bower Scripts");
                        var vendorJSUrl = Fluro.apiURL + "/get/site/" + $scope.model._id + "/js";
                        script += '<script type="text/lazy" src="' + vendorJSUrl + '"></script>';
                    }
                    script && script.length && ($element.append(script), $compile($element.contents())($scope));
                }
            }, !0);
        }
    };
}), app.directive("fluroComponentPreview", function(Fluro, $compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-component-preview/fluro-component-preview.html",
        link: function($scope, $element, $attr) {
            $scope.$watch("model", function(component) {
                if ($element.empty(), component) {
                    var directiveName = component.directive, script = '<script type="text/lazy">' + component.compiled + "</script>", template = "<" + directiveName + "></" + directiveName + ">";
                    $element.append(script), $compile($element.contents())($scope), $element.append(template), 
                    $compile($element.contents())($scope);
                }
            });
        }
    };
}), app.directive("fluroComponentSelect", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            preview: "=ngPreview"
        },
        templateUrl: "fluro-component-select/fluro-component-select.html",
        controller: "FluroComponentSelectController"
    };
}), app.controller("FluroComponentSelectController", function($scope, FluroContent) {
    $scope.model || ($scope.model = []), $scope.previewComponent = function(component) {
        $scope.preview || ($scope.preview = {}), $scope.preview.component = component;
    }, $scope.refreshStatus = "", $scope.reloadComponents = function() {
        $scope.refreshStatus = "loading", FluroContent.endpoint("my/components", !1, !0).query().$promise.then(function(res) {
            console.log("RES", res), $scope.items = res, $scope.refreshStatus = "complete";
        });
    }, $scope.reloadComponents(), $scope.reloadComponent = function(component) {
        console.log("Reload Component", component.title);
        var index = _.findIndex($scope.model, {
            _id: component._id
        });
        -1 != index ? (console.log("Replace component"), $scope.model[index] = component) : (console.log("Insert Component"), 
        $scope.model.push(component));
    }, $scope.isSelected = function(item) {
        var checkId = item;
        return item._id && (checkId = item._id), _.some($scope.model, function(active) {
            return active._id ? active._id == checkId : active == checkId;
        });
    }, $scope.deselect = function(item) {
        var checkId = item;
        item._id && (checkId = item._id), _.remove($scope.model, function(search) {
            return search._id ? search._id == checkId : search == checkId;
        });
    }, $scope.select = function(item) {
        console.log("Select", item), $scope.model.push(item);
    }, $scope.toggle = function(item) {
        var active = $scope.isSelected(item);
        active ? $scope.deselect(item) : $scope.select(item);
    };
}), app.factory("dateutils", function() {
    function validateDate(date) {
        var d = new Date(Date.UTC(date.year, date.month, date.day)), result = d && d.getMonth() === date.month && d.getDate() === Number(date.day);
        return result;
    }
    function changeDate(date) {
        return date.day > 28 ? (date.day--, date) : date.month > 11 ? (date.day = 31, date.month--, 
        date) : void 0;
    }
    var self = this;
    return this.days = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 ], 
    this.months = [ {
        value: 0,
        name: "January"
    }, {
        value: 1,
        name: "February"
    }, {
        value: 2,
        name: "March"
    }, {
        value: 3,
        name: "April"
    }, {
        value: 4,
        name: "May"
    }, {
        value: 5,
        name: "June"
    }, {
        value: 6,
        name: "July"
    }, {
        value: 7,
        name: "August"
    }, {
        value: 8,
        name: "September"
    }, {
        value: 9,
        name: "October"
    }, {
        value: 10,
        name: "November"
    }, {
        value: 11,
        name: "December"
    } ], {
        checkDate: function(date) {
            if (!date.day) return !1;
            if (0 === date.month) ; else if (!date.month) return !1;
            return date.year ? validateDate(date) ? date : this.checkDate(changeDate(date)) : !1;
        },
        get: function(name) {
            return self[name];
        }
    };
}), app.directive("dobSelect", [ "dateutils", function(dateutils) {
    return {
        restrict: "E",
        replace: !0,
        require: "ngModel",
        scope: {
            model: "=ngModel",
            hideAge: "=?",
            hideDates: "=?"
        },
        controller: [ "$scope", "dateutils", function($scope, dateutils) {
            function dateFieldsChanged(newDate, oldDate) {
                if (newDate != oldDate) {
                    if (stopWatchingAge(), !$scope.dateFields) return console.log("No datefields object");
                    if (!$scope.dateFields.year) return console.log("No Year");
                    if (0 === $scope.dateFields.month) ; else if (!$scope.dateFields.month) return console.log("No Month");
                    if (!$scope.dateFields.day) return console.log("No day");
                    var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                    date.setHours(0, 0, 0, 0), $scope.model || ($scope.model = new Date()), $scope.model.setDate(date.getDate()), 
                    $scope.model.setMonth(date.getMonth()), $scope.model.setFullYear(date.getFullYear());
                    var diff = moment().diff($scope.model, "years");
                    $scope.age = parseInt(diff), startWatchingAge();
                }
            }
            function ageChanged(newAge, oldAge) {
                if (newAge != oldAge) {
                    console.log("Age changed", newAge, oldAge);
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    var currentYear = today.getFullYear(), diff = parseInt(currentYear) - parseInt(newAge);
                    if (diff) {
                        if (stopWatchingDate(), $scope.dateFields) $scope.dateFields.year = diff; else {
                            var dateFields = {};
                            dateFields.year = diff, $scope.dateFields = dateFields;
                        }
                        startWatchingDate();
                    } else console.log("NOPE", newAge, currentYear, diff);
                }
            }
            function startWatchingAge() {
                ageWatcher = $scope.$watch("age", ageChanged);
            }
            function stopWatchingAge() {
                ageWatcher && ageWatcher();
            }
            function startWatchingDate() {
                dateWatcher = $scope.$watch("dateFields", dateFieldsChanged, !0);
            }
            function stopWatchingDate() {
                dateWatcher && dateWatcher();
            }
            $scope.days = dateutils.get("days"), $scope.months = dateutils.get("months"), $scope.model ? (console.log("TESTING", $scope.model, _.isDate($scope.model)), 
            _.isDate($scope.model) || ($scope.model = new Date($scope.model))) : console.log("No way man");
            var dateFields = {};
            $scope.model && (dateFields.day = new Date($scope.model).getDate(), dateFields.month = new Date($scope.model).getMonth(), 
            dateFields.year = new Date($scope.model).getFullYear(), $scope.dateFields = dateFields, 
            $scope.age = moment().diff($scope.model, "years")), $scope.checkDate = function() {
                var date = dateutils.checkDate($scope.dateFields);
                date && ($scope.dateFields = date);
            };
            var ageWatcher, dateWatcher;
            startWatchingAge(), startWatchingDate();
        } ],
        templateUrl: "fluro-dob-select/fluro-dob-select.html",
        link: function(scope, element, attrs, ctrl) {
            attrs.yearText && (scope.yearText = !0), attrs.dayDivClass && (angular.element(element[0].children[0]).removeClass("form-group col-xs-3"), 
            angular.element(element[0].children[0]).addClass(attrs.dayDivClass)), attrs.dayClass && (angular.element(element[0].children[0].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass)), attrs.monthDivClass && (angular.element(element[0].children[1]).removeClass("form-group col-xs-5"), 
            angular.element(element[0].children[1]).addClass(attrs.monthDivClass)), attrs.monthClass && (angular.element(element[0].children[1].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass)), 
            attrs.yearDivClass && (angular.element(element[0].children[2]).removeClass("form-group col-xs-4"), 
            angular.element(element[0].children[2]).addClass(attrs.yearDivClass)), attrs.yearClass && (angular.element(element[0].children[2].children[0]).removeClass("form-control"), 
            angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass));
            var currentYear = parseInt(attrs.startingYear, 10) || new Date().getFullYear(), numYears = parseInt(attrs.numYears, 10) || 100, oldestYear = currentYear - numYears;
            scope.years = [];
            for (var i = currentYear; i >= oldestYear; i--) scope.years.push(i);
            scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                scope.disableFields = newVal;
            });
        }
    };
} ]), app.controller("FluroInteractionButtonSelectController", function($scope, FluroValidate) {
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    var opts = ($scope.to, $scope.options);
    $scope.selection = {
        values: [],
        value: null
    };
    var definition = $scope.to.definition, minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.dragControlListeners = {
        orderChanged: function(event) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        }
    }, $scope.selectBox = {}, $scope.selectUpdate = function() {
        $scope.selectBox.item && ($scope.selection.values.push($scope.selectBox.item), $scope.model[opts.key] = angular.copy($scope.selection.values));
    }, $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : $scope.selection.value = null;
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference), 
        setModel();
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
}), app.directive("fluroDateSelect", [ function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-interaction-form/date-select/date-selector.html",
        controller: [ "$scope", "dateutils", function($scope, dateutils) {
            function modelChanged() {
                $scope.model && (_.isDate($scope.model) || ($scope.model = new Date($scope.model)), 
                dateFields.day = new Date($scope.model).getDate(), dateFields.month = new Date($scope.model).getMonth(), 
                dateFields.year = new Date($scope.model).getFullYear(), $scope.dateFields = dateFields);
            }
            function dateFieldsChanged(newDate, oldDate) {
                if (newDate != oldDate && $scope.dateFields && $scope.dateFields.year) {
                    if (0 === $scope.dateFields.month) ; else if (!$scope.dateFields.month) return;
                    if ($scope.dateFields.day) {
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0), $scope.model || ($scope.model = new Date()), $scope.model.setDate(date.getDate()), 
                        $scope.model.setMonth(date.getMonth()), $scope.model.setFullYear(date.getFullYear());
                    }
                }
            }
            $scope.days = dateutils.get("days"), $scope.months = dateutils.get("months");
            var dateFields = {};
            $scope.$watch("dateFields", dateFieldsChanged, !0), $scope.$watch("model", modelChanged), 
            $scope.checkDate = function() {
                var date = dateutils.checkDate($scope.dateFields);
                date && ($scope.dateFields = date);
            };
        } ]
    };
} ]), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "dob-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/dob-select/fluro-dob-select.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "embedded",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/embedded/fluro-embedded.html",
        controller: "FluroInteractionNestedController",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.directive("interactionForm", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            vm: "=?config",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "InteractionFormController",
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find("[ng-transclude-here]").append(clone);
            });
        }
    };
}), app.directive("webform", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback",
            linkedEvent: "=?linkedEvent"
        },
        transclude: !0,
        controller: "InteractionFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
        name: "currency",
        "extends": "input",
        overwriteOk: !0,
        controller: [ "$scope", function($scope) {} ],
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: "ng-currency"
                },
                fractionValue: {
                    attribute: "fraction",
                    bound: "fraction"
                },
                minimum: {
                    attribute: "min",
                    bound: "min"
                },
                maximum: {
                    attribute: "max",
                    bound: "max"
                }
            },
            templateOptions: {
                customAttrVal: "",
                required: !0,
                fraction: 2
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {
                        var numericValue = Number($modelValue);
                        if (isNaN(numericValue)) return !1;
                        var minimumAmount = scope.options.data.minimumAmount, maximumAmount = scope.options.data.maximumAmount;
                        return minimumAmount && minimumAmount > numericValue ? !1 : maximumAmount && numericValue > maximumAmount ? !1 : !0;
                    }
                }
            }
        }
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {
        var fluroErrorTemplate = $templateCache.get("fluro-interaction-form/field-errors.html");
        return "<div>" + template + fluroErrorTemplate + "</div>";
    }), formlyConfig.setType({
        name: "upload",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/upload/upload.html",
        defaultOptions: {
            noFormControl: !0,
            wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: "FluroInteractionFormUploadController"
    }), formlyConfig.setType({
        name: "multiInput",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/multi.html",
        defaultOptions: {
            noFormControl: !0,
            wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: [ "$scope", function($scope) {
            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
            $scope.copyItemOptions = copyItemOptions;
        } ]
    }), formlyConfig.setType({
        name: "custom",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/custom.html",
        controller: "CustomInteractionFieldController",
        wrapper: [ "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "button-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/button-select/fluro-button-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "date-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/date-select/fluro-date-select.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "terms",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/fluro-terms.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "order-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/order-select/fluro-order-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("CustomInteractionFieldController", function($scope, FluroValidate) {
    $scope.$watch("model[options.key]", function(value) {
        value && $scope.fc && $scope.fc.$setTouched();
    }, !0);
}), app.controller("InteractionFormController", function($scope, $q, $timeout, $rootScope, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function debugLog() {
        $scope.debugMode && console.log(arguments);
    }
    function validateAllFields() {
        $timeout(function() {
            function setValid(ready) {
                $scope.readyToSubmit = ready;
            }
            function mapRecursiveField(field) {
                if (field) {
                    var output = [ field ];
                    if (field.fields && field.fields.length && output.push(_.map(field.fields, mapRecursiveField)), 
                    field.fieldGroup && field.fieldGroup.length && output.push(_.map(field.fieldGroup, mapRecursiveField)), 
                    field.data && field.data.replicatedFields && field.data.replicatedFields.length) {
                        var nestedForms = _.chain(field.data.replicatedFields).flatten().map(mapRecursiveField).value();
                        output.push(nestedForms);
                    } else field.data && field.data.fields && field.data.fields.length && output.push(_.map(field.data.fields, mapRecursiveField));
                    return output;
                }
            }
            $scope.errorList = _.chain($scope.vm.modelFields).map(mapRecursiveField).flattenDeep().compact().filter(function(field) {
                var required = _.get(field, "formControl.$error.required"), invalid = _.get(field, "formControl.$error.validInput"), uploadInProgress = _.get(field, "formControl.$error.uploadInProgress");
                return required || invalid || uploadInProgress;
            }).value();
            var interactionForm = $scope.vm.modelForm;
            if (!interactionForm) return setValid(!1);
            if (interactionForm.$invalid) return setValid(!1);
            if (interactionForm.$error) {
                if (interactionForm.$error.required && interactionForm.$error.required.length) return setValid(!1);
                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return setValid(!1);
                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) return setValid(!1);
            }
            setValid(!0);
        });
    }
    function definitionModelChanged() {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.minimum = parseInt(fieldDefinition.minimum), 
                fieldDefinition.maximum = parseInt(fieldDefinition.maximum), fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.directive) {
                  case "reference-select":
                  case "value-select":
                    newField.type = "button-select";
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive;
                }
                switch (fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "custom" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    "integer" == fieldDefinition.type && (templateOptions.type = "number", templateOptions.baseDefaultValue = 0, 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrpattern = "[0-9]*", templateOptions.customAttrinputmode = "numeric", 
                    fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        replicatedFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions.hide = fieldDefinition.hideExpression);
                    var allExpressions = _.values(fieldDefinition.expressions).join("+");
                    newField.watcher = {
                        expression: function(field, scope) {
                            var watchScope = {
                                model: scope.model,
                                data: $scope.vm.model,
                                interaction: $scope.vm.model
                            }, val = $parse(allExpressions)(watchScope);
                            return val;
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            debugLog("Expression Changed");
                            var checkScope = {
                                model: scope.model,
                                data: $scope.vm.model,
                                interaction: $scope.vm.model
                            };
                            _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(checkScope);
                                debugLog("expression:", retrievedValue, checkScope);
                                var fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = retrievedValue;

                                  case "hide":
                                    return field.hide = retrievedValue;
                                }
                            });
                        }
                    };
                }
                fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (newField.hideExpression = function($viewValue, $modelValue, scope) {
                    var checkScope = {
                        model: scope.model,
                        data: $scope.vm.model,
                        interaction: $scope.vm.model
                    }, parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);
                    return parsedValue;
                }), newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if (!$scope.model || "interaction" != $scope.model.parentType) return console.log("Model is not an interaction");
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitInteraction, 
        $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
        var interactionFormSettings = $scope.model.data;
        if (interactionFormSettings || (interactionFormSettings = {}), !interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) switch (interactionFormSettings.requireFirstName = !0, 
        interactionFormSettings.requireLastName = !0, interactionFormSettings.requireGender = !0, 
        interactionFormSettings.requireEmail = !0, interactionFormSettings.identifier) {
          case "both":
            interactionFormSettings.requireEmail = interactionFormSettings.requirePhone = !0;
            break;

          case "email":
            interactionFormSettings.requireEmail = !0;
            break;

          case "phone":
            interactionFormSettings.requirePhone = !0;
            break;

          case "either":
            interactionFormSettings.askPhone = !0, interactionFormSettings.askEmail = !0;
        }
        var firstNameField, lastNameField, genderField;
        if ((interactionFormSettings.askGender || interactionFormSettings.requireGender) && (genderField = {
            key: "_gender",
            type: "select",
            templateOptions: {
                type: "email",
                label: "Title",
                placeholder: "Please select a title",
                options: [ {
                    name: "Mr",
                    value: "male"
                }, {
                    name: "Ms / Mrs",
                    value: "female"
                } ],
                required: interactionFormSettings.requireGender,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            },
            validators: {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;
                    return "male" == value || "female" == value;
                }
            }
        }), (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) && (firstNameField = {
            key: "_firstName",
            type: "input",
            templateOptions: {
                type: "text",
                label: "First Name",
                placeholder: "Please enter your first name",
                required: interactionFormSettings.requireFirstName,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            }
        }), (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) && (lastNameField = {
            key: "_lastName",
            type: "input",
            templateOptions: {
                type: "text",
                label: "Last Name",
                placeholder: "Please enter your last name",
                required: interactionFormSettings.requireLastName,
                onBlur: "to.focused=false",
                onFocus: "to.focused=true"
            }
        }), genderField && firstNameField && lastNameField ? (genderField.className = "col-sm-2", 
        firstNameField.className = lastNameField.className = "col-sm-5", $scope.vm.modelFields.push({
            fieldGroup: [ genderField, firstNameField, lastNameField ],
            className: "row"
        })) : firstNameField && lastNameField && !genderField ? (firstNameField.className = lastNameField.className = "col-sm-6", 
        $scope.vm.modelFields.push({
            fieldGroup: [ firstNameField, lastNameField ],
            className: "row"
        })) : (genderField && $scope.vm.modelFields.push(genderField), firstNameField && $scope.vm.modelFields.push(firstNameField), 
        lastNameField && $scope.vm.modelFields.push(lastNameField)), interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
            var newField = {
                key: "_email",
                type: "input",
                templateOptions: {
                    type: "email",
                    label: "Email Address",
                    placeholder: "Please enter a valid email address",
                    required: interactionFormSettings.requireEmail,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return validator.isEmail(value);
                    }
                }
            };
            "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                "templateOptions.required": function($viewValue, $modelValue, scope) {
                    return scope.model._phoneNumber && scope.model._phoneNumber.length ? !1 : !0;
                }
            }), $scope.vm.modelFields.push(newField);
        }
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: "_phoneNumber",
                type: "input",
                templateOptions: {
                    type: "tel",
                    label: "Contact Phone Number",
                    placeholder: "Please enter a contact phone number",
                    required: interactionFormSettings.requirePhone,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            };
            "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                "templateOptions.required": function($viewValue, $modelValue, scope) {
                    return scope.model._email && scope.model._email.length ? !1 : !0;
                }
            }), $scope.vm.modelFields.push(newField);
        }
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: "_dob",
                type: "dob-select",
                templateOptions: {
                    label: "Date of birth",
                    placeholder: "Please provide your date of birth",
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            };
            $scope.vm.modelFields.push(newField);
        }
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        }), $scope.model.paymentDetails || ($scope.model.paymentDetails = {});
        var paymentSettings = $scope.model.paymentDetails;
        if (paymentSettings.required || paymentSettings.allow) {
            var paymentWrapperFields = [], paymentCardFields = [];
            if (paymentSettings.required) debugLog("-- DEBUG -- Payment Settings Required"), 
            paymentWrapperFields.push({
                templateUrl: "fluro-interaction-form/payment/payment-summary.html",
                controller: [ "$scope", "$parse", function($scope, $parse) {
                    function calculateTotal(watchString) {
                        return debugLog("Recalculate total", watchString), $scope.calculatedTotal = requiredAmount, 
                        $scope.modifications = [], paymentSettings.modifiers && paymentSettings.modifiers.length ? (_.each(paymentSettings.modifiers, function(modifier) {
                            var parsedValue = $parse(modifier.expression)($scope);
                            if (parsedValue = Number(parsedValue), isNaN(parsedValue)) return void debugLog("Payment modifier error", modifier.title, parsedValue);
                            var parsedCondition = !0;
                            if (modifier.condition && String(modifier.condition).length && (parsedCondition = $parse(modifier.condition)($scope)), 
                            !parsedCondition) return void debugLog("inactive", modifier.title, modifier, $scope);
                            var operator = "", operatingValue = "$" + parseFloat(parsedValue / 100).toFixed(2);
                            switch (modifier.operation) {
                              case "add":
                                operator = "+", $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                break;

                              case "subtract":
                                operator = "-", $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                break;

                              case "divide":
                                operator = "÷", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                break;

                              case "multiply":
                                operator = "×", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                break;

                              case "set":
                                $scope.calculatedTotal = parsedValue;
                            }
                            $scope.modifications.push({
                                title: modifier.title,
                                total: $scope.calculatedTotal,
                                description: operator + " " + operatingValue,
                                operation: modifier.operation
                            });
                        }), (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) && ($scope.calculatedTotal = 0), 
                        void $timeout(function() {
                            $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                        })) : void debugLog("No payment modifiers set");
                    }
                    $scope.paymentDetails = paymentSettings;
                    var requiredAmount = paymentSettings.amount;
                    $formScope.vm.model.calculatedTotal = requiredAmount, $scope.calculatedTotal = requiredAmount;
                    var watchString = "", modelVariables = _.chain(paymentSettings.modifiers).map(function(paymentModifier) {
                        var string = "(" + paymentModifier.expression + ") + (" + paymentModifier.condition + ")";
                        return string;
                    }).flatten().compact().uniq().value();
                    modelVariables.length && (watchString = modelVariables.join(' + "a" + ')), watchString.length ? (debugLog("Watching changes", watchString), 
                    $scope.$watch(watchString, calculateTotal)) : ($scope.calculatedTotal = requiredAmount, 
                    $scope.modifications = []);
                } ]
            }); else {
                var amountDescription = "Please enter an amount (" + String(paymentSettings.currency).toUpperCase() + ")", minimum = paymentSettings.minAmount, maximum = paymentSettings.maxAmount, paymentErrorMessage = (paymentSettings.amount, 
                "Invalid amount");
                minimum && (minimum = parseInt(minimum) / 100, paymentErrorMessage = "Amount must be a number at least " + $filter("currency")(minimum, "$"), 
                amountDescription += "Enter at least " + $filter("currency")(minimum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                maximum && (maximum = parseInt(maximum) / 100, paymentErrorMessage = "Amount must be a number less than " + $filter("currency")(maximum, "$"), 
                amountDescription += "Enter up to " + $filter("currency")(maximum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                minimum && maximum && (amountDescription = "Enter a numeric amount between " + $filter("currency")(minimum) + " and  " + $filter("currency")(maximum) + " " + String(paymentSettings.currency).toUpperCase(), 
                paymentErrorMessage = "Amount must be a number between " + $filter("currency")(minimum) + " and " + $filter("currency")(maximum));
                var fieldConfig = {
                    key: "_paymentAmount",
                    type: "currency",
                    templateOptions: {
                        type: "text",
                        label: "Amount",
                        description: amountDescription,
                        placeholder: "0.00",
                        required: !0,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum
                    }
                };
                minimum && (fieldConfig.defaultValue = minimum), paymentWrapperFields.push({
                    template: "<hr/><h3>Payment Details</h3>"
                }), paymentWrapperFields.push(fieldConfig);
            }
            var defaultCardName, defaultCardNumber, defaultCardExpMonth, defaultCardExpYear, defaultCardCVN;
            $scope.debugMode && (defaultCardName = "John Citizen", defaultCardNumber = "4242424242424242", 
            defaultCardExpMonth = "05", defaultCardExpYear = "2020", defaultCardCVN = "123"), 
            paymentCardFields.push({
                key: "_paymentCardName",
                type: "input",
                defaultValue: defaultCardName,
                templateOptions: {
                    type: "text",
                    label: "Name on Card",
                    placeholder: "eg. (John Smith)",
                    required: paymentSettings.required,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            }), paymentCardFields.push({
                key: "_paymentCardNumber",
                type: "input",
                defaultValue: defaultCardNumber,
                templateOptions: {
                    type: "text",
                    label: "Card Number",
                    placeholder: "Card Number (No dashes or spaces)",
                    required: paymentSettings.required,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var luhnChk = function(a) {
                            return function(c) {
                                if (!c) return !1;
                                for (var v, l = c.length, b = 1, s = 0; l; ) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10;
                            };
                        }([ 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 ]), value = $modelValue || $viewValue, valid = luhnChk(value);
                        return valid;
                    }
                }
            }), paymentCardFields.push({
                className: "row clearfix",
                fieldGroup: [ {
                    key: "_paymentCardExpMonth",
                    className: "col-xs-6 col-sm-5",
                    type: "input",
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: "text",
                        label: "Expiry Month",
                        placeholder: "MM",
                        required: paymentSettings.required,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                }, {
                    key: "_paymentCardExpYear",
                    className: "col-xs-6 col-sm-5",
                    type: "input",
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: "text",
                        label: "Expiry Year",
                        placeholder: "YYYY",
                        required: paymentSettings.required,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                }, {
                    key: "_paymentCardCVN",
                    className: "col-xs-4 col-sm-2",
                    type: "input",
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: "text",
                        label: "CVN",
                        placeholder: "CVN",
                        required: paymentSettings.required,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                } ]
            }), (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) && paymentCardFields.push({
                key: "_paymentEmail",
                type: "input",
                templateOptions: {
                    type: "email",
                    label: "Receipt Email Address",
                    placeholder: "Enter an email address for transaction receipt",
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            });
            var cardDetailsField = {
                className: "payment-details",
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {
                    return 0 === $formScope.vm.model.calculatedTotal ? !0 : void 0;
                }
            };
            if (paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length) {
                var methodSelection = {
                    className: "payment-method-select",
                    data: {
                        fields: [ cardDetailsField ],
                        settings: paymentSettings
                    },
                    controller: [ "$scope", function($scope) {
                        $scope.settings = paymentSettings, $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        }), $scope.methodOptions.unshift({
                            title: "Pay with Card",
                            key: "card"
                        }), $scope.model._paymentMethod || ($scope.model._paymentMethod = "card"), $scope.selected = {
                            method: $scope.methodOptions[0]
                        }, $scope.selectMethod = function(method) {
                            $scope.settings.showOptions = !1, $scope.selected.method = method, $scope.model._paymentMethod = method.key;
                        };
                    } ],
                    templateUrl: "fluro-interaction-form/payment/payment-method.html",
                    hideExpression: function($viewValue, $modelValue, scope) {
                        return 0 === $formScope.vm.model.calculatedTotal ? !0 : void 0;
                    }
                };
                paymentWrapperFields.push(methodSelection);
            } else paymentWrapperFields.push(cardDetailsField);
            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields
            });
        }
        $scope.promises.length ? ($scope.promisesResolved = !1, $q.all($scope.promises).then(function() {
            $scope.promisesResolved = !0;
        })) : $scope.promisesResolved = !0;
    }
    function submitInteraction() {
        function processRequest() {
            function submissionSuccess(res) {
                $scope.callback && $scope.callback(res), $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
                $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), $formScope = $scope, 
                $scope.response = res, $scope.vm.state = "complete";
            }
            function submissionFail(res) {
                return $scope.vm.state = "error", res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                    return error.message;
                }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
            }
            delete interactionDetails._paymentCardCVN, delete interactionDetails._paymentCardExpMonth, 
            delete interactionDetails._paymentCardExpYear, delete interactionDetails._paymentCardName, 
            delete interactionDetails._paymentCardNumber, interactionDetails._paymentAmount && (paymentDetails.amount = 100 * parseFloat(interactionDetails._paymentAmount)), 
            interactionDetails._paymentEmail && (paymentDetails.email = interactionDetails._paymentEmail), 
            delete interactionDetails._paymentEmail;
            var definitionID = $scope.model._id;
            definitionID._id && (definitionID = definitionID._id);
            var params = {
                definition: definitionID
            }, request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);
            request.then(submissionSuccess, submissionFail);
        }
        $scope.vm.state = "sending";
        var requiresPayment, allowsPayment, interactionKey = $scope.model.definitionName, interactionDetails = angular.copy($scope.vm.model), paymentConfiguration = $scope.model.paymentDetails;
        if (paymentConfiguration && (requiresPayment = paymentConfiguration.required, allowsPayment = paymentConfiguration.allow), 
        !requiresPayment && !allowsPayment) return processRequest();
        var paymentDetails = {};
        if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {
            var selectedMethod = interactionDetails._paymentMethod;
            if (selectedMethod && "card" != selectedMethod) return paymentDetails.method = selectedMethod, 
            processRequest();
        }
        if (requiresPayment && !$formScope.vm.model.calculatedTotal) return processRequest();
        var paymentIntegration = $scope.integration;
        if (!paymentIntegration || !paymentIntegration.publicDetails) return alert("This form has not been configured properly. Please notify the administrator of this website immediately."), 
        void ($scope.vm.state = "ready");
        switch (paymentDetails.integration = paymentIntegration._id, paymentIntegration.module) {
          case "eway":
            if (!window.eCrypt) return console.log("ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information"), 
            void ($scope.vm.state = "ready");
            var key = paymentIntegration.publicDetails.publicKey, cardDetails = {};
            cardDetails.name = interactionDetails._paymentCardName, cardDetails.number = eCrypt.encryptValue(interactionDetails._paymentCardNumber, key), 
            cardDetails.cvc = eCrypt.encryptValue(interactionDetails._paymentCardCVN, key);
            var expiryMonth = String(interactionDetails._paymentCardExpMonth), expiryYear = String(interactionDetails._paymentCardExpYear);
            return expiryMonth.length < 1 && (expiryMonth = "0" + expiryMonth), cardDetails.exp_month = expiryMonth, 
            cardDetails.exp_year = expiryYear.slice(-2), paymentDetails.details = cardDetails, 
            processRequest();

          case "stripe":
            if (!window.Stripe) return console.log("ERROR: Stripe is selected for payment but the Stripe API has not been included in this application"), 
            void ($scope.vm.state = "ready");
            var liveKey = paymentIntegration.publicDetails.livePublicKey, sandboxKey = paymentIntegration.publicDetails.testPublicKey, key = liveKey;
            paymentIntegration.publicDetails.sandbox && (key = sandboxKey), Stripe.setPublishableKey(key);
            var cardDetails = {};
            cardDetails.name = interactionDetails._paymentCardName, cardDetails.number = interactionDetails._paymentCardNumber, 
            cardDetails.cvc = interactionDetails._paymentCardCVN, cardDetails.exp_month = interactionDetails._paymentCardExpMonth, 
            cardDetails.exp_year = interactionDetails._paymentCardExpYear, Stripe.card.createToken(cardDetails, function(status, response) {
                $timeout(function() {
                    return response.error ? (console.log("Stripe token error", response), $scope.processErrorMessages = [ response.error.message ], 
                    $scope.vm.state = "error", void 0) : (paymentDetails.details = response, processRequest());
                });
            });
        }
    }
    $scope.vm || ($scope.vm = {}), $formScope = $scope, $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.correctPermissions = !0, 
    $scope.scrollToField = function(field) {
        var element = _.get(field, "formControl.$$element[0]");
        element && element.scrollIntoView && element.scrollIntoView();
    }, $scope.readyToSubmit = !1, $scope.$watch("vm.modelFields", validateAllFields, !0), 
    $scope.$watch("vm.model", validateAllFields, !0), formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is still processing";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), definitionModelChanged(), 
        $scope.response = null, $scope.vm.state = "ready", $scope.$broadcast("form-reset");
    }, $scope.$watch("model", definitionModelChanged);
}), app.directive("postForm", function($compile) {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            host: "=hostId",
            reply: "=?reply",
            thread: "=?thread",
            userStore: "=?user",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "PostFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.directive("recaptchaRender", function($window) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs, $ctrl) {
            function activateRecaptcha(recaptcha) {
                console.log("Activate recaptcha!!"), cancelWatch && cancelWatch(), recaptcha && ($scope.vm.recaptchaID = recaptcha.render(element, {
                    sitekey: "6LelOyUTAAAAADSACojokFPhb9AIzvrbGXyd-33z"
                }));
            }
            if ($scope.model.data && $scope.model.data.recaptcha) {
                var cancelWatch, element = $element[0];
                window.grecaptcha ? activateRecaptcha(window.grecaptcha) : cancelWatch = $scope.$watch(function() {
                    return window.grecaptcha;
                }, activateRecaptcha);
            }
        }
    };
}), app.controller("PostFormController", function($scope, $rootScope, $q, $http, Fluro, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function resetCaptcha() {
        var recaptchaID = $scope.vm.recaptchaID;
        console.log("Reset Captcha", recaptchaID), window.grecaptcha && recaptchaID && window.grecaptcha.reset(recaptchaID);
    }
    function getAllErrorFields(array) {
        return _.chain(array).map(function(field) {
            if (field.fieldGroup && field.fieldGroup.length) return getAllErrorFields(field.fieldGroup);
            if (field.data && (field.data.fields && field.data.fields.length || field.data.dataFields && field.data.dataFields || field.data.replicatedFields && field.data.replicatedFields)) {
                var combined = [];
                return combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields), 
                combined = _.compact(combined), getAllErrorFields(combined);
            }
            return field;
        }).flatten().value();
    }
    function submitPost() {
        function submissionSuccess(res) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {
                data: {}
            }, $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
            $scope.response = res, $scope.thread && $scope.thread.push(res), $scope.vm.state = "complete";
        }
        function submissionFail(res) {
            return $scope.vm.state = "error", res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                return error.message;
            }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
        }
        $scope.vm.state = "sending";
        var submissionKey = $scope.model.definitionName, submissionModel = {
            data: angular.copy($scope.vm.model)
        }, hostID = $scope.host;
        if ($scope.reply && (submissionModel.reply = $scope.reply), "undefined" != typeof $scope.vm.recaptchaID) {
            var response = window.grecaptcha.getResponse($scope.vm.recaptchaID);
            submissionModel["g-recaptcha-response"] = response;
        }
        var request;
        $scope.userStore ? $scope.userStore.config().then(function(config) {
            var postURL = Fluro.apiURL + "/post/" + hostID + "/" + submissionKey;
            request = $http.post(postURL, submissionModel, config), request.then(function(res) {
                return submissionSuccess(res.data);
            }, function(res) {
                return submissionFail(res.data);
            });
        }) : (request = FluroContent.endpoint("post/" + hostID + "/" + submissionKey).save(submissionModel).$promise, 
        request.then(submissionSuccess, submissionFail));
    }
    $scope.thread || ($scope.thread = []), $scope.vm || ($scope.vm = {}), $scope.promisesResolved = !0, 
    $scope.correctPermissions = !0, $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.readyToSubmit = !1, 
    $scope.$watch("vm.modelForm.$invalid + vm.modelForm.$error", function() {
        var interactionForm = $scope.vm.modelForm;
        if (!interactionForm) return $scope.readyToSubmit = !1;
        if (interactionForm.$invalid) return $scope.readyToSubmit = !1;
        if (interactionForm.$error) {
            if (interactionForm.$error.required && interactionForm.$error.required.length) return $scope.readyToSubmit = !1;
            if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return $scope.readyToSubmit = !1;
        }
        $scope.readyToSubmit = !0;
    }, !0), formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
        $scope.response = null, $scope.vm.state = "ready", console.log("Broadcast reset"), 
        $scope.$broadcast("form-reset");
    }, $scope.$watch("model", function(newData, oldData) {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.directive) {
                  case "reference-select":
                  case "value-select":
                    newField.type = "button-select";
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive;
                }
                switch (fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "custom" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    "integer" == fieldDefinition.type && (templateOptions.type = "number", templateOptions.baseDefaultValue = 0, 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrpattern = "[0-9]*", templateOptions.customAttrinputmode = "numeric", 
                    fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        replicatedFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions.hide = fieldDefinition.hideExpression);
                    var allExpressions = _.values(fieldDefinition.expressions).join("+");
                    newField.watcher = {
                        expression: function(field, scope) {
                            return $parse(allExpressions)(scope);
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            scope.interaction || (scope.interaction = $scope.vm.model), _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(scope), fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = retrievedValue;

                                  case "hide":
                                    return field.hide = retrievedValue;
                                }
                            });
                        }
                    };
                }
                fieldDefinition.hideExpression && (newField.hideExpression = fieldDefinition.hideExpression), 
                newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if ($scope.model && "post" == $scope.model.parentType) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
            $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitPost, 
            $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
            var interactionFormSettings = $scope.model.data;
            interactionFormSettings || (interactionFormSettings = {}), _.each($scope.model.fields, function(fieldDefinition) {
                addFieldDefinition($scope.vm.modelFields, fieldDefinition);
            });
        }
    }), $scope.$watch("vm.modelFields", function(fields) {
        $scope.errorList = getAllErrorFields(fields);
    }, !0);
}), app.directive("postThread", function() {
    return {
        restrict: "E",
        transclude: !0,
        scope: {
            definitionName: "=?type",
            host: "=?hostId",
            thread: "=?thread"
        },
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        },
        controller: function($scope, $filter, $rootScope, FluroContent) {
            function reloadThread() {
                return console.log("Reload thread", "post." + hostID + "." + definitionName), FluroContent.endpoint("post/" + hostID + "/" + definitionName, !0, !0).query().$promise.then(threadLoaded, threadError);
            }
            function threadLoaded(res) {
                console.log("Thread reloaded", res);
                var allPosts = res;
                $scope.thread = _.chain(res).map(function(post) {
                    return post.thread = _.filter(allPosts, function(sub) {
                        return sub.reply == post._id;
                    }), post.reply ? void 0 : post;
                }).compact().value();
            }
            function threadError(err) {
                console.log("Thread Error", err), $scope.thread = [];
            }
            $scope.outer = $scope.$parent, $scope.thread || ($scope.thread = []);
            var hostID, definitionName;
            $scope.$watch("host + definitionName", function() {
                if (hostID = $scope.host, definitionName = $scope.definitionName, hostID && definitionName) {
                    var threadRefreshEvent = "post." + hostID + "." + definitionName;
                    return console.log("LISTENING FOR", threadRefreshEvent), $rootScope.$on(threadRefreshEvent, reloadThread), 
                    reloadThread();
                }
            });
        }
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "nested",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/nested/fluro-nested.html",
        controller: "FluroInteractionNestedController"
    });
}), app.controller("FluroInteractionNestedController", function($scope) {
    function resetDefaultValue() {
        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        !$scope.model, $scope.model[$scope.options.key] = defaultValue;
    }
    var def = $scope.to.definition, minimum = def.minimum, maximum = def.maximum;
    $scope.$watch("model[options.key]", function(model) {
        model || resetDefaultValue();
    }), $scope.$on("form-reset", resetDefaultValue), $scope.addAnother = function() {
        $scope.model[$scope.options.key].push({});
    }, $scope.canRemove = function() {
        return minimum ? $scope.model[$scope.options.key].length > minimum ? !0 : void 0 : !0;
    }, $scope.canAdd = function() {
        return maximum ? $scope.model[$scope.options.key].length < maximum ? !0 : void 0 : !0;
    }, $scope.remove = function($index) {
        $scope.model[$scope.options.key].splice($index, 1), $scope.options.data.replicatedFields.splice($index, 1);
    }, $scope.copyFields = function() {
        var copiedFields = angular.copy($scope.options.data.fields);
        return $scope.options.data.replicatedFields.push(copiedFields), copiedFields;
    }, $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        return $scope.options.data.replicatedFields.push(copiedFields), copiedFields;
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "search-select",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/search-select/fluro-search-select.html",
        controller: "FluroSearchSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("FluroSearchSelectController", function($scope, $http, Fluro, $filter, FluroValidate) {
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    $scope.search = {}, $scope.proposed = {};
    var opts = ($scope.to, $scope.options);
    $scope.selection = {};
    var definition = $scope.to.definition;
    definition.params || (definition.params = {});
    var restrictType = definition.params.restrictType, searchLimit = definition.params.searchLimit;
    searchLimit || (searchLimit = 10);
    var minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.multiple ? $scope.model[opts.key] && _.isArray($scope.model[opts.key]) && ($scope.selection.values = angular.copy($scope.model[opts.key])) : $scope.model[opts.key] && ($scope.selection.value = $scope.model[opts.key]), 
    $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
    $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
        $scope.proposed = {}, setModel();
    }, $scope.retrieveReferenceOptions = function(val) {
        var searchUrl = Fluro.apiURL + "/content";
        return restrictType && (searchUrl += "/" + restrictType), searchUrl += "/search", 
        $http.get(searchUrl + "/" + val, {
            ignoreLoadingBar: !0,
            params: {
                limit: searchLimit
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selection.values, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    }, $scope.getValueLabel = function(value) {
        if (definition.options && definition.options.length) {
            var match = _.find(definition.options, {
                value: value
            });
            if (match && match.name) return match.name;
        }
        return value;
    }, $scope.retrieveValueOptions = function(val) {
        if (definition.options && definition.options.length) {
            var options = _.reduce(definition.options, function(results, item) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, item.value) : $scope.selection.value == item.value, 
                exists || results.push({
                    name: item.name,
                    value: item.value
                }), results;
            }, []);
            return $filter("filter")(options, val);
        }
        if (definition.allowedValues && definition.allowedValues.length) {
            var options = _.reduce(definition.allowedValues, function(results, allowedValue) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, allowedValue) : $scope.selection.value == allowedValue, 
                exists || results.push({
                    name: allowedValue,
                    value: allowedValue
                }), results;
            }, []);
            return $filter("filter")(options, val);
        }
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : delete $scope.selection.value, 
        setModel();
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference);
    };
}), app.controller("FluroInteractionFormUploadController", function($scope, $http, FluroValidate) {
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    $scope.ctrl = {
        fileArray: []
    };
    var opts = ($scope.to, $scope.options), definition = $scope.to.definition, minimum = definition.minimum, maximum = definition.maximum;
    minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum;
    var definition = _.get($scope, "to.definition");
    if ($scope.remove = function(item) {
        if (_.pull($scope.ctrl.fileArray, item), !$scope.ctrl.fileArray || !$scope.ctrl.fileArray.length) {
            var fileElement = angular.element("#" + $scope.options.id + " input");
            angular.element(fileElement).val(null);
        }
        $scope.ctrl.refresh();
    }, $scope.ctrl.refresh = function() {
        var maximum = definition.maximum;
        if (1 == maximum) if ($scope.ctrl.fileArray.length) {
            var firstFile = $scope.ctrl.fileArray[0];
            "complete" == firstFile.state ? $scope.model[$scope.options.key] = firstFile.attachmentID : $scope.model[$scope.options.key] = firstFile.state;
        } else $scope.model[$scope.options.key] = null; else $scope.model[$scope.options.key] = _.map($scope.ctrl.fileArray, function(fileItem) {
            return "complete" == fileItem.state ? fileItem.attachmentID : fileItem.state;
        });
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }, opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
    $scope.filesize = function(fileSizeInBytes) {
        var i = -1, byteUnits = [ " kB", " MB", " GB", " TB", "PB", "EB", "ZB", "YB" ];
        do fileSizeInBytes /= 1024, i++; while (fileSizeInBytes > 1024);
        return Math.max(fileSizeInBytes, .1).toFixed(1) + byteUnits[i];
    };
}), app.directive("fluroFileInput", function(Fluro, $http) {
    return {
        scope: {
            model: "=fileArray",
            definition: "=",
            callback: "="
        },
        link: function($scope, $element, $attrs) {
            function filesSelected(changeEvent) {
                var files = changeEvent.target.files, realmID = _.get($scope.definition, "params.realm"), uploadURL = Fluro.apiURL + "/file/attach/" + realmID;
                $scope.model = _.map(files, function(file) {
                    var output = {};
                    output.state = "processing";
                    var formData = new FormData();
                    formData.append("file", file);
                    var request = $http({
                        url: uploadURL,
                        method: "POST",
                        data: formData,
                        headers: {
                            "Content-Type": void 0
                        },
                        uploadEventHandlers: {
                            progress: function(e) {
                                $scope.callback(), e.lengthComputable && (output.progress = Math.round(e.loaded / e.total * 100));
                            }
                        }
                    });
                    return output.file = file, output.request = request, request.then(function(res) {
                        output.state = "complete", output.attachmentID = res.data._id, $scope.callback();
                    }, function(err) {
                        output.state = "error", $scope.callback();
                    }), output;
                }), $scope.callback();
            }
            $scope.model || ($scope.model = []), $element.bind("change", filesSelected);
        }
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "value",
        overwriteOk: !0,
        templateUrl: "fluro-interaction-form/value/value.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.directive("menuManager", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-menu-manager/fluro-menu-manager.html",
        controller: "MenuManagerController"
    };
}), app.controller("MenuManagerController", function($scope, SiteTools, $rootScope, ObjectSelection) {
    $scope.selection = new ObjectSelection(), $scope.selection.multiple = !1, $scope.model ? $scope.selection.select($scope.model[0]) : $scope.model = [], 
    $scope.addAllMenuItems = function(array) {
        var siteRoutes = SiteTools.getFlattenedRoutes($rootScope.site.routes);
        _.each(siteRoutes, function(route) {
            var newMenuItem = {
                title: route.title,
                items: [],
                state: route.state,
                type: "state"
            };
            array.push(newMenuItem);
        });
    }, $scope.availablePages = function() {
        return $rootScope.site ? SiteTools.getFlattenedRoutes($rootScope.site.routes) : void 0;
    }, $scope.removeMenu = function() {
        _.pull($scope.model, $scope.selection.item), $scope.selection.deselect();
    }, $scope.addMenuItem = function(array) {
        var newMenuItem = {
            title: "New Menu Item",
            items: [],
            type: "state"
        };
        array.push(newMenuItem);
    }, $scope.addMenu = function() {
        var newMenu = {
            title: "New Menu",
            items: []
        };
        $scope.model.push(newMenu), $scope.selection.select(newMenu);
    };
}), app.service("FluroMenuService", function($rootScope) {
    var controller = {};
    return controller.get = function(string, id) {
        var menu, result;
        return $rootScope.site && (menu = _.find($rootScope.site.menus, {
            key: string
        })), result = menu && id ? _.find(menu.items, {
            id: id
        }) : menu;
    }, controller;
}), app.directive("routeManager", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel",
            selection: "=ngSelection"
        },
        templateUrl: "fluro-route-manager/fluro-route-manager.html",
        controller: "RouteManagerController"
    };
}), app.controller("RouteManagerController", function($scope, BuildSelectionService) {
    if ($scope.settings = {}, $scope.containsDot = function(string) {
        return _.contains(string, ".");
    }, $scope.hasSlug = function(route) {
        return route ? _.includes(route.url, ":slug") || _.includes(route.url, ":_id") : void 0;
    }, $scope.hasIssue = function(route) {
        if (!route.state || !route.state.length) return "No state name defined";
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model), dupe = _.some(flattenedRoutes, function(r) {
            return route.state == r.state && route != r;
        });
        if (dupe) return "Conflicting state name with existing page (" + route.state + ")";
        if (!route.url || !route.url.length) return "No URL path defined";
        if ($scope.hasSlug(route)) {
            if (!route.seo) return "No slug title";
            if (!route.seo.slugTitle || !route.seo.slugTitle.length) return "No slug title";
            if (!route.seo.slugDescription || !route.seo.slugDescription.length) return "No slug description";
            if (!route.seo.slugImage || !route.seo.slugImage.length) return "No slug share image";
        }
        return route.sections && route.sections.length ? !1 : "No sections created for route";
    }, $scope.model) {
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model), firstRoute = _.find(flattenedRoutes, function(route) {
            return !route.type || "folder" != route.type;
        });
        $scope.selection.select(firstRoute);
    } else $scope.model = [];
    $scope.removeRoute = function() {
        var item = $scope.selection.item, folders = BuildSelectionService.getFlattenedFolders($scope.model), parentFolder = _.find(folders, function(folder) {
            var found = _.contains(folder.routes, item);
            return found;
        });
        parentFolder ? _.pull(parentFolder.routes, item) : _.pull($scope.model, item), $scope.selection.deselect();
    }, $scope.duplicate = function(route, parent) {
        if (route) {
            var index = $scope.model.indexOf(route), newRoute = angular.copy(route);
            newRoute.state = "", newRoute.title = newRoute.title + " copy", newRoute.url = "", 
            -1 != index ? $scope.model.splice(index + 1, 0, newRoute) : $scope.model.push(newRoute), 
            $scope.selection.select(newRoute);
        }
    }, $scope.editFolderName = function(node, event) {
        event.preventDefault(), event.stopPropagation(), $scope.$targetFolder = node;
    }, $scope.addRoute = function() {
        var newRoute = {
            title: "New page"
        };
        $scope.model.push(newRoute), $scope.selection.select(newRoute);
    }, $scope.addFolder = function() {
        var newFolder = {
            title: "New Folder",
            type: "folder",
            routes: []
        };
        $scope.model.push(newFolder);
    };
}), app.directive("routeSectionManager", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-route-section-manager/fluro-route-section-manager.html",
        controller: "RouteSectionManager"
    };
}), app.controller("RouteSectionManager", function($scope, $rootScope, $modal, ObjectSelection, FluroContent) {
    $rootScope.site.templates || ($rootScope.site.templates = []), $scope.getTemplateSource = function(key) {
        return _.find($rootScope.site.templates, {
            key: key
        });
    }, $scope.selectExisting = function() {
        var modalInstance = $modal.open({
            templateUrl: "fluro-route-section-manager/section-modal.html",
            controller: "SectionModalController",
            size: "md",
            resolve: {
                templates: function(FluroContent, $q) {
                    var deferred = $q.defer();
                    return FluroContent.endpoint("content").query({
                        includePublic: !0,
                        searchInheritable: !0,
                        type: "siteblock"
                    }).$promise.then(deferred.resolve, function(err) {
                        return deferred.resolve([]);
                    }), deferred.promise;
                }
            }
        });
        modalInstance.result.then(function(sections) {
            sections && sections.length && (console.log("Modal Selected", sections), _.each(sections, function(section) {
                var copy = angular.copy(section);
                section._id && (copy.exportID = section._id), $scope.model.push(copy);
            }), $scope.selection.select($scope.model[$scope.model.length - 1]));
        });
    }, $scope.exportTemplate = function(section) {
        if (section.title && section.title.length && section.html && section.html.length) {
            var newTemplate = {
                title: section.title,
                key: _.camelCase(section.title),
                html: section.html
            };
            $rootScope.site.templates.push(newTemplate), section.template = newTemplate.key;
        }
    }, $scope.replicateTemplate = function(section) {
        if (section.template) {
            var template = $scope.getTemplateSource(section.template);
            template && (section.html = template.html, section.template = null);
        }
    }, $scope.$watch("model", function(model) {
        model ? $scope.model = model : $scope.model = [];
        var selection = new ObjectSelection();
        selection.multiple = !1, $scope.selection = selection, $scope.addSection = function() {
            var newSection = {};
            $scope.model.push(newSection), $scope.selection.select(newSection);
        }, $scope.removeSection = function(section) {
            _.pull($scope.model, section), $scope.selection.deselect(section);
        }, $scope.exportSection = function(section) {
            section.exporting = !0;
            var newData = _.clone(section);
            newData.realms = $rootScope.site.realms, console.log("NEW DATA", newData, $scope.model), 
            FluroContent.resource("siteblock").save(newData).$promise.then(function(res) {
                console.log("Site block was saved", res), section.exporting = !1, section.exportID = res._id;
            }, function(err) {
                console.log("Error exporting site block", err), section.exporting = !1;
            });
        };
    });
}), app.controller("SectionModalController", function($rootScope, $scope, ObjectSelection, templates) {
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
            return "folder" == route.type ? getFlattenedRoutes(route.routes) : route;
        }).flatten().compact().value();
    }
    $scope.selection = new ObjectSelection(), $scope.selection.multiple = !0, $scope.settings = {}, 
    $scope.templates = templates, $scope.toggleGroup = function(group) {
        $scope.settings.selectedGroup == group ? $scope.settings.selectedGroup = null : $scope.settings.selectedGroup = group;
    }, $scope.$watch(function() {
        return $rootScope.site && $rootScope.site.routes ? $rootScope.site.routes : void 0;
    }, function() {
        var routes = getFlattenedRoutes($rootScope.site.routes);
        routes && ($scope.sections = _.chain(routes).map(function(route) {
            return route.sections && route.sections.length ? {
                pageName: route.title,
                route: route,
                sections: route.sections
            } : void 0;
        }).compact().value());
    });
}), app.directive("scriptManager", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-script-manager/fluro-script-manager.html",
        controller: "ScriptManagerController"
    };
}), app.controller("ScriptManagerController", function($scope) {
    $scope.selected = {}, $scope.model ? $scope.selected.script = $scope.model[0] : $scope.model = [], 
    $scope.removeScript = function() {
        _.pull($scope.model, $scope.selected.script), $scope.selected = {};
    }, $scope.addScript = function() {
        var newScript = {
            title: "New Script",
            body: ""
        };
        $scope.model.push(newScript), $scope.selected.script = newScript;
    };
}), app.directive("scrollActive", function($compile, $timeout, $window, FluroScrollService) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs) {
            function setScrollContext(context) {
                currentContext != context && (currentContext = context, $timeout(function() {
                    switch (context) {
                      case "active":
                        $element.removeClass("scroll-after"), $element.removeClass("scroll-before"), $element.addClass("scroll-active"), 
                        $scope.scrollActive = !0, $scope.scrollBefore = !1, $scope.scrollAfter = !1, onActive && onActive();
                        break;

                      case "before":
                        $element.removeClass("scroll-after"), $element.addClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !0, $scope.scrollAfter = !1, onBefore && onBefore();
                        break;

                      case "after":
                        $element.addClass("scroll-after"), $element.removeClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !1, $scope.scrollAfter = !0, onAfter && onAfter();
                    }
                }));
            }
            function updateParentScroll() {
                var viewportHeight = (parent.scrollTop(), parent.height()), contentHeight = parent.get(0).scrollHeight, maxScroll = contentHeight - viewportHeight, startView = 0, endView = startView + viewportHeight, halfView = endView - viewportHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.position().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            function updateFromMainScroll(scrollValue) {
                var windowHeight = $window.innerHeight, documentHeight = body.height(), maxScroll = documentHeight - windowHeight, startView = scrollValue;
                startView || (startView = 0);
                var endView = startView + windowHeight, halfView = endView - windowHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.offset().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    console.log(rangeStart, start, rangeEnd), start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, 
                    anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            var onActive, onBefore, onAfter, onAnchor;
            $scope.scrollActiveStartOffset || ($scope.scrollActiveStartOffset = 100);
            var anchored, currentContext = "";
            $attrs.onActive && (onActive = function() {
                $scope.$eval($attrs.onActive);
            }), $attrs.onAnchor && (onAnchor = function() {
                $scope.$eval($attrs.onAnchor);
            }), $attrs.onAfter && (onAfter = function() {
                $scope.$eval($attrs.onAfter);
            }), $attrs.onBefore && (onBefore = function() {
                $scope.$eval($attrs.onBefore);
            });
            var parent = $element.closest("[scroll-active-parent]"), body = angular.element("body");
            parent.length ? (parent.bind("scroll", updateParentScroll), $timeout(updateParentScroll, 10)) : ($scope.$watch(function() {
                return FluroScrollService.getScroll();
            }, updateFromMainScroll), $timeout(updateFromMainScroll, 10));
        }
    };
}), app.service("FluroScrollService", function($window, $location, $timeout) {
    function updateScroll() {
        var v = this.pageYOffset;
        _value != this.pageYOffset && (_value > v ? controller.direction = "up" : controller.direction = "down", 
        $timeout(function() {
            _value = this.pageYOffset;
        }));
    }
    var controller = {};
    controller.cache = {}, controller.direction = "down";
    var _value = 0;
    angular.element("html,body");
    return controller.setAnchor = function(id) {
        $location.hash("jump-to-" + id);
    }, controller.getAnchor = function() {
        var hash = $location.hash();
        return _.startsWith(hash, "jump-to-") ? hash.substring(8) : hash;
    }, controller.scrollToID = controller.scrollToId = function(id, speed, selector, offset) {
        speed || (speed = "fast");
        var $target = angular.element("#" + id);
        if ($target && $target.offset && $target.offset()) {
            selector || (selector = "body,html");
            var pos = $target.offset().top;
            offset && (pos += Number(offset)), angular.element(selector).animate({
                scrollTop: pos
            }, speed);
        }
    }, controller.scrollToPosition = controller.scrollTo = function(pos, speed, selector, offset) {
        speed || (speed = "fast"), selector || (selector = "body,html"), offset && (pos += Number(offset)), 
        angular.element(selector).animate({
            scrollTop: pos
        }, speed);
    }, controller.get = controller.getScroll = function() {
        return _value;
    }, controller.getMax = function(selector) {
        selector || (selector = "body,html");
        var bodyHeight = angular.element(selector).height(), windowHeight = $window.innerHeight;
        return bodyHeight - windowHeight;
    }, controller.getHalfPoint = function() {
        return $window.innerHeight / 2;
    }, controller.getWindowHeight = function() {
        return $window.innerHeight;
    }, angular.element($window).bind("scroll", updateScroll), updateScroll(), controller;
}), app.directive("fluroStylesheetLoader", function(Fluro, $compile, FluroSocket) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        template: '<div class="stylesheet-loader"></div>',
        link: function($scope, $element, $attr) {
            function reloadStylesheets() {
                if (console.log("Refresh Styles"), $element.empty(), $scope.model) {
                    var script = "", ids = [];
                    if ($scope.model.stylesheets && $scope.model.stylesheets.length) {
                        var stylesheetIds = _.reduce($scope.model.stylesheets, function(result, include) {
                            return include._id ? result.push(include._id) : result.push(include), result;
                        }, []);
                        ids = ids.concat(stylesheetIds);
                    }
                    if ($scope.model.components && $scope.model.components.length) {
                        var componentIds = _.reduce($scope.model.components, function(result, include) {
                            return include.css && include.css.length && (include._id ? result.push(include._id) : result.push(include)), 
                            result;
                        }, []);
                        componentIds.length && (ids = ids.concat(componentIds));
                    }
                    if ($scope.model.afterStylesheets && $scope.model.afterStylesheets.length) {
                        var afterStyles = _.reduce($scope.model.afterStylesheets, function(result, include) {
                            return include._id ? result.push(include._id) : result.push(include), result;
                        }, []);
                        ids = ids.concat(afterStyles);
                    }
                    if (ids.length) {
                        var url = Fluro.apiURL + "/get/compiled/scss?ids=" + ids.join(",");
                        Fluro.token && (url += "&access_token=" + Fluro.token), script += '<link href="' + url + '" rel="stylesheet" type="text/css"/>';
                    }
                    script && script.length && ($element.append(script), $compile($element.contents())($scope));
                }
            }
            $scope.$watch("model.stylesheets.length + model.afterStylesheets.length + model.bower.length + model.components.length", reloadStylesheets), 
            FluroSocket.on("content.edit", function(socketUpdate) {
                var itemID = socketUpdate.item;
                itemID._id && (itemID = itemID._id);
                var combinedDependencies = [].concat($scope.model.components, $scope.model.stylesheets, $scope.model.afterStylesheets);
                console.log("CSS UPDATE", socketUpdate);
                var requireUpdate = _.chain(combinedDependencies).compact().some(function(item) {
                    return item ? item._id ? item._id == itemID : item == itemID : !1;
                }).value();
                requireUpdate && reloadStylesheets();
            });
        }
    };
}), app.directive("templateManager", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        templateUrl: "fluro-template-manager/fluro-template-manager.html",
        controller: "TemplateManagerController"
    };
}), app.controller("TemplateEditorController", function($scope) {
    function stopWatchingTitle() {
        watchTitle && watchTitle();
    }
    function startWatchingTitle() {
        watchTitle && watchTitle(), watchTitle = $scope.$watch("selection.item.title", function(newValue) {
            newValue && ($scope.selection.item.key = _.camelCase(newValue));
        });
    }
    $scope.$watch("selection.item", function(item) {
        item.key && item.key.length ? stopWatchingTitle() : startWatchingTitle();
    });
    var watchTitle;
    $scope.$watch("selection.item.key", function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.selection.item.key = $scope.selection.item.key.replace(regexp, "");
        }
    });
}), app.controller("TemplateManagerController", function($scope, ObjectSelection) {
    var selection = new ObjectSelection();
    selection.multiple = !1, $scope.selection = selection, $scope.model ? $scope.selection.select($scope.model[0]) : $scope.model = [], 
    $scope.removeTemplate = function() {
        _.pull($scope.model, $scope.selection.item), $scope.selection.deselect();
    }, $scope.duplicate = function(template) {
        if (template) {
            var index = $scope.model.indexOf(template), newTemplate = angular.copy(template);
            newTemplate.title = newTemplate.title + " copy", -1 != index ? $scope.model.splice(index + 1, 0, newTemplate) : $scope.model.push(newTemplate), 
            $scope.selection.select(newTemplate);
        }
    }, $scope.addTemplate = function() {
        var newTemplate = {
            title: ""
        };
        $scope.model.push(newTemplate), $scope.selection.select(newTemplate);
    };
}), app.service("FluroWindowService", function($window, $timeout) {
    function updateSize() {
        return controller.width = $window.innerWidth, controller.height = $window.innerHeight, 
        controller.width < 768 ? controller.breakpoint = "xs" : controller.width < 992 ? controller.breakpoint = "sm" : controller.width < 1200 ? controller.breakpoint = "md" : controller.breakpoint = "lg";
    }
    var controller = {};
    return controller.breakpoint = "xs", angular.element($window).bind("resize", updateSize), 
    updateSize(), controller;
}), app.directive("compileHtml", function($compile) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            scope.$watch(function() {
                return scope.$eval(attrs.compileHtml);
            }, function(value) {
                element.html(value), $compile(element.contents())(scope);
            });
        }
    };
}), app.directive("editable", function($compile, $timeout) {
    return {
        restrict: "A",
        scope: {
            model: "=ngModel"
        },
        link: function(scope, element, attrs) {
            scope.active = !1;
            var normal = angular.element('<span class="editable-text" ng-hide="active" ng-dblclick="setActive($event)">{{model}}</span>'), editor = angular.element('<input class="editable-input" ng-show="active" ng-model="model" ng-blur="active = false"/>');
            $compile(normal)(scope), $compile(editor)(scope), element.append(normal), element.append(editor), 
            scope.setActive = function(event) {
                scope.active = !0, $timeout(function() {
                    editor.select();
                }, 10);
            }, element.bind("keydown keypress", function(event) {
                console.log("Enter pressed"), 13 === event.which && (event.preventDefault(), $timeout(function() {
                    scope.active = !1;
                }));
            });
        }
    };
}), app.directive("handle", function() {
    return {
        restrict: "E",
        replace: !0,
        template: '<div class="btn-handle"><i class="fa fa-arrows"></i></div>'
    };
}), angular.module("fluro.lazyload", []).directive("script", function() {
    return {
        restrict: "E",
        scope: !1,
        link: function(scope, elem, attr) {
            if ("text/lazy" === attr.type) {
                var code = elem.text(), f = new Function(code);
                f();
            }
        }
    };
}), app.directive("machineName", function($parse) {
    return {
        restrict: "A",
        require: "^ngModel",
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$viewChangeListeners.push(function() {
                var regexp = /[^a-zA-Z0-9-_]+/g, val = ngModelCtrl.$viewValue.replace(regexp, "-"), cameled = _.camelCase(val);
                $parse(attrs.ngModel).assign(scope, cameled);
            });
        }
    };
}), app.directive("notifications", function() {
    return {
        restrict: "E",
        replace: !0,
        controller: "NotificationsController",
        template: '<div class="notifications"><div ng-if="lastMessage" class="notification {{lastMessage.type}}"><i class="fa fa-notification-{{lastMessage.type}}"></i><span>{{lastMessage.text}}</span></div></div>'
    };
}), app.controller("NotificationsController", function($scope, Notifications) {
    $scope.$watch(function() {
        return Notifications.list.length;
    }, function() {
        $scope.lastMessage = _.last(Notifications.list);
    });
}), app.filter("age", function() {
    return function(date) {
        return Number(moment().diff(date, "years"));
    };
}), app.filter("comma", function() {
    return function(array, key, characterLimit) {
        if (!array) return "";
        if (key) var strings = _.map(array, function(item) {
            return item[key];
        }); else strings = array;
        return characterLimit ? strings.join(", ").substring(0, characterLimit) : strings.join(", ");
    };
}), app.filter("simple", function() {
    return function(data) {
        if (_.isString(data)) return data;
        if (_.isArray(data)) {
            var array = _.map(data, function(item) {
                return _.isObject(item) ? item.title : _.isString(item) ? item : void 0;
            });
            return array.join(", ");
        }
    };
}), app.filter("kebabcase", function() {
    return function(text) {
        return _.kebabCase(text);
    };
}), app.filter("reference", function() {
    return function(items, fieldName, id) {
        function getProperty(obj, prop) {
            var parts = prop.split("."), last = parts.pop(), l = parts.length, i = 1, current = parts[0];
            if (!parts.length) return obj[prop];
            for (;(obj = obj[current]) && l > i; ) current = parts[i], i++;
            return obj ? obj[last] : void 0;
        }
        var results = _.filter(items, function(item) {
            var key = getProperty(item, fieldName);
            if (key) {
                if (_.isArray(key)) {
                    var array = key;
                    return _.some(array, function(e) {
                        return _.isObject(e) ? e._id ? e._id == id : void 0 : e == id;
                    });
                }
                return key._id == id || key == id;
            }
        });
        return results;
    };
}), app.filter("startFrom", function() {
    return function(input, start) {
        return input ? (start = +start, input.slice(start)) : [];
    };
}), app.filter("timeago", function() {
    return function(date) {
        return moment(date).fromNow();
    };
}), app.service("FluroStorage", function($rootScope, $localStorage, $sessionStorage) {
    var controller = {};
    return controller.resetSessionStorage = function() {
        $rootScope.user && ($sessionStorage[$rootScope.user._id] = {});
    }, controller.resetLocalStorage = function() {
        $rootScope.user && ($localStorage[$rootScope.user._id] = {});
    }, controller.sessionStorage = function(key) {
        return $rootScope.user ? ($sessionStorage[$rootScope.user._id] || ($sessionStorage[$rootScope.user._id] = {}), 
        $sessionStorage[$rootScope.user._id][key] || ($sessionStorage[$rootScope.user._id][key] = {}), 
        $sessionStorage[$rootScope.user._id][key]) : void 0;
    }, controller.localStorage = function(key) {
        return $rootScope.user ? ($localStorage[$rootScope.user._id] || ($localStorage[$rootScope.user._id] = {}), 
        $localStorage[$rootScope.user._id][key] || ($localStorage[$rootScope.user._id][key] = {}), 
        $localStorage[$rootScope.user._id][key]) : void 0;
    }, controller;
}), app.service("ModalService", function($modal, $rootScope, Fluro, FluroContent, TypeService) {
    var controller = {};
    return controller.message = function(ids, successCallback, cancelCallback) {
        var modalInstance = $modal.open({
            templateUrl: "fluro-message-center/main.html",
            controller: "FluroMessageCenterController",
            size: "lg",
            backdrop: "static",
            resolve: {
                contacts: function($q) {
                    return FluroContent.endpoint("message/contacts").query({
                        ids: ids
                    }).$promise;
                }
            }
        });
        modalInstance.result.then(successCallback, cancelCallback);
    }, controller.create = function(typeName, params, successCallback, cancelCallback) {
        function createModal(definition) {
            var modalInstance = $modal.open({
                templateUrl: "fluro-admin-content/types/form.html",
                controller: "ContentFormController",
                size: "lg",
                backdrop: "static",
                resolve: {
                    definition: function() {
                        return definition;
                    },
                    type: function() {
                        return definition ? TypeService.getTypeFromPath(definition.parentType) : type;
                    },
                    access: function(FluroAccess) {
                        var canCreate;
                        return canCreate = definition ? FluroAccess.can("create", definition.definitionName) : FluroAccess.can("create", type.path), 
                        FluroAccess.resolveIf(canCreate);
                    },
                    item: function($q) {
                        var deferred = $q.defer(), newItem = {};
                        return params.template && (newItem = angular.copy(params.template)), definition ? definition.definitionName ? (newItem.definition = definition.definitionName, 
                        deferred.resolve(newItem)) : deferred.reject() : deferred.resolve(newItem), deferred.promise;
                    },
                    extras: function() {
                        return {};
                    }
                }
            });
            modalInstance.result.then(successCallback, cancelCallback);
        }
        params || (params = {});
        var type = TypeService.getTypeFromPath(typeName);
        type.parentType ? FluroContent.endpoint("defined/" + type.path).get({}, createModal) : createModal();
    }, controller.view = function(itemObject, successCallback, cancelCallback) {
        var definedName = itemObject._type;
        itemObject.definition && (definedName = itemObject.definition), FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            function createModal(definition) {
                var modalInstance = $modal.open({
                    templateUrl: "fluro-admin-content/types/view.html",
                    controller: "ContentViewController",
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            return item;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess) {
                            var definitionName = type.path;
                            definition && (definitionName = definition.definitionName);
                            var canView = FluroAccess.canViewItem(item, "user" == definitionName);
                            return FluroAccess.resolveIf(canView);
                        },
                        extras: function(FluroContent, $q) {
                            var deferred = $q.defer(), data = {};
                            switch (type.path) {
                              case "event":
                              case "plan":
                                var id;
                                id = item.event ? item.event._id ? item.event._id : item.event : item._id, id ? FluroContent.endpoint("confirmations/event/" + id).query().$promise.then(function(res) {
                                    data.confirmations = res, deferred.resolve(data);
                                }) : deferred.resolve(data);
                                break;

                              default:
                                deferred.resolve(data);
                            }
                            return deferred.promise;
                        }
                    }
                });
                modalInstance.result.then(successCallback, cancelCallback);
            }
            var type = TypeService.getTypeFromPath(item._type);
            item.definition ? FluroContent.endpoint("defined/" + item.definition).get({}, createModal) : createModal();
        });
    }, controller.edit = function(itemObject, successCallback, cancelCallback, createCopy, createFromTemplate, $scope) {
        var definedName = itemObject._type;
        itemObject.definition && (definedName = itemObject.definition), FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            function createModal(definition) {
                var modalInstance = $modal.open({
                    templateUrl: "fluro-admin-content/types/form.html",
                    controller: "ContentFormController",
                    size: "lg",
                    backdrop: "static",
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            var newItem = item;
                            if (createCopy || createFromTemplate) {
                                if (newItem = angular.copy(item), delete newItem._id, delete newItem.slug, delete newItem.author, 
                                delete newItem.apikey, "event" == newItem._type && (newItem.plans = [], newItem.assignments = []), 
                                "event" == newItem._type || "plan" == newItem._type) {
                                    var initDate = new Date();
                                    $scope && ($scope.stringBrowseDate && (initDate = $scope.stringBrowseDate()), $scope.item && $scope.item.startDate && (initDate = $scope.item.startDate)), 
                                    newItem.useEndDate = !1, newItem.endDate = initDate, newItem.startDate = initDate;
                                }
                                "plan" != newItem._type || newItem.event || $scope.item && $scope.item._id && (newItem.event = $scope.item);
                            }
                            return createCopy && (newItem.title = newItem.title + " Copy"), createFromTemplate && (newItem.status = "active"), 
                            itemObject.event && (newItem.event = itemObject.event, "plan" == newItem._type && (newItem.startDate = newItem.event.startDate)), 
                            newItem;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess, $rootScope) {
                            var definitionName = type.path;
                            definition && (definitionName = definition.definitionName);
                            var author = !1;
                            if ("user" == type.path) {
                                if (author = $rootScope.user._id == item._id) return !0;
                            } else author = _.isObject(item.author) ? item.author._id == $rootScope.user._id : item.author == $rootScope.user._id;
                            var canEdit = FluroAccess.canEditItem(item, "user" == definitionName), canCreate = FluroAccess.can("create", definitionName);
                            return createCopy ? FluroAccess.resolveIf(canCreate && canEdit) : FluroAccess.resolveIf(canEdit);
                        },
                        extras: function(FluroContent, $q) {
                            var deferred = $q.defer(), data = {};
                            switch (type.path) {
                              case "event":
                                !itemObject._id || createCopy || createFromTemplate ? deferred.resolve(data) : FluroContent.endpoint("confirmations/event/" + itemObject._id).query().$promise.then(function(res) {
                                    data.confirmations = res, deferred.resolve(data);
                                });
                                break;

                              default:
                                deferred.resolve(data);
                            }
                            return deferred.promise;
                        }
                    }
                });
                modalInstance.result.then(successCallback, cancelCallback);
            }
            var type = TypeService.getTypeFromPath(item._type);
            item.definition ? FluroContent.endpoint("defined/" + item.definition).get({}, createModal) : createModal();
        });
    }, controller.browse = function(type, modelSource, params) {
        params || (params = {}), modelSource || (modelSource = {}), modelSource.items || (modelSource.items = []);
        var modalDetails = {
            template: '<content-browser ng-model="model.items" ng-done="$dismiss" ng-type="type" params="params"></content-browser>',
            controller: function($scope) {
                $scope.type = type, $scope.model = modelSource, $scope.params = params;
            },
            size: "lg"
        }, modalInstance = $modal.open(modalDetails);
        return modalInstance;
    }, controller.batch = function(type, definition, ids, callback) {
        $modal.open({
            template: "<batch-editor></batch-editor>",
            backdrop: "static",
            controller: function($scope) {
                $scope.type = type, $scope.definition = definition, $scope.ids = ids;
            },
            size: "lg"
        });
    }, controller;
}), app.service("SiteTools", function() {
    var controller = {};
    return controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
            return "folder" == route.type ? controller.getFlattenedRoutes(route.routes) : route;
        }).flatten().compact().value();
    }, controller;
}), app.service("Tools", function() {
    var controller = {};
    return controller.getDeepProperty = function(obj, prop) {
        var parts = prop.split("."), last = parts.pop(), l = parts.length, i = 1, current = parts[0];
        if (!parts.length) return obj[prop];
        for (;(obj = obj[current]) && l > i; ) current = parts[i], i++;
        return obj ? obj[last] : void 0;
    }, controller;
});

var redactorOptions = {};

app.constant("redactorOptions", redactorOptions), app.directive("redactor", function($timeout) {
    return {
        restrict: "A",
        require: "ngModel",
        controller: function($scope, $modal) {
            $scope.modal = $modal;
        },
        link: function(scope, element, attrs, ngModel) {
            scope.redactorLoaded = !1;
            var updateModel = function(value) {
                $timeout(function() {
                    scope.$apply(function() {
                        ngModel.$setViewValue(value);
                    });
                });
            }, options = {
                changeCallback: updateModel
            }, additionalOptions = attrs.redactor ? scope.$eval(attrs.redactor) : {};
            additionalOptions.buttons || (additionalOptions.buttons = [ "html", "formatting", "bold", "underline", "italic", "unorderedlist", "orderedlist", "insertImage", "video", "table", "link", "indent", "outdent", "horizontalrule" ]), 
            additionalOptions.formatting = [], additionalOptions.formattingAdd = [], additionalOptions.formattingAdd.push({
                tag: "p",
                title: "Normal text",
                clear: !0
            }), additionalOptions.formattingAdd.push({
                tag: "p",
                title: "Lead text",
                "class": "lead",
                clear: !0
            }), additionalOptions.formattingAdd.push({
                tag: "p",
                title: "Small text",
                "class": "small",
                clear: !0
            }), additionalOptions.formattingAdd.push({
                tag: "h1",
                title: "Heading 1"
            }), additionalOptions.formattingAdd.push({
                tag: "h2",
                title: "Heading 2"
            }), additionalOptions.formattingAdd.push({
                tag: "h3",
                title: "Heading 3"
            }), additionalOptions.formattingAdd.push({
                tag: "h4",
                title: "Heading 4"
            }), additionalOptions.formattingAdd.push({
                tag: "h5",
                title: "Heading 5"
            }), additionalOptions.formattingAdd.push({
                tag: "blockquote",
                title: "Quote"
            }), additionalOptions.formattingAdd.push({
                tag: "code",
                title: "Code"
            }), additionalOptions.allowedAttr = [ [ "p", "class" ], [ "img", [ "src", "alt", "title", "class", "ng-src" ] ], [ "fluro-video", "ng-model" ], [ "iframe", [ "src", "style", "allowfullscreen", "frameborder" ] ], [ "tr", [ "class" ] ], [ "td", [ "class", "colspan" ] ], [ "table", [ "class" ] ], [ "div", [ "class" ] ], [ "a", "*" ], [ "span", [ "class", "rel", "data-verified" ] ], [ "iframe", "*" ], [ "video", "*" ], [ "audio", "*" ], [ "embed", "*" ], [ "object", "*" ], [ "param", "*" ], [ "source", "*" ] ], 
            additionalOptions.replaceTags = [ [ "strike", "del" ], [ "b", "strong" ] ], additionalOptions.replaceDivs = !1, 
            additionalOptions.plugins || (additionalOptions.plugins = [ "insertImage", "video", "table", "undoAction", "redoAction" ]), 
            additionalOptions.toolbarExternal || (additionalOptions.toolbarExternal = "#article-toolbar");
            var editor;
            angular.extend(options, redactorOptions, additionalOptions);
            var changeCallback = additionalOptions.changeCallback || redactorOptions.changeCallback;
            changeCallback && (options.changeCallback = function(value) {
                updateModel.call(this, value), changeCallback.call(this, value);
            }), $timeout(function() {
                editor = element.redactor(options), ngModel.$render(), element.on("remove", function() {
                    element.off("remove"), element.redactor("core.destroy");
                });
            }), ngModel.$render = function() {
                angular.isDefined(editor) && $timeout(function() {
                    element.redactor("code.set", ngModel.$viewValue || ""), element.redactor("placeholder.toggle"), 
                    scope.redactorLoaded = !0;
                });
            };
        }
    };
}), function($) {
    "use strict";
    function Redactor(el, options) {
        return new Redactor.prototype.init(el, options);
    }
    Function.prototype.bind || (Function.prototype.bind = function(scope) {
        var fn = this;
        return function() {
            return fn.apply(scope);
        };
    });
    var uuid = 0;
    $.fn.redactor = function(options) {
        var val = [], args = Array.prototype.slice.call(arguments, 1);
        return "string" == typeof options ? this.each(function() {
            var func, instance = $.data(this, "redactor");
            if ("-1" != options.search(/\./) ? (func = options.split("."), "undefined" != typeof instance[func[0]] && (func = instance[func[0]][func[1]])) : func = instance[options], 
            "undefined" != typeof instance && $.isFunction(func)) {
                var methodVal = func.apply(instance, args);
                void 0 !== methodVal && methodVal !== instance && val.push(methodVal);
            } else $.error('No such method "' + options + '" for Redactor');
        }) : this.each(function() {
            $.data(this, "redactor", {}), $.data(this, "redactor", Redactor(this, options));
        }), 0 === val.length ? this : 1 === val.length ? val[0] : val;
    }, $.Redactor = Redactor, $.Redactor.VERSION = "10.2.3", $.Redactor.modules = [ "alignment", "autosave", "block", "buffer", "build", "button", "caret", "clean", "code", "core", "dropdown", "file", "focus", "image", "indent", "inline", "insert", "keydown", "keyup", "lang", "line", "link", "linkify", "list", "modal", "observe", "paragraphize", "paste", "placeholder", "progress", "selection", "shortcuts", "tabifier", "tidy", "toolbar", "upload", "utils" ], 
    $.Redactor.opts = {
        lang: "en",
        direction: "ltr",
        plugins: !1,
        focus: !1,
        focusEnd: !1,
        placeholder: !1,
        visual: !0,
        tabindex: !1,
        minHeight: !1,
        maxHeight: !1,
        linebreaks: !1,
        replaceDivs: !0,
        paragraphize: !0,
        cleanStyleOnEnter: !1,
        enterKey: !0,
        cleanOnPaste: !0,
        cleanSpaces: !0,
        pastePlainText: !1,
        autosave: !1,
        autosaveName: !1,
        autosaveInterval: 60,
        autosaveOnChange: !1,
        autosaveFields: !1,
        linkTooltip: !0,
        linkProtocol: "http",
        linkNofollow: !1,
        linkSize: 50,
        imageEditable: !0,
        imageLink: !0,
        imagePosition: !0,
        imageFloatMargin: "10px",
        imageResizable: !0,
        imageUpload: null,
        imageUploadParam: "file",
        uploadImageField: !1,
        dragImageUpload: !0,
        fileUpload: null,
        fileUploadParam: "file",
        dragFileUpload: !0,
        s3: !1,
        convertLinks: !0,
        convertUrlLinks: !0,
        convertImageLinks: !0,
        convertVideoLinks: !0,
        preSpaces: 4,
        tabAsSpaces: !1,
        tabKey: !0,
        scrollTarget: !1,
        toolbar: !0,
        toolbarFixed: !0,
        toolbarFixedTarget: document,
        toolbarFixedTopOffset: 0,
        toolbarExternal: !1,
        toolbarOverflow: !1,
        source: !0,
        buttons: [ "html", "formatting", "bold", "italic", "deleted", "unorderedlist", "orderedlist", "outdent", "indent", "image", "file", "link", "alignment", "horizontalrule" ],
        buttonsHide: [],
        buttonsHideOnMobile: [],
        formatting: [ "p", "blockquote", "pre", "h1", "h2", "h3", "h4", "h5", "h6" ],
        formattingAdd: !1,
        tabifier: !0,
        deniedTags: [ "script", "style" ],
        allowedTags: !1,
        paragraphizeBlocks: [ "table", "div", "pre", "form", "ul", "ol", "h1", "h2", "h3", "h4", "h5", "h6", "dl", "blockquote", "figcaption", "address", "section", "header", "footer", "aside", "article", "object", "style", "script", "iframe", "select", "input", "textarea", "button", "option", "map", "area", "math", "hr", "fieldset", "legend", "hgroup", "nav", "figure", "details", "menu", "summary", "p" ],
        removeComments: !1,
        replaceTags: [ [ "strike", "del" ], [ "b", "strong" ] ],
        replaceStyles: [ [ "font-weight:\\s?bold", "strong" ], [ "font-style:\\s?italic", "em" ], [ "text-decoration:\\s?underline", "u" ], [ "text-decoration:\\s?line-through", "del" ] ],
        removeDataAttr: !1,
        removeAttr: !1,
        allowedAttr: !1,
        removeWithoutAttr: [ "span" ],
        removeEmpty: [ "p" ],
        activeButtons: [ "deleted", "italic", "bold", "underline", "unorderedlist", "orderedlist", "alignleft", "aligncenter", "alignright", "justify" ],
        activeButtonsStates: {
            b: "bold",
            strong: "bold",
            i: "italic",
            em: "italic",
            del: "deleted",
            strike: "deleted",
            ul: "unorderedlist",
            ol: "orderedlist",
            u: "underline"
        },
        shortcuts: {
            "ctrl+shift+m, meta+shift+m": {
                func: "inline.removeFormat"
            },
            "ctrl+b, meta+b": {
                func: "inline.format",
                params: [ "bold" ]
            },
            "ctrl+i, meta+i": {
                func: "inline.format",
                params: [ "italic" ]
            },
            "ctrl+h, meta+h": {
                func: "inline.format",
                params: [ "superscript" ]
            },
            "ctrl+l, meta+l": {
                func: "inline.format",
                params: [ "subscript" ]
            },
            "ctrl+k, meta+k": {
                func: "link.show"
            },
            "ctrl+shift+7": {
                func: "list.toggle",
                params: [ "orderedlist" ]
            },
            "ctrl+shift+8": {
                func: "list.toggle",
                params: [ "unorderedlist" ]
            }
        },
        shortcutsAdd: !1,
        buffer: [],
        rebuffer: [],
        emptyHtml: "<p>&#x200b;</p>",
        invisibleSpace: "&#x200b;",
        imageTypes: [ "image/png", "image/jpeg", "image/gif" ],
        indentValue: 20,
        verifiedTags: [ "a", "img", "b", "strong", "sub", "sup", "i", "em", "u", "small", "strike", "del", "cite", "ul", "ol", "li" ],
        inlineTags: [ "strong", "b", "u", "em", "i", "code", "del", "ins", "samp", "kbd", "sup", "sub", "mark", "var", "cite", "small" ],
        alignmentTags: [ "P", "H1", "H2", "H3", "H4", "H5", "H6", "DL", "DT", "DD", "DIV", "TD", "BLOCKQUOTE", "OUTPUT", "FIGCAPTION", "ADDRESS", "SECTION", "HEADER", "FOOTER", "ASIDE", "ARTICLE" ],
        blockLevelElements: [ "PRE", "UL", "OL", "LI" ],
        highContrast: !1,
        observe: {
            dropdowns: []
        },
        langs: {
            en: {
                html: "HTML",
                video: "Insert Video",
                image: "Insert Image",
                table: "Table",
                link: "Link",
                link_insert: "Insert link",
                link_edit: "Edit link",
                unlink: "Unlink",
                formatting: "Formatting",
                paragraph: "Normal text",
                quote: "Quote",
                code: "Code",
                header1: "Header 1",
                header2: "Header 2",
                header3: "Header 3",
                header4: "Header 4",
                header5: "Header 5",
                bold: "Bold",
                italic: "Italic",
                fontcolor: "Font Color",
                backcolor: "Back Color",
                unorderedlist: "Unordered List",
                orderedlist: "Ordered List",
                outdent: "Outdent",
                indent: "Indent",
                cancel: "Cancel",
                insert: "Insert",
                save: "Save",
                _delete: "Delete",
                insert_table: "Insert Table",
                insert_row_above: "Add Row Above",
                insert_row_below: "Add Row Below",
                insert_column_left: "Add Column Left",
                insert_column_right: "Add Column Right",
                delete_column: "Delete Column",
                delete_row: "Delete Row",
                delete_table: "Delete Table",
                rows: "Rows",
                columns: "Columns",
                add_head: "Add Head",
                delete_head: "Delete Head",
                title: "Title",
                image_position: "Position",
                none: "None",
                left: "Left",
                right: "Right",
                center: "Center",
                image_web_link: "Image Web Link",
                text: "Text",
                mailto: "Email",
                web: "URL",
                video_html_code: "Video Embed Code or Youtube/Vimeo Link",
                file: "Insert File",
                upload: "Upload",
                download: "Download",
                choose: "Choose",
                or_choose: "Or choose",
                drop_file_here: "Drop file here",
                align_left: "Align text to the left",
                align_center: "Center text",
                align_right: "Align text to the right",
                align_justify: "Justify text",
                horizontalrule: "Insert Horizontal Rule",
                deleted: "Deleted",
                anchor: "Anchor",
                link_new_tab: "Open link in new tab",
                underline: "Underline",
                alignment: "Alignment",
                filename: "Name (optional)",
                edit: "Edit",
                upload_label: "Drop file here or "
            }
        },
        linkify: {
            regexps: {
                youtube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.\-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/gi,
                vimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
                image: /((https?|www)[^\s]+\.)(jpe?g|png|gif)(\?[^\s-]+)?/gi,
                url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/gi
            }
        },
        codemirror: !1
    }, Redactor.fn = $.Redactor.prototype = {
        keyCode: {
            BACKSPACE: 8,
            DELETE: 46,
            UP: 38,
            DOWN: 40,
            ENTER: 13,
            SPACE: 32,
            ESC: 27,
            TAB: 9,
            CTRL: 17,
            META: 91,
            SHIFT: 16,
            ALT: 18,
            RIGHT: 39,
            LEFT: 37,
            LEFT_WIN: 91
        },
        init: function(el, options) {
            if (this.$element = $(el), this.uuid = uuid++, this.rtePaste = !1, this.$pasteBox = !1, 
            this.loadOptions(options), this.loadModules(), this.formatting = {}, $.merge(this.opts.blockLevelElements, this.opts.alignmentTags), 
            this.reIsBlock = new RegExp("^(" + this.opts.blockLevelElements.join("|") + ")$", "i"), 
            this.tidy.setupAllowed(), this.opts.deniedTags !== !1) for (var tags = [ "html", "head", "link", "body", "meta", "applet" ], i = 0; i < tags.length; i++) this.opts.deniedTags.push(tags[i]);
            this.lang.load(), $.extend(this.opts.shortcuts, this.opts.shortcutsAdd), this.core.setCallback("start"), 
            this.start = !0, this.build.run();
        },
        loadOptions: function(options) {
            this.opts = $.extend({}, $.extend(!0, {}, $.Redactor.opts), this.$element.data(), options);
        },
        getModuleMethods: function(object) {
            return Object.getOwnPropertyNames(object).filter(function(property) {
                return "function" == typeof object[property];
            });
        },
        loadModules: function() {
            for (var len = $.Redactor.modules.length, i = 0; len > i; i++) this.bindModuleMethods($.Redactor.modules[i]);
        },
        bindModuleMethods: function(module) {
            if ("undefined" != typeof this[module]) {
                this[module] = this[module]();
                for (var methods = this.getModuleMethods(this[module]), len = methods.length, z = 0; len > z; z++) this[module][methods[z]] = this[module][methods[z]].bind(this);
            }
        },
        alignment: function() {
            return {
                left: function() {
                    this.alignment.set("");
                },
                right: function() {
                    this.alignment.set("right");
                },
                center: function() {
                    this.alignment.set("center");
                },
                justify: function() {
                    this.alignment.set("justify");
                },
                set: function(type) {
                    this.utils.browser("msie") || this.$editor.focus(), this.buffer.set(), this.selection.save(), 
                    this.alignment.blocks = this.selection.getBlocks(), this.alignment.type = type, 
                    this.alignment.isLinebreaksOrNoBlocks() ? this.alignment.setText() : this.alignment.setBlocks(), 
                    this.selection.restore(), this.code.sync();
                },
                setText: function() {
                    var wrapper = this.selection.wrap("div");
                    $(wrapper).attr("data-tagblock", "redactor").css("text-align", this.alignment.type);
                },
                setBlocks: function() {
                    $.each(this.alignment.blocks, $.proxy(function(i, el) {
                        var $el = this.utils.getAlignmentElement(el);
                        $el && (this.alignment.isNeedReplaceElement($el) ? this.alignment.replaceElement($el) : this.alignment.alignElement($el));
                    }, this));
                },
                isLinebreaksOrNoBlocks: function() {
                    return this.opts.linebreaks && this.alignment.blocks[0] === !1;
                },
                isNeedReplaceElement: function($el) {
                    return "" === this.alignment.type && "undefined" != typeof $el.data("tagblock");
                },
                replaceElement: function($el) {
                    $el.replaceWith($el.html());
                },
                alignElement: function($el) {
                    $el.css("text-align", this.alignment.type), this.utils.removeEmptyAttr($el, "style");
                }
            };
        },
        autosave: function() {
            return {
                html: !1,
                enable: function() {
                    this.opts.autosave && (this.autosave.name = this.opts.autosaveName ? this.opts.autosaveName : this.$textarea.attr("name"), 
                    this.opts.autosaveOnChange || (this.autosaveInterval = setInterval(this.autosave.load, 1e3 * this.opts.autosaveInterval)));
                },
                onChange: function() {
                    this.opts.autosaveOnChange && this.autosave.load();
                },
                load: function() {
                    if (this.opts.autosave && (this.autosave.source = this.code.get(), this.autosave.html !== this.autosave.source)) {
                        var data = {};
                        data.name = this.autosave.name, data[this.autosave.name] = this.autosave.source, 
                        data = this.autosave.getHiddenFields(data);
                        var jsxhr = $.ajax({
                            url: this.opts.autosave,
                            type: "post",
                            data: data
                        });
                        jsxhr.done(this.autosave.success);
                    }
                },
                getHiddenFields: function(data) {
                    return this.opts.autosaveFields === !1 || "object" != typeof this.opts.autosaveFields ? data : ($.each(this.opts.autosaveFields, $.proxy(function(k, v) {
                        null !== v && 0 === v.toString().indexOf("#") && (v = $(v).val()), data[k] = v;
                    }, this)), data);
                },
                success: function(data) {
                    var json;
                    try {
                        json = $.parseJSON(data);
                    } catch (e) {
                        json = data;
                    }
                    var callbackName = "undefined" == typeof json.error ? "autosave" : "autosaveError";
                    this.core.setCallback(callbackName, this.autosave.name, json), this.autosave.html = this.autosave.source;
                },
                disable: function() {
                    clearInterval(this.autosaveInterval);
                }
            };
        },
        block: function() {
            return {
                formatting: function(name) {
                    this.block.clearStyle = !1;
                    var type, value;
                    "undefined" != typeof this.formatting[name].data ? type = "data" : "undefined" != typeof this.formatting[name].attr ? type = "attr" : "undefined" != typeof this.formatting[name]["class"] && (type = "class"), 
                    "undefined" != typeof this.formatting[name].clear && (this.block.clearStyle = !0), 
                    type && (value = this.formatting[name][type]), this.block.format(this.formatting[name].tag, type, value);
                },
                format: function(tag, type, value) {
                    "quote" == tag && (tag = "blockquote");
                    var formatTags = [ "p", "pre", "blockquote", "h1", "h2", "h3", "h4", "h5", "h6" ];
                    if (-1 != $.inArray(tag, formatTags)) {
                        this.block.isRemoveInline = "pre" == tag || -1 != tag.search(/h[1-6]/i), this.utils.browser("msie") || this.$editor.focus();
                        var html = $.trim(this.$editor.html());
                        if (this.block.isEmpty = this.utils.isEmpty(html), this.utils.browser("mozilla") && !this.focus.isFocused() && this.block.isEmpty) {
                            var $first;
                            this.opts.linebreaks || ($first = this.$editor.children().first(), this.caret.setEnd($first));
                        }
                        this.block.blocks = this.selection.getBlocks(), this.block.blocksSize = this.block.blocks.length, 
                        this.block.type = type, this.block.value = value, this.buffer.set(), this.selection.save(), 
                        this.block.set(tag), this.selection.restore(), this.code.sync(), this.observe.load();
                    }
                },
                set: function(tag) {
                    this.selection.get(), this.block.containerTag = this.range.commonAncestorContainer.tagName, 
                    this.range.collapsed ? this.block.setCollapsed(tag) : this.block.setMultiple(tag);
                },
                setCollapsed: function(tag) {
                    if (this.opts.linebreaks && this.block.isEmpty && "p" != tag) {
                        var node = document.createElement(tag);
                        return this.$editor.html(node), void this.caret.setEnd(node);
                    }
                    var block = this.block.blocks[0];
                    if (block !== !1) {
                        if ("LI" == block.tagName) {
                            if ("blockquote" != tag) return;
                            return void this.block.formatListToBlockquote();
                        }
                        var isContainerTable = "TD" == this.block.containerTag || "TH" == this.block.containerTag;
                        if (isContainerTable && !this.opts.linebreaks) document.execCommand("formatblock", !1, "<" + tag + ">"), 
                        block = this.selection.getBlock(), this.block.toggle($(block)); else if (block.tagName.toLowerCase() != tag) if (this.opts.linebreaks && "p" == tag) $(block).append("<br>"), 
                        this.utils.replaceWithContents(block); else {
                            var $formatted = this.utils.replaceToTag(block, tag);
                            this.block.toggle($formatted), "p" != tag && "blockquote" != tag && $formatted.find("img").remove(), 
                            this.block.isRemoveInline && this.utils.removeInlineTags($formatted), ("p" == tag || this.block.headTag) && $formatted.find("p").contents().unwrap(), 
                            this.block.formatTableWrapping($formatted);
                        } else if ("blockquote" == tag && block.tagName.toLowerCase() == tag) if (this.opts.linebreaks) $(block).append("<br>"), 
                        this.utils.replaceWithContents(block); else {
                            var $el = this.utils.replaceToTag(block, "p");
                            this.block.toggle($el);
                        } else block.tagName.toLowerCase() == tag && this.block.toggle($(block));
                        "undefined" == typeof this.block.type && "undefined" == typeof this.block.value && $(block).removeAttr("class").removeAttr("style");
                    }
                },
                setMultiple: function(tag) {
                    var block = this.block.blocks[0], isContainerTable = "TD" == this.block.containerTag || "TH" == this.block.containerTag;
                    if (block !== !1 && 1 === this.block.blocksSize) if (block.tagName.toLowerCase() == tag && "blockquote" == tag) if (this.opts.linebreaks) $(block).append("<br>"), 
                    this.utils.replaceWithContents(block); else {
                        var $el = this.utils.replaceToTag(block, "p");
                        this.block.toggle($el);
                    } else if ("LI" == block.tagName) {
                        if ("blockquote" != tag) return;
                        this.block.formatListToBlockquote();
                    } else if ("BLOCKQUOTE" == this.block.containerTag) this.block.formatBlockquote(tag); else if (this.opts.linebreaks && (isContainerTable || this.range.commonAncestorContainer != block)) this.block.formatWrap(tag); else if (this.opts.linebreaks && "p" == tag) $(block).prepend("<br>").append("<br>"), 
                    this.utils.replaceWithContents(block); else if ("TD" === block.tagName) this.block.formatWrap(tag); else {
                        var $formatted = this.utils.replaceToTag(block, tag);
                        this.block.toggle($formatted), this.block.isRemoveInline && this.utils.removeInlineTags($formatted), 
                        ("p" == tag || this.block.headTag) && $formatted.find("p").contents().unwrap();
                    } else if (this.opts.linebreaks || "p" != tag) {
                        if ("blockquote" == tag) {
                            for (var count = 0, i = 0; i < this.block.blocksSize; i++) "BLOCKQUOTE" == this.block.blocks[i].tagName && count++;
                            if (count == this.block.blocksSize) return void $.each(this.block.blocks, $.proxy(function(i, s) {
                                var $formatted = !1;
                                this.opts.linebreaks ? ($(s).prepend("<br>").append("<br>"), $formatted = this.utils.replaceWithContents(s)) : $formatted = this.utils.replaceToTag(s, "p"), 
                                $formatted && "undefined" == typeof this.block.type && "undefined" == typeof this.block.value && $formatted.removeAttr("class").removeAttr("style");
                            }, this));
                        }
                        this.block.formatWrap(tag);
                    } else {
                        var classSize = 0, toggleType = !1;
                        "class" == this.block.type && (toggleType = "toggle", classSize = $(this.block.blocks).filter("." + this.block.value).length, 
                        this.block.blocksSize == classSize ? toggleType = "toggle" : this.block.blocksSize > classSize ? toggleType = "set" : 0 === classSize && (toggleType = "set"));
                        var exceptTags = [ "ul", "ol", "li", "td", "th", "dl", "dt", "dd" ];
                        $.each(this.block.blocks, $.proxy(function(i, s) {
                            if (-1 == $.inArray(s.tagName.toLowerCase(), exceptTags)) {
                                var $formatted = this.utils.replaceToTag(s, tag);
                                toggleType ? "toggle" == toggleType ? this.block.toggle($formatted) : "remove" == toggleType ? this.block.remove($formatted) : "set" == toggleType && this.block.setForce($formatted) : this.block.toggle($formatted), 
                                "p" != tag && "blockquote" != tag && $formatted.find("img").remove(), this.block.isRemoveInline && this.utils.removeInlineTags($formatted), 
                                ("p" == tag || this.block.headTag) && $formatted.find("p").contents().unwrap(), 
                                "undefined" == typeof this.block.type && "undefined" == typeof this.block.value && $formatted.removeAttr("class").removeAttr("style");
                            }
                        }, this));
                    }
                },
                setForce: function($el) {
                    return this.block.clearStyle && $el.removeAttr("class").removeAttr("style"), "class" == this.block.type ? void $el.addClass(this.block.value) : "attr" == this.block.type || "data" == this.block.type ? void $el.attr(this.block.value.name, this.block.value.value) : void 0;
                },
                toggle: function($el) {
                    return this.block.clearStyle && $el.removeAttr("class").removeAttr("style"), "class" == this.block.type ? void $el.toggleClass(this.block.value) : "attr" == this.block.type || "data" == this.block.type ? void ($el.attr(this.block.value.name) == this.block.value.value ? $el.removeAttr(this.block.value.name) : $el.attr(this.block.value.name, this.block.value.value)) : void $el.removeAttr("style class");
                },
                remove: function($el) {
                    $el.removeClass(this.block.value);
                },
                formatListToBlockquote: function() {
                    var block = $(this.block.blocks[0]).closest("ul, ol", this.$editor[0]);
                    $(block).find("ul, ol").contents().unwrap(), $(block).find("li").append($("<br>")).contents().unwrap();
                    var $el = this.utils.replaceToTag(block, "blockquote");
                    this.block.toggle($el);
                },
                formatBlockquote: function(tag) {
                    document.execCommand("outdent"), document.execCommand("formatblock", !1, tag), this.clean.clearUnverified(), 
                    this.$editor.find("p:empty").remove();
                    var formatted = this.selection.getBlock();
                    "p" != tag && $(formatted).find("img").remove(), this.opts.linebreaks || this.block.toggle($(formatted)), 
                    this.$editor.find("ul, ol, tr, blockquote, p").each($.proxy(this.utils.removeEmpty, this)), 
                    this.opts.linebreaks && "p" == tag && this.utils.replaceWithContents(formatted);
                },
                formatWrap: function(tag) {
                    if ("UL" == this.block.containerTag || "OL" == this.block.containerTag) {
                        if ("blockquote" != tag) return;
                        this.block.formatListToBlockquote();
                    }
                    var formatted = this.selection.wrap(tag);
                    if (formatted !== !1) {
                        var $formatted = $(formatted);
                        this.block.formatTableWrapping($formatted);
                        var $elements = $formatted.find(this.opts.blockLevelElements.join(",") + ", td, table, thead, tbody, tfoot, th, tr");
                        if ($elements.contents().unwrap(), "p" != tag && "blockquote" != tag && $formatted.find("img").remove(), 
                        $.each(this.block.blocks, $.proxy(this.utils.removeEmpty, this)), $formatted.append(this.selection.getMarker(2)), 
                        this.opts.linebreaks || this.block.toggle($formatted), this.$editor.find("ul, ol, tr, blockquote, p").each($.proxy(this.utils.removeEmpty, this)), 
                        $formatted.find("blockquote:empty").remove(), this.block.isRemoveInline && this.utils.removeInlineTags($formatted), 
                        this.opts.linebreaks && "p" == tag && this.utils.replaceWithContents($formatted), 
                        this.opts.linebreaks) {
                            var $next = $formatted.next().next();
                            0 != $next.size() && "BR" === $next[0].tagName && $next.remove();
                        }
                    }
                },
                formatTableWrapping: function($formatted) {
                    0 !== $formatted.closest("table", this.$editor[0]).length && (0 === $formatted.closest("tr", this.$editor[0]).length && $formatted.wrap("<tr>"), 
                    0 === $formatted.closest("td", this.$editor[0]).length && 0 === $formatted.closest("th").length && $formatted.wrap("<td>"));
                },
                removeData: function(name, value) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).removeAttr("data-" + name), this.code.sync();
                },
                setData: function(name, value) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).attr("data-" + name, value), this.code.sync();
                },
                toggleData: function(name, value) {
                    var blocks = this.selection.getBlocks();
                    $.each(blocks, function() {
                        $(this).attr("data-" + name) ? $(this).removeAttr("data-" + name) : $(this).attr("data-" + name, value);
                    });
                },
                removeAttr: function(attr, value) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).removeAttr(attr), this.code.sync();
                },
                setAttr: function(attr, value) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).attr(attr, value), this.code.sync();
                },
                toggleAttr: function(attr, value) {
                    var blocks = this.selection.getBlocks();
                    $.each(blocks, function() {
                        $(this).attr(name) ? $(this).removeAttr(name) : $(this).attr(name, value);
                    });
                },
                removeClass: function(className) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).removeClass(className), this.utils.removeEmptyAttr(blocks, "class"), this.code.sync();
                },
                setClass: function(className) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).addClass(className), this.code.sync();
                },
                toggleClass: function(className) {
                    var blocks = this.selection.getBlocks();
                    $(blocks).toggleClass(className), this.code.sync();
                }
            };
        },
        buffer: function() {
            return {
                set: function(type) {
                    "undefined" == typeof type || "undo" == type ? this.buffer.setUndo() : this.buffer.setRedo();
                },
                setUndo: function() {
                    this.selection.save(), this.opts.buffer.push(this.$editor.html()), this.selection.restore();
                },
                setRedo: function() {
                    this.selection.save(), this.opts.rebuffer.push(this.$editor.html()), this.selection.restore();
                },
                getUndo: function() {
                    this.$editor.html(this.opts.buffer.pop());
                },
                getRedo: function() {
                    this.$editor.html(this.opts.rebuffer.pop());
                },
                add: function() {
                    this.opts.buffer.push(this.$editor.html());
                },
                undo: function() {
                    0 !== this.opts.buffer.length && (this.buffer.set("redo"), this.buffer.getUndo(), 
                    this.selection.restore(), setTimeout($.proxy(this.observe.load, this), 50));
                },
                redo: function() {
                    0 !== this.opts.rebuffer.length && (this.buffer.set("undo"), this.buffer.getRedo(), 
                    this.selection.restore(), setTimeout($.proxy(this.observe.load, this), 50));
                }
            };
        },
        build: function() {
            return {
                run: function() {
                    this.build.createContainerBox(), this.build.loadContent(), this.build.loadEditor(), 
                    this.build.enableEditor(), this.build.setCodeAndCall();
                },
                isTextarea: function() {
                    return "TEXTAREA" === this.$element[0].tagName;
                },
                createContainerBox: function() {
                    this.$box = $('<div class="redactor-box" role="application" />');
                },
                createTextarea: function() {
                    this.$textarea = $("<textarea />").attr("name", this.build.getTextareaName());
                },
                getTextareaName: function() {
                    return "undefined" == typeof name ? "content-" + this.uuid : this.$element.attr("id");
                },
                loadContent: function() {
                    var func = this.build.isTextarea() ? "val" : "html";
                    this.content = $.trim(this.$element[func]());
                },
                enableEditor: function() {
                    this.$editor.attr({
                        contenteditable: !0,
                        dir: this.opts.direction
                    });
                },
                loadEditor: function() {
                    var func = this.build.isTextarea() ? "fromTextarea" : "fromElement";
                    this.build[func]();
                },
                fromTextarea: function() {
                    this.$editor = $("<div />"), this.$textarea = this.$element, this.$box.insertAfter(this.$element).append(this.$editor).append(this.$element), 
                    this.$editor.addClass("redactor-editor"), this.$element.hide();
                },
                fromElement: function() {
                    this.$editor = this.$element, this.build.createTextarea(), this.$box.insertAfter(this.$editor).append(this.$editor).append(this.$textarea), 
                    this.$editor.addClass("redactor-editor"), this.$textarea.hide();
                },
                setCodeAndCall: function() {
                    this.code.set(this.content), this.build.setOptions(), this.build.callEditor(), this.opts.visual || setTimeout($.proxy(this.code.showCode, this), 200);
                },
                callEditor: function() {
                    this.build.disableMozillaEditing(), this.build.disableIeLinks(), this.build.setEvents(), 
                    this.build.setHelpers(), this.opts.toolbar && (this.opts.toolbar = this.toolbar.init(), 
                    this.toolbar.build()), this.modal.loadTemplates(), this.build.plugins(), setTimeout($.proxy(this.observe.load, this), 4), 
                    this.core.setCallback("init");
                },
                setOptions: function() {
                    $(this.$textarea).attr("dir", this.opts.direction), this.opts.linebreaks && this.$editor.addClass("redactor-linebreaks"), 
                    this.opts.tabindex && this.$editor.attr("tabindex", this.opts.tabindex), this.opts.minHeight && this.$editor.css("minHeight", this.opts.minHeight), 
                    this.opts.maxHeight && this.$editor.css("maxHeight", this.opts.maxHeight);
                },
                setEventDropUpload: function(e) {
                    if (e.preventDefault(), this.opts.dragImageUpload && this.opts.dragFileUpload) {
                        var files = e.dataTransfer.files;
                        this.upload.directUpload(files[0], e);
                    }
                },
                setEventDrop: function(e) {
                    this.code.sync(), setTimeout(this.clean.clearUnverified, 1), this.core.setCallback("drop", e);
                },
                setEvents: function() {
                    this.$editor.on("drop.redactor", $.proxy(function(e) {
                        return e = e.originalEvent || e, void 0 !== window.FormData && e.dataTransfer ? 0 === e.dataTransfer.files.length ? this.build.setEventDrop(e) : (this.build.setEventDropUpload(e), 
                        setTimeout(this.clean.clearUnverified, 1), void this.core.setCallback("drop", e)) : !0;
                    }, this)), this.$editor.on("click.redactor", $.proxy(function(e) {
                        var event = this.core.getEvent(), type = "click" == event || "arrow" == event ? !1 : "click";
                        this.core.addEvent(type), this.utils.disableSelectAll(), this.core.setCallback("click", e);
                    }, this)), this.$editor.on("paste.redactor", $.proxy(this.paste.init, this)), this.$editor.on("cut.redactor", $.proxy(this.code.sync, this)), 
                    this.$editor.on("keydown.redactor", $.proxy(this.keydown.init, this)), this.$editor.on("keyup.redactor", $.proxy(this.keyup.init, this)), 
                    $.isFunction(this.opts.codeKeydownCallback) && this.$textarea.on("keydown.redactor-textarea", $.proxy(this.opts.codeKeydownCallback, this)), 
                    $.isFunction(this.opts.codeKeyupCallback) && this.$textarea.on("keyup.redactor-textarea", $.proxy(this.opts.codeKeyupCallback, this)), 
                    this.$editor.on("focus.redactor", $.proxy(function(e) {
                        $.isFunction(this.opts.focusCallback) && this.core.setCallback("focus", e), this.selection.getCurrent() === !1 && (this.selection.get(), 
                        this.range.setStart(this.$editor[0], 0), this.range.setEnd(this.$editor[0], 0), 
                        this.selection.addRange());
                    }, this)), $(document).on("mousedown.redactor-blur." + this.uuid, $.proxy(function(e) {
                        this.start || this.rtePaste || 0 === $(e.target).closest(".redactor-editor, .redactor-toolbar, .redactor-dropdown").size() && (this.utils.disableSelectAll(), 
                        $.isFunction(this.opts.blurCallback) && this.core.setCallback("blur", e));
                    }, this));
                },
                setHelpers: function() {
                    this.linkify.isEnabled() && this.linkify.format(), this.placeholder.enable(), this.opts.focus && setTimeout(this.focus.setStart, 100), 
                    this.opts.focusEnd && setTimeout(this.focus.setEnd, 100);
                },
                plugins: function() {
                    this.opts.plugins && $.each(this.opts.plugins, $.proxy(function(i, s) {
                        var func = "undefined" != typeof RedactorPlugins && "undefined" != typeof RedactorPlugins[s] ? RedactorPlugins : Redactor.fn;
                        if ($.isFunction(func[s])) {
                            this[s] = func[s]();
                            for (var methods = this.getModuleMethods(this[s]), len = methods.length, z = 0; len > z; z++) this[s][methods[z]] = this[s][methods[z]].bind(this);
                            $.isFunction(this[s].init) && this[s].init();
                        }
                    }, this));
                },
                disableMozillaEditing: function() {
                    if (this.utils.browser("mozilla")) try {
                        document.execCommand("enableObjectResizing", !1, !1), document.execCommand("enableInlineTableEditing", !1, !1);
                    } catch (e) {}
                },
                disableIeLinks: function() {
                    this.utils.browser("msie") && document.execCommand("AutoUrlDetect", !1, !1);
                }
            };
        },
        button: function() {
            return {
                build: function(btnName, btnObject) {
                    var $button = $('<a href="#" class="re-icon re-' + btnName + '" rel="' + btnName + '" />').attr({
                        role: "button",
                        "aria-label": btnObject.title,
                        tabindex: "-1"
                    });
                    if ((btnObject.func || btnObject.command || btnObject.dropdown) && this.button.setEvent($button, btnName, btnObject), 
                    btnObject.dropdown) {
                        $button.addClass("redactor-toolbar-link-dropdown").attr("aria-haspopup", !0);
                        var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + " redactor-dropdown-box-" + btnName + '" style="display: none;">');
                        $button.data("dropdown", $dropdown), this.dropdown.build(btnName, $dropdown, btnObject.dropdown);
                    }
                    return this.utils.isDesktop() && this.button.createTooltip($button, btnName, btnObject.title), 
                    $button;
                },
                setEvent: function($button, btnName, btnObject) {
                    $button.on("touchstart click", $.proxy(function(e) {
                        if ($button.hasClass("redactor-button-disabled")) return !1;
                        var type = "func", callback = btnObject.func;
                        btnObject.command ? (type = "command", callback = btnObject.command) : btnObject.dropdown && (type = "dropdown", 
                        callback = !1), this.button.onClick(e, btnName, type, callback);
                    }, this));
                },
                createTooltip: function($button, name, title) {
                    var $tooltip = $("<span>").addClass("redactor-toolbar-tooltip redactor-toolbar-tooltip-" + this.uuid + " redactor-toolbar-tooltip-" + name).hide().html(title);
                    $tooltip.appendTo("body"), $button.on("mouseover", function() {
                        if (!$(this).hasClass("redactor-button-disabled")) {
                            var pos = $button.offset();
                            $tooltip.show(), $tooltip.css({
                                top: pos.top + $button.innerHeight() + "px",
                                left: pos.left + $button.innerWidth() / 2 - $tooltip.innerWidth() / 2 + "px"
                            });
                        }
                    }), $button.on("mouseout", function() {
                        $tooltip.hide();
                    });
                },
                onClick: function(e, btnName, type, callback) {
                    this.button.caretOffset = this.caret.getOffset(), e.preventDefault(), $(document).find(".redactor-toolbar-tooltip").hide(), 
                    this.utils.browser("msie") && (e.returnValue = !1), "command" == type ? this.inline.format(callback) : "dropdown" == type ? this.dropdown.show(e, btnName) : this.button.onClickCallback(e, callback, btnName);
                },
                onClickCallback: function(e, callback, btnName) {
                    var func;
                    if ($.isFunction(callback)) callback.call(this, btnName); else if ("-1" != callback.search(/\./)) {
                        if (func = callback.split("."), "undefined" == typeof this[func[0]]) return;
                        this[func[0]][func[1]](btnName);
                    } else this[callback](btnName);
                    this.observe.buttons(e, btnName);
                },
                get: function(key) {
                    return this.$toolbar.find("a.re-" + key);
                },
                setActive: function(key) {
                    this.button.get(key).addClass("redactor-act");
                },
                setInactive: function(key) {
                    this.button.get(key).removeClass("redactor-act");
                },
                setInactiveAll: function(key) {
                    "undefined" == typeof key ? this.$toolbar.find("a.re-icon").removeClass("redactor-act") : this.$toolbar.find("a.re-icon").not(".re-" + key).removeClass("redactor-act");
                },
                setActiveInVisual: function() {
                    this.$toolbar.find("a.re-icon").not("a.re-html, a.re-fullscreen").removeClass("redactor-button-disabled");
                },
                setInactiveInCode: function() {
                    this.$toolbar.find("a.re-icon").not("a.re-html, a.re-fullscreen").addClass("redactor-button-disabled");
                },
                changeIcon: function(key, classname) {
                    this.button.get(key).addClass("re-" + classname);
                },
                removeIcon: function(key, classname) {
                    this.button.get(key).removeClass("re-" + classname);
                },
                setAwesome: function(key, name) {
                    var $button = this.button.get(key);
                    $button.removeClass("redactor-btn-image").addClass("fa-redactor-btn"), $button.html('<i class="fa ' + name + '"></i>');
                },
                addCallback: function($btn, callback) {
                    if ("buffer" != $btn) {
                        var type = "dropdown" == callback ? "dropdown" : "func", key = $btn.attr("rel");
                        $btn.on("touchstart click", $.proxy(function(e) {
                            return $btn.hasClass("redactor-button-disabled") ? !1 : void this.button.onClick(e, key, type, callback);
                        }, this));
                    }
                },
                addDropdown: function($btn, dropdown) {
                    $btn.addClass("redactor-toolbar-link-dropdown").attr("aria-haspopup", !0);
                    var key = $btn.attr("rel");
                    this.button.addCallback($btn, "dropdown");
                    var $dropdown = $('<div class="redactor-dropdown redactor-dropdown-' + this.uuid + " redactor-dropdown-box-" + key + '" style="display: none;">');
                    return $btn.data("dropdown", $dropdown), dropdown && this.dropdown.build(key, $dropdown, dropdown), 
                    $dropdown;
                },
                add: function(key, title) {
                    if (this.opts.toolbar) {
                        if (this.button.isMobileUndoRedo(key)) return "buffer";
                        var btn = this.button.build(key, {
                            title: title
                        });
                        return btn.addClass("redactor-btn-image"), this.$toolbar.append($("<li>").append(btn)), 
                        btn;
                    }
                },
                addFirst: function(key, title) {
                    if (this.opts.toolbar) {
                        if (this.button.isMobileUndoRedo(key)) return "buffer";
                        var btn = this.button.build(key, {
                            title: title
                        });
                        return btn.addClass("redactor-btn-image"), this.$toolbar.prepend($("<li>").append(btn)), 
                        btn;
                    }
                },
                addAfter: function(afterkey, key, title) {
                    if (this.opts.toolbar) {
                        if (this.button.isMobileUndoRedo(key)) return "buffer";
                        var btn = this.button.build(key, {
                            title: title
                        });
                        btn.addClass("redactor-btn-image");
                        var $btn = this.button.get(afterkey);
                        return 0 !== $btn.length ? $btn.parent().after($("<li>").append(btn)) : this.$toolbar.append($("<li>").append(btn)), 
                        btn;
                    }
                },
                addBefore: function(beforekey, key, title) {
                    if (this.opts.toolbar) {
                        if (this.button.isMobileUndoRedo(key)) return "buffer";
                        var btn = this.button.build(key, {
                            title: title
                        });
                        btn.addClass("redactor-btn-image");
                        var $btn = this.button.get(beforekey);
                        return 0 !== $btn.length ? $btn.parent().before($("<li>").append(btn)) : this.$toolbar.append($("<li>").append(btn)), 
                        btn;
                    }
                },
                remove: function(key) {
                    this.button.get(key).remove();
                },
                isMobileUndoRedo: function(key) {
                    return ("undo" == key || "redo" == key) && !this.utils.isDesktop();
                }
            };
        },
        caret: function() {
            return {
                setStart: function(node) {
                    if (this.utils.isBlock(node)) this.caret.set(node, 0, node, 0); else {
                        var space = this.utils.createSpaceElement();
                        $(node).prepend(space), this.caret.setEnd(space);
                    }
                },
                setEnd: function(node) {
                    return node = node[0] || node, 1 == node.lastChild.nodeType ? this.caret.setAfter(node.lastChild) : void this.caret.set(node, 1, node, 1);
                },
                set: function(orgn, orgo, focn, foco) {
                    if (orgn = orgn[0] || orgn, focn = focn[0] || focn, this.utils.isBlockTag(orgn.tagName) && "" === orgn.innerHTML && (orgn.innerHTML = this.opts.invisibleSpace), 
                    "BR" == orgn.tagName && this.opts.linebreaks === !1) {
                        var parent = $(this.opts.emptyHtml)[0];
                        $(orgn).replaceWith(parent), orgn = parent, focn = orgn;
                    }
                    this.selection.get();
                    try {
                        this.range.setStart(orgn, orgo), this.range.setEnd(focn, foco);
                    } catch (e) {}
                    this.selection.addRange();
                },
                setAfter: function(node) {
                    try {
                        var tag = $(node)[0].tagName;
                        if ("BR" == tag || this.utils.isBlock(node)) "BR" != tag && this.utils.browser("msie") ? this.caret.setStart($(node).next()) : this.caret.setAfterOrBefore(node, "after"); else {
                            var space = this.utils.createSpaceElement();
                            $(node).after(space), this.caret.setEnd(space);
                        }
                    } catch (e) {
                        var space = this.utils.createSpaceElement();
                        $(node).after(space), this.caret.setEnd(space);
                    }
                },
                setBefore: function(node) {
                    this.utils.isBlock(node) ? this.caret.setEnd($(node).prev()) : this.caret.setAfterOrBefore(node, "before");
                },
                setAfterOrBefore: function(node, type) {
                    if (this.utils.browser("msie") || this.$editor.focus(), node = node[0] || node, 
                    this.selection.get(), "after" == type) try {
                        this.range.setStartAfter(node), this.range.setEndAfter(node);
                    } catch (e) {} else try {
                        this.range.setStartBefore(node), this.range.setEndBefore(node);
                    } catch (e) {}
                    this.range.collapse(!1), this.selection.addRange();
                },
                getOffsetOfElement: function(node) {
                    node = node[0] || node, this.selection.get();
                    var cloned = this.range.cloneRange();
                    return cloned.selectNodeContents(node), cloned.setEnd(this.range.endContainer, this.range.endOffset), 
                    $.trim(cloned.toString()).length;
                },
                getOffset: function() {
                    var offset = 0, sel = window.getSelection();
                    if (sel.rangeCount > 0) {
                        var range = window.getSelection().getRangeAt(0), caretRange = range.cloneRange();
                        caretRange.selectNodeContents(this.$editor[0]), caretRange.setEnd(range.endContainer, range.endOffset), 
                        offset = caretRange.toString().length;
                    }
                    return offset;
                },
                setOffset: function(start, end) {
                    "undefined" == typeof end && (end = start), this.focus.isFocused() || this.focus.setStart();
                    for (var node, offset = (this.selection.get(), 0), walker = document.createTreeWalker(this.$editor[0], NodeFilter.SHOW_TEXT, null, null); node == walker.nextNode(); ) if (offset += node.nodeValue.length, 
                    offset > start && (this.range.setStart(node, node.nodeValue.length + start - offset), 
                    start = 1 / 0), offset >= end) {
                        this.range.setEnd(node, node.nodeValue.length + end - offset);
                        break;
                    }
                    this.range.collapse(!1), this.selection.addRange();
                },
                setToPoint: function(start, end) {
                    this.caret.setOffset(start, end);
                },
                getCoords: function() {
                    return this.caret.getOffset();
                }
            };
        },
        clean: function() {
            return {
                onSet: function(html) {
                    html = this.clean.savePreCode(html), html = html.replace(/<script(.*?[^>]?)>([\w\W]*?)<\/script>/gi, '<pre class="redactor-script-tag" style="display: none;" $1>$2</pre>'), 
                    html = html.replace(/\$/g, "&#36;"), html = html.replace(/<a href="(.*?[^>]?)®(.*?[^>]?)">/gi, '<a href="$1&reg$2">'), 
                    this.opts.replaceDivs && !this.opts.linebreaks && (html = this.clean.replaceDivs(html)), 
                    this.opts.linebreaks && (html = this.clean.replaceParagraphsToBr(html)), html = this.clean.saveFormTags(html);
                    var $div = $("<div>");
                    $div.html(html);
                    var fonts = $div.find("font[style]");
                    return 0 !== fonts.length && (fonts.replaceWith(function() {
                        var $el = $(this), $span = $("<span>").attr("style", $el.attr("style"));
                        return $span.append($el.contents());
                    }), html = $div.html()), $div.remove(), html = html.replace(/<font(.*?)>/gi, ""), 
                    html = html.replace(/<\/font>/gi, ""), html = this.tidy.load(html), this.opts.paragraphize && (html = this.paragraphize.load(html)), 
                    html = this.clean.setVerified(html), html = this.clean.convertInline(html), html = html.replace(/&amp;/g, "&");
                },
                onSync: function(html) {
                    if (html = html.replace(/\u200B/g, ""), html = html.replace(/&#x200b;/gi, ""), this.opts.cleanSpaces && (html = html.replace(/&nbsp;/gi, " ")), 
                    -1 != html.search(/^<p>(||\s||<br\s?\/?>||&nbsp;)<\/p>$/i)) return "";
                    html = html.replace(/<pre class="redactor-script-tag" style="display: none;"(.*?[^>]?)>([\w\W]*?)<\/pre>/gi, "<script$1>$2</script>"), 
                    html = this.clean.restoreFormTags(html);
                    var chars = {
                        "™": "&trade;",
                        "©": "&copy;",
                        "…": "&hellip;",
                        "—": "&mdash;",
                        "‐": "&dash;"
                    };
                    $.each(chars, function(i, s) {
                        html = html.replace(new RegExp(i, "g"), s);
                    }), this.utils.browser("mozilla") && (html = html.replace(/<br\s?\/?>$/gi, "")), 
                    html = html.replace(new RegExp("<br\\s?/?></li>", "gi"), "</li>"), html = html.replace(new RegExp("</li><br\\s?/?>", "gi"), "</li>"), 
                    html = html.replace(/<(.*?)rel="\s*?"(.*?[^>]?)>/gi, '<$1$2">'), html = html.replace(/<(.*?)style="\s*?"(.*?[^>]?)>/gi, '<$1$2">'), 
                    html = html.replace(/="">/gi, ">"), html = html.replace(/""">/gi, '">'), html = html.replace(/"">/gi, '">'), 
                    html = html.replace(/<div(.*?)data-tagblock="redactor"(.*?[^>])>/gi, "<div$1$2>"), 
                    html = html.replace(/<(.*?) data-verified="redactor"(.*?[^>])>/gi, "<$1$2>");
                    var $div = $("<div/>").html($.parseHTML(html, document, !0));
                    return $div.find("span").removeAttr("rel"), $div.find("pre .redactor-invisible-space").each(function() {
                        $(this).contents().unwrap();
                    }), html = $div.html(), html = html.replace(/<img(.*?[^>])rel="(.*?[^>])"(.*?[^>])>/gi, "<img$1$3>"), 
                    html = html.replace(/<span class="redactor-invisible-space">(.*?)<\/span>/gi, "$1"), 
                    html = html.replace(/ data-save-url="(.*?[^>])"/gi, ""), html = html.replace(/<span(.*?)id="redactor-image-box"(.*?[^>])>([\w\W]*?)<img(.*?)><\/span>/gi, "$3<img$4>"), 
                    html = html.replace(/<span(.*?)id="redactor-image-resizer"(.*?[^>])>(.*?)<\/span>/gi, ""), 
                    html = html.replace(/<span(.*?)id="redactor-image-editter"(.*?[^>])>(.*?)<\/span>/gi, ""), 
                    html = html.replace(/<font(.*?)>/gi, ""), html = html.replace(/<\/font>/gi, ""), 
                    html = this.tidy.load(html), this.opts.linkNofollow && (html = html.replace(/<a(.*?)rel="nofollow"(.*?[^>])>/gi, "<a$1$2>"), 
                    html = html.replace(/<a(.*?[^>])>/gi, '<a$1 rel="nofollow">')), html = html.replace(/\sdata-redactor-(tag|class|style)="(.*?[^>])"/gi, ""), 
                    html = html.replace(new RegExp('<(.*?) data-verified="redactor"(.*?[^>])>', "gi"), "<$1$2>"), 
                    html = html.replace(new RegExp('<(.*?) data-verified="redactor">', "gi"), "<$1>"), 
                    html = html.replace(/&amp;/g, "&");
                },
                onPaste: function(html, setMode) {
                    if (html = $.trim(html), html = html.replace(/\$/g, "&#36;"), html = html.replace(/<span class="s[0-9]">/gi, "<span>"), 
                    html = html.replace(/<span class="Apple-converted-space">&nbsp;<\/span>/gi, " "), 
                    html = html.replace(/<span class="Apple-tab-span"[^>]*>\t<\/span>/gi, "	"), html = html.replace(/<span[^>]*>(\s|&nbsp;)<\/span>/gi, " "), 
                    this.opts.pastePlainText) return this.clean.getPlainText(html);
                    if (!this.utils.isSelectAll() && "undefined" == typeof setMode) {
                        if (this.utils.isCurrentOrParent([ "FIGCAPTION", "A" ])) return this.clean.getPlainText(html, !1);
                        if (this.utils.isCurrentOrParent("PRE")) return html = html.replace(/”/g, '"'), 
                        html = html.replace(/“/g, '"'), html = html.replace(/‘/g, "'"), html = html.replace(/’/g, "'"), 
                        this.clean.getPreCode(html);
                        if (this.utils.isCurrentOrParent([ "BLOCKQUOTE", "H1", "H2", "H3", "H4", "H5", "H6" ])) {
                            if (html = this.clean.getOnlyImages(html), !this.utils.browser("msie")) {
                                var block = this.selection.getBlock();
                                block && "P" == block.tagName && (html = html.replace(/<img(.*?)>/gi, "<p><img$1></p>"));
                            }
                            return html;
                        }
                        if (this.utils.isCurrentOrParent([ "TD" ])) return html = this.clean.onPasteTidy(html, "td"), 
                        this.opts.linebreaks && (html = this.clean.replaceParagraphsToBr(html)), html = this.clean.replaceDivsToBr(html);
                        if (this.utils.isCurrentOrParent([ "LI" ])) return this.clean.onPasteTidy(html, "li");
                    }
                    return html = this.clean.isSingleLine(html, setMode), this.clean.singleLine || (this.opts.linebreaks && (html = this.clean.replaceParagraphsToBr(html)), 
                    this.opts.replaceDivs && (html = this.clean.replaceDivs(html)), html = this.clean.saveFormTags(html)), 
                    html = this.clean.onPasteWord(html), html = this.clean.onPasteExtra(html), html = this.clean.onPasteTidy(html, "all"), 
                    !this.clean.singleLine && this.opts.paragraphize && (html = this.paragraphize.load(html)), 
                    html = this.clean.removeDirtyStyles(html), html = this.clean.onPasteRemoveSpans(html), 
                    html = this.clean.onPasteRemoveEmpty(html), html = this.clean.convertInline(html);
                },
                onPasteWord: function(html) {
                    if (html = html.replace(/<!--[\s\S]*?-->/gi, ""), html = html.replace(/<style[^>]*>[\s\S]*?<\/style>/gi, ""), 
                    html = html.replace(/<o\:p[^>]*>[\s\S]*?<\/o\:p>/gi, ""), html.match(/class="?Mso|style="[^"]*\bmso-|style='[^'']*\bmso-|w:WordDocument/i)) {
                        html = html.replace(/<!--[\s\S]+?-->/gi, ""), html = html.replace(/<(!|script[^>]*>.*?<\/script(?=[>\s])|\/?(\?xml(:\w+)?|img|meta|link|style|\w:\w+)(?=[\s\/>]))[^>]*>/gi, ""), 
                        html = html.replace(/<(\/?)s>/gi, "<$1strike>"), html = html.replace(/ /gi, " "), 
                        html = html.replace(/<span\s+style\s*=\s*"\s*mso-spacerun\s*:\s*yes\s*;?\s*"\s*>([\s\u00a0]*)<\/span>/gi, function(str, spaces) {
                            return spaces.length > 0 ? spaces.replace(/./, " ").slice(Math.floor(spaces.length / 2)).split("").join(" ") : "";
                        }), html = this.clean.onPasteIeFixLinks(html), html = html.replace(/<img(.*?)v:shapes=(.*?)>/gi, ""), 
                        html = html.replace(/src="file\:\/\/(.*?)"/, 'src=""');
                        var $div = $("<div/>").html(html), lastList = !1, lastLevel = 1, listsIds = [];
                        $div.find("p[style]").each(function() {
                            var matches = $(this).attr("style").match(/mso\-list\:l([0-9]+)\slevel([0-9]+)/);
                            if (matches) {
                                var currentList = parseInt(matches[1]), currentLevel = parseInt(matches[2]), listType = $(this).html().match(/^[\w]+\./) ? "ol" : "ul", $li = $("<li/>").html($(this).html());
                                if ($li.html($li.html().replace(/^([\w\.]+)</, "<")), $li.find("span:first").remove(), 
                                1 == currentLevel && -1 == $.inArray(currentList, listsIds)) {
                                    var $list = $("<" + listType + "/>").attr({
                                        "data-level": currentLevel,
                                        "data-list": currentList
                                    }).html($li);
                                    $(this).replaceWith($list), lastList = currentList, listsIds.push(currentList);
                                } else {
                                    if (currentLevel > lastLevel) {
                                        for (var $prevList = $div.find('[data-level="' + lastLevel + '"][data-list="' + lastList + '"]'), $lastList = $prevList, i = lastLevel; currentLevel > i; i++) $list = $("<" + listType + "/>"), 
                                        $list.appendTo($lastList.find("li").last()), $lastList = $list;
                                        $lastList.attr({
                                            "data-level": currentLevel,
                                            "data-list": currentList
                                        }).html($li);
                                    } else {
                                        var $prevList = $div.find('[data-level="' + currentLevel + '"][data-list="' + currentList + '"]').last();
                                        $prevList.append($li);
                                    }
                                    lastLevel = currentLevel, lastList = currentList, $(this).remove();
                                }
                            }
                        }), $div.find("[data-level][data-list]").removeAttr("data-level data-list"), html = $div.html(), 
                        html = html.replace(/·/g, ""), html = html.replace(/<p class="Mso(.*?)"/gi, "<p"), 
                        html = html.replace(/ class=\"(mso[^\"]*)\"/gi, ""), html = html.replace(/ class=(mso\w+)/gi, ""), 
                        html = html.replace(/<o:p(.*?)>([\w\W]*?)<\/o:p>/gi, "$2"), html = html.replace(/\n/g, " "), 
                        html = html.replace(/<p>\n?<li>/gi, "<li>");
                    }
                    return html;
                },
                onPasteExtra: function(html) {
                    return html = html.replace(/<b\sid="internal-source-marker(.*?)">([\w\W]*?)<\/b>/gi, "$2"), 
                    html = html.replace(/<b(.*?)id="docs-internal-guid(.*?)">([\w\W]*?)<\/b>/gi, "$3"), 
                    html = html.replace(/<span[^>]*(font-style: italic; font-weight: bold|font-weight: bold; font-style: italic)[^>]*>/gi, '<span style="font-weight: bold;"><span style="font-style: italic;">'), 
                    html = html.replace(/<span[^>]*font-style: italic[^>]*>/gi, '<span style="font-style: italic;">'), 
                    html = html.replace(/<span[^>]*font-weight: bold[^>]*>/gi, '<span style="font-weight: bold;">'), 
                    html = html.replace(/<span[^>]*text-decoration: underline[^>]*>/gi, '<span style="text-decoration: underline;">'), 
                    html = html.replace(/<img>/gi, ""), html = html.replace(/\n{3,}/gi, "\n"), html = html.replace(/<font(.*?)>([\w\W]*?)<\/font>/gi, "$2"), 
                    html = html.replace(/<p><p>/gi, "<p>"), html = html.replace(/<\/p><\/p>/gi, "</p>"), 
                    html = html.replace(/<li>(\s*|\t*|\n*)<p>/gi, "<li>"), html = html.replace(/<\/p>(\s*|\t*|\n*)<\/li>/gi, "</li>"), 
                    html = html.replace(/<\/p>\s<p/gi, "</p><p"), html = html.replace(/<img src="webkit-fake-url\:\/\/(.*?)"(.*?)>/gi, ""), 
                    html = html.replace(/<p>•([\w\W]*?)<\/p>/gi, "<li>$1</li>"), this.utils.browser("mozilla") && (html = html.replace(/<br\s?\/?>$/gi, "")), 
                    html;
                },
                onPasteTidy: function(html, type) {
                    var tags = [ "span", "a", "pre", "blockquote", "small", "em", "strong", "code", "kbd", "mark", "address", "cite", "var", "samp", "dfn", "sup", "sub", "b", "i", "u", "del", "ol", "ul", "li", "dl", "dt", "dd", "p", "br", "video", "audio", "iframe", "embed", "param", "object", "img", "table", "td", "th", "tr", "tbody", "tfoot", "thead", "h1", "h2", "h3", "h4", "h5", "h6" ], tagsEmpty = !1, attrAllowed = [ [ "a", "*" ], [ "img", [ "src", "alt" ] ], [ "span", [ "class", "rel", "data-verified" ] ], [ "iframe", "*" ], [ "video", "*" ], [ "audio", "*" ], [ "embed", "*" ], [ "object", "*" ], [ "param", "*" ], [ "source", "*" ] ];
                    "all" == type ? (tagsEmpty = [ "p", "span", "h1", "h2", "h3", "h4", "h5", "h6" ], 
                    attrAllowed = [ [ "table", "class" ], [ "td", [ "colspan", "rowspan" ] ], [ "a", "*" ], [ "img", [ "src", "alt", "data-redactor-inserted-image" ] ], [ "span", [ "class", "rel", "data-verified" ] ], [ "iframe", "*" ], [ "video", "*" ], [ "audio", "*" ], [ "embed", "*" ], [ "object", "*" ], [ "param", "*" ], [ "source", "*" ] ]) : "td" == type ? tags = [ "ul", "ol", "li", "span", "a", "small", "em", "strong", "code", "kbd", "mark", "cite", "var", "samp", "dfn", "sup", "sub", "b", "i", "u", "del", "ol", "ul", "li", "dl", "dt", "dd", "br", "iframe", "video", "audio", "embed", "param", "object", "img", "h1", "h2", "h3", "h4", "h5", "h6" ] : "li" == type && (tags = [ "ul", "ol", "li", "span", "a", "small", "em", "strong", "code", "kbd", "mark", "cite", "var", "samp", "dfn", "sup", "sub", "b", "i", "u", "del", "br", "iframe", "video", "audio", "embed", "param", "object", "img" ]);
                    var options = {
                        deniedTags: this.opts.deniedTags ? this.opts.deniedTags : !1,
                        allowedTags: this.opts.allowedTags ? this.opts.allowedTags : tags,
                        removeComments: !0,
                        removePhp: !0,
                        removeAttr: this.opts.removeAttr ? this.opts.removeAttr : !1,
                        allowedAttr: this.opts.allowedAttr ? this.opts.allowedAttr : attrAllowed,
                        removeEmpty: tagsEmpty
                    };
                    return this.tidy.load(html, options);
                },
                onPasteRemoveEmpty: function(html) {
                    return html = html.replace(/<(p|h[1-6])>(|\s|\n|\t|<br\s?\/?>)<\/(p|h[1-6])>/gi, ""), 
                    this.opts.linebreaks || (html = html.replace(/<br>$/i, "")), html;
                },
                onPasteRemoveSpans: function(html) {
                    return html = html.replace(/<span>(.*?)<\/span>/gi, "$1"), html = html.replace(/<span[^>]*>\s|&nbsp;<\/span>/gi, " ");
                },
                onPasteIeFixLinks: function(html) {
                    if (!this.utils.browser("msie")) return html;
                    var tmp = $.trim(html);
                    return 0 === tmp.search(/^<a(.*?)>(.*?)<\/a>$/i) && (html = html.replace(/^<a(.*?)>(.*?)<\/a>$/i, "$2")), 
                    html;
                },
                isSingleLine: function(html, setMode) {
                    if (this.clean.singleLine = !1, !this.utils.isSelectAll() && "undefined" == typeof setMode) {
                        var blocks = this.opts.blockLevelElements.join("|").replace("P|", "").replace("DIV|", ""), matchBlocks = html.match(new RegExp("</(" + blocks + ")>", "gi")), matchContainers = html.match(/<\/(p|div)>/gi);
                        if (!matchBlocks && (null === matchContainers || matchContainers && matchContainers.length <= 1)) {
                            var matchBR = html.match(/<br\s?\/?>/gi);
                            matchBR || (this.clean.singleLine = !0, html = html.replace(/<\/?(p|div)(.*?)>/gi, ""));
                        }
                    }
                    return html;
                },
                stripTags: function(input, allowed) {
                    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join("");
                    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
                    return input.replace(tags, function($0, $1) {
                        return allowed.indexOf("<" + $1.toLowerCase() + ">") > -1 ? $0 : "";
                    });
                },
                savePreCode: function(html) {
                    return html = this.clean.savePreFormatting(html), html = this.clean.saveCodeFormatting(html), 
                    html = this.clean.restoreSelectionMarker(html);
                },
                savePreFormatting: function(html) {
                    var pre = html.match(/<pre(.*?)>([\w\W]*?)<\/pre>/gi);
                    return null !== pre && $.each(pre, $.proxy(function(i, s) {
                        var arr = s.match(/<pre(.*?)>([\w\W]*?)<\/pre>/i);
                        arr[2] = arr[2].replace(/<br\s?\/?>/g, "\n"), arr[2] = arr[2].replace(/&nbsp;/g, " "), 
                        this.opts.preSpaces && (arr[2] = arr[2].replace(/\t/g, Array(this.opts.preSpaces + 1).join(" "))), 
                        arr[2] = this.clean.encodeEntities(arr[2]), arr[2] = arr[2].replace(/\$/g, "&#36;"), 
                        html = html.replace(s, "<pre" + arr[1] + ">" + arr[2] + "</pre>");
                    }, this)), html;
                },
                saveCodeFormatting: function(html) {
                    var code = html.match(/<code(.*?)>([\w\W]*?)<\/code>/gi);
                    return null !== code && $.each(code, $.proxy(function(i, s) {
                        var arr = s.match(/<code(.*?)>([\w\W]*?)<\/code>/i);
                        arr[2] = arr[2].replace(/&nbsp;/g, " "), arr[2] = this.clean.encodeEntities(arr[2]), 
                        arr[2] = arr[2].replace(/\$/g, "&#36;"), html = html.replace(s, "<code" + arr[1] + ">" + arr[2] + "</code>");
                    }, this)), html;
                },
                restoreSelectionMarker: function(html) {
                    return html = html.replace(/&lt;span id=&quot;selection-marker-([0-9])&quot; class=&quot;redactor-selection-marker&quot; data-verified=&quot;redactor&quot;&gt;​&lt;\/span&gt;/g, '<span id="selection-marker-$1" class="redactor-selection-marker" data-verified="redactor">​</span>');
                },
                getTextFromHtml: function(html) {
                    html = html.replace(/<br\s?\/?>|<\/H[1-6]>|<\/p>|<\/div>|<\/li>|<\/td>/gi, "\n");
                    var tmp = document.createElement("div");
                    return tmp.innerHTML = html, html = tmp.textContent || tmp.innerText, $.trim(html);
                },
                getPlainText: function(html, paragraphize) {
                    return html = this.clean.getTextFromHtml(html), html = html.replace(/\n/g, "<br />"), 
                    this.opts.paragraphize && "undefined" == typeof paragraphize && !this.utils.browser("mozilla") && (html = this.paragraphize.load(html)), 
                    html;
                },
                getPreCode: function(html) {
                    return html = html.replace(/<img(.*?) style="(.*?)"(.*?[^>])>/gi, "<img$1$3>"), 
                    html = html.replace(/<img(.*?)>/gi, "&lt;img$1&gt;"), html = this.clean.getTextFromHtml(html), 
                    this.opts.preSpaces && (html = html.replace(/\t/g, Array(this.opts.preSpaces + 1).join(" "))), 
                    html = this.clean.encodeEntities(html);
                },
                getOnlyImages: function(html) {
                    return html = html.replace(/<img(.*?)>/gi, "[img$1]"), html = html.replace(/<([Ss]*?)>/gi, ""), 
                    html = html.replace(/\[img(.*?)\]/gi, "<img$1>");
                },
                getOnlyLinksAndImages: function(html) {
                    return html = html.replace(/<a(.*?)href="(.*?)"(.*?)>([\w\W]*?)<\/a>/gi, '[a href="$2"]$4[/a]'), 
                    html = html.replace(/<img(.*?)>/gi, "[img$1]"), html = html.replace(/<(.*?)>/gi, ""), 
                    html = html.replace(/\[a href="(.*?)"\]([\w\W]*?)\[\/a\]/gi, '<a href="$1">$2</a>'), 
                    html = html.replace(/\[img(.*?)\]/gi, "<img$1>");
                },
                encodeEntities: function(str) {
                    return str = String(str).replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, '"'), 
                    str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
                },
                removeDirtyStyles: function(html) {
                    if (this.utils.browser("msie")) return html;
                    var div = document.createElement("div");
                    return div.innerHTML = html, this.clean.clearUnverifiedRemove($(div)), html = div.innerHTML, 
                    $(div).remove(), html;
                },
                clearUnverified: function() {
                    if (!this.utils.browser("msie")) {
                        this.clean.clearUnverifiedRemove(this.$editor);
                        var headers = this.$editor.find("h1, h2, h3, h4, h5, h6");
                        headers.find("span").removeAttr("style"), headers.find(this.opts.verifiedTags.join(", ")).removeAttr("style"), 
                        this.code.sync();
                    }
                },
                clearUnverifiedRemove: function($editor) {
                    $editor.find(this.opts.verifiedTags.join(", ")).removeAttr("style"), $editor.find("span").not('[data-verified="redactor"]').removeAttr("style"), 
                    $editor.find('span[data-verified="redactor"], img[data-verified="redactor"]').each(function(i, s) {
                        var $s = $(s);
                        $s.attr("style", $s.attr("rel"));
                    });
                },
                cleanEmptyParagraph: function() {},
                setVerified: function(html) {
                    if (this.utils.browser("msie")) return html;
                    html = html.replace(new RegExp("<img(.*?[^>])>", "gi"), '<img$1 data-verified="redactor">'), 
                    html = html.replace(new RegExp("<span(.*?[^>])>", "gi"), '<span$1 data-verified="redactor">');
                    var matches = html.match(new RegExp('<(span|img)(.*?)style="(.*?)"(.*?[^>])>', "gi"));
                    if (matches) for (var len = matches.length, i = 0; len > i; i++) try {
                        var newTag = matches[i].replace(/style="(.*?)"/i, 'style="$1" rel="$1"');
                        html = html.replace(matches[i], newTag);
                    } catch (e) {}
                    return html;
                },
                convertInline: function(html) {
                    var $div = $("<div />").html(html), tags = this.opts.inlineTags;
                    return tags.push("span"), $div.find(tags.join(",")).each(function() {
                        var $el = $(this), tag = this.tagName.toLowerCase();
                        $el.attr("data-redactor-tag", tag), "span" == tag && ($el.attr("style") ? $el.attr("data-redactor-style", $el.attr("style")) : $el.attr("class") && $el.attr("data-redactor-class", $el.attr("class")));
                    }), html = $div.html(), $div.remove(), html;
                },
                normalizeLists: function() {
                    this.$editor.find("li").each(function(i, s) {
                        var $next = $(s).next();
                        0 === $next.length || "UL" != $next[0].tagName && "OL" != $next[0].tagName || $(s).append($next);
                    });
                },
                removeSpaces: function(html) {
                    return html = html.replace(/\n/g, ""), html = html.replace(/[\t]*/g, ""), html = html.replace(/\n\s*\n/g, "\n"), 
                    html = html.replace(/^[\s\n]*/g, " "), html = html.replace(/[\s\n]*$/g, " "), html = html.replace(/>\s{2,}</g, "> <"), 
                    html = html.replace(/\n\n/g, "\n"), html = html.replace(/\u200B/g, "");
                },
                replaceDivs: function(html) {
                    return this.opts.linebreaks ? (html = html.replace(/<div><br\s?\/?><\/div>/gi, "<br />"), 
                    html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, "$2<br />")) : html = html.replace(/<div(.*?)>([\w\W]*?)<\/div>/gi, "<p$1>$2</p>"), 
                    html = html.replace(/<div(.*?[^>])>/gi, ""), html = html.replace(/<\/div>/gi, "");
                },
                replaceDivsToBr: function(html) {
                    return html = html.replace(/<div\s(.*?)>/gi, "<p>"), html = html.replace(/<div><br\s?\/?><\/div>/gi, "<br /><br />"), 
                    html = html.replace(/<div>([\w\W]*?)<\/div>/gi, "$1<br /><br />");
                },
                replaceParagraphsToBr: function(html) {
                    return html = html.replace(/<p\s(.*?)>/gi, "<p>"), html = html.replace(/<p><br\s?\/?><\/p>/gi, "<br />"), 
                    html = html.replace(/<p>([\w\W]*?)<\/p>/gi, "$1<br /><br />"), html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, "</blockquote>");
                },
                saveFormTags: function(html) {
                    return html.replace(/<form(.*?)>([\w\W]*?)<\/form>/gi, '<section$1 rel="redactor-form-tag">$2</section>');
                },
                restoreFormTags: function(html) {
                    return html.replace(/<section(.*?) rel="redactor-form-tag"(.*?)>([\w\W]*?)<\/section>/gi, "<form$1$2>$3</form>");
                }
            };
        },
        code: function() {
            return {
                set: function(html) {
                    html = $.trim(html.toString()), html = this.clean.onSet(html), this.utils.browser("msie") && (html = html.replace(/<span(.*?)id="selection-marker-(1|2)"(.*?)><\/span>/gi, "")), 
                    this.$editor.html(html), this.code.sync(), "" !== html && this.placeholder.remove(), 
                    setTimeout($.proxy(this.buffer.add, this), 15), this.start === !1 && this.observe.load();
                },
                get: function() {
                    var code = this.$textarea.val();
                    return this.opts.replaceDivs && (code = this.clean.replaceDivs(code)), this.opts.linebreaks && (code = this.clean.replaceParagraphsToBr(code)), 
                    code = this.tabifier.get(code);
                },
                sync: function() {
                    setTimeout($.proxy(this.code.startSync, this), 10);
                },
                startSync: function() {
                    var html = this.$editor.html();
                    this.code.syncCode && this.code.syncCode == html || (this.code.syncCode = html, 
                    html = this.core.setCallback("syncBefore", html), html = this.clean.onSync(html), 
                    this.$textarea.val(html), this.core.setCallback("sync", html), this.start === !1 && this.core.setCallback("change", html), 
                    this.start = !1, 0 == this.autosave.html && (this.autosave.html = this.code.get()), 
                    this.opts.codemirror && this.$textarea.next(".CodeMirror").each(function(i, el) {
                        el.CodeMirror.setValue(html);
                    }), this.autosave.onChange(), this.autosave.enable());
                },
                toggle: function() {
                    this.opts.visual ? this.code.showCode() : this.code.showVisual();
                },
                showCode: function() {
                    this.selection.save(), this.code.offset = this.caret.getOffset();
                    var scroll = $(window).scrollTop(), height = (this.$editor.innerWidth(), this.$editor.innerHeight());
                    this.$editor.hide();
                    var html = this.$textarea.val();
                    this.modified = this.clean.removeSpaces(html), html = this.tabifier.get(html);
                    var start = 0, end = 0, $editorDiv = $("<div/>").append($.parseHTML(this.clean.onSync(this.$editor.html()), document, !0)), $selectionMarkers = $editorDiv.find("span.redactor-selection-marker");
                    if ($selectionMarkers.length > 0) {
                        var editorHtml = this.tabifier.get($editorDiv.html()).replace(/&amp;/g, "&");
                        1 == $selectionMarkers.length ? (start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML")), 
                        end = start) : 2 == $selectionMarkers.length && (start = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-1").prop("outerHTML")), 
                        end = this.utils.strpos(editorHtml, $editorDiv.find("#selection-marker-2").prop("outerHTML")) - $editorDiv.find("#selection-marker-1").prop("outerHTML").toString().length);
                    }
                    this.selection.removeMarkers(), this.$textarea.val(html), this.opts.codemirror ? this.$textarea.next(".CodeMirror").each(function(i, el) {
                        $(el).show(), el.CodeMirror.setValue(html), el.CodeMirror.setSize("100%", height), 
                        el.CodeMirror.refresh(), start == end ? el.CodeMirror.setCursor(el.CodeMirror.posFromIndex(start).line, el.CodeMirror.posFromIndex(end).ch) : el.CodeMirror.setSelection({
                            line: el.CodeMirror.posFromIndex(start).line,
                            ch: el.CodeMirror.posFromIndex(start).ch
                        }, {
                            line: el.CodeMirror.posFromIndex(end).line,
                            ch: el.CodeMirror.posFromIndex(end).ch
                        }), el.CodeMirror.focus();
                    }) : (this.$textarea.height(height).show().focus(), this.$textarea.on("keydown.redactor-textarea-indenting", this.code.textareaIndenting), 
                    $(window).scrollTop(scroll), this.$textarea[0].setSelectionRange && this.$textarea[0].setSelectionRange(start, end), 
                    this.$textarea[0].scrollTop = 0), this.opts.visual = !1, this.button.setInactiveInCode(), 
                    this.button.setActive("html"), this.core.setCallback("source", html);
                },
                showVisual: function() {
                    var html;
                    if (!this.opts.visual) {
                        var start = 0, end = 0;
                        if (this.opts.codemirror) {
                            var selection;
                            this.$textarea.next(".CodeMirror").each(function(i, el) {
                                selection = el.CodeMirror.listSelections(), start = el.CodeMirror.indexFromPos(selection[0].anchor), 
                                end = el.CodeMirror.indexFromPos(selection[0].head), html = el.CodeMirror.getValue();
                            });
                        } else start = this.$textarea.get(0).selectionStart, end = this.$textarea.get(0).selectionEnd, 
                        html = this.$textarea.hide().val();
                        if (start > end && end > 0) {
                            var tempStart = end, tempEnd = start;
                            start = tempStart, end = tempEnd;
                        }
                        if (start = this.code.enlargeOffset(html, start), end = this.code.enlargeOffset(html, end), 
                        html = html.substr(0, start) + this.selection.getMarkerAsHtml(1) + html.substr(start), 
                        end > start) {
                            var markerLength = this.selection.getMarkerAsHtml(1).toString().length;
                            html = html.substr(0, end + markerLength) + this.selection.getMarkerAsHtml(2) + html.substr(end + markerLength);
                        }
                        this.modified !== this.clean.removeSpaces(html) && this.code.set(html), this.opts.codemirror && this.$textarea.next(".CodeMirror").hide(), 
                        this.$editor.show(), this.utils.isEmpty(html) || this.placeholder.remove(), this.selection.restore(), 
                        this.$textarea.off("keydown.redactor-textarea-indenting"), this.button.setActiveInVisual(), 
                        this.button.setInactive("html"), this.observe.load(), this.opts.visual = !0, this.core.setCallback("visual", html);
                    }
                },
                textareaIndenting: function(e) {
                    if (9 !== e.keyCode) return !0;
                    var $el = this.$textarea, start = $el.get(0).selectionStart;
                    return $el.val($el.val().substring(0, start) + "	" + $el.val().substring($el.get(0).selectionEnd)), 
                    $el.get(0).selectionStart = $el.get(0).selectionEnd = start + 1, !1;
                },
                enlargeOffset: function(html, offset) {
                    var htmlLength = html.length, c = 0;
                    if (">" == html[offset]) c++; else for (var i = offset; htmlLength >= i && (c++, 
                    ">" != html[i]); i++) if ("<" == html[i] || i == htmlLength) {
                        c = 0;
                        break;
                    }
                    return offset + c;
                }
            };
        },
        core: function() {
            return {
                getObject: function() {
                    return $.extend({}, this);
                },
                getEditor: function() {
                    return this.$editor;
                },
                getBox: function() {
                    return this.$box;
                },
                getElement: function() {
                    return this.$element;
                },
                getTextarea: function() {
                    return this.$textarea;
                },
                getToolbar: function() {
                    return this.$toolbar ? this.$toolbar : !1;
                },
                addEvent: function(name) {
                    this.core.event = name;
                },
                getEvent: function() {
                    return this.core.event;
                },
                setCallback: function(type, e, data) {
                    var eventName = type + "Callback", eventNamespace = "redactor", callback = this.opts[eventName];
                    if (this.$textarea) {
                        var returnValue = !1, events = $._data(this.$textarea[0], "events");
                        if ("undefined" != typeof events && "undefined" != typeof events[eventName] && $.each(events[eventName], $.proxy(function(key, value) {
                            if (value.namespace == eventNamespace) {
                                var data = "undefined" == typeof data ? [ e ] : [ e, data ];
                                returnValue = "undefined" == typeof data ? value.handler.call(this, e) : value.handler.call(this, e, data);
                            }
                        }, this)), returnValue) return returnValue;
                    }
                    return $.isFunction(callback) ? "undefined" == typeof data ? callback.call(this, e) : callback.call(this, e, data) : "undefined" == typeof data ? e : data;
                },
                destroy: function() {
                    this.opts.destroyed = !0, this.core.setCallback("destroy"), this.$element.off(".redactor").removeData("redactor"), 
                    this.$editor.off(".redactor"), $(document).off("mousedown.redactor-blur." + this.uuid), 
                    $(document).off("mousedown.redactor." + this.uuid), $(document).off("click.redactor-image-delete." + this.uuid), 
                    $(document).off("click.redactor-image-resize-hide." + this.uuid), $(document).off("touchstart.redactor." + this.uuid + " click.redactor." + this.uuid), 
                    $("body").off("scroll.redactor." + this.uuid), $(this.opts.toolbarFixedTarget).off("scroll.redactor." + this.uuid), 
                    this.$editor.removeClass("redactor-editor redactor-linebreaks redactor-placeholder"), 
                    this.$editor.removeAttr("contenteditable");
                    var html = this.code.get();
                    this.opts.toolbar && this.$toolbar.find("a").each(function() {
                        var $el = $(this);
                        $el.data("dropdown") && ($el.data("dropdown").remove(), $el.data("dropdown", {}));
                    }), this.build.isTextarea() ? (this.$box.after(this.$element), this.$box.remove(), 
                    this.$element.val(html).show()) : (this.$box.after(this.$editor), this.$box.remove(), 
                    this.$element.html(html).show()), this.$pasteBox && this.$pasteBox.remove(), this.$modalBox && this.$modalBox.remove(), 
                    this.$modalOverlay && this.$modalOverlay.remove(), $(".redactor-toolbar-tooltip-" + this.uuid).remove(), 
                    clearInterval(this.autosaveInterval);
                }
            };
        },
        dropdown: function() {
            return {
                build: function(name, $dropdown, dropdownObject) {
                    "formatting" == name && this.opts.formattingAdd && $.each(this.opts.formattingAdd, $.proxy(function(i, s) {
                        var func, name = s.tag;
                        "undefined" != typeof s["class"] && (name = name + "-" + s["class"]), s.type = this.utils.isBlockTag(s.tag) ? "block" : "inline", 
                        func = "undefined" != typeof s.func ? s.func : "inline" == s.type ? "inline.formatting" : "block.formatting", 
                        this.opts.linebreaks && "block" == s.type && "p" == s.tag || (this.formatting[name] = {
                            tag: s.tag,
                            style: s.style,
                            "class": s["class"],
                            attr: s.attr,
                            data: s.data,
                            clear: s.clear
                        }, dropdownObject[name] = {
                            func: func,
                            title: s.title
                        });
                    }, this)), $.each(dropdownObject, $.proxy(function(btnName, btnObject) {
                        var $item = $('<a href="#" class="redactor-dropdown-' + btnName + '" role="button">' + btnObject.title + "</a>");
                        "formatting" == name && $item.addClass("redactor-formatting-" + btnName), $item.on("click", $.proxy(function(e) {
                            e.preventDefault();
                            var type = "func", callback = btnObject.func;
                            btnObject.command ? (type = "command", callback = btnObject.command) : btnObject.dropdown && (type = "dropdown", 
                            callback = btnObject.dropdown), $(e.target).hasClass("redactor-dropdown-link-inactive") || (this.button.onClick(e, btnName, type, callback), 
                            this.dropdown.hideAll());
                        }, this)), this.observe.addDropdown($item, btnName, btnObject), $dropdown.append($item);
                    }, this));
                },
                show: function(e, key) {
                    if (!this.opts.visual) return e.preventDefault(), !1;
                    var $button = this.button.get(key), $dropdown = $button.data("dropdown").appendTo(document.body);
                    if (this.opts.highContrast && $dropdown.addClass("redactor-dropdown-contrast"), 
                    $button.hasClass("dropact")) this.dropdown.hideAll(); else {
                        this.dropdown.hideAll(), this.observe.dropdowns(), this.core.setCallback("dropdownShow", {
                            dropdown: $dropdown,
                            key: key,
                            button: $button
                        }), this.button.setActive(key), $button.addClass("dropact");
                        var keyPosition = $button.offset(), dropdownWidth = $dropdown.width();
                        keyPosition.left + dropdownWidth > $(document).width() && (keyPosition.left = Math.max(0, keyPosition.left - dropdownWidth));
                        var left = keyPosition.left + "px";
                        if (this.$toolbar.hasClass("toolbar-fixed-box")) {
                            var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset, position = "fixed";
                            this.opts.toolbarFixedTarget !== document && (top = this.$toolbar.innerHeight() + this.$toolbar.offset().top + this.opts.toolbarFixedTopOffset, 
                            position = "absolute"), $dropdown.css({
                                position: position,
                                left: left,
                                top: top + "px"
                            }).show();
                        } else {
                            var top = $button.innerHeight() + keyPosition.top + "px";
                            $dropdown.css({
                                position: "absolute",
                                left: left,
                                top: top
                            }).show();
                        }
                        this.core.setCallback("dropdownShown", {
                            dropdown: $dropdown,
                            key: key,
                            button: $button
                        }), this.$dropdown = $dropdown;
                    }
                    $(document).one("click.redactor-dropdown", $.proxy(this.dropdown.hide, this)), this.$editor.one("click.redactor-dropdown", $.proxy(this.dropdown.hide, this)), 
                    $(document).one("keyup.redactor-dropdown", $.proxy(this.dropdown.closeHandler, this)), 
                    $dropdown.on("mouseover.redactor-dropdown", $.proxy(this.utils.disableBodyScroll, this)).on("mouseout.redactor-dropdown", $.proxy(this.utils.enableBodyScroll, this)), 
                    e.stopPropagation();
                },
                closeHandler: function(e) {
                    e.which == this.keyCode.ESC && (this.dropdown.hideAll(), this.$editor.focus());
                },
                hideAll: function() {
                    this.$toolbar.find("a.dropact").removeClass("redactor-act").removeClass("dropact"), 
                    this.utils.enableBodyScroll(), $(".redactor-dropdown-" + this.uuid).hide(), $(".redactor-dropdown-link-selected").removeClass("redactor-dropdown-link-selected"), 
                    this.$dropdown && (this.$dropdown.off(".redactor-dropdown"), this.core.setCallback("dropdownHide", this.$dropdown), 
                    this.$dropdown = !1);
                },
                hide: function(e) {
                    var $dropdown = $(e.target);
                    $dropdown.hasClass("dropact") || $dropdown.hasClass("redactor-dropdown-link-inactive") || ($dropdown.removeClass("dropact"), 
                    $dropdown.off("mouseover mouseout"), this.dropdown.hideAll());
                }
            };
        },
        file: function() {
            return {
                show: function() {
                    this.modal.load("file", this.lang.get("file"), 700), this.upload.init("#redactor-modal-file-upload", this.opts.fileUpload, this.file.insert), 
                    this.selection.save(), this.selection.get();
                    var text = this.sel.toString();
                    $("#redactor-filename").val(text), this.modal.show();
                },
                insert: function(json, direct, e) {
                    if ("undefined" != typeof json.error) return this.modal.close(), this.selection.restore(), 
                    void this.core.setCallback("fileUploadError", json);
                    var link;
                    if ("string" == typeof json) link = json; else {
                        var text = $("#redactor-filename").val();
                        ("undefined" == typeof text || "" === text) && (text = json.filename), link = '<a href="' + json.filelink + '" id="filelink-marker">' + text + "</a>";
                    }
                    if (direct) {
                        this.selection.removeMarkers();
                        var marker = this.selection.getMarker();
                        this.insert.nodeToCaretPositionFromPoint(e, marker);
                    } else this.modal.close();
                    if (this.selection.restore(), this.buffer.set(), this.insert.htmlWithoutClean(link), 
                    "string" != typeof json) {
                        var linkmarker = $(this.$editor.find("a#filelink-marker"));
                        0 !== linkmarker.length ? linkmarker.removeAttr("id").removeAttr("style") : linkmarker = !1, 
                        this.core.setCallback("fileUpload", linkmarker, json);
                    }
                }
            };
        },
        focus: function() {
            return {
                setStart: function() {
                    this.$editor.focus();
                    var first = this.$editor.children().first();
                    if (0 !== first.length && 0 !== first[0].length && "BR" != first[0].tagName && 3 != first[0].nodeType) {
                        if ("UL" == first[0].tagName || "OL" == first[0].tagName) {
                            var child = first.find("li").first();
                            if (!this.utils.isBlock(child) && "" === child.text()) return void this.caret.setStart(child);
                        }
                        return this.opts.linebreaks && !this.utils.isBlockTag(first[0].tagName) ? (this.selection.get(), 
                        this.range.setStart(this.$editor[0], 0), this.range.setEnd(this.$editor[0], 0), 
                        void this.selection.addRange()) : void this.caret.setStart(first);
                    }
                },
                setEnd: function() {
                    var last = this.$editor.children().last();
                    this.$editor.focus(), 0 !== last.size() && (this.utils.isEmpty(this.$editor.html()) ? (this.selection.get(), 
                    this.range.collapse(!0), this.range.setStartAfter(last[0]), this.range.setEnd(last[0], 0), 
                    this.selection.addRange()) : (this.selection.get(), this.range.selectNodeContents(last[0]), 
                    this.range.collapse(!1), this.selection.addRange()));
                },
                isFocused: function() {
                    var focusNode = document.getSelection().focusNode;
                    return null === focusNode ? !1 : this.opts.linebreaks && $(focusNode.parentNode).hasClass("redactor-linebreaks") ? !0 : this.utils.isRedactorParent(focusNode.parentNode) ? this.$editor.is(":focus") : !1;
                }
            };
        },
        image: function() {
            return {
                show: function() {
                    this.modal.load("image", this.lang.get("image"), 700), this.upload.init("#redactor-modal-image-droparea", this.opts.imageUpload, this.image.insert), 
                    this.selection.save(), this.modal.show();
                },
                showEdit: function($image) {
                    var $link = $image.closest("a", this.$editor[0]);
                    if (this.modal.load("imageEdit", this.lang.get("edit"), 705), this.modal.createCancelButton(), 
                    this.image.buttonDelete = this.modal.createDeleteButton(this.lang.get("_delete")), 
                    this.image.buttonSave = this.modal.createActionButton(this.lang.get("save")), this.image.buttonDelete.on("click", $.proxy(function() {
                        this.image.remove($image);
                    }, this)), this.image.buttonSave.on("click", $.proxy(function() {
                        this.image.update($image);
                    }, this)), $(".redactor-link-tooltip").remove(), $("#redactor-image-title").val($image.attr("alt")), 
                    this.opts.imageLink) {
                        var $redactorImageLink = $("#redactor-image-link");
                        $redactorImageLink.attr("href", $image.attr("src")), 0 !== $link.length && ($redactorImageLink.val($link.attr("href")), 
                        "_blank" == $link.attr("target") && $("#redactor-image-link-blank").prop("checked", !0));
                    } else $(".redactor-image-link-option").hide();
                    if (this.opts.imagePosition) {
                        var floatValue = "block" == $image.css("display") && "none" == $image.css("float") ? "center" : $image.css("float");
                        $("#redactor-image-align").val(floatValue);
                    } else $(".redactor-image-position-option").hide();
                    this.modal.show(), $("#redactor-image-title").focus();
                },
                setFloating: function($image) {
                    var floating = $("#redactor-image-align").val(), imageFloat = "", imageDisplay = "", imageMargin = "";
                    switch (floating) {
                      case "left":
                        imageFloat = "left", imageMargin = "0 " + this.opts.imageFloatMargin + " " + this.opts.imageFloatMargin + " 0";
                        break;

                      case "right":
                        imageFloat = "right", imageMargin = "0 0 " + this.opts.imageFloatMargin + " " + this.opts.imageFloatMargin;
                        break;

                      case "center":
                        imageDisplay = "block", imageMargin = "auto";
                    }
                    $image.css({
                        "float": imageFloat,
                        display: imageDisplay,
                        margin: imageMargin
                    }), $image.attr("rel", $image.attr("style"));
                },
                update: function($image) {
                    this.image.hideResize(), this.buffer.set();
                    var $link = $image.closest("a", this.$editor[0]), title = $("#redactor-image-title").val().replace(/(<([^>]+)>)/gi, "");
                    $image.attr("alt", title), this.image.setFloating($image);
                    var link = $.trim($("#redactor-image-link").val()), link = link.replace(/(<([^>]+)>)/gi, "");
                    if ("" !== link) {
                        var pattern = "((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}", re = new RegExp("^(http|ftp|https)://" + pattern, "i"), re2 = new RegExp("^" + pattern, "i");
                        -1 == link.search(re) && 0 === link.search(re2) && this.opts.linkProtocol && (link = this.opts.linkProtocol + "://" + link);
                        var target = $("#redactor-image-link-blank").prop("checked") ? !0 : !1;
                        if (0 === $link.length) {
                            var a = $('<a href="' + link + '">' + this.utils.getOuterHtml($image) + "</a>");
                            target && a.attr("target", "_blank"), $image.replaceWith(a);
                        } else $link.attr("href", link), target ? $link.attr("target", "_blank") : $link.removeAttr("target");
                    } else 0 !== $link.length && $link.replaceWith(this.utils.getOuterHtml($image));
                    this.modal.close(), this.observe.images(), this.code.sync();
                },
                setEditable: function($image) {
                    this.opts.imageEditable && $image.on("dragstart", $.proxy(this.image.onDrag, this));
                    var handler = $.proxy(function(e) {
                        this.observe.image = $image, this.image.resizer = this.image.loadEditableControls($image), 
                        $(document).on("mousedown.redactor-image-resize-hide." + this.uuid, $.proxy(this.image.hideResize, this)), 
                        this.opts.imageResizable && this.image.resizer.on("mousedown.redactor touchstart.redactor", $.proxy(function(e) {
                            this.image.setResizable(e, $image);
                        }, this));
                    }, this);
                    $image.off("mousedown.redactor").on("mousedown.redactor", $.proxy(this.image.hideResize, this)), 
                    $image.off("click.redactor touchstart.redactor").on("click.redactor touchstart.redactor", handler);
                },
                setResizable: function(e, $image) {
                    e.preventDefault(), this.image.resizeHandle = {
                        x: e.pageX,
                        y: e.pageY,
                        el: $image,
                        ratio: $image.width() / $image.height(),
                        h: $image.height()
                    }, e = e.originalEvent || e, e.targetTouches && (this.image.resizeHandle.x = e.targetTouches[0].pageX, 
                    this.image.resizeHandle.y = e.targetTouches[0].pageY), this.image.startResize();
                },
                startResize: function() {
                    $(document).on("mousemove.redactor-image-resize touchmove.redactor-image-resize", $.proxy(this.image.moveResize, this)), 
                    $(document).on("mouseup.redactor-image-resize touchend.redactor-image-resize", $.proxy(this.image.stopResize, this));
                },
                moveResize: function(e) {
                    e.preventDefault(), e = e.originalEvent || e;
                    var height = this.image.resizeHandle.h;
                    height += e.targetTouches ? e.targetTouches[0].pageY - this.image.resizeHandle.y : e.pageY - this.image.resizeHandle.y;
                    var width = Math.round(height * this.image.resizeHandle.ratio);
                    if (!(50 > height || 100 > width)) {
                        var height = Math.round(this.image.resizeHandle.el.width() / this.image.resizeHandle.ratio);
                        this.image.resizeHandle.el.attr({
                            width: width,
                            height: height
                        }), this.image.resizeHandle.el.width(width), this.image.resizeHandle.el.height(height), 
                        this.code.sync();
                    }
                },
                stopResize: function() {
                    this.handle = !1, $(document).off(".redactor-image-resize"), this.image.hideResize();
                },
                onDrag: function(e) {
                    return 0 !== this.$editor.find("#redactor-image-box").length ? (e.preventDefault(), 
                    !1) : void this.$editor.on("drop.redactor-image-inside-drop", $.proxy(function() {
                        setTimeout($.proxy(this.image.onDrop, this), 1);
                    }, this));
                },
                onDrop: function() {
                    this.image.fixImageSourceAfterDrop(), this.observe.images(), this.$editor.off("drop.redactor-image-inside-drop"), 
                    this.clean.clearUnverified(), this.code.sync();
                },
                fixImageSourceAfterDrop: function() {
                    this.$editor.find("img[data-save-url]").each(function() {
                        var $el = $(this);
                        $el.attr("src", $el.attr("data-save-url")), $el.removeAttr("data-save-url");
                    });
                },
                hideResize: function(e) {
                    if (!e || 0 === $(e.target).closest("#redactor-image-box", this.$editor[0]).length) {
                        if (e && "IMG" == e.target.tagName) {
                            var $image = $(e.target);
                            $image.attr("data-save-url", $image.attr("src"));
                        }
                        var imageBox = this.$editor.find("#redactor-image-box");
                        0 !== imageBox.length && ($("#redactor-image-editter").remove(), $("#redactor-image-resizer").remove(), 
                        imageBox.find("img").css({
                            marginTop: imageBox[0].style.marginTop,
                            marginBottom: imageBox[0].style.marginBottom,
                            marginLeft: imageBox[0].style.marginLeft,
                            marginRight: imageBox[0].style.marginRight
                        }), imageBox.css("margin", ""), imageBox.find("img").css("opacity", ""), imageBox.replaceWith(function() {
                            return $(this).contents();
                        }), $(document).off("mousedown.redactor-image-resize-hide." + this.uuid), "undefined" != typeof this.image.resizeHandle && this.image.resizeHandle.el.attr("rel", this.image.resizeHandle.el.attr("style")), 
                        this.code.sync());
                    }
                },
                loadResizableControls: function($image, imageBox) {
                    if (this.opts.imageResizable && !this.utils.isMobile()) {
                        var imageResizer = $('<span id="redactor-image-resizer" data-redactor="verified"></span>');
                        return this.utils.isDesktop() || imageResizer.css({
                            width: "15px",
                            height: "15px"
                        }), imageResizer.attr("contenteditable", !1), imageBox.append(imageResizer), imageBox.append($image), 
                        imageResizer;
                    }
                    return imageBox.append($image), !1;
                },
                loadEditableControls: function($image) {
                    var imageBox = $('<span id="redactor-image-box" data-redactor="verified">');
                    if (imageBox.css("float", $image.css("float")).attr("contenteditable", !1), "auto" != $image[0].style.margin ? (imageBox.css({
                        marginTop: $image[0].style.marginTop,
                        marginBottom: $image[0].style.marginBottom,
                        marginLeft: $image[0].style.marginLeft,
                        marginRight: $image[0].style.marginRight
                    }), $image.css("margin", "")) : imageBox.css({
                        display: "block",
                        margin: "auto"
                    }), $image.css("opacity", ".5").after(imageBox), this.opts.imageEditable) {
                        this.image.editter = $('<span id="redactor-image-editter" data-redactor="verified">' + this.lang.get("edit") + "</span>"), 
                        this.image.editter.attr("contenteditable", !1), this.image.editter.on("click", $.proxy(function() {
                            this.image.showEdit($image);
                        }, this)), imageBox.append(this.image.editter);
                        var editerWidth = this.image.editter.innerWidth();
                        this.image.editter.css("margin-left", "-" + editerWidth / 2 + "px");
                    }
                    return this.image.loadResizableControls($image, imageBox);
                },
                remove: function(image) {
                    var $image = $(image), $link = $image.closest("a", this.$editor[0]), $figure = $image.closest("figure", this.$editor[0]), $parent = $image.parent();
                    0 !== $("#redactor-image-box").length && ($parent = $("#redactor-image-box").parent());
                    var $next;
                    0 !== $figure.length ? ($next = $figure.next(), $figure.remove()) : 0 !== $link.length ? ($parent = $link.parent(), 
                    $link.remove()) : $image.remove(), $("#redactor-image-box").remove(), 0 !== $figure.length ? this.caret.setStart($next) : this.caret.setStart($parent), 
                    this.core.setCallback("imageDelete", $image[0].src, $image), this.modal.close(), 
                    this.code.sync();
                },
                insert: function(json, direct, e) {
                    if ("undefined" != typeof json.error) return this.modal.close(), this.selection.restore(), 
                    void this.core.setCallback("imageUploadError", json);
                    var $img;
                    "string" == typeof json ? $img = $(json).attr("data-redactor-inserted-image", "true") : ($img = $("<img>"), 
                    $img.attr("src", json.filelink).attr("data-redactor-inserted-image", "true"));
                    var node = $img, isP = this.utils.isCurrentOrParent("P");
                    if (isP && (node = $("<blockquote />").append($img)), direct) {
                        this.selection.removeMarkers();
                        var marker = this.selection.getMarker();
                        this.insert.nodeToCaretPositionFromPoint(e, marker);
                    } else this.modal.close();
                    this.selection.restore(), this.buffer.set(), this.insert.html(this.utils.getOuterHtml(node), !1);
                    var $image = this.$editor.find("img[data-redactor-inserted-image=true]").removeAttr("data-redactor-inserted-image");
                    isP ? $image.parent().contents().unwrap().wrap("<p />") : this.opts.linebreaks && (this.utils.isEmpty(this.code.get()) || $image.before("<br>"), 
                    $image.after("<br>")), "string" != typeof json && this.core.setCallback("imageUpload", $image, json);
                }
            };
        },
        indent: function() {
            return {
                increase: function() {
                    this.utils.browser("msie") || this.$editor.focus(), this.buffer.set(), this.selection.save();
                    var block = this.selection.getBlock();
                    block && "LI" == block.tagName ? this.indent.increaseLists() : block === !1 && this.opts.linebreaks ? this.indent.increaseText() : this.indent.increaseBlocks(), 
                    this.selection.restore(), this.code.sync();
                },
                increaseLists: function() {
                    document.execCommand("indent"), this.indent.fixEmptyIndent(), this.clean.normalizeLists(), 
                    this.clean.clearUnverified();
                },
                increaseBlocks: function() {
                    $.each(this.selection.getBlocks(), $.proxy(function(i, elem) {
                        if ("TD" !== elem.tagName && "TH" !== elem.tagName) {
                            var $el = this.utils.getAlignmentElement(elem), left = this.utils.normalize($el.css("margin-left")) + this.opts.indentValue;
                            $el.css("margin-left", left + "px");
                        }
                    }, this));
                },
                increaseText: function() {
                    var wrapper = this.selection.wrap("div");
                    $(wrapper).attr("data-tagblock", "redactor"), $(wrapper).css("margin-left", this.opts.indentValue + "px");
                },
                decrease: function() {
                    this.buffer.set(), this.selection.save();
                    var block = this.selection.getBlock();
                    block && "LI" == block.tagName ? this.indent.decreaseLists() : this.indent.decreaseBlocks(), 
                    this.selection.restore(), this.code.sync();
                },
                decreaseLists: function() {
                    document.execCommand("outdent");
                    var current = this.selection.getCurrent(), $item = $(current).closest("li", this.$editor[0]);
                    this.indent.fixEmptyIndent(), this.opts.linebreaks || 0 !== $item.length || (document.execCommand("formatblock", !1, "p"), 
                    this.$editor.find("ul, ol, blockquote, p").each($.proxy(this.utils.removeEmpty, this))), 
                    this.clean.clearUnverified();
                },
                decreaseBlocks: function() {
                    $.each(this.selection.getBlocks(), $.proxy(function(i, elem) {
                        var $el = this.utils.getAlignmentElement(elem), left = this.utils.normalize($el.css("margin-left")) - this.opts.indentValue;
                        0 >= left ? this.opts.linebreaks && "undefined" != typeof $el.data("tagblock") ? $el.replaceWith($el.html() + "<br />") : ($el.css("margin-left", ""), 
                        this.utils.removeEmptyAttr($el, "style")) : $el.css("margin-left", left + "px");
                    }, this));
                },
                fixEmptyIndent: function() {
                    var block = this.selection.getBlock();
                    if (this.range.collapsed && block && "LI" == block.tagName && this.utils.isEmpty($(block).text())) {
                        var $block = $(block);
                        $block.find("span").not(".redactor-selection-marker").contents().unwrap(), $block.append("<br>");
                    }
                }
            };
        },
        inline: function() {
            return {
                formatting: function(name) {
                    var type, value;
                    "undefined" != typeof this.formatting[name].style ? type = "style" : "undefined" != typeof this.formatting[name]["class"] && (type = "class"), 
                    type && (value = this.formatting[name][type]), this.inline.format(this.formatting[name].tag, type, value);
                },
                format: function(tag, type, value) {
                    var current = this.selection.getCurrent();
                    if (!(current && "TR" === current.tagName || this.utils.isCurrentOrParent("PRE") || this.utils.isCurrentOrParentHeader())) {
                        for (var tags = [ "b", "bold", "i", "italic", "underline", "strikethrough", "deleted", "superscript", "subscript" ], replaced = [ "strong", "strong", "em", "em", "u", "del", "del", "sup", "sub" ], i = 0; i < tags.length; i++) tag == tags[i] && (tag = replaced[i]);
                        if (this.opts.allowedTags) {
                            if (-1 == $.inArray(tag, this.opts.allowedTags)) return;
                        } else if (-1 !== $.inArray(tag, this.opts.deniedTags)) return;
                        this.inline.type = type || !1, this.inline.value = value || !1, this.buffer.set(), 
                        this.utils.browser("msie") || this.opts.linebreaks || this.$editor.focus(), this.selection.get(), 
                        this.range.collapsed ? this.inline.formatCollapsed(tag) : this.inline.formatMultiple(tag);
                    }
                },
                formatCollapsed: function(tag) {
                    var current = this.selection.getCurrent(), $parent = $(current).closest(tag + "[data-redactor-tag=" + tag + "]", this.$editor[0]);
                    if (0 !== $parent.length && "style" != this.inline.type && "SPAN" != $parent[0].tagName) return void (this.utils.isEmpty($parent.text()) ? (this.caret.setAfter($parent[0]), 
                    $parent.remove(), this.code.sync()) : this.utils.isEndOfElement($parent) && this.caret.setAfter($parent[0]));
                    var node = $("<" + tag + ">").attr("data-verified", "redactor").attr("data-redactor-tag", tag);
                    node.html(this.opts.invisibleSpace), node = this.inline.setFormat(node);
                    var node = this.insert.node(node);
                    this.caret.setEnd(node), this.code.sync();
                },
                formatMultiple: function(tag) {
                    if (this.inline.formatConvert(tag), this.selection.save(), document.execCommand("strikethrough"), 
                    this.$editor.find("strike").each($.proxy(function(i, s) {
                        var $el = $(s);
                        this.inline.formatRemoveSameChildren($el, tag);
                        var $span;
                        this.inline.type ? ($span = $("<span>").attr("data-redactor-tag", tag).attr("data-verified", "redactor"), 
                        $span = this.inline.setFormat($span)) : $span = $("<" + tag + ">").attr("data-redactor-tag", tag).attr("data-verified", "redactor"), 
                        $el.replaceWith($span.html($el.contents()));
                        var $parent = $span.parent();
                        if ("A" === $span[0].tagName && $parent && "U" === $parent[0].tagName && $span.parent().replaceWith($span), 
                        "span" == tag && $parent && "SPAN" === $parent[0].tagName && "style" === this.inline.type) for (var arr = this.inline.value.split(";"), z = 0; z < arr.length; z++) {
                            if ("" === arr[z]) return;
                            var style = arr[z].split(":");
                            $parent.css(style[0], ""), this.utils.removeEmptyAttr($parent, "style") && $parent.replaceWith($parent.contents());
                        }
                    }, this)), "span" != tag && this.$editor.find(this.opts.inlineTags.join(", ")).each($.proxy(function(i, s) {
                        var $el = $(s);
                        if ("U" === s.tagName && 0 === s.attributes.length) return void $el.replaceWith($el.contents());
                        var property = $el.css("text-decoration");
                        "line-through" === property && ($el.css("text-decoration", ""), this.utils.removeEmptyAttr($el, "style"));
                    }, this)), "del" != tag) {
                        var _this = this;
                        this.$editor.find("inline").each(function(i, s) {
                            _this.utils.replaceToTag(s, "del");
                        });
                    }
                    this.selection.restore(), this.code.sync();
                },
                formatRemoveSameChildren: function($el, tag) {
                    var self = this;
                    $el.children(tag).each(function() {
                        var $child = $(this);
                        if (!$child.hasClass("redactor-selection-marker")) if ("style" == self.inline.type) for (var arr = self.inline.value.split(";"), z = 0; z < arr.length; z++) {
                            if ("" === arr[z]) return;
                            var style = arr[z].split(":");
                            $child.css(style[0], ""), self.utils.removeEmptyAttr($child, "style") && $child.replaceWith($child.contents());
                        } else $child.contents().unwrap();
                    });
                },
                formatConvert: function(tag) {
                    this.selection.save();
                    var find = "";
                    "class" == this.inline.type ? find = "[data-redactor-class=" + this.inline.value + "]" : "style" == this.inline.type && (find = '[data-redactor-style="' + this.inline.value + '"]');
                    var self = this;
                    "del" != tag && this.$editor.find("del").each(function(i, s) {
                        self.utils.replaceToTag(s, "inline");
                    }), "span" != tag && this.$editor.find(tag).each(function() {
                        var $el = $(this);
                        $el.replaceWith($("<strike />").html($el.contents()));
                    }), this.$editor.find('[data-redactor-tag="' + tag + '"]' + find).each(function() {
                        if ("" !== find || "span" != tag || this.tagName.toLowerCase() != tag) {
                            var $el = $(this);
                            $el.replaceWith($("<strike />").html($el.contents()));
                        }
                    }), this.selection.restore();
                },
                setFormat: function(node) {
                    switch (this.inline.type) {
                      case "class":
                        node.hasClass(this.inline.value) ? (node.removeClass(this.inline.value), node.removeAttr("data-redactor-class")) : (node.addClass(this.inline.value), 
                        node.attr("data-redactor-class", this.inline.value));
                        break;

                      case "style":
                        node[0].style.cssText = this.inline.value, node.attr("data-redactor-style", this.inline.value);
                    }
                    return node;
                },
                removeStyle: function() {
                    this.buffer.set();
                    var current = this.selection.getCurrent(), nodes = this.selection.getInlines();
                    if (this.selection.save(), current && "SPAN" === current.tagName) {
                        var $s = $(current);
                        $s.removeAttr("style"), 0 === $s[0].attributes.length && $s.replaceWith($s.contents());
                    }
                    $.each(nodes, $.proxy(function(i, s) {
                        var $s = $(s);
                        -1 == $.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) || $s.hasClass("redactor-selection-marker") || ($s.removeAttr("style"), 
                        0 === $s[0].attributes.length && $s.replaceWith($s.contents()));
                    }, this)), this.selection.restore(), this.code.sync();
                },
                removeStyleRule: function(name) {
                    this.buffer.set();
                    var parent = this.selection.getParent(), nodes = this.selection.getInlines();
                    if (this.selection.save(), parent && "SPAN" === parent.tagName) {
                        var $s = $(parent);
                        $s.css(name, ""), this.utils.removeEmptyAttr($s, "style"), 0 === $s[0].attributes.length && $s.replaceWith($s.contents());
                    }
                    $.each(nodes, $.proxy(function(i, s) {
                        var $s = $(s);
                        -1 == $.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) || $s.hasClass("redactor-selection-marker") || ($s.css(name, ""), 
                        this.utils.removeEmptyAttr($s, "style"), 0 === $s[0].attributes.length && $s.replaceWith($s.contents()));
                    }, this)), this.selection.restore(), this.code.sync();
                },
                removeFormat: function() {
                    this.buffer.set();
                    var current = this.selection.getCurrent();
                    this.selection.save(), document.execCommand("removeFormat"), current && "SPAN" === current.tagName && $(current).replaceWith($(current).contents()), 
                    $.each(this.selection.getNodes(), $.proxy(function(i, s) {
                        var $s = $(s);
                        -1 == $.inArray(s.tagName.toLowerCase(), this.opts.inlineTags) || $s.hasClass("redactor-selection-marker") || $s.replaceWith($s.contents());
                    }, this)), this.selection.restore(), this.code.sync();
                },
                toggleClass: function(className) {
                    this.inline.format("span", "class", className);
                },
                toggleStyle: function(value) {
                    this.inline.format("span", "style", value);
                }
            };
        },
        insert: function() {
            return {
                set: function(html, clean) {
                    this.placeholder.remove(), html = this.clean.setVerified(html), "undefined" == typeof clean && (html = this.clean.onPaste(html, !1)), 
                    this.$editor.html(html), this.selection.remove(), this.focus.setEnd(), this.clean.normalizeLists(), 
                    this.code.sync(), this.observe.load(), "undefined" == typeof clean && setTimeout($.proxy(this.clean.clearUnverified, this), 10);
                },
                text: function(text) {
                    if (this.placeholder.remove(), text = text.toString(), text = $.trim(text), text = this.clean.getPlainText(text, !1), 
                    this.$editor.focus(), this.utils.browser("msie")) this.insert.htmlIe(text); else {
                        this.selection.get(), this.range.deleteContents();
                        var el = document.createElement("div");
                        el.innerHTML = text;
                        for (var node, lastNode, frag = document.createDocumentFragment(); node = el.firstChild; ) lastNode = frag.appendChild(node);
                        if (this.range.insertNode(frag), lastNode) {
                            var range = this.range.cloneRange();
                            range.setStartAfter(lastNode), range.collapse(!0), this.sel.removeAllRanges(), this.sel.addRange(range);
                        }
                    }
                    this.code.sync(), this.clean.clearUnverified();
                },
                htmlWithoutClean: function(html) {
                    this.insert.html(html, !1);
                },
                html: function(html, clean) {
                    this.placeholder.remove(), "undefined" == typeof clean && (clean = !0), this.opts.linebreaks || this.$editor.focus(), 
                    html = this.clean.setVerified(html), clean && (html = this.clean.onPaste(html)), 
                    this.utils.browser("msie") ? this.insert.htmlIe(html) : (this.clean.singleLine ? this.insert.execHtml(html) : document.execCommand("insertHTML", !1, html), 
                    this.insert.htmlFixMozilla()), this.clean.normalizeLists(), this.opts.linebreaks || this.$editor.find("p").each($.proxy(this.utils.removeEmpty, this)), 
                    this.code.sync(), this.observe.load(), clean && this.clean.clearUnverified();
                },
                htmlFixMozilla: function() {
                    if (this.utils.browser("mozilla")) {
                        var $next = $(this.selection.getBlock()).next();
                        $next.length > 0 && "P" == $next[0].tagName && "" === $next.html() && $next.remove();
                    }
                },
                htmlIe: function(html) {
                    if (this.utils.isIe11()) {
                        var parent = this.utils.isCurrentOrParent("P"), $html = $("<div>").append(html), blocksMatch = $html.contents().is("p, :header, dl, ul, ol, div, table, td, blockquote, pre, address, section, header, footer, aside, article");
                        return void (parent && blocksMatch ? this.insert.ie11FixInserting(parent, html) : this.insert.ie11PasteFrag(html));
                    }
                    document.selection.createRange().pasteHTML(html);
                },
                execHtml: function(html) {
                    html = this.clean.setVerified(html), this.selection.get(), this.range.deleteContents();
                    var el = document.createElement("div");
                    el.innerHTML = html;
                    for (var node, lastNode, frag = document.createDocumentFragment(); node = el.firstChild; ) lastNode = frag.appendChild(node);
                    this.range.insertNode(frag), this.range.collapse(!0), this.caret.setAfter(lastNode);
                },
                node: function(node, deleteContents) {
                    node = node[0] || node;
                    var html = this.utils.getOuterHtml(node);
                    return html = this.clean.setVerified(html), null !== html.match(/</g) && (node = $(html)[0]), 
                    this.selection.get(), deleteContents !== !1 && this.range.deleteContents(), this.range.insertNode(node), 
                    this.range.collapse(!1), this.selection.addRange(), node;
                },
                nodeToPoint: function(node, x, y) {
                    node = node[0] || node, this.selection.get();
                    var range;
                    if (document.caretPositionFromPoint) {
                        var pos = document.caretPositionFromPoint(x, y);
                        this.range.setStart(pos.offsetNode, pos.offset), this.range.collapse(!0), this.range.insertNode(node);
                    } else if (document.caretRangeFromPoint) range = document.caretRangeFromPoint(x, y), 
                    range.insertNode(node); else if ("undefined" != typeof document.body.createTextRange) {
                        range = document.body.createTextRange(), range.moveToPoint(x, y);
                        var endRange = range.duplicate();
                        endRange.moveToPoint(x, y), range.setEndPoint("EndToEnd", endRange), range.select();
                    }
                },
                nodeToCaretPositionFromPoint: function(e, node) {
                    node = node[0] || node;
                    var range, x = e.clientX, y = e.clientY;
                    if (document.caretPositionFromPoint) {
                        var pos = document.caretPositionFromPoint(x, y), sel = document.getSelection();
                        range = sel.getRangeAt(0), range.setStart(pos.offsetNode, pos.offset), range.collapse(!0), 
                        range.insertNode(node);
                    } else if (document.caretRangeFromPoint) range = document.caretRangeFromPoint(x, y), 
                    range.insertNode(node); else if ("undefined" != typeof document.body.createTextRange) {
                        range = document.body.createTextRange(), range.moveToPoint(x, y);
                        var endRange = range.duplicate();
                        endRange.moveToPoint(x, y), range.setEndPoint("EndToEnd", endRange), range.select();
                    }
                },
                ie11FixInserting: function(parent, html) {
                    var node = document.createElement("span");
                    node.className = "redactor-ie-paste", this.insert.node(node);
                    var parHtml = $(parent).html();
                    parHtml = "<p>" + parHtml.replace(/<span class="redactor-ie-paste"><\/span>/gi, "</p>" + html + "<p>") + "</p>", 
                    parHtml = parHtml.replace(/<p><\/p>/gi, ""), $(parent).replaceWith(parHtml);
                },
                ie11PasteFrag: function(html) {
                    this.selection.get(), this.range.deleteContents();
                    var el = document.createElement("div");
                    el.innerHTML = html;
                    for (var node, lastNode, frag = document.createDocumentFragment(); node = el.firstChild; ) lastNode = frag.appendChild(node);
                    this.range.insertNode(frag), this.range.collapse(!1), this.selection.addRange();
                }
            };
        },
        keydown: function() {
            return {
                init: function(e) {
                    if (!this.rtePaste) {
                        var key = e.which, arrow = key >= 37 && 40 >= key;
                        this.keydown.ctrl = e.ctrlKey || e.metaKey, this.keydown.current = this.selection.getCurrent(), 
                        this.keydown.parent = this.selection.getParent(), this.keydown.block = this.selection.getBlock(), 
                        this.keydown.pre = this.utils.isTag(this.keydown.current, "pre"), this.keydown.blockquote = this.utils.isTag(this.keydown.current, "blockquote"), 
                        this.keydown.figcaption = this.utils.isTag(this.keydown.current, "figcaption"), 
                        this.shortcuts.init(e, key), this.utils.isDesktop() && (this.keydown.checkEvents(arrow, key), 
                        this.keydown.setupBuffer(e, key)), this.keydown.addArrowsEvent(arrow), this.keydown.setupSelectAll(e, key);
                        var keydownStop = this.core.setCallback("keydown", e);
                        if (keydownStop === !1) return e.preventDefault(), !1;
                        if (this.opts.enterKey && (this.utils.browser("msie") || this.utils.browser("mozilla")) && (key === this.keyCode.DOWN || key === this.keyCode.RIGHT)) {
                            var isEndOfTable = !1, $table = !1;
                            if (this.keydown.block && "TD" === this.keydown.block.tagName && ($table = $(this.keydown.block).closest("table", this.$editor[0])), 
                            $table && $table.find("td").last()[0] === this.keydown.block && (isEndOfTable = !0), 
                            this.utils.isEndOfElement() && isEndOfTable) {
                                var node = $(this.opts.emptyHtml);
                                $table.after(node), this.caret.setStart(node);
                            }
                        }
                        if (this.opts.enterKey && key === this.keyCode.DOWN && this.keydown.onArrowDown(), 
                        !this.opts.enterKey && key === this.keyCode.ENTER) return e.preventDefault(), void (this.range.collapsed || this.range.deleteContents());
                        if (key == this.keyCode.ENTER && !e.shiftKey && !e.ctrlKey && !e.metaKey) {
                            var stop = this.core.setCallback("enter", e);
                            if (stop === !1) return e.preventDefault(), !1;
                            if (this.keydown.blockquote && this.keydown.exitFromBlockquote(e) === !0) return !1;
                            var current, $next;
                            if (this.keydown.pre) return this.keydown.insertNewLine(e);
                            if (this.keydown.blockquote || this.keydown.figcaption) return current = this.selection.getCurrent(), 
                            $next = $(current).next(), 0 !== $next.length && "BR" == $next[0].tagName ? this.keydown.insertBreakLine(e) : this.utils.isEndOfElement() && current && "SPAN" != current ? this.keydown.insertDblBreakLine(e) : this.keydown.insertBreakLine(e);
                            if (this.opts.linebreaks && !this.keydown.block) return current = this.selection.getCurrent(), 
                            $next = $(this.keydown.current).next(), 0 !== $next.length && "BR" == $next[0].tagName ? this.keydown.insertBreakLine(e) : current !== !1 && $(current).hasClass("redactor-invisible-space") ? (this.caret.setAfter(current), 
                            $(current).contents().unwrap(), this.keydown.insertDblBreakLine(e)) : this.utils.isEndOfEditor() ? this.keydown.insertDblBreakLine(e) : 0 === $next.length && current === !1 && "undefined" != typeof $next.context ? this.keydown.insertBreakLine(e) : this.keydown.insertBreakLine(e);
                            if (this.opts.linebreaks && this.keydown.block) setTimeout($.proxy(this.keydown.replaceDivToBreakLine, this), 1); else if (!this.opts.linebreaks && this.keydown.block) {
                                if (setTimeout($.proxy(this.keydown.replaceDivToParagraph, this), 1), "LI" === this.keydown.block.tagName) {
                                    current = this.selection.getCurrent();
                                    var $parent = $(current).closest("li", this.$editor[0]), $list = $parent.closest("ul,ol", this.$editor[0]);
                                    if (0 !== $parent.length && this.utils.isEmpty($parent.html()) && 0 === $list.next().length && this.utils.isEmpty($list.find("li").last().html())) {
                                        $list.find("li").last().remove();
                                        var node = $(this.opts.emptyHtml);
                                        return $list.after(node), this.caret.setStart(node), !1;
                                    }
                                }
                            } else if (!this.opts.linebreaks && !this.keydown.block) return this.keydown.insertParagraph(e);
                        }
                        if (key === this.keyCode.ENTER && (e.ctrlKey || e.shiftKey)) return this.keydown.onShiftEnter(e);
                        if (key === this.keyCode.TAB || e.metaKey && 221 === key || e.metaKey && 219 === key) return this.keydown.onTab(e, key);
                        if (key === this.keyCode.BACKSPACE || key === this.keyCode.DELETE) {
                            var nodes = this.selection.getNodes();
                            if (nodes) for (var last, len = nodes.length, i = 0; len > i; i++) {
                                var children = $(nodes[i]).children("img");
                                if (0 !== children.length) {
                                    var self = this;
                                    $.each(children, function(z, s) {
                                        var $s = $(s);
                                        "none" == $s.css("float") && (self.core.setCallback("imageDelete", s.src, $s), last = s);
                                    });
                                } else "IMG" == nodes[i].tagName && last != nodes[i] && (this.core.setCallback("imageDelete", nodes[i].src, $(nodes[i])), 
                                last = nodes[i]);
                            }
                        }
                        if (key === this.keyCode.BACKSPACE) {
                            var block = this.selection.getBlock(), indented = "0px" !== $(block).css("margin-left");
                            if (block && indented && this.range.collapsed && this.utils.isStartOfElement()) return this.indent.decrease(), 
                            void e.preventDefault();
                            if (this.utils.browser("mozilla")) {
                                var prev = this.selection.getPrev(), prev2 = $(prev).prev()[0];
                                prev && "HR" === prev.tagName && $(prev).remove(), prev2 && "HR" === prev2.tagName && $(prev2).remove();
                            }
                            this.keydown.removeInvisibleSpace(), this.keydown.removeEmptyListInTable(e);
                        }
                        this.code.sync();
                    }
                },
                checkEvents: function(arrow, key) {
                    arrow || "click" != this.core.getEvent() && "arrow" != this.core.getEvent() || (this.core.addEvent(!1), 
                    this.keydown.checkKeyEvents(key) && this.buffer.set());
                },
                checkKeyEvents: function(key) {
                    var k = this.keyCode, keys = [ k.BACKSPACE, k.DELETE, k.ENTER, k.ESC, k.TAB, k.CTRL, k.META, k.ALT, k.SHIFT ];
                    return -1 == $.inArray(key, keys) ? !0 : !1;
                },
                addArrowsEvent: function(arrow) {
                    return arrow ? "click" == this.core.getEvent() || "arrow" == this.core.getEvent() ? void this.core.addEvent(!1) : void this.core.addEvent("arrow") : void 0;
                },
                setupBuffer: function(e, key) {
                    return this.keydown.ctrl && 90 === key && !e.shiftKey && !e.altKey && this.opts.buffer.length ? (e.preventDefault(), 
                    void this.buffer.undo()) : this.keydown.ctrl && 90 === key && e.shiftKey && !e.altKey && 0 !== this.opts.rebuffer.length ? (e.preventDefault(), 
                    void this.buffer.redo()) : void (this.keydown.ctrl || (key == this.keyCode.BACKSPACE || key == this.keyCode.DELETE || key == this.keyCode.ENTER && !e.ctrlKey && !e.shiftKey) && this.buffer.set());
                },
                setupSelectAll: function(e, key) {
                    this.keydown.ctrl && 65 === key ? this.utils.enableSelectAll() : key == this.keyCode.LEFT_WIN || this.keydown.ctrl || this.utils.disableSelectAll();
                },
                onArrowDown: function() {
                    for (var tags = [ this.keydown.blockquote, this.keydown.pre, this.keydown.figcaption ], i = 0; i < tags.length; i++) if (tags[i]) return this.keydown.insertAfterLastElement(tags[i]), 
                    !1;
                },
                onShiftEnter: function(e) {
                    return this.buffer.set(), this.utils.isEndOfElement() ? this.keydown.insertDblBreakLine(e) : this.keydown.insertBreakLine(e);
                },
                onTab: function(e, key) {
                    if (!this.opts.tabKey) return !0;
                    if (this.utils.isEmpty(this.code.get()) && this.opts.tabAsSpaces === !1) return !0;
                    e.preventDefault();
                    var node;
                    return this.keydown.pre && !e.shiftKey ? (node = this.opts.preSpaces ? document.createTextNode(Array(this.opts.preSpaces + 1).join(" ")) : document.createTextNode("	"), 
                    this.insert.node(node), this.code.sync()) : this.opts.tabAsSpaces !== !1 ? (node = document.createTextNode(Array(this.opts.tabAsSpaces + 1).join(" ")), 
                    this.insert.node(node), this.code.sync()) : e.metaKey && 219 === key ? this.indent.decrease() : e.metaKey && 221 === key ? this.indent.increase() : e.shiftKey ? this.indent.decrease() : this.indent.increase(), 
                    !1;
                },
                replaceDivToBreakLine: function() {
                    var blockElem = this.selection.getBlock(), blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, "");
                    if (("DIV" === blockElem.tagName || "P" === blockElem.tagName) && "" === blockHtml && !$(blockElem).hasClass("redactor-editor")) {
                        var br = document.createElement("br");
                        return $(blockElem).replaceWith(br), this.caret.setBefore(br), this.code.sync(), 
                        !1;
                    }
                },
                replaceDivToParagraph: function() {
                    var blockElem = this.selection.getBlock(), blockHtml = blockElem.innerHTML.replace(/<br\s?\/?>/gi, "");
                    if ("DIV" === blockElem.tagName && this.utils.isEmpty(blockHtml) && !$(blockElem).hasClass("redactor-editor")) {
                        var p = document.createElement("p");
                        return p.innerHTML = this.opts.invisibleSpace, $(blockElem).replaceWith(p), this.caret.setStart(p), 
                        this.code.sync(), !1;
                    }
                    this.opts.cleanStyleOnEnter && "P" == blockElem.tagName && $(blockElem).removeAttr("class").removeAttr("style");
                },
                insertParagraph: function(e) {
                    e.preventDefault(), this.selection.get();
                    var p = document.createElement("p");
                    return p.innerHTML = this.opts.invisibleSpace, this.range.deleteContents(), this.range.insertNode(p), 
                    this.caret.setStart(p), this.code.sync(), !1;
                },
                exitFromBlockquote: function(e) {
                    if (this.utils.isEndOfElement()) {
                        var tmp = $.trim($(this.keydown.block).html());
                        if (-1 != tmp.search(/(<br\s?\/?>){2}$/i)) {
                            if (e.preventDefault(), this.opts.linebreaks) {
                                var br = document.createElement("br");
                                $(this.keydown.blockquote).after(br), this.caret.setBefore(br), $(this.keydown.block).html(tmp.replace(/<br\s?\/?>$/i, ""));
                            } else {
                                var node = $(this.opts.emptyHtml);
                                $(this.keydown.blockquote).after(node), this.caret.setStart(node);
                            }
                            return !0;
                        }
                    }
                },
                insertAfterLastElement: function(element) {
                    if (this.utils.isEndOfElement()) if (this.buffer.set(), this.opts.linebreaks) {
                        var contents = $("<div>").append($.trim(this.$editor.html())).contents(), last = contents.last()[0];
                        if ("SPAN" == last.tagName && "" === last.innerHTML && (last = contents.prev()[0]), 
                        this.utils.getOuterHtml(last) != this.utils.getOuterHtml(element)) return;
                        var br = document.createElement("br");
                        $(element).after(br), this.caret.setAfter(br);
                    } else {
                        if (this.$editor.contents().last()[0] !== element) return;
                        var node = $(this.opts.emptyHtml);
                        $(element).after(node), this.caret.setStart(node);
                    }
                },
                insertNewLine: function(e) {
                    e.preventDefault();
                    var node = document.createTextNode("\n");
                    return this.selection.get(), this.range.deleteContents(), this.range.insertNode(node), 
                    this.caret.setAfter(node), this.code.sync(), !1;
                },
                insertBreakLine: function(e) {
                    return this.keydown.insertBreakLineProcessing(e);
                },
                insertDblBreakLine: function(e) {
                    return this.keydown.insertBreakLineProcessing(e, !0);
                },
                insertBreakLineProcessing: function(e, dbl) {
                    e.stopPropagation(), this.selection.get();
                    var br1 = document.createElement("br");
                    this.utils.browser("msie") ? (this.range.collapse(!1), this.range.setEnd(this.range.endContainer, this.range.endOffset)) : this.range.deleteContents(), 
                    this.range.insertNode(br1);
                    var $parentA = $(br1).parent("a");
                    if ($parentA.length > 0 && ($parentA.find(br1).remove(), $parentA.after(br1)), dbl === !0) {
                        var $next = $(br1).next();
                        if (0 !== $next.length && "BR" === $next[0].tagName && this.utils.isEndOfEditor()) return this.caret.setAfter(br1), 
                        this.code.sync(), !1;
                        var br2 = document.createElement("br");
                        this.range.insertNode(br2), this.caret.setAfter(br2);
                    } else if (this.utils.browser("msie")) {
                        var space = document.createElement("span");
                        space.innerHTML = "&#x200b;", $(br1).after(space), this.caret.setAfter(space), $(space).remove();
                    } else {
                        var range = document.createRange();
                        range.setStartAfter(br1), range.collapse(!0);
                        var selection = window.getSelection();
                        selection.removeAllRanges(), selection.addRange(range);
                    }
                    return this.code.sync(), !1;
                },
                removeInvisibleSpace: function() {
                    var $current = $(this.keydown.current);
                    0 === $current.text().search(/^\u200B$/g) && $current.remove();
                },
                removeEmptyListInTable: function(e) {
                    var $current = $(this.keydown.current), $parent = $(this.keydown.parent), td = $current.closest("td", this.$editor[0]);
                    if (0 !== td.length && $current.closest("li", this.$editor[0]) && 1 === $parent.children("li").length) {
                        if (!this.utils.isEmpty($current.text())) return;
                        e.preventDefault(), $current.remove(), $parent.remove(), this.caret.setStart(td);
                    }
                }
            };
        },
        keyup: function() {
            return {
                init: function(e) {
                    if (!this.rtePaste) {
                        var key = e.which;
                        this.keyup.current = this.selection.getCurrent(), this.keyup.parent = this.selection.getParent();
                        var $parent = this.utils.isRedactorParent($(this.keyup.parent).parent()), keyupStop = this.core.setCallback("keyup", e);
                        if (keyupStop === !1) return e.preventDefault(), !1;
                        if (!this.opts.linebreaks && 3 === this.keyup.current.nodeType && this.keyup.current.length <= 1 && (this.keyup.parent === !1 || "BODY" == this.keyup.parent.tagName) && this.keyup.replaceToParagraph(), 
                        !this.opts.linebreaks && this.utils.isRedactorParent(this.keyup.current) && "DIV" === this.keyup.current.tagName && this.keyup.replaceToParagraph(!1), 
                        this.opts.linebreaks || !$(this.keyup.parent).hasClass("redactor-invisible-space") || $parent !== !1 && "BODY" != $parent[0].tagName || ($(this.keyup.parent).contents().unwrap(), 
                        this.keyup.replaceToParagraph()), this.linkify.isEnabled() && this.linkify.isKey(key) && this.linkify.format(), 
                        key === this.keyCode.DELETE || key === this.keyCode.BACKSPACE) {
                            if (this.utils.browser("mozilla")) {
                                var td = $(this.keydown.current).closest("td", this.$editor[0]);
                                if (0 !== td.size() && "" !== td.text()) return e.preventDefault(), !1;
                            }
                            return this.clean.clearUnverified(), this.observe.image ? (e.preventDefault(), this.image.hideResize(), 
                            this.buffer.set(), this.image.remove(this.observe.image), this.observe.image = !1, 
                            !1) : (this.$editor.find("p").each($.proxy(function(i, s) {
                                this.utils.removeEmpty(i, $(s).html());
                            }, this)), this.opts.linebreaks && this.keyup.current && "DIV" == this.keyup.current.tagName && this.utils.isEmpty(this.keyup.current.innerHTML) && ($(this.keyup.current).after(this.selection.getMarkerAsHtml()), 
                            this.selection.restore(), $(this.keyup.current).remove()), this.keyup.formatEmpty(e));
                        }
                    }
                },
                replaceToParagraph: function(clone) {
                    var node, $current = $(this.keyup.current);
                    node = clone === !1 ? $("<p>").append($current.html()) : $("<p>").append($current.clone()), 
                    $current.replaceWith(node);
                    var next = $(node).next();
                    "undefined" != typeof next[0] && "BR" == next[0].tagName && next.remove(), this.caret.setEnd(node);
                },
                formatEmpty: function(e) {
                    var html = $.trim(this.$editor.html());
                    if (this.utils.isEmpty(html)) return e.preventDefault(), this.opts.linebreaks ? (this.$editor.html(this.selection.getMarkerAsHtml()), 
                    this.selection.restore()) : (this.$editor.html(this.opts.emptyHtml), this.focus.setStart()), 
                    this.code.sync(), !1;
                }
            };
        },
        lang: function() {
            return {
                load: function() {
                    this.opts.curLang = this.opts.langs[this.opts.lang];
                },
                get: function(name) {
                    return "undefined" != typeof this.opts.curLang[name] ? this.opts.curLang[name] : "";
                }
            };
        },
        line: function() {
            return {
                insert: function() {
                    this.buffer.set();
                    var blocks = this.selection.getBlocks();
                    return blocks[0] !== !1 && this.line.isExceptLastOrFirst(blocks) ? void (this.utils.browser("msie") || this.$editor.focus()) : void (this.utils.browser("msie") ? this.line.insertInIe() : this.line.insertInOthersBrowsers());
                },
                isExceptLastOrFirst: function(blocks) {
                    var exceptTags = [ "li", "td", "th", "blockquote", "figcaption", "pre", "dl", "dt", "dd" ], first = blocks[0].tagName.toLowerCase(), last = this.selection.getLastBlock();
                    last = "undefined" == typeof last ? first : last.tagName.toLowerCase();
                    var firstFound = -1 != $.inArray(first, exceptTags), lastFound = -1 != $.inArray(last, exceptTags);
                    return firstFound && lastFound || firstFound ? !0 : void 0;
                },
                insertInIe: function() {
                    this.utils.saveScroll(), this.buffer.set(), this.insert.node(document.createElement("hr")), 
                    this.utils.restoreScroll(), this.code.sync();
                },
                insertInOthersBrowsers: function() {
                    this.buffer.set();
                    var extra = '<p id="redactor-insert-line"><br /></p>';
                    this.opts.linebreaks && (extra = '<br id="redactor-insert-line">'), document.execCommand("insertHtml", !1, "<hr>" + extra), 
                    this.line.setFocus(), this.code.sync();
                },
                setFocus: function() {
                    var node = this.$editor.find("#redactor-insert-line"), next = $(node).next()[0], target = next;
                    this.utils.browser("mozilla") && next && "" === next.innerHTML && (target = $(next).next()[0], 
                    $(next).remove()), target ? (node.remove(), this.opts.linebreaks || (this.$editor.focus(), 
                    this.line.setStart(target))) : (node.removeAttr("id"), this.line.setStart(node[0]));
                },
                setStart: function(node) {
                    if ("undefined" != typeof node) {
                        var textNode = document.createTextNode("​");
                        this.selection.get(), this.range.setStart(node, 0), this.range.insertNode(textNode), 
                        this.range.collapse(!0), this.selection.addRange();
                    }
                }
            };
        },
        link: function() {
            return {
                show: function(e) {
                    "undefined" != typeof e && e.preventDefault && e.preventDefault(), this.observe.isCurrent("a") ? this.modal.load("link", this.lang.get("link_edit"), 600) : this.modal.load("link", this.lang.get("link_insert"), 600), 
                    this.modal.createCancelButton();
                    var buttonText = this.observe.isCurrent("a") ? this.lang.get("edit") : this.lang.get("insert");
                    this.link.buttonInsert = this.modal.createActionButton(buttonText), this.selection.get(), 
                    this.link.getData(), this.link.cleanUrl(), "_blank" == this.link.target && $("#redactor-link-blank").prop("checked", !0), 
                    this.link.$inputUrl = $("#redactor-link-url"), this.link.$inputText = $("#redactor-link-url-text"), 
                    this.link.$inputText.val(this.link.text), this.link.$inputUrl.val(this.link.url), 
                    this.link.buttonInsert.on("click", $.proxy(this.link.insert, this)), $(".redactor-link-tooltip").remove(), 
                    this.selection.save(), this.modal.show(), this.link.$inputUrl.focus();
                },
                cleanUrl: function() {
                    var thref = self.location.href.replace(/\/$/i, "");
                    if ("undefined" != typeof this.link.url && (this.link.url = this.link.url.replace(thref, ""), 
                    this.link.url = this.link.url.replace(/^\/#/, "#"), this.link.url = this.link.url.replace("mailto:", ""), 
                    !this.opts.linkProtocol)) {
                        var re = new RegExp("^(http|ftp|https)://" + self.location.host, "i");
                        this.link.url = this.link.url.replace(re, "");
                    }
                },
                getData: function() {
                    this.link.$node = !1;
                    var $el = $(this.selection.getCurrent()).closest("a", this.$editor[0]);
                    0 !== $el.length && "A" === $el[0].tagName ? (this.link.$node = $el, this.link.url = $el.attr("href"), 
                    this.link.text = $el.text(), this.link.target = $el.attr("target")) : (this.link.text = this.sel.toString(), 
                    this.link.url = "", this.link.target = "");
                },
                insert: function() {
                    this.placeholder.remove();
                    var target = "", link = this.link.$inputUrl.val(), text = this.link.$inputText.val().replace(/(<([^>]+)>)/gi, "");
                    if ("" === $.trim(link)) return void this.link.$inputUrl.addClass("redactor-input-error").on("keyup", function() {
                        $(this).removeClass("redactor-input-error"), $(this).off("keyup");
                    });
                    if (-1 != link.search("@") && /(http|ftp|https):\/\//i.test(link) === !1) link = "mailto:" + link; else if (0 !== link.search("#")) {
                        $("#redactor-link-blank").prop("checked") && (target = "_blank");
                        var pattern = "((xn--)?[a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,}", re = new RegExp("^(http|ftp|https)://" + pattern, "i"), re2 = new RegExp("^" + pattern, "i"), re3 = new RegExp(".(html|php)$", "i");
                        -1 == link.search(re) && -1 == link.search(re3) && 0 === link.search(re2) && this.opts.linkProtocol && (link = this.opts.linkProtocol + "://" + link);
                    }
                    this.link.set(text, link, target), this.modal.close();
                },
                set: function(text, link, target) {
                    text = $.trim(text.replace(/<|>/g, "")), this.selection.restore();
                    var blocks = this.selection.getBlocks();
                    if ("" !== text || "" !== link) {
                        if ("" === text && "" !== link && (text = link), this.link.$node) {
                            this.buffer.set();
                            var $link = this.link.$node, $el = $link.children();
                            if ($el.length > 0) {
                                for (;$el.length; ) $el = $el.children();
                                $el = $el.end();
                            } else $el = $link;
                            $link.attr("href", link), $el.text(text), "" !== target ? $link.attr("target", target) : $link.removeAttr("target"), 
                            this.selection.selectElement($link), this.code.sync();
                        } else {
                            if (this.utils.browser("mozilla") && "" === this.link.text) {
                                var $a = $("<a />").attr("href", link).text(text);
                                "" !== target && $a.attr("target", target), $a = $(this.insert.node($a)), this.opts.linebreaks && $a.after("&nbsp;"), 
                                this.selection.selectElement($a);
                            } else {
                                var $a;
                                this.utils.browser("msie") ? ($a = $('<a href="' + link + '">').text(text), "" !== target && $a.attr("target", target), 
                                $a = $(this.insert.node($a)), this.selection.getText().match(/\s$/) && $a.after(" "), 
                                this.selection.selectElement($a)) : (document.execCommand("createLink", !1, link), 
                                $a = $(this.selection.getCurrent()).closest("a", this.$editor[0]), this.utils.browser("mozilla") && ($a = $('a[_moz_dirty=""]')), 
                                "" !== target && $a.attr("target", target), $a.removeAttr("style").removeAttr("_moz_dirty"), 
                                this.selection.getText().match(/\s$/) && $a.after(" "), ("" !== this.link.text || this.link.text != text) && (!this.opts.linebreaks && blocks && blocks.length <= 1 ? $a.text(text) : this.opts.linebreaks && $a.text(text), 
                                this.selection.selectElement($a)));
                            }
                            this.code.sync(), this.core.setCallback("insertedLink", $a);
                        }
                        setTimeout($.proxy(function() {
                            this.observe.links();
                        }, this), 5);
                    }
                },
                unlink: function(e) {
                    "undefined" != typeof e && e.preventDefault && e.preventDefault();
                    var nodes = this.selection.getNodes();
                    if (nodes) {
                        this.buffer.set();
                        for (var len = nodes.length, links = [], i = 0; len > i; i++) {
                            "A" === nodes[i].tagName && links.push(nodes[i]);
                            var $node = $(nodes[i]).closest("a", this.$editor[0]);
                            $node.replaceWith($node.contents());
                        }
                        this.core.setCallback("deletedLink", links), $(".redactor-link-tooltip").remove(), 
                        this.code.sync();
                    }
                },
                toggleClass: function(className) {
                    this.link.setClass(className, "toggleClass");
                },
                addClass: function(className) {
                    this.link.setClass(className, "addClass");
                },
                removeClass: function(className) {
                    this.link.setClass(className, "removeClass");
                },
                setClass: function(className, func) {
                    var links = this.selection.getInlinesTags([ "a" ]);
                    links !== !1 && $.each(links, function() {
                        $(this)[func](className);
                    });
                }
            };
        },
        linkify: function() {
            return {
                isKey: function(key) {
                    return key == this.keyCode.ENTER || key == this.keyCode.SPACE;
                },
                isEnabled: function() {
                    return this.opts.convertLinks && (this.opts.convertUrlLinks || this.opts.convertImageLinks || this.opts.convertVideoLinks) && !this.utils.isCurrentOrParent("PRE");
                },
                format: function() {
                    var linkify = this.linkify, opts = this.opts;
                    this.$editor.find(":not(iframe,img,a,pre)").addBack().contents().filter(function() {
                        return 3 === this.nodeType && "" != $.trim(this.nodeValue) && !$(this).parent().is("pre") && (this.nodeValue.match(opts.linkify.regexps.youtube) || this.nodeValue.match(opts.linkify.regexps.vimeo) || this.nodeValue.match(opts.linkify.regexps.image) || this.nodeValue.match(opts.linkify.regexps.url));
                    }).each(function() {
                        var text = $(this).text(), html = text;
                        opts.convertVideoLinks && (html.match(opts.linkify.regexps.youtube) || html.match(opts.linkify.regexps.vimeo)) ? html = linkify.convertVideoLinks(html) : opts.convertImageLinks && html.match(opts.linkify.regexps.image) ? html = linkify.convertImages(html) : opts.convertUrlLinks && (html = linkify.convertLinks(html)), 
                        $(this).before(text.replace(text, html)).remove();
                    });
                    var objects = this.$editor.find(".redactor-linkify-object").each(function() {
                        var $el = $(this);
                        return $el.removeClass("redactor-linkify-object"), "" === $el.attr("class") && $el.removeAttr("class"), 
                        $el[0];
                    });
                    setTimeout($.proxy(function() {
                        this.observe.load(), this.core.setCallback("linkify", objects);
                    }, this), 100), this.code.sync();
                },
                convertVideoLinks: function(html) {
                    var iframeStart = '<iframe class="redactor-linkify-object" width="500" height="281" src="', iframeEnd = '" frameborder="0" allowfullscreen></iframe>';
                    return html.match(this.opts.linkify.regexps.youtube) && (html = html.replace(this.opts.linkify.regexps.youtube, iframeStart + "//www.youtube.com/embed/$1" + iframeEnd)), 
                    html.match(this.opts.linkify.regexps.vimeo) && (html = html.replace(this.opts.linkify.regexps.vimeo, iframeStart + "//player.vimeo.com/video/$2" + iframeEnd)), 
                    html;
                },
                convertImages: function(html) {
                    var matches = html.match(this.opts.linkify.regexps.image);
                    return matches && (html = html.replace(html, '<img src="' + matches + '" class="redactor-linkify-object" />'), 
                    this.opts.linebreaks && (this.utils.isEmpty(this.code.get()) || (html = "<br>" + html)), 
                    html += "<br>"), html;
                },
                convertLinks: function(html) {
                    var matches = html.match(this.opts.linkify.regexps.url);
                    if (matches) {
                        matches = $.grep(matches, function(v, k) {
                            return $.inArray(v, matches) === k;
                        });
                        for (var length = matches.length, i = 0; length > i; i++) {
                            var href = matches[i], text = href, linkProtocol = this.opts.linkProtocol + "://";
                            null !== href.match(/(https?|ftp):\/\//i) && (linkProtocol = ""), text.length > this.opts.linkSize && (text = text.substring(0, this.opts.linkSize) + "..."), 
                            -1 === text.search("%") && (text = decodeURIComponent(text));
                            var regexB = "\\b";
                            -1 != $.inArray(href.slice(-1), [ "/", "&", "=" ]) && (regexB = "");
                            var regexp = new RegExp("(" + href.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + regexB + ")", "g");
                            html = html.replace(regexp, '<a href="' + linkProtocol + $.trim(href) + '" class="redactor-linkify-object">' + $.trim(text) + "</a>");
                        }
                    }
                    return html;
                }
            };
        },
        list: function() {
            return {
                toggle: function(cmd) {
                    this.placeholder.remove(), this.utils.browser("msie") || this.$editor.focus(), this.buffer.set(), 
                    this.selection.save();
                    var parent = this.selection.getParent(), $list = $(parent).closest("ol, ul", this.$editor[0]);
                    this.utils.isRedactorParent($list) || 0 === $list.length || ($list = !1);
                    var isUnorderedCmdOrdered, isOrderedCmdUnordered, remove = !1;
                    if ($list && $list.length) {
                        remove = !0;
                        var listTag = $list[0].tagName;
                        isUnorderedCmdOrdered = "orderedlist" === cmd && "UL" === listTag, isOrderedCmdUnordered = "unorderedlist" === cmd && "OL" === listTag;
                    }
                    isUnorderedCmdOrdered ? this.utils.replaceToTag($list, "ol") : isOrderedCmdUnordered ? this.utils.replaceToTag($list, "ul") : remove ? this.list.remove(cmd, $list) : this.list.insert(cmd), 
                    this.selection.restore(), this.code.sync();
                },
                insert: function(cmd) {
                    var current = this.selection.getCurrent(), $td = $(current).closest("td, th", this.$editor[0]);
                    this.utils.browser("msie") && this.opts.linebreaks ? this.list.insertInIe(cmd) : document.execCommand("insert" + cmd);
                    var parent = this.selection.getParent(), $list = $(parent).closest("ol, ul", this.$editor[0]);
                    if (0 !== $td.length) {
                        var newTd = $td.clone();
                        $td.after(newTd).remove("");
                    }
                    if (this.utils.isEmpty($list.find("li").text())) {
                        var $children = $list.children("li");
                        $children.find("br").remove(), $children.append(this.selection.getMarkerAsHtml()), 
                        this.opts.linebreaks && this.utils.browser("mozilla") && 2 == $children.size() && this.utils.isEmpty($children.eq(1).text()) && $children.eq(1).remove();
                    }
                    if ($list.length) {
                        var $listParent = $list.parent();
                        this.utils.isRedactorParent($listParent) && "LI" != $listParent[0].tagName && this.utils.isBlock($listParent[0]) && $listParent.replaceWith($listParent.contents());
                    }
                    this.utils.browser("msie") || this.$editor.focus(), this.clean.clearUnverified();
                },
                insertInIe: function(cmd) {
                    var wrapper = this.selection.wrap("div"), wrapperHtml = $(wrapper).html(), tmpList = $("orderedlist" == cmd ? "<ol>" : "<ul>"), tmpLi = $("<li>");
                    if ("" === $.trim(wrapperHtml)) tmpLi.append(this.selection.getMarkerAsHtml()), 
                    tmpList.append(tmpLi), this.$editor.find("#selection-marker-1").replaceWith(tmpList); else {
                        var items = wrapperHtml.split(/<br\s?\/?>/gi);
                        if (items) for (var i = 0; i < items.length; i++) "" !== $.trim(items[i]) && tmpList.append($("<li>").html(items[i])); else tmpLi.append(wrapperHtml), 
                        tmpList.append(tmpLi);
                        $(wrapper).replaceWith(tmpList);
                    }
                },
                remove: function(cmd, $list) {
                    $.inArray("ul", this.selection.getBlocks()) && (cmd = "unorderedlist"), document.execCommand("insert" + cmd);
                    var $current = $(this.selection.getCurrent());
                    this.indent.fixEmptyIndent(), this.opts.linebreaks || 0 !== $current.closest("li, th, td", this.$editor[0]).length || (document.execCommand("formatblock", !1, "p"), 
                    this.$editor.find("ul, ol, blockquote").each($.proxy(this.utils.removeEmpty, this)));
                    var $table = $(this.selection.getCurrent()).closest("table", this.$editor[0]), $prev = $table.prev();
                    this.opts.linebreaks || 0 === $table.length || 0 === $prev.length || "BR" != $prev[0].tagName || $prev.remove(), 
                    this.clean.clearUnverified();
                }
            };
        },
        modal: function() {
            return {
                callbacks: {},
                loadTemplates: function() {
                    this.opts.modal = {
                        imageEdit: String() + '<section id="redactor-modal-image-edit"><label>' + this.lang.get("title") + '</label><input class="form-control" type="text" id="redactor-image-title" /><label class="redactor-image-link-option">' + this.lang.get("link") + '</label><input class="form-control" type="text" id="redactor-image-link" class="redactor-image-link-option" aria-label="' + this.lang.get("link") + '" /><label class="redactor-image-link-option"><input class="form-control" type="checkbox" id="redactor-image-link-blank" aria-label="' + this.lang.get("link_new_tab") + '"> ' + this.lang.get("link_new_tab") + '</label><label class="redactor-image-position-option">' + this.lang.get("image_position") + '</label><select class="redactor-image-position-option" id="redactor-image-align" aria-label="' + this.lang.get("image_position") + '"><option value="none">' + this.lang.get("none") + '</option><option value="left">' + this.lang.get("left") + '</option><option value="center">' + this.lang.get("center") + '</option><option value="right">' + this.lang.get("right") + "</option></select></section>",
                        image: String() + '<section id="redactor-modal-image-insert"><div id="redactor-modal-image-droparea"></div></section>',
                        file: String() + '<section id="redactor-modal-file-insert"><div id="redactor-modal-file-upload-box"><label>' + this.lang.get("filename") + '</label><input class="form-control" type="text" id="redactor-filename" aria-label="' + this.lang.get("filename") + '" /><br><br><div id="redactor-modal-file-upload"></div></div></section>',
                        link: String() + '<section id="redactor-modal-link-insert"><label>URL</label><input class="form-control" type="url" id="redactor-link-url" aria-label="URL" /><label>' + this.lang.get("text") + '</label><input class="form-control" type="text" id="redactor-link-url-text" aria-label="' + this.lang.get("text") + '" /><label><input class="form-control" type="checkbox" id="redactor-link-blank"> ' + this.lang.get("link_new_tab") + "</label></section>"
                    }, $.extend(this.opts, this.opts.modal);
                },
                addCallback: function(name, callback) {
                    this.modal.callbacks[name] = callback;
                },
                createTabber: function($modal) {
                    this.modal.$tabber = $("<div>").attr("id", "redactor-modal-tabber"), $modal.prepend(this.modal.$tabber);
                },
                addTab: function(id, name, active) {
                    var $tab = $('<a href="#" rel="tab' + id + '">').text(name);
                    active && $tab.addClass("active");
                    var self = this;
                    $tab.on("click", function(e) {
                        e.preventDefault(), $(".redactor-tab").hide(), $(".redactor-" + $(this).attr("rel")).show(), 
                        self.modal.$tabber.find("a").removeClass("active"), $(this).addClass("active");
                    }), this.modal.$tabber.append($tab);
                },
                addTemplate: function(name, template) {
                    this.opts.modal[name] = template;
                },
                getTemplate: function(name) {
                    return this.opts.modal[name];
                },
                getModal: function() {
                    return this.$modalBody.find("section");
                },
                load: function(templateName, title, width) {
                    this.modal.templateName = templateName, this.modal.width = width, this.modal.build(), 
                    this.modal.enableEvents(), this.modal.setTitle(title), this.modal.setDraggable(), 
                    this.modal.setContent(), "undefined" != typeof this.modal.callbacks[templateName] && this.modal.callbacks[templateName].call(this);
                },
                show: function() {
                    this.utils.disableBodyScroll(), this.utils.isMobile() ? this.modal.showOnMobile() : this.modal.showOnDesktop(), 
                    this.opts.highContrast && this.$modalBox.addClass("redactor-modal-contrast"), this.$modalOverlay.show(), 
                    this.$modalBox.show(), this.$modal.attr("tabindex", "-1"), this.$modal.focus(), 
                    this.modal.setButtonsWidth(), this.utils.saveScroll(), this.utils.isMobile() || (setTimeout($.proxy(this.modal.showOnDesktop, this), 0), 
                    $(window).on("resize.redactor-modal", $.proxy(this.modal.resize, this))), this.core.setCallback("modalOpened", this.modal.templateName, this.$modal), 
                    $(document).off("focusin.modal"), this.$modal.find("input[type=text],input[type=url],input[type=email]").on("keydown.redactor-modal", $.proxy(this.modal.setEnter, this));
                },
                showOnDesktop: function() {
                    var height = this.$modal.outerHeight(), windowHeight = $(window).height(), windowWidth = $(window).width();
                    return this.modal.width > windowWidth ? void this.$modal.css({
                        width: "96%",
                        marginTop: windowHeight / 2 - height / 2 + "px"
                    }) : void (height > windowHeight ? this.$modal.css({
                        width: this.modal.width + "px",
                        marginTop: "20px"
                    }) : this.$modal.css({
                        width: this.modal.width + "px",
                        marginTop: windowHeight / 2 - height / 2 + "px"
                    }));
                },
                showOnMobile: function() {
                    this.$modal.css({
                        width: "96%",
                        marginTop: "2%"
                    });
                },
                resize: function() {
                    this.utils.isMobile() ? this.modal.showOnMobile() : this.modal.showOnDesktop();
                },
                setTitle: function(title) {
                    this.$modalHeader.html(title);
                },
                setContent: function() {
                    this.$modalBody.html(this.modal.getTemplate(this.modal.templateName));
                },
                setDraggable: function() {
                    "undefined" != typeof $.fn.draggable && (this.$modal.draggable({
                        handle: this.$modalHeader
                    }), this.$modalHeader.css("cursor", "move"));
                },
                setEnter: function(e) {
                    13 == e.which && (e.preventDefault(), this.$modal.find("button.redactor-modal-action-btn").click());
                },
                createCancelButton: function() {
                    var button = $("<button>").addClass("redactor-modal-btn redactor-modal-close-btn").html(this.lang.get("cancel"));
                    button.on("click", $.proxy(this.modal.close, this)), this.$modalFooter.append(button);
                },
                createDeleteButton: function(label) {
                    return this.modal.createButton(label, "delete");
                },
                createActionButton: function(label) {
                    return this.modal.createButton(label, "action");
                },
                createButton: function(label, className) {
                    var button = $("<button>").addClass("redactor-modal-btn").addClass("redactor-modal-" + className + "-btn").html(label);
                    return this.$modalFooter.append(button), button;
                },
                setButtonsWidth: function() {
                    var buttons = this.$modalFooter.find("button"), buttonsSize = buttons.length;
                    0 !== buttonsSize && buttons.css("width", 100 / buttonsSize + "%");
                },
                build: function() {
                    this.modal.buildOverlay(), this.$modalBox = $('<div id="redactor-modal-box"/>').hide(), 
                    this.$modal = $('<div id="redactor-modal" role="dialog" aria-labelledby="redactor-modal-header" />'), 
                    this.$modalHeader = $('<header id="redactor-modal-header"/>'), this.$modalClose = $('<button type="button" id="redactor-modal-close" tabindex="1" aria-label="Close" />').html("&times;"), 
                    this.$modalBody = $('<div id="redactor-modal-body" />'), this.$modalFooter = $("<footer />"), 
                    this.$modal.append(this.$modalHeader), this.$modal.append(this.$modalClose), this.$modal.append(this.$modalBody), 
                    this.$modal.append(this.$modalFooter), this.$modalBox.append(this.$modal), this.$modalBox.appendTo(document.body);
                },
                buildOverlay: function() {
                    this.$modalOverlay = $('<div id="redactor-modal-overlay">').hide(), $("body").prepend(this.$modalOverlay);
                },
                enableEvents: function() {
                    this.$modalClose.on("click.redactor-modal", $.proxy(this.modal.close, this)), $(document).on("keyup.redactor-modal", $.proxy(this.modal.closeHandler, this)), 
                    this.$editor.on("keyup.redactor-modal", $.proxy(this.modal.closeHandler, this)), 
                    this.$modalBox.on("click.redactor-modal", $.proxy(this.modal.close, this));
                },
                disableEvents: function() {
                    this.$modalClose.off("click.redactor-modal"), $(document).off("keyup.redactor-modal"), 
                    this.$editor.off("keyup.redactor-modal"), this.$modalBox.off("click.redactor-modal"), 
                    $(window).off("resize.redactor-modal");
                },
                closeHandler: function(e) {
                    e.which == this.keyCode.ESC && this.modal.close(!1);
                },
                close: function(e) {
                    if (e) {
                        if (!$(e.target).hasClass("redactor-modal-close-btn") && e.target != this.$modalClose[0] && e.target != this.$modalBox[0]) return;
                        e.preventDefault();
                    }
                    this.$modalBox && (this.modal.disableEvents(), this.utils.enableBodyScroll(), this.$modalOverlay.remove(), 
                    this.$modalBox.fadeOut("fast", $.proxy(function() {
                        this.$modalBox.remove(), setTimeout($.proxy(this.utils.restoreScroll, this), 0), 
                        void 0 !== e && this.selection.restore(), $(document.body).css("overflow", this.modal.bodyOveflow), 
                        this.core.setCallback("modalClosed", this.modal.templateName);
                    }, this)));
                }
            };
        },
        observe: function() {
            return {
                load: function() {
                    if ("undefined" == typeof this.opts.destroyed) {
                        if (this.utils.browser("msie")) {
                            var self = this;
                            this.$editor.find("pre, code").on("mouseover", function() {
                                self.$editor.attr("contenteditable", !1), $(this).attr("contenteditable", !0);
                            }).on("mouseout", function() {
                                self.$editor.attr("contenteditable", !0), $(this).removeAttr("contenteditable");
                            });
                        }
                        this.observe.images(), this.observe.links();
                    }
                },
                toolbar: function(e, btnName) {
                    this.observe.buttons(e, btnName), this.observe.dropdowns();
                },
                isCurrent: function($el, $current) {
                    if ("undefined" == typeof $current) var $current = $(this.selection.getCurrent());
                    return $current.is($el) || $current.parents($el).length > 0;
                },
                dropdowns: function() {
                    var $current = $(this.selection.getCurrent());
                    $.each(this.opts.observe.dropdowns, $.proxy(function(key, value) {
                        var observe = value.observe, element = observe.element, $item = value.item, inValues = "undefined" != typeof observe["in"] ? observe["in"] : !1, outValues = "undefined" != typeof observe.out ? observe.out : !1;
                        $current.closest(element).size() > 0 ? this.observe.setDropdownProperties($item, inValues, outValues) : this.observe.setDropdownProperties($item, outValues, inValues);
                    }, this));
                },
                setDropdownProperties: function($item, addProperties, deleteProperties) {
                    deleteProperties && "undefined" != typeof deleteProperties.attr && this.observe.setDropdownAttr($item, deleteProperties.attr, !0), 
                    "undefined" != typeof addProperties.attr && this.observe.setDropdownAttr($item, addProperties.attr), 
                    "undefined" != typeof addProperties.title && $item.text(addProperties.title);
                },
                setDropdownAttr: function($item, properties, isDelete) {
                    $.each(properties, function(key, value) {
                        "class" == key ? isDelete ? $item.removeClass(value) : $item.addClass(value) : isDelete ? $item.removeAttr(key) : $item.attr(key, value);
                    });
                },
                addDropdown: function($item, btnName, btnObject) {
                    "undefined" != typeof btnObject.observe && (btnObject.item = $item, this.opts.observe.dropdowns.push(btnObject));
                },
                buttons: function(e, btnName) {
                    var current = this.selection.getCurrent(), parent = this.selection.getParent();
                    if (e !== !1 ? this.button.setInactiveAll() : this.button.setInactiveAll(btnName), 
                    e === !1 && "html" !== btnName) return void (-1 != $.inArray(btnName, this.opts.activeButtons) && this.button.toggleActive(btnName));
                    $.each(this.opts.activeButtonsStates, $.proxy(function(key, value) {
                        var parentEl = $(parent).closest(key, this.$editor[0]), currentEl = $(current).closest(key, this.$editor[0]);
                        (0 === parentEl.length || this.utils.isRedactorParent(parentEl)) && this.utils.isRedactorParent(currentEl) && (0 !== parentEl.length || 0 !== currentEl.closest(key, this.$editor[0]).length) && this.button.setActive(value);
                    }, this));
                    var $parent = $(parent).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
                    if (this.utils.isRedactorParent(parent) && $parent.length) {
                        var align = "" === $parent.css("text-align") ? "left" : $parent.css("text-align");
                        this.button.setActive("align" + align);
                    }
                },
                addButton: function(tag, btnName) {
                    this.opts.activeButtons.push(btnName), this.opts.activeButtonsStates[tag] = btnName;
                },
                images: function() {
                    this.$editor.find("img").each($.proxy(function(i, img) {
                        var $img = $(img);
                        $img.closest("a", this.$editor[0]).on("click", function(e) {
                            e.preventDefault();
                        }), this.utils.browser("msie") && $img.attr("unselectable", "on"), this.image.setEditable($img);
                    }, this)), $(document).on("click.redactor-image-delete." + this.uuid, $.proxy(function(e) {
                        this.observe.image = !1, "IMG" == e.target.tagName && this.utils.isRedactorParent(e.target) && (this.observe.image = this.observe.image && this.observe.image == e.target ? !1 : e.target);
                    }, this));
                },
                links: function() {
                    this.opts.linkTooltip && (this.$editor.find("a").on("touchstart.redactor." + this.uuid + " click.redactor." + this.uuid, $.proxy(this.observe.showTooltip, this)), 
                    this.$editor.on("touchstart.redactor." + this.uuid + " click.redactor." + this.uuid, $.proxy(this.observe.closeTooltip, this)), 
                    $(document).on("touchstart.redactor." + this.uuid + " click.redactor." + this.uuid, $.proxy(this.observe.closeTooltip, this)));
                },
                getTooltipPosition: function($link) {
                    return $link.offset();
                },
                showTooltip: function(e) {
                    var $el = $(e.target);
                    if ("IMG" != $el[0].tagName && ("A" !== $el[0].tagName && ($el = $el.closest("a", this.$editor[0])), 
                    "A" === $el[0].tagName)) {
                        var $link = $el, pos = this.observe.getTooltipPosition($link), tooltip = $('<span class="redactor-link-tooltip"></span>'), href = $link.attr("href");
                        void 0 === href && (href = ""), href.length > 24 && (href = href.substring(0, 24) + "...");
                        var aLink = $('<a href="' + $link.attr("href") + '" target="_blank" />').html(href).addClass("redactor-link-tooltip-action"), aEdit = $('<a href="#" />').html(this.lang.get("edit")).on("click", $.proxy(this.link.show, this)).addClass("redactor-link-tooltip-action"), aUnlink = $('<a href="#" />').html(this.lang.get("unlink")).on("click", $.proxy(this.link.unlink, this)).addClass("redactor-link-tooltip-action");
                        tooltip.append(aLink).append(" | ").append(aEdit).append(" | ").append(aUnlink), 
                        tooltip.css({
                            top: pos.top + parseInt($link.css("line-height"), 10) + "px",
                            left: pos.left + "px"
                        }), $(".redactor-link-tooltip").remove(), $("body").append(tooltip);
                    }
                },
                closeTooltip: function(e) {
                    e = e.originalEvent || e;
                    var target = e.target, $parent = $(target).closest("a", this.$editor[0]);
                    (0 === $parent.length || "A" !== $parent[0].tagName || "A" === target.tagName) && ("A" === target.tagName && this.utils.isRedactorParent(target) || $(target).hasClass("redactor-link-tooltip-action") || $(".redactor-link-tooltip").remove());
                }
            };
        },
        paragraphize: function() {
            return {
                load: function(html) {
                    return this.opts.linebreaks ? html : "" === html || "<p></p>" === html ? this.opts.emptyHtml : (html += "\n", 
                    this.paragraphize.safes = [], this.paragraphize.z = 0, html = html.replace(/(<br\s?\/?>){1,}\n?<\/blockquote>/gi, "</blockquote>"), 
                    html = this.paragraphize.getSafes(html), html = this.paragraphize.getSafesComments(html), 
                    html = this.paragraphize.replaceBreaksToNewLines(html), html = this.paragraphize.replaceBreaksToParagraphs(html), 
                    html = this.paragraphize.clear(html), html = this.paragraphize.restoreSafes(html), 
                    html = html.replace(new RegExp("<br\\s?/?>\n?<(" + this.opts.paragraphizeBlocks.join("|") + ")(.*?[^>])>", "gi"), "<p><br /></p>\n<$1$2>"), 
                    $.trim(html));
                },
                getSafes: function(html) {
                    var $div = $("<div />").append(html);
                    return $div.find("blockquote p").replaceWith(function() {
                        return $(this).append("<br />").contents();
                    }), html = $div.html(), $div.find(this.opts.paragraphizeBlocks.join(", ")).each($.proxy(function(i, s) {
                        this.paragraphize.z++, this.paragraphize.safes[this.paragraphize.z] = s.outerHTML, 
                        html = html.replace(s.outerHTML, "\n{replace" + this.paragraphize.z + "}");
                    }, this)), html;
                },
                getSafesComments: function(html) {
                    var commentsMatches = html.match(/<!--([\w\W]*?)-->/gi);
                    return commentsMatches ? ($.each(commentsMatches, $.proxy(function(i, s) {
                        this.paragraphize.z++, this.paragraphize.safes[this.paragraphize.z] = s, html = html.replace(s, "\n{replace" + this.paragraphize.z + "}");
                    }, this)), html) : html;
                },
                restoreSafes: function(html) {
                    return $.each(this.paragraphize.safes, function(i, s) {
                        s = "undefined" != typeof s ? s.replace(/\$/g, "&#36;") : s, html = html.replace("{replace" + i + "}", s);
                    }), html;
                },
                replaceBreaksToParagraphs: function(html) {
                    var htmls = html.split(new RegExp("\n", "g"), -1);
                    if (html = "", htmls) for (var len = htmls.length, i = 0; len > i; i++) {
                        if (!htmls.hasOwnProperty(i)) return;
                        -1 == htmls[i].search("{replace") ? (htmls[i] = htmls[i].replace(/<p>\n\t?<\/p>/gi, ""), 
                        htmls[i] = htmls[i].replace(/<p><\/p>/gi, ""), "" !== htmls[i] && (html += "<p>" + htmls[i].replace(/^\n+|\n+$/g, "") + "</p>")) : html += htmls[i];
                    }
                    return html;
                },
                replaceBreaksToNewLines: function(html) {
                    return html = html.replace(/<br \/>\s*<br \/>/gi, "\n\n"), html = html.replace(/<br\s?\/?>\n?<br\s?\/?>/gi, "\n<br /><br />"), 
                    html = html.replace(new RegExp("\r\n", "g"), "\n"), html = html.replace(new RegExp("\r", "g"), "\n"), 
                    html = html.replace(new RegExp("/\n\n+/"), "g", "\n\n");
                },
                clear: function(html) {
                    return html = html.replace(new RegExp("</blockquote></p>", "gi"), "</blockquote>"), 
                    html = html.replace(new RegExp("<p></blockquote>", "gi"), "</blockquote>"), html = html.replace(new RegExp("<p><blockquote>", "gi"), "<blockquote>"), 
                    html = html.replace(new RegExp("<blockquote></p>", "gi"), "<blockquote>"), html = html.replace(new RegExp("<p><p ", "gi"), "<p "), 
                    html = html.replace(new RegExp("<p><p>", "gi"), "<p>"), html = html.replace(new RegExp("</p></p>", "gi"), "</p>"), 
                    html = html.replace(new RegExp("<p>\\s?</p>", "gi"), ""), html = html.replace(new RegExp("\n</p>", "gi"), "</p>"), 
                    html = html.replace(new RegExp("<p>	?	?\n?<p>", "gi"), "<p>"), html = html.replace(new RegExp("<p>	*</p>", "gi"), "");
                }
            };
        },
        paste: function() {
            return {
                init: function(e) {
                    return this.opts.cleanOnPaste ? (this.rtePaste = !0, this.buffer.set(), this.selection.save(), 
                    this.utils.saveScroll(), this.paste.createPasteBox(), $(window).on("scroll.redactor-freeze", $.proxy(function() {
                        $(window).scrollTop(this.saveBodyScroll);
                    }, this)), void setTimeout($.proxy(function() {
                        var html = this.$pasteBox.html();
                        this.$pasteBox.remove(), this.selection.restore(), this.utils.restoreScroll(), this.paste.insert(html), 
                        $(window).off("scroll.redactor-freeze"), this.linkify.isEnabled() && this.linkify.format();
                    }, this), 1)) : void setTimeout($.proxy(this.code.sync, this), 1);
                },
                createPasteBox: function() {
                    this.$pasteBox = $("<div>").html("").attr("contenteditable", "true").css({
                        position: "fixed",
                        width: 0,
                        top: 0,
                        left: "-9999px"
                    }), this.utils.browser("msie") ? this.$box.append(this.$pasteBox) : $(".modal-body").length > 0 ? $(".modal.in .modal-body").append(this.$pasteBox) : $("body").append(this.$pasteBox), 
                    this.$pasteBox.focus();
                },
                insert: function(html) {
                    html = this.core.setCallback("pasteBefore", html), html = this.utils.isSelectAll() ? this.clean.onPaste(html, !1) : this.clean.onPaste(html), 
                    html = this.core.setCallback("paste", html), this.utils.isSelectAll() ? this.insert.set(html, !1) : this.insert.html(html, !1), 
                    this.utils.disableSelectAll(), this.rtePaste = !1, setTimeout($.proxy(this.clean.clearUnverified, this), 10), 
                    setTimeout($.proxy(function() {
                        var spans = this.$editor.find("span");
                        $.each(spans, function(i, s) {
                            var html = s.innerHTML.replace(/\u200B/, "");
                            "" === html && 0 === s.attributes.length && $(s).remove();
                        });
                    }, this), 10);
                }
            };
        },
        placeholder: function() {
            return {
                enable: function() {
                    this.placeholder.is() && (this.$editor.attr("placeholder", this.$element.attr("placeholder")), 
                    this.placeholder.toggle(), this.$editor.on("keydown.redactor-placeholder", $.proxy(this.placeholder.toggle, this)));
                },
                toggle: function() {
                    setTimeout($.proxy(function() {
                        var func = this.utils.isEmpty(this.$editor.html(), !1) ? "addClass" : "removeClass";
                        this.$editor[func]("redactor-placeholder");
                    }, this), 5);
                },
                remove: function() {
                    this.$editor.removeClass("redactor-placeholder");
                },
                is: function() {
                    return this.opts.placeholder ? this.$element.attr("placeholder", this.opts.placeholder) : !("undefined" == typeof this.$element.attr("placeholder") || "" === this.$element.attr("placeholder"));
                }
            };
        },
        progress: function() {
            return {
                show: function() {
                    $(document.body).append($('<div id="redactor-progress"><span></span></div>')), $("#redactor-progress").fadeIn();
                },
                hide: function() {
                    $("#redactor-progress").fadeOut(1500, function() {
                        $(this).remove();
                    });
                }
            };
        },
        selection: function() {
            return {
                get: function() {
                    this.sel = document.getSelection(), document.getSelection && this.sel.getRangeAt && this.sel.rangeCount ? this.range = this.sel.getRangeAt(0) : this.range = document.createRange();
                },
                addRange: function() {
                    try {
                        this.sel.removeAllRanges();
                    } catch (e) {}
                    this.sel.addRange(this.range);
                },
                getCurrent: function() {
                    var el = !1;
                    return this.selection.get(), this.sel && this.sel.rangeCount > 0 && (el = this.sel.getRangeAt(0).startContainer), 
                    this.utils.isRedactorParent(el);
                },
                getParent: function(elem) {
                    return elem = elem || this.selection.getCurrent(), elem ? this.utils.isRedactorParent($(elem).parent()[0]) : !1;
                },
                getPrev: function() {
                    return window.getSelection().anchorNode.previousSibling;
                },
                getNext: function() {
                    return window.getSelection().anchorNode.nextSibling;
                },
                getBlock: function(node) {
                    for (node = node || this.selection.getCurrent(); node; ) {
                        if (this.utils.isBlockTag(node.tagName)) return $(node).hasClass("redactor-editor") ? !1 : node;
                        node = node.parentNode;
                    }
                    return !1;
                },
                getInlines: function(nodes, tags) {
                    if (this.selection.get(), this.range && this.range.collapsed) return !1;
                    var inlines = [];
                    nodes = "undefined" == typeof nodes || nodes === !1 ? this.selection.getNodes() : nodes;
                    var inlineTags = this.opts.inlineTags;
                    if (inlineTags.push("span"), "undefined" != typeof tags) for (var i = 0; i < tags.length; i++) inlineTags.push(tags[i]);
                    return $.each(nodes, $.proxy(function(i, node) {
                        -1 != $.inArray(node.tagName.toLowerCase(), inlineTags) && inlines.push(node);
                    }, this)), 0 === inlines.length ? !1 : inlines;
                },
                getInlinesTags: function(tags) {
                    if (this.selection.get(), this.range && this.range.collapsed) return !1;
                    var inlines = [], nodes = this.selection.getNodes();
                    return $.each(nodes, $.proxy(function(i, node) {
                        -1 != $.inArray(node.tagName.toLowerCase(), tags) && inlines.push(node);
                    }, this)), 0 === inlines.length ? !1 : inlines;
                },
                getBlocks: function(nodes) {
                    if (this.selection.get(), this.range && this.range.collapsed) return [ this.selection.getBlock() ];
                    var blocks = [];
                    return nodes = "undefined" == typeof nodes ? this.selection.getNodes() : nodes, 
                    $.each(nodes, $.proxy(function(i, node) {
                        this.utils.isBlock(node) && (this.selection.lastBlock = node, blocks.push(node));
                    }, this)), 0 === blocks.length ? [ this.selection.getBlock() ] : blocks;
                },
                getLastBlock: function() {
                    return this.selection.lastBlock;
                },
                getNodes: function() {
                    this.selection.get();
                    var startNode = this.selection.getNodesMarker(1), endNode = this.selection.getNodesMarker(2);
                    if (this.range.collapsed === !1) {
                        if (window.getSelection) {
                            var sel = window.getSelection();
                            if (sel.rangeCount > 0) {
                                var range = sel.getRangeAt(0), startPointNode = range.startContainer, startOffset = range.startOffset, boundaryRange = range.cloneRange();
                                boundaryRange.collapse(!1), boundaryRange.insertNode(endNode), boundaryRange.setStart(startPointNode, startOffset), 
                                boundaryRange.collapse(!0), boundaryRange.insertNode(startNode), range.setStartAfter(startNode), 
                                range.setEndBefore(endNode), sel.removeAllRanges(), sel.addRange(range);
                            }
                        }
                    } else this.selection.setNodesMarker(this.range, startNode, !0), endNode = startNode;
                    var nodes = [], counter = 0, self = this;
                    this.$editor.find("*").each(function() {
                        if (this == startNode) {
                            var parent = $(this).parent();
                            0 !== parent.length && "BODY" != parent[0].tagName && self.utils.isRedactorParent(parent[0]) && nodes.push(parent[0]), 
                            nodes.push(this), counter = 1;
                        } else counter > 0 && (nodes.push(this), counter += 1);
                        return this == endNode ? !1 : void 0;
                    });
                    for (var finalNodes = [], len = nodes.length, i = 0; len > i; i++) "nodes-marker-1" != nodes[i].id && "nodes-marker-2" != nodes[i].id && finalNodes.push(nodes[i]);
                    return this.selection.removeNodesMarkers(), finalNodes;
                },
                getNodesMarker: function(num) {
                    return $('<span id="nodes-marker-' + num + '" class="redactor-nodes-marker" data-verified="redactor">' + this.opts.invisibleSpace + "</span>")[0];
                },
                setNodesMarker: function(range, node, type) {
                    var range = range.cloneRange();
                    try {
                        range.collapse(type), range.insertNode(node);
                    } catch (e) {}
                },
                removeNodesMarkers: function() {
                    $(document).find("span.redactor-nodes-marker").remove(), this.$editor.find("span.redactor-nodes-marker").remove();
                },
                fromPoint: function(start, end) {
                    this.caret.setOffset(start, end);
                },
                wrap: function(tag) {
                    if (this.selection.get(), this.range.collapsed) return !1;
                    var wrapper = document.createElement(tag);
                    return wrapper.appendChild(this.range.extractContents()), this.range.insertNode(wrapper), 
                    wrapper;
                },
                selectElement: function(node) {
                    if (this.utils.browser("mozilla")) {
                        node = node[0] || node;
                        var range = document.createRange();
                        range.selectNodeContents(node);
                    } else this.caret.set(node, 0, node, 1);
                },
                selectAll: function() {
                    this.selection.get(), this.range.selectNodeContents(this.$editor[0]), this.selection.addRange();
                },
                remove: function() {
                    this.selection.get(), this.sel.removeAllRanges();
                },
                save: function() {
                    this.selection.createMarkers();
                },
                createMarkers: function() {
                    this.selection.get();
                    var node1 = this.selection.getMarker(1);
                    if (this.selection.setMarker(this.range, node1, !0), this.range.collapsed === !1) {
                        var node2 = this.selection.getMarker(2);
                        this.selection.setMarker(this.range, node2, !1);
                    }
                    this.savedSel = this.$editor.html();
                },
                getMarker: function(num) {
                    return "undefined" == typeof num && (num = 1), $('<span id="selection-marker-' + num + '" class="redactor-selection-marker"  data-verified="redactor">' + this.opts.invisibleSpace + "</span>")[0];
                },
                getMarkerAsHtml: function(num) {
                    return this.utils.getOuterHtml(this.selection.getMarker(num));
                },
                setMarker: function(range, node, type) {
                    range = range.cloneRange();
                    try {
                        range.collapse(type), range.insertNode(node);
                    } catch (e) {
                        this.focus.setStart();
                    }
                },
                restore: function() {
                    var node1 = this.$editor.find("span#selection-marker-1"), node2 = this.$editor.find("span#selection-marker-2");
                    this.utils.browser("mozilla") && this.$editor.focus(), 0 !== node1.length && 0 !== node2.length ? this.caret.set(node1, 0, node2, 0) : 0 !== node1.length ? this.caret.set(node1, 0, node1, 0) : this.$editor.focus(), 
                    this.selection.removeMarkers(), this.savedSel = !1;
                },
                removeMarkers: function() {
                    this.$editor.find("span.redactor-selection-marker").each(function(i, s) {
                        var text = $(s).text().replace(/\u200B/g, "");
                        "" === text ? $(s).remove() : $(s).replaceWith(function() {
                            return $(this).contents();
                        });
                    });
                },
                getText: function() {
                    return this.selection.get(), this.sel.toString();
                },
                getHtml: function() {
                    var html = "";
                    if (this.selection.get(), this.sel.rangeCount) {
                        for (var container = document.createElement("div"), len = this.sel.rangeCount, i = 0; len > i; ++i) container.appendChild(this.sel.getRangeAt(i).cloneContents());
                        html = container.innerHTML;
                    }
                    return this.clean.onSync(html);
                },
                replaceSelection: function(html) {
                    this.selection.get(), this.range.deleteContents();
                    var div = document.createElement("div");
                    div.innerHTML = html;
                    for (var child, frag = document.createDocumentFragment(); child = div.firstChild; ) frag.appendChild(child);
                    this.range.insertNode(frag);
                },
                replaceWithHtml: function(html) {
                    html = this.selection.getMarkerAsHtml(1) + html + this.selection.getMarkerAsHtml(2), 
                    this.selection.get(), window.getSelection && window.getSelection().getRangeAt ? this.selection.replaceSelection(html) : document.selection && document.selection.createRange && this.range.pasteHTML(html), 
                    this.selection.restore(), this.code.sync();
                }
            };
        },
        shortcuts: function() {
            return {
                init: function(e, key) {
                    return this.opts.shortcuts ? void $.each(this.opts.shortcuts, $.proxy(function(str, command) {
                        for (var keys = str.split(","), len = keys.length, i = 0; len > i; i++) "string" == typeof keys[i] && this.shortcuts.handler(e, $.trim(keys[i]), $.proxy(function() {
                            var func;
                            "-1" != command.func.search(/\./) ? (func = command.func.split("."), "undefined" != typeof this[func[0]] && this[func[0]][func[1]].apply(this, command.params)) : this[command.func].apply(this, command.params);
                        }, this));
                    }, this)) : (!e.ctrlKey && !e.metaKey || 66 !== key && 73 !== key || e.preventDefault(), 
                    !1);
                },
                handler: function(e, keys, origHandler) {
                    var hotkeysSpecialKeys = {
                        8: "backspace",
                        9: "tab",
                        10: "return",
                        13: "return",
                        16: "shift",
                        17: "ctrl",
                        18: "alt",
                        19: "pause",
                        20: "capslock",
                        27: "esc",
                        32: "space",
                        33: "pageup",
                        34: "pagedown",
                        35: "end",
                        36: "home",
                        37: "left",
                        38: "up",
                        39: "right",
                        40: "down",
                        45: "insert",
                        46: "del",
                        59: ";",
                        61: "=",
                        96: "0",
                        97: "1",
                        98: "2",
                        99: "3",
                        100: "4",
                        101: "5",
                        102: "6",
                        103: "7",
                        104: "8",
                        105: "9",
                        106: "*",
                        107: "+",
                        109: "-",
                        110: ".",
                        111: "/",
                        112: "f1",
                        113: "f2",
                        114: "f3",
                        115: "f4",
                        116: "f5",
                        117: "f6",
                        118: "f7",
                        119: "f8",
                        120: "f9",
                        121: "f10",
                        122: "f11",
                        123: "f12",
                        144: "numlock",
                        145: "scroll",
                        173: "-",
                        186: ";",
                        187: "=",
                        188: ",",
                        189: "-",
                        190: ".",
                        191: "/",
                        192: "`",
                        219: "[",
                        220: "\\",
                        221: "]",
                        222: "'"
                    }, hotkeysShiftNums = {
                        "`": "~",
                        "1": "!",
                        "2": "@",
                        "3": "#",
                        "4": "$",
                        "5": "%",
                        "6": "^",
                        "7": "&",
                        "8": "*",
                        "9": "(",
                        "0": ")",
                        "-": "_",
                        "=": "+",
                        ";": ": ",
                        "'": '"',
                        ",": "<",
                        ".": ">",
                        "/": "?",
                        "\\": "|"
                    };
                    keys = keys.toLowerCase().split(" ");
                    var special = hotkeysSpecialKeys[e.keyCode], character = String.fromCharCode(e.which).toLowerCase(), modif = "", possible = {};
                    $.each([ "alt", "ctrl", "meta", "shift" ], function(index, specialKey) {
                        e[specialKey + "Key"] && special !== specialKey && (modif += specialKey + "+");
                    }), special && (possible[modif + special] = !0), character && (possible[modif + character] = !0, 
                    possible[modif + hotkeysShiftNums[character]] = !0, "shift+" === modif && (possible[hotkeysShiftNums[character]] = !0));
                    for (var i = 0, len = keys.length; len > i; i++) if (possible[keys[i]]) return e.preventDefault(), 
                    origHandler.apply(this, arguments);
                }
            };
        },
        tabifier: function() {
            return {
                get: function(code) {
                    if (!this.opts.tabifier) return code;
                    var ownLine = [ "area", "body", "head", "hr", "i?frame", "link", "meta", "noscript", "style", "script", "table", "tbody", "thead", "tfoot" ], contOwnLine = [ "li", "dt", "dt", "h[1-6]", "option", "script" ], newLevel = [ "p", "blockquote", "div", "dl", "fieldset", "form", "frameset", "map", "ol", "pre", "select", "td", "th", "tr", "ul" ];
                    this.tabifier.lineBefore = new RegExp("^<(/?" + ownLine.join("|/?") + "|" + contOwnLine.join("|") + ")[ >]"), 
                    this.tabifier.lineAfter = new RegExp("^<(br|/?" + ownLine.join("|/?") + "|/" + contOwnLine.join("|/") + ")[ >]"), 
                    this.tabifier.newLevel = new RegExp("^</?(" + newLevel.join("|") + ")[ >]");
                    var i = 0, codeLength = code.length, point = 0, start = null, end = null, tag = "", out = "", cont = "";
                    for (this.tabifier.cleanlevel = 0; codeLength > i; i++) {
                        if (point = i, -1 == code.substr(i).indexOf("<")) return out += code.substr(i), 
                        this.tabifier.finish(out);
                        for (;codeLength > point && "<" != code.charAt(point); ) point++;
                        for (i != point && (cont = code.substr(i, point - i), cont.match(/^\s{2,}$/g) || ("\n" == out.charAt(out.length - 1) ? out += this.tabifier.getTabs() : "\n" == cont.charAt(0) && (out += "\n" + this.tabifier.getTabs(), 
                        cont = cont.replace(/^\s+/, "")), out += cont), cont.match(/\n/) && (out += "\n" + this.tabifier.getTabs())), 
                        start = point; codeLength > point && ">" != code.charAt(point); ) point++;
                        tag = code.substr(start, point - start), i = point;
                        var t;
                        if ("!--" == tag.substr(1, 3)) {
                            if (!tag.match(/--$/)) {
                                for (;"-->" != code.substr(point, 3); ) point++;
                                point += 2, tag = code.substr(start, point - start), i = point;
                            }
                            "\n" != out.charAt(out.length - 1) && (out += "\n"), out += this.tabifier.getTabs(), 
                            out += tag + ">\n";
                        } else "!" == tag[1] ? out = this.tabifier.placeTag(tag + ">", out) : "?" == tag[1] ? out += tag + ">\n" : (t = tag.match(/^<(script|style|pre)/i)) ? (t[1] = t[1].toLowerCase(), 
                        tag = this.tabifier.cleanTag(tag), out = this.tabifier.placeTag(tag, out), end = String(code.substr(i + 1)).toLowerCase().indexOf("</" + t[1]), 
                        end && (cont = code.substr(i + 1, end), i += end, out += cont)) : (tag = this.tabifier.cleanTag(tag), 
                        out = this.tabifier.placeTag(tag, out));
                    }
                    return this.tabifier.finish(out);
                },
                getTabs: function() {
                    for (var s = "", j = 0; j < this.tabifier.cleanlevel; j++) s += "	";
                    return s;
                },
                finish: function(code) {
                    return code = code.replace(/\n\s*\n/g, "\n"), code = code.replace(/^[\s\n]*/, ""), 
                    code = code.replace(/[\s\n]*$/, ""), code = code.replace(/<script(.*?)>\n<\/script>/gi, "<script$1></script>"), 
                    this.tabifier.cleanlevel = 0, code;
                },
                cleanTag: function(tag) {
                    var tagout = "";
                    tag = tag.replace(/\n/g, " "), tag = tag.replace(/\s{2,}/g, " "), tag = tag.replace(/^\s+|\s+$/g, " ");
                    var suffix = "";
                    tag.match(/\/$/) && (suffix = "/", tag = tag.replace(/\/+$/, ""));
                    for (var m; m = /\s*([^= ]+)(?:=((['"']).*?\3|[^ ]+))?/.exec(tag); ) m[2] ? tagout += m[1].toLowerCase() + "=" + m[2] : m[1] && (tagout += m[1].toLowerCase()), 
                    tagout += " ", tag = tag.substr(m[0].length);
                    return tagout.replace(/\s*$/, "") + suffix + ">";
                },
                placeTag: function(tag, out) {
                    var nl = tag.match(this.tabifier.newLevel);
                    return (tag.match(this.tabifier.lineBefore) || nl) && (out = out.replace(/\s*$/, ""), 
                    out += "\n"), nl && "/" == tag.charAt(1) && this.tabifier.cleanlevel--, "\n" == out.charAt(out.length - 1) && (out += this.tabifier.getTabs()), 
                    nl && "/" != tag.charAt(1) && this.tabifier.cleanlevel++, out += tag, (tag.match(this.tabifier.lineAfter) || tag.match(this.tabifier.newLevel)) && (out = out.replace(/ *$/, "")), 
                    out;
                }
            };
        },
        tidy: function() {
            return {
                setupAllowed: function() {
                    var index = $.inArray("span", this.opts.removeEmpty);
                    if (-1 !== index && this.opts.removeEmpty.splice(index, 1), this.opts.allowedTags && (this.opts.deniedTags = !1), 
                    this.opts.allowedAttr && (this.opts.removeAttr = !1), !this.opts.linebreaks) {
                        var tags = [ "p", "section" ];
                        this.opts.allowedTags && this.tidy.addToAllowed(tags), this.opts.deniedTags && this.tidy.removeFromDenied(tags);
                    }
                },
                addToAllowed: function(tags) {
                    for (var len = tags.length, i = 0; len > i; i++) -1 == $.inArray(tags[i], this.opts.allowedTags) && this.opts.allowedTags.push(tags[i]);
                },
                removeFromDenied: function(tags) {
                    for (var len = tags.length, i = 0; len > i; i++) {
                        var pos = $.inArray(tags[i], this.opts.deniedTags);
                        -1 != pos && this.opts.deniedTags.splice(pos, 1);
                    }
                },
                load: function(html, options) {
                    return this.tidy.settings = {
                        deniedTags: this.opts.deniedTags,
                        allowedTags: this.opts.allowedTags,
                        removeComments: this.opts.removeComments,
                        replaceTags: this.opts.replaceTags,
                        replaceStyles: this.opts.replaceStyles,
                        removeDataAttr: this.opts.removeDataAttr,
                        removeAttr: this.opts.removeAttr,
                        allowedAttr: this.opts.allowedAttr,
                        removeWithoutAttr: this.opts.removeWithoutAttr,
                        removeEmpty: this.opts.removeEmpty
                    }, $.extend(this.tidy.settings, options), html = this.tidy.removeComments(html), 
                    this.tidy.$div = $("<div />").append(html), this.tidy.replaceTags(), this.tidy.replaceStyles(), 
                    this.tidy.removeTags(), this.tidy.removeAttr(), this.tidy.removeEmpty(), this.tidy.removeParagraphsInLists(), 
                    this.tidy.removeDataAttr(), this.tidy.removeWithoutAttr(), html = this.tidy.$div.html(), 
                    this.tidy.$div.remove(), html;
                },
                removeComments: function(html) {
                    return this.tidy.settings.removeComments ? html.replace(/<!--[\s\S]*?-->/gi, "") : html;
                },
                replaceTags: function(html) {
                    if (!this.tidy.settings.replaceTags) return html;
                    for (var len = this.tidy.settings.replaceTags.length, replacement = [], rTags = [], i = 0; len > i; i++) rTags.push(this.tidy.settings.replaceTags[i][1]), 
                    replacement.push(this.tidy.settings.replaceTags[i][0]);
                    $.each(replacement, $.proxy(function(key, value) {
                        this.tidy.$div.find(value).replaceWith(function() {
                            return $("<" + rTags[key] + " />", {
                                html: $(this).html()
                            });
                        });
                    }, this));
                },
                replaceStyles: function() {
                    if (this.tidy.settings.replaceStyles) {
                        var len = this.tidy.settings.replaceStyles.length;
                        this.tidy.$div.find("span").each($.proxy(function(n, s) {
                            for (var $el = $(s), style = $el.attr("style"), i = 0; len > i; i++) if (style && style.match(new RegExp("^" + this.tidy.settings.replaceStyles[i][0], "i"))) {
                                var tagName = this.tidy.settings.replaceStyles[i][1];
                                $el.replaceWith(function() {
                                    var tag = document.createElement(tagName);
                                    return $(tag).append($(this).contents());
                                });
                            }
                        }, this));
                    }
                },
                removeTags: function() {
                    !this.tidy.settings.deniedTags && this.tidy.settings.allowedTags && this.tidy.$div.find("*").not(this.tidy.settings.allowedTags.join(",")).each(function(i, s) {
                        "" === s.innerHTML ? $(s).remove() : $(s).contents().unwrap();
                    }), this.tidy.settings.deniedTags && this.tidy.$div.find(this.tidy.settings.deniedTags.join(",")).each(function(i, s) {
                        $(s).hasClass("redactor-script-tag") || $(s).hasClass("redactor-selection-marker") || ("" === s.innerHTML ? $(s).remove() : $(s).contents().unwrap());
                    });
                },
                removeAttr: function() {
                    var len;
                    if (!this.tidy.settings.removeAttr && this.tidy.settings.allowedAttr) {
                        var allowedAttrTags = [], allowedAttrData = [];
                        len = this.tidy.settings.allowedAttr.length;
                        for (var i = 0; len > i; i++) allowedAttrTags.push(this.tidy.settings.allowedAttr[i][0]), 
                        allowedAttrData.push(this.tidy.settings.allowedAttr[i][1]);
                        this.tidy.$div.find("*").each($.proxy(function(n, s) {
                            var $el = $(s), pos = $.inArray($el[0].tagName.toLowerCase(), allowedAttrTags), attributesRemove = this.tidy.removeAttrGetRemoves(pos, allowedAttrData, $el);
                            attributesRemove && $.each(attributesRemove, function(z, f) {
                                $el.removeAttr(f);
                            });
                        }, this));
                    }
                    if (this.tidy.settings.removeAttr) {
                        len = this.tidy.settings.removeAttr.length;
                        for (var i = 0; len > i; i++) {
                            var attrs = this.tidy.settings.removeAttr[i][1];
                            $.isArray(attrs) && (attrs = attrs.join(" ")), this.tidy.$div.find(this.tidy.settings.removeAttr[i][0]).removeAttr(attrs);
                        }
                    }
                },
                removeAttrGetRemoves: function(pos, allowed, $el) {
                    var attributesRemove = [];
                    return -1 == pos ? $.each($el[0].attributes, function(i, item) {
                        attributesRemove.push(item.name);
                    }) : "*" == allowed[pos] ? attributesRemove = [] : $.each($el[0].attributes, function(i, item) {
                        $.isArray(allowed[pos]) ? -1 == $.inArray(item.name, allowed[pos]) && attributesRemove.push(item.name) : allowed[pos] != item.name && attributesRemove.push(item.name);
                    }), attributesRemove;
                },
                removeAttrs: function(el, regex) {
                    return regex = new RegExp(regex, "g"), el.each(function() {
                        for (var self = $(this), len = this.attributes.length - 1, i = len; i >= 0; i--) {
                            var item = this.attributes[i];
                            item && item.specified && item.name.search(regex) >= 0 && self.removeAttr(item.name);
                        }
                    });
                },
                removeEmpty: function() {
                    this.tidy.settings.removeEmpty && this.tidy.$div.find(this.tidy.settings.removeEmpty.join(",")).each(function() {
                        var $el = $(this), text = $el.text();
                        text = text.replace(/\u200B/g, ""), text = text.replace(/&nbsp;/gi, ""), text = text.replace(/\s/g, ""), 
                        "" === text && 0 === $el.children().length && $el.remove();
                    });
                },
                removeParagraphsInLists: function() {
                    this.tidy.$div.find("li p").contents().unwrap();
                },
                removeDataAttr: function() {
                    if (this.tidy.settings.removeDataAttr) {
                        var tags = this.tidy.settings.removeDataAttr;
                        $.isArray(this.tidy.settings.removeDataAttr) && (tags = this.tidy.settings.removeDataAttr.join(",")), 
                        this.tidy.removeAttrs(this.tidy.$div.find(tags), "^(data-)");
                    }
                },
                removeWithoutAttr: function() {
                    this.tidy.settings.removeWithoutAttr && this.tidy.$div.find(this.tidy.settings.removeWithoutAttr.join(",")).each(function() {
                        0 === this.attributes.length && $(this).contents().unwrap();
                    });
                }
            };
        },
        toolbar: function() {
            return {
                init: function() {
                    return {
                        html: {
                            title: this.lang.get("html"),
                            func: "code.toggle"
                        },
                        formatting: {
                            title: this.lang.get("formatting"),
                            dropdown: {
                                p: {
                                    title: this.lang.get("paragraph"),
                                    func: "block.format"
                                },
                                blockquote: {
                                    title: this.lang.get("quote"),
                                    func: "block.format"
                                },
                                pre: {
                                    title: this.lang.get("code"),
                                    func: "block.format"
                                },
                                h1: {
                                    title: this.lang.get("header1"),
                                    func: "block.format"
                                },
                                h2: {
                                    title: this.lang.get("header2"),
                                    func: "block.format"
                                },
                                h3: {
                                    title: this.lang.get("header3"),
                                    func: "block.format"
                                },
                                h4: {
                                    title: this.lang.get("header4"),
                                    func: "block.format"
                                },
                                h5: {
                                    title: this.lang.get("header5"),
                                    func: "block.format"
                                }
                            }
                        },
                        bold: {
                            title: this.lang.get("bold"),
                            func: "inline.format"
                        },
                        italic: {
                            title: this.lang.get("italic"),
                            func: "inline.format"
                        },
                        deleted: {
                            title: this.lang.get("deleted"),
                            func: "inline.format"
                        },
                        underline: {
                            title: this.lang.get("underline"),
                            func: "inline.format"
                        },
                        unorderedlist: {
                            title: "&bull; " + this.lang.get("unorderedlist"),
                            func: "list.toggle"
                        },
                        orderedlist: {
                            title: "1. " + this.lang.get("orderedlist"),
                            func: "list.toggle"
                        },
                        outdent: {
                            title: "< " + this.lang.get("outdent"),
                            func: "indent.decrease"
                        },
                        indent: {
                            title: "> " + this.lang.get("indent"),
                            func: "indent.increase"
                        },
                        image: {
                            title: this.lang.get("image"),
                            func: "image.show"
                        },
                        file: {
                            title: this.lang.get("file"),
                            func: "file.show"
                        },
                        link: {
                            title: this.lang.get("link"),
                            dropdown: {
                                link: {
                                    title: this.lang.get("link_insert"),
                                    func: "link.show",
                                    observe: {
                                        element: "a",
                                        "in": {
                                            title: this.lang.get("link_edit")
                                        },
                                        out: {
                                            title: this.lang.get("link_insert")
                                        }
                                    }
                                },
                                unlink: {
                                    title: this.lang.get("unlink"),
                                    func: "link.unlink",
                                    observe: {
                                        element: "a",
                                        out: {
                                            attr: {
                                                "class": "redactor-dropdown-link-inactive",
                                                "aria-disabled": !0
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        alignment: {
                            title: this.lang.get("alignment"),
                            dropdown: {
                                left: {
                                    title: this.lang.get("align_left"),
                                    func: "alignment.left"
                                },
                                center: {
                                    title: this.lang.get("align_center"),
                                    func: "alignment.center"
                                },
                                right: {
                                    title: this.lang.get("align_right"),
                                    func: "alignment.right"
                                },
                                justify: {
                                    title: this.lang.get("align_justify"),
                                    func: "alignment.justify"
                                }
                            }
                        },
                        horizontalrule: {
                            title: this.lang.get("horizontalrule"),
                            func: "line.insert"
                        }
                    };
                },
                build: function() {
                    this.toolbar.hideButtons(), this.toolbar.hideButtonsOnMobile(), this.toolbar.isButtonSourceNeeded(), 
                    0 !== this.opts.buttons.length && (this.$toolbar = this.toolbar.createContainer(), 
                    this.toolbar.setOverflow(), this.toolbar.append(), this.toolbar.setFormattingTags(), 
                    this.toolbar.loadButtons(), this.toolbar.setFixed(), this.opts.activeButtons && this.$editor.on("mouseup.redactor keyup.redactor focus.redactor", $.proxy(this.observe.toolbar, this)));
                },
                createContainer: function() {
                    return $("<ul>").addClass("redactor-toolbar").attr({
                        id: "redactor-toolbar-" + this.uuid,
                        role: "toolbar"
                    });
                },
                setFormattingTags: function() {
                    $.each(this.opts.toolbar.formatting.dropdown, $.proxy(function(i, s) {
                        -1 == $.inArray(i, this.opts.formatting) && delete this.opts.toolbar.formatting.dropdown[i];
                    }, this));
                },
                loadButtons: function() {
                    $.each(this.opts.buttons, $.proxy(function(i, btnName) {
                        if (this.opts.toolbar[btnName]) {
                            if ("file" === btnName) {
                                if (this.opts.fileUpload === !1) return;
                                if (!this.opts.fileUpload && this.opts.s3 === !1) return;
                            }
                            if ("image" === btnName) {
                                if (this.opts.imageUpload === !1) return;
                                if (!this.opts.imageUpload && this.opts.s3 === !1) return;
                            }
                            var btnObject = this.opts.toolbar[btnName];
                            this.$toolbar.append($("<li>").append(this.button.build(btnName, btnObject)));
                        }
                    }, this));
                },
                append: function() {
                    this.opts.toolbarExternal ? (this.$toolbar.addClass("redactor-toolbar-external"), 
                    $(this.opts.toolbarExternal).html(this.$toolbar)) : this.$box.prepend(this.$toolbar);
                },
                setFixed: function() {
                    this.utils.isDesktop() && (this.opts.toolbarExternal || this.opts.toolbarFixed && (this.toolbar.observeScroll(), 
                    $(this.opts.toolbarFixedTarget).on("scroll.redactor." + this.uuid, $.proxy(this.toolbar.observeScroll, this))));
                },
                setOverflow: function() {
                    this.utils.isMobile() && this.opts.toolbarOverflow && this.$toolbar.addClass("redactor-toolbar-overflow");
                },
                isButtonSourceNeeded: function() {
                    if (!this.opts.source) {
                        var index = this.opts.buttons.indexOf("html");
                        -1 !== index && this.opts.buttons.splice(index, 1);
                    }
                },
                hideButtons: function() {
                    0 !== this.opts.buttonsHide.length && $.each(this.opts.buttonsHide, $.proxy(function(i, s) {
                        var index = this.opts.buttons.indexOf(s);
                        this.opts.buttons.splice(index, 1);
                    }, this));
                },
                hideButtonsOnMobile: function() {
                    this.utils.isMobile() && 0 !== this.opts.buttonsHideOnMobile.length && $.each(this.opts.buttonsHideOnMobile, $.proxy(function(i, s) {
                        var index = this.opts.buttons.indexOf(s);
                        this.opts.buttons.splice(index, 1);
                    }, this));
                },
                observeScroll: function() {
                    var scrollTop = $(this.opts.toolbarFixedTarget).scrollTop(), boxTop = 1;
                    this.opts.toolbarFixedTarget === document && (boxTop = this.$box.offset().top), 
                    scrollTop + this.opts.toolbarFixedTopOffset > boxTop ? this.toolbar.observeScrollEnable(scrollTop, boxTop) : this.toolbar.observeScrollDisable();
                },
                observeScrollEnable: function(scrollTop, boxTop) {
                    var top = this.opts.toolbarFixedTopOffset + scrollTop - boxTop, left = 0, end = boxTop + this.$box.height() - 32, width = this.$box.innerWidth();
                    this.$toolbar.addClass("toolbar-fixed-box"), this.$toolbar.css({
                        position: "absolute",
                        width: width,
                        top: top + "px",
                        left: left
                    }), scrollTop > end && $(".redactor-dropdown-" + this.uuid + ":visible").hide(), 
                    this.toolbar.setDropdownsFixed(), this.$toolbar.css("visibility", end > scrollTop ? "visible" : "hidden");
                },
                observeScrollDisable: function() {
                    this.$toolbar.css({
                        position: "relative",
                        width: "auto",
                        top: 0,
                        left: 0,
                        visibility: "visible"
                    }), this.toolbar.unsetDropdownsFixed(), this.$toolbar.removeClass("toolbar-fixed-box");
                },
                setDropdownsFixed: function() {
                    var top = this.$toolbar.innerHeight() + this.opts.toolbarFixedTopOffset, position = "fixed";
                    this.opts.toolbarFixedTarget !== document && (top = this.$toolbar.innerHeight() + this.$toolbar.offset().top + this.opts.toolbarFixedTopOffset, 
                    position = "absolute"), $(".redactor-dropdown-" + this.uuid).each(function() {
                        $(this).css({
                            position: position,
                            top: top + "px"
                        });
                    });
                },
                unsetDropdownsFixed: function() {
                    var top = this.$toolbar.innerHeight() + this.$toolbar.offset().top;
                    $(".redactor-dropdown-" + this.uuid).each(function() {
                        $(this).css({
                            position: "absolute",
                            top: top + "px"
                        });
                    });
                }
            };
        },
        upload: function() {
            return {
                init: function(id, url, callback) {
                    this.upload.direct = !1, this.upload.callback = callback, this.upload.url = url, 
                    this.upload.$el = $(id), this.upload.$droparea = $('<div id="redactor-droparea" />'), 
                    this.upload.$placeholdler = $('<div id="redactor-droparea-placeholder" />').text(this.lang.get("upload_label")), 
                    this.upload.$input = $('<input class="form-control" type="file" name="file" />'), 
                    this.upload.$placeholdler.append(this.upload.$input), this.upload.$droparea.append(this.upload.$placeholdler), 
                    this.upload.$el.append(this.upload.$droparea), this.upload.$droparea.off("redactor.upload"), 
                    this.upload.$input.off("redactor.upload"), this.upload.$droparea.on("dragover.redactor.upload", $.proxy(this.upload.onDrag, this)), 
                    this.upload.$droparea.on("dragleave.redactor.upload", $.proxy(this.upload.onDragLeave, this)), 
                    this.upload.$input.on("change.redactor.upload", $.proxy(function(e) {
                        e = e.originalEvent || e, this.upload.traverseFile(this.upload.$input[0].files[0], e);
                    }, this)), this.upload.$droparea.on("drop.redactor.upload", $.proxy(function(e) {
                        e.preventDefault(), this.upload.$droparea.removeClass("drag-hover").addClass("drag-drop"), 
                        this.upload.onDrop(e);
                    }, this));
                },
                directUpload: function(file, e) {
                    this.upload.direct = !0, this.upload.traverseFile(file, e);
                },
                onDrop: function(e) {
                    e = e.originalEvent || e;
                    var files = e.dataTransfer.files;
                    this.upload.traverseFile(files[0], e);
                },
                traverseFile: function(file, e) {
                    if (this.opts.s3) return this.upload.setConfig(file), void this.upload.s3uploadFile(file);
                    var formData = window.FormData ? new FormData() : null;
                    if (window.FormData) {
                        this.upload.setConfig(file);
                        var name = "image" == this.upload.type ? this.opts.imageUploadParam : this.opts.fileUploadParam;
                        formData.append(name, file);
                    }
                    this.progress.show(), this.core.setCallback("uploadStart", e, formData), this.upload.sendData(formData, e);
                },
                setConfig: function(file) {
                    this.upload.getType(file), this.upload.direct && (this.upload.url = "image" == this.upload.type ? this.opts.imageUpload : this.opts.fileUpload, 
                    this.upload.callback = "image" == this.upload.type ? this.image.insert : this.file.insert);
                },
                getType: function(file) {
                    this.upload.type = "image", -1 == this.opts.imageTypes.indexOf(file.type) && (this.upload.type = "file");
                },
                getHiddenFields: function(obj, fd) {
                    return obj === !1 || "object" != typeof obj ? fd : ($.each(obj, $.proxy(function(k, v) {
                        null !== v && 0 === v.toString().indexOf("#") && (v = $(v).val()), fd.append(k, v);
                    }, this)), fd);
                },
                sendData: function(formData, e) {
                    "image" == this.upload.type ? (formData = this.upload.getHiddenFields(this.opts.uploadImageFields, formData), 
                    formData = this.upload.getHiddenFields(this.upload.imageFields, formData)) : (formData = this.upload.getHiddenFields(this.opts.uploadFileFields, formData), 
                    formData = this.upload.getHiddenFields(this.upload.fileFields, formData));
                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", this.upload.url), xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest"), 
                    xhr.onreadystatechange = $.proxy(function() {
                        if (4 == xhr.readyState) {
                            var data = xhr.responseText;
                            data = data.replace(/^\[/, ""), data = data.replace(/\]$/, "");
                            var json;
                            try {
                                json = "string" == typeof data ? $.parseJSON(data) : data;
                            } catch (err) {
                                json = {
                                    error: !0
                                };
                            }
                            this.progress.hide(), this.upload.direct || this.upload.$droparea.removeClass("drag-drop"), 
                            this.upload.callback(json, this.upload.direct, e);
                        }
                    }, this), xhr.send(formData);
                },
                onDrag: function(e) {
                    e.preventDefault(), this.upload.$droparea.addClass("drag-hover");
                },
                onDragLeave: function(e) {
                    e.preventDefault(), this.upload.$droparea.removeClass("drag-hover");
                },
                clearImageFields: function() {
                    this.upload.imageFields = {};
                },
                addImageFields: function(name, value) {
                    this.upload.imageFields[name] = value;
                },
                removeImageFields: function(name) {
                    delete this.upload.imageFields[name];
                },
                clearFileFields: function() {
                    this.upload.fileFields = {};
                },
                addFileFields: function(name, value) {
                    this.upload.fileFields[name] = value;
                },
                removeFileFields: function(name) {
                    delete this.upload.fileFields[name];
                },
                s3uploadFile: function(file) {
                    this.upload.s3executeOnSignedUrl(file, $.proxy(function(signedURL) {
                        this.upload.s3uploadToS3(file, signedURL);
                    }, this));
                },
                s3executeOnSignedUrl: function(file, callback) {
                    var xhr = new XMLHttpRequest(), mark = "-1" !== this.opts.s3.search(/\?/) ? "?" : "&";
                    xhr.open("GET", this.opts.s3 + mark + "name=" + file.name + "&type=" + file.type, !0), 
                    xhr.overrideMimeType && xhr.overrideMimeType("text/plain; charset=x-user-defined");
                    var that = this;
                    xhr.onreadystatechange = function(e) {
                        4 == this.readyState && 200 == this.status ? (that.progress.show(), callback(decodeURIComponent(this.responseText))) : 4 == this.readyState && 200 != this.status;
                    }, xhr.send();
                },
                s3createCORSRequest: function(method, url) {
                    var xhr = new XMLHttpRequest();
                    return "withCredentials" in xhr ? xhr.open(method, url, !0) : "undefined" != typeof XDomainRequest ? (xhr = new XDomainRequest(), 
                    xhr.open(method, url)) : xhr = null, xhr;
                },
                s3uploadToS3: function(file, url) {
                    var xhr = this.upload.s3createCORSRequest("PUT", url);
                    xhr && (xhr.onload = $.proxy(function() {
                        if (200 == xhr.status) {
                            this.progress.hide();
                            var s3file = url.split("?");
                            if (!s3file[0]) return !1;
                            this.upload.direct || this.upload.$droparea.removeClass("drag-drop");
                            var json = {
                                filelink: s3file[0]
                            };
                            if ("file" == this.upload.type) {
                                var arr = s3file[0].split("/");
                                json.filename = arr[arr.length - 1];
                            }
                            this.upload.callback(json, this.upload.direct, !1);
                        }
                    }, this), xhr.onerror = function() {}, xhr.upload.onprogress = function(e) {}, xhr.setRequestHeader("Content-Type", file.type), 
                    xhr.setRequestHeader("x-amz-acl", "public-read"), xhr.send(file));
                }
            };
        },
        utils: function() {
            return {
                isMobile: function() {
                    return /(iPhone|iPod|BlackBerry|Android)/.test(navigator.userAgent);
                },
                isDesktop: function() {
                    return !/(iPhone|iPod|iPad|BlackBerry|Android)/.test(navigator.userAgent);
                },
                isString: function(obj) {
                    return "[object String]" == Object.prototype.toString.call(obj);
                },
                isEmpty: function(html, removeEmptyTags) {
                    return html = html.replace(/[\u200B-\u200D\uFEFF]/g, ""), html = html.replace(/&nbsp;/gi, ""), 
                    html = html.replace(/<\/?br\s?\/?>/g, ""), html = html.replace(/\s/g, ""), html = html.replace(/^<p>[^\W\w\D\d]*?<\/p>$/i, ""), 
                    html = html.replace(/<iframe(.*?[^>])>$/i, "iframe"), html = html.replace(/<source(.*?[^>])>$/i, "source"), 
                    removeEmptyTags !== !1 && (html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, ""), 
                    html = html.replace(/<[^\/>][^>]*><\/[^>]+>/gi, "")), html = $.trim(html), "" === html;
                },
                normalize: function(str) {
                    return "undefined" == typeof str ? 0 : parseInt(str.replace("px", ""), 10);
                },
                hexToRgb: function(hex) {
                    if ("undefined" != typeof hex) {
                        if (-1 == hex.search(/^#/)) return hex;
                        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                            return r + r + g + g + b + b;
                        });
                        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                        return "rgb(" + parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + ", " + parseInt(result[3], 16) + ")";
                    }
                },
                getOuterHtml: function(el) {
                    return $("<div>").append($(el).eq(0).clone()).html();
                },
                getAlignmentElement: function(el) {
                    return -1 !== $.inArray(el.tagName, this.opts.alignmentTags) ? $(el) : $(el).closest(this.opts.alignmentTags.toString().toLowerCase(), this.$editor[0]);
                },
                removeEmptyAttr: function(el, attr) {
                    var $el = $(el);
                    return "undefined" == typeof $el.attr(attr) ? !0 : "" === $el.attr(attr) ? ($el.removeAttr(attr), 
                    !0) : !1;
                },
                removeEmpty: function(i, s) {
                    var $s = $($.parseHTML(s));
                    if ($s.find(".redactor-invisible-space").removeAttr("style").removeAttr("class"), 
                    0 === $s.find("hr, br, img, iframe, source").length) {
                        var text = $.trim($s.text());
                        this.utils.isEmpty(text, !1) && $s.remove();
                    }
                },
                saveScroll: function() {
                    this.saveEditorScroll = this.$editor.scrollTop(), this.saveBodyScroll = $(window).scrollTop(), 
                    this.opts.scrollTarget && (this.saveTargetScroll = $(this.opts.scrollTarget).scrollTop());
                },
                restoreScroll: function() {
                    ("undefined" != typeof this.saveScroll || "undefined" != typeof this.saveBodyScroll) && ($(window).scrollTop(this.saveBodyScroll), 
                    this.$editor.scrollTop(this.saveEditorScroll), this.opts.scrollTarget && $(this.opts.scrollTarget).scrollTop(this.saveTargetScroll));
                },
                createSpaceElement: function() {
                    var space = document.createElement("span");
                    return space.className = "redactor-invisible-space", space.innerHTML = this.opts.invisibleSpace, 
                    space;
                },
                removeInlineTags: function(node) {
                    var tags = this.opts.inlineTags;
                    tags.push("span"), "PRE" == node.tagName && tags.push("a"), $(node).find(tags.join(",")).not("span.redactor-selection-marker").contents().unwrap();
                },
                replaceWithContents: function(node, removeInlineTags) {
                    var self = this;
                    return $(node).replaceWith(function() {
                        return removeInlineTags === !0 && self.utils.removeInlineTags(this), $(this).contents();
                    }), $(node);
                },
                replaceToTag: function(node, tag, removeInlineTags) {
                    var replacement, self = this;
                    return $(node).replaceWith(function() {
                        replacement = $("<" + tag + " />").append($(this).contents());
                        for (var i = 0; i < this.attributes.length; i++) replacement.attr(this.attributes[i].name, this.attributes[i].value);
                        return removeInlineTags === !0 && self.utils.removeInlineTags(replacement), replacement;
                    }), replacement;
                },
                isStartOfElement: function() {
                    var block = this.selection.getBlock();
                    if (!block) return !1;
                    var offset = this.caret.getOffsetOfElement(block);
                    return 0 === offset ? !0 : !1;
                },
                isEndOfElement: function(element) {
                    if ("undefined" == typeof element) {
                        var element = this.selection.getBlock();
                        if (!element) return !1;
                    }
                    var offset = this.caret.getOffsetOfElement(element), text = $.trim($(element).text()).replace(/\n\r\n/g, "");
                    return offset == text.length ? !0 : !1;
                },
                isStartOfEditor: function() {
                    var offset = this.caret.getOffsetOfElement(this.$editor[0]);
                    return 0 === offset ? !0 : !1;
                },
                isEndOfEditor: function() {
                    var block = this.$editor[0], offset = this.caret.getOffsetOfElement(block), text = $.trim($(block).html().replace(/(<([^>]+)>)/gi, ""));
                    return offset == text.length ? !0 : !1;
                },
                isBlock: function(block) {
                    return block = block[0] || block, block && this.utils.isBlockTag(block.tagName);
                },
                isBlockTag: function(tag) {
                    return "undefined" == typeof tag ? !1 : this.reIsBlock.test(tag);
                },
                isTag: function(current, tag) {
                    var element = $(current).closest(tag, this.$editor[0]);
                    return 1 == element.length ? element[0] : !1;
                },
                isSelectAll: function() {
                    return this.selectAll;
                },
                enableSelectAll: function() {
                    this.selectAll = !0;
                },
                disableSelectAll: function() {
                    this.selectAll = !1;
                },
                isRedactorParent: function(el) {
                    return el ? 0 === $(el).parents(".redactor-editor").length || $(el).hasClass("redactor-editor") ? !1 : el : !1;
                },
                isCurrentOrParentHeader: function() {
                    return this.utils.isCurrentOrParent([ "H1", "H2", "H3", "H4", "H5", "H6" ]);
                },
                isCurrentOrParent: function(tagName) {
                    var parent = this.selection.getParent(), current = this.selection.getCurrent();
                    if ($.isArray(tagName)) {
                        var matched = 0;
                        return $.each(tagName, $.proxy(function(i, s) {
                            this.utils.isCurrentOrParentOne(current, parent, s) && matched++;
                        }, this)), 0 === matched ? !1 : !0;
                    }
                    return this.utils.isCurrentOrParentOne(current, parent, tagName);
                },
                isCurrentOrParentOne: function(current, parent, tagName) {
                    return tagName = tagName.toUpperCase(), parent && parent.tagName === tagName ? parent : current && current.tagName === tagName ? current : !1;
                },
                isOldIe: function() {
                    return this.utils.browser("msie") && parseInt(this.utils.browser("version"), 10) < 9 ? !0 : !1;
                },
                isLessIe10: function() {
                    return this.utils.browser("msie") && parseInt(this.utils.browser("version"), 10) < 10 ? !0 : !1;
                },
                isIe11: function() {
                    return !!navigator.userAgent.match(/Trident\/7\./);
                },
                browser: function(browser) {
                    var ua = navigator.userAgent.toLowerCase(), match = /(opr)[\/]([\w.]+)/.exec(ua) || /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];
                    return "safari" == browser ? "undefined" != typeof match[3] ? "safari" == match[3] : !1 : "version" == browser ? match[2] : "webkit" == browser ? "chrome" == match[1] || "opr" == match[1] || "webkit" == match[1] : "rv" == match[1] ? "msie" == browser : "opr" == match[1] ? "webkit" == browser : browser == match[1];
                },
                strpos: function(haystack, needle, offset) {
                    var i = haystack.indexOf(needle, offset);
                    return i >= 0 ? i : !1;
                },
                disableBodyScroll: function() {
                    var $body = $("html"), windowWidth = window.innerWidth;
                    if (!windowWidth) {
                        var documentElementRect = document.documentElement.getBoundingClientRect();
                        windowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
                    }
                    var isOverflowing = document.body.clientWidth < windowWidth, scrollbarWidth = this.utils.measureScrollbar();
                    $body.css("overflow", "hidden"), isOverflowing && $body.css("padding-right", scrollbarWidth);
                },
                measureScrollbar: function() {
                    var $body = $("body"), scrollDiv = document.createElement("div");
                    scrollDiv.className = "redactor-scrollbar-measure", $body.append(scrollDiv);
                    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
                    return $body[0].removeChild(scrollDiv), scrollbarWidth;
                },
                enableBodyScroll: function() {
                    $("html").css({
                        overflow: "",
                        "padding-right": ""
                    }), $("body").remove("redactor-scrollbar-measure");
                }
            };
        }
    }, $(window).on("load.tools.redactor", function() {
        $('[data-tools="redactor"]').redactor();
    }), Redactor.prototype.init.prototype = Redactor.prototype;
}(jQuery), $.Redactor.prototype.insertImage = function() {
    var controller = {};
    return controller.init = function() {
        var button = this.button.add("insertImage", "Insert image");
        this.button.addCallback(button, controller.select), this.button.setAwesome("insertImage", "fa-image");
    }, controller.select = function() {
        this.selection.save(), this.buffer.set();
        var $scope = angular.element(this.$element).scope();
        $scope.modal.open({
            template: '<content-browser ng-model="images" ng-done="submit" ng-type="\'image\'"></content-browser>',
            controller: function($modalInstance, $rootScope, $scope) {
                $scope.images = [], $scope.submit = function() {
                    var mapped = _.map($scope.images, function(img) {
                        return {
                            _id: img._id,
                            url: $rootScope.asset.imageUrl(img._id)
                        };
                    });
                    controller.insert(mapped), $modalInstance.close($scope.images);
                };
            }
        });
    }, controller.insert = function(images) {
        var self = this;
        if (self.selection.restore(), images.length) {
            var str = "";
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(\'' + image._id + "')}}\"/>";
            }), self.insert.html(str), self.code.sync();
        }
    }, controller;
}, $.Redactor.prototype.video = function() {
    return {
        reUrlYoutube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/gi,
        reUrlVimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
        getTemplate: function() {
            return String() + '<section id="redactor-modal-video-insert"><label>' + this.lang.get("video_html_code") + '</label><textarea class="form-control" id="redactor-insert-video-area" style="height: 160px;"></textarea></section>';
        },
        init: function() {
            var button = this.button.addAfter("image", "video", "Embed Video");
            this.button.addCallback(button, this.video.show);
        },
        show: function() {
            this.modal.addTemplate("video", this.video.getTemplate()), this.modal.load("video", this.lang.get("video"), 700), 
            this.modal.createCancelButton();
            var button = this.modal.createActionButton(this.lang.get("insert"));
            button.on("click", this.video.insert), this.selection.save(), this.modal.show(), 
            $("#redactor-insert-video-area").focus();
        },
        insert: function() {
            var data = $("#redactor-insert-video-area").val();
            if (!data.match(/<iframe|<video/gi)) {
                data = this.clean.stripTags(data);
                var iframeStart = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="', iframeEnd = '" frameborder="0" allowfullscreen></iframe></div>';
                data.match(this.video.reUrlYoutube) ? data = data.replace(this.video.reUrlYoutube, iframeStart + "//www.youtube.com/embed/$1" + iframeEnd) : data.match(this.video.reUrlVimeo) && (data = data.replace(this.video.reUrlVimeo, iframeStart + "//player.vimeo.com/video/$2" + iframeEnd));
            }
            this.selection.restore(), this.modal.close();
            var current = this.selection.getBlock() || this.selection.getCurrent();
            current ? $(current).after(data) : this.insert.html(data), this.code.sync();
        }
    };
}, $.Redactor.prototype.table = function() {
    return {
        getTemplate: function() {
            return String() + '<section id="redactor-modal-table-insert"><label>' + this.lang.get("rows") + '</label><input class="form-control" type="text" size="5" value="2" id="redactor-table-rows" /><label>' + this.lang.get("columns") + '</label><input class="form-control" type="text" size="5" value="3" id="redactor-table-columns" /></section>';
        },
        init: function() {
            var dropdown = {};
            dropdown.insert_table = {
                title: this.lang.get("insert_table"),
                func: this.table.show,
                observe: {
                    element: "table",
                    "in": {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.insert_row_above = {
                title: this.lang.get("insert_row_above"),
                func: this.table.addRowAbove,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.insert_row_below = {
                title: this.lang.get("insert_row_below"),
                func: this.table.addRowBelow,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.insert_column_left = {
                title: this.lang.get("insert_column_left"),
                func: this.table.addColumnLeft,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.insert_column_right = {
                title: this.lang.get("insert_column_right"),
                func: this.table.addColumnRight,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.add_head = {
                title: this.lang.get("add_head"),
                func: this.table.addHead,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.delete_head = {
                title: this.lang.get("delete_head"),
                func: this.table.deleteHead,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.delete_column = {
                title: this.lang.get("delete_column"),
                func: this.table.deleteColumn,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.delete_row = {
                title: this.lang.get("delete_row"),
                func: this.table.deleteRow,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, dropdown.delete_table = {
                title: this.lang.get("delete_table"),
                func: this.table.deleteTable,
                observe: {
                    element: "table",
                    out: {
                        attr: {
                            "class": "redactor-dropdown-link-inactive",
                            "aria-disabled": !0
                        }
                    }
                }
            }, this.observe.addButton("td", "table"), this.observe.addButton("th", "table");
            var button = this.button.addBefore("link", "table", this.lang.get("table"));
            this.button.addDropdown(button, dropdown);
        },
        show: function() {
            this.modal.addTemplate("table", this.table.getTemplate()), this.modal.load("table", this.lang.get("insert_table"), 300), 
            this.modal.createCancelButton();
            var button = this.modal.createActionButton(this.lang.get("insert"));
            button.on("click", this.table.insert), this.selection.save(), this.modal.show(), 
            $("#redactor-table-rows").focus();
        },
        insert: function() {
            this.placeholder.remove();
            var i, $row, z, $column, rows = $("#redactor-table-rows").val(), columns = $("#redactor-table-columns").val(), $tableBox = $("<div>"), tableId = Math.floor(99999 * Math.random()), $table = $('<table id="table' + tableId + '" class="table table-striped table-bordered"><tbody></tbody></table>');
            for (i = 0; rows > i; i++) {
                for ($row = $("<tr>"), z = 0; columns > z; z++) $column = $("<td>" + this.opts.invisibleSpace + "</td>"), 
                0 === i && 0 === z && $column.append(this.selection.getMarker()), $($row).append($column);
                $table.append($row);
            }
            $tableBox.append($table);
            var html = $tableBox.html();
            if (this.modal.close(), this.selection.restore(), !this.table.getTable()) {
                this.buffer.set();
                var current = this.selection.getBlock() || this.selection.getCurrent();
                current && "BODY" != current.tagName ? ("LI" == current.tagName && (current = $(current).closest("ul, ol")), 
                $(current).after(html)) : this.insert.html(html, !1), this.selection.restore();
                var table = this.$editor.find("#table" + tableId), p = table.prev("p");
                if (p.length > 0 && this.utils.isEmpty(p.html()) && p.remove(), !this.opts.linebreaks && (this.utils.browser("mozilla") || this.utils.browser("msie"))) {
                    var $next = table.next();
                    0 === $next.length && table.after(this.opts.emptyHtml);
                }
                this.observe.buttons(), table.find("span.redactor-selection-marker").remove(), table.removeAttr("id"), 
                this.code.sync(), this.core.setCallback("insertedTable", table);
            }
        },
        getTable: function() {
            var $table = $(this.selection.getParent()).closest("table");
            return this.utils.isRedactorParent($table) ? 0 === $table.size() ? !1 : $table : !1;
        },
        restoreAfterDelete: function($table) {
            this.selection.restore(), $table.find("span.redactor-selection-marker").remove(), 
            this.code.sync();
        },
        deleteTable: function() {
            var $table = this.table.getTable();
            if ($table) {
                this.buffer.set();
                var $next = $table.next();
                this.opts.linebreaks || 0 === $next.length ? this.caret.setAfter($table) : this.caret.setStart($next), 
                $table.remove(), this.code.sync();
            }
        },
        deleteRow: function() {
            var $table = this.table.getTable();
            if ($table) {
                var $current = $(this.selection.getCurrent());
                this.buffer.set();
                var $current_tr = $current.closest("tr"), $focus_tr = $current_tr.prev().length ? $current_tr.prev() : $current_tr.next();
                if ($focus_tr.length) {
                    var $focus_td = $focus_tr.children("td, th").first();
                    $focus_td.length && $focus_td.prepend(this.selection.getMarker());
                }
                $current_tr.remove(), this.table.restoreAfterDelete($table);
            }
        },
        deleteColumn: function() {
            var $table = this.table.getTable();
            if ($table) {
                this.buffer.set();
                var $current = $(this.selection.getCurrent()), $current_td = $current.closest("td, th"), index = $current_td[0].cellIndex;
                $table.find("tr").each($.proxy(function(i, elem) {
                    var $elem = $(elem), focusIndex = 0 > index - 1 ? index + 1 : index - 1;
                    0 === i && $elem.find("td, th").eq(focusIndex).prepend(this.selection.getMarker()), 
                    $elem.find("td, th").eq(index).remove();
                }, this)), this.table.restoreAfterDelete($table);
            }
        },
        addHead: function() {
            var $table = this.table.getTable();
            if ($table) {
                if (this.buffer.set(), 0 !== $table.find("thead").size()) return void this.table.deleteHead();
                var tr = $table.find("tr").first().clone();
                tr.find("td").replaceWith($.proxy(function() {
                    return $("<th>").html(this.opts.invisibleSpace);
                }, this)), $thead = $("<thead></thead>").append(tr), $table.prepend($thead), this.code.sync();
            }
        },
        deleteHead: function() {
            var $table = this.table.getTable();
            if ($table) {
                var $thead = $table.find("thead");
                0 !== $thead.size() && (this.buffer.set(), $thead.remove(), this.code.sync());
            }
        },
        addRowAbove: function() {
            this.table.addRow("before");
        },
        addRowBelow: function() {
            this.table.addRow("after");
        },
        addColumnLeft: function() {
            this.table.addColumn("before");
        },
        addColumnRight: function() {
            this.table.addColumn("after");
        },
        addRow: function(type) {
            var $table = this.table.getTable();
            if ($table) {
                this.buffer.set();
                var $current = $(this.selection.getCurrent()), $current_tr = $current.closest("tr"), new_tr = $current_tr.clone();
                new_tr.find("th").replaceWith(function() {
                    var $td = $("<td>");
                    return $td[0].attributes = this.attributes, $td.append($(this).contents());
                }), new_tr.find("td").html(this.opts.invisibleSpace), "after" == type ? $current_tr.after(new_tr) : $current_tr.before(new_tr), 
                this.code.sync();
            }
        },
        addColumn: function(type) {
            var $table = this.table.getTable();
            if ($table) {
                var index = 0, current = $(this.selection.getCurrent());
                this.buffer.set();
                var $current_tr = current.closest("tr"), $current_td = current.closest("td, th");
                $current_tr.find("td, th").each($.proxy(function(i, elem) {
                    $(elem)[0] === $current_td[0] && (index = i);
                }, this)), $table.find("tr").each($.proxy(function(i, elem) {
                    var $current = $(elem).find("td, th").eq(index), td = $current.clone();
                    td.html(this.opts.invisibleSpace), "after" == type ? $current.after(td) : $current.before(td);
                }, this)), this.code.sync();
            }
        }
    };
}, $.Redactor.prototype.undoAction = function() {
    var controller = {};
    return controller.init = function() {
        var button = this.button.add("undoAction", "Undo");
        this.button.addCallback(button, this.buffer.undo), this.button.setAwesome("undoAction", "fa-undo");
    }, controller;
}, $.Redactor.prototype.redoAction = function() {
    var controller = {};
    return controller.init = function() {
        var button = this.button.add("redoAction", "Redo");
        this.button.addCallback(button, this.buffer.redo), this.button.setAwesome("redoAction", "fa-repeat");
    }, controller;
}, app.directive("route", function($compile, $parse, $rootScope, FluroContentRetrieval) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            route: "=ngModel"
        },
        template: "<div></div>",
        controller: function($scope, $controller) {
            var noCache = !0;
            $scope.$watch("route.testSlug", function(slug) {
                var appendPostCount = $scope.route.appendPostCount, appendPosts = $scope.route.appendPosts, appendContactDetail = $scope.route.appendContactDetail, appendProcess = $scope.route.appendProcess, appendForms = $scope.route.appendForms, appendTeams = $scope.route.appendTeams, appendAssignments = $scope.route.appendAssignments, includePublicSearchResults = $scope.route.includePublic;
                appendPostCount && appendPostCount.length || (appendPostCount = "all"), appendProcess && appendProcess.length || (appendProcess = "all"), 
                appendForms && appendForms.length || (appendForms = "all"), slug ? FluroContentRetrieval.get([ slug._id ], noCache, {
                    searchInheritable: !0,
                    includePublic: includePublicSearchResults,
                    appendPostCount: appendPostCount,
                    appendPosts: appendPosts,
                    appendProcess: appendProcess,
                    appendContactDetail: appendContactDetail,
                    appendForms: appendForms,
                    appendAssignments: appendAssignments,
                    appendTeams: appendTeams
                }).then(function(res) {
                    res && res.length && ($scope.slug = res[0]);
                }) : delete $scope.slug;
            }), $scope.$watch("route.content", function(nodes) {
                nodes && nodes.length ? FluroContentRetrieval.populate(nodes, noCache, {
                    searchInheritable: !0
                }).then(function(res) {
                    $scope.items = res;
                }) : $scope.items = [];
            }, !0), $scope.$watch("route.keys", function(keyGroups) {
                var nids = _.chain(keyGroups).map(function(group) {
                    return group.content;
                }).flatten().compact().value();
                nids && nids.length ? FluroContentRetrieval.populate(nids, noCache, {
                    searchInheritable: !0
                }).then(function(res) {
                    $scope.content = _.reduce(keyGroups, function(result, keyGroup) {
                        return result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                            return nid._id && (nid = nid._id), _.find(res, {
                                _id: nid
                            });
                        }), result;
                    }, {});
                }) : $scope.content = {};
            }, !0), $scope.route.params ? $scope.params = $scope.route.params : $scope.params = {}, 
            $scope.$watch("route.collections", function(nodes) {
                if ($scope.collections = [], $scope.collectionResults = [], nodes && nodes.length) {
                    var collectionIds = _.chain(nodes).map(function(collection) {
                        return collection._id ? collection._id : collection;
                    }).compact().value();
                    FluroContentRetrieval.getMultipleCollections(collectionIds, !0).then(function(res) {
                        res && ($scope.collections = res, $scope.collectionResults = _.chain(res).map(function(collection) {
                            return collection.items;
                        }).flatten().uniq().value());
                    });
                }
            }, !0), $scope.$watch("route.queries + route.queryVariables + slug", function() {
                var nodes = $scope.route.queries;
                if ($scope.results = [], $scope.queryResults = [], nodes && nodes.length) {
                    var queryIds = _.chain(nodes).map(function(query) {
                        return query._id ? query._id : query;
                    }).compact().value();
                    if (queryIds && queryIds.length) {
                        var variables;
                        $scope.route.queryVariables && (variables = _.mapValues($scope.route.queryVariables, function(value) {
                            var val = $parse(value)($scope);
                            return val;
                        }), variables = _.pick(variables, _.identity)), FluroContentRetrieval.queryMultiple(queryIds, !0, variables).then(function(res) {
                            res && ($scope.queryResults = [], $scope.results = _.reduce(queryIds, function(result, queryId) {
                                var array = res[queryId];
                                return $scope.queryResults.push(array), array && array.length && _.each(array, function(item) {
                                    result.push(item);
                                }), result;
                            }, []));
                        });
                    }
                }
            }, !0), this.page = $scope, $scope.route.controllerName && $controller($scope.route.controllerName, {
                $scope: $scope
            });
        },
        link: function($scope, $element, $attr, $routeController) {
            $scope.$watch("route.html", function(html) {
                if ($element.empty(), html && html.length) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                } else {
                    var template = $compile('<route-section ng-model="section" ng-route="route" ng-repeat="section in route.sections track by $index" ng-if="!section.disabled"></route-section>')($scope);
                    $element.append(template);
                }
            });
        }
    };
}), app.directive("routeSection", function($compile, $rootScope, $templateCache, $timeout, $parse, FluroContentRetrieval) {
    return {
        restrict: "E",
        replace: !0,
        require: "^route",
        scope: {
            model: "=ngModel",
            route: "=ngRoute"
        },
        template: "<section></section>",
        link: function($scope, $element, $attr, $ctrl) {
            var noCache = !0;
            $scope.page = $ctrl.page, $scope.$watch(function() {
                return $scope.page.slug;
            }, function(slug) {
                $scope.slug = slug;
            }), $scope.$watch("model.temp.highlight", function(highlight) {
                highlight ? $element.addClass("_highlight") : $element.removeClass("_highlight");
            }), $scope.$watch("model.content", function(nodes) {
                $scope.model.select && $scope.model.select.length ? nodes && nodes.length ? FluroContentRetrieval.populatePartial(nodes, $scope.model.select, noCache, !0).then(function(res) {
                    $scope.items = res;
                }) : delete $scope.items : nodes && nodes.length ? FluroContentRetrieval.populate(nodes, noCache, {
                    searchInheritable: !0
                }).then(function(res) {
                    $scope.items = res;
                }) : delete $scope.items;
            }, !0), $scope.$watch("model.keys", function(keyGroups) {
                var nids = _.chain(keyGroups).map(function(group) {
                    return group.content;
                }).flatten().compact().value();
                nids && nids.length ? FluroContentRetrieval.populate(nids, noCache, {
                    searchInheritable: !0
                }).then(function(res) {
                    $scope.content = _.reduce(keyGroups, function(result, keyGroup) {
                        return result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                            return nid._id && (nid = nid._id), _.find(res, {
                                _id: nid
                            });
                        }), result;
                    }, {});
                }) : delete $scope.content;
            }, !0), $scope.$watch("model.params", function(params) {
                $scope.params = params;
            }), $scope.$watch("model.collections", function(nodes) {
                if ($scope.collections = [], $scope.collectionResults = [], nodes && nodes.length) {
                    var collectionIds = _.chain(nodes).map(function(collection) {
                        return collection._id ? collection._id : collection;
                    }).compact().value();
                    FluroContentRetrieval.getMultipleCollections(collectionIds, !0).then(function(res) {
                        res && ($scope.collections = res, $scope.collectionResults = _.chain(res).map(function(collection) {
                            return collection.items;
                        }).flatten().uniq().value());
                    });
                }
            }, !0), $scope.$watch("model.queries + model.queryVariables + slug", function() {
                var nodes = $scope.model.queries;
                if ($scope.results = [], $scope.queryResults = [], nodes && nodes.length) {
                    var queryIds = _.chain(nodes).map(function(query) {
                        return query._id ? query._id : query;
                    }).compact().value();
                    if (queryIds && queryIds.length) {
                        var variables;
                        $scope.model.queryVariables && (variables = _.mapValues($scope.model.queryVariables, function(value) {
                            var val = $parse(value)($scope);
                            return val;
                        }), variables = _.pick(variables, _.identity)), FluroContentRetrieval.queryMultiple(queryIds, !0, variables).then(function(res) {
                            res && ($scope.queryResults = [], $scope.results = _.reduce(queryIds, function(result, queryId) {
                                var array = res[queryId];
                                return $scope.queryResults.push(array), array && array.length && _.each(array, function(item) {
                                    result.push(item);
                                }), result;
                            }, []));
                        });
                    }
                }
            }, !0);
            $scope.$watch(function() {
                var html;
                return $scope.model && (html = $scope.model.template ? $templateCache.get($scope.model.template) : $scope.model.html), 
                html;
            }, function() {
                var html = $scope.model.html;
                if ($scope.model.template && (html = $templateCache.get($scope.model.template)), 
                $element.empty(), html) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                }
            });
        }
    };
}), app.service("BuildSelectionService", function() {
    var controller = {};
    return controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
            return "folder" == route.type ? controller.getFlattenedRoutes(route.routes) : route;
        }).flatten().compact().value();
    }, controller.getFlattenedFolders = function(array) {
        return _.chain(array).reduce(function(result, route, key) {
            if ("folder" == route.type && result.push(route), route.routes && route.routes.length) {
                var folders = controller.getFlattenedFolders(route.routes);
                folders.length && result.push(folders);
            }
            return result;
        }, []).flatten().compact().value();
    }, controller;
}), app.service("BuildService", function(ObjectSelection) {
    var controller = {};
    return controller.selection = new ObjectSelection(), controller.selection.multiple = !1, 
    controller;
}), app.controller("BuildController", function($scope, $state, FluroSocket, $timeout, BuildService, $interval, $rootScope, $modal, snippets, $rootScope, item, BuildSelectionService, $state, Fluro, Notifications, FluroContent, $window, $compile) {
    function socketUpdate(socketEvent) {
        if (socketEnabled) {
            var socketItemID = socketEvent.item;
            if (!socketItemID) return;
            socketItemID._id && (socketItemID = socketItemID._id);
            var sameId = socketItemID == item._id, newVersion = socketEvent.data.updated != item.updated, differentSocket = socketEvent.ignoreSocket != FluroSocket.getSocketID();
            console.log("SOCKET", sameId, newVersion, differentSocket, FluroSocket.getSocketID(), socketEvent.ignoreSocket), 
            sameId && newVersion && differentSocket && $timeout(function() {
                socketEnabled = !1, jsondiffpatch.patch(item, socketEvent.data.diff), socketEnabled = !0, 
                socketEvent.user ? $rootScope.user._id != socketEvent.user._id ? Notifications.warning("This content was just updated by " + socketEvent.user.name) : Notifications.warning("This content was updated in another window") : Notifications.warning("This content was updated by another user");
            });
        }
    }
    function keyUp(event) {
        "91" == String(event.which) && (keySaveDown = !1);
    }
    function keyDown(event) {
        if (event.ctrlKey || event.metaKey) switch (String.fromCharCode(event.which).toLowerCase()) {
          case "s":
            if (event.preventDefault(), keySaveDown) return;
            keySaveDown = !0, $scope.save();
        }
    }
    function saveSuccess(res) {
        return 500 == res.status ? (console.log("VERSION ERROR!!", res, $scope.item), Notifications.error("Version Error, Please try again")) : (Notifications.status("Site saved successfully"), 
        console.log("SAVE SUCCESS", res), res._id && ($scope.item._id ? ($scope.item._id = res._id, 
        $scope.item.__v = res.__v) : ($scope.item._id = res._id, console.log("Move to the State please!"), 
        $state.go("build.default", {
            id: res._id
        }))), void ($scope.isSaving = !1));
    }
    function saveFail(res) {
        console.log("SAVE FAILED", res), res.data ? res.data.errors ? _.each(res.data.errors, function(err) {
            Notifications.error(err);
        }) : Notifications.error("Site failed to save " + res.data) : Notifications.error(res), 
        delete $scope.item.__v, $scope.isSaving = !1;
    }
    FluroSocket.on("content.edit", socketUpdate), $scope.$on("$destroy", function() {
        FluroSocket.off("content.edit", socketUpdate);
    });
    var socketEnabled = !0;
    if ($rootScope.snippets = snippets, $scope.openPreview = function() {
        $window.open("/preview");
    }, window.buildService = BuildService, $scope.buildService = BuildService, $scope.item = item, 
    $scope.$watch("item", function(item) {
        $rootScope.site = item, BuildService.site = item;
    }), item.routes) {
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes(item.routes);
        BuildService.selection.select(flattenedRoutes[0]);
    }
    $(window).on("keydown", keyDown), $(window).on("keyup", keyUp);
    var keySaveDown;
    $scope.$on("$destroy", function() {
        $(window).off("keydown", keyDown), $(window).off("keyup", keyUp);
    });
    var versionMessage = "";
    $scope.saveVersion = function() {
        var modalInstance = $modal.open({
            templateUrl: "routes/build/save-modal.html",
            controller: "SaveModalController",
            size: "sm"
        });
        modalInstance.result.then(function(message) {
            message && message.length ? $scope.save({
                version: message
            }) : $scope.save();
        });
    }, $scope.save = function(options) {
        if ($scope.isSaving) return void console.log("SStop");
        if ($scope.isSaving = !0, options || (options = {}), delete $scope.item.version, 
        options.version && versionMessage != options.version && (console.log("Saving with new message", options.version), 
        versionMessage = options.version, $scope.item.version = options.version, $scope.item.milestone = !0), 
        !$scope.item.title) return void Notifications.warning("Please provide a title before saving");
        Notifications.status("Saving");
        var saveData = angular.copy($scope.item);
        saveData.ignoreSocket = FluroSocket.getSocketID(), $scope.item._id ? FluroContent.resource("site").update({
            id: $scope.item._id
        }, saveData, saveSuccess, saveFail) : (console.log("Save New"), FluroContent.resource("site", !0).save(saveData, saveSuccess, saveFail));
    };
}), app.controller("SaveModalController", function($scope) {}), app.controller("AvailableComponentController", function($scope, FluroContent) {
    function upsert(arr, key, newval) {
        var match = _.find(arr, key);
        if (match) {
            var index = _.indexOf(arr, _.find(arr, key));
            arr.splice(index, 1, newval);
        } else arr.push(newval);
    }
    $scope.item.components || ($scope.item.components = []), $scope.componentUsed = function(component) {
        var match = _.find($scope.item.components, {
            _id: component._id
        });
        return match ? !0 : void 0;
    }, $scope.updateComponent = function(component) {
        var cleanedUp = angular.fromJson(angular.toJson(component));
        upsert($scope.item.components, {
            _id: cleanedUp._id
        }, cleanedUp);
    }, $scope.useComponent = function(component) {
        $scope.componentUsed(component) || $scope.item.components.push(component);
    };
}), app.controller("ComponentManagerController", function($scope, $rootScope, ModalService, FluroContent, $sessionStorage) {
    $scope.selected = {}, $scope.settings = {}, $scope.search = {}, $scope.item.components || ($scope.item.components = []), 
    $scope.sessionStorage = $sessionStorage, $scope.refreshAvailableComponents = function() {
        console.log("Refresh available components"), $scope.availableComponents = FluroContent.resource("component", !1, !0).query();
    }, $scope.refreshAvailableComponents(), $scope.refreshMarketComponents = function() {
        console.log("Refresh market components"), $scope.marketComponents = FluroContent.endpoint("my/components", !1, !0).query();
    }, $scope.sameAccount = function(component) {
        var accountId = component.account;
        if (accountId) return accountId._id && (accountId = accountId._id), accountId == $rootScope.user.account._id;
    }, $scope.addComponent = function() {
        var startScript = "app.components.directive('[DIRECTIVENAME]', function() {\n    return {\n        restrict: 'E', \n        replace:true, \n        template:'[FLURO_TEMPLATE]', \n        link: function($scope, $elem, $attr) {\n        } \n    } \n});", newScript = {
            title: "New Component",
            js: startScript,
            html: "<div></div>",
            realms: []
        }, params = {};
        params.template = newScript, ModalService.create("component", params, function(res) {
            $scope.item.components.push(res), $scope.selected.component = res, $scope.refreshAvailableComponents();
        });
    }, $scope.editComponent = function() {
        ModalService.edit($scope.selected.component, function(res) {
            $scope.replaceComponent(res), $scope.refreshAvailableComponents();
        });
    }, $scope.removeComponent = function() {
        $scope.selected.component && (_.pull($scope.item.components, $scope.selected.component), 
        $scope.selected.component = null);
    }, $scope.pullComponent = function(component) {
        if (console.log("Pull Component", component), component) {
            var match = _.find($scope.item.components, {
                _id: component._id
            });
            match && (_.pull($scope.item.components, match), $scope.selected.component = null);
        }
    }, $scope.replaceComponent = function(component) {
        var cleanedUp = angular.fromJson(angular.toJson(component));
        _.assign($scope.selected.component, cleanedUp), console.log("Updated component", $scope.selected.component);
    }, $scope.updateComponent = function(component) {
        function success(res) {
            $scope.settings.status = "ready", $scope.replaceComponent(res);
        }
        function failed(res) {
            $scope.settings.status = "ready", console.log("Failed to update component", res);
        }
        $scope.settings.status = "processing", FluroContent.resource("component").get({
            id: component._id
        }).$promise.then(success, failed);
    }, $scope.publishComponent = function(component) {
        function success(res) {
            $scope.settings.status = "ready", component._id = res._id, console.log("Saved component", res), 
            $scope.refreshAvailableComponents();
        }
        function failed(res) {
            $scope.settings.status = "ready", console.log("Failed to save component", res);
        }
        (component._id || component.realms || component.realms.length) && ($scope.settings.status = "processing", 
        component._id ? FluroContent.resource("component").update({
            id: component._id
        }, component, success, failed) : FluroContent.resource("component").save(component).$promise.then(success, failed));
    };
}), app.controller("ComponentMarketController", function($scope, $modal, FluroContent) {
    function upsert(arr, key, newval) {
        var match = _.find(arr, key);
        if (match) {
            var index = _.indexOf(arr, _.find(arr, key));
            arr.splice(index, 1, newval);
        } else arr.push(newval);
    }
    $scope.item.components || ($scope.item.components = []), $scope.refreshMarketComponents(), 
    $scope.componentUsed = function(component) {
        var match = _.find($scope.item.components, {
            _id: component._id
        });
        return match ? !0 : void 0;
    }, $scope.viewReadme = function(component) {
        $modal.open({
            template: '<div class="panel panel-default"><div class="panel-body"><h2>{{model.title}}</h2><div compile-html="model.readme"/></div></div>',
            controller: function($scope) {
                $scope.model = component;
            }
        });
    }, $scope.updateComponent = function(component) {
        var cleanedUp = angular.fromJson(angular.toJson(component));
        upsert($scope.item.components, {
            _id: cleanedUp._id
        }, cleanedUp);
    }, $scope.useComponent = function(component) {
        $scope.componentUsed(component) || $scope.item.components.push(component);
    };
}), app.controller("HomeController", function($scope, items) {
    console.log("Home Controller", items), $scope.items = items;
}), app.controller("PreviewController", function($scope, $rootScope, $templateCache, $timeout, $compile, Fluro, $interval) {
    function refreshPreview() {
        var newTemplate = "";
        if (!window.opener || !window.opener.buildService) return console.log("window opener not found");
        var buildService = window.opener.buildService;
        if ($rootScope.buildService = buildService, $rootScope.site = $scope.site = buildService.site, 
        $rootScope.currentRoute = buildService.selection.item, $rootScope.bodyClasses = "route-" + _.kebabCase(buildService.selection.item.state), 
        $scope.site.outerHTML && $scope.site.outerHTML.length && (newTemplate = buildService.site.outerHTML), 
        _.each(buildService.site.templates, function(sectionTemplate) {
            $templateCache.put(sectionTemplate.key, sectionTemplate.html);
        }), cachedTemplate != newTemplate) {
            cachedTemplate = newTemplate;
            var template = cachedTemplate;
            template && template.length || (template = defaultTemplate), template = template.replace(/<fluro-header\/>/g, '<route class="site-header" ng-if="!$root.currentRoute.disableHeader" ng-model="buildService.site.header"></route>'), 
            template = template.replace(/<fluro-content\/>/g, '<div class="site-page-container"><route class="site-page" ng-model="buildService.selection.item"></route></div>'), 
            template = template.replace(/<fluro-footer\/>/g, '<route class="site-footer" ng-if="!$root.currentRoute.disableFooter" ng-model="buildService.site.footer"></route>'), 
            $element.empty(), $timeout(function() {
                var compiled = $compile(template)($scope);
                $element.append(compiled);
            }, 1e3);
        }
    }
    $rootScope.previewMode = !0;
    var cachedTemplate, $element = angular.element(".site"), defaultTemplate = "<fluro-header/><fluro-content/><fluro-footer/>";
    $interval(refreshPreview, 1500);
}), app.controller("TemplatesController", function($scope, items, $state, $modal, FluroContent) {
    $scope.items = items, $scope.search = {}, $scope.useTemplate = function(item) {
        $modal.open({
            backdrop: "static",
            templateUrl: "routes/templates/template-copy-modal.html",
            controller: function($scope) {
                $scope.template = {
                    title: item.title,
                    _id: item._id,
                    extras: []
                }, $scope.status = "ready", $scope.create = function() {
                    $scope.status = "processing";
                    var data = $scope.template;
                    FluroContent.endpoint("templates/use").save(data).$promise.then(function(site) {
                        $scope.$close(), $state.go("build.default", {
                            id: site._id
                        });
                    }, function(res) {
                        console.log("Failed"), $scope.status = "error";
                    });
                }, $scope.isSelected = function(typeName) {
                    var includes = $scope.template.extras;
                    return _.contains(includes, typeName);
                }, $scope.toggle = function(typeName) {
                    $scope.isSelected(typeName) ? _.pull($scope.template.extras, typeName) : $scope.template.extras.push(typeName);
                }, $scope.types = [], $scope.types.push({
                    title: "Definitions",
                    type: "definitions",
                    description: "Include all definitions from the parent account"
                }), $scope.types.push({
                    title: "Roles and Permission Sets",
                    type: "roles",
                    description: "Recreate the roles and permission sets defined in the parent account"
                }), $scope.types.push({
                    title: "Setup new application",
                    type: "application",
                    description: 'Setup a new "Website" application and include this site model'
                });
            },
            size: "md"
        });
    };
}), app.controller("VersionListController", function($scope, logs) {}), app.factory("ObjectSelection", function() {
    var ObjectSelection = function() {
        var controller = {
            items: [],
            item: null
        };
        return controller.multiple = !0, controller.select = function(item) {
            controller.multiple ? _.contains(controller.items, item) || controller.items.push(item) : controller.item = item;
        }, controller.deselect = function(item) {
            controller.multiple ? _.pull(controller.items, item) : controller.item = null;
        }, controller.toggle = function(item) {
            controller.multiple ? _.contains(controller.items, item) ? controller.deselect(item) : controller.select(item) : controller.item == item ? controller.item = null : controller.item = item;
        }, controller.contains = function(item) {
            return controller.multiple ? _.contains(controller.items, item) : controller.item == item;
        }, controller;
    };
    return ObjectSelection;
}), app.directive("compileTemplate", function($compile, $rootScope, $templateCache) {
    return {
        restrict: "A",
        link: function($scope, $element, $attr) {
            var templateName;
            $attr.$observe("compileTemplate", function(newTemplateName) {
                $element.empty(), templateName = newTemplateName;
            }), $scope.$watch(function() {
                return templateName ? $templateCache.get(templateName) : void 0;
            }, function(html) {
                if ($element.empty(), html && html.length) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                }
            }, !0);
        }
    };
}), angular.module("fluro").run([ "$templateCache", function($templateCache) {
    "use strict";
    $templateCache.put("admin-content-browser/admin-content-browser.html", '<div class="content-browser application-panel"><div class=application-header><div class=type-icon ng-if=type><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1>{{title}} <span class=text-muted ng-if=model.length>({{model.length}} selected)</span> <span class=text-muted ng-if=search.terms>// \'{{search.terms}}\'</span></h1></div><div class="pull-right text-right"><div class=form-inline><div class="form-group search"><search ng-model=search.terms autofocus></search></div><a class="btn btn-default" ng-click=refresh()><i class="fa fa-refresh"></i></a> <a class="btn btn-primary" ng-click=close()>Done</a></div></div></div><div class=application-body><div class=application-columns><div class=filter-list><filter-block ng-model=search.filters persist=true ng-source=items ng-filter-key="\'status\'" ng-filter-title="\'Status\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'_type\'" ng-filter-title="\'Type\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'definition\'" ng-filter-title="\'Definition\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block><filter-block ng-model=search.filters ng-if=params.searchInheritable ng-source=filteredItems ng-filter-key="\'account\'" ng-filter-title="\'Account\'"></filter-block></div><div class=list-column><div class=application-panel><div class=application-body><div class=table-responsive ng-if=filteredItems.length><table class="table table-striped"><thead><tr><th class=select>Select</th><th class=select></th><th class=sortable ng-class="{active:search.order == \'title\'}" ng-click="setOrder(\'title\'); setReverse(!search.reverse)">Title <i class=fa ng-class="{\'fa-caret-down\':search.reverse, \'fa-caret-up\':!search.reverse}"></i></th><th class=sortable ng-class="{active:search.order == \'updated\'}" ng-click="setOrder(\'updated\'); setReverse(!search.reverse)">Updated <i class=fa ng-class="{\'fa-caret-down\':search.reverse, \'fa-caret-up\':!search.reverse}"></i></th><th class=sortable ng-if=params.searchInheritable ng-class="{active:search.order == \'account\'}" ng-click="setOrder(\'account\'); setReverse(!search.reverse)">Account <i class=fa ng-class="{\'fa-caret-down\':search.reverse, \'fa-caret-up\':!search.reverse}"></i></th></tr></thead><tbody><tr ng-click=toggle(item) ng-class="{\'active\':isSelected(item)}" class=table-row ng-repeat="item in pageItems"><td class=select><a class="btn btn-checkbox" ng-class="{\'active\':isSelected(item)}"><i class="fa fa-check"></i></a></td><td ng-if="item._type == \'image\'"><img preload-image ng-src="{{$root.getThumbnailUrl(item._id)}}"></td><td ng-if="item._type != \'image\'"><i class="fa fa-{{item._type}}"></i></td><td><span ng-if="item._type != \'contact\'">{{item.title}}</span> <span ng-if="item._type == \'contact\'">{{item.title | capitalizename}}</span> <span class=text-muted ng-if="item._type == \'event\'">// {{item.startDate | formatDate:\'g:ia - j M Y\'}}</span> <span class=text-muted ng-if="item._type == \'family\'">{{item.firstLine}}</span><div class="text-muted small" ng-if="item._type == \'policy\'">{{item.description}}</div><div class=text-muted ng-if="item._type == \'plan\'"><span ng-if=item.event>{{item.event.title}} -&nbsp;</span><em>{{item.startDate | formatDate:\'g:ia - j M Y\'}}</em></div></td><td>{{item.updated | timeago}}</td><td ng-if=params.searchInheritable>{{item.account.title}}</td></tr></tbody></table></div></div><div ng-if="filteredItems.length > pager.limit" class="application-footer text-center"><pagination max-size=pager.maxSize items-per-page=pager.limit total-items=filteredItems.length ng-model=pager.currentPage ng-change=updatePage() boundary-links=true></pagination></div></div></div></div></div></div>'), 
    $templateCache.put("admin-content-filter-block/admin-content-filter-block.html", '<div><div class="panel panel-filters" ng-class={expanded:expanded} ng-if="count || filtersUsed()"><div class=panel-heading ng-click="expanded = !expanded"><h3 class=panel-title><i class="fa pull-right visible-xs" ng-class="{\'fa-chevron-up\':expanded, \'fa-chevron-down\':!expanded}"></i> {{filterTitle}}</h3></div><div class=panel-expand-body><ul class=list-group><li ng-click=removeFilter() class=list-group-item ng-if=filtersUsed()>All</li><li ng-click=toggleFilter(option.key) ng-class={active:filterIsActive(option.key)} class=list-group-item ng-repeat="option in filterOptions"><span class=badge>{{option.count}}</span> {{option.title}}</li></ul></div></div></div>'), 
    $templateCache.put("admin-content-select/admin-content-select.html", '<div class=content-select><div class="content-list list-group" ng-class="{\'has-items\':selectedItems.items.length}" as-sortable=sortableOptions ng-model=selectedItems.items><div class="list-group-item clearfix" as-sortable-item ng-repeat="item in selectedItems.items"><div class=pull-left><handle as-sortable-item-handle ng-if=isSortable()></handle><img ng-if="item._type == \'image\'" ng-src="{{asset.thumbnailUrl(item._id)}}"> <i ng-if="item._type != \'image\'" class="fa fa-{{item._type}}"></i> <span ng-if="item._type == \'contact\'">{{item.title | capitalizename}}</span> <span ng-if="item._type != \'contact\'">{{item.title}}</span> <span ng-if="item._type == \'event\' || item._type == \'plan\'" class="small text-muted">// {{item.startDate | formatDate:\'jS F Y - g:ia\'}}</span></div><div class="actions pull-right btn-group"><a class="btn btn-tiny btn-xs" ng-if="item.assetType == \'upload\'" target=_blank ng-href={{asset.getUrl(item._id)}}><i class="fa fa-link"></i></a><a class="btn btn-tiny btn-xs" ng-if="!canEdit(item) && canView(item)" ng-click=view(item)><i class="fa fa-link"></i></a><a class="btn btn-tiny btn-xs" ng-if=canEditAdmin(item) ng-click=editInAdmin(item)><i class="fa fa-external-link"></i></a><a class="btn btn-tiny btn-xs" ng-if=canEdit(item) ng-click=edit(item)><i class="fa fa-pencil"></i></a><a class="btn btn-tiny btn-xs" ng-click=remove(item)><i class="fa fa-times"></i></a></div></div></div><script type=text/ng-template id=content-select-option.html><a class="clearfix">\n		\n			<i class="fa fa-{{match.model._type}}" ng-if="match.model._type != \'image\'" ></i>\n			<img ng-src="{{$parent.$parent.$parent.$parent.asset.imageUrl(match.model._id, 30, 30)}}" ng-if="match.model._type == \'image\'">\n			<span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span>\n			<span ng-if="match.model._type == \'event\' || match.model._type == \'plan\'" class="small text-muted">// {{match.model.startDate | formatDate:\'jS F Y - g:ia\'}}</span>\n		</a></script><div class=form-group ng-if=addEnabled()><div class=row><div class=col-md-7><div class=input-group><input class=form-control ng-model=proposed.value typeahead-template-url=content-select-option.html typeahead-on-select=add($item) placeholder={{searchPlaceholder}} typeahead="item.title for item in getOptions($viewValue)" typeahead-loading="loadingLocations"> <span class=input-group-addon ng-if=!loadingLocations><i class="fa fa-search"></i></span> <span class=input-group-addon ng-if=loadingLocations><i class="fa fa-spinner fa-spin"></i></span></div></div><div class=col-md-5><div class="btn-group btn-group-justified"><a class="btn btn-primary" ng-if="createEnabled() && type" ng-click=create()><span>Create</span><i class="fa fa-plus"></i></a><a class="btn btn-primary" popover-placement=bottom popover-template=dynamicPopover.templateUrl ng-if="createEnabled() && !type"><span>Create</span><i class="fa fa-plus"></i></a><a class="btn btn-default" ng-if=!hideBrowse ng-click=browse()><span>Browse</span><i class="fa fa-bars"></i></a></div></div></div></div></div>'), 
    $templateCache.put("admin-content-select/new-content-select-item.html", '<a class=clearfix><i class="fa fa-{{match.model._type}}" ng-if="match.model._type != \'image\'"></i> <img ng-src="{{$parent.$parent.$parent.$parent.asset.imageUrl(match.model._id, 30, 30)}}" ng-if="match.model._type == \'image\'"> <span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span> <span ng-if="match.model._type == \'event\' || match.model._type == \'plan\'" class="small text-muted">// {{match.model.startDate | formatDate:\'jS F Y - g:ia\'}}</span></a>'), 
    $templateCache.put("admin-content-select/new-content-select.html", '<div class=content-select><div class="content-list list-group" ng-class="{\'has-items\':selectedItems.items.length}" as-sortable=sortableOptions ng-model=selectedItems.items><div class="list-group-item clearfix" as-sortable-item ng-repeat="item in selectedItems.items"><div class=pull-left><handle as-sortable-item-handle ng-if=isSortable()></handle><img ng-if="item._type == \'image\'" ng-src="{{asset.thumbnailUrl(item._id)}}"> <i ng-if="item._type != \'image\'" class="fa fa-{{item._type}}"></i> <span>{{item.title}}</span> <span ng-if="item._type == \'event\' || item._type == \'plan\'" class="small text-muted">// {{item.startDate | formatDate:\'jS F Y - g:ia\'}}</span></div><div class="actions pull-right btn-group"><a class="btn btn-tiny btn-xs" ng-if="item.assetType == \'upload\'" target=_blank ng-href={{asset.getUrl(item._id)}}><i class="fa fa-link"></i></a><a class="btn btn-tiny btn-xs" ng-if=canEdit(item) ng-click=edit(item)><i class="fa fa-pencil"></i></a><a class="btn btn-tiny btn-xs" ng-click=remove(item)><i class="fa fa-times"></i></a></div></div></div><div class=form-group ng-if=addEnabled()><div class=row><div class=col-md-7><div class=input-group><input class=form-control ng-model=proposed.value typeahead-template-url=admin-content-select/new-content-select-item.html typeahead-on-select=add($item) placeholder={{searchPlaceholder}} typeahead="item.title for item in getOptions($viewValue)" typeahead-loading="loadingLocations"> <span class=input-group-addon ng-if=!loadingLocations><i class="fa fa-search"></i></span> <span class=input-group-addon ng-if=loadingLocations><i class="fa fa-spinner fa-spin"></i></span></div></div><div class=col-md-5><div class="btn-group btn-group-justified"><a class="btn btn-primary" ng-if="createEnabled() && type" ng-click=create()><span>Create</span><i class="fa fa-plus"></i></a><a class="btn btn-default" popover-placement=bottom popover-template=dynamicPopover.templateUrl ng-if="createEnabled() && !type"><span>Create</span><i class="fa fa-plus"></i></a><a class="btn btn-default" ng-if=!hideBrowse ng-click=browse()><span>Browse</span><i class="fa fa-bars"></i></a></div></div></div></div></div>'), 
    $templateCache.put("admin-field-edit/admin-field-edit.html", '<div class=field-edit><tabset><tab heading=Field><div class=field-edit-body><div class=row><div class=col-sm-6><div class=form-group><label>Title</label><p class=help-block>The question title of this field</p><input class=form-control ng-model=model.title placeholder="Title"></div></div><div class=col-sm-6><div class=form-group><label>Key</label><p class=help-block>The unique key to store this field\'s data in the database.</p><input class=form-control ng-model=model.key data-ng-trim=false placeholder="Key"></div></div></div><div class=row ng-if="model.type != \'void\'"><div class=col-sm-6><div class=form-group><label>Description</label><p class=help-block>The description can be used to display some help text below this field to help users input data</p><input class=form-control ng-model=model.description placeholder="Description text"></div></div><div class=col-sm-6><div class=form-group><label>Placeholder</label><p class=help-block>Placeholder text inside the field to help the user with data entry</p><input class=form-control ng-model=model.placeholder placeholder="Placeholder"></div></div></div><div class=form-group><label>Data Type</label><p class=help-block>The allowed data type that can be entered into this field</p><select class=form-control ng-model=model.type><option value=string>String / Text</option><option value=date>Date</option><option value=number>Number</option><option value=integer>Integer</option><option value=float>Float</option><option value=boolean>Boolean</option><option value=email>Email Address</option><option value=url>URL</option><option value=reference>Content Reference</option><option value=void>Null / No Value</option></select></div><div class=form-group ng-if="model.type == \'boolean\'"><div class=checkbox><label><input type=checkbox ng-model="model.params.storeCopy"> Terms and Conditions</label><p class=help-block>Select whether to store a copy of the terms being agreed to on the content. <em class=text-muted>Useful for terms and conditions and contractual agreements</em></p></div><div ng-if=model.params.storeCopy><label>Conditions</label><p class=help-block>Write the terms the user is agreeing to when ticking this field</p><textarea class=form-control ng-model=model.params.storeData placeholder="Terms and Conditions"></textarea></div></div><div class=form-group ng-if="model.type == \'reference\'"><label>Reference type</label><p class=help-block>Restrict what kind of content can be referenced</p><select class=form-control ng-model=model.params.restrictType><option value="">Any</option><option value={{type.path}} ng-selected="{{model.params.restrictType == type.path}}" ng-repeat="type in types | orderBy:\'singular\'">{{type.singular}}</option><option value={{defined.definitionName}} ng-selected="{{model.params.restrictType == defined.definitionName}}" ng-repeat="defined in definedTypes | orderBy:\'title\'">{{defined.title}}</option></select></div><hr><div class=row ng-if="model.type != \'void\'"><div class=form-group ng-class="{\'col-sm-4\':model.directive == \'embedded\', \'col-sm-6\':model.directive != \'embedded\'}"><label>Minimum Values</label><input class=form-control ng-model=model.minimum placeholder="Minimum Values"><p class=help-block>Minimum amount of answers that can be provided for this field<br><em class=text-muted>Setting this to a number higher than 0 will make this field required</em></p></div><div class=form-group ng-class="{\'col-sm-4\':model.directive == \'embedded\', \'col-sm-6\':model.directive != \'embedded\'}"><label>Maximum Values</label><input class=form-control ng-model=model.maximum placeholder="Maximum Values"><p class=help-block>Maximum amount of answers that can be provided for this field <em>Usually 1</em></p></div><div class="form-group col-sm-4" ng-if="model.directive == \'embedded\'"><label>Ask count</label><input type=number class=form-control ng-model=model.askCount placeholder="Default Count"><p class=help-block>How many entries should be asked for by default</p></div></div><hr><div class=form-group ng-if=showOptions()><label>Select Options</label><div ng-switch=model.type><div ng-switch-when=reference><em ng-if=!model.allowedReferences.length>Select options will be loaded from the server dynamically</em> <em ng-if=model.allowedReferences.length>Select options will be restricted to the allowed references above</em></div><div ng-switch-default><p class=help-block>Options presented to the user</p><options-select ng-model=model.options></options-select></div></div></div><div class=row ng-if="model.type != \'embedded\'"><div class="form-group col-sm-6"><label>Input Type</label><p class=help-block>What kind of field will be presented to the user for data entry</p><select class=form-control ng-model=model.directive><option value=input>Text Input</option><option value=textarea>Text Area</option><option value=wysiwyg>WYSIWYG Text Area</option><option value=code>Code</option><option value=select>Select</option><option value=button-select>Multiple button Select</option><option value=order-select>Ordered Select</option><option value=search-select>Autocomplete Search</option><option value=dob-select ng-if="model.type == \'date\' || model.type == \'string\'">Age / Birthdate Select</option><option value=date-select ng-if="model.type == \'date\' || model.type == \'string\'">Date picker</option><option value=terms ng-if="model.type == \'boolean\'">Terms / Conditions Checkbox</option><option value=reference-select ng-if="model.type == \'reference\'">Reference Select</option><option value=embedded ng-if="model.type == \'reference\'">Embedded Form</option><option value=pathlink ng-if="model.type == \'reference\'">Path Link to embedded content</option><option value=custom>Custom</option><option value=value>Hidden Value</option></select></div><div class="form-group col-sm-6" ng-if="model.directive == \'code\'"><label>Code Syntax</label><select class=form-control ng-model=model.params.syntax><option value=json>JSON</option><option value=js>Javascript</option><option value=html>HTML</option><option value=css>CSS</option></select></div></div><div ng-if="model.directive == \'pathlink\'"><div class=form-group><label>Relative Path</label><p class=help-block>Enter the relative path from this field to the embedded field you wish to link to. <em>eg. \'^.^.contact\' would be two keys up then a field with a key of \'contact\'</em></p><input class=form-control placeholder="Eg. ^.^.key" ng-model="model.params.relativePath"></div></div><div class=form-group ng-if="model.directive == \'dob-select\'"><div class=checkbox><label><input type=checkbox ng-model="model.params.hideAge"> Hide \'Age\' input</label></div><div class=checkbox><label><input type=checkbox ng-model="model.params.hideDates"> Hide \'Date\' fields</label></div></div><div class=form-group ng-if="model.type != \'void\'"><label>Error Message</label><p class=help-block>Customise the message to display to the user if they enter invalid information</p><input class=form-control ng-model=model.errorMessage placeholder="Error message"></div></div></tab><tab heading=Advanced><div class=field-edit-body ng-if="model.type != \'reference\'"><div class=form-group ng-if="model.type != \'reference\'"><label>Default values</label><multi-value-field ng-model=model.defaultValues ng-minimum=model.minimum ng-maximum=model.maximum placeholder="Default value"></multi-value-field></div><div class=form-group ng-if="model.type != \'reference\'"><label>Allowed values</label><multi-value-field ng-model=model.allowedValues placeholder="Allowed value"></multi-value-field></div><div class=row ng-if=isNumberType(model.type)><div class=col-sm-6><div class=form-group><label>Minimum Number</label><p class=help-block>What is the smallest valid number that can be provided</p><input class=form-control type=number ng-model=model.params.minValue placeholder="Minimum Numeric Value"></div></div><div class=col-sm-6><div class=form-group><label>Maximum Number</label><p class=help-block>What is the largest valid number that can be provided</p><input class=form-control type=number ng-model=model.params.maxValue placeholder="Maximum Numeric Value"></div></div></div><hr><div class=form-group><div class=checkbox><label><input ng-model=model.params.disableWebform type="checkbox"> Disable field on public web forms</label><p class=help-block>Check this if you want this field to be excluded from public facing webforms</p></div></div><div ng-if="model.type == \'string\'"><hr><div class=form-group><div class=checkbox><label><input ng-model=model.params.disableSanitize type="checkbox"> Disable input sanitizing</label><p class=help-block>By default any dangerous tags will be stripped from user input, disable this if you want to allow dangerous tags to be submitted</p></div></div><div class=form-group ng-if=!model.params.disableSanitize><label>Allowed tags</label><p class=help-block>Specify each tag that you do not want to be stripped from this input</p><multi-value-field ng-model=model.params.allowedTags placeholder="Allowed Tag eg. (strong, br, a, h2)"></multi-value-field></div></div></div><div class=field-edit-body ng-if="model.type == \'reference\'"><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="model.params.searchInheritable"> Search Inheritable References</label><p class=help-block>Allow references to be made to inherited content</p></div></div><div class=form-group><label>Default references</label><content-select ng-params={type:model.params.restrictType} ng-model=model.defaultReferences></content-select></div><div class=form-group><label>Allowed references</label><content-select ng-params={type:model.params.restrictType} ng-model=model.allowedReferences></content-select></div><div class=form-group ng-if="model.directive == \'embedded\'"><label>Exclude Fields</label><p class=help-block>By default, fields from this embedded definition will be appended to the \'data\' key of this reference, if you want to exclude particular defined fields specify each key below</p><multi-value-field ng-model=model.params.excludeKeys placeholder="Exclude Field Keys"></multi-value-field></div><div class=form-group><label>Population</label><p class=help-block>Which fields need to be loaded when content of this definition is viewed</p><select class=form-control ng-model=model.params.population><option value="">Normal (Basic fields)</option><option value=full>Full (Entire object)</option><option value=additional>Additional (Includes basic fields)</option><option value=custom>Custom</option></select></div><div class=form-group ng-if="model.params.population == \'additional\' || model.params.population == \'custom\'"><label>Populate Details</label><p class=help-block>Seperate each field with a space</p><input class=form-control ng-model="model.params.populationDetails"></div><div class=form-group><label>Query references</label><p class=help-block>Show results from a specified query as selectable options</p><content-select ng-params="{type:\'query\', maximum:1}" ng-model=model.sourceQuery></content-select></div><div class=form-group><label>Template</label><p class=help-block>Use a template to reformat the query results</p><content-select ng-params="{type:\'code\', maximum:1}" ng-model=model.queryTemplate></content-select></div></div></tab><tab heading="Custom Template" ng-if="model.directive == \'custom\'"><div class=field-edit-body><div class=form-group><label>Template</label><p class=help-block>Use HTML to customise your own for this field. <a href=http://angular-formly.com/#/example/custom-types/custom-templates>More information</a></p><div><strong>You have access to the following variables in your template:</strong><br><ul><li><strong>options</strong><br>The data provided to configure the field. As a template author, if you need custom properties, you use the templateOptions property on this object. Formly will add a scope variable called to which is the equivalent to options.templateOptions for convenience. Note, there are a few things that get added to your field configuration (and hence, these options). One of these is "formControl" which can be used to display errors to the user (for example).</li><li><strong>id</strong><br>the id of the field, use this for the name of your ng-model</li><li><strong>index</strong><br>The index of the field the form is on (in ng-repeat)</li><li><strong>form</strong><br>the form controller the field is in</li><li><strong>model</strong><br>the model of the form (or the model specified by the field if it was specified).</li><li><strong>fields</strong><br>all the fields for the form</li></ul><p class=text-muted>to set the value of this field use <em>model[options.key] = \'your value\'</em></p></div></div><div class=code-editor ng-model=model.template ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div></div></tab><tab heading=Theme><div class=field-edit-body><div class=form-group><label>Classes</label><p class=help-block>Classes to attach to this group when rendering the form</p><input class=form-control ng-model=model.className placeholder="Class"></div></div></tab><tab heading=Expressions><div class=field-edit-body><div class=form-group><label>Expression Properties</label><p class=help-block>Expressions are used to alter this field according to changes in other fields<br><ul><li><em class=text-muted>\'value\', \'defaultValue\', \'hide\', \'required\'</em> are possible keys</li><li><em class=text-muted>\'model\'</em> is the immediate model for this field</li><li><em class=text-muted>\'interaction\'</em> is the root model for the submitted content</li></ul></p><key-value-select ng-model=model.expressions></key-value-select></div><div class=form-group style="display:none !important"><label>Hide Expression</label><p class=help-block>Expression that if true will hide this field <a class=text-muted target=_blank href=http://angular-formly.com/#/example/field-options/hide-fields>Click here for more information</a></p><input class=form-control ng-model=model.hideExpression placeholder="Hide Expression"></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model=model.disableValidation> Disable server side requirement for this field if not provided</label></div><p class=help-block>Check this box to disable server side validation if no value is submitted (sometimes this is helpful for optional fields)</p></div></div></tab></tabset></div>'), 
    $templateCache.put("admin-field-edit/admin-field-editor.html", '<div class=field-editor><script type=text/ng-template id=field_editor_node.html><div class="panel panel-accordion">\n			<div class="panel-heading clearfix" ng-class="{selected:node == selected.field, \'small text-muted\':node.type == \'void\'}" ng-click="selectField(node)">\n				<handle ui-tree-handle></handle>\n				<span tooltip-placement="bottom" tooltip="{{getFieldPath(node)}}">{{node.title}}</span>\n				<em class="text-muted" ng-if="node.type != \'group\'">\n					<span class="text-capitalize" ng-if="!node.params.disableWebform">{{node.type}}</span>\n					<span class="small text-muted" ng-if="node.params.disableWebform">Excluded</span>\n				</em> \n				<em class="text-muted" ng-if="node.type == \'group\'">{{node.fields.length}} fields</em>\n\n				<a ng-if="hasSubFields(node)" class="btn btn-default btn-xs pull-right" ng-click="node.collapsed = !node.collapsed">\n					<i class="fa" ng-class="{\'fa-angle-right\':!node.collapsed, \'fa-angle-down\':node.collapsed}"></i>\n				</a>\n				\n			</div>\n			<div class="panel-footer" ng-if="hasSubFields(node) && node.collapsed">\n				<ol ui-tree-nodes="" ng-model="node.fields">\n					<li ng-repeat="node in node.fields" ui-tree-node ng-include="\'field_editor_node.html\'"></li>\n				</ol>\n			</div>\n		</div></script><div class=field-editor-side-list><div class=field-editor-fields-wrapper><div class=field-editor-fields ui-tree><ol ui-tree-nodes="" ng-model=model id=tree-root><li ng-repeat="node in model" ui-tree-node ng-include="\'field_editor_node.html\'"></li></ol></div></div><div class=field-editor-fields-actions><div class="btn-group btn-group-justified"><a class="btn btn-primary" ng-click=create()><span>Add Field</span><i class="fa fa-plus"></i></a> <a class="btn btn-default" ng-if=selected.field ng-click=duplicate()><span>Duplicate</span><i class="fa fa-copy"></i></a> <a class="btn btn-default" ng-click=createGroup()><span>Add Group</span><i class="fa fa-folder-o"></i></a></div></div></div><div class=field-editor-main-options><div class="panel panel-default" ng-if=!selected.field><div class=panel-body><p class=help-block>Add / Select fields on the left to adjust configuration</p></div></div><div class="panel panel-default" ng-if=selected.field><div class="panel-heading clearfix"><div class=pull-left><h5 class=panel-header ng-if="selected.field.type != \'group\'"><i class="fa fa-cog"></i> Configuration</h5><h5 class=panel-header ng-if="selected.field.type == \'group\'"><i class="fa fa-folder-o"></i> Group Configuration</h5></div><a class="btn btn-default pull-right" ng-click=remove(selected.field)><span>Remove / Delete</span><i class="fa fa-times"></i></a></div><field-edit ng-model=selected.field ng-if="selected.field.type != \'group\'"></field-edit><field-group-edit ng-model=selected.field ng-if="selected.field.type == \'group\'"></field-group-edit></div></div></div>'), 
    $templateCache.put("admin-field-edit/admin-field-group-edit.html", '<div class=field-group-edit><tabset><tab heading="Group Details"><div class=field-edit-body><div class=row><div class=col-sm-4><div class=form-group><label>Group Title</label><input class=form-control ng-model=model.title placeholder="Title"></div></div><div class=col-sm-4><div class=form-group><label>Key</label><input class=form-control ng-model=model.key data-ng-trim=false placeholder="Key"></div></div><div class=col-sm-4><div class=form-group><label>Class</label><input class=form-control ng-model=model.className placeholder="Class"></div></div></div><hr><div class=form-group><div class=checkbox><label><input type=checkbox ng-model=model.asObject> Group as Sub Object</label><p class=help-block>Attach all fields in this group onto the group object instead of the form model</p></div></div><div ng-if=model.asObject><hr><div class=row><div class="form-group col-sm-4"><label>Minimum</label><input type=number class=form-control ng-model=model.minimum placeholder="Minimum"><p class=help-block>Minimum amount of entries for this group that can be created</p></div><div class="form-group col-sm-4"><label>Maximum</label><input type=number class=form-control ng-model=model.maximum placeholder="Maximum"><p class=help-block>Maximum amount of entries for this group that can be created</p></div><div class="form-group col-sm-4"><label>Ask count</label><input type=number class=form-control ng-model=model.askCount placeholder="Default Count"><p class=help-block>How many entries should be asked for by default</p></div></div><hr><div class=form-group><label>Create as reference</label><p class=help-block>Attempt to create a new content item from using the data submitted in this group</p><select class=form-control ng-model=model.asObjectType><option value="">Don\'t create item</option><option value=family ng-selected="{{model.asObjectType == \'family\'}}">Create a Family</option><option value=contact ng-selected="{{model.asObjectType == \'contact\'}}">Create a Contact</option></select></div></div></div></tab><tab heading=Advanced><div class=field-edit-body><div><div class=form-group><label>Hide Expression</label><p class=help-block>Expression that if returns true will hide this group and it\'s contents<ul><li><em class=text-muted>\'model\'</em> is the immediate model for this field</li><li><em class=text-muted>\'interaction\'</em> is the root model for this interaction</li></ul></p><div class=input-group><div class=input-group-addon>Hide if</div><input class=form-control ng-model=model.hideExpression placeholder="Hide Expression"></div></div></div><div class=form-group><label>Admin Display Style</label><p class=help-block>How should this data be displayed in the admin panel?</p><select class=form-control ng-model=model.adminDisplayStyle><option value="">Tabbed form</option><option value=inline>Inline fields</option><option value=panel>Panels / Fieldsets</option></select></div><div class=form-group><div class=checkbox><label><input ng-model=model.params.disableWebform type="checkbox"> Disable field on public web forms</label><p class=help-block>Check this if you want this group and all it\'s contained fields to be excluded from public facing webforms</p></div></div></div></tab></tabset></div>'), 
    $templateCache.put("admin-key-content-select/admin-key-content-select.html", '<div class=clearfix><div class=input-group ng-repeat="item in model"><div class=input-group-addon>{{item.key}}</div><content-select ng-model=item.content></content-select><div class=input-group-addon ng-click=remove(item)><i class="fa fa-times"></i></div></div><div class=row><div class=col-xs-5><input class=form-control ng-enter=create() ng-model=_new.key machinename placeholder="Key"></div><div class=col-xs-5><content-select ng-model=_new.content></content-select></div><div class=col-xs-2><a class="btn btn-primary" ng-click=create()><i class="fa fa-plus"></i></a></div></div></div>'), 
    $templateCache.put("admin-key-value-select/admin-key-value-select.html", '<div class=clearfix><div class=input-group ng-repeat="(key, value) in model"><div class=input-group-addon>{{key}}</div><input class=form-control ng-model=model[key] placeholder="Value"><div class=input-group-addon ng-click=remove(key)><i class="fa fa-times"></i></div></div><div class=row><div class=col-xs-5><input class=form-control ng-enter=create() ng-model=_new.key machinename placeholder="Key"></div><div class=col-xs-5><input class=form-control ng-enter=create() ng-model=_new.value placeholder="Value"></div><div class=col-xs-2><a class="btn btn-primary" ng-click=create()><i class="fa fa-plus"></i></a></div></div></div>'), 
    $templateCache.put("admin-multi-value-field/multi-value-field.html", '<div class=multi-value-field><div as-sortable="" ng-model=model><div class=clearfix as-sortable-item ng-repeat="(key, entry) in model"><div class=input-group><div class=input-group-addon as-sortable-item-handle><i class="fa fa-arrows"></i></div><input class=form-control ng-model=model[$index]><div class=input-group-addon ng-click=remove(entry)><i class="fa fa-times"></i></div></div></div></div><div class=input-group><input ng-enter=add(_proposed) class=form-control placeholder={{placeholder}} ng-model=_proposed><div class=input-group-addon ng-click=add(_proposed)><i class="fa fa-plus"></i></div></div></div>'), 
    $templateCache.put("admin-realm-select/admin-realm-popover.html", '<div><div class=form-group><div class=input-group><input class=form-control ng-model=search.terms placeholder="Search realms"> <span class=input-group-addon ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></span></div></div><div class=realm-select-list><div ng-if=tree.length><div class=realm-select-items ng-class="{\'has-selection\':model.length}"><realm-select-item ng-model=realm ng-search-terms=search.terms ng-selection=model ng-repeat="realm in tree | filter:search.terms | orderBy:\'title\' track by realm._id"></realm-select-item></div></div></div><ul class=list-group ng-if=!tree.length><li class="list-group-item disabled" ng-repeat="realm in model track by realm._id">{{realm.title}}</li></ul></div>'), 
    $templateCache.put("admin-realm-select/admin-realm-select-item.html", '<div class="realm-select-item expanded" ng-class="{\'active\':contains(realm)}"><div class=realm-select-item-link ng-class="{\'active\':contains(realm), \'active-trail\':activeTrail(realm)}"><a ng-click=toggle(realm)><i class="fa dot fa-circle" style="color: {{realm.bgColor}}"></i> {{realm.title}} <i class="fa fa-check tick"></i></a></div><div class=realm-select-children></div></div>'), 
    $templateCache.put("admin-realm-select/admin-realm-select.html", '<a popover-template=dynamicPopover.templateUrl style=width:200px popover-append-to-body=true popover-placement={{popoverDirection}} popover-title={{dynamicPopover.title}} class="btn btn-default btn-realm-select"><span class=circles><i class="fa fa-circle" ng-repeat="realm in model | limitTo:5 track by realm._id " style=color:{{realm.bgColor}}></i></span> <span ng-show=!model.length>Select Realms</span> <span ng-show="model.length == 1">{{model[0].title}}</span> <span ng-show="model.length > 1 && model.length < 4">{{model | comma:\'title\'}}</span> <span ng-show="model.length > 3">{{model.length}} Realms</span> <i ng-show=!model.length class="fa fa-realm"></i></a>'), 
    $templateCache.put("admin-search/search.html", "<div class=input-group><input class=form-control ng-model=model placeholder=\"Search\"> <span class=input-group-addon ng-click=clear()><i class=fa ng-class=\"{'fa-search':!model.length, 'fa-times':model.length}\"></i></span></div>"), 
    $templateCache.put("admin-tag-select/admin-tag-popover.html", '<div class=tag-select><script type=text/ng-template id=tag-list-item.html><a class="clearfix">\n			<i class="fa fa-tag"></i>\n			<span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span>\n		</a></script><div class=form-group><div class=input-group><input class=form-control ng-model=proposed.value typeahead-template-url=tag-list-item.html typeahead-on-select=add($item) placeholder="Type to add tags" typeahead="tag.title for tag in getTags($viewValue)" typeahead-loading="loadingLocations"> <span class=input-group-addon ng-if="!loadingLocations && !proposed.value"><i class="fa fa-search"></i></span> <span class=input-group-addon ng-if=loadingLocations><i class="fa fa-spinner fa-spin"></i></span> <span class=input-group-addon ng-click=create() ng-if="!loadingLocations && proposed.value">Create <i class="fa fa-plus"></i></span></div></div><div class=tags ng-if=model.length><span class=inline-tag ng-click=removeTag(tag) ng-repeat="tag in model"><i class="fa fa-tag on"></i><i class="fa fa-times off"></i>{{tag.title}}</span></div></div>'), 
    $templateCache.put("admin-tag-select/admin-tag-select.html", '<a popover-template=dynamicPopover.templateUrl popover-append-to-body=true popover-placement=bottom popover-title={{dynamicPopover.title}} class="btn btn-default"><i class="fa fa-tag"></i></a>'), 
    $templateCache.put("application/application.html", ""), $templateCache.put("fluro-admin-content/content-formly/code.html", "<div id={{options.id}}><div class=code-editor ng-model=model[options.key] ui-ace=\"{\n		onLoad: aceLoaded,\n		onChange: aceChanged,\n		useWrapMode : false, \n		showGutter: true, \n		theme:'tomorrow_night_eighties'}\"></div></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/date-select.html", "<div id={{options.id}} class=date-select-field><dateselect ng-label=options.label use-time=true ng-model=model[options.key]></dateselect></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/form.html", "<div><formly-form model=vm.model fields=vm.fields form=vm.form options=vm.options></formly-form></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/list-select.html", '<div id={{options.id}} class=list-select><ul class=list-group ng-model=model[options.key]><li class=list-group-item ng-class={active:contains(option.value)} id="{{id + \'_\'+ $index}}" ng-click=toggle(option.value) ng-repeat="(key, option) in to.options"><i class="fa right" ng-class="{\'fa-toggle-off\':contains(object), \'fa-toggle-on\':!contains(object)}"></i> <span>{{option.name}}</span></li></ul></div>'), 
    $templateCache.put("fluro-admin-content/content-formly/nested.html", '<div><div ng-switch=to.definition.adminDisplayStyle><div class=inline-group ng-class=to.definition.className ng-switch-when=inline><div class="panel panel-default"><div class=panel-heading><h5>{{to.label}} <em class=text-muted>({{model[options.key].length}})</em></h5></div><div class=nested-group-item ng-repeat="entry in model[options.key] track by $index"><div class=row><div class=col-sm-10><div ng-class=to.definition.className><formly-form model=entry fields=options.data.fields></formly-form></div></div><div class=col-sm-2><a ng-if=canRemove() class="btn btn-danger btn-block btn-sm" ng-click="model[options.key].splice($index, 1)"><span>Remove</span><i class="fa fa-times"></i></a></div></div></div><div class="panel-footer clearfix"><a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=model[options.key].push({active:true})><span>Add {{to.label}}</span><i class="fa fa-plus"></i></a></div></div></div><div ng-switch-default ng-class=to.definition.className><div ng-if="to.definition.maximum == 1 && options.key" class="panel panel-default"><div class=panel-heading><h5>{{to.label}}</h5></div><div class=panel-body><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div><div class=form-multi-group ng-if="to.definition.maximum != 1"><div class="panel panel-default"><div class="panel-heading clearfix"><h6 class="panel-title pull-left">{{to.label}}</h6><div class=pull-right><a class="btn btn-default btn-sm" ng-class={active:to.orderMode} ng-click="to.orderMode = !to.orderMode"><i class="fa fa-list-ol"></i></a> <a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=model[options.key].push({active:true})><span>Add another {{to.label}}</span><i class="fa fa-plus"></i></a></div></div><div class=panel-body ng-if=to.orderMode><div ui-tree><ul class=list-group ui-tree-nodes ng-model=model[options.key]><li ui-tree-node class=list-group-item ng-repeat="item in model[options.key]"><handle ui-tree-handle></handle><a class="pull-right btn-default btn-xs" ng-click=remove(item)><i class="fa fa-times"></i></a> <strong ng-if=item.title>{{item.title}}</strong> <strong ng-if=!item.title>{{to.label}} {{$index + 1}}</strong><div class=text-muted>{{firstLine(item)}}</div></li></ul></div></div><div ng-if=!to.orderMode><ul class="nav nav-tabs"><li ng-repeat="entry in model[options.key] track by $index"><a ng-click="settings.active = entry"><span ng-if=entry.title>{{entry.title}}</span> <span ng-if=!entry.title>{{to.label}} {{$index + 1}}</span></a></li></ul><div class=tab-content><div ng-if="entry == settings.active" class="tab-pane active" ng-repeat="entry in model[options.key] track by $index"><div class=panel-body><formly-form model=entry fields=options.data.fields></formly-form></div><div class="panel-footer clearfix"><a ng-if=canRemove() class="btn btn-danger btn-sm pull-right" ng-click=remove(entry)><span ng-if=entry.title>Remove {{entry.title}}</span> <span ng-if=!entry.title>Remove {{to.label}} {{$index + 1}}</span> <i class="fa fa-times"></i></a></div></div></div></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/content-formly/object-editor.html", "<div id={{options.id}} class=object-editor-field><pre>{{model[options.key] | json}}</pre></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/reference-select.html", "<div id={{options.id}} class=content-select-field><new-content-select ng-model=model[options.key] config=config></new-content-select></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/terms.html", '<div><div class="terms small well">{{options.data.agreements[options.key]}}</div><div class=checkbox><label><input ng-model=model[options.key] type="checkbox"> {{to.label}}</label></div></div>'), 
    $templateCache.put("fluro-admin-content/content-formly/unknown.html", "<div class=form-control ng-if=\"model[options.key] && to.definition.type != 'void'\"><label>{{to.label}}</label><pre>{{ model[options.key] | json}}</pre></div>"), 
    $templateCache.put("fluro-admin-content/content-formly/wysiwyg.html", '<div id={{options.id}} class=standard-body-wrapper><div class=standard-body-wrapper><div class="article-toolbar cf"><div id=toolbar-sub-{{options.key}}></div></div><div class=article-body><textarea class=article-body redactor="{toolbarExternal:\'#toolbar-sub-\' + options.key }" ng-model=model[options.key]></textarea></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/account/form.html", '<div class="account-form form"><div class="flex-content page-content"><div class=form-group><label>Account Name</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Parent Accounts</label><p class=help-block>Inherit content definitions from these parent accounts</p><new-content-select ng-model=item.parentAccounts config="{type:\'account\', canCreate:false}"></new-content-select></div><div ng-if=!item._id><div class=form-group><label>Create a user</label><input type=checkbox class=form-control ng-model=item.createNewUser></div><div ng-if=item.createNewUser><div class=form-group><label>User Name</label><input required class=form-control ng-model=item.name placeholder=Username></div><div class=form-group><label>Email Address</label><input required class=form-control ng-model=item.email placeholder="Email Address"></div><div class=row><div class=col-md-6><div class=form-group><label>Password</label><input required type=password class=form-control ng-model=item.password placeholder=Password></div></div><div class=col-md-6><div class=form-group><label>Confirm Password</label><input required type=password class=form-control ng-model=item.confirmPassword placeholder="Confirm Password"></div></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/account/list.html", '<div class=application-panel><div class=application-header><div class=type-icon><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1>{{titlePlural}} <span class=text-muted ng-if=search.terms>// \'{{search.terms}}\'</span></h1></div><div class="pull-right text-right"><div class=form-inline><div class=btn-group ng-if=type.viewModes.length><a class="btn btn-default" ng-class="{active:currentViewMode(\'default\')}" ng-click="settings.viewMode = \'default\'"><i class="fa fa-bars"></i></a><a class="btn btn-default" ng-class={active:currentViewMode(viewMode)} ng-click="settings.viewMode = viewMode" ng-repeat="viewMode in type.viewModes"><i class="fa fa-{{viewMode}}"></i></a></div><div class=btn-group ng-if=canCreate()><a class="btn btn-primary" ng-if=uploadType ui-sref=upload>Upload {{titlePlural}}<i class="fa fa-cloud-upload"></i></a> <a class="btn btn-primary" ui-sref={{type.path}}.create({definitionName:definition.definitionName})><span>Create {{titleSingular}}</span><i class="fa fa-plus"></i></a></div><div class=form-group><search ng-model=search.terms></search></div></div></div></div><div class=application-body><div class=application-columns><div class=definition-list ng-if=definitions.length><ul class="nav nav-sidebar nav-stacked"><li ui-sref-active=active><a ui-sref={{type.path}}.default>{{type.plural}}</a></li><li ui-sref-active=active ng-repeat="def in definitions | orderBy:\'plural\'"><a ui-sref="{{type.path}}.custom({definition:\'{{def.definitionName}}\'})">{{def.plural}}</a></li></ul></div><div class=filter-list ng-if="filteredItems.length || filtersActive()"><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'status\'" ng-filter-title="\'Status\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in definition.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in type.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'author\'" ng-filter-title="\'Author\'"></filter-block></div><div class=list-column><div class=application-panel><select-toolbar ng-model=selection ng-type=type ng-definitions=definitions ng-definition=definition ng-if=selection.items.length ng-plural=titlePlural></select-toolbar><empty-text ng-if=!filteredItems.length></empty-text><div class=application-body ng-if=filteredItems.length ng-switch=settings.viewMode><account-list-view ng-switch-default></account-list-view></div><div ng-if="filteredItems.length > pager.limit" class="application-footer text-center"><pagination max-size=pager.maxSize items-per-page=pager.limit total-items=filteredItems.length ng-model=pager.currentPage ng-change=updatePage()></pagination></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/account/view.html", "<div class=account-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/application/form.html", '<div class="application-form form"><tabset class=flex-tabs><tab heading="Basic Info"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Domain Name</label><p class=help-block>Add domain name for this application <em class=text-muted>(without http://)</em></p><input class=form-control ng-model=item.domain placeholder="Domain Name"></div><div class=form-group><label>Forwarding Domains</label><p class=help-block>Add domains that should redirect to the primary domain name</p><multi-value-field ng-model=item.forwards placeholder="Forwarding Domain URL"></multi-value-field></div><div class=form-group><label>Application Type</label><p class=help-block>Quick select existing applications</p><select class=form-control ng-model=item.applicationType><option value=webrender>Website Renderer</option><option value=slipstream>Slipstream</option><option value=admin>Admin Panel</option></select></div><div class=form-group><label>Application Type</label><p class=help-block>For other applications enter your application type below</p><input ng-model=item.applicationType class="form-control"></div></div></tab><tab heading=Authentication><div class=page-content><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.requireLogin"> <strong>Require User Login</strong></label><p class=help-block>Check this box to only allow access to registered users</p></div></div><div class=form-group ng-if=item.requireLogin><div class=checkbox><label><input type=checkbox ng-model="item.lockAccount"> <strong>Lock Account</strong></label><p class=help-block>Only allow users with a persona in this account to access this application</p></div></div><div class=form-group ng-if=item.requireLogin><div class=checkbox><label><input type=checkbox ng-model="item.lockRole"> <strong>Lock Role</strong></label><p class=help-block>Only allow users with roles that have specified access to this application</p></div></div><hr><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.forceSSL"> <strong>Force SSL Encryption</strong></label><p class=help-block>Check this box to redirect all requests to https://</p></div></div><div class=form-group><label>Authentication Style</label><select class=form-control ng-model=item.authenticationStyle><option value=client>Client Session</option><option value=global>Global Fluro User</option><option value=application>Application</option></select></div><div ng-switch=item.authenticationStyle><p class=help-block ng-switch-when=global>The application will access the API as the logged in user and assume all permissions, realms and context from the user.</p><p class=help-block ng-switch-when=client>The application will access the API as the logged in user but assume the context of this application.</p><p class=help-block ng-switch-default>The application will access the API using the permissions and realms specified in the next \'Permission Set\' tab.</p></div><div class="panel panel-default" ng-if="item.authenticationStyle == \'application\'"><div class=panel-body><div class=form-group><label>Application API Key</label><p class=help-block>Use this key when requesting API endpoints to authenticate as this application</p><input class=form-control ng-model=item.apikey readonly></div></div></div><div class="panel panel-default" ng-if="item.authenticationStyle == \'application\'"><div class=panel-body><div class=form-group ng-if=!item.whiteList.length><label>Blacklist IPs</label><p class=help-block>IP addresses on this list will be blocked from using this application access token.</p><multi-value-field ng-model=item.blackList placeholder=0.0.0.0></multi-value-field></div><div class=form-group ng-if=!item.blackList.length><label>Whitelist IPs</label><p class=help-block>IP addresses that are not included in this list will be blocked from using this application access token.</p><multi-value-field ng-model=item.whiteList placeholder=0.0.0.0></multi-value-field></div></div></div></div></tab><tab heading="Permission Sets" ng-if="item.authenticationStyle == \'application\'"><div class=page-content><permission-set-manager ng-model=item.permissionSets></permission-set-manager></div></tab><tab heading=Data><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly><div class="panel panel-default"><div class=panel-heading><h5 class=panel-title>Configuration</h5><p class=help-block>Add extra configuration options for this application</p></div><div class=panel-body><json-editor config=item.data.configuration></json-editor><pre>{{item.data.configuration | json}}</pre></div></div></div></tab><tab heading=Advanced><div class=page-content><div class=form-group><label>API URL</label><p class=help-block>Specify which API URL this application should use. If left blank, the default fluro api url will be used.</p><input class=form-control placeholder=https://apiv2.fluro.io ng-model=item.apipath></div><div class=form-group><label>Timezone</label><p class=help-block>Which timezone should be used for this application</p><select ng-model=item.timezone class=form-control><option ng-selected="{{item.timezone == \'\'}}" value="">Users Timezone</option><option ng-selected="{{item.timezone == timezone}}" value={{timezone}} ng-repeat="timezone in timezones">{{timezone}}</option></select></div><div class=form-group><label>Favicon</label><p class=help-block>Choose an image to use for the favicon<br><em class=text-muted>Recommend a 1:1 ratio and at least 1024 x 1024px</em></p><content-select ng-model=item.icon ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div><div class=form-group><label>Application Icon</label><p class=help-block>Choose an image to use for the dashboard shortcut<br><em class=text-muted>Will default to the favicon if none provided. Recommend a 1:1 ratio and at least 1024 x 1024px</em></p><content-select ng-model=item.applicationIcon ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and access this application</p></div></div></div></tab><tab heading=Redirects><div class=page-content><div class=form-group><label>Path forwarding / Redirects</label><redirect-select ng-model=item.pathRedirects></redirect-select></div></div></tab><tab heading=Analytics><div class=page-content><div class=form-group><label>Google Analytics ID</label><p class=help>Paste your GA tracking code to enable google analytics to track this site</p><input class=form-control ng-model=item.data.gaTrackingCode placeholder="GA Tracking Code"></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/application/view.html", "<div class=application-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/article/form.html", '<div class="article-form form"><div class=flex-article ng-if=!definition><div class="article-toolbar cf"><div id=article-toolbar></div></div><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div><tabset ng-if="definition && !definition.data.hideBody" class=flex-tabs><tab heading={{bodyLabel}}><div class=flex-article><div class="article-toolbar cf"><div id=article-toolbar></div></div><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></tab><tab heading="{{definition.title}} information" ng-if=definition><div class=page-content><div class=form-group ng-if="definition.data.titleGeneration != \'force\'"><label>{{titleLabel}}</label><input class=form-control placeholder={{titleLabel}} ng-model=item.title></div><content-formly ng-model=item.data definition=definition></content-formly></div></tab></tabset><div class=page-content ng-if="definition && definition.data.hideBody"><div class=form-group ng-if="definition.data.titleGeneration != \'force\'"><label>{{titleLabel}}</label><input class=form-control placeholder={{titleLabel}} ng-model=item.title></div><content-formly ng-model=item.data definition=definition></content-formly></div></div>'), 
    $templateCache.put("fluro-admin-content/types/article/view.html", "<div class=article-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/asset/asset-replace-form.html", '<div><div ng-if=item._id><div class=form-group><label>Asset File</label><div class=clearfix><div class=col-md-6><span><i class="fa fa-asset"></i> {{item.filename}}</span> <a class="btn btn-default" ng-click=showReplaceDialog() ng-if=!replaceDialogShowing>Replace</a></div></div></div><div class=form-group ng-if=replaceDialogShowing><label>Replace with new file</label><div class=upload-progress ng-if=uploader.isUploading><div class=bar-preloader><div class=preloader-progress ng-style="{ \'width\': uploader.progress + \'%\' }"></div><span class=preloader-text>{{uploader.progress}}%</span></div></div><div class=row ng-if=!uploader.isUploading><div class=col-md-6><input class=form-control type=file nv-file-select="" uploader="uploader"></div><div class=col-md-6><div class="btn btn-primary" ng-click=uploader.uploadAll() ng-disabled=!uploader.getNotUploadedItems().length><span>Upload</span><i class="fa fa-cloud-upload"></i></div></div></div></div></div><div class=form-group ng-if=!item._id><label>Upload file</label><div class=upload-progress ng-if=uploader.isUploading><div class=bar-preloader><div class=preloader-progress ng-style="{ \'width\': uploader.progress + \'%\' }"></div><span class=preloader-text>{{uploader.progress}}%</span></div></div><div class=clearfix ng-if=!uploader.queue.length><input class=form-control type=file nv-file-select="" uploader="uploader"></div><div class=clearfix ng-if="!uploader.isUploading && uploader.queue.length"><span>{{uploader.queue[0].file.name}}</span> <a class="btn btn-default" ng-click=uploader.clearQueue()>Change</a></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/asset/form.html", '<div class="asset-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder="Filename will be used if left blank" ng-model=item.title></div><asset-replace-form></asset-replace-form><content-formly ng-model=item.data definition=definition></content-formly><div class=standard-body-wrapper><label>Body</label><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></div><div class=form-group><div class="privacy-select btn-group btn-group-justified"><a class="btn btn-privacy" ng-click="item.privacy = \'secure\'" ng-class="{\'active\':item.privacy == \'secure\'}"><i class="fa fa-lock"></i> <strong>Secure</strong><br><em class=small>Only users and applications with correct permissions can view</em></a> <a class="btn btn-privacy" ng-click="item.privacy = \'public\'" ng-class="{\'active\':item.privacy == \'public\'}"><i class="fa fa-globe"></i> <strong>Public</strong><br><em class=small>Anyone in the world can view</em></a></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/asset/view.html", '<div class=asset-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div><a class="btn btn-default btn-block" target=_blank ng-if="item.assetType == \'upload\'" ng-href={{getDownloadUrl(item._id)}}><span>Download</span><i class="fa fa-download"></i></a></div></div>'), 
    $templateCache.put("fluro-admin-content/types/attendance/form.html", '<div class="attendance-form form"><div class=page-content><div class=form-group ng-if="definition.data.titleGeneration != \'force\'"><label>{{titleLabel}}</label><input class=form-control placeholder={{titleLabel}} ng-model=item.title></div><div class=form-group><label>Event</label><content-select ng-params="{type:\'event\', canCreate:false, minimum:1, maximum:1}" ng-model=item.event></content-select></div><div class=form-group><label>Count</label><input type=number class=form-control pattern=[0-9]* select-on-focus placeholder=Count ng-model=item.count inputmode=numeric></div><content-formly ng-model=item.data definition=definition></content-formly></div></div>'), 
    $templateCache.put("fluro-admin-content/types/attendance/view.html", "<div class=article-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/audio/form.html", '<div class="audio-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder="Filename will be used if left blank" ng-model=item.title></div><div ng-if=!item.assetType class=form-group><label>Asset Type</label><select class=form-control ng-model=item.assetType><option value=upload>Upload / Hosted</option><option value=soundcloud>Soundcloud</option></select></div><div ng-switch=item.assetType><div ng-switch-when=upload><div ng-if=item._id class="audio-element form-group"><div class=audio-wrapper><audio controls><source ng-src="{{getAssetUrl(item._id) | trustedResource}}" type="{{item.mimetype}}"></audio></div></div><asset-replace-form></asset-replace-form></div><div ng-switch-when=soundcloud class=row><div class=col-md-6><div class=form-group><label>Soundcloud URL</label><input class=form-control ng-model="item.external.soundcloud"></div><div class=audio-wrapper><div class=video-wrapper><soundcloud-audio track-url=item.external.soundcloud></soundcloud-audio></div></div></div></div></div><content-formly ng-model=item.data definition=definition></content-formly><div class=standard-body-wrapper><label>Body</label><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></div><div class=form-group><div class="privacy-select btn-group btn-group-justified"><a class="btn btn-privacy" ng-click="item.privacy = \'secure\'" ng-class="{\'active\':item.privacy == \'secure\'}"><i class="fa fa-lock"></i> <strong>Secure</strong><br><em class=small>Only users and applications with correct permissions can view</em></a> <a class="btn btn-privacy" ng-click="item.privacy = \'public\'" ng-class="{\'active\':item.privacy == \'public\'}"><i class="fa fa-globe"></i> <strong>Public</strong><br><em class=small>Anyone in the world can view</em></a></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/audio/view.html", '<div class=audio-view><div class=page-content><div ng-switch=item.assetType><div ng-switch-when=upload><div ng-if=item._id class="audio-element form-group"><div class=audio-wrapper><audio controls><source ng-src="{{$root.getUrl(item._id) | trustedResource}}" type="{{item.mimetype}}"></audio></div></div></div><div ng-switch-when=soundcloud><div class=audio-wrapper></div></div></div><div view-extended-fields></div><div class=form-group><strong>Security / Privacy</strong> {{item.privacy}}</div><div class=form-group><strong>Filesize</strong> {{item.filesize | filesize}}</div><div compile-html=item.body></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/checkin/form.html", '<div class="checkin-form form"><div class=page-content><div class="form-group hidden"><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Contact</label><content-select ng-model=item.contact ng-params="{maximum:1, type:\'contact\'}"></content-select></div><div class=row ng-if=!item.contact><div class=col-sm-6><div class=form-group><label>First Name</label><input ng-model=item.firstName placeholder="First name" class="form-control"></div></div><div class=col-sm-6><div class=form-group><label>Last Name</label><input ng-model=item.lastName placeholder="Last name" class="form-control"></div></div></div><div class=form-group><label>Event</label><content-select ng-params="{type:\'event\', canCreate:false, minimum:1, maximum:1}" ng-model=item.event></content-select></div><div class=form-group><label>Pin Number</label><input class=form-control ng-model=item.pinNumber placeholder="Pin Number"></div><div extended-fields></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/checkin/view.html", "<div class=checkin-view><div class=page-content><div view-extended-fields></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/code/form.html", '<div ng-controller=CodeFormController class="code-form form"><tabset class=flex-tabs><tab heading=Code><div class=flex-article><div class="code-toolbar cf"><div class=form-inline><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Syntax</label><select class=form-control ng-model=item.syntax><option value=js>Javascript</option><option value=css>CSS</option><option value=scss>SCSS</option><option value=html>HTML</option></select></div></div></div><div class=code-body ui-ace="{\n				onLoad: aceLoaded,\n				onChange: aceChanged,\n				enableBasicAutocompletion: true,\n				enableLiveAutocompletion: true\n			}" ng-model=item.body></div></div></tab><tab heading="{{definition.title}} settings" ng-if="definition && definition.fields.length"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading=Advanced><div class=page-content><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and access this content</p></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/code/view.html", "<div class=code-view><div class=page-content><div view-extended-fields></div><pre> {{item.body}}></pre></div></div>"), 
    $templateCache.put("fluro-admin-content/types/collection/form.html", '<div class="collection-form form"><tabset><tab heading=Collection><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder=Title ng-model=item.title></div><content-formly ng-model=item.data definition=definition></content-formly><div class=form-group><label>Items</label><content-select ng-model=item.items></content-select></div></div></tab><tab heading=Advanced><div class=page-content><p class=help-block>Provided extra map options for renaming files when packed and archived as an asset</p><div class=table-responsive><table class="table table-striped"><thead><th>Content</th><th>Map</th></thead><tbody><tr ng-repeat="mappedItem in item.items"><td class=col-sm-6><img ng-if="mappedItem._type == \'image\'" ng-src="{{$root.getThumbnailUrl(mappedItem._id)}}"> <i ng-if="mappedItem._type != \'image\'" class="fa fa-{{mappedItem._type}}"></i> {{mappedItem.title}}</td><td class=col-sm-6><input class=form-control ng-model="item.map[mappedItem._id]"></td></tr></tbody></table></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/collection/view.html", "<div class=collection-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/component/form.html", '<div ng-controller=ComponentFormController class="component-form form"><div class=application-panel><div class=application-header><div class="form-inline clearfix"><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Directive</label><input required class=form-control ng-model=item.directive placeholder="Directive / Tag name"></div><div class="form-group pull-right"><div class=btn-group><a class="btn btn-default" ng-class={active:!jsDisabled} ng-click="jsDisabled = !jsDisabled">Javascript</a> <a class="btn btn-default" ng-class={active:!htmlDisabled} ng-click="htmlDisabled = !htmlDisabled">HTML</a> <a class="btn btn-default" ng-class={active:!scssDisabled} ng-click="scssDisabled = !scssDisabled">SCSS</a></div></div></div></div><tabset class=application-body><tab heading=Code><div class=application-body><div class=application-columns><div class=application-panel ng-if=!jsDisabled><div class=application-header><i class="fa fa-script"></i> Javascript</div><div class=application-body><div class=code-editor ng-model=item.js ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'javascript\'}"></div></div></div><div class=application-panel ng-if=!htmlDisabled><div class=application-header><i class="fa fa-code"></i> HTML</div><div class=application-body><div class=code-editor ng-model=item.html ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div></div></div><div class=application-panel ng-if=!scssDisabled><div class=application-header><i class="fa fa-code"></i> SCSS</div><div class=application-body><div class=code-editor ng-model=item.css ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'scss\'}"></div></div></div></div></div></tab><tab heading="Instructions / Help"><div class=flex-article><div class="article-toolbar cf"><div id=article-toolbar></div></div><textarea class=article-body redactor ng-model=item.readme cols=30 rows=10></textarea></div></tab><tab heading=Advanced><div class=page-content><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and use this component</p></div></div></div></tab></tabset></div></div>'), 
    $templateCache.put("fluro-admin-content/types/contact/form.html", '<div ng-controller=ContactFormController class="contact-form form"><tabset class=flex-tabs><tab heading="Personal Details"><div class=page-content><div class=row><div class=col-md-8><div class=row><div class=col-md-6><div class=form-group><label>First Name</label><input class=form-control ng-model=item.firstName placeholder="First Name"></div></div><div class=col-md-6><div class=form-group><label>Last Name</label><input class=form-control ng-model=item.lastName placeholder="Last Name"></div></div></div><div class=row><div class=col-md-12><div class=form-group><label>Family</label><content-select ng-model=item.family ng-params="{maximum:1, type:\'family\'}"></content-select></div></div></div><div class="panel panel-default"><div class=panel-body><div ng-if=item.dobVerified><label>Age</label><br>{{ item.dob | age}} <em class=text-muted>(Born {{ item.dob | formatDate:\'j M Y\'}})</em></div><dob-select ng-if=!item.dobVerified ng-model=item.dob></dob-select><div class=checkbox><label><input type=checkbox ng-model=item.dobVerified> Birth Date is correct</label><p class=help-block ng-if=!item.dobVerified>Leave this unchecked if only age is known but the birthdate is not confirmed</p></div></div></div><div class=clearfix><div class=form-group><label>Gender</label><select class=form-control placeholder=Gender ng-model=item.gender><option value=male>Male</option><option value=female>Female</option></select></div><div class=form-group><label>Email Addresses</label><multi-value-field ng-model=item.emails placeholder="Email addresses"></multi-value-field></div><div class=form-group><label>Phone Numbers</label><multi-value-field ng-model=item.phoneNumbers placeholder="Phone Numbers"></multi-value-field></div></div><div class=form-group ng-if="details.length || getCreateableDetails().length"><label>Further details</label><p class=help-block>Click to edit detail sheets</p><div class="content-list list-group" ng-if=details.length><div class="list-group-item clearfix" ng-if=canEdit(sheet) ng-click=edit(sheet) ng-repeat="sheet in details"><div class=pull-left><i class="fa fa-contactdetail"></i> <span>{{sheet.title}}</span></div><div class="actions pull-right btn-group"><a class="btn btn-tiny btn-xs"><i class="fa fa-pencil"></i></a></div></div></div><div ng-repeat="sheet in getCreateableDetails()"><a class="btn btn-block btn-ghost" ng-click=createDetailSheet(sheet)>Add {{sheet.title}} <i class="fa fa-plus"></i></a></div></div></div><div class=col-md-4><div class=form-group></div></div></div></div></tab><tab heading=Attendance><div class=page-content><div class="contact-info-list panel panel-default" ng-if=checkins.length><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Recent Checkins</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Event</th><th class=col-xs-2>When</th><th></th></thead><tbody><tr ng-repeat="checkin in checkins"><td><a ng-click=$root.modal.view(checkin)><i class="fa fa-interaction"></i> <span>Checked in</span></a></td><td><a ui-sref="event.edit({id:checkin.event._id, definitionName:checkin.event.definition})">{{checkin.event.title}}</a></td><td>{{checkin.created | timeago}}</td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(checkin)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></div></tab><tab heading="{{upcomingassignments.length}} Upcoming Assignments"><div class=page-content><div class="contact-info-list panel panel-default"><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Upcoming Assignments</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Date</th><th class=col-xs-2></th><th></th></thead><tbody><tr ng-repeat="assignment in upcomingassignments"><td><a ng-click=$root.modal.view(assignment)><i class="fa fa-event"></i> <span>{{assignment.title}}</span></a></td><td>{{assignment.startDate | formatDate:\'g:ia - l j M\'}}</td><td></td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(assignment)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></div></tab><tab heading="{{pastassignments.length}} Past Assignments"><div class=page-content><div class="contact-info-list panel panel-default"><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Past Assignments</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Date</th><th class=col-xs-2>When</th><th></th></thead><tbody><tr ng-repeat="assignment in pastassignments"><td><a ng-click=$root.modal.view(assignment)><i class="fa fa-event"></i> <span>{{assignment.title}}</span></a></td><td>{{assignment.startDate | formatDate:\'g:ia - l j M\'}}</td><td>{{assignment.startDate | timeago}}</td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(assignment)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></div></tab><tab heading="{{interactions.length}} Interactions"><div class=page-content><div class="contact-info-list panel panel-default" ng-if=interactions.length><div class=panel-heading><h5 style=margin:0><i class="fa fa-interaction"></i> Recent Interactions</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5></th><th class=col-xs-2>When</th></thead><tbody><tr ng-repeat="interaction in interactions"><td><a ng-click=$root.modal.view(interaction)><i class="fa fa-interaction"></i> <span>{{interaction.title}}</span></a></td><td>{{interaction.definition}}</td><td>Submitted {{interaction.created | timeago}}</td><td class=actions><div class=btn-group ng-if=$root.access.canEditItem(interaction)><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(interaction)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/contact/view.html", '<div class=contact-view><div class=page-content><div class=page-header><h2>{{item.firstName | capitalizename}} {{item.lastName | capitalizename}}</h2><h4 class=text-muted>{{item.gender}}</h4><h5>Age {{item.dob | age}} <em class=text-muted>({{item.dob | formatDate:\'jS F Y\'}})</em></h5><div class=form-group><label>Realms</label><div><span class=inline-tag style="color: {{realm.color}}; background-color: {{realm.bgColor}}" ng-repeat="realm in item.realms">{{realm.title}}</span></div></div></div><div class=form-group ng-if=item.family><label>Family</label><div class="content-list list-group"><div class="list-group-item clearfix" ng-click=viewDetail(item.family)><div class=pull-left><i class="fa fa-family"></i> <span>{{item.family.title}}</span></div></div></div></div><div class=row><div class=col-sm-6><div class=form-group><label>Email Addresses</label><div class=list-group ng-if=item.emails.length><a class="list-group-item clearfix" ng-href=mailto:{{email}} ng-repeat="email in item.emails"><i class="fa fa-envelope pull-right"></i> {{email}}</a></div><div class=text-muted ng-if=!item.emails.length>No known email addresses</div></div></div><div class=col-sm-6><div class=form-group><label>Phone Numbers</label><div class=list-group ng-if=item.phoneNumbers.length><a class="list-group-item clearfix" ng-href=tel:{{number}} ng-repeat="number in item.phoneNumbers"><i class="fa fa-envelope pull-right"></i> {{number}}</a></div><div class=text-muted ng-if=!item.phoneNumbers.length>No known phone numbers</div></div></div></div><div class=form-group><label>Further details</label><div class="content-list list-group" ng-if=details.length><div class="list-group-item clearfix" ng-click=viewDetail(sheet) ng-repeat="sheet in details"><div class=pull-left><i class="fa fa-contactdetail"></i> <span>{{sheet.title}}</span></div></div></div></div><tabset><tab heading=Attendance><div class="contact-info-list panel panel-default" ng-if=checkins.length><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Recent Checkins</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Event</th><th class=col-xs-2>When</th><th></th></thead><tbody><tr ng-repeat="checkin in checkins"><td><a ng-click=$root.modal.view(checkin)><i class="fa fa-interaction"></i> <span>Checked in</span></a></td><td><a ui-sref="event.edit({id:checkin.event._id, definitionName:checkin.event.definition})">{{checkin.event.title}}</a></td><td>{{checkin.updated | timeago}}</td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(checkin)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></tab><tab heading="{{upcomingassignments.length}} Upcoming Assignments"><div class="contact-info-list panel panel-default"><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Upcoming Assignments</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Date</th><th class=col-xs-2></th><th></th></thead><tbody><tr ng-repeat="assignment in upcomingassignments"><td><a ng-click=$root.modal.view(assignment)><i class="fa fa-event"></i> <span>{{assignment.title}}</span></a></td><td>{{assignment.startDate | formatDate:\'g:ia - l j M\'}}</td><td></td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(assignment)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></tab><tab heading="{{pastassignments.length}} Past Assignments"><div class="contact-info-list panel panel-default"><div class=panel-heading><h5 style=margin:0><i class="fa fa-checkin"></i> Past Assignments</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5>Date</th><th class=col-xs-2>When</th><th></th></thead><tbody><tr ng-repeat="assignment in pastassignments"><td><a ng-click=$root.modal.view(assignment)><i class="fa fa-event"></i> <span>{{assignment.title}}</span></a></td><td>{{assignment.startDate | formatDate:\'g:ia - l j M\'}}</td><td>{{assignment.startDate | timeago}}</td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(assignment)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></tab><tab heading=Interactions><div class="contact-info-list panel panel-default" ng-if=interactions.length><div class=panel-heading><h5 style=margin:0><i class="fa fa-interaction"></i> Recent Interactions</h5></div><table class="table table-striped"><thead><th class=col-xs-5>Title</th><th class=col-xs-5></th><th class=col-xs-2>When</th></thead><tbody><tr ng-repeat="interaction in interactions"><td><a ng-click=$root.modal.view(interaction)><i class="fa fa-interaction"></i> <span>{{interaction.title}}</span></a></td><td></td><td>Submitted {{interaction.created | timeago}}</td><td class=actions><div class=btn-group><a class="btn btn-xs btn-default" ng-click=$root.modal.edit(interaction)><i class="fa fa-edit"></i></a></div></td></tr></tbody></table></div></tab></tabset><div view-extended-fields></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/contactdetail/form.html", '<div class="contactdetail-form form"><div class=page-content ng-if=!definition><h2>Please choose a definition before creating/editing details</h2></div><div class=page-content ng-if=definition><div class=form-group ng-if=!item.contact><label>Contact</label><content-select ng-model=item.contact ng-params="{maximum:1, type:\'contact\'}"></content-select></div><content-formly ng-model=item.data definition=definition></content-formly></div></div>'), 
    $templateCache.put("fluro-admin-content/types/contactdetail/view.html", "<div class=contactdetail-view><div class=page-content><div ng-if=item.contact><div class=row><div class=col-sm-6><div class=form-group><label>First name</label><div>{{item.contact.firstName}}</div></div></div><div class=col-sm-6><div class=form-group><label>Last name</label><div>{{item.contact.lastName}}</div></div></div></div></div><div view-extended-fields></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/definition/form.html", '<div ng-controller=DefinitionFormController class="definition-form form"><tabset class=flex-tabs><tab heading="Manage Fields"><field-editor ng-model=item.fields></field-editor></tab><tab ng-if=definition heading="{{definition.title}} information"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading="Definition Setup"><div class=page-content><div class=form-group><label>Title</label><p class=help-block>The human readable for this definition</p><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Definition Name</label><p class=help-block>The machine name for this definition</p><input class=form-control ng-model=item.definitionName placeholder="Definition Name"></div><div class=form-group><label>Plural Title</label><p class=help-block>The human readable plural for this definition</p><input class=form-control ng-model=item.plural placeholder="Plural Title"></div><div class=form-group><label>Parent Type</label><p class=help-block>Select which primitive type of content you are defining</p><select class=form-control ng-model=item.parentType><option value=article>Article</option><option value=audio>Audio</option><option value=asset>Asset</option><option value=image>Image</option><option value=video>Video</option><option value=event>Event</option><option value=contact>Contact</option><option value=interaction>Interaction</option><option value=location>Location</option><option value=collection>Collection</option><option value=application>Application</option><option value=package>Package</option><option value=plan>Plan</option><option value=contactdetail>Contact Detail</option><option value=product>Product</option><option value=post>Post</option><option value=checkin>Checkin</option><option value=code>Code</option><option value=team>Team</option><option value=definition>Definition (beta)</option></select></div></div></tab><tab heading="Event settings" ng-if="item.parentType == \'event\'"><div class=page-content><div class=form-group><label>Default Assignments</label><p class=help>Choose which assignments to ask for by default</p><contact-assign-select ng-model=item.data.defaultAssignments></contact-assign-select></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>{{item.plural}} Checkin Settings</label><p class=help-block>Toggle whether checkins should be enabled for this type of event</p><p><a class=btn ng-click="item.data.checkinDisabled = !item.data.checkinDisabled" ng-class="{\'btn-default\':item.data.checkinDisabled, \'btn-primary\':!item.data.checkinDisabled}"><span ng-if=!item.data.checkinDisabled>Checkins enabled</span> <span ng-if=item.data.checkinDisabled>Checkins disabled</span> <i class=fa ng-class="{\'fa-square-o\':item.data.checkinDisabled, \'fa-check-square-o\':!item.data.checkinDisabled}"></i></a></p></div></div></div></div></tab><tab heading="Product settings" ng-if="item.parentType == \'product\'"><div class=page-content><div class=form-group><label>Public Data Population</label><p class=help>Write additional paths (seperated by a space) to populate on the publicData object of items contained within this product<br><em class=text-muted>For example, if you wanted to populate \'items.data.publicData.images\' you would add \'images\' below</em></p><input class=form-control ng-model="item.data.publicDataPopulate"></div></div></tab><tab heading="Interaction settings" ng-if="item.parentType == \'interaction\'"><div class=page-content><div class="panel panel-default"><div class=panel-heading>Contact Creation</div><div class=panel-body><div class=form-group><label>Anonymous Interaction</label><div class=checkbox><label><input ng-model=item.data.allowAnonymous type=checkbox><p class=help-block>Disable the contact requirement and do not link interaction to a contact</p></label></div></div><div class=form-group ng-if=!item.data.allowAnonymous><label>Manual Fields</label><div class=checkbox><label><input ng-model=item.data.disableDefaultFields type=checkbox><p class=help-block>Disable the default fields and opt to create them manually</p></label></div></div><div ng-if="item.data.disableDefaultFields && !item.data.allowAnonymous"><div class=form-group><strong>Warning:</strong> You will need to provide the following contact details for users to be able submit this interaction</div><ul class=list-group><li class=list-group-item><h5>First Name</h5><h6 class=text-muted>contact.firstName (String)</h6></li><li class=list-group-item><h5>Last Name</h5><h6 class=text-muted>contact.lastName (String)</h6></li><li class=list-group-item><h5>Email Address or Addresses</h5><h6 class=text-muted>contact.emails (Array) <strong>OR</strong> contact.email (String)</h6><p class=help-block>Either an array of valid email addresses for the contact or a single valid email address</p></li><li class=list-group-item><h5>Phone Numbers or Number</h5><h6 class=text-muted>contact.phoneNumbers (Array) <strong>OR</strong> contact.phoneNumber (String)</h6><p class=help-block>Either an array of valid phone numbers for the contact or a single valid phone number</p></li></ul></div><div class=form-group ng-if="!item.data.allowAnonymous && !item.data.disableDefaultFields"><label>Contact ID Requirement</label><p class=help-block>Every contact requires either an email address or a phone number for verification.<br>Sometimes makes sense to ask/require one or both of these</p><select ng-model=item.data.identifier class=form-control><option value=email>Require an Email Address</option><option value=phone>Require a Phone Number</option><option value=either>Require either Phone Number or Email Address</option><option value=both>Require both Phone Number and Email Address</option></select></div></div></div><div ng-if=!item.data.disableDefaultFields><div class="panel panel-default"><div class=panel-heading>Optional Fields</div><table class="table table-striped"><thead><th>Field Name</th><th>Ask</th><th>Require</th></thead><tbody><tr ng-if=item.data.allowAnonymous><td><strong>First Name</strong></td><td><div class=checkbox><label><input ng-model=item.data.askFirstName type=checkbox><p class=help>Ask first name</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requireFirstName type=checkbox><p class=help>Require first name</p></label></div></td></tr><tr ng-if=item.data.allowAnonymous><td><strong>Last Name</strong></td><td><div class=checkbox><label><input ng-model=item.data.askLastName type=checkbox><p class=help>Ask last name</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requireLastName type=checkbox><p class=help>Require last name</p></label></div></td></tr><tr ng-if=item.data.allowAnonymous><td><strong>Email Address</strong></td><td><div class=checkbox><label><input ng-model=item.data.askEmail type=checkbox><p class=help>Ask Email Address</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requireEmail type=checkbox><p class=help>Require at least one Email Address</p></label></div></td></tr><tr ng-if=item.data.allowAnonymous><td><strong>Phone Number</strong></td><td><div class=checkbox><label><input ng-model=item.data.askPhone type=checkbox><p class=help>Ask for a Phone Number</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requirePhone type=checkbox><p class=help>Require at least one Phone Number</p></label></div></td></tr><tr ng-if=item.data.allowAnonymous><td><strong>Gender</strong></td><td><div class=checkbox><label><input ng-model=item.data.askGender type=checkbox><p class=help>Ask for gender</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requireGender type=checkbox><p class=help>Require gender</p></label></div></td></tr><tr><td><strong>Date of Birth</strong></td><td><div class=checkbox><label><input ng-model=item.data.askDOB type=checkbox><p class=help>Ask Date of Birth</p></label></div></td><td><div class=checkbox><label><input ng-model=item.data.requireDOB type=checkbox><p class=help>Require Date of Birth</p></label></div></td></tr></tbody></table></div></div><div class="panel panel-default"><div class=panel-heading><div class=checkbox><label><input type=checkbox ng-model="item.data.rateLimit.enabled"> Enable submission limits</label></div></div><div class=panel-body ng-if=item.data.rateLimit.enabled><div class=row><div class="form-group col-sm-6"><label>Allowed Attempts</label><p class=help-block>The number of submission attempts a user can make <em class=text-muted>Defaults to 1</em></p><input class=form-control type=number placeholder=1 ng-model="item.data.rateLimit.limit"></div><div class="form-group col-sm-6"><label>Period Duration</label><p class=help-block>The number of milliseconds <em class=text-muted>Defaults to 5 seconds</em></p><input class=form-control type=number placeholder=5000 ng-model="item.data.rateLimit.duration"></div></div></div></div></div></tab><tab heading="Reaction Settings"><div class=page-content><div class=form-group><label>Reactions</label><p class=help-block>Choose reactions to be fired when content linked to this definition is created</p><content-select ng-model=item.reactions ng-params="{type:\'reaction\'}"></content-select></div></div></tab><tab heading="Payment Settings" ng-if="item.parentType == \'interaction\'"><div class=page-content><div class=form-group><label>Require Payment</label><div class=checkbox><label><input ng-model=item.paymentDetails.required type=checkbox> Require a payment for creating this content</label></div></div><div class=form-group ng-if=!item.paymentDetails.required><label>Allow Payment</label><div class=checkbox><label><input ng-model=item.paymentDetails.allow type=checkbox> Allow a payment to be made when creating this content</label></div></div><div ng-if="item.paymentDetails.required || item.paymentDetails.allow"><div class=form-group ng-if=item.paymentDetails.required><label>Base Amount</label><p class=help-block>The base amount required for payment to create this type of content<br><em class=text-muted>A positive integer in the smallest currency unit (e.g 100 cents to charge $1.00). If left empty payment modifiers will need to be used to create a positive amount</em></p><div class=input-group><div class=input-group-addon>{{item.paymentDetails.amount / 100 | currency}}</div><input class=form-control type=number ng-model=item.paymentDetails.amount placeholder="Amount"></div></div><div class=row ng-if="item.paymentDetails.allow && !item.paymentDetails.required"><div class=col-sm-6><div class=form-group><label>Minimum Amount</label><p class=help-block>The minimum amount required for payment to create this type of content<br><em class=text-muted>A positive integer in the smallest currency unit (e.g 100 cents to charge $1.00)</em></p><div class=input-group><div class=input-group-addon>{{item.paymentDetails.minAmount / 100 | currency}}</div><input class=form-control type=number ng-model=item.paymentDetails.minAmount placeholder="Min Amount"></div></div></div><div class=col-sm-6><div class=form-group><label>Maximum Amount</label><p class=help-block>The maximum amount that can be charged when creating this type of content<br><em class=text-muted>A positive integer in the smallest currency unit (e.g 100 cents to charge $1.00)</em></p><div class=input-group><div class=input-group-addon>{{item.paymentDetails.maxAmount / 100 | currency}}</div><input class=form-control type=number ng-model=item.paymentDetails.maxAmount placeholder="Max Amount"></div></div></div></div><div class=form-group><label>Currency</label><select class=form-control type=number ng-model=item.paymentDetails.currency placeholder=Currency><option value=aud>AUD</option></select></div><div class="panel panel-default" ng-if=item.paymentDetails.required><div class=panel-heading>Payment Modifiers</div><tabset><tab heading=Modifiers><div class=application-panel><div class=table-responsive><table class=table><thead><th></th><th>Name</th><th>Modifier</th><th>Value</th><th>Condition</th><th></th></thead><tbody as-sortable=sortableOptions ng-model=item.paymentDetails.modifiers><tr as-sortable-item ng-repeat="modifier in item.paymentDetails.modifiers"><td><handle as-sortable-item-handle></handle></td><td><input placeholder=Title class=form-control ng-model="modifier.title"></td><td><select class=form-control ng-model=modifier.operation><option value=add>Add</option><option value=subtract>Subtract</option><option value=multiply>Multiply</option><option value=divide>Divide</option><option value=set>Set value</option></select></td><td><input class=form-control ng-model=modifier.expression placeholder="model.contacts.length"></td><td><input class=form-control ng-model=modifier.condition placeholder="model.contacts.length"></td><td><a class="btn btn-default" ng-click=removeModifier(modifier)><i class="fa fa-times"></i></a></td></tr></tbody></table></div><div class=panel-body><a class="btn btn-primary" ng-click=addPaymentModifier()><span>Add payment modifier</span> <i class="fa fa-plus"></i></a></div></div></tab><tab heading="Available variables"><div class=panel-body><table class="table table-striped"><thead><th>Name</th><th>Path</th><th>Type</th></thead><tbody><tr><td><strong>calculatedTotal</strong><p class=help-block>The calculated total inclusive of all active payment modifiers specified above</p></td><td>calculatedTotal</td><td>Number</td></tr><tr ng-repeat="field in availableFields"><td><strong>{{field.title}}</strong></td><td>model.{{field.path}}</td><td>{{field.type}}</td></tr></tbody></table></div></tab></tabset></div><div class="panel panel-default" ng-if=item.paymentDetails.required><div class=panel-heading>Alternative Payment Methods</div><div class=panel-body><div class=form-group><label>Allow alternative payments</label><div class=checkbox><label><input ng-model=item.paymentDetails.allowAlternativePayments type=checkbox> Allow payments to be made at a later time through other methods</label><p class=help-block>Checking this box will allow the user to create this content without paying the full required amount<br>payments will need to be made manually via other arrangements that can be added below.</p></div></div></div><div ng-if=item.paymentDetails.allowAlternativePayments><tabset class=flex-tabs><tab heading={{method.title}} ng-repeat="method in item.paymentDetails.paymentMethods"><div class=application-panel><div class=panel-body><div class=form-group><label>Title</label><input class=form-control ng-model=method.title placeholder="Title"></div><div class=form-group><label>Key</label><input class=form-control ng-model=method.key placeholder="Key"></div><div class=standard-body-wrapper><label>Help / Description</label><p class=help-block>Write a summary of how you want people to pay you if they are using alternative methods</p><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=method.description cols=30 rows=10></textarea></div></div><div class=clearfix><a class="btn btn-danger btn-sm pull-right" ng-click=removeAlternativePaymentMethod(method)><span>Remove \'{{method.title}}\'</span> <i class="fa fa-times"></i></a></div></div></div></tab></tabset><div class=panel-footer><a class="btn btn-primary" ng-click=addAlternativePaymentMethod()><span>Add additional payment method</span> <i class="fa fa-plus"></i></a></div></div></div></div></div></tab><tab heading="Advanced settings"><div class=page-content><div class="panel panel-default"><div class=panel-heading ng-click="settings.showLabelSection = !settings.showLabelSection"><i class="fa pull-right" ng-class="{\'fa-angle-up\':settings.showLabelSection, \'fa-angle-down\':!settings.showLabelSection}"></i><label>Customise Default Fields</label></div><div class=panel-body ng-if=settings.showLabelSection><div class=form-group><label>Automatic Title Generation</label><p class=help-block>Title of this kind of content are required by default, but you can set up this content to generate a title automatically based on a rule set below</p><select class=form-control ng-model=item.data.titleGeneration><option value="" ng-selected=!item.data.titleGeneration.length>Require a title for this content</option><option value=force ng-selected="item.data.titleGeneration == \'force\'">Hide the title field and generate automatically</option><option value=generate ng-selected="item.data.titleGeneration == \'generate\'">Generate automatically if left blank</option></select></div><div class=form-group ng-if=item.data.titleGeneration.length><label>Generation Rule</label><p class=help-block>Create titles automatically by using submitted field keys<br><em class=text-muted>Example {{data.fieldName}}</em></p><textarea class=form-control ng-model=item.data.titleGenerationRule placeholder="">\n									</textarea></div><div class=row><div class=col-sm-6 ng-if="item.data.titleGeneration != \'force\'"><div class=form-group><label>Title</label><p class=help-block>Rename the label for the title field</p><input class=form-control ng-model=item.data.titleLabel placeholder="Title Label"></div></div><div class=col-sm-6><div class=form-group><label>Body</label><div ng-if=!item.data.hideBody><p class=help-block>Rename the label for the body field</p><input class=form-control ng-model=item.data.bodyLabel placeholder="Body Label"></div><div class=checkbox><label><input type=checkbox ng-model="item.data.hideBody"> Hide body field</label></div></div></div></div><div class=row><div class=col-sm-6><div class=form-group><label>Submit Button Text</label><p class=help-block>Change the default text used for the submit button</p><input class=form-control ng-model=item.data.submitLabel placeholder="Submit button text"></div></div></div></div></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Filters</label><p class=help-block>Set which filters should be available within the administration list view (eg \'author\', \'realms\',\'data.group\')</p><filter-select ng-model=item.filters></filter-select></div></div></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Columns</label><div class=checkbox><label><input type=checkbox ng-model="item.disableColumns"> Disable basic columns in admin</label></div><p class=help-block>Set each column you want to show within the administration list view</p><column-select ng-model=item.columns></column-select></div></div></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Restrict To Realms</label><p class=help-block>Choose to restrict this content to only be created within specified realms</p><user-realm-select ng-model=item.restrictRealms></user-realm-select></div></div></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Deep Population Trail</label><p class=help-block>Add each path relative to the main content that will be populated sequentially to populate deep properties of this kind of content</p><populate-field-select ng-model=item.deepPopulate></populate-field-select></div><hr><div class=checkbox><label><input type=checkbox ng-model="item.disableBasicPopulation"> Disable basic population</label><p class=help-block>If using deep population you can turn this off allowing you to specify all the data you wish to retrieve when retrieving this content</p></div></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and use this definition</p></div></div><div class=form-group style=display:none><label>Populate Additional Fields <em class=text-muted>Deprecated</em></label><p class=help-block>Type in each field you would like to populate seperated by a space, these fields will be populated on each content reference in addition to the usual fields <em class=text-muted>eg. title _id</em>.</p><input class=form-control ng-model="item.populateFields"></div></div></tab><tab heading=Posts><div class=page-content><div class=form-group><label>Restrict Post Types</label><p class=help-block>Enter the machine name of each post type that can be attached to this type of content</p><multi-value-field ng-model=item.postTypes placeholder="Post Definition Name"></multi-value-field></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/definition/view.html", "<div class=definition-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/delete.html", "<div class=application-panel><div class=application-header><div class=type-icon><i class=\"fa fa-exclamation\"></i></div><div class=pull-left><h1>Delete <em ng-if=\"type.path == 'user'\">'{{item.name}}'</em><em ng-if=\"type.path != 'user'\">'{{item.title}}'</em> <span class=text-muted>// {{titleSingular}}</span></h1></div></div><div class=page-content><p>Are you sure you want to delete <em ng-if=\"type.path == 'user'\">'{{item.name}}'</em><em ng-if=\"type.path != 'user'\">'{{item.title}}'</em>?</p><a class=\"btn btn-default\" ui-sref={{cancelPath}}>Cancel</a> <a class=\"btn btn-default\" ng-click=delete()>Delete</a></div></div>"), 
    $templateCache.put("fluro-admin-content/types/endpoint/form.html", '<div ng-controller=EndpointFormController class="endpoint-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>API Key</label><input class=form-control ng-model=item.apikey readonly></div><h3>Basic Permissions</h3><permission-select ng-model=item.permissions ng-options=permissions></permission-select><h3>Custom Permissions</h3><permission-select ng-model=item.genericPermissions ng-options=genericPermissions></permission-select></div></div>'), 
    $templateCache.put("fluro-admin-content/types/endpoint/view.html", "<div class=endpoint-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/event/form.html", '<div ng-controller=EventFormController class="event-form form"><tabset class=flex-tabs><tab heading="Basic Information"><div class=page-content><div class=form-group><label>{{titleLabel}}</label><input class=form-control placeholder={{titleLabel}} ng-model=item.title></div><div class=form-group><label>Subtitle</label><input class=form-control ng-model=item.firstLine placeholder=Subtitle></div><div class="form-group clearfix"><div class=row><div class=col-md-5><label>Start Date</label></div><div class=col-md-5><div class=checkbox-wrapper><a class="btn btn-checkbox" ng-click="useEndDate = !useEndDate" ng-class={active:useEndDate}><i class="fa fa-check"></i></a><label>Include an end date</label></div></div></div><div class=row><div class=col-md-5><dateselect ng-label="\'Start Date\'" use-time=true ng-model=item.startDate></dateselect></div><div class=col-md-5><dateselect ng-label="\'End Date\'" ng-if=useEndDate use-time=true init-date=item.startDate min-date=item.startDate ng-model=item.endDate></dateselect></div></div></div><div class=form-group ng-if=!definition.data.hideBody><label>{{bodyLabel}}</label><div class=standard-body-wrapper><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></div></div></div></tab><tab ng-if=definition heading="{{definition.title}} information"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading=Assignments><div class=page-content><div class=form-group><contact-assign-select ng-model=item.assignments ng-confirmations=confirmations></contact-assign-select></div></div></tab><tab heading=Volunteers><div class=page-content><div class=form-group><volunteer-select ng-model=item.volunteers></volunteer-select></div></div></tab><tab heading=Plans ng-if=item._id><div class=page-content><div class=form-group><label>Plans</label><p class=help-block>Create new or select existing plans for this event</p><content-select ng-params="{type:\'plan\', template:{title:\'New plan\', realms:item.realms, startDate:item.startDate, \'event\':item}}" ng-model=item.plans></content-select></div><a class="btn btn-primary" ng-click=createPlanFromTemplate()><span>Create plan from Template</span><i class="fa fa-plus"></i></a></div></tab><tab heading=Media><div class=page-content><div class=form-group><label>Images</label><content-select ng-params="{type:\'image\'}" ng-model=item.images></content-select></div><div class=form-group><label>Videos</label><content-select ng-params="{type:\'video\'}" ng-model=item.videos></content-select></div></div></tab><tab heading="Files & Resources"><div class=page-content><div class=form-group><label>Files and resources</label><content-select ng-params="{type:\'asset\'}" ng-model=item.assets></content-select></div></div></tab><tab heading=Location><div class=page-content><div ng-if=item.locations.length class=form-group><list-map ng-model=item.locations></list-map></div><location-select ng-locations=item.locations ng-rooms=item.rooms></location-select></div></tab><tab heading="Check In"><div class=page-content><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.checkinData.disabled"> <strong>Disable Checkin</strong></label><p class=help-block ng-if=item.checkinData.disabled>Uncheck this box to enable checkin to this event</p><p class=help-block ng-if=!item.checkinData.disabled>Check this box to disable checkin to this event</p></div></div><div ng-if=!item.checkinData.disabled><div class=form-group><label>Checkin Groups</label><p class=help-block>Add group names to help segment checkins for this event eg. (Ages, Rooms, etc)</p><multi-value-field ng-model=item.checkinData.groups placeholder="Group Name"></multi-value-field></div><hr><div class=row><div class=col-sm-6><div class=form-group><label>Checkin Opens</label><p class=help-block>How many minutes earlier can users checkin</p><div class=input-group><input type=number min=0 max=180 class=form-control ng-model="item.checkinData.checkinStartOffset"><div class=input-group-addon>Minutes earlier</div></div></div><p class=help-block>No checkins before</p><p class=lead>{{checkinStartDate | formatDate:\'g:ia l j M\'}}</p></div><div class=col-sm-6><div class=form-group><label>Checkin Closes</label><p class=help-block>How many minutes after this event\'s end time can a user still checkin</p><div class=input-group><input type=number min=0 max=180 class=form-control ng-model="item.checkinData.checkinEndOffset"><div class=input-group-addon>Minutes later</div></div></div><p class=help-block>No checkins after</p><p class=lead>{{checkinEndDate | formatDate:\'g:ia l j M\'}}</p></div></div><hr><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.checkinData.requirePin"> Require PIN Number</label><p class=help-block>Require a PIN number for all checkins at this event</p></div></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/event/list.html", '<div class=application-panel><script type=text/ng-template id=template-list.html><div class="realm-list">\n				<ul class="list-group">\n					<li class="list-group-item"  ng-click="createFromTemplate(template)" ng-repeat="template in templates">\n						{{template.title}}\n						<i class="fa fa-plus right"></i>\n					</li>\n				</ul>\n			</div></script><div class=application-header ng-class={expanded:headerExpanded}><a class="btn btn-expand btn-xs visible-xs btn-block" ng-click="headerExpanded = !headerExpanded"><i class=fa ng-class="{\'fa-chevron-down\':!headerExpanded, \'fa-chevron-up\':headerExpanded}"></i></a><div class=header-options ng-class={expanded:headerExpanded}><div class=type-icon><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1>{{titlePlural}} <span class=text-muted ng-if=search.browseDate>// {{search.browseDate | date}}</span></h1></div><div class="pull-right text-right"><div class=form-inline><div class=btn-group ng-if=type.viewModes.length><a class="btn btn-default" ng-class="{active:currentViewMode(\'default\')}" ng-click="settings.viewMode = \'default\'"><i class="fa fa-bars"></i></a><a class="btn btn-default" ng-class={active:currentViewMode(viewMode)} ng-click="settings.viewMode = viewMode" ng-repeat="viewMode in type.viewModes"><i class="fa fa-{{viewMode}}"></i></a></div><div class=btn-group ng-if=canCreate()><a class="btn btn-primary" ui-sref="{{type.path}}.create({definitionName:definition.definitionName, initDate:stringBrowseDate()})"><span>Create {{titleSingular}}</span><i class="fa fa-plus"></i></a></div><div class=form-group><search ng-model=search.terms></search></div></div></div></div></div><div class=application-body><div class=application-columns><div class="filter-list event-filter-list"><div class="panel panel-filters" ng-class={expanded:expanded}><div class=panel-heading ng-click="expanded = !expanded"><h3 class=panel-title><i class="fa pull-right visible-xs" ng-class="{\'fa-chevron-up\':expanded, \'fa-chevron-down\':!expanded}"></i> Filter by date <em class=text-muted>{{search.browseDate | date}}</em></h3></div><div class=panel-expand-body><datepicker class=datepicker ng-change=changeDate() custom-class="getMarker(date, mode)" ng-model=search.browseDate show-weeks=false></datepicker><div class=date-options><div class="btn-group btn-group-justified"><a class="btn btn-default btn-sm" tooltip-append-to-body=true tooltip-placement=bottom tooltip="Events after {{search.browseDate | formatDate:\'jS M\'}}" ng-class={active:!search.restrictToDate} ng-click=setShowAll()>Upcoming</a> <a class="btn btn-default btn-sm" tooltip-append-to-body=true tooltip-placement=bottom tooltip="Events on {{search.browseDate | formatDate:\'jS M\'}}" ng-class={active:search.restrictToDate} ng-click=setShowDate()>Date</a></div><br><a class="btn btn-default btn-sm" ng-click=resetToday()>Reset to Today</a></div></div></div><div ng-if="filteredItems.length || filtersActive()"><filter-block ng-model=search.filters persist=true ng-source=items ng-filter-key="\'status\'" ng-filter-title="\'Status\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in definition.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in type.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'author\'" ng-filter-title="\'Author\'"></filter-block></div></div><div class=list-column><div class=application-panel><select-toolbar ng-model=selection ng-type=type ng-filtered=filteredItems ng-definitions=definitions ng-definition=definition ng-if=selection.items.length ng-plural=titlePlural></select-toolbar><div class=application-body ng-switch=settings.viewMode><div class=event-calendar ng-switch-when=calendar><div class="calendar-tools clearfix"><div class=pull-left><div class=form-inline><label><i class="fa fa-clock-o"></i> Calendar view</label><select ng-model=search.calendarMode class=form-control><option value=day>Day</option><option value=week>Week</option><option value=month>Month</option><option value=year>Year</option></select></div></div><div class=pull-right><div class=btn-group><a class="btn btn-default btn-sm" mwl-date-modifier date=search.browseDate decrement=search.calendarMode><i class="fa fa-chevron-left"></i><span>Previous {{search.calendarMode}}</span></a> <a class="btn btn-default btn-sm" mwl-date-modifier date=search.browseDate set-to-today><span>Today</span></a> <a class="btn btn-default btn-sm" mwl-date-modifier date=search.browseDate increment=search.calendarMode><span>Next {{search.calendarMode}}</span><i class="fa fa-chevron-right"></i></a></div></div></div><mwl-calendar view=search.calendarMode current-day=search.browseDate events=calendar.events view-title=calendarTitle on-event-click=calendarOpen(calendarEvent) on-event-drop="calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd" edit-event-html="\'<i class=\\\'glyphicon glyphicon-pencil\\\'></i>\'" delete-event-html="\'<i class=\\\'glyphicon glyphicon-remove\\\'></i>\'" on-edit-event-click=eventEdited(calendarEvent) on-delete-event-click=eventDeleted(calendarEvent) auto-open=true></mwl-calendar></div><card-events ng-switch-when=cards class=view-mode ng-type=definedType ng-date=search.browseDate ng-model=filteredItems></card-events><div ng-switch-default class=application-panel><empty-text ng-if=!filteredItems.length></empty-text><event-list-view ng-if=filteredItems.length></event-list-view></div></div><div ng-if=pagerEnabled() class="application-footer text-center"><pagination max-size=pager.maxSize items-per-page=pager.limit total-items=filteredItems.length ng-model=pager.currentPage ng-change=updatePage()></pagination></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/event/view.html", '<div class=event-view ng-controller=EventViewController><tabset><tab heading="Event Details"><div class=page-content><div class=event-overview><div class=page-header><h3><i class="fa fa-event"></i> {{item.title}}</h3><h4><span class=text-muted>{{item.startDate | formatDate:\'g:ia l j F Y\'}}</span></h4></div><div ng-if=item.body><h4>Overview</h4><div ng-bind-html="item.body | trusted"></div></div><div view-extended-fields></div><div ng-if=item.locations.length><h4>Locations</h4><list-map ng-model=item.locations></list-map></div></div></div></tab><tab heading="Team Members" ng-if=item.assignments.length><div class=page-content><div class=team-overview><div class=page-header><h4>Team Members</h4></div><div class=row><div class="col-xs-6 col-sm-4 form-group" ng-repeat="assignment in item.assignments"><strong>{{assignment.title}}</strong><div ng-repeat="contact in assignment.contacts"><div ng-if="isConfirmed(contact, assignment)" class=confirmed><i class="fa fa-check hidden-print"></i> {{contact.title | capitalizename}}</div><div ng-if="isUnavailable(contact, assignment)" tooltip={{assignment.description}} tooltip-trigger=mouseenter class=unavailable><i class="fa fa-times hidden-print"></i> {{contact.title | capitalizename}}</div><div ng-if="isUnknown(contact, assignment)">{{contact.title | capitalizename}}</div></div></div></div><div ng-if=item.volunteers.length><div class=page-header><h4>Volunteers</h4></div><div class=row><div class="col-xs-6 col-sm-4 form-group" ng-repeat="slot in item.volunteers"><strong>{{slot.title}}</strong><div ng-repeat="contact in slot.contacts"><div class=confirmed><i class="fa fa-check hidden-print"></i> {{contact.title | capitalizename}}</div></div></div></div></div><div class=page-header><h4>Confirmations</h4><p class=text-muted ng-if=!confirmations.length>No team members have responded for this event</p></div><div ng-if=confirmations.length><tabset><tab heading="{{unavailableList.length}} Unavailable"><div class=tab-content><table class="table table-striped"><thead><th>Name</th><th>Assignment</th><th>Reason</th><th>When</th></thead><tbody><tr class=unavailable ng-repeat="confirmation in unavailableList | orderBy:\'-date\'"><td><i class="fa fa-times unavailable"></i> {{confirmation.contact.title | capitalizename}}</td><td>{{confirmation.title}}</td><td>{{confirmation.description}}</td><td>{{confirmation.date | timeago}}</td></tr></tbody></table></div></tab><tab heading="{{confirmedList.length}} Confirmed"><div class=tab-content><table class="table table-striped"><thead><th>Name</th><th>Assignment</th><th>Reason</th><th>When</th></thead><tbody><tr class=confirmed ng-repeat="confirmation in confirmedList | orderBy:\'-date\'"><td><i class="fa fa-check confirmed"></i> {{confirmation.contact.title | capitalizename}}</td><td>{{confirmation.title}}</td><td>{{confirmation.reason}}</td><td>{{confirmation.date | timeago}}</td></tr></tbody></table></div></tab><tab heading="{{confirmations.length}} Total"><div class=tab-content><table class="table table-striped"><thead><th>Name</th><th>Assignment</th><th>Status</th><th>When</th></thead><tbody><tr ng-class="{\'confirmed\':confirmation.status == \'confirmed\', \'unavailable\':confirmation.status == \'denied\'}" ng-repeat="confirmation in confirmations | orderBy:\'-date\'"><td><i class=fa ng-class="{\'fa-check confirmed\':confirmation.status == \'confirmed\', \'fa-times unavailable\':confirmation.status == \'denied\'}"></i> {{confirmation.contact.title | capitalizename}}</td><td>{{confirmation.title}}</td><td>{{confirmation.status}}</td><td>{{confirmation.date | timeago}}</td></tr></tbody></table></div></tab></tabset></div></div></div></tab><tab heading={{plan.title}} ng-repeat="plan in item.plans"><admin-plan-view ng-model=plan ng-event=item ng-confirmations=confirmations></admin-plan-view></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/family/form.html", '<div class="family-form form"><tabset class=flex-tabs><tab heading="Household Information"><div class=page-content><div class=form-group><label>Title / Family Name</label><input class=form-control ng-model=item.title placeholder="Family Name"></div><div class=row><div class="form-group col-sm-6"><label>Email Addresses</label><multi-value-field ng-model=item.emails placeholder="Email addresses"></multi-value-field></div><div class="form-group col-sm-6"><label>Phone Numbers</label><multi-value-field ng-model=item.phoneNumbers placeholder="Phone Numbers"></multi-value-field></div></div><div class="panel panel-default"><div class=panel-body><h4><i class="fa fa-map-marker"></i> Home Address</h4><address-form ng-model=item.address></address-form></div></div><a ng-click="item.samePostal = false" ng-if=item.samePostal>Postal address is different</a> <a ng-click="item.samePostal = true" ng-if=!item.samePostal>Postal address is the same</a><div class="panel panel-default" ng-if=!item.samePostal><div class=panel-body><h4><i class="fa fa-map-marker"></i> Postal Address</h4><address-form ng-if=!item.samePostal ng-model=item.postalAddress></address-form></div></div></div></tab><tab ng-if=definition heading="{{definition.title}} information"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading="{{item.items.length}} Family Members" ng-if=item._id><div class=page-content><div class=row><div class="col-sm-3 col-md-2" ng-repeat="contact in item.items"><a ng-click=$root.modal.edit(contact)><div class=avatar><i class="fa fa-{{contact.gender}}"></i></div><h5 class=text-capitalize>{{contact.firstName}} {{contact.lastName}}</h5></a></div><div class="col-sm-3 col-md-2"><a ng-click=addContact()><div class=avatar><i class="fa fa-plus"></i></div><h5 class=text-capitalize>Create New</h5></a></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/family/view.html", "<div class=family-view><div class=page-content><h2>{{item.title}}</h2></div></div>"), 
    $templateCache.put("fluro-admin-content/types/form.html", '<div class=application-panel><notifications ng-if=$dismiss></notifications><div class=application-header ng-class={expanded:expanded}><a class="btn btn-expand btn-xs visible-xs btn-block" ng-click="expanded = !expanded"><i class=fa ng-class="{\'fa-chevron-down\':!expanded, \'fa-chevron-up\':expanded}"></i></a><div class=header-options ng-class={expanded:expanded}><div class=type-icon><i class="fa fa-{{icon}}"></i></div><div class=pull-left ng-if=!useInlineTitle><h1 ng-if="type.path != \'user\' && type.path != \'contact\'">{{item.title}} <span class=text-muted>// {{titleSingular}}</span></h1><h1 ng-if="type.path == \'user\'">{{item.name | capitalizename}} <span class=text-muted>// {{titleSingular}}</span></h1><h1 ng-if="type.path == \'contact\'">{{item.title | capitalizename}} <span class=text-muted>// {{titleSingular}}</span></h1></div><div class=pull-left ng-if=useInlineTitle><input class="form-control inline-edit" placeholder="{{titleSingular}} title" ng-model=item.title></div><div class="pull-right text-right"><div class=btn-group ng-if=realmSelectEnabled><realm-select ng-type=definedType ng-restrict=definition.restrictRealms ng-model=item.realms></realm-select></div><div class=btn-group ng-if=tagSelectEnabled><tag-select ng-model=item.tags></tag-select></div><a class="btn btn-default" ng-if="item.assetType == \'upload\' && item._id" ng-href={{getDownloadUrl(item._id)}} target=_blank><span>Download</span><i class="fa fa-download"></i></a> <a class="btn btn-default" ng-click=cancel()>{{cancelText}}</a> <a class="btn btn-primary" ng-if=!uploadSave ng-click=save()>{{saveText}}</a> <a class="btn btn-primary" ng-if=uploadSave ng-click=saveAndUpload() ng-disabled=uploader.isUploading>Upload</a></div></div></div><div class=application-body><div ng-include=template class=form-template-wrapper></div></div><div class=application-footer ng-if="type.path != \'user\' && type.path != \'persona\'"><div ng-if=showConfiguration><div class=row><div class="form-group col-sm-4"><label>Slug path</label><input ng-model=item.slug placeholder="Slug will be automatically generated" class="form-control"></div><div class="form-group col-sm-4"><label>{{item.owners.length}} Owner<span ng-if="item.owners.length != 1">s</span></label><admin-owner-select ng-model=item.owners></admin-owner-select></div><div class="form-group col-sm-4" ng-if=item._id><label>Advanced</label><br><a class="btn btn-default" ng-click=openAPIDocument()><span>View from API</span> <i class="fa fa-angle-right"></i></a></div></div></div><div class=clearfix><div class=pull-left><a class="btn btn-sm btn-default" ng-click="showConfiguration = !showConfiguration"><i class="fa fa-cogs"></i></a></div><div class=pull-right><div class=updated-summary>Last updated <span ng-if=item.updatedBy>by {{item.updatedBy}} -</span> {{item.updated | timeago}}</div><div class=status-summary><status-select ng-model=item></status-select></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/image/form.html", '<div class="image-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder="Filename will be used if left blank" ng-model=item.title></div><div ng-if=item._id class="image-container form-group"><img preload-image ng-src={{getImageURL()}}></div><asset-replace-form></asset-replace-form><content-formly ng-model=item.data definition=definition></content-formly><div class=row ng-if="needsWidth || needsHeight"><div class="form-group col-sm-6" ng-if=needsWidth><label>Width</label><input type=number ng-model=item.width placeholder="Width (px)" class="form-control"></div><div class="form-group col-sm-6" ng-if=needsHeight><label>Height</label><input type=number ng-model=item.height placeholder="Height (px)" class="form-control"></div></div><div class=standard-body-wrapper><label>Body</label><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></div><div class=form-group><div class="privacy-select btn-group btn-group-justified"><a class="btn btn-privacy" ng-click="item.privacy = \'secure\'" ng-class="{\'active\':item.privacy == \'secure\'}"><i class="fa fa-lock"></i> <strong>Secure</strong><br><em class=small>Only users and applications with correct permissions can view</em></a> <a class="btn btn-privacy" ng-click="item.privacy = \'public\'" ng-class="{\'active\':item.privacy == \'public\'}"><i class="fa fa-globe"></i> <strong>Public</strong><br><em class=small>Anyone in the world can view</em></a></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/image/view.html", '<div class=image-view><div class=page-content><div class="image-container form-group"><img ng-src={{$root.getImageUrl(item._id)}}></div><div view-extended-fields></div><div class=form-group ng-if="item.width && item.height"><strong>Dimensions</strong> {{item.width}}x{{item.height}}</div><div class=form-group><strong>Security / Privacy</strong> {{item.privacy}}</div><div class=form-group><strong>Filesize</strong> {{item.filesize | filesize}}</div><div compile-html=item.body></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/integration/form.html", '<div class="integration-form form"><div class=page-content><div class=col-md-6><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div ng-if=!item.module class=form-group><label>Module</label><select class=form-control ng-model=item.module placeholder=Module><option value=sendgrid>Sendgrid</option><option value=instagram>Instagram</option><option value=smsbroadcast>SMS Broadcast</option><option value=stripe>Stripe</option><option value=eway>eWay</option></select></div><div ng-switch=item.module><div class="panel panel-default" ng-switch-when=smsbroadcast><div class=panel-heading><h5>SMS Broadcast Details</h5></div><div class=panel-body><div class=form-group><label>Username</label><input class=form-control ng-model=item.privateDetails.username placeholder=Username></div><div class=form-group><label>Password</label><input class=form-control ng-model=item.privateDetails.password placeholder=Password></div></div></div><div class="panel panel-default" ng-switch-when=eway><div class=panel-heading><h5>eWay Integration Details</h5></div><div class=panel-body><div class=form-group><label>Rapid API Key</label><input class=form-control ng-model=item.privateDetails.apiKey placeholder="Rapid API Key"></div><div class=form-group><label>Rapid API Password</label><input class=form-control ng-model=item.privateDetails.apiPassword placeholder="Rapid API Password"></div><div class=form-group><label>Client Side Encryption Key</label><p class=help-block>To implement client side encryption <a target=_blank href=https://eway.io/api-v3/#implementation>More information can be found here</a></p><input class=form-control ng-model=item.publicDetails.publicKey placeholder="Public Key"></div></div></div><div class="panel panel-default" ng-switch-when=instagram><div class=panel-heading><h5>Instagram API Details</h5></div><div class=panel-body><p class=help-block>Instagram now requires OAuth authentication to connect to it\'s API, please create an application client by visiting the <a target=_blank href="https://www.instagram.com/developer/clients/manage/">Instagram Development Console</a></p><div class=form-group><label>Client ID</label><input class=form-control ng-disabled=item.publicDetails.accessToken.length ng-model=item.publicDetails.clientID placeholder="Client ID"></div><div class=form-group><label>Client Secret</label><input class=form-control ng-disabled=item.publicDetails.accessToken.length ng-model=item.privateDetails.clientSecret placeholder="Client Secret"></div><div ng-if=!item._id><div class=form-group><label><i class="fa fa-warning"></i> Save this integration and reopen to authenticate</label></div></div><div class=form-group ng-if="!item.publicDetails.accessToken.length && item._id"><label>Redirect URI</label><p class=help-block>Set your instagram client redirect_uri to <em>https://apiv2.fluro.io/integrate/instagram/{{item._id}}</em> then authenticate below to activate this integration</p><a class="btn btn-primary" ng-click=authorizeInstagram()><span>Authenticate</span> <i class="fa fa-angle-right"></i></a></div><div class=form-group ng-if=item.publicDetails.accessToken.length><label>Access Token</label><input class=form-control ng-model=item.publicDetails.accessToken disabled class=disabled placeholder="Access token will be generated after save"></div><div class=form-group ng-if=!item.publicDetails.accessToken.length></div></div></div><div class="panel panel-default" ng-switch-when=stripe><div class=panel-heading><h5>Stripe Integration Details</h5></div><div class=panel-body><div class=form-group><label>Test Secret Key</label><input class=form-control ng-model=item.privateDetails.testSecretKey placeholder="Test Secret Key"></div><div class=form-group><label>Test Public Key</label><input class=form-control ng-model=item.publicDetails.testPublicKey placeholder="Test Public Key"></div><div class=form-group><label>Live Secret Key</label><input class=form-control ng-model=item.privateDetails.liveSecretKey placeholder="Live Secret Key"></div><div class=form-group><label>Live Public Key</label><input class=form-control ng-model=item.publicDetails.livePublicKey placeholder="Live Public Key"></div><div class=form-group><label>Sandbox Mode</label><input class=form-control type=checkbox ng-model=item.publicDetails.sandbox></div></div></div><div class="panel panel-default" ng-switch-when=mailchimp><div class=panel-heading><h5>Mail Chimp Integration Details</h5></div><div class=panel-body><div class=form-group><label>Test Secret Key</label><input class=form-control ng-model=item.privateDetails.testSecretKey placeholder="Test Secret Key"></div><div class=form-group><label>Test Public Key</label><input class=form-control ng-model=item.publicDetails.testPublicKey placeholder="Test Public Key"></div><div class=form-group><label>Live Secret Key</label><input class=form-control ng-model=item.privateDetails.liveSecretKey placeholder="Live Secret Key"></div><div class=form-group><label>Live Public Key</label><input class=form-control ng-model=item.publicDetails.livePublicKey placeholder="Live Public Key"></div></div></div><div class="panel panel-default" ng-switch-when=youtube><div class=panel-heading><h5>Youtube Integration Details</h5></div><div class=panel-body><div class=form-group><label>Access Token</label><input class=form-control ng-model=item.publicDetails.accessToken placeholder="Access Token"></div><div class=form-group><label>Client ID</label><input class=form-control ng-model=item.privateDetails.clientId placeholder="Client ID"></div><div class=form-group><label>Client Secret</label><input class=form-control ng-model=item.privateDetails.clientSecret placeholder="Client Secret"></div><div class=form-group><label>Expires In</label><input class=form-control ng-model=item.privateDetails.expiresIn placeholder="Expires In"></div><div class=form-group><label>ID Token</label><input class=form-control ng-model=item.privateDetails.idToken placeholder="ID Token"></div><div class=form-group><label>Refresh Token</label><input class=form-control ng-model=item.privateDetails.refreshToken placeholder="refresh Token"></div><div class=form-group><label>Token Type</label><input class=form-control ng-model=item.privateDetails.tokenType placeholder="Token Type"></div></div></div><div class="panel panel-default" ng-switch-when=vimeo><div class=panel-heading><h5>Vimeo Integration Details</h5></div><div class=panel-body><div class=form-group><label>Client ID</label><input class=form-control ng-model=item.privateDetails.clientId placeholder="Client ID"></div><div class=form-group><label>Client Secret</label><input class=form-control ng-model=item.privateDetails.clientSecret placeholder="Client Secret"></div><div class=form-group><label>Access Token</label><input class=form-control ng-model=item.publicDetails.accessToken placeholder="Access Token"></div></div></div><div class="panel panel-default" ng-switch-when=sendgrid><div class=panel-heading><h5>Sendgrid Integration Details</h5></div><div class=panel-body><div class=form-group><label>Username</label><input class=form-control ng-model=item.privateDetails.username placeholder=Username></div><div class=form-group><label>Password</label><input type=password class=form-control ng-model=item.privateDetails.password placeholder=Password></div></div></div><div class="panel panel-default" ng-switch-when=gmail><div class=panel-heading><h5>Gmail Integration Details</h5></div><div class=panel-body><div class=form-group><label>Username / Email Address</label><input class=form-control ng-model=item.privateDetails.username placeholder=Username></div><div class=form-group><label>Password</label><input type=password class=form-control ng-model=item.privateDetails.password placeholder=Password></div></div></div></div></div><div class=col-md-6><pre>{{item | json}}</pre></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/integration/view.html", "<div class=integration-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/interaction/form.html", '<div ng-controller=InteractionFormController class="integration-form form"><tabset class=flex-tabs><tab heading=Edit><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder=Title ng-model="item.title"></div><div ng-if=item._id class=form-group><label>Key</label><input class=form-control placeholder="Interaction Key" ng-model="item.key"></div><div class=form-group ng-if="item.contact || item.showContactLinks"><label>Primary Contact</label><p class=help-block>Primary contact for this interaction</p><content-select ng-model=item.contact ng-params="{maximum:1, type:\'contact\'}"></content-select></div><div class=form-group ng-if="item.contacts.length || item.showContactLinks"><label>Linked Contacts</label><p class=help-block>Link multiple contacts</p><content-select ng-model=item.contacts ng-params="{type:\'contact\'}"></content-select></div><div class="panel panel-default" ng-if="!item.contact && !item.contacts.length && !item.showContactLinks"><div class=panel-heading><strong>Contact Details</strong></div><div class=panel-body><p class=help-block>This interaction could not be linked to a contact so all of the details that were provided are included below</p><div class=row><div class="col-sm-6 form-group"><label>First Name</label><input ng-model=item.data._firstName placeholder="First name" class="form-control"></div><div class="col-sm-6 form-group"><label>Last Name</label><input ng-model=item.data._lastName placeholder="Last name" class="form-control"></div></div><div class=row><div class="col-sm-6 form-group"><label>Gender</label><select ng-model=item.data._gender class=form-control><option value=male ng-selected="item.data._gender == \'male\'">Male</option><option value=female ng-selected="item.data._gender == \'female\'">Female</option></select></div></div><div class=row><div class="col-sm-6 form-group"><label>Email Address</label><input ng-model=item.data._email placeholder="Email address" class="form-control"></div><div class="col-sm-6 form-group"><label>Phone Number</label><input ng-model=item.data._phoneNumber placeholder="Phone Number" class="form-control"></div></div><a class="btn btn-default" ng-click="item.showContactLinks = true"><span>Link to contacts</span> <i class="fa fa-contact"></i></a></div></div><content-formly ng-model=item.data definition=definition agreements=item.agreements></content-formly></div></tab><tab heading=Model><div class=page-content><div class=form-group><label>Raw Data</label><pre>{{item | json}}</pre></div></div></tab><tab heading="Terms and Condition Agreements" ng-if=item.agreements><div class=page-content><p class=help-block>Below is a list of each condition the creator of this interaction agreed on when submitting this interaction</p><div class=form-group ng-repeat="(key, agreement) in item.agreements"><label>{{key}}</label><div class=small>{{agreement}}</div></div></div></tab><tab heading="Transactions / Payments" ng-if="definition.paymentDetails.required || definition.paymentDetails.allow"><div class=page-content><div class="panel panel-default"><div class=panel-body><div class=row><div class="form-group col-sm-4"><label>Amount Payable</label><h3 class=text-danger>{{item.amount / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div><div class="form-group col-sm-4"><label>Amount Paid</label><h3 class=text-success>{{amountPaid() / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div><div class="form-group col-sm-4"><label><span ng-if="amountDue() < 0">Refund</span> Due</label><h3 ng-class="{\'text-danger\':amountDue() > 0, \'text-success\':amountDue() <= 0}">{{amountDue() / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div></div></div></div><div class=form-group><label>Transactions</label><content-select ng-model=item.transactions ng-params="{type:\'transaction\'}"></content-select></div><manual-payment-field ng-model=item.manualPayments></manual-payment-field></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/interaction/view.html", '<div class=interaction-view ng-controller=InteractionFormController><div class=page-content div printable=item.title><div ng-if="item.contacts || item.contact"><h3>Linked Contacts</h3><div class=list-group><div class=list-group-item ng-if=item.contact ng-click=$root.modal.view(item.contact)>{{item.contact.title | capitalizename}}</div><div class=list-group-item ng-click=$root.modal.view(contact) ng-repeat="contact in item.contacts">{{contact.title | capitalizename}}</div></div></div><div class="panel panel-default" ng-if="definition.paymentDetails.required || definition.paymentDetails.allow"><div class=panel-body><div class=row><div class="form-group col-sm-4"><label>Amount Payable</label><h3 class=text-danger>{{item.amount / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div><div class="form-group col-sm-4"><label>Amount Paid</label><h3 class=text-success>{{amountPaid() / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div><div class="form-group col-sm-4"><label><span ng-if="amountDue() < 0">Refund</span> Due</label><h3 ng-class="{\'text-danger\':amountDue() > 0, \'text-success\':amountDue() <= 0}">{{amountDue() / 100 | currency}} <em class="text-muted small text-uppercase">{{item.currency}}</em></h3></div></div></div></div><div ng-if="!item.contact && item.data._firstName" class="panel panel-default"><div class=panel-body><h3 ng-if="item.data._firstName || item.data._lastName"><span ng-if=item.data._firstName>{{item.data._firstName}}</span> <span ng-if=item.data._lastName>{{item.data._lastName}}</span></h3><div class=btn-group ng-if="item.data._email || item.data._phoneNumber"><a class="btn btn-default" ng-href=mailto:{{item.data._email}} ng-if=item.data._email><i class="fa fa-envelope"></i><span>{{item.data._email}}</span></a> <a class="btn btn-default" ng-href=tel:{{item.data._phoneNumber}} ng-if=item.data._email><i class="fa fa-phone"></i><span>{{item.data._phoneNumber}}</span></a></div></div></div><div view-extended-fields></div><div compile-html=item.body></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/list.html", '<div class=application-panel><script type=text/ng-template id=template-list.html><div class="realm-list">\n			<ul class="list-group">\n				<li class="list-group-item"  ng-click="createFromTemplate(template)" ng-repeat="template in templates">\n					{{template.title}}\n					<i class="fa fa-plus right"></i>\n				</li>\n			</ul>\n		</div></script><script type=text/ng-template id=filter-list-mobile.html><div class="filter-list-mobile">\n				<filter-block ng-model="search.filters" persist="true" ng-source="items" ng-filter-key="\'status\'" ng-filter-title="\'Status\'"></filter-block>\n				<filter-block ng-model="search.filters" ng-source="filteredItems" ng-filter-key="filter.key" ng-filter-title="filter.title" ng-repeat="filter in definition.filters"></filter-block>			\n				<filter-block ng-model="search.filters" ng-source="filteredItems" ng-filter-key="filter.key" ng-filter-title="filter.title" ng-repeat="filter in type.filters"></filter-block>			\n				<filter-block ng-model="search.filters" ng-source="filteredItems" ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block>\n				<filter-block ng-model="search.filters" ng-source="filteredItems" ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block>\n				<filter-block ng-model="search.filters" ng-source="filteredItems" ng-filter-key="\'author\'" ng-filter-title="\'Author\'"></filter-block>\n			</div></script><div class=application-header ng-class={expanded:expanded}><a class="btn btn-expand btn-xs visible-xs btn-block" ng-click="expanded = !expanded"><i class=fa ng-class="{\'fa-chevron-down\':!expanded, \'fa-chevron-up\':expanded}"></i></a><div class=header-options ng-class={expanded:expanded}><div class=type-icon><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1>{{titlePlural}} <span class=text-muted ng-if=search.terms>// \'{{search.terms}}\'</span></h1></div><a popover-template="\'filter-list-mobile.html\'" popover-append-to-body=true popover-placement=bottom class="btn visible-xs-block btn-default">Filter results</a><div class="pull-right text-right"><div class=form-inline><div class=btn-group ng-if=type.viewModes.length><a class="btn btn-default" ng-class="{active:currentViewMode(\'default\')}" ng-click="settings.viewMode = \'default\'"><i class="fa fa-bars"></i></a><a class="btn btn-default" ng-class={active:currentViewMode(viewMode)} ng-click="settings.viewMode = viewMode" ng-repeat="viewMode in type.viewModes"><i class="fa fa-{{viewMode}}"></i></a></div><div class=btn-group ng-if=canCreate()><a class="btn btn-primary" ng-if=uploadType ui-sref=upload({type:uploadName})><span>Upload {{titlePlural}}</span><i class="fa fa-cloud-upload"></i></a><a class="btn btn-primary" ui-sref={{type.path}}.create({definitionName:definition.definitionName})><span>Create {{titleSingular}}</span><i class="fa fa-plus"></i></a></div><div class="form-group search-form"><search ng-model=search.terms></search></div></div></div></div></div><div class=application-body><div class=application-columns><div class="filter-list hidden-xs" ng-if="filteredItems.length || filtersActive()"><filter-block ng-model=search.filters persist=true ng-source=items ng-filter-key="\'status\'" ng-filter-title="\'Status\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in definition.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in type.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'author\'" ng-filter-title="\'Author\'"></filter-block></div><div class=list-column><div class=application-panel><select-toolbar ng-model=selection ng-filtered=filteredItems ng-type=type ng-definitions=definitions ng-definition=definition ng-if=selection.items.length ng-plural=titlePlural></select-toolbar><empty-text ng-if=!filteredItems.length></empty-text><div class=application-body ng-if=filteredItems.length ng-switch=settings.viewMode><div ng-switch-when=map class=list-map><list-map ng-model=filteredItems ng-select-callback=selection.toggle></list-map></div><div ng-switch-when=grid class=list-grid><grid-list ng-model=pageItems ng-select-callback=selection.toggle></grid-list></div><duplicate-manager ng-switch-when=duplicates></duplicate-manager><default-list-view ng-switch-default></default-list-view></div><div ng-if="(filteredItems.length > pager.limit) && settings.viewMode != \'duplicates\'" class="application-footer text-center"><pagination max-size=pager.maxSize boundary-links=true items-per-page=pager.limit total-items=filteredItems.length ng-model=pager.currentPage ng-change=updatePage()></pagination></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/location/form.html", '<div ng-controller=LocationFormController class="location-form form"><tabset><tab heading="Location Details"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Address Line 1</label><input class=form-control ng-model=item.addressLine1 placeholder="Address Line 1"></div><div class=form-group><label>Address Line 2</label><input class=form-control ng-model=item.addressLine2 placeholder="Address Line 2"></div><div class=row><div class=col-md-6><div class=form-group><label>Suburb /City</label><input class=form-control ng-model=item.suburb placeholder="Suburb / City"></div></div><div class=col-md-3><div class=form-group><label>State</label><input class=form-control ng-model=item.state placeholder=State></div></div><div class=col-md-3><div class=form-group><label>Postal Code</label><input class=form-control ng-model=item.postalCode placeholder="Postal Code"></div></div></div><div class=form-group><label>Country</label><input class=form-control ng-model=item.country placeholder=Country></div></div></tab><tab heading="{{definition.title}} settings" ng-if=definition><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading=Rooms><div class=page-content><room-list ng-model=item.rooms></room-list></div></tab><tab heading="Location / Map" select="showMap = true" select="showMap = true"><location-map ng-if=showMap ng-location=item></location-map><div class=col-md-4><div class=page-content><label>{{item.title}}</label><p class=help>Enter the longitude and latitude below or click on the map to place location</p><div class=form-group><label>Latitude</label><input class=form-control ng-model=item.latitude placeholder=Latitude></div><div class=form-group><label>Longitude</label><input class=form-control ng-model=item.longitude placeholder=Longitude></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/location/view.html", "<div class=location-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/package/form.html", '<div class="package-form form"><div class=page-content><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Package Key</label><input required class=form-control ng-model=item.packageKey placeholder="Package Key"></div><div class=form-group><label>Files</label><div ng-repeat="file in item.files">{{file.localName}}</div></div><div extended-fields></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/package/view.html", "<div class=package-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/persona/form.html", '<div class="persona-form form"><div class=page-content><div ng-if=item.user><div class=row><div class="form-group col-xs-6"><label>First Name</label><div class=lead>{{item.user.firstName}}</div></div><div class="form-group col-xs-6"><label>Last Name</label><div class=lead>{{item.user.lastName}}</div></div></div><div class=form-group><label>Email Address</label><div class=lead>{{item.user.email}}</div></div><hr><div class=form-group><label>Change User Details</label><p class=help-block>To change this user\'s password or email details click the button below, this will send an invitation to the user to create a new password and then login to the system</p><a class="btn btn-primary" ng-disabled="resendState == \'processing\'" ng-switch=resendState ng-click=sendResetEmail()><span ng-switch-when=processing><span>Sending</span><i class="fa fa-spinner fa-spin"></i></span> <span ng-switch-default><span>Resend invitation email</span><i class="fa fa-envelope"></i></span></a></div></div><div ng-if=!item.user><div class=row><div class="form-group col-sm-6"><label>First Name</label><input class=form-control ng-model=item.firstName placeholder="First Name"></div><div class="form-group col-sm-6"><label>Last Name</label><input class=form-control ng-model=item.lastName placeholder="Last Name"></div></div><div ng-if=!item.managed><div class=form-group><label>Invite Email Address</label><p class=help-block>An invitation will be sent to this email address allowing the user to confirm and accept this persona</p><input class=form-control ng-model=item.collectionEmail placeholder="Email Address" type="email"></div><div class=form-group ng-if=item._id><a class="btn btn-primary" ng-disabled="resendState == \'processing\'" ng-switch=resendState ng-click=sendResetEmail()><span ng-switch-when=processing><span>Sending</span><i class="fa fa-spinner fa-spin"></i></span> <span ng-switch-default><span>Resend invitation email</span><i class="fa fa-envelope"></i></span></a></div><hr><div class=form-group ng-if=!item.collectionEmail><div class=checkbox><label><input type=checkbox ng-model="item.managed"> Create a managed user</label><p class=help-block>A managed user allows a child who may not have access to an email address to login with a username and password combination</p></div></div></div><div ng-if=item.managed><div class=form-group><label>Managed User</label><p class=help-block>Usernames must be unique within your account</p></div><div class=row><div class="form-group col-sm-6"><label>Username</label><input class=form-control ng-model=item.managedUsername placeholder="Managed Username"></div><div class="form-group col-sm-6"><label>Password</label><input class=form-control ng-model=item.managedPassword placeholder="Managed Password"></div></div></div></div><hr><div class=form-group><label>Permission Policies</label><p class=help-block>Create or select permission policies that can apply to multiple users.</p><content-select ng-model=item.policies ng-params="{type:\'policy\'}"></content-select></div><div class=form-group><label>Individual Permission Sets</label><p class=help-block>Select a combination of roles and realms to grant permissions to {{item.name}} as an individual</p><permission-set-manager ng-model=item.permissionSets></permission-set-manager></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/persona/view.html", "<div class=persona-view><div class=page-content></div></div>"), 
    $templateCache.put("fluro-admin-content/types/plan/form.html", '<div class="team-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><content-formly ng-model=item.data definition=definition></content-formly><div class=form-group><label>Event</label><content-select ng-params="{type:\'event\', canCreate:false, minimum:1, maximum:1}" ng-model=item.event></content-select></div><schedule-planner ng-model=item></schedule-planner></div></div>'), 
    $templateCache.put("fluro-admin-content/types/plan/view.html", '<div><div heading={{item.title}} ng-controller=AdminPlanViewController><div class=page-content><div printable=item.title class=plan-summary><div class="plan-header clearfix"><div class=hidden-print><h3 ng-if=event>{{event.title}}<div ng-if="item.status != \'active\'" class="label label-warning pull-right">{{item.status}}</div></h3><h4>{{item.title}}</h4><h5 ng-if=event>{{event.startDate | formatDate:\'l j F Y\'}} <em class=small>(Plan starts at {{item.startDate | formatDate:\'g:ia\'}})</em></h5><h5 ng-if=!event>Plan starts at {{item.startDate | formatDate:\'g:ia l j F Y\'}}</h5><a class="btn btn-default" ng-if=canEditPlan ng-click=editPlan(plan)><span>Edit this plan</span><i class="fa fa-edit"></i></a></div><div class=visible-print-block><h3 ng-if=event>{{event.title}}<div ng-if="item.status != \'active\'" class="label label-warning pull-right">{{item.status}}</div></h3><h4>{{item.title}}</h4><h5 ng-if=event>{{event.startDate | formatDate:\'l j F Y\'}} <em class=small>(Plan starts at {{item.startDate | formatDate:\'g:ia\'}})</em></h5><h5 ng-if=!event>Plan starts at {{item.startDate | formatDate:\'g:ia l j F Y\'}}</h5></div></div><div class="plan-options hidden-print"><div class=form-inline><label>Show</label><div class=team-checkbox-group ng-repeat="team in item.teams"><div ng-if=!isBlankColumn(team)><a class="btn btn-checkbox" ng-click=toggleContext(team) ng-class="{\'active\':!contextIsActive(team)}"><i class="fa fa-check"></i></a><span>{{team}}</span></div><div class=text-muted ng-if=isBlankColumn(team)>No {{team}} notes</div></div><div class=team-checkbox-group ng-if=event><a class="btn btn-checkbox" ng-click="item.showTeamOnSide = !item.showTeamOnSide" ng-class="{\'active\':item.showTeamOnSide}"><i class="fa fa-check"></i></a><span>Show team on side</span></div><div class="btn-group pull-right"><a class="btn btn-default btn-xs" ng-class={active:!item.fontSize} ng-click="item.fontSize = null">Sml</a> <a class="btn btn-default btn-xs" ng-class="{active:item.fontSize == 1}" ng-click="item.fontSize = 1">Med</a> <a class="btn btn-default btn-xs" ng-class="{active:item.fontSize == 1.2}" ng-click="item.fontSize = 1.2">Lrg</a> <a class="btn btn-default btn-xs" ng-class="{active:item.fontSize == 1.4}" ng-click="item.fontSize = 1.4">XL</a></div></div></div><div class=row><div ng-class="{\'col-xs-10\':item.showTeamOnSide, \'col-xs-12\':!item.showTeamOnSide}"><div class="panel panel-default"><div class=table-responsive><table class="table table-bordered table-striped"><thead><tr><th><i class="fa fa-time"></i> Time</th><th>Details</th><th class=note ng-class="{\'hidden\':contextIsActive(team) || isBlankColumn(team)}" ng-repeat="team in item.teams">{{team}}</th></tr></thead><tbody><tr ng-repeat="schedule in item.schedules track by $index"><td class="note note-time" style="font-size: {{item.fontSize}}em !important"><div ng-if=schedule.duration><strong>{{estimateTime(item.startDate, item.schedules, schedule) | formatDate:\'g:ia\' }}</strong><div><em ng-if=schedule.duration class=text-muted>{{schedule.duration}} mins</em></div></div></td><td class="note note-detail" style="font-size: {{item.fontSize}}em !important"><div ng-if="schedule.title || schedule.detail"><strong>{{schedule.title}}</strong><br><div class=wrap-normal ng-bind-html="schedule.detail | trusted"></div></div></td><td class=note style="font-size: {{item.fontSize}}em !important" ng-class="{\'hidden\':contextIsActive(team) || isBlankColumn(team)}" ng-repeat="team in item.teams"><div class=wrap-normal ng-bind-html="schedule.notes[team] | trusted"></div></td></tr></tbody></table></div></div></div><div ng-if="item.showTeamOnSide && event" class=col-xs-2><div class="panel panel-default"><div class=table-responsive><table class="table table-striped"><thead><th><i class="fa fa-contact"></i> Team</th></thead><tbody><tr ng-repeat="assignment in event.assignments"><td class=note style="font-size: {{item.fontSize}}em !important"><strong>{{assignment.title}}</strong><div ng-repeat="contact in assignment.contacts"><div ng-if="isConfirmed(contact, assignment)" class=confirmed><i class="fa fa-check hidden-print"></i> {{contact.title | capitalizename}}</div><div ng-if="isUnavailable(contact, assignment)" class=unavailable><i class="fa fa-times hidden-print"></i> {{contact.title | capitalizename}}</div><div ng-if="isUnknown(contact, assignment)">{{contact.title | capitalizename}}</div></div></td></tr></tbody></table></div></div></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/policy/form.html", '<div class="policy-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Description</label><p class=help-block>Enter a one line description summarizing what permissions this policy will grant</p><input class=form-control ng-model=item.description placeholder=Description></div><permission-set-manager ng-model=item.sets></permission-set-manager></div></div>'), 
    $templateCache.put("fluro-admin-content/types/policy/view.html", "<div class=policy-view><div class=page-content></div></div>"), 
    $templateCache.put("fluro-admin-content/types/post/form.html", '<div class="post-form form"><div class=page-content><div class=form-group ng-if="definition.data.titleGeneration != \'force\'"><label>{{titleLabel}}</label><input class=form-control placeholder={{titleLabel}} ng-model=item.title></div><div class=form-group><label>Parent</label><content-select ng-params="{minimum:1, maximum:1}" ng-model=item.parent></content-select></div><content-formly ng-model=item.data definition=definition></content-formly></div></div>'), 
    $templateCache.put("fluro-admin-content/types/post/view.html", "<div class=comment-view><div class=page-content><div compile-html=item.body></div><div view-extended-fields></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/product/form.html", '<div class="product-form form"><tabset><tab heading="Product information"><div class=page-content><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Amount</label><p class=help-block>The amount required for payment to create this type of content<br><em class=text-muted>A positive integer in the smallest currency unit (e.g 100 cents to charge $1.00)</em></p><div class=row><div class=col-sm-6><div class=input-group><div class=input-group-addon>{{item.amount / 100 | currency}}</div><input class=form-control type=number ng-model=item.amount placeholder="Amount in cents"></div></div><div class=col-sm-6><select class=form-control ng-model=item.frequency><option value=once ng-selected=!item.frequency>One time purchase</option><option value=weekly>Weekly</option><option value=monthly>Monthly</option><option value=annually>Annually</option></select></div></div></div><div class=form-group><label>Privacy / Availability</label><p class=help-block>Select the availabilty of this product from various stockists</p><select required class=form-control ng-model=item.privacy><option value=public>Public</option><option value=secure>Secure / Internal</option><option value=private>Private</option></select></div><div ng-switch=item.privacy><p class=help ng-switch-when=internal>This product will be accessible and purchaseable within internal stores or applications.</p><p class=help ng-switch-when=private>This product will be hidden and not purchaseable.</p><p class=help ng-switch-default>This product will be accessible and purchaseable through any fluro applications even those in other accounts.</p></div></div></tab><tab ng-if=definition heading="{{definition.title}} Information"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading=Contents><div class=page-content><div class=form-group><label>Items</label><content-select ng-model=item.items></content-select></div></div></tab><tab heading=Media><div class=page-content><div class=form-group><label>Images</label><content-select ng-params="{type:\'image\'}" ng-model=item.images></content-select></div><div class=form-group><label>Videos</label><content-select ng-params="{type:\'video\'}" ng-model=item.videos></content-select></div><div class=form-group><label>Assets</label><content-select ng-params="{type:\'asset\'}" ng-model=item.assets></content-select></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/product/view.html", "<div class=product-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/purchase/form.html", '<div class="product-form form"><div class=page-content><div class=form-group><label>Product</label><content-select ng-params="{minimum:1, maximum:1, type:\'product\'}" ng-model=item.product></content-select></div><hr><div class=form-group ng-if=item.distributor><label>Distributor</label><div>{{item.distributor.title}}</div></div><div class=form-group ng-if=item.purchaser><label>Purchased by</label><div>{{item.purchaser.name}}</div></div><div class=form-group ng-if=item.owner><label>Licensed to</label><div>{{item.owner.name}}</div></div><div class=form-group ng-if="item.owner && item.redemptionDate"><label>Redeemed</label><div>{{item.redemptionDate | formatDate:\'g:ia l j F Y\'}} <em class=text-muted>({{item.redemptionDate | timeago}})</em></div></div><hr><div class=form-group><div class=checkbox><label><input ng-model=item.expires type="checkbox"> Expire purchase</label></div></div><div class=form-group ng-if=item.expires><label>Expiry Date</label><p class=help-block>{{item.expiryDate | formatDate:\'g:ia l j F Y\'}} <em class=text-muted>({{item.expiryDate | timeago}})</em></p><dateselect ng-label="\'Expiry Date\'" use-time=true ng-model=item.expiryDate></dateselect></div><div ng-if=!item.owner><hr><div class=form-group><label>Collection Email</label><p class=help-block>An invitation will be sent to this email address for a person to collect</p><div class=row><div class=col-xs-12 ng-class="{\'col-sm-8\':item._id && item.collectionEmail}"><input class=form-control placeholder="Email Address" ng-model="item.collectionEmail"></div><div class=col-sm-4 ng-if="item._id && item.collectionEmail"><div ng-if="item.collectionEmail == originalEmail"><a class="btn btn-block btn-primary" ng-disabled="resendStatus == \'processing\'" ng-click=resendPurchaseInvitation()><span>Resend email notification</span> <i class=fa ng-class="{\'fa-angle-right\':resendStatus != \'processing\', \'fa-spinner fa-spin\':resendStatus == \'processing\'}"></i></a></div><div ng-if="item.collectionEmail != originalEmail"><a class="btn btn-block btn-default" ng-disabled=true><span>Save Changes First</span> <i class="fa fa-warning"></i></a></div></div></div></div></div><hr><div class=form-group><label>Redemption Code</label><input disabled class="form-control disabled" ng-model=item.redemptionCode placeholder="Redemption code will be generated upon save"></div><hr><div class=form-group><label>Transaction</label><content-select ng-params="{minimum:1, maximum:1, type:\'transaction\'}" ng-model=item.transaction></content-select></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/purchase/view.html", "<div class=purchase-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/query/form.html", '<div class="query-form form"><tabset><tab heading="Query Setup"><div class=page-content><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=row><div class=col-md-3><div class=form-group><label>Limit</label><p class=help-block>How many items to retrieve <em class=text-muted>(defaults to 20)</em></p><input type=number class=form-control ng-model=item.limit placeholder=Limit></div></div><div class=col-md-3><div class=form-group><label>Offset</label><p class=help-block>How many items to skip</p><input type=number class=form-control ng-model=item.offset placeholder=Offset></div></div><div class=col-md-3><div class=form-group><label>Select Fields</label><p class=help-block>Space seperated fields to select <em class=text-muted>(defaults to all fields)</em></p><input class=form-control ng-model=item.select placeholder="Select fields"></div></div><div class=col-md-3><div class=form-group><label>Sort By Field</label><p class=help-block>Sort by this field, (to sort in reverse prefix with \'-\') eg. \'-title\' will sort in reverse alphabetical by title</p><input class=form-control ng-model=item.sort placeholder=Sort></div></div></div><div class=row><div class=col-md-6><div class=form-group><label>Populate Fields</label><p class=help-block>Space seperated fields to populate <em class=text-muted>(defaults to all fields)</em></p><input class=form-control ng-model=item.populateFields placeholder="Populate Fields"></div></div><div class=col-md-6><div class=form-group><label>Populate Select Fields</label><p class=help-block>Space seperated fields to select on populated content<em class=text-muted>(defaults to all fields)</em></p><input class=form-control ng-model=item.populateSelect placeholder="Populate Select Fields"></div></div></div><div class=form-group><label>Default Variables</label><p class=help-block>Add default variables to use if none are provided</p><key-value-select ng-model=item.defaultVariables></key-value-select></div><div class=form-group ng-if=item._id><a target=_blank ng-href={{previewLink}} class="btn btn-primary">Test Query</a></div><div class=form-group><label>Query</label><p class=help-block>To use dynamic variables enter them as placeholders in the form of <em class=text-muted>variable(key)</em></p><div class=query-editor ng-model=item.query ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'json\'}"></div></div><div class=form-group><div class="privacy-select btn-group btn-group-justified"><a class="btn btn-privacy" ng-click="item.privacy = \'secure\'" ng-class="{\'active\':item.privacy == \'secure\'}"><i class="fa fa-lock"></i> <strong>Secure</strong><br><em class=small>Only users and applications with correct permissions can view</em></a> <a class="btn btn-privacy" ng-click="item.privacy = \'public\'" ng-class="{\'active\':item.privacy == \'public\'}"><i class="fa fa-globe"></i> <strong>Public</strong><br><em class=small>Anyone in the world can view</em></a></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and use this query</p></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/query/view.html", "<div class=query-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/reaction/form.html", '<div class="reaction-form form"><div class=application-panel><tabset class=application-body><tab heading="Reaction Settings"><div class=page-content><div class=form-group><label>Title</label><p class=help-block>Name of this reaction</p><input class=form-control ng-model=item.title placeholder="Name of this reaction"></div><div class=form-group><label>Action</label><p class=help-block>What action should be taken when this reaction is triggered</p><select class=form-control ng-model=item.action><option value=email>Send Notification Email</option><option value=sms>Send SMS</option><option value=invite>Invite as User</option><option value=request>Forward Request</option></select></div><div ng-switch=item.action><div ng-switch-when=sms>SMS Messaging is unavailable at this time</div><div ng-switch-when=request><p>Make a request to an external service</p><div class=row><div class="form-group col-sm-2"><label>Method</label><select ng-model=item.details.requestMethod class=form-control><option value=POST>POST</option><option value=GET>GET</option><option value=PUT>PUT</option><option value=DELETE>DELETE</option></select></div><div class="form-group col-sm-10"><label>URL</label><input ng-model=item.details.requestURL placeholder="http://" class="form-control"></div></div><div class=form-group><label>Request Headers</label><p class=help-block>Enter the URL to forward this request to</p><key-value-select ng-model=item.details.requestHeaders></key-value-select></div><div ng-if="item.details.requestMethod == \'POST\' || item.details.requestMethod == \'PUT\'"><div class=form-group><label>Format</label><select ng-model=item.details.requestFormat class=form-control><option value=json>JSON</option><option value=form>Form Data</option></select></div><div class=form-group><label>Request Body</label><p class=help-block>Structure the data to send with the request</p><div class=code-body ng-model=item.details.template ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'json\'}"></div></div><div class=form-group><label>Include items</label><p class=help-block>Include articles and other content in your template<br>eg. <em class=text-muted><%= items[0].body %></em> would print the body of the first included piece of content</p><content-select ng-model=item.items></content-select></div><div><h2>Help / Instructions</h2><p>The email template uses EJS to allow formatting and templating.<br>More information on EJS <a href="http://www.embeddedjs.com/">can be found here</a></p><h4>Available variables</h4><ul><li><strong>interaction</strong><br><p class=help-block>This is the entire interaction object. eg.<pre><%= interaction.title %></pre>would print the title of the interaction</p></li><li><strong>reaction</strong><br><p class=help-block>This is the entire reaction object. (What you are creating/editing now) eg.<pre><%= reaction.title %></pre>would print <strong>\'{{item.title}}\'</strong>.</p></li><li><strong>items</strong><br><p class=help-block>This is an array of included items specified above<pre><%- items[0].body %></pre>would print the first included item\'s body, note that the &lt;%- is used instead of &lt;%= so we can escape the html text,<pre><%= items[1].title %></pre>will print the title of the second include<pre><% for (var i = 0; i < items.length; i++) { %>&lt;h3&gt;<%= items[i].title; %>&lt;/h3&gt;<%- items[i].body; %><% } %>\n												</pre>will print each article with a title heading and body</p></li></ul></div></div></div><div ng-switch-when=invite><div class="panel panel-default"><div class=panel-heading><h5 class=panel-title>Invite as User</h5><p class=help-block>This will create a user account for contact that initiated this interaction</p></div><tabset><tab heading=Permissions><div class=panel-body><div class=form-group><label>Permission Policies</label><p class=help-block>Select permission policies to grant to the new user</p><content-select ng-params="{type:\'policy\'}" ng-model=item.details.newUserPolicies></content-select></div></div></tab><tab heading=Realms><div class=panel-body><div class=form-group><label>Realms</label><p class=help-block>Select which realm the new user should be added to</p><user-realm-select ng-model=item.details.newUserRealms></user-realm-select></div></div></tab><tab heading="Invitation Settings"><div class=panel-body><div class=form-group><label>From Name</label><p class=help-block>Name that the invitation email will be sent from</p><input ng-model=item.details.inviteFromName class="form-control"></div><div class=form-group><label>From Email</label><p class=help-block>Email that the invitation will be sent from</p><input ng-model=item.details.inviteFromEmail class="form-control"></div><hr><div class=form-group><label>Redirect to Application</label><p class=help-block>Redirect the user to an application after invitation is accepted</p><content-select ng-params="{type:\'application\', allDefinitions:true, maximum:1}" ng-model=item.details.targetApplication></content-select></div><div class=form-group><label>Redirect to specific URL</label><p class=help-block>Redirect the user to a specific url after invitation is accepted <em class=text-muted>This will override any applications selected above</em></p><input ng-model=item.details.targetURL placeholder="http://www.fluro.io/" class="form-control"></div></div></tab></tabset></div></div><div ng-switch-when=email><div class="panel panel-default"><div class=panel-heading><h5 class=panel-title>Send to</h5></div><tabset><tab heading=Contacts><div class=panel-body><div class=form-group><label>Send to contacts</label><content-select ng-model=item.contacts ng-params="{type:\'contact\'}"></content-select></div></div></tab><tab heading=Teams><div class=panel-body><div class=form-group><label>Send to Teams</label><content-select ng-model=item.teams ng-params="{type:\'team\'}"></content-select></div></div></tab><tab heading="Individual Emails"><div class=panel-body><div class=form-group><label>Send to email addresses</label><multi-value-field placeholder="Type to add email addresses" ng-model=item.details.emails></multi-value-field></div></div></tab><tab heading="Interaction creator"><div class=panel-body><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.details.sendToCreator"> Send to the contact or user that created the initial content</label><p class=help-block>If the content is an interaction it will send this reaction to the primary contact connected to the interaction</p></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.details.sendToAllConnectedContacts"> Send to all connected contacts</label><p class=help-block>Send reaction to any contacts with an email address that were created or linked to this interaction will receive this email</p></div></div></div></tab><tab heading="Addresses from content"><div class=panel-body><div class=form-group><label>Send to emails from submitted fields</label><p class=help-block>Add keys that contain valid email addresses, if the key is an array all emails included in the array will be used</p><multi-value-field placeholder="Add path from main content" ng-model=item.details.sendToValues></multi-value-field></div></div></tab></tabset></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Subject</label><p class=help-block>Defaults to the generated title of the interaction</p><input class=form-control placeholder=Subject ng-model="item.details.subject"></div></div></div><div class="panel panel-default"><div class=panel-heading><h5 class=panel-title>From</h5></div><div class=panel-body><div class=form-group><label>From Name</label><p class=help-block>This is the name that the email will be sent from<br><em class=text-muted>If none provided, the name of the contact who triggered this reaction will be used. If a name can not retrieved then \'{{$root.user.account.title}}\' will be used as the from name</em></p><input class=form-control placeholder="Type to add name" ng-model="item.details.fromName"></div><div class=form-group><label>From Email Address</label><p class=help-block>This is the email address that this email will be sent from<br><em class=text-muted>If none provided, the email of the contact who triggered this reaction will be used. If an email can not retrieved then \'donotreply@fluro.io\' will be used as the sender</em></p><input class=form-control placeholder="Type to add email addresses" ng-model="item.details.fromEmail"></div></div></div></div></div></div></tab><tab ng-if=definition heading="{{definition.title}} information"><div class=page-content><content-formly ng-model=item.data definition=definition></content-formly></div></tab><tab heading="Email Template" ng-if="item.action == \'email\'"><div class=application-body><div class=code-body ng-model=item.html ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div><div class=page-content><div class=form-group><label>Include items</label><p class=help-block>Include articles and other content in your template<br>eg. <em class=text-muted><%= items[0].body %></em> would print the body of the first included piece of content</p><content-select ng-model=item.items></content-select></div><div class=form-group><label>Stylesheets</label><p class=help-block>Include stylesheets for your template</p><content-select ng-params="{type:\'code\'}" ng-model=item.styles></content-select></div><div><h2>Help / Instructions</h2><p>The email template uses EJS to allow formatting and templating.<br>More information on EJS <a href="http://www.embeddedjs.com/">can be found here</a></p><h4>Available variables</h4><ul><li><strong>interaction</strong><br><p class=help-block>This is the entire interaction object. eg.<pre><%= interaction.title %></pre>would print the title of the interaction</p></li><li><strong>reaction</strong><br><p class=help-block>This is the entire reaction object. (What you are creating/editing now) eg.<pre><%= reaction.title %></pre>would print <strong>\'{{item.title}}\'</strong>.</p></li><li><strong>to</strong><br><p class=help-block>This is the \'to\' data of the contact we are sending to eg.<pre><%= to.name %></pre>would print the contact\'s first name,<pre><%= to.email %></pre>will print the email address we are sending to</p></li><li><strong>items</strong><br><p class=help-block>This is an array of included items specified above<pre><%- items[0].body %></pre>would print the first included item\'s body, note that the &lt;%- is used instead of &lt;%= so we can escape the html text,<pre><%= items[1].title %></pre>will print the title of the second include<pre><% for (var i = 0; i < items.length; i++) { %>&lt;h3&gt;<%= items[i].title; %>&lt;/h3&gt;<%- items[i].body; %><% } %>\n										</pre>will print each article with a title heading and body</p></li></ul></div></div></div></tab><tab heading=Test ng-if="item.action == \'email\'"><div class=application-body><div class=page-content><p>Simulate what the reaction will look like by testing with an existing interaction</p><div class=form-group><label>Test Interaction</label><p class=help-block>Select an existing interaction to simulate the \'interaction\' variable</p><content-select ng-model=item.testContent ng-params="{type:\'interaction\', maximum:1}"></content-select></div><reaction-tester ng-reaction=item._id ng-interaction=item.testContent._id></reaction-tester></div></div></tab></tabset></div></div>'), 
    $templateCache.put("fluro-admin-content/types/reaction/view.html", "<div class=contact-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/realm/form.html", '<div class="realm-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Realm label preview</label><br><div class=inline-tag style="background-color: {{item.bgColor}}; color: {{item.color}}">{{item.title}}</div></div><div class=row><div class=col-md-6><div class=form-group><label>Color</label><br><div class=input-group><span class=input-group-addon colorpicker=hex ng-model=item.color><swatch ng-model=item.color></swatch></span> <input class=form-control ng-model=item.color></div></div></div><div class=col-md-6><div class=form-group><label>Background Color</label><br><div class=input-group><span class=input-group-addon colorpicker=hex ng-model=item.bgColor><swatch ng-model=item.bgColor></swatch></span> <input class=form-control ng-model=item.bgColor></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/realm/view.html", "<div class=realm-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/role/form.html", '<div ng-controller=RoleFormController class="role-form form"><tabset class=flex-tabs><tab heading="Basic Information"><div class=page-content><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><hr><h5>Application Access</h5><p class=help-block>Select applications that users with this role should have access to</p><content-select ng-params="{type:\'application\', searchInheritable:true}" ng-model=item.applicationKeys></content-select></div></tab><tab heading="Basic Permissions"><div class=page-content><h3>Basic Permissions</h3><permission-select ng-model=item.permissions ng-permission-options=permissions></permission-select></div></tab><tab heading="Definition Permissions"><div class=page-content><h3>Definition Permissions</h3><permission-select ng-model=item.genericPermissions ng-permission-options=genericPermissions></permission-select></div></tab><tab heading="Custom Permissions"><div class=page-content><h3>Custom Permissions</h3><p class=help-block>Assign your own string based custom permissions <em>(these permissions will be attached to the users session prefixed with \'custom \')</em></p><div class=form-group><multi-value-field ng-model=item.customPermissions placeholder="Permission name"></multi-value-field></div></div></tab><tab heading=Configuration><div class=page-content><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="item.inheritable"> Inheritable</label><p class=help-block>Allow child accounts to reference and use this query</p></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/role/view.html", "<div class=role-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/site/form.html", '<div ng-controller=SiteFormController class="site-form form"><tabset class=flex-tabs><tab heading=Pages><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder=Title ng-model=item.title></div><div class=row><div class=col-sm-3><script type=text/ng-template id=site-route.html><div class="site-tree-branch" ng-class="{active:selection.item == (route)}" ng-click="selection.select(route)" ng-if="route.type != \'folder\'">\n								<i class="fa fa-file-o text-muted"></i> {{route.title}}\n							</div>\n							<div class="site-tree-folder" ng-if="route.type == \'folder\'">\n								<div class="site-tree-folder-heading">\n									<i class="fa fa-folder-open-o text-muted"></i> {{route.title}}\n								</div>\n								<div ng-if="route.routes.length" class="site-tree-folder-contents">\n									<div ng-include="\'site-route.html\'" ng-repeat="route in route.routes"></div>\n								</div>\n							</div></script><div class=site-tree><div ng-include="\'site-route.html\'" ng-repeat="route in item.routes"></div></div></div><div class=col-sm-9 ng-if=selection.item><div class=page-header><h2>{{selection.item.title}}</h2><h4 class=text-muted>{{selection.item.url}}</h4></div><tabset type=pills><tab heading=Sections><div class="panel panel-default" ng-class={disabled:section.disabled} ng-repeat="section in selection.item.sections"><div class=panel-heading>{{section.title}} <em class=text-muted ng-if=section.disabled>(Disabled)</em></div><tabset ng-if=!section.disabled><tab heading=Content><div class=panel-body><label>Content</label><content-select ng-model=section.content></content-select></div></tab><tab heading=Collections ng-if="$root.access.canAccess(\'collection\')"><div class=panel-body><label>Collections</label><content-select ng-model=section.collections></content-select></div></tab><tab heading=Queries ng-if="$root.access.canAccess(\'query\')"><div class=panel-body><label>Queries</label><content-select ng-model=section.queries></content-select></div></tab></tabset></div></tab><tab heading="SEO / Social"><div class="panel panel-default"><div class=panel-heading>SEO / Social</div><tabset><tab heading="Page SEO"><div class=panel-body><p class=help-block>This information will be what is submitted when this page is shared to social media websites like facebook and twitter.<br>If nothing is entered it will inherit the SEO information from the page or the site itself</p><div class=form-group><label>Meta Title</label><input ng-model=selection.item.seo.title class=form-control placeholder="SEO Title"></div><div class=form-group><label>Meta Description</label><textarea ng-model=selection.item.seo.description class=form-control placeholder="SEO Meta Description">\n													</textarea></div><div class=form-group><label>Share/SEO Image</label><content-select ng-model=selection.item.seo.image ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div></div></tab><tab heading="Slug SEO" ng-if=hasSlug(selection.item)><div class=panel-body><p class=help-block>To replace SEO details with information from the slug content, type the field key in to the inputs below</p><div class=form-group><label>Meta Title</label><input ng-model=selection.item.seo.slugTitle class=form-control placeholder="title"></div><div class=form-group><label>Meta Description</label><input ng-model=selection.item.seo.slugDescription class=form-control placeholder="body"></div><div class=form-group><label>Share/SEO Image</label><input ng-model=selection.item.seo.slugImage class=form-control placeholder="image"></div></div></tab></tabset></div></tab></tabset></div></div></div></tab><tab heading=Versions ng-if=versions.length><div class=page-content><div class="panel panel-default"><div class=table-responsive><table class=table><thead><th>Message</th><th>Ago</th><th>Date</th><th>Author</th><th class=text-right></th></thead><tbody><tr ng-repeat="version in versions | orderBy:\'-date\'"><td>{{version.title}}</td><td><em>{{version.date | timeago}}</em></td><td>{{version.date | formatDate:\'g:ia - l M Y\'}}</td><td>{{version.author.name}}</td><td class=text-right><a class="btn btn-default"><span>View</span></a></td></tr></tbody></table></div></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/site/view.html", "<div class=site-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/tag/form.html", '<div class="tag-form form"><div class=page-content><div class=form-group><label>Title</label><input required class=form-control ng-model=item.title placeholder=Title></div><div class=row><div class="form-group col-md-6"><label>Realms</label><p class=help>Limit this tag to these realms</p><realm-select ng-model=item.realms></realm-select></div><div class="form-group col-md-6"><label>Types</label><p class=help>Limit this tag to these types of content, If none are selected this tag will be useable on all types</p><content-type-select ng-model=item.restrictTypes></content-type-select></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/tag/view.html", "<div class=tag-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/task/form.html", "<div class=\"task-form form\"><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder=Title ng-model=item.title></div><div class=form-group><label>Run date</label><dateselect ng-label=\"'Run Date'\" use-time=true ng-model=item.runDate></dateselect></div><label>Query</label><p class=help-block>Write a custom query. The results of this query will be affected by the actions specified below</p><div class=query-editor ng-model=item.query ui-ace=\"{useWrapMode : false, showGutter: true, theme:'tomorrow_night_eighties', mode: 'json'}\"></div><h4>Actions</h4><p class=help-block>Configure actions to modify content when this task is run.</p><table class=\"table table-striped table-bordered\"><thead><th>Action</th><th>Parameters</th></thead><tbody><tr><td>Add to Realms</td><td>Leightons</td></tr><tr><td>Remove From Realms</td><td>Provisioned</td></tr><tr><td>Update Status</td><td>'active'</td></tr><tr><td>Run again</td><td>in 28 Days</td></tr></tbody></table><div ng-if=definition><content-formly ng-model=item.data definition=definition></content-formly></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/team/form.html", '<div class="team-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><content-formly ng-model=item.data definition=definition></content-formly><div class=form-group><contact-assign-select ng-model=item.assignments></contact-assign-select></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/team/view.html", '<div class=team-view><div class=page-content><div class=page-content><div class=team-overview><h4><i class="fa fa-contact"></i> Team</h4><div class=assignment-row ng-repeat="assignment in item.assignments"><div class=row><div class="assignment-label col-xs-6"><strong>{{assignment.title}}</strong></div><div class=col-xs-6><div ng-repeat="contact in assignment.contacts">{{contact.title}}</div></div></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/transaction/form.html", '<div class="transaction-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control ng-model=item.title placeholder=Title></div><div class=form-group><label>Data</label><pre>{{item | json }}</pre></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/transaction/view.html", "<div class=transaction-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div><div class=form-group><label>Data</label><pre>{{item | json }}</pre></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/trash.html", '<div class=application-panel><div class=application-header><div class=type-icon><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1>Trashed {{titlePlural}} <span class=text-muted ng-if=search.terms>// \'{{search.terms}}\'</span></h1></div><div class="pull-right text-right"><div class=form-inline><search ng-model=search.terms></search></div></div></div><div class=application-body><div class=application-columns><div class=filter-list ng-if="filteredItems.length || filtersActive()"><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in definition.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key=filter.key ng-filter-title=filter.title ng-repeat="filter in type.filters"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'realms\'" ng-filter-title="\'Realms\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'tags\'" ng-filter-title="\'Tags\'"></filter-block><filter-block ng-model=search.filters ng-source=filteredItems ng-filter-key="\'author\'" ng-filter-title="\'Author\'"></filter-block></div><div class=list-column><div class=application-panel><trash-select-toolbar ng-done=done ng-type=type ng-model=selection ng-definition=definition ng-if=selection.items.length ng-plural=titlePlural></trash-select-toolbar><empty-text ng-if=!filteredItems.length></empty-text><div class=application-body ng-if=filteredItems.length><trash-list-view></trash-list-view></div><div ng-if="filteredItems.length > pager.limit" class="application-footer text-center"><pagination max-size=pager.maxSize boundary-links=true items-per-page=pager.limit total-items=filteredItems.length ng-model=pager.currentPage ng-change=updatePage()></pagination></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/user/form.html", '<div class="user-form form"><tabset><tab heading="User details"><div class=page-content><div><div class=form-group><label>Username</label><input class=form-control ng-disabled=!canEditAuthDetails ng-model=item.name placeholder=Name></div><div class=row><div class=col-md-6><div class=form-group><label>First Name</label><input class=form-control ng-disabled=!canEditAuthDetails ng-model=item.firstName placeholder="First Name"></div></div><div class=col-md-6><div class=form-group><label>Last Name</label><input class=form-control ng-disabled=!canEditAuthDetails ng-model=item.lastName placeholder="Last Name"></div></div></div><div class=form-group><label>Password</label><input type=password ng-disabled=!canEditAuthDetails class=form-control ng-model=item.password minlength=6 placeholder=Password></div><div class=form-group ng-if="item._id != $root.user._id"><label>Email Address</label><input ng-class="{error:hasError(\'email\')}" ng-disabled=!canEditAuthDetails type=email class=form-control ng-model=item.email placeholder="Email Address"></div></div><div class="panel panel-default" ng-if=item._id><div class=panel-body><p class=help-block>To reset this user\'s password click the button below, this will send an invitation for the user to create a new password and then login to the system</p><div ng-switch=resendState><div class="btn btn-default" ng-switch-when=processing><span>Sending</span><i class="fa fa-spinner fa-spin"></i></div><div ng-switch-default><a class="btn btn-primary" ng-click=resendEmail()><span>Resend invitation email</span><i class="fa fa-mail"></i></a></div></div></div></div><div class="panel panel-default" ng-if=!item._id><div class=panel-heading><a class="btn btn-checkbox" ng-click="item.sendInvite = !item.sendInvite" ng-class="{\'active\':item.sendInvite}"><i class="fa fa-check"></i></a> Send invitation email</div><div class=panel-body ng-if=item.sendInvite><div class=row><div class=col-md-6><div class=form-group><label>Personal message</label><p class=help-block>Write a custom message that will be sent by email to this user.<br>A link will be included at the bottom of the email allowing the user to set their password and login</p><div class=standard-body-wrapper><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor="{buttons:[\'bold\', \'italic\'], plugins:{}}" ng-model=item.invitationMessage cols=30 rows=10></textarea></div></div></div></div><div class=col-md-6><div class=form-group><label>Email preview</label><p class=help-block>This email will be sent from: <em>{{$root.user.email}}</em></p><div class=well><p><strong>Subject:</strong> {{$root.user.name}} has invited you to join {{$root.user.account.title}}</p><hr><p>Hi {{item.name}},</p><div ng-bind-html="item.invitationMessage | trusted"></div><p><strong>Please click on the link below to accept {{$root.user.name}}\'s invitation and to set your password</strong></p><p>[LINK]</p></div></div></div></div><div class=form-group><label>Redirect to Application</label><p class=help-block>Select which application to forward the user to once they have accepted your invitation.<br><em class=text-muted>If none selected they will be forwarded to the www.fluro.io landing page with shortcuts to particular applications</em></p><content-select ng-params="{type:\'application\', allDefinitions:true, maximum:1}" ng-model=item.targetApplication></content-select></div><div class=form-group><label>Redirect to URL</label><p class=help-block>Select a url to forward the user to once they have accepted your invitation.<br><em class=text-muted>this will override any application selected above</em></p><input class=form-control ng-model=item.targetURL placeholder=http://www.fluro.io></div></div></div><div ng-if=allowAccountChange class=form-group><label>Active Account</label><select class=form-control ng-model=item.account><option ng-repeat="account in item.availableAccounts | orderBy:\'title\'" ng-selected="account._id == item.account._id" value={{account._id}}>{{account.title}}</option></select></div><div class=row style="display:none !important"><div ng-if=allowRoleChange class=col-md-6><div class=form-group><label><i class="fa fa-role"></i> Roles</label><user-role-select ng-model=item.roles></user-role-select></div></div><div ng-if=allowRealmChange class=col-md-6><div class=form-element><label><i class="fa fa-realm"></i> Realms</label><user-realm-select ng-model=item.realms></user-realm-select></div></div></div></div></tab><tab heading="Permission Sets"><div class=page-content><h4>Permission Policies</h4><p class=help-block>Create or select permission policies that can apply to multiple users.<br></p><div class="panel panel-default"><div class=panel-heading><h4 class=panel-title>Policies</h4></div><div class=panel-body><div class=form-group><policy-manager ng-model=item.policies></policy-manager></div></div></div><div ng-repeat="set in allowedPermissionSets"><h4>Individual permissions</h4><p class=help-block>Select a combination of roles and realms to grant permissions to {{item.name}} as an individual</p><permission-set-manager ng-model=set.sets></permission-set-manager></div></div></tab></tabset></div>'), 
    $templateCache.put("fluro-admin-content/types/user/view.html", "<div class=user-view><div class=page-content><div view-extended-fields></div><div compile-html=item.body></div></div></div>"), 
    $templateCache.put("fluro-admin-content/types/video/form.html", '<div class="video-form form"><div class=page-content><div class=form-group><label>Title</label><input class=form-control placeholder="Filename will be used if left blank" ng-model=item.title></div><div ng-if=!item.assetType class=form-group><label>Asset Type</label><select class=form-control ng-model=item.assetType><option value=youtube>Youtube</option><option value=vimeo>Vimeo</option><option value=upload>Upload / Hosted</option></select></div><div ng-switch=item.assetType><div ng-switch-when=upload><div ng-if=item._id class="video-element form-group"><div class=video-wrapper><video controls><source ng-src="{{getAssetUrl(item._id) | trustedResource}}" type="{{item.mimetype}}"></video></div></div><asset-replace-form></asset-replace-form></div><div ng-switch-when=youtube class=row><div class=col-md-6><div class=form-group><label>Youtube URL</label><input class=form-control ng-model="item.external.youtube"></div><div class=video-wrapper><youtube-video video-url=item.external.youtube></youtube-video></div></div></div><div ng-switch-when=vimeo class=row><div class=col-md-6><div class=form-group><label>Vimeo URL</label><input class=form-control ng-model="item.external.vimeo"></div><div class=video-wrapper><vimeo-video video-url=item.external.vimeo player-vars="{portrait:0, badge:0}"></vimeo-video></div></div></div></div><content-formly ng-model=item.data definition=definition></content-formly><div class=standard-body-wrapper><label>Body</label><div class="article-toolbar cf"><div id=article-toolbar></div></div><div class=article-body><textarea class=article-body redactor ng-model=item.body cols=30 rows=10></textarea></div></div><div class=form-group><div class="privacy-select btn-group btn-group-justified"><a class="btn btn-privacy" ng-click="item.privacy = \'secure\'" ng-class="{\'active\':item.privacy == \'secure\'}"><i class="fa fa-lock"></i> <strong>Secure</strong><br><em class=small>Only users and applications with correct permissions can view</em></a> <a class="btn btn-privacy" ng-click="item.privacy = \'public\'" ng-class="{\'active\':item.privacy == \'public\'}"><i class="fa fa-globe"></i> <strong>Public</strong><br><em class=small>Anyone in the world can view</em></a></div></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/video/view.html", '<div class=video-view><div class=page-content><div ng-switch=item.assetType><div ng-switch-when=upload><div ng-if=item._id class=video-element><div class=video-wrapper><video controls><source ng-src="{{getAssetUrl(item._id) | trustedResource}}" type="{{item.mimetype}}"></video></div></div></div><div ng-switch-default><fluro-video ng-model=item></fluro-video></div></div><div view-extended-fields></div><div class=form-group><strong>Security / Privacy</strong> {{item.privacy}}</div><div class=form-group><strong>Filesize</strong> {{item.filesize | filesize}}</div><div compile-html=item.body></div></div></div>'), 
    $templateCache.put("fluro-admin-content/types/view.html", '<div class=application-panel><notifications ng-if=$dismiss></notifications><div class=application-header ng-class={expanded:expanded}><a class="btn btn-expand btn-xs visible-xs btn-block" ng-click="expanded = !expanded"><i class=fa ng-class="{\'fa-chevron-down\':!expanded, \'fa-chevron-up\':expanded}"></i></a><div class=header-options ng-class={expanded:expanded}><div class=type-icon><i class="fa fa-{{type.path}}"></i></div><div class=pull-left><h1 ng-if="type.path != \'user\' && type.path != \'contact\'">{{item.title}} <span class=text-muted>// {{titleSingular}}</span></h1><h1 ng-if="type.path == \'user\'">{{item.name | capitalizename}} <span class=text-muted>// {{titleSingular}}</span></h1><h1 ng-if="type.path == \'contact\'">{{item.title | capitalizename}} <span class=text-muted>// {{titleSingular}}</span></h1></div><div class="pull-right text-right"><a class="btn btn-default" ng-if="item.assetType == \'upload\'" ng-href={{getDownloadUrl(item._id)}} target=_blank><span>Download</span><i class="fa fa-download"></i></a> <a class="btn btn-default" ng-if=canEdit(item) ng-click=editItem()><span>Edit</span><i class="fa fa-edit"></i></a> <a class="btn btn-default" ng-click=cancel()><span>Close</span><i class="fa fa-times"></i></a></div></div></div><div class=application-body><div ng-include=template class=form-template-wrapper></div></div><div class=application-footer ng-if="type.path != \'user\'"><div class=clearfix><div class=realm-summary ng-if=realmSelectEnabled><i class="fa fa-realm"></i> <span ng-repeat="realm in item.realms">{{realm.title}}</span></div><div class=pull-right><div class=updated-summary>Last updated <span ng-if=item.updatedBy>by {{item.updatedBy}} -</span> {{item.updated | timeago}}</div><div class=status-summary><div class="btn btn-default btn-disabled" disabled><span>{{item.status}}</span> <i class="fa fa-status-{{item.status}}"></i></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-component-preview/fluro-component-preview.html", "<div class=fluro-component-preview></div>"), 
    $templateCache.put("fluro-component-select/fluro-component-select.html", '<div class="fluro-component-select panel panel-default"><div class="panel-heading clearfix"><h4 class=pull-left>Available components</h4><a class="btn btn-default pull-right" ng-click=reloadComponents()><i class=fa ng-class="{\'fa-refresh\': refreshStatus != \'loading\', \'fa-spinner fa-spin\':refreshStatus == \'loading\'}"></i></a></div><div class=panel-body><input class=form-control ng-model=search.terms placeholder="Search"></div><div class=scroller><div class=list-group><div class=list-group-item ng-class="{\'active\':isSelected(item)}" ng-repeat="item in items | filter:search.terms"><div class=form-group><a class="btn btn-checkbox" ng-class="{\'active\':isSelected(item)}" ng-click=toggle(item)><i class="fa fa-check"></i></a> {{item.title}}<br><em class="small text-muted">{{item.account.title}}</em></div><div class="btn-group btn-group-justified"><a class="btn btn-default btn-xs" ng-click=previewComponent(item)>Read Instructions</a> <a class="btn btn-default btn-xs" ng-click=reloadComponent(item)><span>Reload</span><i class="fa fa-refresh"></i></a></div></div></div></div></div>'), 
    $templateCache.put("fluro-dob-select/fluro-dob-select.html", '<div><div class=form-group ng-hide=hideAge><label>Age</label><input type=number select-on-focus ng-model-options="{debounce: { \'default\': 400, \'blur\': 0 } }" class=form-control pattern=[0-9]* placeholder=Age ng-model=age inputmode=numeric></div><div class=form-group ng-hide=hideDates><label>Date of Birth</label><div class=row><div class=col-xs-3><select name=dateFields.day data-ng-model=dateFields.day placeholder=Day class=form-control ng-options="day for day in days" ng-change=checkDate() ng-disabled=disableFields></select></div><div class=col-xs-5><select name=dateFields.month data-ng-model=dateFields.month placeholder=Month class=form-control ng-options="month.value as month.name for month in months" value={{dateField.month}} ng-change=checkDate() ng-disabled=disableFields></select></div><div class=col-xs-4><select ng-if=!yearText name=dateFields.year data-ng-model=dateFields.year placeholder=Year class=form-control ng-options="year for year in years" ng-change=checkDate() ng-disabled=disableFields></select><input ng-if=yearText name=dateFields.year data-ng-model=dateFields.year placeholder=Year class=form-control ng-disabled=disableFields></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/button-select/fluro-button-select.html", '<div id={{options.id}} class="button-select {{to.definition.directive}}-buttons" ng-model=model[options.key]><a ng-repeat="(key, option) in to.options" ng-class={active:contains(option.value)} class="btn btn-default" id="{{id + \'_\'+ $index}}" ng-click=toggle(option.value)><span>{{option.name}}</span><i class="fa fa-check"></i></a></div>'), 
    $templateCache.put("fluro-interaction-form/custom.html", "<div ng-model=model[options.key] compile-html=to.definition.template></div>"), 
    $templateCache.put("fluro-interaction-form/date-select/date-selector.html", '<div class=row><div class=col-xs-3><select name=dateFields.day data-ng-model=dateFields.day placeholder=Day class=form-control ng-options="day for day in days" ng-change=checkDate()><option value="" disabled selected>Day</option></select></div><div class=col-xs-5><select name=dateFields.month data-ng-model=dateFields.month placeholder=Month class=form-control ng-options="month.value as month.name for month in months" value={{dateField.month}} ng-change=checkDate()><option value="" disabled selected>Month</option></select></div><div class=col-xs-4><input type=number name=dateFields.year min=0 max=9999 data-ng-model=dateFields.year placeholder=YYYY class=form-control></div></div>'), 
    $templateCache.put("fluro-interaction-form/date-select/fluro-date-select.html", "<div><div class=visible-xs-block><input type=date class=form-control ng-model=model[options.key] ng-required=to.required></div><div class=hidden-xs><fluro-date-select ng-model=model[options.key]></fluro-date-select></div></div>"), 
    $templateCache.put("fluro-interaction-form/dob-select/fluro-dob-select.html", "<div class=fluro-interaction-dob-select><dob-select ng-model=model[options.key] hide-age=to.params.hideAge hide-dates=to.params.hideDates></dob-select></div>"), 
    $templateCache.put("fluro-interaction-form/embedded/fluro-embedded.html", '<div class=fluro-embedded-form><div class=form-multi-group ng-if="to.definition.maximum != 1"><div class="panel panel-default" ng-init="fields = copyFields(); dataFields = copyDataFields(); " ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><a ng-if=canRemove() class="btn btn-danger btn-sm pull-right" ng-click="model[options.key].splice($index, 1)"><span>Remove {{to.label}}</span><i class="fa fa-times"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form></div></div><a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/field-errors.html", '<div class=field-errors ng-if="fc.$touched && fc.$invalid"><div ng-show=fc.$error.required class="alert alert-danger" role=alert><i class="fa fa-exclamation" aria-hidden=true></i> <span class=sr-only>Error:</span> {{to.label}} is required.</div><div ng-show="fc.$viewValue.length && fc.$error.validInput" class="alert alert-danger" role=alert><i class="fa fa-exclamation" aria-hidden=true></i> <span class=sr-only>Error:</span> <span ng-if=!to.errorMessage.length>\'{{fc.$viewValue}}\' is not a valid value</span> <span ng-if=to.errorMessage.length>{{to.errorMessage}}</span></div><div ng-show="fc.$viewValue.length && fc.$error.email" class="alert alert-danger" role=alert><i class="fa fa-exclamation" aria-hidden=true></i> <span class=sr-only>Error:</span> <span>\'{{fc.$viewValue}}\' is not a valid email address</span></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-interaction-input.html", '<div class="fluro-input form-group" scroll-active ng-class="{\'fluro-valid\':isValid(), \'fluro-dirty\':isDirty, \'fluro-invalid\':!isValid()}"><label><i class="fa fa-check" ng-if=isValid()></i><i class="fa fa-exclamation" ng-if=!isValid()></i><span>{{field.title}}</span></label><div class="error-message help-block"><span ng-if=field.errorMessage>{{field.errorMessage}}</span> <span ng-if=!field.errorMessage>Please provide valid input for this field</span></div><span class=help-block ng-if="field.description && field.type != \'boolean\'">{{field.description}}</span><div class=fluro-input-wrapper></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-terms.html", '<div class=terms-checkbox><div class=checkbox><label><input type=checkbox ng-model="model[options.key]"> {{to.definition.params.storeData}}</label></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-web-form.html", '<div class="fluro-interaction-form fluro-webform"><div ng-if=!correctPermissions class=form-permission-warning><div class="alert alert-warning small"><i class="fa fa-warning"></i> <span>You do not have permission to post {{model.plural}}</span></div></div><div ng-if=promisesResolved><div ng-if=debugMode><div class="btn-group btn-group-justified"><a ng-click="vm.state = \'ready\'" class="btn btn-default">State to ready</a> <a ng-click="vm.state = \'complete\'" class="btn btn-default">State to complete</a> <a ng-click=reset() class="btn btn-default">Reset</a></div><hr></div><div ng-show="vm.state != \'complete\'"><form novalidate ng-submit=vm.onSubmit()><formly-form model=vm.model fields=vm.modelFields form=vm.modelForm options=vm.options><div class=fluro-webform-recaptcha ng-if=model.data.recaptcha><div recaptcha-render></div></div><div class="form-error-summary form-client-error alert alert-warning" ng-if=errorList.length><div class=form-error-summary-item ng-click=scrollToField(field) ng-repeat="field in errorList track by field.id"><i class="fa fa-exclamation"></i> <span ng-if=field.templateOptions.definition.errorMessage.length>{{field.templateOptions.definition.errorMessage}}</span> <span ng-if=!field.templateOptions.definition.errorMessage.length>{{field.templateOptions.label}} has not been provided.</span></div></div><div ng-switch=vm.state class=fluro-webform-submit><div ng-switch-when=sending class=fluro-webform-submit-processing><a class="btn btn-primary" ng-disabled=true><span>Processing</span> <i class="fa fa-spinner fa-spin"></i></a></div><div ng-switch-when=error class=fluro-webform-submit-error><div class="form-error-summary form-server-error alert alert-danger" ng-if=processErrorMessages.length><div class=form-error-summary-item ng-repeat="error in processErrorMessages track by $index"><i class="fa fa-exclamation"></i> <span>Error processing your submission: {{error}}</span></div></div><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>Try Again</span> <i class="fa fa-angle-right"></i></button></div><div ng-switch-default class=fluro-webform-submit-buttons><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>{{submitLabel}}</span> <i class="fa fa-angle-right"></i></button></div></div></formly-form></form></div><div ng-show="vm.state == \'complete\'"><div compile-html=transcludedContent></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/nested/fluro-nested.html", '<div><div class=form-multi-group ng-if="(to.definition.maximum != 1) || (to.definition.minimum != 1)"><div class="panel panel-default" ng-init="fields = copyFields()" ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><div class=row><div class="col-xs-12 col-sm-8"><h5 class=multi-group-title>{{to.label}} {{$index + 1}}</h5></div><div class="col-xs-12 col-sm-4 text-left text-sm-right"><a ng-if=canRemove() class="btn btn-danger btn-multi-group-remove btn-sm btn-block" ng-click=remove($index)><span>Remove&nbsp;<span class=hidden-xs>{{to.label}}</span></span><i class="fa fa-times"></i></a></div></div></div><div class=panel-body><formly-form model=entry fields=fields></formly-form></div></div><a class="btn btn-primary btn-sm btn-multi-group-add" ng-if=canAdd() ng-click=addAnother()><span>Add&nbsp;<span ng-if=model[options.key].length>another</span>&nbsp;{{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && to.definition.minimum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/order-select/fluro-order-select.html", '<div id={{options.id}} class=fluro-order-select><div ng-if=selection.values.length><p class=help-block>Drag to reorder your choices</p></div><div class=list-group as-sortable=dragControlListeners formly-skip-ng-model-attrs-manipulator ng-model=selection.values><div class="list-group-item clearfix" as-sortable-item ng-repeat="item in selection.values"><div class=pull-left as-sortable-item-handle><i class="fa fa-arrows order-select-handle"></i> <span class="order-number text-muted">{{$index+1}}</span> <span>{{item}}</span></div><div class="pull-right order-select-remove" ng-click=deselect(item)><i class="fa fa-times"></i></div></div></div><div ng-if=canAddMore()><p class=help-block>Choose by selecting options below</p><select class=form-control ng-model=selectBox.item ng-change=selectUpdate()><option ng-repeat="(key, option) in to.options | orderBy:\'value\'" ng-if=!contains(option.value) value={{option.value}}>{{option.value}}</option></select></div></div>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-method.html", '<hr><div class=payment-method-select><div ng-if=!settings.showOptions><h4><div class=row><div class="col-xs-12 col-sm-6">{{selected.method.title}}</div><div class="col-xs-12 col-sm-6 text-left text-sm-right"><em class="small text-muted" ng-click="settings.showOptions = !settings.showOptions">Other payment options&nbsp;<i class="fa fa-angle-right"></i></em></div></div></h4></div><div ng-if=settings.showOptions><h4 class=clearfix>Select payment method <em ng-click="settings.showOptions = false" class="pull-right small">Back&nbsp;<i class="fa fa-angle-up"></i></em></h4><div class="payment-method-list list-group"><div class="payment-method-list-item list-group-item" ng-class="{active:method == selected.method}" ng-click=selectMethod(method) ng-repeat="method in methodOptions"><h5 class=title>{{method.title}}</h5></div></div></div><div ng-if=!settings.showOptions><div ng-if="selected.method.key == \'card\'"><formly-form model=model form=form fields=options.data.fields></formly-form></div><div ng-if="selected.method == method && selected.method.description.length" ng-repeat="method in methodOptions"><div compile-html=method.description></div></div></div></div><hr>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-summary.html", '<hr><div class=payment-summary><h4>Payment details</h4><div class=form-group><div ng-if=modifications.length class=payment-running-total><div class="row payment-base-row"><div class=col-xs-6><strong>Base Price</strong></div><div class="col-xs-3 col-xs-offset-3">{{paymentDetails.amount / 100 | currency}}</div></div><div class="row text-muted" ng-repeat="mod in modifications"><div class=col-xs-6><em>{{mod.title}}</em></div><div class="col-xs-3 text-right nowrap"><em>{{mod.description}}</em></div><div class=col-xs-3><em class=text-muted>{{mod.total / 100 | currency}}</em></div></div><div class="row payment-total-row"><div class=col-xs-6><h4>Total</h4></div><div class="col-xs-6 col-sm-3 col-sm-offset-3 text-right text-sm-left"><h4>{{calculatedTotal /100 |currency}} <span class="text-uppercase text-muted">{{paymentDetails.currency}}</span></h4></div></div></div><div class=payment-amount ng-if=!modifications.length>{{calculatedTotal /100 |currency}} <span class=text-uppercase>({{paymentDetails.currency}})</span></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-item.html", '<a class=clearfix><i class="fa fa-{{match.model._type}}"></i> <span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span> <span ng-if="match.model._type == \'event\' || match.model._type == \'plan\'" class="small text-muted">// {{match.model.startDate | formatDate:\'jS F Y - g:ia\'}}</span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-value.html", '<a class=clearfix><span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select.html", '<div class=fluro-search-select><div ng-if="to.definition.type == \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="item in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(item)></i> {{item.title}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{selection.value.title}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-item.html typeahead-on-select=select($item) placeholder=Search typeahead="item.title for item in retrieveReferenceOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div><div ng-if="to.definition.type != \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="value in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(value)></i> {{getValueLabel(value)}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{getValueLabel(selection.value)}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-value.html typeahead-on-select=select($item.value) placeholder=Search typeahead="item.name for item in retrieveValueOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/upload/upload.html", '<div id={{options.id}} class=fluro-file-input><div class=list-group ng-if=ctrl.fileArray.length><div class="list-group-item upload-input-item clearfix" ng-switch=item.state ng-repeat="item in ctrl.fileArray"><div ng-switch-when=processing><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small text-muted"><i class="fa fa-spinner fa-spin"></i> Uploading {{item.progress}}%</div></div><div ng-switch-when=error><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small brand-warning"><i class="fa fa-warning brand-danger"></i> {{item.state}}</div></div><div ng-switch-when=complete><i ng-click=remove(item) class="fa fa-fw fa-times pull-right"></i> <strong>{{::item.file.name}}</strong> <em class="small text-muted">{{::filesize(item.file.size)}}</em><div class="small brand-success"><i class="fa fa-check"></i> Upload Complete</div></div></div></div><div ng-if="to.definition.maximum == 1 && !ctrl.fileArray.length"><input type=file fluro-file-input definition=to.definition callback=ctrl.refresh formly-skip-ng-model-attrs-manipulator file-array=ctrl.fileArray></div><div ng-if="to.definition.maximum != 1 && (!to.definition.maximum || ctrl.fileArray.length < to.definition.maximum)"><input type=file fluro-file-input definition=to.definition callback=ctrl.refresh formly-skip-ng-model-attrs-manipulator file-array=ctrl.fileArray multiple></div></div>'), 
    $templateCache.put("fluro-interaction-form/value/value.html", "<div class=fluro-interaction-value style=display:none><pre>{{model[options.key] | json}}</pre></div>"), 
    $templateCache.put("fluro-menu-manager/fluro-menu-manager.html", '<div class="fluro-menu-manager fluro-manager"><div class="row row-flex"><div class="col-xs-12 col-sm-3 col-md-2 sidebar"><div class=page-content><div class="list-group small" as-sortable=sortableOptions ng-model=model><div class="list-group-item small" as-sortable-item ng-class={active:selection.contains(menu)} ng-click=selection.select(menu) ng-repeat="menu in model"><i class="fa fa-arrows" as-sortable-item-handle></i> <strong>{{menu.title}}</strong></div></div><a class="btn btn-sm btn-primary btn-block" ng-click=addMenu()><span>Add menu</span><i class="fa fa-plus"></i></a></div></div><div class="col-xs-12 col-sm-9 col-md-10" ng-if=selection.item><div class=page-content><div class="panel panel-default"><div class=panel-heading>Menu settings</div><div class=panel-body><div class=form-group><label>Title</label><input ng-model=selection.item.title class=form-control placeholder="Menu Title"></div><div class=form-group><label>Menu Key</label><input ng-model=selection.item.key class=form-control placeholder="Menu Key"></div><script type=text/ng-template id=menu_item_render.html><div class="menu-item">\n								<div class="row no-gutter">\n									<div class="col-xs-1">\n										<handle ui-tree-handle></handle>\n									</div>\n									<div class="col-xs-10">\n										<div class="row">\n											<div class="col-xs-4">\n												<input ng-model="node.title" class="form-control" placeholder="Title"/>\n											</div>\n											<div class="col-xs-7">\n												<div ng-switch="node.type">\n												<!--\n												<select  ng-model="node.type" class="form-control">\n													<option value="state">Page</option>\n													<option value="url">URL</option>\n												</select>\n											-->\n\n											<div ng-switch-default>\n\n												<div class="input-group">\n													<div class="input-group-addon">\n														<i class="fa fa-file-o" ng-click="node.type = \'url\'"></i>\n													</div>\n\n													<select class="form-control" ng-model="node.state">\n														<option value="{{page.state}}" ng-selected="{{page.state == node.state}}" ng-repeat="page in availablePages()">{{page.title}}</option>\n													</select>\n												</div>\n											</div>\n											<div ng-switch-when="url">\n												<div class="input-group">\n													<div class="input-group-addon">\n														<i class="fa fa-external-link" ng-click="node.type = \'state\'"></i>\n													</div>\n													<input ng-model="node.url" class="form-control" placeholder="http://"/>\n												</div>\n\n											</div>\n										</div>\n\n										\n									</div>\n									<div class="col-xs-1">\n										<a class="btn btn-default" ng-class="{active:node.advanced}" ng-click="node.advanced = !node.advanced">\n											<i class="fa fa-cogs"></i>\n										</a>\n									</div>\n								</div>\n							</div>\n\n							<div class="col-xs-1">\n								<a class="btn btn-danger" ng-click="remove()"><i class="fa fa-times"></i></a>\n							</div>\n\n						</div>\n						<div class="menu-advanced" ng-if="node.advanced">\n\n\n							<div class="row">\n								<div class="form-group col-xs-5 col-xs-offset-1">\n									<label>\n										ID\n									</label>\n									<input ng-model="node.id" class="form-control" placeholder="Menu ID"/>\n								</div>\n\n								<div class="from-group col-xs-3">\n\n									<label>\n										Class\n									</label>\n									<input ng-model="node.class" class="form-control" placeholder="Class"/>\n								</div>\n\n								<div class="form-group col-xs-2">\n									<label>\n										Target\n									</label>\n									<input ng-model="node.target" class="form-control" placeholder="Target"/>\n								</div>\n							</div>\n\n							<div class="row" ng-if="node.type == \'state\'">\n								<div class="form-group col-xs-5 col-xs-offset-1">\n									<label>State Parameters</label>\n									<key-value-select ng-model="node.stateParams"></key-value-select>\n								</div>\n								<div class="form-group col-xs-5">\n									<label>State Ref Options</label>\n									<key-value-select ng-model="node.stateRefOptions"></key-value-select>\n								</div>\n							</div>\n						</div>\n					</div>\n\n\n					<ol ui-tree-nodes="" ng-model="node.items">\n						<li ng-repeat="node in node.items" ui-tree-node ng-include="\'menu_item_render.html\'">\n						</li>\n					</ol></script><div ui-tree><ol ui-tree-nodes="" ng-model=selection.item.items id=tree-root><li ng-repeat="node in selection.item.items" ui-tree-node ng-include="\'menu_item_render.html\'"></li></ol></div><a class="btn btn-primary" ng-click=addMenuItem(selection.item.items)><span>Add Menu Item</span><i class="fa fa-plus"></i></a> <a class="btn btn-primary" ng-click=addAllMenuItems(selection.item.items)><span>Add All Pages</span><i class="fa fa-plus"></i></a> <a class="btn btn-danger" ng-click=removeMenu()><span>Delete {{selection.item.title}}</span><i class="fa fa-times"></i></a></div></div></div></div></div></div>'), 
    $templateCache.put("fluro-route-manager/fluro-route-manager.html", '<div class="fluro-route-manager flex-column"><script type=text/ng-template id=route_tree_node.html><div class="page-tree-item" ng-if="node.type != \'folder\'" ng-click="selection.select(node)" ng-class="{active:selection.contains(node), error:hasIssue(node)}">\n			<div>\n				<i class="fa fa-arrows drag-handle" ui-tree-handle></i>\n				<i class="fa fa-exclamation" tooltip-append-to-body="true" tooltip-placement="right" tooltip="{{hasIssue(node)}}" ng-if="hasIssue(node)"></i>\n				<i class="fa fa-file-o" ng-if="!hasIssue(node)"></i>\n				{{node.title}} <em class="state-name">{{node.state}}</em>\n			</div>\n\n		</div>\n\n		<div class="route-folder" ng-if="node.type == \'folder\'">\n			<!-- selection.select(node);  -->\n			<div class="route-folder-heading clearfix" ng-class="{active:selection.contains(node)}">\n				<i class="fa fa-arrows drag-handle" ui-tree-handle></i>\n				<span ng-click="node.collapsed = !node.collapsed;">\n					<i class="fa" ng-class="{\'fa-caret-down\':!node.collapsed, \'fa-caret-right\':node.collapsed}"></i>\n					<i class="fa" ng-class="{\'fa-folder-open\':!node.collapsed, \'fa-folder\':node.collapsed}"></i>\n				</span>\n\n				<span editable ng-model="node.title"></span>\n						<!-- <span ng-hide="$targetFolder == node" ng-dblclick="$targetFolder = node">{{node.title}}</span>\n         				<input ng-show="$targetFolder == node" ng-model="node.title" ng-blur="$targetFolder = null" autofocus/>\n         			-->\n\n         			<i class="fa fa-cog pull-right" ng-click="selection.select(node)"></i>\n\n\n\n\n\n\n         		</div>\n         		<div class="route-folder-contents" ng-if="!node.collapsed">\n         			<ol ui-tree-nodes ng-model="node.routes">\n         				<li ng-repeat="node in node.routes" ui-tree-node ng-include="\'route_tree_node.html\'"></li>\n         			</ol>\n         		</div>\n         	</div></script><div class="row row-flex"><div class="col-xs-12 col-sm-3 col-md-2 sidebar"><div class=flex-column><div class=flex-column-header><div class=box-header><h4 class=title>Pages</h4></div></div><div class="flex-column-body border-top border-bottom"><div class=page-tree ui-tree><div ui-tree-nodes ng-model=model id=tree-root><div ng-repeat="node in model" ui-tree-node ng-include="\'route_tree_node.html\'"></div></div></div></div><div class=flex-column-footer><div class=box-footer><a class="btn btn-sm btn-primary btn-block" ng-click=addRoute()><span>New page</span><i class="fa fa-plus"></i></a> <a class="btn btn-sm btn-default btn-block" ng-click=addFolder()><span>New folder</span><i class="fa fa-folder-open-o"></i></a> <a class="btn btn-sm btn-default btn-block" ng-if=selection.item ng-click=duplicate(selection.item)><span>Duplicate selected</span><i class="fa fa-files-o"></i></a></div></div></div></div><div class="col-sm-9 col-md-10"><div class=flex-column ng-show=model.length ng-switch=selection.item.type><div class=flex-column-header><div class="box-header border-bottom"><div class=row><div class=col-sm-10><h4 class=title><i class="fa fa-file-o small"></i> {{selection.item.title}} <em class=text-muted>{{selection.item.url}}</em></h4></div></div></div></div><div class=flex-column-body ng-switch-when=folder><div class=panel-heading>Folder settings</div><div class=panel-body><div class=form-group><label>Folder name</label><input ng-model=selection.item.title class=form-control placeholder="Title"></div><a class="btn btn-danger" ng-click=removeRoute()>Delete Folder</a></div></div><div ng-switch-default class="flex-column flex-column-body"><tabset class=flex-tabs><tab heading="Page Settings"><div class=page-content><div class="panel panel-default"><div class=panel-heading>Page settings</div><div class=panel-body><div class=row><div class=col-sm-4><div class=form-group><label>Title</label><p class=help-block>Title of this page/state</p><input ng-model=selection.item.title class=form-control placeholder="Title"></div></div><div class=col-sm-4><div class=form-group><label>State Name</label><p class=help-block>The machine name for this page/state</p><input ng-model=selection.item.state class=form-control placeholder="State name"></div><div class=form-group ng-if=containsDot(selection.item.state)><label>View Name</label><p class=help-block>The subview Name to load this content into. <em class=text-muted>If targeting a view defined in root template, header or footer append \'@\' to the end of the view name</em></p><input ng-model=selection.item.vid class=form-control placeholder="View Name"></div></div><div class=col-sm-4><div class=form-group><label>URL Path</label><p class=help-block>The path this page should have with preceding slash <em class=text-muted>eg(/about, /events)</em></p><input ng-model=selection.item.url class=form-control placeholder="/"></div></div></div></div></div><div class="panel panel-default"><div class=panel-heading>Page Configuration</div><div class=panel-body><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.disableHeader"> Disable Header</label><p class=help-block>Disable the header for this page</p></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.disableFooter"> Disable Footer</label><p class=help-block>Disable the footer for this page</p></div></div></div></div><div class="panel panel-default"><div class=panel-heading>SEO / Social</div><tabset><tab heading="Page SEO"><div class=panel-body><p class=help-block>This information will be what is submitted when this page is shared to social media websites like facebook and twitter.<br>If nothing is entered it will inherit the SEO information from the page or the site itself</p><div class=form-group><label>Meta Title</label><input ng-model=selection.item.seo.title class=form-control placeholder="SEO Title"></div><div class=form-group><label>Meta Description</label><textarea ng-model=selection.item.seo.description class=form-control placeholder="SEO Meta Description">\n														</textarea></div><div class=form-group><label>Social / SEO Image</label><content-select ng-model=selection.item.seo.image ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div></div></tab><tab heading="Slug Item SEO" ng-if=hasSlug(selection.item)><div class=panel-body><p class=help-block>To replace SEO details with information from the loaded slug item\'s content, type the field key you want to use for each input below</p><div class=form-group><label>Meta Title</label><input ng-model=selection.item.seo.slugTitle class=form-control placeholder="title"></div><div class=form-group><label>Meta Description</label><input ng-model=selection.item.seo.slugDescription class=form-control placeholder="body"></div><div class=form-group><label>Share/SEO Image</label><input ng-model=selection.item.seo.slugImage class=form-control placeholder="image"></div><div class=form-group><label>Sitemap Query</label><p class=help-block>Provide a query that returns all possible slugs for this address to generate the sitemap</p><content-select ng-model=selection.item.seo.query ng-params="{type:\'query\',maximum:1, minimum:1, searchInheritable:true}"></content-select></div></div></tab><tab heading=Advanced><div class=panel-body><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.hidden"> Hide this page from sitemap</label></div></div><div class=row><div class="form-group col-sm-6"><label>Change Frequency</label><p class=help-block>How frequently does the content on this page change?</p><select class=form-control ng-model=selection.item.seo.changeFrequency><option value=never>Never</option><option value=hourly>Hourly</option><option value=daily>Daily</option><option value=weekly>Weekly</option><option value=monthly>Monthly</option><option value=yearly>Yearly</option><option value=always>Always</option></select></div><div class="form-group col-sm-6"><label>Priority</label><p class=help-block>Range from 0.0 to 1.1</p><input class=form-control placeholder=0.5 ng-model="selection.item.seo.priority"></div></div></div></tab></tabset></div><div class="panel panel-default"><div class=panel-body><a class="btn btn-danger" ng-click=removeRoute()>Delete this page</a></div></div></div></tab><tab heading="Page Sections"><route-section-manager ng-model=selection.item.sections></route-section-manager></tab><tab heading="Page Content"><div class=flex-column><div class=flex-column-body><div class="page-content border-bottom"><h4>Page Content</h4><p class=help-block>Include content that will be made available to all sections of this page/route</p></div><div class=page-content><div class="panel panel-default"><tabset><tab><tab-heading><span class=text-muted ng-if=selection.item.content.length>{{selection.item.content.length}}</span> Items</tab-heading><div class=panel-body><div class=form-group><label>Items</label><p class=help-block>Select specific items to be included in this page, these will be populated and available to this page as <strong>$scope.page.items</strong></p><content-select ng-model=selection.item.content ng-params={searchInheritable:true}></content-select></div></div></tab><tab><tab-heading>Mapped Content</tab-heading><div class=panel-body><div class=form-group><label>Content</label><p class=help-block>Select specific keyed items to be included in this page, these will be populated as key value pairs on <strong>$scope.page.content</strong></p><key-content-select ng-model=selection.item.keys></key-content-select></div></div></tab><tab><tab-heading><span class=text-muted ng-if=selection.item.queries.length>{{selection.item.queries.length}}</span> Queries</tab-heading><div class=panel-body><div class=form-group><label>Queries</label><p class=help-block>Select queries to be included, these will be populated and available to this template as <strong>$scope.page.results</strong> <em>(combined together)</em> or <strong>$scope.page.queryResults</strong> <em>(seperate collections)</em><br></p><content-select ng-model=selection.item.queries ng-params="{type:\'query\', searchInheritable:true}"></content-select></div><div class=form-group><label>Query Variables</label><p class=help-block>Inject variables into the selected queries, the values will be parsed against this page\'s $scope</p><key-value-select ng-model=selection.item.queryVariables></key-value-select></div></div></tab><tab><tab-heading><span class=text-muted ng-if=selection.item.collections.length>{{selection.item.collections.length}}</span> Collections</tab-heading><div class=panel-body><div class=form-group><label>Collections</label><p class=help-block>Select collections to be included, these will be populated and available to this page as <strong>$scope.page.collections</strong><br>All of the collected items will be available to this page as <strong>$scope.page.collectionResults</strong></p><content-select ng-model=selection.item.collections ng-params="{type:\'collection\'}"></content-select></div></div></tab><tab heading=Params><div class=panel-body><div class=form-group><label>Parameters</label><p class=help-block>Add extra key/value parameters these will be available to this page as <strong>$scope.page.params</strong></p><key-value-select ng-model=selection.item.params></key-value-select></div></div></tab></tabset></div><div class="panel panel-default"><div class=panel-body><div class=form-group><label>Page Controller</label><p class=help-block>Add a controller name for this route. <em class=text-muted>Note that you will need to refresh the preview window after modifying controllers</em></p><input class=form-control ng-model=selection.item.controllerName placeholder="MyPageControllerName"></div></div></div></div></div></div></tab><tab heading=Slug ng-if=hasSlug(selection.item)><div class=flex-column><div class=flex-column-body><div class="page-content border-bottom"><h4>Slug</h4><p class=help-block>Load individual content items depending on the slug provided to this pages URL, this can either be provided as \'/yourpath/:slug\' or \'/yourpath/:_id\'. When the content\'s slug or _id is provided the content will be loaded from Fluro and added to the $scope of each page section as <strong>\'$scope.slug\'</strong></p></div><div class="page-content border-bottom"><div class=form-group><label>Preview Slug</label><p>Select a content item below to use as a test slug in the preview window</p><content-select ng-model=selection.item.testSlug ng-params="{searchInheritable:true, minimum:0, maximum:1}"></content-select></div></div><div class="page-content border-bottom"><h4>Advanced Slug Settings</h4><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.noCacheSlug"> Disable slug cache</label><p class=help-block>By default Fluro will cache your slug query, this improves performance for public facing websites but can sometimes be undesired when building dynamic applications. Check this box if you want to disable this feature.</p></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.includePublic"> Search in public items</label><p class=help-block>By default Fluro will search within your account or direct inheritable parent accounts, check this box if you want to search for publicly available content from other non-connected accounts.</p></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model="selection.item.excludeInheritable"> Exclude inherited results</label><p class=help-block>By default Fluro will search within your account or direct inheritable parent accounts, check this box if you wish to excluded inherited results.</p></div></div></div><div class="page-content border-bottom"><h4>Append content</h4><div class=row><div class="form-group col-sm-6"><label><i class="fa fa-contactdetail"></i> Append Contact Details</label><p class=help-block>Add the definition name of each contact detail you would like to append for the requested slug</p><multi-value-field ng-model=selection.item.appendContactDetail placeholder="definitionName eg(checkinDetails)"></multi-value-field></div><div class="form-group col-sm-6"><label><i class="fa fa-process"></i> Append Process Information</label><p class=help-block>Add the definition name of each process you would like to append for the requested slug</p><multi-value-field ng-model=selection.item.appendProcess placeholder="definitionName eg(newPersonProcess, yearLevel)"></multi-value-field></div></div><div class=row><div class="form-group col-sm-6"><label><i class="fa fa-team"></i> Append Groups/Teams</label><p class=help-block>Add the definition name of each group/team detail you would like to append for the requested slug if it is a contact</p><multi-value-field ng-model=selection.item.appendTeams placeholder="definitionName eg(lifegroup)"></multi-value-field></div><div class="form-group col-sm-6"><label><i class="fa fa-assignment"></i> Append Assignments</label><div class=checkbox><label><input type=checkbox ng-model="selection.item.appendAssignments"> Append Assignments</label><p class=help-block>If the slug is an event, select whether or not to append assignment details</p></div></div></div><div class=row><div class="form-group col-sm-6"><label><i class="fa fa-post"></i> Append Post Count</label><p class=help-block>Add the definition name of each post type attached to the requested slug</p><multi-value-field ng-model=selection.item.appendPostCount placeholder="definitionName eg(comment)"></multi-value-field></div><div class="form-group col-sm-6"><label><i class="fa fa-post"></i> Append Posts</label><p class=help-block>Add the definition name of each post type. And these posts will be appended to your results</p><multi-value-field ng-model=selection.item.appendPosts placeholder="definitionName eg(comment)"></multi-value-field></div></div><div class=row><div class="form-group col-sm-6"><label><i class="fa fa-post"></i> Append Forms</label><p class=help-block>Add the definition name of each form to attach to the requested slug</p><multi-value-field ng-model=selection.item.appendForms placeholder="definitionName eg(registrationForm)"></multi-value-field></div></div></div></div></div></tab></tabset></div></div></div></div></div>'), 
    $templateCache.put("fluro-route-section-manager/fluro-route-section-manager.html", '<div class="fluro-route-section-manager fluro-manager"><div class="row row-flex"><div class="col-xs-12 col-sm-3 col-md-2 sidebar"><div class=section-list as-sortable=sortableOptions ng-model=model><div class=section-list-item as-sortable-item ng-class="{active:selection.contains(section), disabled:section.disabled}" ng-click=selection.select(section) ng-repeat="section in model"><handle as-sortable-item-handle></handle><strong>{{section.title}}</strong></div></div><div class=page-content><a class="btn btn-primary btn-block btn-sm" ng-click=addSection()><span>Add Section</span><i class="fa fa-plus"></i></a> <a class="btn btn-default btn-block btn-sm" ng-click=selectExisting()><span>Add existing</span> <i class="fa fa-plus"></i></a></div></div><div class="col-sm-9 col-md-10" ng-if=selection.contains(section) class=flex-column-body ng-repeat="section in model"><tabset class=flex-tabs><tab heading="Section Content"><div class=page-content><div class=form-group><label>Section Title</label><input class=form-control ng-model="section.title"></div><div class=form-group><label><input type=checkbox ng-model="section.disabled"> Disable this section</label><p class=help-block>Check this to disable this section without deleting it</p></div><div class="panel panel-default"><tabset><tab><tab-heading><span class=text-muted ng-if=section.content.length>{{section.content.length}}</span> Items</tab-heading><div class=panel-body><div class=form-group><label>Items</label><p class=help-block>Select specific items to be included in this section, these will be populated and available to this section as <strong>$scope.items</strong></p><content-select ng-model=section.content ng-params={searchInheritable:true}></content-select></div></div></tab><tab><tab-heading>Mapped Content</tab-heading><div class=panel-body><div class=form-group><label>Content</label><p class=help-block>Select specific keyed items to be included in this section, these will be populated as key value pairs on <strong>$scope.content</strong></p><key-content-select ng-model=section.keys></key-content-select></div></div></tab><tab><tab-heading><span class=text-muted ng-if=section.queries.length>{{section.queries.length}}</span> Queries</tab-heading><div class=panel-body><div class=form-group><label>Queries</label><p class=help-block>Select queries to be included, these will be populated and available to this template as <strong>$scope.results</strong> <em>(combined together)</em> or <strong>$scope.queryResults</strong> <em>(seperate collections)</em><br></p><content-select ng-model=section.queries ng-params="{type:\'query\', searchInheritable:true}"></content-select></div><div class=form-group><label>Query Variables</label><p class=help-block>Inject variables into the selected queries, the values will be parsed against this section\'s $scope</p><key-value-select ng-model=section.queryVariables></key-value-select></div></div></tab><tab><tab-heading><span class=text-muted ng-if=section.collections.length>{{section.collections.length}}</span> Collections</tab-heading><div class=panel-body><div class=form-group><label>Collections</label><p class=help-block>Select collections to be included, these will be populated and available to this section as <strong>$scope.collections</strong><br>All of the collected items will be available to this section as <strong>$scope.collectionResults</strong></p><content-select ng-model=section.collections ng-params="{type:\'collection\'}"></content-select></div></div></tab><tab heading=Params><div class=panel-body><div class=form-group><label>Parameters</label><p class=help-block>Add extra key/value parameters these will be available to this section as <strong>$scope.params</strong></p><key-value-select ng-model=section.params></key-value-select></div></div></tab></tabset></div><hr><a class="btn btn-danger btn-sm" ng-click=removeSection(section)><span>Delete this Section</span></a> <a class="btn btn-default btn-sm" ng-click=exportSection(section) ng-if=!section.exportID><span>Export this Section as Site Block</span> <i class=fa ng-class="{\'fa-spinner fa-spin\':section.exporting, \'fa-share\':!section.exporting}"></i></a></div></tab><tab heading="HTML Template"><div class=flex-column><div class=code-editor ng-if=!section.template.length ng-model=section.html ui-ace=$root.aceEditorOptions.html></div><div class=code-editor ng-if=section.template.length ng-model=getTemplateSource(section.template).html ui-ace=$root.aceEditorOptions.html></div><div class=page-content><div class=row><div class=col-sm-8><select class=form-control ng-model=section.template><option value="">Unique Template (Unique to this section)</option><option value={{template.key}} ng-repeat="template in $root.site.templates">{{template.title}}</option></select></div><div class=col-sm-4><a class="btn btn-sm btn-default btn-block" ng-if=!section.template ng-click=exportTemplate(section)><span>Use as template</span> <i class="fa fa-mail-forward"></i></a> <a class="btn btn-sm btn-default btn-block" ng-if=section.template ng-click=replicateTemplate(section)><span>Use unique template</span> <i class="fa fa-mail-reply"></i></a></div></div></div></div></tab></tabset></div></div></div>'), 
    $templateCache.put("fluro-route-section-manager/section-modal.html", '<div class="panel panel-default"><div class="panel-heading clearfix"><h4 class=pull-left>Select Sections <em class=text-muted ng-if=selection.items.length>{{selection.items.length}}</em></h4><a ng-click=$close(selection.items) class="pull-right btn btn-primary"><span>Done</span> <i class="fa fa-check"></i></a></div><tabset class=flex-tabs><tab heading="Sections in this site"><div class=scroller><div class=expander ng-repeat="sectionGroup in sections"><div class=expander-heading ng-click=toggleGroup(sectionGroup)><strong>{{sectionGroup.pageName}}</strong> <em class="text-muted small">{{sectionGroup.sections.length}} sections</em></div><div class=expander-body ng-if="settings.selectedGroup == sectionGroup"><div class=list-group-item ng-class="{active:selection.contains(section), disabled:section.disabled}" ng-click=selection.toggle(section) ng-repeat="section in sectionGroup.sections">{{section.title}} <em class="text-muted small" ng-if=section.disabled>(disabled)</em></div></div></div></div></tab><tab heading="Section Templates"><div class=scroller><table class="table table-striped"><thead><tr><th>Title</th><th>Account</th><th>Created by</th></tr></thead><tbody><tr ng-class="{active:selection.contains(section), disabled:section.disabled}" ng-click=selection.toggle(section) ng-repeat="section in templates track by section._id"><td>{{section.title}}</td><td>{{section.account.title}}</td><td>{{section.author.name}}</td></tr></tbody></table></div></tab></tabset></div>'), 
    $templateCache.put("fluro-script-manager/fluro-script-manager.html", '<div class="fluro-script-manager fluro-manager"><div class="row row-flex"><div class="col-xs-12 col-sm-4 col-md-3 sidebar"><div class=page-content><div class="list-group small" as-sortable=sortableOptions ng-model=model><div class="list-group-item small" as-sortable-item ng-class="{active:selected.script == script}" ng-click="selected.script = script" ng-repeat="script in model"><handle as-sortable-item-handle></handle><strong>{{script.title}}</strong></div></div><a class="btn btn-primary btn-sm btn-block" ng-click=addScript()><span>Add Script</span> <i class="fa fa-plus"></i></a></div></div><div class="col-xs-12 col-sm-8 col-md-9" ng-if=selected.script><div class=flex-column><div class=flex-column-header><div class="page-content bg-white"><h4><i class="fa fa-terminal"></i> {{selected.script.title}}</h4><hr><div class=row><div class=col-sm-10><label>Title</label><input class=form-control ng-model=selected.script.title placeholder="Script Name"></div><div class=col-sm-2><label>&nbsp;</label><a class="btn btn-sm btn-danger btn-smbtn-block" ng-click=removeScript(selected.script)><span>Delete</span></a></div></div></div></div><div class=page-code-editor ng-model=selected.script.body ui-ace=$root.aceEditorOptions.js></div></div></div></div></div>'), 
    $templateCache.put("fluro-template-manager/fluro-template-manager.html", '<div class="fluro-template-manager fluro-manager"><div class="row row-flex"><div class="col-xs-12 col-sm-4 col-md-3 sidebar"><div class=page-content><div class="list-group small" as-sortable=sortableOptions ng-model=model><div class="list-group-item small" as-sortable-item ng-class="{active:selection.contains(template), disabled:template.disabled}" ng-click=selection.select(template) ng-repeat="template in model"><handle as-sortable-item-handle></handle><strong>{{template.title}}</strong><br><em class="text-muted small">{{template.key}}</em></div></div><a class="btn btn-sm btn-primary btn-block" ng-click=addTemplate()><span>Add template</span><i class="fa fa-plus"></i></a> <a class="btn btn-sm btn-default btn-block" ng-if=selection.item ng-click=duplicate(selection.item)><span>Duplicate selected</span><i class="fa fa-files-o"></i></a></div></div><div class="col-xs-12 col-sm-8 col-md-9" ng-if=selection.item><div class=flex-column><div class=flex-column-header ng-controller=TemplateEditorController><div class="page-content bg-white"><h4><i class="fa fa-code"></i> {{selection.item.title}}</h4><hr><div class=row><div class="form-group col-sm-5"><label>Title</label><input ng-model=selection.item.title class=form-control placeholder="Title"></div><div class=col-sm-4 ng-class="{\'has-error\':!selection.item.key.length}"><label>Unique Key</label><input ng-model=selection.item.key machine-name class=form-control placeholder="Unique Key"></div><div class=col-sm-3><label>&nbsp;</label><div><a class="btn btn-sm btn-danger btn-block" ng-click=removeTemplate()>Delete</a></div></div></div></div></div><div class=page-code-editor ng-model=selection.item.html ui-ace=$root.aceEditorOptions.html></div></div></div></div></div>'), 
    $templateCache.put("routes/build/build.html", '<div id=builder class=builder-application><notifications></notifications><div class=builder-application-header><div class=masthead><div class=container-fluid><div class=row><div class="col-xs-12 col-sm-4 stacked-xs"><input ng-model=item.title class=form-control placeholder="Site title"></div><div class="col-xs-12 col-sm-8 text-center text-sm-right"><realm-select ng-type="\'site\'" ng-model=item.realms></realm-select><a class="btn btn-default btn-sm" target=_blank href=http://support.fluro.io/guide/wb><span>Help</span><i class="fa fa-support"></i></a> <a class="btn btn-default btn-sm" ng-click=openPreview()><span>New Preview</span><i class="fa fa-image"></i></a> <a class="btn btn-primary btn-sm" ng-click=saveVersion()><span>Save</span><i class="fa fa-check"></i></a></div></div></div></div><ul class=main-menu><li><a ui-sref-active=active ui-sref=build.default>Pages</a></li><li><a ui-sref-active=active ui-sref=build.info>Site Information</a></li><li><a ui-sref-active=active ui-sref=build.dependencies>Dependencies</a></li><li><a ui-sref-active=active ui-sref=build.components>Components</a></li><li><a ui-sref-active=active ui-sref=build.styles>Styles</a></li></ul></div><div class=builder-application-body ui-view></div></div>'), 
    $templateCache.put("routes/build/components/manager.html", '<tabset class=flex-tabs><tab heading="Included Components ({{item.components.length}})"><div class=flex-column><div class=flex-column-header><div class="manager-header border-bottom"><div class=container-fluid><h4>Components</h4><p class=help-block>Include extra functionality with components</p></div></div></div><div class=page-content><div class=row><div class=col-sm-3><div class=left-sidebar><div class=list-group><a class=list-group-item ng-click="selected.component = component" ng-class="{active:component == selected.component}" ng-repeat="component in item.components"><span editable ng-model=component.title></span></a></div><a class="btn btn-primary btn-block" ng-click=addComponent()><span>Add Component</span> <i class="fa fa-plus"></i></a></div></div><div class=col-sm-9 ng-if=selected.component><div class=clearfix><div class=pull-left><h3>{{selected.component.title}}</h3></div><div class=pull-right><realm-select ng-if=!selected.component._id ng-model=selected.component.realms></realm-select><a class="btn btn-default" ng-click=updateComponent(selected.component)><span>Refresh Component</span> <i class="fa fa-refresh"></i></a> <a class="btn btn-danger" ng-click=removeComponent()><span>Remove</span> <i class="fa fa-times"></i></a> <a class="btn btn-default" ng-if=sameAccount(selected.component) ng-click=editComponent()><span>Edit</span> <i class="fa fa-share"></i></a></div></div><div compile-html=selected.component.readme></div><pre>{{selected.component.css}}</pre></div></div></div></div></tab><tab heading="Component Market"><div class=flex-column ng-controller=ComponentMarketController><div class=flex-column-header><div class="manager-header bg-white border-bottom"><div class=container-fluid><div class=row><div class=col-sm-8><h4 class=title>Component Marketplace</h4></div><div class=col-sm-4><div class=input-group><input placeholder="Search marketplace" ng-model=search.terms class="form-control"><div class=input-group-addon><i class="fa fa-search"></i></div></div></div></div></div></div></div><div class=flex-column-body><div class=page-content><div class="row row-inline text-center"><div class="col-sm-3 col-md-2 text-left" ng-repeat="com in marketComponents  | filter:search.terms"><div class="panel panel-default" ng-class="{\'component-in-use\':componentUsed(com)}"><div class=panel-body><h5>{{com.title}}</h5><p class=help-block>{{com.account.title}}</p><a class="btn btn-default btn-xs" ng-click=viewReadme(com) ng-if=com.readme.length><span>Info</span> <i class="fa fa-info"></i></a> <a class="btn btn-primary btn-xs" ng-if=!componentUsed(com) ng-click=useComponent(com)><span>Use</span> <i class="fa fa-download"></i></a> <a class="btn btn-danger btn-xs" ng-if=componentUsed(com) ng-click=pullComponent(com)><span>Remove</span> <i class="fa fa-times"></i></a></div></div></div></div></div></div></div></tab></tabset>'), 
    $templateCache.put("routes/build/default/manager.html", '<tabset><tab heading=Pages><tabset><tab heading=Pages><div class=page-content><h2>Pages</h2><p class=help-block>Create pages below then add content and templates specific to each page</p><route-manager ng-model=item.routes ng-selection=buildService.selection></route-manager></div></tab><tab heading=Header><div class=page-content><h2>Header</h2><p class=help-block>Content and code included in the header will be rendered at the top of every page on your site, This is useful for things like a main menu or logo.</p><tabset><tab heading="Header Sections"><route-section-manager ng-model=item.header.sections></route-section-manager></tab><tab heading="Header Template"><div class=code-editor ng-model=item.header.html ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div></tab></tabset></div></tab><tab heading=Footer><div class=page-content><h2>Footer</h2><p class=help-block>Content and code included in the footer will be rendered at the bottom of every page on your site, This is useful for things like copyright information and quicklinks</p><tabset><tab heading="Footer Sections"><route-section-manager ng-model=item.footer.sections></route-section-manager></tab><tab heading="Footer Template"><div class=code-editor ng-model=item.footer.html ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div></tab></tabset></div></tab><tab heading=Menus><div class=page-content><h2>Menus</h2><p class=help-block>Create menus that can be rendered in your templates</p><menu-manager ng-model=item.menus></menu-manager></div></tab><tab heading=Structure><div class=page-content><h2>Structure</h2><p class=help-block>Customise the site layout template</p><div class="panel panel-default"><div class=panel-heading>Site Structure Template</div><div class=panel-body><div class=form-group><p class=help-block>Override the main site template by writing in your html below<br><em class=text-muted>This is helpful for adding preloaders and wrappers around your site</em></p><label>Default Template</label><p>By default the site template will use the code below.<br>You can alter the site template by adding markup. To add header, footer and page content use the variables below</p><code>&lt;fluro-header/&gt;<br>&lt;fluro-content/&gt;<br>&lt;fluro-footer/&gt;<br></code></div></div><div class=code-editor ng-model=item.outerHTML ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'html\'}"></div></div></div></tab><tab heading=Templates><div class=page-content><h2>Templates</h2><p class=help-block>Create reuseable markup for your sections</p><template-manager ng-model=item.templates></template-manager></div></tab></tabset></tab><tab heading="Site information"><div class=page-content><div class="panel panel-default"><div class=panel-heading>Site Settings</div><div class=panel-body><div class=form-group><label>Title</label><input ng-model=item.title class=form-control placeholder="Title"></div></div></div><div class="panel panel-default"><div class=panel-heading>Default SEO / Social</div><div class=panel-body><div class=form-group><label>Meta Title</label><input ng-model=item.seo.title class=form-control placeholder="SEO Title"></div><div class=form-group><label>Meta Description</label><textarea ng-model=item.seo.description class=form-control placeholder="SEO Meta Description">\n							</textarea></div><div class=form-group><label>Share/SEO Image</label><content-select ng-model=item.seo.image ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div></div></div><div class="panel panel-default"><div class=panel-heading>Performance</div><div class=panel-body><div class=form-group><div class=checkbox><label><input ng-model=item.useCompiled class=form-control type="checkbox"> Use compiled SaSS and Components</label></div><p class=help-block>Each time this site is saved, the components and sass are compiled and minified. Turn this setting on once the site goes live for faster load times and performance</p></div></div></div></div></tab><tab heading=Dependencies><div class=page-content><h2>External Dependencies</h2><div class=form-group><label>Angular Module Dependencies</label><p class=help-block>Each module name listed below will be injected into the main <em>angular.module(\'fluro\',[DEPENDENCIES])</em><br>Do not include commas, just each individual module name, note that if these dependencies are unavailable or not included this will break your site</p><multi-value-field ng-model=item.dependencies placeholder="Module name"></multi-value-field></div><div class=form-group><label>External Javascript</label><p class=help-block>Include javascript from external sources<br><strong>Note:</strong> including scripts from unknown sources could be problematic for security reason. So only use this if you know what you are doing!</p><multi-value-field ng-model=item.external placeholder="http://"></multi-value-field></div><div class=form-group ng-controller=BowerManagerController><label>Bower Components</label><p class=help-block>Include bower components to compile into this website. This site must be saved before bower components can be installed.</p><div ng-if=item._id><multi-value-field ng-model=item.bower placeholder=bower-component-name></multi-value-field><div ng-switch=bowerStatus><a class="btn btn-default" ng-disabled=true ng-switch-when=progress><span>Installing</span><i class="fa fa-spin fa-spinner"></i></a> <a class="btn btn-primary" ng-click=install() ng-switch-default><span>Bower install <span class=text-muted>(Save first)</span></span><i class="fa fa-chevron-right"></i></a></div></div></div></div></tab><tab heading=Components><div class=page-content><h2>Components</h2><tabset><tab heading="Included Components"><div class=row><div class=col-md-3><fluro-component-select ng-model=item.components ng-preview=preview></fluro-component-select></div><div class=col-md-9><div ng-if=preview.component class="panel panel-default"><div class=panel-body><h3>{{preview.component.title}}</h3><fluro-component-preview ng-model=preview.component></fluro-component-preview><div ng-if=preview.component.readme.length><h3>Help / Readme</h3><div compile-html=preview.component.readme></div></div><div ng-if=preview.component.css.length><h3>Included SCSS</h3><div>{{preview.component.css}}</div></div></div></div></div></div></tab><tab heading="Custom Embedded Scripts"><h3>Custom Scripts</h3><p>Include controllers, filters and services specific for this site in here</p><div class=code-editor ng-model=item.inlineScript ui-ace="{useWrapMode : false, showGutter: true, theme:\'tomorrow_night_eighties\', mode: \'javascript\'}"></div></tab></tabset></div></tab><tab heading=Styles><div class=page-content><h2>Style Sheets</h2><div class="panel panel-default"><div class=panel-body><h5>Include Before Components</h5><p class=help-block>Stylesheets selected will be included <strong>before</strong> component css</p><content-select ng-model=item.stylesheets ng-params="{type:\'code\'}"></content-select></div></div><div class="panel panel-default"><div class=panel-body><h5>Include After Components</h5><p class=help-block>Stylesheets selected will be included <strong>after</strong> component css</p><content-select ng-model=item.afterStylesheets ng-params="{type:\'code\'}"></content-select></div></div></div></tab></tabset>'), 
    $templateCache.put("routes/build/dependencies/manager.html", '<div class=page-content><h5>External Dependencies</h5><div class="panel panel-default"><div class=panel-heading><label>Angular Module Dependencies</label><p class=help-block>Each module name listed below will be injected into the main <em>angular.module(\'fluro\',[DEPENDENCIES])</em><br>Do not include commas, just each individual module name, note that if these dependencies are unavailable or not included this will break your site</p></div><div class=panel-body><div class=form-group><multi-value-field ng-model=item.dependencies placeholder="Module name"></multi-value-field></div></div></div><div class="panel panel-default"><div class=panel-heading><label>External Javascript</label><p class=help-block>Include javascript from external sources<br><strong>Note:</strong> including scripts from unknown sources could be problematic for security reason. So only use this if you know what you are doing!</p></div><div class=panel-body><div class=form-group><multi-value-field ng-model=item.external placeholder="http://"></multi-value-field></div></div></div><div class="panel panel-default"><div class=panel-heading><label>Bower Components</label><p class=help-block>Include bower components to compile into this website. This site must be saved before bower components can be installed. To add specific versions of libraries provide the version number like this <em>(angular-bootstrap#0.13.4)</em></p></div><div class=panel-body><div class=form-group ng-controller=BowerManagerController><div ng-if=item._id><div class=form-group><multi-value-field ng-model=item.bower placeholder=bower-component-name></multi-value-field></div><div ng-switch=bowerStatus><a class="btn btn-default" ng-disabled=true ng-switch-when=progress><span>Installing</span><i class="fa fa-spin fa-spinner"></i></a> <a class="btn btn-primary" ng-click=install() ng-switch-default><span>Bower install <span class=text-muted>(Save first)</span></span><i class="fa fa-chevron-right"></i></a></div></div></div></div></div></div>'), 
    $templateCache.put("routes/build/info/manager.html", '<div class=page-content><div class="panel panel-default"><div class=panel-heading>Site Settings</div><div class=panel-body><div class=form-group><label>Title</label><input ng-model=item.title class=form-control placeholder="Title"></div></div></div><div class="panel panel-default"><div class=panel-heading>Default SEO / Social</div><div class=panel-body><div class=form-group><label>Meta Title</label><input ng-model=item.seo.title class=form-control placeholder="SEO Title"></div><div class=form-group><label>Meta Description</label><textarea ng-model=item.seo.description class=form-control placeholder="SEO Meta Description">\n							</textarea></div><div class=form-group><label>Share/SEO Image</label><content-select ng-model=item.seo.image ng-params="{type:\'image\',maximum:1, minimum:1}"></content-select></div></div></div><div class="panel panel-default"><div class=panel-heading>Performance</div><div class=panel-body><div class=form-group><div class=checkbox><label><input ng-model=item.useCompiled type="checkbox"> Use compiled SaSS and Components</label><p class=help-block>Each time this site is saved, the components and sass are compiled and minified. Turn this setting on once the site goes live for faster load times and performance</p></div></div></div></div></div>'), 
    $templateCache.put("routes/build/pages/manager.html", '<tabset class=flex-tabs><tab heading=Pages><route-manager ng-model=item.routes ng-selection=buildService.selection></route-manager></tab><tab heading=Header><div class=flex-column><route-section-manager ng-model=item.header.sections></route-section-manager></div></tab><tab heading=Footer><div class=flex-column><route-section-manager ng-model=item.footer.sections></route-section-manager></div></tab><tab heading=Menus><div class=flex-column><menu-manager ng-model=item.menus></menu-manager></div></tab><tab heading=Structure><div class=flex-column><div class=page-content><div class=form-group><label>Default Template</label><p class=help-block>By default the site template will use default template. But you can override default by writing in your html below<br>You can alter the site template by adding markup. To add header, footer and page content use the variables below</p><code>&lt;fluro-header/&gt;<br>&lt;fluro-content/&gt;<br>&lt;fluro-footer/&gt;<br></code></div></div><div class=page-code-editor ng-model=item.outerHTML ui-ace=$root.aceEditorOptions.html></div></div></tab><tab heading="HTML Templates"><div class=flex-column><template-manager ng-model=item.templates></template-manager></div></tab><tab heading=Scripts><div class=flex-column><script-manager ng-model=item.scripts></script-manager></div></tab></tabset>'), 
    $templateCache.put("routes/build/save-modal.html", '<div class=panel-body><h5>Save a new version</h5><div class=form-group><p class=help-block>Write a short message detailing the changes you\'ve made</p><input ng-model=message class=form-control placeholder="Write what you did here"></div><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a> <a class="btn btn-primary" ng-click=$close(message)><span>Save</span> <i class="fa fa-check"></i></a></div>'), 
    $templateCache.put("routes/build/styles/manager.html", "<div class=page-content><h2>Style Sheets</h2><div class=\"panel panel-default\"><div class=panel-body><h5>Include Before Components</h5><p class=help-block>Stylesheets selected will be included <strong>before</strong> component css</p><content-select ng-model=item.stylesheets ng-params=\"{type:'code', syntax:'scss',  searchInheritable:true}\"></content-select></div></div><div class=\"panel panel-default\"><div class=panel-body><h5>Include After Components</h5><p class=help-block>Stylesheets selected will be included <strong>after</strong> component css</p><content-select ng-model=item.afterStylesheets ng-params=\"{type:'code', syntax:'scss', searchInheritable:true}\"></content-select></div></div></div>"), 
    $templateCache.put("routes/home/home.html", '<div><div class=application-top><div class=container><div class=text-wrap><div class=row><div class="col-sm-4 hidden-xs">Web Builder</div><div class="col-sm-8 text-right"><a class="btn btn-primary" ui-sref="build.default({id:\'new\'})"><span>Create</span><i class="fa fa-plus"></i></a></div></div></div></div></div><div class="fixed-body application-body"><div class=container><div class=text-wrap><table class="table table-striped"><thead><th>Title</th><th>Updated</th><th>Creator</th></thead><tbody><tr ng-repeat="item in items"><td>{{item.title}}</td><td>{{item.updated | timeago}}</td><td>{{item.author.name}}</td><td class=text-right><div class=btn-group><a class="btn btn-default" ui-sref=build.default({id:item._id})><span>Open</span><i class="fa fa-pencil"></i></a></div></td></tr></tbody></table></div></div></div></div>'), 
    $templateCache.put("routes/preview/preview.html", "<fluro-component-loader ng-model=site></fluro-component-loader><fluro-stylesheet-loader ng-model=site></fluro-stylesheet-loader><div class=site></div>"), 
    $templateCache.put("routes/templates/template-copy-modal.html", '<div class=panel-body><h5>Create site from template</h5><div ng-switch=status><div ng-switch-default><p class=help-block>Copying \'{{template.title}}\'.<br>The new site and stylesheets will be copied into the realm selected below</p><div class=form-group><realm-select ng-model=template.realms ng-type="\'site\'"></realm-select></div><div class=form-group><label>Include</label><p class=help-block>Stylesheets, queries and other required parts of this site will be copied to your account.<br>Below you can select extra includes to copy that may not be explicitly linked to this site. <em class=text-muted>Eg. Content types and permissions</em></p><div class="list-group small"><a class=list-group-item ng-class={active:isSelected(type.type)} ng-click=toggle(type.type) ng-repeat="type in types"><div class=clearfix><i class=fa ng-class="{\'fa-check-square-o\':isSelected(type.type), \'fa-square-o\':!isSelected(type.type)}"></i> <span>{{type.title}}</span></div><em class=text-muted ng-if=type.description>{{type.description}}</em></a></div></div><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a> <a class="btn btn-primary" ng-disabled=!template.realms.length ng-click=create()><span>Copy</span> <i class="fa fa-check"></i></a></div><div class=form-group ng-switch-when=processing><i class="fa fa-spin fa-spinner"></i> <span>Configuring site...</span></div></div></div>'), 
    $templateCache.put("routes/templates/templates.html", '<div class="application-body fixed-body"><div class=container><div class=form-group><div class=input-group><input class=form-control placeholder=Search ng-model="search.terms"><div class=input-group-addon><i class="fa fa-search"></i></div></div></div><table class="table table-striped"><thead><th>Title</th><th>Account</th><th>Updated</th><th>Created</th></thead><tbody><tr ng-repeat="item in items | orderBy:\'title\' | filter:search.terms"><td>{{item.title}}</td><td>{{item.account.title}}</td><td>{{item.updated | timeago}}</td><td>{{item.created | timeago}}</td><td><div class=btn-group><a class="btn btn-default" ng-click=useTemplate(item)><span>Copy</span><i class="fa fa-plus"></i></a></div></td></tr></tbody></table></div></div>'), 
    $templateCache.put("routes/versions/versions.html", "<div></div>");
} ]);