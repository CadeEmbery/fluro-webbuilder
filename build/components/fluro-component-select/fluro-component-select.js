app.directive('fluroComponentSelect', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            preview: '=ngPreview',
        },
        // Replace the div with our template
        templateUrl: 'fluro-component-select/fluro-component-select.html',
        controller: 'FluroComponentSelectController',
    };
});


app.controller('FluroComponentSelectController', function($scope, FluroContent) {

    if (!$scope.model) {
        $scope.model = [];
    }

    //Load the components
    //$scope.items = FluroContent.endpoint('my/components', false, true).query();

   

    $scope.previewComponent = function(component) {
        if(!$scope.preview) {
            $scope.preview = {}
        }

        $scope.preview.component = component;
    }


    $scope.refreshStatus = ''

    //////////////////////////////////////////


    $scope.reloadComponents = function() {
        $scope.refreshStatus = 'loading';
        FluroContent.endpoint('my/components', false, true).query().$promise.then(function(res) {
            console.log('RES', res)
            $scope.items = res;
            $scope.refreshStatus = 'complete';
        });
    }

    //One for good measure
     $scope.reloadComponents();

    //////////////////////////////////////////

    $scope.reloadComponent = function(component) {
        console.log('Reload Component', component.title)
        var index = _.findIndex($scope.model, { '_id': component._id});

        if(index != -1) {
            console.log('Replace component');
            $scope.model[index] = component;
        } else {
            console.log('Insert Component');
            $scope.model.push(component);
        }
    }


    //////////////////////////////////////////

    $scope.isSelected = function(item) {

        var checkId = item;
        if (item._id) {
            checkId = item._id;
        }

        //Check if the item is activated
        return _.some($scope.model, function(active) {
            if (active._id) {
                return active._id == checkId;
            } else {
                return active == checkId;
            }
        });

    }

    //////////////////////////////////////////

    $scope.deselect = function(item) {

        var checkId = item;
        if(item._id) {
            checkId = item._id;
        }

        _.remove($scope.model, function(search) {
            if(search._id) {
                return search._id == checkId;
            } else {
                return search == checkId;
            }
        })
    }

    //////////////////////////////////////////

    $scope.select = function(item) {
        console.log('Select', item)
        $scope.model.push(item);
    }

    //////////////////////////////////////////

    $scope.toggle = function(item) {
        var active = $scope.isSelected(item);
        if (active) {
            $scope.deselect(item);
        } else {
            $scope.select(item);
        }

    }

    //////////////////////////////////////////

})