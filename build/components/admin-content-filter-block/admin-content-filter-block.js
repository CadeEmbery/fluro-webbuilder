app.directive('filterBlock', function() {

    return {
        restrict: 'E',
        replace:true,
        scope: {
            model: '=ngModel',
            source: '=ngSource',
            getFilterKey: '&ngFilterKey',
            getFilterTitle: '&ngFilterTitle',
            persist: '@',
        },
        templateUrl: 'admin-content-filter-block/admin-content-filter-block.html',
        controller: 'ContentFilterBlock',
    };
});


app.controller('ContentFilterBlock', function($scope, $timeout, $filter, Tools) {

    var filterKey = $scope.getFilterKey();
    var filterTitle = $scope.getFilterTitle();

    $scope.filterKey = filterKey;
    $scope.filterTitle = filterTitle;

    if (!$scope.model) {
        $scope.model = {};
    }


    // console.log(filterKey, $scope.model, $scope.source)

    ///////////////////////////////////////////////

    $scope.filterIsActive = function(value) {
        return _.contains($scope.model[filterKey], value);
    }

    ///////////////////////////////////////////////

    $scope.filtersUsed = function() {
        return ($scope.model[filterKey] && $scope.model[filterKey].length)
    }

    ///////////////////////////////////////////////

    $scope.toggleFilter = function(value) {

        var active = _.contains($scope.model[filterKey], value);

        if ($scope.persist) {

            if (active) {
                //Always select
                $scope.model[filterKey] = [];
            } else {
                //Always select
                $scope.model[filterKey] = [value];
            }

        } else {

            if (active) {
                $scope.removeFilter(value);
            } else {
                $scope.addFilter(value);
            }
        }
    }

    ///////////////////////////////////////////////

    $scope.removeFilter = function(value) {
        //console.log('remove filter', key)
        if (!value) {
            delete $scope.model[filterKey];
        } else {
            _.pull($scope.model[filterKey], value);
        }
    }

    ///////////////////////////////////////////////

    $scope.addFilter = function(value) {

        if (!$scope.model[filterKey]) {
            $scope.model[filterKey] = [];
        }

        if (!_.contains($scope.model[filterKey], value)) {
            $scope.model[filterKey].push(value);
        }
    }

    ///////////////////////////////////////////////

    function addFilterOption(result, item, field_key, source) {

        if (!source) {
            return;
        }
        var key = source[field_key];
        var title;

        if (_.isString(source)) {
            title = key = source;
        } else {
            if (source.title) {
                title = source.title;
            } else {
                title = source.name;
            }
        }

        if (!result[key]) {
            result[key] = {
                title: title,
                key: key,
                //items: [item],
                count: 1,
            }
        } else {
            //result[key].items.push(item);
            result[key].count++;
        }
    }



    ///////////////////////////////////////////////

    $scope.$watch('source', function(items) {


        var filterOptions = _.chain(items)
            .filter(function(item) {
                var obj = Tools.getDeepProperty(item, filterKey);
                if (obj) {
                    return true;
                }
            })
            .reduce(function(result, item, key) {
                var obj = Tools.getDeepProperty(item, filterKey);

                if (_.isArray(obj)) {
                    _.each(obj, function(sub) {
                        addFilterOption(result, item, '_id', sub)
                    })
                } else {
                    if (_.isString(obj)) {
                        addFilterOption(result, item, filterKey, obj);
                    } else {
                        addFilterOption(result, item, '_id', obj);
                    }
                }
                return result;
            }, {})
            .value();

        $scope.filterOptions = filterOptions;
        //$scope.filterOptions = $filter('orderBy')(filterOptions, 'title');


        $scope.count = _.keys($scope.filterOptions).length;

    });

    ///////////////////////////////////////////////


});