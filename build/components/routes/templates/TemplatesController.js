app.controller('TemplatesController', function($scope, items, $state, $modal, FluroContent) {



    $scope.items = items;

    ///////////////////////////////////////////////////////////

    $scope.search = {}


    ///////////////////////////////////////////////////////////

    $scope.useTemplate = function(item) {

        //Open a modal to ask for more details
        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            backdrop:'static',
            templateUrl: 'routes/templates/template-copy-modal.html',
            controller: function($scope) {
                $scope.template = {
                    title: item.title,
                    _id: item._id,
                    extras: []
                }

                $scope.status = 'ready'


                $scope.create = function() {

                    $scope.status = 'processing';

                    var data = $scope.template;
                    /**/
                    FluroContent.endpoint('templates/use').save(data).$promise
                    .then(function(site) {

                    	$scope.$close();

                        $state.go('build.default', {
                            id: site._id
                        });
                    }, function(res) {
                    	console.log('Failed');
                    	$scope.status = 'error'
                    })
/**/
                }

                /////////////////////////////////////

                $scope.isSelected = function(typeName) {
                    var includes = $scope.template.extras;
                    return _.contains(includes, typeName);
                }

                /////////////////////////////////////

                $scope.toggle = function(typeName) {

                    //Check if it's activated already

                    if ($scope.isSelected(typeName)) {
                        _.pull($scope.template.extras, typeName);
                    } else {
                        $scope.template.extras.push(typeName);
                    }

                }

                /////////////////////////////////////

                $scope.types = [];

                // $scope.types.push({
                //     title: 'Stylesheets & Scripts',
                //     type: 'code',
                // });

                $scope.types.push({
                    title: 'Definitions',
                    type: 'definitions',
                    description: 'Include all definitions from the parent account',
                });

                // $scope.types.push({
                //     title: 'Queries',
                //     type: 'query',

                // });

                // $scope.types.push({
                //     title: 'Articles',
                //     type: 'article',
                // });

                // $scope.types.push({
                //     title: 'Locations',
                //     type: 'location',
                // });

                $scope.types.push({
                    title: 'Roles and Permission Sets',
                    type: 'roles',
                    description: 'Recreate the roles and permission sets defined in the parent account',
                });

                $scope.types.push({
                    title: 'Setup new application',
                    type: 'application',
                    description: 'Setup a new "Website" application and include this site model',
                });


                // $scope.template.includes =  _.map($scope.types, function(type) {
                // 	return type.type;
                // });

            },
            size: 'md',
        });

        ////////////////////////////////////////////////////////

        //Modal was closed with a result
        // modalInstance.result.then(function(id) {

        // });
    }

});