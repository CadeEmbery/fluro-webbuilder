app.service('BuildSelectionService', function() {

    var controller = {};

    //Now we have nested routes we need to flatten
    controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return controller.getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    //Now we have nested routes we need to flatten
    controller.getFlattenedFolders = function(array) {
        return _.chain(array)
            .reduce(function(result, route, key) {

                if (route.type == 'folder') {
                    result.push(route);
                }

                if (route.routes && route.routes.length) {
                    //Recursive search
                    var folders = controller.getFlattenedFolders(route.routes);

                    if (folders.length) {
                        result.push(folders)
                    }
                }

                return result;

            }, [])
            .flatten()
            .compact()
            .value();

    }

    return controller;


});


//////////////////////////////////////////

app.service('BuildService', function(ObjectSelection) {

    var controller = {};

    //////////////////////////////////////////

    controller.selection = new ObjectSelection();;
    controller.selection.multiple = false;


    //////////////////////////////////////////

    return controller;
})

//////////////////////////////////////////

app.controller('BuildController', function($scope, $state, FluroSocket, $timeout, BuildService, $interval, $rootScope, $modal, snippets, $rootScope, item, BuildSelectionService, $state, Fluro, Notifications, FluroContent, $window, $compile) {






    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////


    //Listen to the content.edit event
    FluroSocket.on('content.edit', socketUpdate);

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.edit', socketUpdate);
    });

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    var socketEnabled = true;

    function socketUpdate(socketEvent) {

        if (socketEnabled) {
            //Get the item that was updated
            var socketItemID = socketEvent.item;

            if (!socketItemID) {
                return;
            }

            //////////////////////////////////////////////

            //Simplify the id
            if (socketItemID._id) {
                socketItemID = socketItemID._id;
            }

            //////////////////////////////////////////////

            //Same id as the current site that we are working on
            var sameId = (socketItemID == item._id);

            //////////////////////////////////////////////

            //Check if the update is newer than the current item we are working on
            var newVersion = (socketEvent.data.updated != item.updated);

            //Make sure we only update if we weren't the originating socket
            var differentSocket = (socketEvent.ignoreSocket != FluroSocket.getSocketID());

            //////////////////////////////////////////////

            console.log('SOCKET', sameId, newVersion, differentSocket, FluroSocket.getSocketID(), socketEvent.ignoreSocket);

            //If the same id, and its a new version and a different socket that cause the request
            if (sameId && newVersion && differentSocket) {

                //Then apply
                $timeout(function() {
                    //Assign the new content
                    socketEnabled = false;

                    // console.log('APPLY PATCH', socketEvent)
                    //Patch the differences
                    jsondiffpatch.patch(item, socketEvent.data.diff);

                    //Renable the socket
                    socketEnabled = true;

                    // //console.log($rootScope.user._id, data.user._id)
                    if (socketEvent.user) {
                        if ($rootScope.user._id != socketEvent.user._id) {
                            Notifications.warning('This content was just updated by ' + socketEvent.user.name);
                        } else {
                            // if(socketEvent.user.name) {
                            //     Notifications.status('This content was updated by '+ $rootScope.user.name +' in another window');
                            // } else {
                                Notifications.warning('This content was updated in another window');
                            // }
                        }
                    } else {
                        Notifications.warning('This content was updated by another user');
                    }

                    // FluroContent.resource('site/' + item._id, true, true).get().$promise.then(function(site) {
                    //     console.log('Got NEW SITE', site);
                    //     _.assign($scope.item, site);
                    // })

                    // $state.reload();
                })
            }
        }
    }

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////



    $rootScope.snippets = snippets;

    $scope.openPreview = function() {
        $window.open('/preview');
    }

    /////////////////////////////////////////

    window.buildService = BuildService;

    /////////////////////////////////////////

    $scope.buildService = BuildService;

    /////////////////////////////////////////

    //Add the item to the scope
    $scope.item = item;

    //Anytime the item changes
    $scope.$watch('item', function(item) {

        //
        $rootScope.site = item;
        BuildService.site = item;
    })

    /////////////////////////////////////////

    //Setup one route selected at a time
    // var routeSelection = new ObjectSelection();
    // routeSelection.multiple = false;
    // $scope.routeSelection = routeSelection;




    //Select first route
    if (item.routes) {
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes(item.routes);
        BuildService.selection.select(flattenedRoutes[0]);
    }


    //$scope.selection.select(firstRoute);


    //////////////////////////////////////////////////





    /**
    function(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    alert('ctrl-s');
                    break;
                case 'f':
                    event.preventDefault();
                    alert('ctrl-f');
                    break;
                case 'g':
                    event.preventDefault();
                    alert('ctrl-g');
                    break;
            }
        }
    });



    $(window).on('keydown', keyDown);
    /**/

    ///////////////////////////////
    ///////////////////////////////

    $(window).on('keydown', keyDown);
    $(window).on('keyup', keyUp);

    ///////////////////////////////

    var keySaveDown;

    ///////////////////////////////

    function keyUp(event) {
        if (String(event.which) == '91') {
            keySaveDown = false;
        }
    }
    ///////////////////////////////



    function keyDown(event) {

        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    //event.stopPropagation();

                    if (keySaveDown) {
                        return;
                    }

                    keySaveDown = true;
                    $scope.save();
                    break;
            }
        }
    }

    $scope.$on("$destroy", function() {
        $(window).off('keydown', keyDown);
        $(window).off('keyup', keyUp);
    });

    /////////////////////////////////////////////


    var versionMessage = '';

    /////////////////////////////////////////////

    $scope.saveVersion = function() {


        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            templateUrl: 'routes/build/save-modal.html',
            controller: 'SaveModalController',
            size: 'sm',
        });

        modalInstance.result.then(function(message) {

            //If we wrote a message then just keep going
            if (message && message.length) {
                $scope.save({
                    version: message,
                });
            } else {
                $scope.save();
            }
        });
    }

    //////////////////////////////////////////////////

    //Save
    $scope.save = function(options) {

        if ($scope.isSaving) {
            console.log('SStop')
            return;
        }



        $scope.isSaving = true;

        ///////////////////////////////////

        if (!options) {
            options = {};
        }

        //Remove the version information
        delete $scope.item.version;

        //Check if we need to save a new version
        if (options.version) {
            //Check if our message has changed since last time
            if (versionMessage != options.version) {
                console.log('Saving with new message', options.version);
                //If it has then append the version message
                versionMessage = options.version;
                $scope.item.version = options.version;
                $scope.item.milestone = true;
            }
        }

        if (!$scope.item.title) {
            Notifications.warning('Please provide a title before saving');
            return;
        }

        Notifications.status('Saving');


        //////////////////////////////////////////////////////////

        //Duplicate the item and append the ignore socket attribute
        var saveData = angular.copy($scope.item);
        saveData.ignoreSocket = FluroSocket.getSocketID();

        //////////////////////////////////////////////////////////

        if ($scope.item._id) {
            //Updating existing content
            FluroContent.resource('site').update({
                id: $scope.item._id,
            }, saveData, saveSuccess, saveFail);
        } else {
            console.log('Save New')
                //If we are creating new content
            FluroContent.resource('site', true).save(saveData, saveSuccess, saveFail);
        }
    }


    function saveSuccess(res) {

        if (res.status == 500) {
            //delete $scope.item.__v;
            console.log('VERSION ERROR!!', res, $scope.item)
            return Notifications.error('Version Error, Please try again');
        }

        Notifications.status('Site saved successfully')
        console.log('SAVE SUCCESS', res);

        if (res._id) {

            if (!$scope.item._id) {
                $scope.item._id = res._id;

                console.log('Move to the State please!');
                //Move to the actual book instead of the create form
                $state.go('build.default', {
                    id: res._id
                })

            } else {
                $scope.item._id = res._id;
                $scope.item.__v = res.__v;
            }
        }



        $scope.isSaving = false;
    }

    function saveFail(res) {
        console.log('SAVE FAILED', res);

        if (res.data) {
            if (res.data.errors) {
                _.each(res.data.errors, function(err) {
                    Notifications.error(err)
                })
            } else {
                Notifications.error('Site failed to save ' + res.data);
            }
        } else {
            Notifications.error(res);
        }

        delete $scope.item.__v;


        $scope.isSaving = false;
    }


});