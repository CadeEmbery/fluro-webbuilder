app.controller('ComponentMarketController', function($scope, $modal, FluroContent) {


	if (!$scope.item.components) {
        $scope.item.components = [];
    }

    $scope.refreshMarketComponents();

	$scope.componentUsed = function(component) {


		var match  = _.find($scope.item.components, {_id:component._id});

		if(match) {
			return true;
		}
	}


	$scope.viewReadme = function(component) {
		 var modalInstance = $modal.open({
            template: '<div class="panel panel-default"><div class="panel-body"><h2>{{model.title}}</h2><div compile-html="model.readme"/></div></div>',
            controller:function($scope) {
            	$scope.model = component;
            }
        });
        
        // modalInstance.result.then(function() {
        //     CheckinService.refresh();
        // });
	}

	function upsert(arr, key, newval) {
	    
	    var match = _.find(arr, key);

	    if(match){
	        var index = _.indexOf(arr, _.find(arr, key));
	        arr.splice(index, 1, newval);
	    } else {
	        arr.push(newval);
	    }
	}



	$scope.updateComponent = function(component) {

		var cleanedUp = angular.fromJson(angular.toJson(component));

		upsert($scope.item.components, {_id:cleanedUp._id}, cleanedUp);
	}

	$scope.useComponent = function(component) {
		if($scope.componentUsed(component)) {
			return;
		}

		$scope.item.components.push(component);
	}


 })