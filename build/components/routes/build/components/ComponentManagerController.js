app.controller('ComponentManagerController', function($scope, $rootScope, ModalService, FluroContent, $sessionStorage) {


    $scope.selected = {};
    $scope.settings = {};
    $scope.search = {};

    if(!$scope.item.components) {
        $scope.item.components = [];
    }

    /////////////////////////////////////////

    $scope.sessionStorage = $sessionStorage;

    /////////////////////////////////////////

    $scope.refreshAvailableComponents = function() {
        console.log('Refresh available components')
        $scope.availableComponents = FluroContent.resource('component', false, true).query();
    }

    $scope.refreshAvailableComponents();

    ////////////////////////////////////

    $scope.refreshMarketComponents = function() {
        console.log('Refresh market components')
        $scope.marketComponents = FluroContent.endpoint('my/components', false, true).query();
    }

    ////////////////////////////////////

    /**/
    $scope.sameAccount = function(component) {
        var accountId = component.account;

        if(!accountId) {
            return;
        }
        if (accountId._id) {
            accountId = accountId._id;
        }

        return (accountId == $rootScope.user.account._id);
    }
    /**/

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.addComponent = function() {

        var startScript = "app.components.directive('[DIRECTIVENAME]', function() {\n    return {\n        restrict: 'E', \n        replace:true, \n        template:'[FLURO_TEMPLATE]', \n        link: function($scope, $elem, $attr) {\n        } \n    } \n});";

        var newScript = {
            title: 'New Component',
            js: startScript,
            html: '<div></div>',
            realms: []
        };

        //$scope.item.components.push(newScript)
        //$scope.selected.component = newScript;

        /**/
        var params = {};
        params.template = newScript;

        ModalService.create('component', params, function(res) {

            $scope.item.components.push(res);
            $scope.selected.component = res;
            $scope.refreshAvailableComponents();
        });
        /**/
    }

    ////////////////////////////////////

    $scope.editComponent = function() {
        ModalService.edit($scope.selected.component, function(res) {

            $scope.replaceComponent(res);

            //$scope.item.components.push(res);
            $scope.refreshAvailableComponents();
        });
        /**/
    }

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.removeComponent = function() {
        if ($scope.selected.component) {

            _.pull($scope.item.components, $scope.selected.component);
            $scope.selected.component = null;
        }
    }

    ////////////////////////////////////

    $scope.pullComponent = function(component) {

        console.log('Pull Component', component);
        if (component) {

            var match = _.find($scope.item.components, {_id:component._id});
            if(match) {
                _.pull($scope.item.components, match);
                $scope.selected.component = null;
            }
        }
    }


    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.replaceComponent = function(component) {
        var cleanedUp = angular.fromJson(angular.toJson(component));
        _.assign($scope.selected.component, cleanedUp);
        console.log('Updated component', $scope.selected.component);
    }

    ////////////////////////////////////

    $scope.updateComponent = function(component) {
        $scope.settings.status = 'processing';

        function success(res) {
            $scope.settings.status = 'ready';
            $scope.replaceComponent(res);
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to update component', res)
        }

        FluroContent.resource('component').get({
            id: component._id
        }).$promise.then(success, failed)
    }

    ////////////////////////////////////

    $scope.publishComponent = function(component) {
        if (!component._id) {
            if (!component.realms && !component.realms.length) {
                return;
            }
        }

        $scope.settings.status = 'processing';
        //delete component._id;

        function success(res) {
            $scope.settings.status = 'ready';
            component._id = res._id;
            console.log('Saved component', res)

            $scope.refreshAvailableComponents();
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to save component', res)
        }

        if (component._id) {
            FluroContent.resource('component').update({
                id: component._id,
            }, component, success, failed);

        } else {
            FluroContent.resource('component').save(component).$promise.then(success, failed)
        }
    }


    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////


    /*
    if (!$scope.item.components) {
        $scope.item.components = [];
    }

    $scope.scriptSelection = {};


    $scope.settings = {};


    ////////////////////////////////////


    $scope.publishScript = function(component) {


        


        if (!component._id) {
            if (!component.realms && !component.realms.length) {
                return;
            }
        }

        $scope.settings.status = 'processing';
        //delete component._id;

        function success(res) {
            $scope.settings.status = 'ready';
            component._id = res._id;
            console.log('Saved component', res)
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to save component', res)
        }

        if (component._id) {

                FluroContent.resource('component').update({
                id: component._id,
            }, component, success, failed);

        } else {
            FluroContent.resource('component').save(component).$promise.then(success, failed)
        }
    }


    ////////////////////////////////////


     $scope.removeScriptID = function(component) {
        delete component._id;
     }

    ////////////////////////////////////

    $scope.updateScript = function(component) {
        $scope.settings.status = 'processing';

        function success(res) {
            $scope.settings.status = 'ready';


            var cleanedUp = angular.fromJson(angular.toJson(res));
            _.assign(component, cleanedUp);
            console.log('Updated component', $scope.selected.component);
        }

        function failed(res) {
            $scope.settings.status = 'ready';
            console.log('Failed to update component', res)
        }

        FluroContent.resource('component').get({
            id: component._id
        }).$promise.then(success, failed)
    }

    ////////////////////////////////////

    $scope.addScript = function() {


        var startScript = "app.components.directive('[DIRECTIVENAME]', function() {\n    return {\n        restrict: 'E', \n        replace:true, \n        template:'[TEMPLATE]', \n        link: function($scope, $elem, $attr) {\n        } \n    } \n});";

        //var startScript = "app.components.directive('[DIRECTIVENAME]', function() { \n return { \n restrict: 'E', \n replace:true, \n template:'[TEMPLATE]', \n link: function($scope, $elem, $attr) { \n \n } \n } \n });";

        var newScript = {
            title: 'New Script',
            js: startScript,
            html: '<div></div>',
        };


        $scope.item.components.push(newScript)
        $scope.selected.component = newScript;


    }



*/





});