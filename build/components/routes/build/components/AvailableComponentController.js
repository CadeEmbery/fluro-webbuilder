app.controller('AvailableComponentController', function($scope, FluroContent) {


	if (!$scope.item.components) {
        $scope.item.components = [];
    }



	$scope.componentUsed = function(component) {


		var match  = _.find($scope.item.components, {_id:component._id});

		if(match) {
			return true;
		}
	}



	function upsert(arr, key, newval) {
	    
	    var match = _.find(arr, key);

	    if(match){
	        var index = _.indexOf(arr, _.find(arr, key));
	        arr.splice(index, 1, newval);
	    } else {
	        arr.push(newval);
	    }
	}



	$scope.updateComponent = function(component) {

		var cleanedUp = angular.fromJson(angular.toJson(component));

		upsert($scope.item.components, {_id:cleanedUp._id}, cleanedUp);
	}

	$scope.useComponent = function(component) {
		if($scope.componentUsed(component)) {
			return;
		}

		$scope.item.components.push(component);
	}


 })