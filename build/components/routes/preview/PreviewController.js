app.controller('PreviewController', function($scope, $rootScope, $templateCache, $timeout, $compile, Fluro, $interval) {


    //Set Preview Mode
    $rootScope.previewMode = true;

    /////////////////////////////////////////////////

    //Get the site element
    var $element = angular.element('.site');

    /////////////////////////////////////////////////
    /////////////////////////////////////////////////

    //Store the cached template
    var cachedTemplate;
    var defaultTemplate = '<fluro-header/><fluro-content/><fluro-footer/>';

    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////

    function refreshPreview() {

        //Start off with nothing
        var newTemplate = '';

        /////////////////////////////////////////


        if(window.opener && window.opener.buildService) {
            var buildService = window.opener.buildService;

            $rootScope.buildService = buildService;
            $rootScope.site =
            $scope.site = buildService.site;
            $rootScope.currentRoute = buildService.selection.item;
            $rootScope.bodyClasses = 'route-' + _.kebabCase(buildService.selection.item.state);


            //Check if there is an outer html template
            if ($scope.site.outerHTML && $scope.site.outerHTML.length) {
                newTemplate = buildService.site.outerHTML;
            }


            ///////////////////////////////////

            //Add each template to the template cache
            _.each(buildService.site.templates, function(sectionTemplate) {
                $templateCache.put(sectionTemplate.key, sectionTemplate.html);
            })
        } else {
            return console.log('window opener not found'); 
        }


       /**


        //Get the scope from the opening window
        var $parentScope = window.opener.angular.element('#builder').scope();

        if ($parentScope) {
            $rootScope.builder =
                $scope.builder = $parentScope;
            $scope.site = $scope.builder.item;
            $rootScope.site = $scope.builder.item;
            $rootScope.currentRoute = $scope.builder.routeSelection.item;

            //Set route class
            $rootScope.bodyClasses = 'route-' + _.kebabCase($rootScope.currentRoute.state);

            //Check if there is an outer html template
            if ($scope.site.outerHTML && $scope.site.outerHTML.length) {
                newTemplate = $scope.site.outerHTML;
            }

            ///////////////////////////////////

        } else {
            $scope.builder = null;
            $scope.site = null;
        }
        /**/



        

        /////////////////////////////////////////

        //No need to change anything
        if (cachedTemplate == newTemplate) {
            return;
        }

        /////////////////////////////////////////

        //Set the template to our new template
        cachedTemplate = newTemplate;

        /////////////////////////////////////////

        //Create our actual template
        var template = cachedTemplate;

        /////////////////////////////////////////

        //Check if we should use the default template
        if (!template || !template.length) {
            template = defaultTemplate;
        }

        /////////////////////////////////////////

        //Replace elements
        template = template.replace(/<fluro-header\/>/g, '<route class="site-header" ng-if="!$root.currentRoute.disableHeader" ng-model="buildService.site.header"></route>');
        template = template.replace(/<fluro-content\/>/g, '<div class="site-page-container"><route class="site-page" ng-model="buildService.selection.item"></route></div>');
        template = template.replace(/<fluro-footer\/>/g, '<route class="site-footer" ng-if="!$root.currentRoute.disableFooter" ng-model="buildService.site.footer"></route>');

        /////////////////////////////////////////

        //Clear the old
        $element.empty();


        $timeout(function() {
            //In with the new
            var compiled = $compile(template)($scope);
            $element.append(compiled);
        }, 1000);



    }

    /////////////////////////////////////////////////

    //Refresh every second
    $interval(refreshPreview, 1500);

    /////////////////////////////////////////////////

    /*

        $scope.$watch('builder.routeSelection.item', function(route) {
        	$scope.route = route;
        })

        /*
            if ($scope.item.components.length) {
                var script = '';

                _.each(components, function(component) {

                    var directiveName = component.directive;
                    //script += component.js;
                    //var html = component.html;

                    //Add the html string
                    template += '<' + directiveName + '></' + directiveName + '>';

                    //var url = Fluro.apiURL + '/get/scripts/component/' + component._id;
                    script += '<script type="text/lazy">' + component.compiled + '</script>';

                    //$ocLazyLoad.inject('textAngular');

                })

                //script += '</script>';




                //console.log(compiledTemplate);

                $element.append(script);
                $compile($element.contents())($scope);

                $element.append(template);
                $compile($element.contents())($scope);


                /*
                        var string = '';

                        var list = _.map(components, function(component) {

                            string += '<div class="panel panel-default"><' + component.directive + '><' + component.directive + '></div>';

                            //Get the script location
                            var url = Fluro.apiURL + '/get/scripts/component/' + component._id;

                            //Return the details to lazyload
                            return {
                                type: 'js',
                                path: url,
                            }
                        });


                        console.log('Lazy load', list);

                        $ocLazyLoad.load(list).then(function() {

                            
                            


                            var template = string;
                            var compiled = $compile(template)($scope);

                            $element.append(compiled);

                        }, function() {
                            console.log('Failed to load components')
                        });
    */
    //  }




    /*
		console.log(window.opener);
		var main = window.opener.angular.element('#outer').scope();

		$scope.item = main.item;
		$scope.currentRoute = main.currentRoute;
	

    /*
	 $scope.getStylesheetURL = function(css) {
         var url = Fluro.apiURL + '/get/scripts/css/' + css._id +'?v='+ css.updated;
         return url;
    }




	/*
	window.setData = function(data) {
		console.log('SET DATA', data)

      $scope.$apply(function(){
        $scope.item = data;
      });
    };
    */

});