app.service('FluroWindowService', function($window, $timeout) {

    var controller = {};
    controller.breakpoint = 'xs';

    /////////////////////////////////////

    function updateSize() {
        controller.width = $window.innerWidth;
        controller.height = $window.innerHeight;

        if(controller.width < 768) {
            return controller.breakpoint = 'xs';
        }

        if(controller.width < 992) {
            return controller.breakpoint = 'sm';
        }

        if(controller.width < 1200) {
            return controller.breakpoint = 'md';
        }
        
        return controller.breakpoint = 'lg';
    }

    /////////////////////////////////////

    angular.element($window).bind("resize", updateSize);
    updateSize();

    /////////////////////////////////////

    return controller;

});