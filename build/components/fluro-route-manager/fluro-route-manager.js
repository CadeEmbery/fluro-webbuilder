app.directive('routeManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-route-manager/fluro-route-manager.html',
        controller: 'RouteManagerController',
    };
});


app.controller('RouteManagerController', function($scope, BuildSelectionService) {

    $scope.settings = {};
/*
    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    //Now we have nested routes we need to flatten
    function getFlattenedFolders(array) {
        return _.chain(array)
        .reduce(function(result, route, key) {

            if (route.type == 'folder') {
                result.push(route);
            }

            if (route.routes && route.routes.length) {
                //Recursive search
                var folders = getFlattenedFolders(route.routes);

                if (folders.length) {
                    result.push(folders)
                }
            }

            return result;

        }, [])
        .flatten()
        .compact()
        .value();

    }
    /**/



    // $scope.$watch(function() {

    //     var item = $scope.selection.item;


    //     return _.includes($scope.model, item);
    // }, function(breakout) {
    //     $scope.breakout = breakout;
    // })

    $scope.containsDot = function(string) {
        return _.contains(string, '.');
    }


    $scope.hasSlug = function(route) {
        if(route) {
            //Check if the path has a slug or an id
            return (_.includes(route.url, ':slug') || _.includes(route.url, ':_id'));
        }
    }




    $scope.hasIssue = function(route) {
        if(!route.state || !route.state.length) {
            return 'No state name defined';
        } else {

             var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model);

            var dupe =_.some(flattenedRoutes, function(r) {
                return (route.state == r.state && route != r);
            }) 

            if(dupe) {
                return 'Conflicting state name with existing page (' + route.state + ')';
            }
        }

        if(!route.url || !route.url.length) {
            return 'No URL path defined';
        }

        if($scope.hasSlug(route)) {
            if(!route.seo) {
                return 'No slug title';
            }

            if(!route.seo.slugTitle || !route.seo.slugTitle.length) {
                return 'No slug title';
            }

            if(!route.seo.slugDescription || !route.seo.slugDescription.length) {
                return 'No slug description';
            }

            if(!route.seo.slugImage || !route.seo.slugImage.length) {
                return 'No slug share image';
            }
        }

        if(!route.sections || !route.sections.length) {
            return 'No sections created for route';
        }

        return false;
    }


    if (!$scope.model) {
        $scope.model = [];
    } else {

        //Flattened routes
        var flattenedRoutes = BuildSelectionService.getFlattenedRoutes($scope.model);

        //First route
        var firstRoute = _.find(flattenedRoutes, function(route) {
            return (!route.type || route.type != 'folder');
        });

        //Select first route
        $scope.selection.select(firstRoute);
    }

    // _.each($scope.model, function(route) {
    //     if(!route.routes) {
    //         route.routes = [];
    //     }
    // })

    //////////////////////////////////////////
    /*
    $scope.removeFolder = function(item) {

        //Get the flattened routes
        var folders = getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {
            var found = _.contains(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }
    }
    */
   
    //////////////////////////////////////////

    $scope.removeRoute = function() {

        //Get the item
        var item = $scope.selection.item;

        //Get the flattened routes
        var folders = BuildSelectionService.getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {

            var found = _.contains(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }


        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.duplicate = function(route, parent) {

        if (!route) {
            return;
        }

        var index = $scope.model.indexOf(route);



        var newRoute = angular.copy(route);
        newRoute.state = '';
        newRoute.title = newRoute.title + ' copy';
        newRoute.url = '';

        if (index != -1) {
            $scope.model.splice(index + 1, 0, newRoute);
        } else {
            $scope.model.push(newRoute);
        }


        $scope.selection.select(newRoute);
    }


    $scope.editFolderName = function(node, event) {
        event.preventDefault();
        event.stopPropagation();

        $scope.$targetFolder = node;
    }

    //////////////////////////////////////////

    $scope.addRoute = function() {

        var newRoute = {
            title: 'New page',
            //includeInMenu:true,
        };

        $scope.model.push(newRoute);
        $scope.selection.select(newRoute);
    }

    //////////////////////////////////////////

    $scope.addFolder = function() {

        var newFolder = {
            title: 'New Folder',
            type: 'folder',
            routes: []
                //includeInMenu:true,
        };

        $scope.model.push(newFolder);
    }

})