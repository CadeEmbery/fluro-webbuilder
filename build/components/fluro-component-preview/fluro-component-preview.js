

app.directive('fluroComponentPreview', function(Fluro, $compile) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        templateUrl: 'fluro-component-preview/fluro-component-preview.html',
        link: function($scope, $element, $attr) {

            $scope.$watch('model', function(component) {
                $element.empty();

                if(component) {

                    var directiveName = component.directive;
                    var script = '<script type="text/lazy">' + component.compiled + '</script>';
                    var template = '<' + directiveName + '></' + directiveName + '>';

                    $element.append(script);
                    $compile($element.contents())($scope);

                    $element.append(template);
                    $compile($element.contents())($scope);
                }
            });

            /*
            $scope.$watch('model.length', function() {


                $element.empty();

                var components = $scope.model;

                if (components.length) {

                    var template = '';
                    var script = '';

                    _.each(components, function(component) {

                        var directiveName = component.directive;
                        //script += component.js;
                        //var html = component.html;

                        //Add the html string
                        template += '<' + directiveName + '></' + directiveName + '>';

                        //var url = Fluro.apiURL + '/get/scripts/component/' + component._id;
                        script += '<script type="text/lazy">' + component.compiled + '</script>';

                        //$ocLazyLoad.inject('textAngular');

                    })

                    //script += '</script>';


                    

                    //console.log(compiledTemplate);

                    $element.append(script);
                    $compile($element.contents())($scope);

                    $element.append(template);
                    $compile($element.contents())($scope);


                    /*
                    var string = '';

                    var list = _.map(components, function(component) {

                        string += '<div class="panel panel-default"><' + component.directive + '><' + component.directive + '></div>';

                        //Get the script location
                        var url = Fluro.apiURL + '/get/scripts/component/' + component._id;

                        //Return the details to lazyload
                        return {
                            type: 'js',
                            path: url,
                        }
                    });


                    console.log('Lazy load', list);

                    $ocLazyLoad.load(list).then(function() {

                        
                        


                        var template = string;
                        var compiled = $compile(template)($scope);

                        $element.append(compiled);

                    }, function() {
                        console.log('Failed to load components')
                    });

                }
            });
*/
        },
    };
});