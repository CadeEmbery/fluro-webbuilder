///////////////////////////////////////////////////////

app.directive('routeSection', function($compile, $rootScope, $templateCache, $timeout, $parse, FluroContentRetrieval) {
    return {
        restrict: 'E',
        replace: true,
        require: '^route',
        scope: {
            model: '=ngModel',
            route: '=ngRoute',
        },
        template: '<section></section>',
        link: function($scope, $element, $attr, $ctrl) {

            //Disable Caching for webbuilder
            var noCache = true;

            //Add the controller to the scope
            $scope.page = $ctrl.page;

            ///////////////////////////////////////////////////////

            //Inherit the slug from the route
            $scope.$watch(function() {
                return $scope.page.slug;
            }, function(slug) {
                $scope.slug = slug;
            });


            ///////////////////////////////////////////////////////

            $scope.$watch('model.temp.highlight', function(highlight) {
                if (highlight) {
                    $element.addClass('_highlight');
                } else {
                    $element.removeClass('_highlight');
                }
            })

            // ///////////////////////////////////////////////////////

            // $scope.$watch('route.testSlug', function(slug) {


            //     // var appendPosts = $scope.route.appendPosts;
            //     var appendPostCount = $scope.route.appendPostCount;
            //     var appendContactDetail = $scope.route.appendContactDetail;
            //     var appendProcess = $scope.route.appendProcess;

            //     ////////////////////////////////////////////////////

            //     if(!appendPostCount || !appendPostCount.length) {
            //         appendPostCount = 'all';
            //     }

            //     if(!appendProcess || !appendProcess.length) {
            //         appendProcess = 'all';
            //     }

            //     ////////////////////////////////////////////////////

            //     if (slug) {
            //         FluroContentRetrieval.get([slug._id], noCache, {
            //             searchInheritable:true,
            //             appendPostCount:appendPostCount,
            //             // appendPosts:appendPosts,
            //             appendProcess:appendProcess,
            //             appendContactDetail:appendContactDetail,
            //         }).then(function(res) {
            //             if (res && res.length) {
            //                 console.log('Got slug!!');
            //                 $scope.slug = res[0];
            //             }
            //         });
            //     } else {
            //         delete $scope.slug;
            //     }
            // })

            ///////////////////////////////////////////////////////

            $scope.$watch('model.content', function(nodes) {

                if ($scope.model.select && $scope.model.select.length) {
                    // console.log('Partial Populate');
                    //console.log('POPULATE', nodes);
                    if (nodes && nodes.length) {
                        FluroContentRetrieval.populatePartial(nodes, $scope.model.select, noCache, true).then(function(res) {
                            $scope.items = res;
                        });
                    } else {
                        delete $scope.items;
                    }
                } else {
                    //console.log('POPULATE', nodes);
                    if (nodes && nodes.length) {
                        FluroContentRetrieval.populate(nodes, noCache, {
                            searchInheritable: true
                        }).then(function(res) {
                            $scope.items = res;
                        });
                    } else {
                        delete $scope.items;
                    }
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('model.keys', function(keyGroups) {

                /////////////////////////////////

                var nids = _.chain(keyGroups)
                    .map(function(group) {
                        return group.content;
                    })
                    .flatten()
                    .compact()
                    .value();

                /////////////////////////////////

                if (nids && nids.length) {
                    FluroContentRetrieval.populate(nids, noCache, {
                        searchInheritable: true
                    }).then(function(res) {

                        $scope.content = _.reduce(keyGroups, function(result, keyGroup) {

                            result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                                if (nid._id) {
                                    nid = nid._id;
                                }
                                return _.find(res, {
                                    _id: nid
                                });;
                            })

                            return result;

                        }, {})
                    });
                } else {
                    delete $scope.content;
                }


            }, true)


            ///////////////////////////////////////////////////////

            $scope.$watch('model.params', function(params) {
                //Add Parameters to the scope
                $scope.params = params;
            });

            ///////////////////////////////////////////////////////

            $scope.$watch('model.collections', function(nodes) {

                //Reset
                $scope.collections = [];
                $scope.collectionResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var collectionIds = _.chain(nodes)
                        .map(function(collection) {
                            if (collection._id) {
                                return collection._id;
                            } else {
                                return collection;
                            }
                        })
                        .compact()
                        .value();


                    //Get the collections
                    FluroContentRetrieval.getMultipleCollections(collectionIds, true).then(function(res) {


                        //If nothing then stop here
                        if (!res) {
                            return;
                        }

                        ///////////////////////////////////////////////////

                        //Add the collections to scope
                        $scope.collections = res;

                        //Add all of the items to the collection results
                        $scope.collectionResults = _.chain(res)
                            .map(function(collection) {
                                return collection.items;
                            })
                            .flatten()
                            .uniq()
                            .value();
                    });
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('model.queries + model.queryVariables + slug', function() {

                var nodes = $scope.model.queries;

                $scope.results = [];
                $scope.queryResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var queryIds = _.chain(nodes)
                        .map(function(query) {
                            if (query._id) {
                                return query._id;
                            } else {
                                return query;
                            }
                        })
                        .compact()
                        .value();

                    ///////////////////////////////////////////////////

                    //Check if we need to go further
                    if (queryIds && queryIds.length) {

                        var variables;

                        if ($scope.model.queryVariables) {

                            //Parse the variables against the current $scope
                            variables = _.mapValues($scope.model.queryVariables, function(value) {
                                var val = $parse(value)($scope);
                                return val;
                            })

                            //Only send through variables that exist
                            variables = _.pick(variables, _.identity);
                        }

                        //Now query the ids
                        FluroContentRetrieval.queryMultiple(queryIds, true, variables).then(function(res) {

                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            $scope.queryResults = [];

                            ///////////////////////////////////////////////////

                            //Loop through each query and pull out results
                            $scope.results = _.reduce(queryIds, function(result, queryId) {

                                var array = res[queryId];

                                $scope.queryResults.push(array);

                                if (array && array.length) {

                                    //Add each item that matches
                                    _.each(array, function(item) {
                                        result.push(item);
                                    })
                                }

                                return result;

                            }, []);

                            ///////////////////////////////////////////////////


                        });
                    }
                }
            }, true)


            var existingTimeout;

            ///////////////////////////////////////////////////////

            $scope.$watch(function() {

                var html;

                if ($scope.model) {
                    if ($scope.model.template) {
                        html = $templateCache.get($scope.model.template);
                    } else {
                        html = $scope.model.html;
                    }
                }

                return html;

            }, function() {

                var html = $scope.model.html;

                if ($scope.model.template) {
                    html = $templateCache.get($scope.model.template);
                }
                $element.empty();
                if (html) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                    //$element.replaceWith(template);




                    /**
                    if(existingTimeout) {
                        $timeout.cancel(existingTimeout);
                    }



                    //Rasterise to canvas
                    existingTimeout = $timeout(function() {


                        var originalWidth = $element.outerWidth();
                        var originalHeight = $element.outerHeight();



                        var canvasWidth = 100;
                        var canvasHeight = Math.floor((originalHeight / originalWidth) * 100);
                        //canvas.width = canvas.height * (canvas.clientWidth / canvas.clientHeight);

                        console.log(originalWidth + 'x' + originalHeight, canvasWidth + 'x' + canvasHeight);

                        html2canvas($element, {
                            useCORS: true,
                            // width: canvasWidth,
                            // height: canvasHeight,
                            // allowTaint:true,
                            // logging:true,
                            onrendered: function(canvas) {


                                // canvas.width = canvas.height * (canvas.clientWidth / canvas.clientHeight);

                                // var extra_canvas = document.createElement("canvas");
                                // // extra_canvas.setAttribute('width',70);
                                // // extra_canvas.setAttribute('height',70);
                                // var ctx = extra_canvas.getContext('2d');
                                // ctx.drawImage(canvas,0,0, canvas.width, canvas.height, 0,0,70,70);
                                var dataURL = canvas.toDataURL();


                                // console.log('GOT CANVAS', canvas)

                                $scope.model.thumbnail = dataURL; //canvas.toDataURL();



                                // canvas is the final rendered <canvas> element
                            }
                        });
                    }, 1000);

                    /**/

                }
            })


        },
    };
})