app.directive('route', function($compile, $parse, $rootScope, FluroContentRetrieval) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            route: '=ngModel',
        },
        template: '<div></div>',
        controller: function($scope, $controller) {

            //Disable Caching for webbuilder
            var noCache = true;

            //////////////////////////////////////////

            // var controller = this;

            ///////////////////////////////////////////////////////

            $scope.$watch('route.testSlug', function(slug) {


                // var appendPosts = $scope.route.appendPosts;
                var appendPostCount = $scope.route.appendPostCount;
                var appendPosts = $scope.route.appendPosts;
                var appendContactDetail = $scope.route.appendContactDetail;
                var appendProcess = $scope.route.appendProcess;
                var appendForms = $scope.route.appendForms;
                var appendTeams = $scope.route.appendTeams;
                var appendAssignments = $scope.route.appendAssignments;
                var includePublicSearchResults = $scope.route.includePublic;

                ////////////////////////////////////////////////////

                //May as well make this standard
                if(!appendPostCount || !appendPostCount.length) {
                    appendPostCount = 'all';
                }

                if(!appendProcess || !appendProcess.length) {
                    appendProcess = 'all';
                }

                if(!appendForms || !appendForms.length) {
                    appendForms = 'all';
                }

                // console.log('APPEND FORMS', appendForms)

                ////////////////////////////////////////////////////

                if (slug) {
                    FluroContentRetrieval.get([slug._id], noCache, {
                        searchInheritable:true,
                        includePublic:includePublicSearchResults,
                        appendPostCount:appendPostCount,
                        appendPosts:appendPosts,
                        appendProcess:appendProcess,
                        appendContactDetail:appendContactDetail,
                        appendForms:appendForms,
                        appendAssignments:appendAssignments,
                        appendTeams:appendTeams,
                    }).then(function(res) {
                        if (res && res.length) {
                            // console.log('Got route slug!!', res[0]);
                            $scope.slug = res[0];
                        }
                    });
                } else {
                    delete $scope.slug;
                }
            })


            //////////////////////////////////////////

            $scope.$watch('route.content', function(nodes) {

                //console.log('POPULATE', nodes);
                if (nodes && nodes.length) {
                    FluroContentRetrieval.populate(nodes, noCache, {
                        searchInheritable: true
                    }).then(function(res) {
                        $scope.items = res;
                    });
                } else {
                    $scope.items = [];
                }

            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('route.keys', function(keyGroups) {

                /////////////////////////////////

                var nids = _.chain(keyGroups)
                    .map(function(group) {
                        return group.content;
                    })
                    .flatten()
                    .compact()
                    .value();

                /////////////////////////////////

                if (nids && nids.length) {
                    FluroContentRetrieval.populate(nids, noCache, {
                        searchInheritable: true
                    }).then(function(res) {

                        $scope.content = _.reduce(keyGroups, function(result, keyGroup) {

                            result[keyGroup.key] = _.map(keyGroup.content, function(nid) {
                                if (nid._id) {
                                    nid = nid._id;
                                }
                                return _.find(res, {
                                    _id: nid
                                });;
                            })

                            return result;

                        }, {})
                    });
                } else {
                    $scope.content = {};
                }


            }, true)


            ///////////////////////////////////////////////////////

            //Add Parameters to the scope
            if($scope.route.params) {
                $scope.params = $scope.route.params;
            } else {
                $scope.params = {};
            }
            

            ///////////////////////////////////////////////////////

            $scope.$watch('route.collections', function(nodes) {

                //Reset
                $scope.collections = [];
                $scope.collectionResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var collectionIds = _.chain(nodes)
                        .map(function(collection) {
                            if (collection._id) {
                                return collection._id;
                            } else {
                                return collection;
                            }
                        })
                        .compact()
                        .value();


                    //Get the collections
                    FluroContentRetrieval.getMultipleCollections(collectionIds, true)
                        .then(function(res) {


                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            //Add the collections to scope
                            $scope.collections = res;

                            //Add all of the items to the collection results
                            $scope.collectionResults = _.chain(res)
                                .map(function(collection) {
                                    return collection.items;
                                })
                                .flatten()
                                .uniq()
                                .value();
                        });
                }
            }, true)

            ///////////////////////////////////////////////////////

            $scope.$watch('route.queries + route.queryVariables + slug', function() {

                var nodes = $scope.route.queries;

                $scope.results = [];
                $scope.queryResults = [];

                if (nodes && nodes.length) {

                    //Get the query ids
                    var queryIds = _.chain(nodes)
                        .map(function(query) {
                            if (query._id) {
                                return query._id;
                            } else {
                                return query;
                            }
                        })
                        .compact()
                        .value();

                    ///////////////////////////////////////////////////

                    //Check if we need to go further
                    if (queryIds && queryIds.length) {

                        var variables;

                        if ($scope.route.queryVariables) {

                            //Parse the variables against the current $scope
                            variables = _.mapValues($scope.route.queryVariables, function(value) {
                                var val = $parse(value)($scope);
                                return val;
                            })

                            //Only send through variables that exist
                            variables = _.pick(variables, _.identity);
                        }

                        //Now query the ids
                        FluroContentRetrieval.queryMultiple(queryIds, true, variables).then(function(res) {

                            //If nothing then stop here
                            if (!res) {
                                return;
                            }

                            ///////////////////////////////////////////////////

                            $scope.queryResults = [];

                            ///////////////////////////////////////////////////

                            //Loop through each query and pull out results
                            $scope.results = _.reduce(queryIds, function(result, queryId) {

                                var array = res[queryId];

                                $scope.queryResults.push(array);

                                if (array && array.length) {

                                    //Add each item that matches
                                    _.each(array, function(item) {
                                        result.push(item);
                                    })
                                }

                                return result;

                            }, []);

                            ///////////////////////////////////////////////////


                        });
                    }
                }
            }, true);

            ///////////////////////////////////////////////////////////

            this.page = $scope;

            ///////////////////////////////////////////////////////////

            //Bootstrap our custom controller
            if ($scope.route.controllerName) {
                $controller($scope.route.controllerName, {
                    $scope: $scope
                });
            }

            


        },
        link: function($scope, $element, $attr, $routeController) {

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            $scope.$watch('route.html', function(html) {
                $element.empty();
                if (html && html.length) {
                    var template = $compile(html)($scope);
                    $element.append(template);
                } else {
                    //List the sections
                    var template = $compile('<route-section ng-model="section" ng-route="route" ng-repeat="section in route.sections track by $index" ng-if="!section.disabled"></route-section>')($scope);
                    $element.append(template);
                }
            });
        },
    };
});