


app.directive('fluroComponentLoader', function(Fluro, $compile) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        template: '<div class="component-loader"></div>',
        link: function($scope, $element, $attr) {

            //String
            var watchString = 'model.components + model.scripts + model.bower.length';

            /////////////////////////////////////

            $scope.$watch(watchString, function() {

                //Clear out old scripts
                $element.empty();

                if (!$scope.model) {
                    return;
                }

                //Write our script
                var script = '';
                //var styles = '';
                //var sassString = '';

                ///////////////////////////////////////


                if ($scope.model.components && $scope.model.components.length) {

                    console.log('Load components')
                    
                    _.each($scope.model.components, function(component) {
                        // script += component.js;
                        script += '<script type="text/lazy">';
                        script += 'console.log("Init Component ' + component.title + '");\n';
                         script += component.compiled;
                         script += '</script>';
                        // var scriptString = component.js;

                        // //Replace the template placeholder with our html
                        // if (String(component.html) && String(component.html).length) {
                        //     scriptString = scriptString.replace('[TEMPLATE]', String(component.html).replace(/\n|\t/g, ' '));
                        // }


                        //Add the CSS if its available
                        //sassString += component.css;

                        //Add the compiled js
                        // script += scriptString;

                    })
                    
                }

                ///////////////////////////////////////

                //Include Inline Script
                if ($scope.model.scripts && $scope.model.scripts.length) {
                    console.log('Load inline scripts');


                    // script += '<script type="text/lazy">\n';
                    _.each($scope.model.scripts, function(s) {
                        script += '<script type="text/lazy">\n';
                        script += 'console.log("Init ' +s.title + '");\n';
                        script += s.body + ';\n';
                        script += '</script>';
                    })


                    // script += '</script>';
                }

                ///////////////////////////////////////

                //If there are bower components aswell then include the vendor scripts
                if ($scope.model.bower && $scope.model.bower.length) {
                    console.log('Load Bower Scripts');
                    var vendorJSUrl = Fluro.apiURL + '/get/site/' + $scope.model._id + '/js';
                    script += '<script type="text/lazy" src="' + vendorJSUrl + '"></script>';

                    /**
                    var vendorCSSUrl = Fluro.apiURL + '/get/site/' + $scope.model._id + '/css';
                    styles += '<link href="' + vendorCSSUrl + '" rel="stylesheet" type="text/css"/>';
                    /**/
                }

                ///////////////////////////////////////

                /**
                if (sassString && sassString.length) {
                    //Compile the SASS
                    Sass.compile(sassString, function(result) {
                        styles += '<style type="text/css">' + result.text + '</style>';
                    })
                }


                ///////////////////////////////////////

                if(styles && styles.length) {
                    $element.append(styles);
                }
                /**/

                ///////////////////////////////////////

                if (script && script.length) {
                    //Do the magic!
                    $element.append(script);
                    $compile($element.contents())($scope);
                }


            }, true);
        },
    };
});