$.Redactor.prototype.insertImage = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertImage', 'Insert image');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertImage', 'fa-image');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();
        this.buffer.set();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $scope = angular.element(this.$element).scope();

        /////////////////////////////////////////////////////////

        var modalInstance = $scope.modal.open({
            template: '<content-browser ng-model="images" ng-done="submit" ng-type="' + "'image'" + '"></content-browser>',
            controller: function($modalInstance, $rootScope, $scope) {
                $scope.images = [];
                $scope.submit = function() {

                    //Map each image to id and url             
                    var mapped = _.map($scope.images, function(img) {
                        return {
                            _id: img._id,
                            url: $rootScope.asset.imageUrl(img._id)
                        }
                    });

                    //Insert all the images into the text area
                    controller.insert(mapped);

                    //Close the modal
                    $modalInstance.close($scope.images);
                }
            }
        });
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(images) {
        var self = this;

        //Restore the selection
        self.selection.restore();

        //Only proceed if we actually selected some images
        if (images.length) {
            var str = '';
            _.each(images, function(image) {
                str += '<img src="' + image.url + '" ng-src="{{$root.asset.imageUrl(' + "'" + image._id + "'" + ')}}"/>';
            })

            //Insert
            self.insert.html(str);
            self.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};