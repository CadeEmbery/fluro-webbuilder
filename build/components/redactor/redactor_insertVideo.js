$.Redactor.prototype.video = function() {
    return {
        reUrlYoutube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig,
        reUrlVimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
        getTemplate: function() {
            return String() + '<section id="redactor-modal-video-insert">' + '<label>' + this.lang.get('video_html_code') + '</label>' + '<textarea class="form-control" id="redactor-insert-video-area" style="height: 160px;"></textarea>' + '</section>';
        },
        init: function() {
            var button = this.button.addAfter('image', 'video', 'Embed Video');
            this.button.addCallback(button, this.video.show);
        },
        show: function() {
            this.modal.addTemplate('video', this.video.getTemplate());

            this.modal.load('video', this.lang.get('video'), 700);
            this.modal.createCancelButton();

            var button = this.modal.createActionButton(this.lang.get('insert'));
            button.on('click', this.video.insert);

            this.selection.save();
            this.modal.show();

            $('#redactor-insert-video-area').focus();

        },
        insert: function() {
            var data = $('#redactor-insert-video-area').val();

            if (!data.match(/<iframe|<video/gi)) {
                data = this.clean.stripTags(data);

                // parse if it is link on youtube & vimeo
               /* var iframeStart = '<iframe style="width: 500px; height: 281px;" src="',
                    iframeEnd = '" frameborder="0" allowfullscreen></iframe>';
*/

                   var iframeStart = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="';
                   var iframeEnd ='" frameborder="0" allowfullscreen></iframe></div>';



                if (data.match(this.video.reUrlYoutube)) {
                    data = data.replace(this.video.reUrlYoutube, iframeStart + '//www.youtube.com/embed/$1' + iframeEnd);
                } else if (data.match(this.video.reUrlVimeo)) {
                    data = data.replace(this.video.reUrlVimeo, iframeStart + '//player.vimeo.com/video/$2' + iframeEnd);
                }
            }

            this.selection.restore();
            this.modal.close();

            var current = this.selection.getBlock() || this.selection.getCurrent();

            if (current) $(current).after(data);
            else {
                this.insert.html(data);
            }

            this.code.sync();
        }
    };
}




/*$.Redactor.prototype.insertVideo = function() {

    var controller = {}

    ////////////////////////////////////

    controller.init = function() {
        var button = this.button.add('insertVideo', 'Insert video');
        this.button.addCallback(button, controller.select);
        this.button.setAwesome('insertVideo', 'fa-video');
    }

    ////////////////////////////////////

    controller.select = function() {

        //Save Selection
        this.selection.save();
        this.buffer.set();

        /////////////////////////////////////////////////////////

        //Get the Scope
        var $scope = angular.element(this.$element).scope();

        /////////////////////////////////////////////////////////

        var modalInstance = $scope.modal.open({
            template: '<content-browser ng-model="videos" ng-done="submit" ng-type="' + "'video'" + '"></content-browser>',
            controller: function($modalInstance, $rootScope, $scope) {
                $scope.videos = [];
                $scope.submit = function() {

                    //Map each video to id and url             
                    var mapped = _.map($scope.videos, function(img) {
                        return {
                            _id: img._id,
                            url: $rootScope.asset.videoUrl(img._id)
                        }
                    });

                    //Insert all the videos into the text area
                    controller.insert(mapped);

                    //Close the modal
                    $modalInstance.close($scope.videos);
                }
            }
        });
    }

    /////////////////////////////////////////////////////////

    controller.insert = function(videos) {
        var self = this;

        //Restore the selection
        self.selection.restore();

        //Only proceed if we actually selected some videos
        if (videos.length) {
            var str = '';
            _.each(videos, function(video) {
                str += '<fluro-video src="' + video.url + '" ng-src="{{$root.asset.videoUrl(' + "'" + video._id + "'" + ')}}"/>';
            })

            //Insert
            self.insert.html(str);
            self.code.sync();
        }
    }

    /////////////////////////////////////////////////////////

    return controller;
};
*/