app.directive('fluroStylesheetLoader', function(Fluro, $compile, FluroSocket) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        template: '<div class="stylesheet-loader"></div>',
        link: function($scope, $element, $attr) {

            //////////////////////////////////////////////////////////////////////////////////////////

            //Watch for any changes
            $scope.$watch('model.stylesheets.length + model.afterStylesheets.length + model.bower.length + model.components.length', reloadStylesheets);

            //////////////////////////////////////////////////////////////////////////////////////////

            function reloadStylesheets() {
                console.log('Refresh Styles')

                //Clear the element
                $element.empty();

                //////////////////////////

                //If there is a site
                if (!$scope.model) {
                    return;
                }

                ///////////////////////////////////////

                var script = '';

                ///////////////////////////////////////

                // THIS IS HANDLED IN Configuration.js now
                // //If there are bower components aswell then include the vendor scripts
                // if ($scope.model.bower && $scope.model.bower.length) {
                //     console.log('Load Bower CSS');
                //     var vendorUrl = Fluro.apiURL + '/get/site/vendor/' + $scope.model._id + '/css';
                //     script += '<link href="' + vendorUrl + '" rel="stylesheet" type="text/css"/>';
                // }

                ///////////////////////////////////////
                ///////////////////////////////////////

                //Keep track of all the stylesheet ids
                var ids = [];

                ///////////////////////////////////////

                if ($scope.model.stylesheets && $scope.model.stylesheets.length) {
                    var stylesheetIds = _.reduce($scope.model.stylesheets, function(result, include) {
                        if (include._id) {
                            result.push(include._id);
                        } else {
                            result.push(include);
                        }

                        return result;

                    }, []);

                    ids = ids.concat(stylesheetIds);

                }

                ///////////////////////////////////////

                //If there are components with styles
                if ($scope.model.components && $scope.model.components.length) {
                    var componentIds = _.reduce($scope.model.components, function(result, include) {
                        //console.log('Component', include.title, include);
                        if (include.css && include.css.length) {
                            if (include._id) {
                                result.push(include._id);
                            } else {
                                result.push(include);
                            }
                        }
                        return result;
                    }, []);

                    ///////////////////////////////////////
                    //  console.log('Load the components', $scope.model.components.length, componentIds);
                    if (componentIds.length) {

                        ids = ids.concat(componentIds);
                    }


                }

                ///////////////////////////////////////

                if ($scope.model.afterStylesheets && $scope.model.afterStylesheets.length) {
                    var afterStyles = _.reduce($scope.model.afterStylesheets, function(result, include) {
                        if (include._id) {
                            result.push(include._id);
                        } else {
                            result.push(include);
                        }
                        return result;
                    }, []);

                    ids = ids.concat(afterStyles);
                }

                ///////////////////////////////////////

                if (ids.length) {
                    //Compiled SASS Url
                    var url = Fluro.apiURL + '/get/compiled/scss?ids=' + ids.join(',');

                    if (Fluro.token) {
                        url += '&access_token=' + Fluro.token;
                    }

                    //Include as a link tag
                    script += '<link href="' + url + '" rel="stylesheet" type="text/css"/>';
                }

                ///////////////////////////////////////

                if (script && script.length) {
                    //Do the magic!
                    $element.append(script);
                    $compile($element.contents())($scope);

                    //return reloadStylesheets();
                }

            }

            //////////////////////////////////////////////////////////////////////////////////////////

            //Listen for any socket events
            FluroSocket.on('content.edit', function(socketUpdate) {               


                //Get the updated item id
                var itemID = socketUpdate.item;
                if(itemID._id) {
                    itemID = itemID._id;
                }

                

                //////////////////////////////////////////////////////////////////////////////////////////

                var combinedDependencies = [].concat($scope.model.components, $scope.model.stylesheets, $scope.model.afterStylesheets)
                
                console.log('CSS UPDATE', socketUpdate);

                var requireUpdate = _.chain(combinedDependencies)
                .compact()
                .some(function(item) {

                    if(!item) {
                        return false;
                    } 

                    if (item._id) {
                    
                        return (item._id == itemID);
                    } else {
                        // console.log('Check ITEM', item, itemID, item == itemID);
                        return (item == itemID);
                    }

                })
                .value();

                //////////////////////////////////////////////////////////////////////////////////////////

                if (requireUpdate) {
                    reloadStylesheets();
                }

            });

            //////////////////////////////////////////////////////////////////////////////////////////



        },
    };
});