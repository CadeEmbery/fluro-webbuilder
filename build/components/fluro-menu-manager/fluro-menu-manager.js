app.directive('menuManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel'
        },
        // Replace the div with our template
        templateUrl: 'fluro-menu-manager/fluro-menu-manager.html',
        controller: 'MenuManagerController',
    };
});


app.controller('MenuManagerController', function($scope, SiteTools, $rootScope, ObjectSelection) {

    $scope.selection = new ObjectSelection();
    $scope.selection.multiple = false;


    if (!$scope.model) {
        $scope.model = [];
    } else {
        $scope.selection.select($scope.model[0]);
    }

    //////////////////////////////////////////

    $scope.addAllMenuItems = function(array) {

        //Get flattened site routes
        var siteRoutes = SiteTools.getFlattenedRoutes($rootScope.site.routes);


        _.each(siteRoutes, function(route) {

            var newMenuItem = {
                title:route.title,
                items:[],
                state:route.state,
                type:'state',
            };

            array.push(newMenuItem);

        })
    }

    //////////////////////////////////////////

    $scope.availablePages = function() {

        if($rootScope.site) {

            return SiteTools.getFlattenedRoutes($rootScope.site.routes);
            //return $rootScope.site.routes;
        }
    }

    //////////////////////////////////////////

    $scope.removeMenu = function() {
        _.pull($scope.model, $scope.selection.item);
        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.addMenuItem = function(array) {
        var newMenuItem = {
            title:'New Menu Item',
            items:[],
            type:'state',
        };

        array.push(newMenuItem);
    }

    //////////////////////////////////////////

    $scope.addMenu = function() {

        var newMenu = {
            title:'New Menu',
            items:[],
        };

        $scope.model.push(newMenu);
        $scope.selection.select(newMenu);
    }

})