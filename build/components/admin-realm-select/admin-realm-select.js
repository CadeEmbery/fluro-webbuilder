app.directive('realmSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            definedName: '=ngType',
            restrictedRealms: '=ngRestrict',
            popoverDirection: '@',
        },
        templateUrl: 'admin-realm-select/admin-realm-select.html',
        controller: 'RealmSelectController',
    };
});





// app.controller('RealmSelectListController', function($scope) {


// });




app.directive('realmSelectItem', function($compile, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            realm: '=ngModel',
            selection: '=ngSelection',
            searchTerms: '=ngSearchTerms',
        },
        templateUrl: 'admin-realm-select/admin-realm-select-item.html',
        link: function($scope, $element, $attrs) {



            $scope.contains = function(realm) {
                return _.some($scope.selection, function(i) {
                    if (_.isObject(i)) {
                        return i._id == realm._id;
                    } else {
                        return i == realm._id;
                    }
                });
            }

            $scope.toggle = function(realm) {

                var matches = _.filter($scope.selection, function(existingRealmID) {

                    var searchRealmID = realm._id;
                    if (existingRealmID._id) {
                        existingRealmID = existingRealmID._id;
                    }

                    return (searchRealmID == existingRealmID);
                })

                ////////////////////////////////

                if (matches.length) {

                    _.each(matches, function(match) {
                        _.pull($scope.selection, match);
                    })
                } else {
                    $scope.selection.push(realm);
                }

            }

            ///////////////////////////////////////////////////

            function hasActiveChild(realm) {

                var active = $scope.contains(realm);

                if (active) {
                    return true;
                }

                if (realm.children && realm.children.length) {
                    return _.some(realm.children, hasActiveChild);
                }
            }

            ///////////////////////////////////////////////////

            $scope.isExpanded = function(realm) {
                if (realm.expanded) {
                    return realm.expanded;
                }

                return $scope.activeTrail(realm);
            }

            ///////////////////////////////////////////////////

            $scope.activeTrail = function(realm) {
                if (!realm.children) {
                    return;
                }

                return _.some(realm.children, hasActiveChild);
            }



            var template = '<realm-select-item ng-model="subRealm" ng-search-terms="searchTerms" ng-selection="selection" ng-repeat="subRealm in realm.children | filter:searchTerms | orderBy:' + "'title'" + ' track by subRealm._id"></realm-select-item>';

            var newElement = angular.element(template);
            $compile(newElement)($scope);
            $element.find('.realm-select-children').append(newElement);
        }
    };
});


app.controller('RealmSelectController', function($scope, $rootScope, TypeService, FluroAccess, FluroSocket) {


    $scope.dynamicPopover = {
        templateUrl: 'admin-realm-select/admin-realm-popover.html',
    };

    if (!$scope.popoverDirection) {
        $scope.popoverDirection = 'bottom';
    }


    ////////////////////////////////////////////////////

    //Create a model if none exists
    if (!$scope.model) {
        $scope.model = [];
    }

    //////////////////////////////////////////////////

    FluroSocket.on('content.create', socketUpdate);

    //////////////////////////////////////////////////

    function socketUpdate(eventData) {
        //New realm was created
        console.log('New realm available')
        if (eventData.data._type == 'realm') {
            return reloadAvailableRealms();
        }
    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content.create', socketUpdate);
    });

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    function reloadAvailableRealms() {

        if (!TypeService.definedTypes) {
            TypeService.refreshDefinedTypes().$promise.then(definedTypesLoaded);
        } else {
            //We need to wait until all the defined types have been loaded
            TypeService.definedTypes.$promise.then(definedTypesLoaded);
        }


        //////////////////////////////////////

        function definedTypesLoaded() {

            //If the parent definition exists get it otherwise get the primitive type
            var typeDefinition = TypeService.getTypeFromPath($scope.definedName);

            if (!typeDefinition) {
                return;
            }

            ////////////////////////////////////////////////////////

            var parentType = $scope.definedName;
            if (typeDefinition.parentType) {
                parentType = typeDefinition.parentType;
            }

            //Now we get the realm tree from FluroAccess service
            FluroAccess.retrieveSelectableRealms('create', $scope.definedName, parentType)
                .then(function(realmTree) {

                    //Add the tree to the scope
                    $scope.tree = realmTree;

                    if ($scope.restrictedRealms && $scope.restrictedRealms.length) {
                        console.log('Restricted Realms', $scope.restrictedRealms)

                        var restrictedRealmIDs = _.map($scope.restrictedRealms, function(realm) {
                            if (realm._id) {
                                return realm._id;
                            } else {
                                return realm;
                            }
                        });

                        ///////////////////////////////////////

                        function getFlatRealms(realm) {

                            if (!realm.children) {
                                return realm;
                            }

                            //Loop through each item
                            var children = _.map(realm.children, getFlatRealms);

                            children.push(realm);
                            return children;
                        }

                        ///////////////////////////////////////

                        var flattened = _.chain($scope.tree)
                            .map(getFlatRealms)
                            .flattenDeep()
                            .compact()
                            .uniq()
                            .value()

                        // console.log('FLATTENED', flattened);

                        ///////////////////////////////////////

                        $scope.tree = _.filter(flattened, function(realm) {
                            return _.includes(restrictedRealmIDs, realm._id);
                        });

                        ////////////////////////////////////////////////////////

                        //If only one realm is available select it by default
                        if ($scope.tree.length == 1 && !$scope.model.length) {
                            $scope.model = [$scope.tree[0]];
                        }

                    } else {
                        ////////////////////////////////////////////////////////

                        //If only one realm is available select it by default
                        if (realmTree.length == 1 && !$scope.model.length) {
                            //If there are no children select this only realm by default
                            if (!realmTree.children || !realmTree.children.length) {
                                $scope.model = [realmTree[0]];
                            }
                        }
                    }


                });
        }
    }

    //////////////////////////////////

    //One for good measure
    reloadAvailableRealms();

    //////////////////////////////////

    $scope.isSelectable = function(realm) {

        var realmID = realm;
        if (realmID._id) {
            realmID = realmID._id;
        }

        //////////////////////////////////

        if ($scope.restrictedRealms && $scope.restrictedRealms.length) {

            //Check if the realm we are asking about is in the restricted realms
            return _.some($scope.restrictedRealms, function(restrictedRealmID) {
                if (restrictedRealmID._id) {
                    restrictedRealmID = restrictedRealmID._id;
                }
                return realmID == restrictedRealmID
            });

        } else {
            return true;
        }
    }

    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////

    $scope.contains = function(realm) {
        return _.some($scope.model, function(i) {
            if (_.isObject(i)) {
                return i._id == realm._id;
            } else {
                return i == realm._id;
            }
        });
    }

    $scope.toggle = function(realm) {

        var matches = _.filter($scope.model, function(r) {
            var id = r;
            if (_.isObject(r)) {
                id = r._id;
            }

            return (id == realm._id);
        })

        ////////////////////////////////

        if (matches.length) {
            $scope.model = _.reject($scope.model, function(r) {
                var id = r;
                if (_.isObject(r)) {
                    id = r._id;
                }

                return (id == realm._id);
            });
        } else {
            $scope.model.push(realm);
        }

    }



});