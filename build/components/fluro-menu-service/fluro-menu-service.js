app.service('FluroMenuService', function($rootScope) {

    var controller ={}
    
    //////////////////////////////////////////

    controller.get = function(string, id) {
        
    	var menu;
    	var result;

        if($rootScope.site) {
           menu = _.find($rootScope.site.menus, {key:string});
        }

        if(menu && id) {
        	result = _.find(menu.items, {id:id});
        } else {
        	result = menu;
        }

        return result;
    }

    return controller;

})