app.directive('search', function() {

    return {
        restrict: 'E',
        replace:true,
        scope:{
        	model:'=ngModel'
        },
        templateUrl: 'admin-search/search.html',
        controller: 'SearchFormController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('SearchFormController', function($scope) {


	$scope.clear = function() {
		$scope.model = '';
	}

});
