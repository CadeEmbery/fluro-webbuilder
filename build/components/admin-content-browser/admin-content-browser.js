app.directive('contentBrowser', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            typeName: '=ngType',
            params: '=?params',
            done: '=ngDone',
        },
        // Replace the div with our template
        templateUrl: 'admin-content-browser/admin-content-browser.html',
        controller: 'ContentBrowserController',
    };
});

//////////////////////////////////////////////////////////////////////////

app.controller('ContentBrowserController', function($scope, TypeService, $rootScope, FluroContent, $http, $filter, Fluro, FluroStorage) {


    if ($scope.typeName) {
        $scope.type = TypeService.getTypeFromPath($scope.typeName);
        $scope.title = 'Select ' + $scope.type.plural;
    } else {
        $scope.title = 'Select content';
    }

    //////////////////////////////

    $scope.close = function() {
        if ($scope.done) {
            $scope.done();
        }
    }

    //////////////////////////////

    $scope.toggle = function(item) {
        if (_.some($scope.model, {
            _id: item._id
        })) {
            $scope.deselect(item)
        } else {
            $scope.select(item)
        }
    }

    //////////////////////////////

    $scope.select = function(item) {
        if (!_.some($scope.model, {
            _id: item._id
        })) {
            $scope.model.push(item)
        }


        // console.log('SELECT IS', $scope, $scope.model, item);

    }

    //////////////////////////////

    $scope.deselect = function(item) {
        var found = _.find($scope.model, {
            _id: item._id
        })

        if (found) {
            _.pull($scope.model, found);
        }
    }

    //////////////////////////////

    $scope.isSelected = function(item) {
        return _.some($scope.model, {
            _id: item._id
        })
    }


    //////////////////////////////

    if(!$scope.params) {
        $scope.params = {};
    }

    //////////////////////////////

    var local = FluroStorage.localStorage('content-browser.' + $scope.typeName)
    var session = FluroStorage.sessionStorage('content-browser.' + $scope.typeName)

    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {
                    status: ['active'],
                }
        }
    } else {
        $scope.search = session.search;
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize: 8,
        }
    } else {
        $scope.pager = session.pager;
    }




    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
    }

    //////////////////////////////

    $scope.$watch('search', updateFilters, true);

    //////////////////////////////

    $scope.refresh = function() {


        //If browsing for an account
        if($scope.type && $scope.type.path == 'account') {
            
            //Get all the options
            return FluroContent.resource('account', false, true).query().$promise.then(function(results) {
                //console.log('Got content results', results)
                $scope.items = results;
                updateFilters();
            });
        }

        ///////////////////////////////////////////


        var queryDetails = {}

        
        if ($scope.type) {
            queryDetails.type = $scope.type.path
        }

        if ($scope.params.searchInheritable) {
            queryDetails.searchInheritable = true;
        }


        if ($scope.params.syntax) {
            queryDetails.syntax = $scope.params.syntax;
        }




        //Get all the options
        FluroContent.endpoint('content', false, true).query(queryDetails).$promise.then(function(results) {
            //console.log('Got content results', results)
            $scope.items = results;
            updateFilters();
        });
    }

    //Load the library
    $scope.refresh();

    /*
    $http.get(Fluro.apiURL + '/node/browse', {
        ignoreLoadingBar: true,
    }).then(function(response) {
    	$scope.items = response.data;
    	updateFilters();
    })
*/


    $scope.removeFilter = function(key) {
        if ($scope.search.filters) {
            delete $scope.search.filters[key];
        }
    }

    $scope.$watch('search', updateFilters, true);


    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    function updateFilters() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);


            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;


            //Update the current page
            $scope.updatePage();

        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {


        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        var filteredItems = $scope.filteredItems;

        //Order by title
        //filteredItems = $filter('orderBy')(filteredItems, 'title');
        //filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);



        //Now break it up into pages
        var pageItems = $filter('startFrom')(filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }



    ///////////////////////////////////////////////

    /*
	///////////////////////////////////////

	$scope.$watch('search.terms', function(keywords) {
		return $http.get(Fluro.apiURL + '/search', {
            ignoreLoadingBar: true,
            params: {
                keys: keywords,
                //type: $scope.restrictType,
                limit: 100,
            }
        }).then(function(response) {
           $scope.items = response.data;
        });
	})
*/

    ///////////////////////////////////////


});