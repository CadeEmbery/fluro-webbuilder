app.directive('interactionForm', function($compile) {

    //////console.log('interaction form controller directive')
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            callback: '=?callback',
        },
        transclude: true,
        controller: 'InteractionFormController',
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };


});

////////////////////////////////////////////////////////////////////////

app.directive('webform', function($compile) {

    //////console.log('web form controller directive')

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            integration: '=ngPaymentIntegration',
            vm: '=?config',
            debugMode: '=?debugMode',
            callback: '=?callback',
            linkedEvent: '=?linkedEvent',
        },
        transclude: true,
        controller: 'InteractionFormController',
        templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
                // console.log('Transclude content', clone)
                //$element.find('[ng-transclude-here]').append(clone); // <-- will transclude it's own scope
            });
        },
    };
});


app.config(function(formlyConfigProvider) {


    formlyConfigProvider.setType({
        name: 'currency',
        extends: 'input',
        overwriteOk: true,
        controller: ['$scope', function($scope) {
            /*
            ////////console.log('CURRENCY SCOPE', $scope);

            $scope.$watch('model[options.key]', function(val) {

                if(!$scope.model[$scope.options.key] && $scope.model[$scope.options.key] != 0 ) {
                    ////////console.log('Set!')
                    $scope.model[$scope.options.key] = 0;
                }
            })
            /**/
        }],

        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: 'ng-currency'
                },
                fractionValue: {
                    attribute: 'fraction',
                    bound: 'fraction'
                },
                minimum: {
                    attribute: 'min',
                    bound: 'min'
                },
                maximum: {
                    attribute: 'max',
                    bound: 'max'
                }
            },
            templateOptions: {
                customAttrVal: '',
                required: true,
                fraction: 2,
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {

                        var numericValue = Number($modelValue);

                        if (isNaN(numericValue)) {
                            return false;
                        }

                        //Get Minimum and Maximum Amounts
                        var minimumAmount = scope.options.data.minimumAmount;
                        var maximumAmount = scope.options.data.maximumAmount;

                        if (minimumAmount && numericValue < minimumAmount) {
                            return false;
                        }

                        if (maximumAmount && numericValue > maximumAmount) {
                            return false;
                        }

                        return true;
                    }
                }
            }
        }
    });


})

////////////////////////////////////////////////////////////////////////

app.run(function(formlyConfig, $templateCache) {

    //////console.log('web form run')
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {
        var fluroErrorTemplate = $templateCache.get('fluro-interaction-form/field-errors.html');
        return '<div>' + template + fluroErrorTemplate + '</div>';
    });

    //////////////////////////////////

    formlyConfig.setType({
        name: 'upload',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/upload/upload.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: 'FluroInteractionFormUploadController',
    });


    //////////////////////////////////

    formlyConfig.setType({
        name: 'multiInput',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/multi.html',
        defaultOptions: {
            noFormControl: true,
            wrapper: ['bootstrapLabel', 'bootstrapHasError'],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: ['$scope', function($scope) {
            $scope.copyItemOptions = copyItemOptions;

            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
        }]
    });

    //////////////////////////////////

    // formlyConfig.setType({
    //     name: 'payment',
    //     overwriteOk: true,
    //     templateUrl: 'fluro-interaction-form/payment/payment.html',
    //     //controller: 'FluroPaymentController',
    //     defaultOptions: {
    //         noFormControl: true,
    //     },
    // });

    formlyConfig.setType({
        name: 'custom',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/custom.html',
        controller: 'CustomInteractionFieldController',
        wrapper: ['bootstrapHasError']
    });

    formlyConfig.setType({
        name: 'button-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/button-select/fluro-button-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });



    formlyConfig.setType({
        name: 'date-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/date-select/fluro-date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });



    formlyConfig.setType({
        name: 'terms',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/fluro-terms.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'order-select',
        overwriteOk: true,
        templateUrl: 'fluro-interaction-form/order-select/fluro-order-select.html',
        controller: 'FluroInteractionButtonSelectController',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    /**/

});

////////////////////////////////////////////////////////////////////////

app.controller('CustomInteractionFieldController', function($scope, FluroValidate) {
    $scope.$watch('model[options.key]', function(value) {
        if (value) {
            if ($scope.fc) {
                $scope.fc.$setTouched();
            }
        }
    }, true);
});

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

app.controller('InteractionFormController', function($scope, $q, $timeout, $rootScope, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {




    // console.log($scope.integration);

    /////////////////////////////////////////////////////////////////

    if (!$scope.vm) {
        $scope.vm = {}
    }

    /////////////////////////////////////////////////////////////////

    //Keep this available to all field scopes
    //this is used so that other fields can get calculated total
    //not just the payment summary field
    $formScope = $scope;

    /////////////////////////////////////////////////////////////////

    function debugLog() {
        if ($scope.debugMode) {
            console.log(arguments);
        }
    }


    /////////////////////////////////////////////////////////////////

    // The model object that we reference
    // on the  element in index.html
    if ($scope.vm.defaultModel) {
        $scope.vm.model = angular.copy($scope.vm.defaultModel);
    } else {
        $scope.vm.model = {};
    }

    /////////////////////////////////////////////////////////////////

    // An array of our form fields with configuration
    // and options set. We make reference to this in
    // the 'fields' attribute on the  element
    $scope.vm.modelFields = [];

    /////////////////////////////////////////////////////////////////

    //Keep track of the state of the form
    $scope.vm.state = 'ready';


    /////////////////////////////////////////////////////////////////

    $scope.correctPermissions = true;

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////


    $scope.scrollToField = function(field) {
        var element = _.get(field, 'formControl.$$element[0]');
        if (element && element.scrollIntoView) {
            element.scrollIntoView();
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    $scope.readyToSubmit = false;

    // $scope.$watch('vm.modelForm.$invalid', updateErrorList)
    $scope.$watch('vm.modelFields', validateAllFields, true)
    $scope.$watch('vm.model', validateAllFields, true)
        // $scope.$watch('vm.modelForm.$touched', updateErrorList)
        // $scope.$watch('vm.modelForm.$error.required', updateErrorList)
        // $scope.$watch('vm.modelForm.$error.validInput', updateErrorList)

    function validateAllFields() {


        $timeout(function() {

            function setValid(ready) {
                $scope.readyToSubmit = ready;
            }

            /////////////////////////////////////

            function mapRecursiveField(field) {
                if (!field) {
                    return;
                }

                var output = [field];


                if (field.fields && field.fields.length) {
                    output.push(_.map(field.fields, mapRecursiveField));
                }

                if (field.fieldGroup && field.fieldGroup.length) {
                    output.push(_.map(field.fieldGroup, mapRecursiveField));
                }

                if (field.data && field.data.replicatedFields && field.data.replicatedFields.length) {

                    //Loop through each set and map the fields
                    var nestedForms = _.chain(field.data.replicatedFields)
                        .flatten()
                        .map(mapRecursiveField)
                        .value();

                    //Add the nested fields
                    output.push(nestedForms);
                } else {
                    if (field.data && field.data.fields && field.data.fields.length) {
                        output.push(_.map(field.data.fields, mapRecursiveField));
                    }
                }

                return output;
            }

            ////////////////////////////////////////////

            $scope.errorList = _.chain($scope.vm.modelFields)
                .map(mapRecursiveField)
                .flattenDeep()
                .compact()
                .filter(function(field) {
                    var required = _.get(field, 'formControl.$error.required');
                    var invalid = _.get(field, 'formControl.$error.validInput');
                    var uploadInProgress = _.get(field, 'formControl.$error.uploadInProgress');


                    return required || invalid || uploadInProgress;
                })
                .value();

            ////////////////////////////////////////////

            //Interaction Form
            var interactionForm = $scope.vm.modelForm;

            if (!interactionForm) {
                // console.log('Invalid no form')
                return setValid(false);
            }

            if (interactionForm.$invalid) {
                // console.log('Invalid because its invalid', interactionForm.$invalid, interactionForm);
                return setValid(false);
            }

            if (interactionForm.$error) {

                if (interactionForm.$error.required && interactionForm.$error.required.length) {
                    // console.log('required input not provided');
                    return setValid(false);
                }

                if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) {
                    // console.log('valid input not provided');
                    return setValid(false);
                }

                if (interactionForm.$error.uploadInProgress && interactionForm.$error.uploadInProgress.length) {
                    // console.log('Upload has not finished');
                    return setValid(false);
                }
            }

            // ////////console.log('Form should be good to go')

            //It all worked so set to true
            setValid(true);
        })

    }

    /**
     /////////////////////////////////////////////////////////////////

    $scope.$watch('vm.modelFields', function(fields) {
        //////////console.log('Interaction Fields changed')
        $scope.errorList = getAllErrorFields(fields);

        console.log('Error List', $scope.errorList);
    }, true)

    /**/


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    formlyValidationMessages.addStringMessage('required', 'This field is required');

    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid value';
    }


    formlyValidationMessages.messages.uploadInProgress = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is still processing';
    }

    formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + ' is not a valid date';
    }

    /////////////////////////////////////////////////////////////////

    $scope.reset = function() {

        //Reset
        if ($scope.vm.defaultModel) {
            //console.log('Reset the default model')
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }
        $scope.vm.modelForm.$setPristine();
        $scope.vm.options.resetModel();


        //Act as if a new definition was changed
        definitionModelChanged();




        //Clear the response from previous submission
        $scope.response = null;
        $scope.vm.state = 'ready';


        //Reset after state change
        ////////console.log('Broadcast reset')
        $scope.$broadcast('form-reset');

    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    // //Function to run on permissions
    // function checkPermissions() {
    //     if ($rootScope.user) {
    //         //Check if we have the correct permissions
    //         var canCreate = FluroAccess.can('create', $scope.model.definitionName);
    //         var canSubmit = FluroAccess.can('submit', $scope.model.definitionName);

    //         //Allow if the user can create or submit
    //         $scope.correctPermissions = (canCreate | canSubmit);
    //     } else {
    //         //Just do this by default
    //         $scope.correctPermissions = true;
    //     }
    // }

    /////////////////////////////////////////////////////////////////

    // //Watch if user login changes
    // $scope.$watch(function() {
    //     return $rootScope.user;
    // }, checkPermissions)

    /////////////////////////////////////////////////////////////////

    $scope.$watch('model', definitionModelChanged);


    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function definitionModelChanged() {


        // ////////console.log('Model changed');
        if (!$scope.model || $scope.model.parentType != 'interaction') {
            return console.log('Model is not an interaction'); //$scope.model = {};
        }

        /////////////////////////////////////////////////////////////////

        //check if we have the correct permissions



        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        // The model object that we reference
        // on the  element in index.html
        // $scope.vm.model = {};
        if ($scope.vm.defaultModel) {
            $scope.vm.model = angular.copy($scope.vm.defaultModel);
        } else {
            $scope.vm.model = {};
        }


        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        $scope.vm.modelFields = [];

        /////////////////////////////////////////////////////////////////

        //Keep track of the state of the form
        $scope.vm.state = 'ready';

        /////////////////////////////////////////////////////////////////

        //Add the submit function
        $scope.vm.onSubmit = submitInteraction;

        /////////////////////////////////////////////////////////////////

        //Keep track of any async promises we need to wait for
        $scope.promises = [];

        /////////////////////////////////////////////////////////////////

        //Submit is finished
        $scope.submitLabel = 'Submit';

        if ($scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length) {
            $scope.submitLabel = $scope.model.data.submitLabel;
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details
        var interactionFormSettings = $scope.model.data;

        if (!interactionFormSettings) {
            interactionFormSettings = {};
        }

        if (!interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) {
            interactionFormSettings.requireFirstName = true;
            interactionFormSettings.requireLastName = true;
            interactionFormSettings.requireGender = true;
            interactionFormSettings.requireEmail = true;

            switch (interactionFormSettings.identifier) {
                case 'both':
                    interactionFormSettings.requireEmail =
                        interactionFormSettings.requirePhone = true;
                    break;
                case 'email':
                    interactionFormSettings.requireEmail = true;
                    break;
                case 'phone':
                    interactionFormSettings.requirePhone = true;
                    break;
                case 'either':
                    interactionFormSettings.askPhone = true;
                    interactionFormSettings.askEmail = true;
                    break;
            }
        }


        /////////////////////////////////////////////////////////////////

        var firstNameField;
        var lastNameField;
        var genderField;

        /////////////////////////////////////////////////////////////////

        //Gender
        if (interactionFormSettings.askGender || interactionFormSettings.requireGender) {
            genderField = {
                    key: '_gender',
                    type: 'select',
                    templateOptions: {
                        type: 'email',
                        label: 'Title',
                        placeholder: 'Please select a title',
                        options: [{
                            name: 'Mr',
                            value: 'male'
                        }, {
                            name: 'Ms / Mrs',
                            value: 'female'
                        }],
                        required: interactionFormSettings.requireGender,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return (value == 'male' || value == 'female');
                        }
                    }
                }
                //$scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //First Name
        if (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) {
            firstNameField = {
                key: '_firstName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'First Name',
                    placeholder: 'Please enter your first name',
                    required: interactionFormSettings.requireFirstName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////

        //Last Name
        if (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) {
            lastNameField = {
                key: '_lastName',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Last Name',
                    placeholder: 'Please enter your last name',
                    required: interactionFormSettings.requireLastName,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        if (genderField && firstNameField && lastNameField) {

            genderField.className = 'col-sm-2';

            firstNameField.className =
                lastNameField.className = 'col-sm-5';

            $scope.vm.modelFields.push({
                fieldGroup: [genderField, firstNameField, lastNameField],
                className: 'row'
            });
        } else if (firstNameField && lastNameField && !genderField) {
            firstNameField.className =
                lastNameField.className = 'col-sm-6';

            $scope.vm.modelFields.push({
                fieldGroup: [firstNameField, lastNameField],
                className: 'row'
            });
        } else {
            if (genderField) {
                $scope.vm.modelFields.push(genderField);
            }

            if (firstNameField) {
                $scope.vm.modelFields.push(firstNameField);
            }

            if (lastNameField) {
                $scope.vm.modelFields.push(lastNameField);
            }
        }



        /////////////////////////////////////////////////////////////////

        //Email Address
        if (interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
            var newField = {
                key: '_email',
                type: 'input',
                templateOptions: {
                    type: 'email',
                    label: 'Email Address',
                    placeholder: 'Please enter a valid email address',
                    required: interactionFormSettings.requireEmail,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return validator.isEmail(value);
                    }
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._phoneNumber || !scope.model._phoneNumber.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }

            $scope.vm.modelFields.push(newField);
        }


        /////////////////////////////////////////////////////////////////

        //Ask Phone Number
        if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
            var newField = {
                key: '_phoneNumber',
                type: 'input',
                templateOptions: {
                    type: 'tel',
                    label: 'Contact Phone Number',
                    placeholder: 'Please enter a contact phone number',
                    required: interactionFormSettings.requirePhone,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                }
            }

            if (interactionFormSettings.identifier == 'either') {
                newField.expressionProperties = {
                    'templateOptions.required': function($viewValue, $modelValue, scope) {
                        if (!scope.model._email || !scope.model._email.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

            }


            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////

        //Age / Date of birth
        if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
            var newField = {
                key: '_dob',
                type: 'dob-select',
                templateOptions: {
                    label: 'Date of birth',
                    placeholder: 'Please provide your date of birth',
                    required: interactionFormSettings.requireDOB,
                    maxDate: new Date(),
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                    // params:{
                    //     hideAge:true,
                    // }

                }
            }

            $scope.vm.modelFields.push(newField);
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        function addFieldDefinition(array, fieldDefinition) {


            if (fieldDefinition.params && fieldDefinition.params.disableWebform) {
                //If we are hiding this field then just do nothing and return here
                return;
            }




            /////////////////////////////
            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Create a new field
            var newField = {};
            newField.key = fieldDefinition.key;

            /////////////////////////////

            //Ensure integers for min/max
            fieldDefinition.minimum = parseInt(fieldDefinition.minimum);
            fieldDefinition.maximum = parseInt(fieldDefinition.maximum);

            /////////////////////////////

            //Add the class name if applicable
            if (fieldDefinition.className) {
                newField.className = fieldDefinition.className;
            }

            /////////////////////////////

            //Template Options
            var templateOptions = {};
            templateOptions.type = 'text';
            templateOptions.label = fieldDefinition.title;
            templateOptions.description = fieldDefinition.description;
            templateOptions.params = fieldDefinition.params;

            //Attach a custom error message
            if (fieldDefinition.errorMessage) {
                templateOptions.errorMessage = fieldDefinition.errorMessage;
            }

            //Include the definition itself
            templateOptions.definition = fieldDefinition;

            /////////////////////////////

            //Add a placeholder
            if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                templateOptions.placeholder = fieldDefinition.placeholder;
            } else if (fieldDefinition.description && fieldDefinition.description.length) {
                templateOptions.placeholder = fieldDefinition.description;
            } else {
                templateOptions.placeholder = fieldDefinition.title;
            }

            /////////////////////////////

            //Require if minimum is greater than 1 and not a field group
            templateOptions.required = (fieldDefinition.minimum > 0);

            /////////////////////////////

            templateOptions.onBlur = 'to.focused=false';
            templateOptions.onFocus = 'to.focused=true';

            /////////////////////////////

            //Directive or widget
            switch (fieldDefinition.directive) {
                case 'reference-select':
                case 'value-select':
                    //Detour here
                    newField.type = 'button-select';
                    break;
                case 'select':
                    newField.type = 'select';
                    break;
                case 'wysiwyg':
                    newField.type = 'textarea';
                    break;
                default:
                    newField.type = fieldDefinition.directive;
                    break;
            }


            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Allowed Options


            switch (fieldDefinition.type) {
                case 'reference':


                    //If we have allowed references specified
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                        templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id,
                            }
                        });
                    } else {
                        //We want to load all the options from the server
                        templateOptions.options = [];

                        if (fieldDefinition.sourceQuery) {

                            //We use the query to find all the references we can find
                            var queryId = fieldDefinition.sourceQuery;
                            if (queryId._id) {
                                queryId = queryId._id;
                            }

                            /////////////////////////

                            var options = {};

                            //If we need to template the query
                            if (fieldDefinition.queryTemplate) {
                                options.template = fieldDefinition.queryTemplate;
                                if (options.template._id) {
                                    options.template = options.template._id;
                                }
                            }

                            /////////////////////////

                            //Now retrieve the query
                            var promise = FluroContentRetrieval.getQuery(queryId, options);

                            //Now get the results from the query
                            promise.then(function(res) {
                                //////////console.log('Options', res);
                                templateOptions.options = _.map(res, function(ref) {
                                    return {
                                        name: ref.title,
                                        value: ref._id,
                                    }
                                })
                            });
                        } else {

                            if (fieldDefinition.directive != 'embedded') {
                                if (fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length) {
                                    //We want to load all the possible references we can select
                                    FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                        templateOptions.options = _.map(res, function(ref) {
                                            return {
                                                name: ref.title,
                                                value: ref._id,
                                            }
                                        })
                                    });
                                }
                            }
                        }
                    }
                    break;
                default:
                    //Just list the options specified
                    if (fieldDefinition.options && fieldDefinition.options.length) {
                        templateOptions.options = fieldDefinition.options;
                    } else {
                        templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                            return {
                                name: val,
                                value: val
                            }
                        });
                    }
                    break;
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //If there is custom attributes
            if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length) {
                newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = 'customAttr' + key;
                    results[customKey] = {
                        attribute: key
                    };

                    //Custom Key
                    templateOptions[customKey] = attr;

                    return results;
                }, {});
            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //What kind of data type, override for things like checkbox
            //if (fieldDefinition.type == 'boolean') {
            if (fieldDefinition.directive != 'custom') {
                switch (fieldDefinition.type) {
                    case 'boolean':
                        if (fieldDefinition.params && fieldDefinition.params.storeCopy) {
                            newField.type = 'terms';
                        } else {
                            newField.type = 'checkbox';
                        }

                        break;
                    case 'number':
                    case 'float':
                    case 'integer':
                    case 'decimal':
                        templateOptions.type = 'input';
                        // templateOptions.step = 'any';

                        if (!newField.ngModelAttrs) {
                            newField.ngModelAttrs = {};
                        }

                        /////////////////////////////////////////////

                        //Only do this if its an integer cos iOS SUCKS!
                        if (fieldDefinition.type == 'integer') {
                            // ////////console.log('Is integer');

                            templateOptions.type = 'number';
                            templateOptions.baseDefaultValue = 0;
                            //Force numeric keyboard
                            newField.ngModelAttrs.customAttrpattern = {
                                attribute: 'pattern',
                            }

                            newField.ngModelAttrs.customAttrinputmode = {
                                attribute: 'inputmode',
                            }

                            //Force numeric keyboard
                            templateOptions.customAttrpattern = "[0-9]*";
                            templateOptions.customAttrinputmode = "numeric"


                            /////////////////////////////////////////////

                            // ////////console.log('SET NUMERICINPUT')

                            if (fieldDefinition.params) {
                                if (parseInt(fieldDefinition.params.maxValue) !== 0) {
                                    templateOptions.max = fieldDefinition.params.maxValue;
                                }

                                if (parseInt(fieldDefinition.params.minValue) !== 0) {
                                    templateOptions.min = fieldDefinition.params.minValue;
                                } else {
                                    templateOptions.min = 0;
                                }
                            }

                        }
                        break;
                }

            }

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            //Default Options

            if (fieldDefinition.maximum == 1) {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0];
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id;
                        }
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]);
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0];
                        }
                    }
                }
            } else {
                if (fieldDefinition.type == 'reference' && fieldDefinition.directive != 'embedded') {
                    if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {
                        if (fieldDefinition.directive == 'search-select') {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultReferences;
                        } else {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                return ref._id;
                            });
                        }
                    } else {
                        templateOptions.baseDefaultValue = [];
                    }
                } else {
                    if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {

                        if (templateOptions.type == 'number') {
                            templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                                return Number(val);
                            });
                        } else {
                            templateOptions.baseDefaultValue = fieldDefinition.defaultValues;
                        }
                    }
                }
            }


            /////////////////////////////

            //Append the template options
            newField.templateOptions = templateOptions;

            /////////////////////////////
            /////////////////////////////
            /////////////////////////////

            newField.validators = {
                validInput: function($viewValue, $modelValue, scope) {
                    var value = $modelValue || $viewValue;

                    if (!value) {
                        return true;
                    }

                    var valid = FluroValidate.validate(value, fieldDefinition);
                    return valid;
                }
            }

            /**
            if (fieldDefinition.directive == 'upload') {
                newField.validators.uploadInProgress = function($viewValue, $modelValue, scope) {

                    var value = $modelValue || $viewValue;
                    var isArray = _.isArray(value);

                    if (isArray) {
                        var values = value;
                        var hasErrors = _.includes(values, 'error');
                        var hasProcessing = _.includes(values, 'processing');

                        if (hasErrors || hasProcessing) {
                            console.log('VALLLLLL', hasErrors, hasProcessing)
                            return false;
                        } else {
                            console.log('VAL GOOD');
                            return true;
                        }
                    } else {
                        return (value == 'error' || value == 'processing');
                    }
                }
            }
            /**/

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////




            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////


            if (fieldDefinition.directive == 'embedded') {
                newField.type = 'embedded';

                //Check if its an array or an object
                if (fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                    templateOptions.baseDefaultValue = {
                        data: {}
                    };
                } else {

                    var askCount = 0;

                    if (fieldDefinition.askCount) {
                        askCount = fieldDefinition.askCount;
                    }

                    //////////console.log('ASK COUNT PLEASE', askCount);

                    //////////////////////////////////////

                    if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                        askCount = fieldDefinition.minimum;
                    }

                    if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                        askCount = fieldDefinition.maximum;
                    }

                    //////////////////////////////////////

                    var initialArray = [];

                    //Fill with the asking amount of objects
                    if (askCount) {
                        _.times(askCount, function() {
                            initialArray.push({});
                        });
                    }

                    //////////console.log('initial array', initialArray);
                    //Now set the default value
                    templateOptions.baseDefaultValue = initialArray;
                }

                //////////////////////////////////////////

                //Create the new data object to store the fields
                newField.data = {
                    fields: [],
                    dataFields: [],
                    replicatedFields: []
                }

                //////////////////////////////////////////

                //Link to the definition of this nested object
                var fieldContainer = newField.data.fields;
                var dataFieldContainer = newField.data.dataFields;


                //////////////////////////////////////////

                //Loop through each sub field inside a group
                if (fieldDefinition.fields && fieldDefinition.fields.length) {
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }

                //////////////////////////////////////////

                var promise = FluroContent.endpoint('defined/' + fieldDefinition.params.restrictType).get().$promise;

                promise.then(function(embeddedDefinition) {

                    //Now loop through and all all the embedded definition fields
                    if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                        var childFields = embeddedDefinition.fields;

                        //Exclude all specified fields
                        if (fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length) {
                            childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            });
                        }


                        ////////console.log('EXCLUSIONS', fieldDefinition.params.excludeKeys, childFields);

                        //Loop through each sub field inside a group
                        _.each(childFields, function(sub) {
                            addFieldDefinition(dataFieldContainer, sub);
                        })
                    }
                });

                //////////////////////////////////////////

                //Keep track of the promise
                $scope.promises.push(promise);

                //////////////////////////////////////////

                // //Need to keep it dynamic so we know when its done
                // newField.expressionProperties = {
                //     'templateOptions.embedded': function() {
                //         return promise;
                //     }
                // }



            }

            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////

            if (fieldDefinition.type == 'group' && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {


                var fieldContainer;

                if (fieldDefinition.asObject) {

                    /*
                    newField = {
                        type: 'nested',
                        className: fieldDefinition.className,
                        data: {
                            fields: []
                        }
                    }
                    */
                    newField.type = 'nested';

                    //Check if its an array or an object
                    if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                        templateOptions.baseDefaultValue = {};
                    } else {

                        var askCount = 0;

                        if (fieldDefinition.askCount) {
                            askCount = fieldDefinition.askCount;
                        }

                        //////////////////////////////////////

                        if (fieldDefinition.minimum && askCount < fieldDefinition.minimum) {
                            askCount = fieldDefinition.minimum;
                        }

                        if (fieldDefinition.maximum && askCount > fieldDefinition.maximum) {
                            askCount = fieldDefinition.maximum;
                        }

                        //////////////////////////////////////

                        var initialArray = [];

                        //Fill with the asking amount of objects
                        if (askCount) {
                            _.times(askCount, function() {
                                initialArray.push({});
                            });
                        }

                        // ////////console.log('initial array', initialArray);
                        //Now set the default value
                        templateOptions.baseDefaultValue = initialArray;
                    }

                    newField.data = {
                        fields: [],
                        replicatedFields: [],
                    }

                    //Link to the definition of this nested object
                    fieldContainer = newField.data.fields;

                } else {
                    //Start again
                    newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className,
                    }

                    //Link to the sub fields
                    fieldContainer = newField.fieldGroup;
                }

                //Loop through each sub field inside a group
                _.each(fieldDefinition.fields, function(sub) {
                    addFieldDefinition(fieldContainer, sub);
                });
            }

            /////////////////////////////

            //Check if there are any expressions added to this field


            if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {

                //Include Expression Properties
                // if (!newField.expressionProperties) {
                //     newField.expressionProperties = {};
                // }

                //////////////////////////////////////////

                //Add the hide expression if added through another method
                if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {
                    fieldDefinition.expressions.hide = fieldDefinition.hideExpression;
                }

                //////////////////////////////////////////

                //Get all expressions and join them together so we just listen once
                var allExpressions = _.values(fieldDefinition.expressions).join('+');

                //////////////////////////////////////////

                //Now create a watcher
                newField.watcher = {
                    expression: function(field, scope) {


                        var watchScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                        }

                        //Return the result
                        var val = $parse(allExpressions)(watchScope);

                        return val;

                    },
                    listener: function(field, newValue, oldValue, scope, stopWatching) {

                        debugLog('Expression Changed');

                        //Create a new scope object
                        var checkScope = {
                            model: scope.model,
                            data: $scope.vm.model,
                            interaction: $scope.vm.model,
                        }

                        //Loop through each expression that needs to be evaluated
                        _.each(fieldDefinition.expressions, function(expression, key) {

                            //Get the value
                            var retrievedValue = $parse(expression)(checkScope);

                            debugLog('expression:', retrievedValue, checkScope);

                            //Get the field key
                            var fieldKey = field.key;

                            ///////////////////////////////////////

                            switch (key) {
                                case 'defaultValue':
                                    if (!field.formControl || !field.formControl.$dirty) {
                                        return scope.model[fieldKey] = retrievedValue;
                                    }
                                    break;
                                case 'value':
                                    return scope.model[fieldKey] = retrievedValue;
                                    break;
                                case 'required':
                                    return field.templateOptions.required = retrievedValue;
                                    break;
                                case 'hide':
                                    return field.hide = retrievedValue;
                                    break;
                                    // case 'label':
                                    //     if(retrievedValue) {
                                    //         var string = String(retrievedValue);
                                    //         return field.templateOptions.label = String(retrievedValue);
                                    //     }
                                    //     break;
                            }

                        });

                    }



                    ///////////////////////////////////////
                }


                //Replace expression
                //var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');



                /*
                    //Add the expression properties
                    newField.expressionProperties[key] = function($viewValue, $modelValue, scope) {


                        //Replace expression
                        var replaceExpression = expression.replace(new RegExp('model', 'g'), 'vm.model');

           


                       // var retrievedValue = $parse(replaceExpression)($scope);
                        var retrievedValue = _.get($scope, replaceExpression);

                         ////////console.log('Testing retrieved value from GET', retrievedValue, replaceExpression);

                        ////////////////////////////////////////

                        

                        return retrievedValue;
                    }
                    /**/
                //});
            }

            /////////////////////////////

            if (fieldDefinition.hideExpression && fieldDefinition.hideExpression.length) {

                newField.hideExpression = function($viewValue, $modelValue, scope) {

                    //Create a new scope object
                    var checkScope = {
                        model: scope.model,
                        data: $scope.vm.model,
                        interaction: $scope.vm.model,
                    }

                    var parsedValue = $parse(fieldDefinition.hideExpression)(checkScope);

                    // console.log('hide expression parsed value', fieldDefinition.hideExpression, checkScope, parsedValue);
                    return parsedValue;
                }


            }

            /////////////////////////////

            if (!newField.fieldGroup) {
                //Create a copy of the default value
                newField.defaultValue = angular.copy(templateOptions.baseDefaultValue);
            }


            /////////////////////////////

            if (newField.type == 'pathlink') {
                return;
            }

            /////////////////////////////
            //Push our new field into the array
            array.push(newField);


        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Loop through each defined field and add it to our form
        _.each($scope.model.fields, function(fieldDefinition) {
            addFieldDefinition($scope.vm.modelFields, fieldDefinition);
        });

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Add the required contact details

        if (!$scope.model.paymentDetails) {
            $scope.model.paymentDetails = {};
        }

        var paymentSettings = $scope.model.paymentDetails;


        // //console.log('SCOPE MODEL', $scope.model);

        /////////////////////////////////////////////////////////////////

        //Credit Card Details
        if (paymentSettings.required || paymentSettings.allow) {

            //Setup the wrapper fields
            var paymentWrapperFields = [];
            var paymentCardFields = [];


            //console.log('PAYMENT SETTINGS REQUIRED', paymentSettings.required)

            // paymentWrapperFields.push({
            //     template: '<h4><i class="fa fa-credit-card"></i> Payment details</h4>'
            // });


            // ////////console.log('BEFORE SCOPE', $scope);

            if (paymentSettings.required) {



                debugLog('-- DEBUG -- Payment Settings Required')



                //Add the payment summary
                paymentWrapperFields.push({
                    templateUrl: 'fluro-interaction-form/payment/payment-summary.html',
                    controller: ['$scope', '$parse', function($scope, $parse) {

                        //Add the payment details to the scope
                        $scope.paymentDetails = paymentSettings;


                        //Start with the required amount
                        var requiredAmount = paymentSettings.amount;

                        //Store the calculatedAmount on the scope
                        $formScope.vm.model.calculatedTotal = requiredAmount;
                        $scope.calculatedTotal = requiredAmount;

                        /////////////////////////////////////////////////////

                        var watchString = '';

                        ////////////////////////////////////////
                        ////////////////////////////////////////

                        //Map each modifier to a property string and combine them all at once
                        var modelVariables = _.chain(paymentSettings.modifiers)
                            .map(function(paymentModifier) {
                                var string = '(' + paymentModifier.expression + ') + (' + paymentModifier.condition + ')';
                                return string;
                            })
                            .flatten()
                            .compact()
                            .uniq()
                            .value();


                        if (modelVariables.length) {
                            watchString = modelVariables.join(' + "a" + ');
                        }

                        ////////////////////////////////////////

                        if (watchString.length) {
                            debugLog('Watching changes', watchString);


                            $scope.$watch(watchString, calculateTotal);
                        } else {

                            // debugLog('No watch string set', paymentSettings);

                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;
                            $scope.modifications = [];
                        }

                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////

                        function calculateTotal(watchString) {

                            // //console.log('calculate please');

                            debugLog('Recalculate total', watchString);


                            //Store the calculatedAmount on the scope
                            $scope.calculatedTotal = requiredAmount;

                            $scope.modifications = [];


                            if (!paymentSettings.modifiers || !paymentSettings.modifiers.length) {

                                debugLog('No payment modifiers set');


                                return;
                            }

                            //Loop through each modifier
                            _.each(paymentSettings.modifiers, function(modifier) {

                                var parsedValue = $parse(modifier.expression)($scope);
                                parsedValue = Number(parsedValue);

                                if (isNaN(parsedValue)) {

                                    debugLog('Payment modifier error', modifier.title, parsedValue);

                                    //throw Error('Invalid or non-numeric pricing modifier ' + modifier.title);
                                    return;
                                }

                                /////////////////////////////////////////

                                var parsedCondition = true;

                                if (modifier.condition && String(modifier.condition).length) {
                                    parsedCondition = $parse(modifier.condition)($scope);
                                }

                                //If the condition returns false then just stop here and go to the next modifier
                                if (!parsedCondition) {

                                    debugLog('inactive', modifier.title, modifier, $scope);

                                    return
                                }

                                /////////////////////////////////////////

                                var operator = '';
                                var operatingValue = '$' + parseFloat(parsedValue / 100).toFixed(2);

                                switch (modifier.operation) {
                                    case 'add':
                                        operator = '+';
                                        $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                        break;
                                    case 'subtract':
                                        operator = '-';
                                        $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                        break;
                                    case 'divide':
                                        operator = '÷';
                                        operatingValue = parsedValue;
                                        $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                        break;
                                    case 'multiply':
                                        operator = '×';
                                        operatingValue = parsedValue;
                                        $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                        break;
                                    case 'set':
                                        $scope.calculatedTotal = parsedValue;
                                        break;
                                }

                                //Let the front end know that this modification was made
                                $scope.modifications.push({
                                    title: modifier.title,
                                    total: $scope.calculatedTotal,
                                    description: operator + ' ' + operatingValue,
                                    operation: modifier.operation,
                                });
                            })


                            //If the modifiers change the price below 0 then change the total back to 0
                            if (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) {
                                $scope.calculatedTotal = 0;
                            }

                            //Add the calculated total to the rootscope

                            $timeout(function() {
                                //console.log('Set calculated total', $scope.calculatedTotal)
                                $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                            })

                        }

                        /**/
                    }],
                });
            } else {

                var amountDescription = 'Please enter an amount (' + String(paymentSettings.currency).toUpperCase() + ')';


                //Limits of amount
                var minimum = paymentSettings.minAmount;
                var maximum = paymentSettings.maxAmount;
                var defaultAmount = paymentSettings.amount;

                ///////////////////////////////////////////

                var paymentErrorMessage = 'Invalid amount';

                ///////////////////////////////////////////

                if (minimum) {
                    minimum = (parseInt(minimum) / 100);
                    paymentErrorMessage = 'Amount must be a number at least ' + $filter('currency')(minimum, '$');

                    amountDescription += 'Enter at least ' + $filter('currency')(minimum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();
                }

                if (maximum) {
                    maximum = (parseInt(maximum) / 100);
                    paymentErrorMessage = 'Amount must be a number less than ' + $filter('currency')(maximum, '$');

                    amountDescription += 'Enter up to ' + $filter('currency')(maximum, '$') + ' ' + String(paymentSettings.currency).toUpperCase();;
                }


                if (minimum && maximum) {
                    amountDescription = 'Enter a numeric amount between ' + $filter('currency')(minimum) + ' and  ' + $filter('currency')(maximum) + ' ' + String(paymentSettings.currency).toUpperCase();;
                    paymentErrorMessage = 'Amount must be a number between ' + $filter('currency')(minimum) + ' and ' + $filter('currency')(maximum);
                }

                ///////////////////////////////////////////

                //Add the option for putting in a custom amount of money
                var fieldConfig = {
                    key: '_paymentAmount',
                    type: 'currency',
                    //defaultValue: 'Cade Embery',
                    templateOptions: {
                        type: 'text',
                        label: 'Amount',
                        description: amountDescription,
                        placeholder: '0.00',
                        required: true,
                        errorMessage: paymentErrorMessage,
                        min: minimum,
                        max: maximum,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                    data: {
                        customMaxLength: 8,
                        minimumAmount: minimum,
                        maximumAmount: maximum,
                    },
                };

                if (minimum) {
                    fieldConfig.defaultValue = minimum;
                }

                paymentWrapperFields.push({
                    'template': '<hr/><h3>Payment Details</h3>'
                });
                paymentWrapperFields.push(fieldConfig);
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Setup debug card details
            var defaultCardName;
            var defaultCardNumber;
            var defaultCardExpMonth;
            var defaultCardExpYear;
            var defaultCardCVN;

            //If testing mode
            if ($scope.debugMode) {
                defaultCardName = 'John Citizen';
                defaultCardNumber = '4242424242424242';
                defaultCardExpMonth = '05';
                defaultCardExpYear = '2020';
                defaultCardCVN = '123';
            }

            //////////////////////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardName',
                type: 'input',
                defaultValue: defaultCardName,
                templateOptions: {
                    type: 'text',
                    label: 'Name on Card',
                    placeholder: 'eg. (John Smith)',
                    required: paymentSettings.required,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
            });




            /////////////////////////////////////////

            paymentCardFields.push({
                key: '_paymentCardNumber',
                type: 'input',
                defaultValue: defaultCardNumber,
                templateOptions: {
                    type: 'text',
                    label: 'Card Number',
                    placeholder: 'Card Number (No dashes or spaces)',
                    required: paymentSettings.required,
                    onBlur: 'to.focused=false',
                    onFocus: 'to.focused=true',
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {

                        /////////////////////////////////////////////
                        var luhnChk = function(a) {
                            return function(c) {

                                if (!c) {
                                    return false;
                                }
                                for (var l = c.length, b = 1, s = 0, v; l;) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                return s && 0 === s % 10
                            }
                        }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

                        /////////////////////////////////////////////

                        var value = $modelValue || $viewValue;
                        var valid = luhnChk(value);
                        return valid;
                    }
                },
            });

            paymentCardFields.push({
                className: 'row clearfix',
                fieldGroup: [{
                    key: '_paymentCardExpMonth',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpMonth,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Month',
                        placeholder: 'MM',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }, {
                    key: '_paymentCardExpYear',
                    className: "col-xs-6 col-sm-5",
                    type: 'input',
                    defaultValue: defaultCardExpYear,
                    templateOptions: {
                        type: 'text',
                        label: 'Expiry Year',
                        placeholder: 'YYYY',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }, {
                    key: '_paymentCardCVN',
                    className: "col-xs-4 col-sm-2",
                    type: 'input',
                    defaultValue: defaultCardCVN,
                    templateOptions: {
                        type: 'text',
                        label: 'CVN',
                        placeholder: 'CVN',
                        required: paymentSettings.required,
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    }
                }]
            });



            //////////////////////////////////////////////////////////

            //If the form is set to allow anonymous or has been set to ask for a specific receipt
            //email then prompt the submitter to type it in
            if (interactionFormSettings.allowAnonymous || interactionFormSettings.askReceipt) {
                paymentCardFields.push({
                    key: '_paymentEmail',
                    type: 'input',
                    templateOptions: {
                        type: 'email',
                        label: 'Receipt Email Address',
                        placeholder: 'Enter an email address for transaction receipt',
                        onBlur: 'to.focused=false',
                        onFocus: 'to.focused=true',
                    },
                });
            }

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            //Create the credit card field group
            var cardDetailsField = {
                className: "payment-details",
                fieldGroup: paymentCardFields,
                hideExpression: function($viewValue, $modelValue, scope) {

                    //If the calculated total is exactly 0
                    if ($formScope.vm.model.calculatedTotal === 0) {
                        return true;
                    }
                }
            }

            //////////////////////////////////////////////////////////

            if (paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length) {

                //Create a method selection widget
                var methodSelection = {
                    className: "payment-method-select",
                    //defaultValue:{},
                    //key:'methods',
                    // fieldGroup:cardDetailsField,
                    data: {
                        fields: [cardDetailsField],
                        settings: paymentSettings,
                    },
                    controller: ['$scope', function($scope) {

                        //Payment Settings on scope
                        $scope.settings = paymentSettings;

                        //Options
                        $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                            return method;
                        });

                        //Add card at the start
                        $scope.methodOptions.unshift({
                            title: 'Pay with Card',
                            key: 'card',
                        });

                        ////////////////////////////////////////

                        if (!$scope.model._paymentMethod) {
                            $scope.model._paymentMethod = 'card';
                        }

                        //Select the first method by default
                        $scope.selected = {
                            method: $scope.methodOptions[0]
                        };

                        $scope.selectMethod = function(method) {
                            $scope.settings.showOptions = false;
                            $scope.selected.method = method;
                            $scope.model._paymentMethod = method.key;
                        }
                    }],
                    templateUrl: 'fluro-interaction-form/payment/payment-method.html',
                    hideExpression: function($viewValue, $modelValue, scope) {

                        //If the calculated total is exactly 0
                        if ($formScope.vm.model.calculatedTotal === 0) {
                            return true;
                        }
                    }
                };

                paymentWrapperFields.push(methodSelection);

            } else {
                //Push the card details
                paymentWrapperFields.push(cardDetailsField);
            }

            //////////////////////////////////////////////////////////

            // $scope.vm.modelFields = $scope.vm.modelFields.concat(paymentWrapperFields);

            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////

            $scope.vm.modelFields.push({
                fieldGroup: paymentWrapperFields,

            });
        }

        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////

        //Wait for all async promises to resolve

        if (!$scope.promises.length) {
            // console.log('NO PROMISES');
            $scope.promisesResolved = true;
        } else {

            $scope.promisesResolved = false;

            $q.all($scope.promises).then(function() {
                // console.log('Promises have been resolved');
                $scope.promisesResolved = true;

                //updateErrorList();
                //Update the error list
                // $scope.errorList = getAllErrorFields($scope.vm.modelFields);
                // ////////console.log('All promises resolved', $scope.errorList);

                // _.each($scope.errorList, function(field) {
                //     ////////console.log('FIELD', field.templateOptions.label, field.formControl)
                // })

            });
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    function getAllErrorFields(array) {


        // field.formControl.$invalid


        return _.chain(array)
            .map(function(field) {
                if (field.fieldGroup && field.fieldGroup.length) {
                    //Get the error fields in a field group
                    return getAllErrorFields(field.fieldGroup);

                } else if (field.data && ((field.data.fields && field.data.fields.length) || (field.data.dataFields && field.data.dataFields) || (field.data.replicatedFields && field.data.replicatedFields))) {

                    var combined = [];
                    combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields);
                    combined = _.compact(combined);

                    //Get the error fields in the combined extra data
                    return getAllErrorFields(combined);
                } else {
                    return field;
                }
            })
            .flatten()
            .value();
    }



    /////////////////////////////////////////////////////////////////


    function submitInteraction() {


        //Sending
        $scope.vm.state = 'sending';
        // console.log('state.sending');

        var interactionKey = $scope.model.definitionName;
        var interactionDetails = angular.copy($scope.vm.model);


        //console.log('____________________________________');
        //console.log('SUBMIT', interactionKey, interactionDetails)

        /////////////////////////////////////////////////////////

        //Asking for Payment
        var requiresPayment;
        var allowsPayment;

        /////////////////////////////////////////////////////////


        var paymentConfiguration = $scope.model.paymentDetails;


        //Check if we have supplied payment details
        if (paymentConfiguration) {
            requiresPayment = paymentConfiguration.required;
            allowsPayment = paymentConfiguration.allow;
        }

        // ////////console.log('PAYMENT CONFIG', paymentConfiguration)
        // ////////console.log('PAYMENT ALLOWED REQUIRED', requiresPayment, allowsPayment)

        /////////////////////////////////////////////////////////

        //Check if we need a payment
        if (requiresPayment || allowsPayment) {

            //console.log('PAYMENT DETAILS', requiresPayment, allowsPayment)


            ////////////////////////////////////

            var paymentDetails = {};

            ////////////////////////////////////

            //Check if we can use alternative payment methods
            if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {

                var selectedMethod = interactionDetails._paymentMethod;
                //If the user chose an alternative payment
                if (selectedMethod && selectedMethod != 'card') {

                    //Mark which method we are using as an alternative method
                    paymentDetails.method = selectedMethod;

                    //console.log('ALTERNATIVE METHOD SO PROCESS REQUEST')
                    //////console.log('Have selected payment method other than card')
                    //Skip straight through to process the request
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Only stop here if we REQUIRE payment
            if (requiresPayment) {
                ////////console.log('TESTING REQUIRES PAYMENT', $formScope);
                //If payment modifiers have removed the need for charging a payment
                if (!$formScope.vm.model.calculatedTotal) {

                    //console.log('NO AMOUNT DUE SO PROCESS REQUEST')
                    //////console.log('Calculated total is ', 0);
                    return processRequest();
                }
            }

            ////////////////////////////////////

            //Get the payment integration 
            var paymentIntegration = $scope.integration;


            if (!paymentIntegration || !paymentIntegration.publicDetails) {

                // if (paymentConfiguration.required) {
                //     //////console.log('No payment integration was supplied for this interaction but payment is required');
                // } else {
                //     //////console.log('No payment integration was supplied for this interaction but payments are set to be allowed');
                // }

                alert('This form has not been configured properly. Please notify the administrator of this website immediately.')
                $scope.vm.state = 'ready';
                // console.log('state.ready');
                return;
            }

            /////////////////////////////////////////////////////////

            //var paymentDetails = {};

            //Ensure we tell the server which integration to use to process payment
            paymentDetails.integration = paymentIntegration._id;

            //Now get the required details for making the transaction
            switch (paymentIntegration.module) {
                case 'eway':

                    //console.log('EWAY PAYMENT')

                    if (!window.eCrypt) {
                        console.log('ERROR: Eway is selected for payment but the eCrypt script has not been included in this application visit https://eway.io/api-v3/#encrypt-function for more information');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from eWay
                    //var liveUrl = 'https://api.ewaypayments.com/DirectPayment.json';
                    //var sandboxUrl = 'https://api.sandbox.ewaypayments.com/DirectPayment.json';

                    /////////////////////////////////////////////

                    //Get the Public Encryption Key
                    var key = paymentIntegration.publicDetails.publicKey;

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var cardDetails = {};
                    cardDetails.name = interactionDetails._paymentCardName;
                    cardDetails.number = eCrypt.encryptValue(interactionDetails._paymentCardNumber, key);
                    cardDetails.cvc = eCrypt.encryptValue(interactionDetails._paymentCardCVN, key);

                    var expiryMonth = String(interactionDetails._paymentCardExpMonth);
                    var expiryYear = String(interactionDetails._paymentCardExpYear);

                    if (expiryMonth.length < 1) {
                        expiryMonth = '0' + expiryMonth;
                    }
                    cardDetails.exp_month = expiryMonth;
                    cardDetails.exp_year = expiryYear.slice(-2);

                    //Send encrypted details to the server
                    paymentDetails.details = cardDetails;

                    //Process the request
                    return processRequest();

                    break;
                case 'stripe':

                    //console.log('STRIPE PAYMENT')

                    if (!window.Stripe) {
                        console.log('ERROR: Stripe is selected for payment but the Stripe API has not been included in this application');
                        $scope.vm.state = 'ready';
                        // console.log('state.ready');
                        return;
                    }

                    //Get encrypted token from Stripe
                    var liveKey = paymentIntegration.publicDetails.livePublicKey;
                    var sandboxKey = paymentIntegration.publicDetails.testPublicKey;

                    var key = liveKey;

                    /////////////////////////////////////////////

                    if (paymentIntegration.publicDetails.sandbox) {
                        key = sandboxKey;
                    }

                    /////////////////////////////////////////////

                    //Set the stripe key
                    Stripe.setPublishableKey(key);

                    /////////////////////////////////////////////

                    //Get the card details from our form
                    var cardDetails = {};
                    cardDetails.name = interactionDetails._paymentCardName;
                    cardDetails.number = interactionDetails._paymentCardNumber;
                    cardDetails.cvc = interactionDetails._paymentCardCVN;
                    cardDetails.exp_month = interactionDetails._paymentCardExpMonth;
                    cardDetails.exp_year = interactionDetails._paymentCardExpYear;

                    /////////////////////////////////////////////

                    Stripe.card.createToken(cardDetails, function(status, response) {

                        $timeout(function() {
                            // console.log('TESTING', status, response);
                            if (response.error) {
                                //Error creating token
                                // Notifications.error(response.error);
                                console.log('Stripe token error', response);
                                $scope.processErrorMessages = [response.error.message];




                                $scope.vm.state = 'error';
                                // console.log('state.error');


                            } else {
                                //Include the payment details
                                paymentDetails.details = response;
                                return processRequest();
                            }

                        })
                    });
                    break;
            }
        } else {
            return processRequest();
        }


        ///////////////////////////////////////////////////////////////////////

        function processRequest() {
            //console.log('PROCESS INTERACTION REQUEST')

            /////////////////////////////////////////////////////////

            //Delete payment details (we don't send these to fluro)
            delete interactionDetails._paymentCardCVN;
            delete interactionDetails._paymentCardExpMonth;
            delete interactionDetails._paymentCardExpYear;
            delete interactionDetails._paymentCardName;
            delete interactionDetails._paymentCardNumber;


            /////////////////////////////////////////////////////////

            //Log the request
            //////////console.log('Process request', interactionKey, interactionDetails, paymentDetails);

            /////////////////////////////////////////////////////////

            //Allow user specified payment
            if (interactionDetails._paymentAmount) {
                paymentDetails.amount = (parseFloat(interactionDetails._paymentAmount) * 100);
            }


            /////////////////////////////////////////////////////////

            //Add the transaction email  details
            if (interactionDetails._paymentEmail) {
                paymentDetails.email = interactionDetails._paymentEmail;
            }

            delete interactionDetails._paymentEmail;

            /////////////////////////////////////////////////////////

            //Explicitly pass through the account we want to use the definition for
            var definitionID = $scope.model._id;
            if (definitionID._id) {
                //ensure its just a simple string and not an object
                definitionID = definitionID._id;
            }

            /////////////////////////////////////////////////////////

            //Query string parameters
            var params = {
                definition: definitionID,
            }

            //Attempt to send information to interact endpoint
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent, params);


            //////////////////////////////////

            //When the promise results fire the callbacks
            request.then(submissionSuccess, submissionFail)

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////

            function submissionSuccess(res) {

                //////console.log('submission success');
                /**
                //TESTING
                $scope.vm.state = 'ready';
                return ////////console.log('RES TEST', res);
                /**/

                ///////////////////////////////////

                if ($scope.callback) {
                    $scope.callback(res);
                }

                ///////////////////////////////////

                //Reset
                if ($scope.vm.defaultModel) {
                    $scope.vm.model = angular.copy($scope.vm.defaultModel);
                } else {
                    $scope.vm.model = {};
                }


                $scope.vm.modelForm.$setPristine();
                $scope.vm.options.resetModel();

                //Reset the form scope
                $formScope = $scope;

                // $scope.vm.model = {}
                // $scope.vm.modelForm.$setPristine();
                // $scope.vm.options.resetModel();

                //Response from server incase we want to use it on the thank you page
                $scope.response = res;

                //Change state
                $scope.vm.state = 'complete';
                // console.log('state.complete');
            }

            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////
            //////////////////////////////////


            function submissionFail(res) {

                //////console.log('submission failed', res);
                ////////console.log('Interaction Failed', res);
                // Notifications.error(res.data);

                $scope.vm.state = 'error';
                // console.log('state.error');

                if (!res.data) {
                    return $scope.processErrorMessages = ['Error: ' + res];
                }

                if (res.data.error) {
                    if (res.data.error.message) {
                        return $scope.processErrorMessages = [res.error.message];
                    } else {
                        return $scope.processErrorMessages = [res.error];
                    }
                }

                if (res.data.errors) {
                    return $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                        return error.message;
                    });
                }

                if (_.isArray(res.data)) {
                    return $scope.processErrorMessages = res.data;
                } else {
                    $scope.processErrorMessages = [res.data];
                }



                //$scope.vm.state = 'ready';
            }


        }
    }

});