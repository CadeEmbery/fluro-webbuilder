app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'embedded',
         overwriteOk: true,
        templateUrl: 'fluro-interaction-form/embedded/fluro-embedded.html',
        controller: 'FluroInteractionNestedController',
        wrapper: ['bootstrapHasError'],
    });

    ////console.log('fluro-embedded')

});

/**

app.controller('FluroEmbeddedDefinitionController', function($scope, $http, Fluro, $filter, FluroValidate) {


})

/**/