
/////////////////////////////////////////////////

app.directive('fieldGroupEdit', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-group-edit.html',

        //templateUrl: 'views/ui/field-group-edit.html',
        controller: 'FieldGroupEditController',
    };
});

app.controller('FieldGroupEditController', function($scope, TypeService) {

    if (!$scope.model) {
        $scope.model = {};
    }


    //  $scope.referenceParams =
    // $scope.definedTypes = TypeService.definedTypes;
    // $scope.types = TypeService.types;

    ///////////////////////////////////////////////

    $scope.$watch('model.asObjectType', function(objectType) {
        if(objectType == 'contactdetail') {
            $scope.definedTypes = _.filter(TypeService.definedTypes, {'parentType':'contactdetail'});
        } else {
            $scope.definedTypes = null;
        }
    })

    ///////////////////////////////////////////////


    
    if (!$scope.model.key) {
        $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })
});