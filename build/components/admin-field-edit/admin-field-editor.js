app.directive('fieldEditor', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-editor.html',
        //templateUrl: 'views/ui/field-editor.html',
        controller: 'FieldEditorController',
    };
});




app.controller('FieldEditorController', function($scope) {

    if (!$scope.model) {
        $scope.model = [];
    }

    $scope.selected = {};

    $scope.selectField = function(node) {
        $scope.selected.field = node;
    }

    ///////////////////////////////////////////////

    $scope.hasSubFields = function(node) {
        return (node.type == 'group' || node.directive == 'embedded');
    }


    var newObject = {
        type: 'string',
        directive: 'input',
        minimum: 0,
        maximum: 1,
        fields: [],
    }

    var newGroup = {
        type: 'group',
        fields: [],
        collapsed: true,
    }

    //Create a new object
    $scope._new = angular.copy(newObject);
    $scope._newGroup = angular.copy(newGroup);

    $scope.sortableOptions = {
        handle: ' .handle'
        // items: ' .panel:not(.panel-heading)'
        // axis: 'y'
    }


    ////////////////////////////////////////////////////

    $scope.add = function(object) {
        if ($scope.contains(object)) {
            return;
        }

        //Add it in to the right place

        var parent = $scope.model;
        var selectedField = $scope.selected.field;

        if (selectedField) {
            parent = getParentArray(selectedField);

            var index = parent.indexOf(selectedField);
            if (index != -1) {

                parent.splice(index + 1, 0, object);
                return true;
            }


        }



        parent.push(object);
        return true;
    }



    //////////////////////////////////////////

    $scope.getFieldPath = function(target) {

        var trail = [];
        var result = {};

        // function callback(actualTrail) {
        //     return actualTrail;
        // }

        getFieldPathTrail($scope.model, target, trail, result);

        return 'interaction.' + result.trail.join('.');
    }


    function getFieldPathTrail(array, target, trail, result) {
        //return $scope.getTrail($scope.model, field, []);

        for (var key in array) {
            var field = array[key];

            if (field == target) {

                if(!field.asObject && field.directive != 'embedded') {
                    trail.push(field.key);
                } else {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }


                //trail.push(field.key);
                result.trail = trail.slice();
                return;

                //return callback(trail.slice());
            }

            if (field.fields && field.fields.length) {

                if (field.asObject || field.directive == 'embedded') {

                    if (field.minimum == field.maximum == 1) {
                        trail.push(field.key);
                    } else {
                        trail.push(field.key + '[i]');
                    }

                }

                getFieldPathTrail(field.fields, target, trail, result);

                if (field.asObject || field.directive == 'embedded') {
                    trail.pop();
                }

            }
        }
    }

    /*
    $scope.getTrail = function(parentArray, child, trail) {

        //////////////////////////////////////////

        return _.chain(parentArray)
            .reduce(function(trailResult, field, key) {

                if(field == child) {
                    return trailResult;
                }

                if (field.fields && field.fields.length) {
                    trailResult.push(field.key);

                    //Recursive search
                    var subTrail = $scope.getTrail(field.fields, child, trail);
                    trailResult.pop();
                }

                return trailResult;

            }, [])
            .flatten()
            .compact()
            .value();

    }
    */

    //////////////////////////////////////////

    //Now we have nested routes we need to flatten
    function getFlattenedFields(array) {
        return _.chain(array)
            .reduce(function(result, field, key) {

                if (field.type == 'group' || field.directive) {
                    result.push(field);
                }

                if (field.fields && field.fields.length) {
                    //Recursive search
                    var folders = getFlattenedFields(field.fields);

                    if (folders.length) {
                        result.push(folders)
                    }
                }

                return result;

            }, [])
            .flatten()
            .compact()
            .value();

    }


    //////////////////////////////////////////

    function getParentArray(item) {

        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            return _.includes(field.fields, item);
        })

        //Remove it
        if (parent) {
            return parent.fields;
        }

        return $scope.model;
    }

    //////////////////////////////////////////

    $scope.remove = function(item) {

        /*
        //Get the flattened routes
        var fieldFolders = getFlattenedFields($scope.model);

        //console.log('Remove item', item, $scope.model, fields)

        //Find the parent
        var parent = _.find(fieldFolders, function(field) {
            
            return _.includes(field.fields, item);
        })
        */

        var parentArray = getParentArray(item);

        //Remove it
        if (parentArray) {
            _.pull(parentArray, item);
        }

        //Deselect
        delete $scope.selected.field;

    }

    /**
    $scope.removeRoute = function() {

        //Get the item
        var item = $scope.selection.item;

        //Get the flattened routes
        var folders = getFlattenedFolders($scope.model);

        //Find the parent
        var parentFolder = _.find(folders, function(folder) {

            var found = _.includes(folder.routes, item);
            return found;
        })

        //Remove it
        if (parentFolder) {
            _.pull(parentFolder.routes, item);
        } else {
            _.pull($scope.model, item);
        }


        $scope.selection.deselect();
    }
    /**/

    ////////////////////////////////////////////////////

    /**
    $scope.remove = function(object) {

        console.log('Remove Object', object);
        if ($scope.contains(object)) {
            delete $scope.selected.field;
            _.pull($scope.model, object);
        }
    }
    /**/

    ////////////////////////////////////////////////////

    $scope.contains = function(object) {
        return _.includes($scope.model, object)
    }

    ////////////////////////////////////////////////////

    $scope.create = function() {
        var insert = angular.copy($scope._new);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._new = angular.copy(newObject);
        }
    }

    ////////////////////////////////////////////////////

    $scope.duplicate = function() {
        if (!$scope.selected.field) {
            return;
        }

        var insert = angular.copy($scope.selected.field);
        insert.title = insert.title + ' Copy';
        insert.key = '';
        delete insert._id;


        var inserted = $scope.add(insert);

        if (inserted) {
            $scope.selected.field = insert;
            $scope._new = angular.copy(newObject);
        }

    }






    ////////////////////////////////////////////////////

    $scope.createGroup = function() {
        var insert = angular.copy($scope._newGroup);
        var inserted = $scope.add(insert);
        $scope.selected.field = insert;

        if (inserted) {
            $scope._newGroup = angular.copy(newGroup);
        }
    }

    ////////////////////////////////////////////////////
    /*
    $scope.createGroup = function() {
        var newGroup = {
            title:'New Group',
            type:'group',
            fields:[]
        }

        $scope.add(newGroup); 
    }
    */

})


/////////////////////////////////////////////////