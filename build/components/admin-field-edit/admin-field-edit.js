app.directive('fieldEdit', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        scope: {
            model: '=ngModel',
        },
        templateUrl: 'admin-field-edit/admin-field-edit.html',
        controller: 'FieldEditController',
    };
});

app.controller('FieldEditController', function($scope, TypeService) {


    //console.log('Field Edit Controller')
    //$scope.$watch('model', function(model) {

    if (!$scope.model) {
        $scope.model = {};
    }

    $scope.referenceParams =
        $scope.definedTypes = TypeService.definedTypes;
    $scope.types = TypeService.types;



    $scope.showOptions = function() {

        switch ($scope.model.directive) {
            case 'select':
            case 'button-select':
            case 'search-select':
            case 'order-select':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.isNumberType = function(type) {
        switch (type) {
            case 'number':
            case 'integer':
            case 'float':
                return true;
                break;
        }
    }

    /////////////////////////////////////////////

    $scope.$watch('model', function(model) {
        if (!model.key || !model.key.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('model.title', function(newValue) {
            if (newValue) {
                $scope.model.key = _.camelCase(newValue); //.toLowerCase();
            }
        });

    }

    /////////////////////////////////////////////

    $scope.$watch('model.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.model.key = $scope.model.key.replace(regexp, '');
        }
    })

    /////////////////////////////////////////////

    // $scope.$watch('model.type + model.directive + model.params.restrictType', function() {
    //     if($scope.model.type == 'reference' && $scope.model.directive == 'reference-create') {

    //         var definition = TypeService.getTypeFromPath($scope.model.params.restrictType);

    //         if(definition) {
    //             $scope.model.embeddedDefinition = definition;
    //         }
    //     } else {
    //         delete $scope.model.embeddedDefinition;
    //     }
    // })

    //});


});