app.controller('BowerManagerController', function($scope, Fluro, $http) {


    $scope.bowerStatus = 'ready';


    $scope.install = function() {
        $scope.bowerStatus = 'progress';

        $http.post(Fluro.apiURL + '/site/bower/' + $scope.item._id, {}).then(function(res) {
            console.log('BOWER RES', res.data);
            $scope.bowerStatus = 'ready';
        }, function(res) {
            console.log('BOWER FAIL', res.data);
            $scope.bowerStatus = 'ready';
        });
    }
})