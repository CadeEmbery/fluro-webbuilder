/////////////////////////////////////////////////////////////////////

console.log('Webbuilder V5 init');

/////////////////////////////////////////////////////////////////////


function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName('meta');

    for (i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == stringKey) {
            return metas[i].getAttribute("content");
        }
    }
    return "";
}

/////////////////////////////////////////////////////////////////////

var dependencies = [
    'ngResource',
    'ngTouch',
    'ngAnimate',
    'ui.tree',
    'ui.router',
    'ui.bootstrap',
    'fluro.config',
    'fluro.customer',
    'fluro.asset',
    'fluro.types',
    'fluro.socket',
    'fluro.util',
    'fluro.content',
    'fluro.access',
    'fluro.auth',
    'fluro.video',
    'fluro.interactions',
    'fluro.notifications',
    'ngStorage',
    'ui.ace',
    'ui.sortable',
    'fluro.lazyload',
    'ui.tree',
    'ui.bootstrap.fontawesome',
    'angular-loading-bar',
    'formly',
    'formlyBootstrap',
    '720kb.socialshare',
    'ng-currency',
    'angularFileUpload',
];

/////////////////////////////////////////////////////////////////////

var $parentScope;

/////////////////////////////////////////////////////////////////////

//Get all the dependencies
if (window.opener && window.opener.angular) {
    //Get the parent dependencies
    var element = window.opener.angular.element('#builder');

    if (element) {
        $parentScope = element.scope();
        if ($parentScope && $parentScope.item && $parentScope.item.dependencies && $parentScope.item.dependencies.length) {
            dependencies = dependencies.concat($parentScope.item.dependencies);
        }
    }
}

/////////////////////////////////////////////////////////////////////

console.log('Launch Dependencies')
var app = angular.module('fluro', dependencies);
var head = angular.element('head');

/////////////////////////////////////////////////////////////////////

//Bootstrap application
function initializeApplication() {

    //////////////////////////////////
    var initInjector = angular.injector(["ng"]);
    var $http = initInjector.get("$http");

    //////////////////////////////////
    //Get the location of our API
    var apiURL = getMetaKey('fluro_url');
    var token = getMetaKey('fluro_application_key');

    //Adjust to V2
    //apiURL = apiURL.replace('http://api', 'http://v2.api');

    //////////////////////////////////

    var config = {}
    if (token) {
        config.headers = {
            'Authorization': 'Bearer ' + token
        }
    } else {
        config.withCredentials = true;
    }

    /////////////////////////////////////////////////////////////////////

    //Now attach the dependency scripts
    if ($parentScope && $parentScope.item) {

        /////////////////////////////////////////////

        if ($parentScope.item.external && $parentScope.item.external.length) {
            _.each($parentScope.item.external, function(url) {
                    //Attach the Javascript dependencies
                    head.append('<script src="' + url + '" type="text/javascript"></script>');
                })
                //console.log('Attached external dependencies')
        }

        /////////////////////////////////////////////

        if ($parentScope.item.bower && $parentScope.item.bower.length) {
            //Vendor Urls
            var jsUrl = apiURL + '/get/site/vendor/' + $parentScope.item._id + '/js'; // + '?access_token=' + token;
            var cssUrl = apiURL + '/get/site/vendor/' + $parentScope.item._id + '/css'; // + '?access_token=' + token;

            if (token) {
                jsUrl += '?access_token=' + token;
                cssUrl += '?access_token=' + token;
            }

            /////////////////////////////////////////////////////////////////////

            //Attach the Javascript dependencies
            head.append('<script src="' + jsUrl + '" type="text/javascript"></script>');

            //Attach the CSS Straight away
            head.append('<link href="' + cssUrl + '" type="text/css" rel="stylesheet"/>');

            /////////////////////////////////////////////////////////////////////

            //Load the JS and wait til its finished
            var req = $http.get(jsUrl, config);

            req.success(function(res) {
                //console.log('Loaded Script Dependencies');
                startApplication();
            });

            req.error(function(res) {
                console.log('Bower Dependencies Error', res);
            });
        } else {
            startApplication();
        }
    } else {
        //Just Start the app
        startApplication();
    }

    /////////////////////////////////////////////////////////////////////

    function startApplication() {

        //Start the application with initial dependencies
        // console.log('Start application')

        /////////////////////////////////////////////////////////////////////

        //Request
        var req = $http.get(apiURL + "/session", config);

        req.success(function(res) {
            if (res) {
                app.constant("$initUser", res);


                //Bootstrap the application
                angular.element(document).ready(function() {
                    angular.bootstrap(document, ["fluro"]);
                });

            } else {
                console.log('Bootstrap error', res)
            }
        });

        req.error(function(res) {
            console.log('Bootstrap error', res)
        });
    }
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

//Start the application
initializeApplication();

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.config(function(
    $controllerProvider,
    $compileProvider,
    $filterProvider,
    $provide,
    $stateProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider) {

    ///////////////////////////////////////////


    app.components = {}
    app.components.controller = $controllerProvider.register;
    app.components.directive = $compileProvider.directive;
    app.components.filter = $filterProvider.register;
    app.components.factory = $provide.factory;
    app.components.service = $provide.service;

    //Get the access token and the API URL
    var accessToken = getMetaKey('fluro_application_key');
    var apiURL = getMetaKey('fluro_url');


    //Create the initial config setup
    var initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: true, //Change this if you want to use local storage  or session storage
        backupToken: accessToken,
    }

    ///////////////////////////////////////////

    //Check if we are developing an app locally
    var appDevelopmentURL = getMetaKey('app_dev_url');

    //If an app development url is set then we know where to login etc
    if (appDevelopmentURL && appDevelopmentURL.length) {
        initialConfig.appDevelopmentURL = appDevelopmentURL;
    } else {
        //Set HTML5 mode to true when we deploy
        //otherwise live reloading will break in local dev environment
        $locationProvider.html5Mode(true);
    }

    ///////////////////////////////////////////

    //Set the initial config
    FluroProvider.set(initialConfig);

    ///////////////////////////////////////////

    //Http Intercpetor to check auth failures for xhr requests
    if (!accessToken) {
        $httpProvider.defaults.withCredentials = true;
    }

    $httpProvider.interceptors.push('FluroAuthentication');

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    $urlRouterProvider.otherwise("/");



    //Define our pages

    $stateProvider.state('home', {
        url: '/',
        views: {
            'builder': {
                templateUrl: 'routes/home/home.html',
                controller: 'HomeController',
                resolve: {
                    items: function(FluroContent) {

                        console.log('Retrieve sites');
                        return FluroContent.resource('site').query().$promise;
                    }
                }
            }
        }
    })


    //Define our pages

    $stateProvider.state('versions', {
        url: '/versions/:id',
        views: {
            'builder': {
                templateUrl: 'routes/versions/versions.html',
                controller: 'VersionListController',
                resolve: {
                    item: function($stateParams, FluroContent) {
                        return FluroContent.endpoint('log').query({
                            item: $stateParams.id,
                            limit: 500,
                        }).$promise;
                    }
                }
            }
        }
    })

    ///////////////////////////////////////////


    //Define our pages

    $stateProvider.state('templates', {
        url: '/templates',
        views: {
            'builder': {
                templateUrl: 'routes/templates/templates.html',
                controller: 'TemplatesController',
                resolve: {
                    items: function(FluroContent) {
                        return FluroContent.endpoint('templates/site').query().$promise;
                    }
                },
            }
        }
    });

    ///////////////////////////////////////////

    $stateProvider.state('build', {
        url: '/build/:id',
        abstract: true,
        views: {
            'builder': {
                templateUrl: 'routes/build/build.html',
                controller: 'BuildController',
                resolve: {
                    item: function(FluroContent, $stateParams) {
                        if ($stateParams.id != 'new') {
                            return FluroContent.resource('site').get({
                                id: $stateParams.id
                            }).$promise;
                        } else {
                            return {}
                        }
                    },
                    snippets: function(FluroContent, $stateParams) {
                        return FluroContent.endpoint('my/snippets').query().$promise;
                    }
                },
            }
        }
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.default', {
        url: '',
        templateUrl: 'routes/build/pages/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.info', {
        url: '/info',
        templateUrl: 'routes/build/info/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.styles', {
        url: '/styles',
        templateUrl: 'routes/build/styles/manager.html',
    })

    ///////////////////////////////////////////

    $stateProvider.state('build.dependencies', {
        url: '/dependencies',
        templateUrl: 'routes/build/dependencies/manager.html',
    })


    ///////////////////////////////////////////

    $stateProvider.state('build.components', {
        url: '/components',
        templateUrl: 'routes/build/components/manager.html',
        controller: 'ComponentManagerController',
    })



    ///////////////////////////////////////////

    $stateProvider.state('preview', {
        url: '/preview',
        views: {
            'builder': {
                templateUrl: 'routes/preview/preview.html',
                controller: 'PreviewController',
            }
        }
    })



    /*
   

    $stateProvider.state('build', {
        url: '/build/:id',
        templateUrl: 'routes/build.html',
        controller: 'BuildController',
        resolve: {
            item: function(FluroContent, $stateParams, Site) {
                if ($stateParams.id != 'new') {
                    return FluroContent.resource('site').get({
                        id: $stateParams.id
                    }).$promise;
                } else {
                    return Site.create()
                }
            },
            styles: function(FluroContent) {

                return FluroContent.resource('get/scripts/css').query().$promise;

            }
        }
    })




    $stateProvider.state('preview', {
        url: '/preview',
        templateUrl: 'views/preview.html',
        controller: 'PreviewController',
    })

    $stateProvider.state('outer', {
        url: '/outer/:id',
        templateUrl: 'views/outer.html',
        controller: 'OuterController',
        resolve: {
            item: function(FluroContent, $stateParams, Site) {
                if ($stateParams.id != 'new') {
                    return FluroContent.resource('site').get({
                        id: $stateParams.id
                    }).$promise;
                } else {
                    return Site.create()
                }
            },
        }
    })
    */




});

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

app.run(function($state, VideoTools, Fluro, FluroTokenService, FluroWindowService, $sessionStorage, $localStorage, $stateParams, $rootScope, FluroAccess, FluroMenuService, Notifications, FluroScrollService, DateTools, Asset, $initUser) {









    $rootScope.getTitle = function() {

        var title = 'WebBuilder';

        if ($rootScope.previewMode) {
            title = 'Preview';
        }

        if ($rootScope.site && $rootScope.site.title) {
            title = title + ' - ' + $rootScope.site.title;
        }

        return title;

    }

    //////////////////////////////////////////////

    var aceEditorOnload = function(editor) {
        //disables the annoying warning message
        editor.$blockScrolling = Infinity;
        // editor.getSession().setUseWorker(false);


        // Editor part
        var _session = editor.getSession();
        //var _renderer = _editor.renderer;

        //////////////////////////////////////////////////
        //////////////////////////////////////////////////

        /*
        editor.commands.addCommand({
            name: 'format',
            bindKey: {
                win: "Ctrl-Shift-F",
                mac: 'Command-Shift-F'
            },
            exec: function(editor) {

                var mode = editor.session.$modeId;

                mode = mode.substr(mode.lastIndexOf('/') + 1);
                var worker = new Worker('worker-jsbeautify.js');
                var options = {
                    mode: mode
                };

                worker.postMessage([editor.getValue(), options]);
                worker.addEventListener('message', function(e) {
                    console.log(e);
                    editor.setValue(e.data);
                }, false);
            }
        })
        /**/


        //////////////////////////////////////////////////

        //Set worker path
        // ace.config.set("workerPath", "/js/worker");

        ace.config.loadModule('ace/snippets/snippets', function() {

            //Snippet manager
            var snippetManager = ace.require('ace/snippets').snippetManager;


            switch (_session.$modeId) {
                case 'ace/mode/html':
                    ace.config.loadModule('ace/snippets/html', function(m) {
                        if (m && $rootScope.snippets.length) {

                            _.chain($rootScope.snippets)
                                .filter({
                                    syntax: 'html'
                                })
                                .each(function(snippet) {
                                    m.snippets.push({
                                        content: snippet.body,
                                        tabTrigger: String(snippet.account.title + ':' + snippet.title).toLowerCase(),
                                    });
                                })
                                .value();

                            snippetManager.register(m.snippets, m.scope);
                        }
                    });
                    break;
                case 'ace/mode/javascript':
                    ace.config.loadModule('ace/snippets/javascript', function(m) {
                        if (m && $rootScope.snippets.length) {

                            _.chain($rootScope.snippets)
                                .filter({
                                    syntax: 'js'
                                })
                                .each(function(snippet) {
                                    m.snippets.push({
                                        content: snippet.body,
                                        tabTrigger: String(snippet.account.title + ':' + snippet.title).toLowerCase(),
                                    });
                                })
                                .value();

                            snippetManager.register(m.snippets, m.scope);
                        }
                    });
                    break;
            }
        })



        /**
        //Load the snippets module
        ace.config.loadModule('ace/snippets/snippets', function() {

            //Snippet manager
            var snippetManager = ace.require('ace/snippets').snippetManager;



            ace.config.loadModule('ace/snippets/html', function(m) {
                if (m) {
                    m.snippets.push({
                        content: '${1:test}.this is custom snippet text!',
                        tabTrigger: 'CustomSnippet'
                    });
                    snippetManager.register(m.snippets, m.scope);
                }
            });

            ace.config.loadModule('ace/snippets/javacript', function(m) {
                if (m) {
                    m.snippets.push({
                        content: '${1:test}.this is custom snippet text!',
                        tabTrigger: 'CustomSnippet'
                    });
                    snippetManager.register(m.snippets, m.scope);
                }
            });
        });
        /**/



        /// var snippetManager = ace.require("ace/snippets").snippetManager;

        /**
        editor.commands.addCommand({
            name: 'format',
            bindKey: {win: "Ctrl-Shift-L", mac: 'Command-Shift-L'},
            exec: function (editor) {
                var mode = editor.session.$modeId;
                mode = mode.substr(mode.lastIndexOf('/') + 1);
                var worker = new Worker('worker-jsbeautify.js');
                var options = {
                    mode: mode
                };
                worker.postMessage([editor.getValue(), options]);
                worker.addEventListener('message', function (e) {
                    console.log(e);
                    editor.setValue(e.data);
                }, false);
            }
        })
        /**/
    };

    $rootScope.aceEditorOptions = {
        html: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'html',
            onLoad: aceEditorOnload

        },
        scss: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'scss',
            onLoad: aceEditorOnload
        },
        js: {
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            useWrapMode: false,
            showGutter: true,
            theme: 'tomorrow_night_eighties',
            mode: 'javascript',
            onLoad: aceEditorOnload
        }
    };


    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////



    /**
    function success(res) {

        console.log('SUCCESSFULLY AUTHENTICATED AS', res, Fluro);
        // $sessionStorage.session = res.data;
        // // Fluro.token = res.data.token;
        // // console.log('SIGN IN COMPLETE', res, Fluro, $sessionStorage);
        $rootScope.user = res.data;
        $rootScope.init = true;
    }

    function fail(res) {
        Notifications.error('Error creating token session');
    }

    //Store the session storage token
    Fluro.sessionStorage = true;

     console.log('CREATE STORAGE TOKEN', $initUser.account._id);

    FluroTokenService.getTokenForAccount($initUser.account._id).then(success, fail);

    /**/

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    //Add the user to the rootscope
    $rootScope.user = $initUser;
    $rootScope.init = true;

    //////////////////////////////////////////////////////////////////

    //Add Storage Options
    $rootScope.localStorage = $localStorage;
    $rootScope.sessionStorage = $sessionStorage;

    //////////////////////////////////////////////////////////////////

    //Add the window to the rootscope
    $rootScope.window = FluroWindowService;

    //////////////////////////////////////////////////////////////////

    //Enable Fluro Access on the rootscope
    $rootScope.access = FluroAccess;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.asset = Asset;

    //////////////////////////////////////////////////////

    $rootScope.getVideoImageUrl = VideoTools.getVideoThumbnail;
    $rootScope.getImageUrl = Asset.imageUrl;
    $rootScope.getThumbnailUrl = Asset.thumbnailUrl;
    $rootScope.getDownloadUrl = Asset.downloadUrl;
    $rootScope.getUrl = Asset.getUrl;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.menu = FluroMenuService;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.date = DateTools;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.notifications = Notifications;

    //////////////////////////////////////////////////////////////////

    //Asset tools
    $rootScope.scroll = FluroScrollService;

    //////////////////////////////////////////////////////////////////

    //Add state params
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    //////////////////////////////////////////////////////////////////
    /*
    $rootScope.getImageUrl = Asset.imageUrl;
    $rootScope.getThumbnailUrl = Asset.thumbnailUrl;
    $rootScope.getDownloadUrl = Asset.downloadUrl;
    $rootScope.getUrl = Asset.getUrl;
    */

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
        if (to.redirectTo) {
            evt.preventDefault();
            $state.go(to.redirectTo, params)
        }
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        console.log('State change error')
        throw error;
    });

    //////////////////////////////////////////////////////////////////

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, error) {
        $rootScope.firstLoad = true;
    });

    //////////////////////////////////////////////////////////////////

    //Make touch devices more responsive
    FastClick.attach(document.body);

});