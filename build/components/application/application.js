app.directive('application', function() {

    return {
        restrict: 'E',
        replace: true,
        // Replace the div with our template
        template:'<div class="outer-application-wrapper" ui-view="builder" ng-if="$root.init"/>',
        //templateUrl: 'application/application.html',
        controller: 'ApplicationController',
    };
});

////////////////////////////////////////////////////////////////////////

app.controller('ApplicationController', function($scope) {


	//$scope.message ='hey';

});
