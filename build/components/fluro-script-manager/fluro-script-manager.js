app.directive('scriptManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
           //selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-script-manager/fluro-script-manager.html',
        controller: 'ScriptManagerController',
    };
});


app.controller('ScriptManagerController', function($scope) {

    /////////////////////////////////////////////

    $scope.selected = {};


    if (!$scope.model) {
        $scope.model = [];
    } else {
        //Select first route
        $scope.selected.script = $scope.model[0];
    }

    //////////////////////////////////////////

    $scope.removeScript = function() {
        _.pull($scope.model, $scope.selected.script);
        $scope.selected = {};
    }

    // //////////////////////////////////////////

    // $scope.duplicate = function(template) {

    //     if (!template) {
    //         return;
    //     }

    //     var index = $scope.model.indexOf(template);

    //     var newTemplate = angular.copy(template);
    //     newTemplate.title = newTemplate.title + ' copy';

    //     if(index != -1) {
    //         $scope.model.splice(index+1, 0, newTemplate);
    //     } else {
    //         $scope.model.push(newTemplate);
    //     }

    //     $scope.selection.select(newTemplate);
    // }

    //////////////////////////////////////////

    $scope.addScript = function() {

        var newScript = {
            title: 'New Script',
            body:''
            //includeInMenu:true,
        };

        $scope.model.push(newScript);
        $scope.selected.script = newScript;
    }

})