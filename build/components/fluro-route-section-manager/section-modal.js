app.controller('SectionModalController', function($rootScope, $scope, ObjectSelection, templates) {


	$scope.selection = new ObjectSelection();
	$scope.selection.multiple = true;

	$scope.settings = {};


    $scope.templates = templates;

    ////////////////////////////////////


    $scope.toggleGroup = function(group) {
    	if($scope.settings.selectedGroup == group) {
    		$scope.settings.selectedGroup = null;
    	} else {
    		$scope.settings.selectedGroup = group;
    	}
    }
    ////////////////////////////////////
    ////////////////////////////////////

    //Now we have nested routes we need to flatten
    function getFlattenedRoutes(array) {
        return _.chain(array).map(function(route) {
                if (route.type == 'folder') {
                    return getFlattenedRoutes(route.routes);
                } else {
                    return route;
                }
            })
            .flatten()
            .compact()
            .value();
    }

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////

    $scope.$watch(function() {
        if ($rootScope.site && $rootScope.site.routes) {
            return $rootScope.site.routes;
        }
    }, function() {

        var routes = getFlattenedRoutes($rootScope.site.routes);

        if (routes) {
            $scope.sections = _.chain(routes).map(function(route) {
                    if (route.sections && route.sections.length) {
                        return {
                            pageName: route.title,
                            route:route,
                            sections: route.sections
                        };
                    }
                })
                .compact()
                .value();
        }
    });

    ////////////////////////////////////
    ////////////////////////////////////
    ////////////////////////////////////





});