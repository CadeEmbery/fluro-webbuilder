app.directive('routeSectionManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
        },
        // Replace the div with our template
        templateUrl: 'fluro-route-section-manager/fluro-route-section-manager.html',
        controller: 'RouteSectionManager',
    };
});




app.controller('RouteSectionManager', function($scope, $rootScope, $modal, ObjectSelection, FluroContent) {

    //Create the template array if doesnt exist
    if(!$rootScope.site.templates) {
        $rootScope.site.templates = [];
    }

    $scope.getTemplateSource = function(key) {
        return _.find($rootScope.site.templates, {key:key});
    }


   



    /*
     $scope.aceLoaded = function(_editor){

        console.log('SET OPTIONS', _editor);
        _editor.setOptions({
            useWrapMode : false, 
            showGutter: true, 
            theme:'tomorrow_night_eighties', 
            mode: 'html', 
            enableBasicAutocompletion: true, 
            enableSnippets: true,
            enableLiveAutocompletion: true
        })
    }
*/
    $scope.selectExisting = function() {

        var modalInstance = $modal.open({
            //animation: $scope.animationsEnabled,
            templateUrl: 'fluro-route-section-manager/section-modal.html',
            controller: 'SectionModalController',
            size: 'md',
            resolve:{
                templates:function(FluroContent, $q) {

                    var deferred = $q.defer();
                    FluroContent.endpoint('content').query({includePublic:true, searchInheritable:true, type:'siteblock'}).$promise.then(deferred.resolve, function(err) {
                        return deferred.resolve([]);
                    });

                    return deferred.promise;
                }
            }
        });

        modalInstance.result.then(function(sections) {

            if (sections && sections.length) {
                console.log('Modal Selected', sections)
                    //$scope.selected = selectedItem;

                _.each(sections, function(section) {

                    var copy = angular.copy(section);
                    if(section._id) {
                        copy.exportID = section._id;
                    }

                    $scope.model.push(copy);
                });

                $scope.selection.select($scope.model[$scope.model.length - 1]);
            }
        });
    }

    /////////////////////////////////////////////////////

    $scope.exportTemplate = function(section) {
        if(section.title && section.title.length && section.html && section.html.length) {
            //Create new template
            var newTemplate = {
                title: section.title,
                key:_.camelCase(section.title),
                html:section.html,
            };

            //Push the html into a new template
            $rootScope.site.templates.push(newTemplate);

            //Use the template for this section
            section.template = newTemplate.key;
        }
    }

    /////////////////////////////////////////////////////

    $scope.replicateTemplate = function(section) {

        //Check if it's got a template
        if(section.template) {

            //Get the actual template object
            var template = $scope.getTemplateSource(section.template);

            //If we have found one
            if(template) {
                //copy the html and append it to the section
                section.html = template.html;

                //Remove the link to the template
                section.template = null;
            }
        }
    }


    /////////////////////////////////////////////////////

    $scope.$watch('model', function(model) {
        if (!model) {
            $scope.model = [];
        } else {
            $scope.model = model;
        }

        var selection = new ObjectSelection();
        selection.multiple = false;
        $scope.selection = selection;

        ////////////////////////////////////

        
      



        /*
        
        ////////////////////////////////////
        $scope.addExistingSection = function(section) {
            var copy = angular.copy(section);
            $scope.model.push(copy);
            $scope.selection.select(copy);
            $scope.dynamic.showExisting = false;
        }

       $scope.dynamic = {}

        ////////////////////////////////////

        //Now we have nested routes we need to flatten
        function getFlattenedRoutes(array) {
            return _.chain(array).map(function(route) {
                    if (route.type == 'folder') {
                        return getFlattenedRoutes(route.routes);
                    } else {
                        return route;
                    }
                })
                .flatten()
                .compact()
                .value();
        }

        ////////////////////////////////////
        $scope.$watch(function() {
            if ($rootScope.site && $rootScope.site.routes) {
                return $rootScope.site.routes;
            }
        }, function() {

            var routes = getFlattenedRoutes($rootScope.site.routes);

            if (routes) {
                $scope.existingSections = _.chain(routes).map(function(route) {
                        if (route.sections && route.sections.length) {
                            return {
                                pageName: route.title,
                                sections: route.sections
                            };
                        }
                    })
                    .compact()
                    .value();
            }
        })
        */
       
        $scope.addSection = function() {

            var newSection = {};
            $scope.model.push(newSection);
            $scope.selection.select(newSection);
            //$scope.dynamic.showExisting = false;

        }

        $scope.removeSection = function(section) {
            _.pull($scope.model, section);
            $scope.selection.deselect(section);
        }


        $scope.exportSection = function(section) {
            
            section.exporting = true;

            var newData = _.clone(section);
            newData.realms = $rootScope.site.realms;


            console.log('NEW DATA', newData, $scope.model);

            FluroContent.resource('siteblock').save(newData).$promise.then(function(res) {
                console.log('Site block was saved', res);
                section.exporting = false;

                section.exportID = res._id;
            }, function(err) {
                console.log('Error exporting site block', err);
                section.exporting = false;
            });
        }

    })

})