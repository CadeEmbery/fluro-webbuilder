app.controller('ContentFormController', function($rootScope, Fluro, ModalService, $scope, FluroStorage, extras, FluroSocket, $rootScope, $state, $controller, Asset, Notifications, CacheManager, TypeService, FluroContent, type, definition, item) {

    $scope.item = item;
    $scope.extras = extras;

    if ($scope.item._id) {
        $scope.saveText = 'Save';
        $scope.cancelText = 'Close';
    } else {
        $scope.saveText = 'Create';
        $scope.cancelText = 'Cancel';
    }

    //Force defaults for saving
    $scope.defaultSaveOptions = {};


    $scope.type = type;
    $scope.definition = definition;

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/form.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    

    //////////////////////////////

    //Cancel path

    if (definition && definition.definitionName) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
        $scope.icon = definition.parentType;
    } else {
        $scope.definedType = type.path;
        $scope.icon = type.path;
    }

    ///////////////////////////////////

    $scope.titleLabel = 'Title';
    $scope.bodyLabel = 'Body';

    if (definition && definition.definitionName) {
        if (definition.data) {
            if (definition.data.titleLabel && definition.data.titleLabel.length) {
                $scope.titleLabel = definition.data.titleLabel;
            }

            if (definition.data.bodyLabel && definition.data.bodyLabel.length) {
                $scope.bodyLabel = definition.data.bodyLabel;
            }
        }
    }

    //////////////////////////////

    //Setup Local storage
    var session;

    if ($scope.item._id) {
        session = FluroStorage.sessionStorage('form-' + $scope.item._id)
    }


    //////////////////////////////

    $scope.openAPIDocument = function() {
        var url = Fluro.apiURL + '/content/' + $scope.definedType + '/' + $scope.item._id + '?context=edit';

        if(Fluro.token) {
            url += '&access_token=' + Fluro.token;
        }

        //Open in a new window
        window.open(url);
    }


    //////////////////////////////



    var socketEnabled = true;
    /**/


    FluroSocket.on('content.edit', socketUpdate);


    function socketUpdate(data) {

        if (socketEnabled) {

            //Same id content
            var sameId = (data.item._id == $scope.item._id);
            var newVersion = (data.item.updated != item.updated);


            console.log('Socket change', item.title, sameId, data.item.updated, item.updated);

            if (sameId && newVersion) {


                console.log('update this data ', data.item._id, $scope.item._id, data.item.title);

                //Notify the user
                //Notifications.warning('Changes to this ' + $scope.titleSingular + ' were made by ' + data.user.name);

                //Assign the new content
                socketEnabled = false;

                _.assign($scope.item, data.item);
                //$scope.item = data.item;
                socketEnabled = true;

                // console.log($rootScope.user._id, data.user._id)
                if ($rootScope.user._id != data.user._id) {
                    Notifications.warning('This content was just updated by ' + data.user.name);
                } else {
                    Notifications.status('You updated this in another window');
                }

            }
        }
    }

    /**/

    //////////////////////////////////////////

    /*
    $scope.$watch('item', function(newData, oldData) {

        if (socketEnabled) {
            //////////////////////////////////////////


            //newData.updated = new Date();

            //Broadcast the message
            var message = {
                event: 'content.live',
                item: newData,
                user: {
                    _id: $rootScope.user._id,
                    name: $rootScope.user.name,
                    account: {
                        _id: $rootScope.user.account._id
                    }
                }
            }


            //////////////////////////////////////////

            FluroSocket.emit('content', message, function(res) {
                console.log('FluroContent Emitted')
            })
        }

        //////////////////////////////////////////


    }, true)
*/

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    $scope.useInlineTitle = (type.path == 'article');

    //////////////////////////////////////////////////

    $scope.getAssetUrl = Asset.getUrl;
    $scope.getDownloadUrl = Asset.downloadUrl;


    //Cleanup Pasted HTML
    $scope.cleanupPastedHTML = $rootScope.cleanupPastedHTML;


    //////////////////////////////////////////////////

    $(document).on('keydown', keyDown);

    function keyDown(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    event.stopPropagation();
                    $scope.save();
                    break;
            }
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        $(document).off('keydown', keyDown);

        FluroSocket.off('content', socketUpdate);
    });


    //////////////////////////////////////////////////

    //////////////////////////////////////////////////

    //Save
    $scope.save = function(options) {

        //Disable Socket
        socketEnabled = false;

        console.log('Disable socket')

        if (!options) {
            options = $scope.defaultSaveOptions;
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        if (options.forceVersioning) {

            if (!$scope.item.version || $scope.item.version.length) {
                console.log('Forcing a version save');
                $scope.item.version = 'Changes made by ' + $rootScope.user.name;
            }
        }

        //////////////////////////////////////////

        console.log('Save item', $scope.item.applicationIcon, $scope.item.icon);

        //////////////////////////////////////////

        if ($scope.item._id) {


            console.log('Push item', $scope.item);
            
            //Edit existing content
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);


        } else {

            /*if (type.path == 'event') {
                FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccess, $scope.saveFail);
            } else {
                */
            FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessExit, $scope.saveFail);
            //}
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccess = function(result) {
        //Clear cache
        CacheManager.clear($scope.definedType);

        if (result._id) {
            //console.log('Updated to version', $scope.item.__v, result.__v);
            $scope.item.__v = result.__v;
        }

        if (result._type == 'definition') {
            TypeService.sideLoadDefinition(result);
        }


        if (result.name) {
            Notifications.status(result.name + ' saved successfully')
        } else {
            Notifications.status(result.title + ' saved successfully')
        }


        //Disable Socket
        socketEnabled = true;
        //console.log('Enable Socket')

        //////////////////////////////////////////////////

        //Able to close and keep moving
        if ($scope.closeOnSave) {
            console.log('Close on save')

            if ($scope.$close) {
                $scope.$close(result)
            }

            if (definition) {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                //Go to path
                $state.go(type.path)
            }

            return true;
        }


        //////////////////////////////////////////////////

        //Finish the save
        return $scope.saveFinished(result);
    }

    //////////////////////////////////////////////////

    $scope.saveFinished = function(result) {
        //console.log('Save Finished')
        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            return false;
        }
    }

    //////////////////////////////////////////////////

    $scope.saveSuccessExit = function(result) {
        //console.log('Save Success Exit')
        var res = $scope.saveSuccess(result);

        if (!res) {
            if (definition) {
                $state.go(type.path + '.custom', {
                    definition: definition.definitionName
                });
            } else {
                //Go to path
                $state.go(type.path)
            }
        }
    }



    //////////////////////////////////////////////////

    $scope.saveFail = function(result) {

        if (!result) {
            return Notifications.error('Failed to save')
        }


        if (!result.data) {
            return Notifications.error('Failed to save')
        }

        if (result.data.errors) {
            _.each(result.data.errors, function(err) {
                if (_.isObject(err)) {
                    Notifications.error(err.message)
                } else {
                    Notifications.error(err)
                }
            });
            return;
        }

        if (result.data) {
            return Notifications.error(result.data)
        }

        return Notifications.error('Failed to save document')


    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }


        if ($scope.item._id == $rootScope.user._id) {
            return $state.go('home')
        }

        //Go to the normal path
        if (definition && definition.definitionName) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            $state.go(type.path)
        }
    }





    var realmSelectEnabled = true;
    var tagSelectEnabled = true;

    switch ($scope.type.path) {
        case 'realm':
        case 'account':
            realmSelectEnabled = false;
            tagSelectEnabled = false;
            break;
        case 'role':
            tagSelectEnabled = false;
            break;
        case 'tag':
            tagSelectEnabled = false;
            break;
        case 'event':
            $controller('EventFormController', {
                $scope: $scope
            });
            break;
        case 'integration':
            $controller('IntegrationFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'application':
            $controller('ApplicationFormController', {
                $scope: $scope
            });
            break;
            /**/
        case 'user':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('UserFormController', {
                $scope: $scope
            });
            break;
        case 'persona':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('PersonaFormController', {
                $scope: $scope
            });
            break;
        case 'contactdetail':
            //realmSelectEnabled = false;
            tagSelectEnabled = false;

            $controller('ContactDetailFormController', {
                $scope: $scope
            });
            break;
        case 'family':


            $controller('FamilyFormController', {
                $scope: $scope
            });
            break;
        case 'query':
            $controller('QueryFormController', {
                $scope: $scope
            });
            break;
        case 'purchase':
            $controller('PurchaseFormController', {
                $scope: $scope
            });
            break;
        case 'checkin':
            $controller('CheckinFormController', {
                $scope: $scope
            });
            break;
        case 'asset':
        case 'video':
        case 'audio':
        case 'image':
            $controller('AssetFormController', {
                $scope: $scope
            });
            break;
    }

    //////////////////////////////////////////////////

    $scope.realmSelectEnabled = realmSelectEnabled;
    $scope.tagSelectEnabled = tagSelectEnabled;

    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////

    // $scope.getExportedJSON = function() {
    //     //Create the json object
    //     var json = angular.toJson($scope.item);

    //     //JSON.stringify(json)
    //     var data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json));

    //     return data;
    // }

    //////////////////////////////////////////////////

/**
    $scope.import = function(object) {
       ModalService.import
    }

    //////////////////////////////////////////////////

    $scope.importDataObject = function(object) {
        $scope.item = object;
    }

    //////////////////////////////////////////////////

    $scope.export = function() {

        //Create the json object
        var json = angular.toJson($scope.item);

        //JSON.stringify(json)
        var data = "text/json;charset=utf-8," + encodeURIComponent(json);


        //Download data
        window.open('data:' + data, '_blank');
        //angular.element('<a href="data:' + data + '" download="'+$scope.item._id+'.json"><i class="fa fa-share-square-o"></i></a>');
        //.appendTo('.status-summary');


    }
    /**/


})