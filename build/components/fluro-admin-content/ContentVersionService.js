app.service('ContentVersionService', function(FluroContent) {

	var controller = {};

	////////////////////////////////////

	controller.list = function(item) {

		var id = item._id;
		var type =  item._type;

		if(item.definition) {
			type = item.definition;
		}

		return FluroContent.endpoint('content/'+type+'/'+ id +'/versions').query();
	}

	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////

	return controller;
})