app.directive('json', function() {
    return {
        restrict: 'A', // only activate on element attribute
        require: 'ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModelCtrl) {

            var lastValid;

            // push() if faster than unshift(), and avail. in IE8 and earlier (unshift isn't)
            ngModelCtrl.$parsers.push(fromUser);
            ngModelCtrl.$formatters.push(toUser);

            // clear any invalid changes on blur
            element.bind('blur', function() {
                element.val(toUser(scope.$eval(attrs.ngModel)));
            });

            // $watch(attrs.ngModel) wouldn't work if this directive created a new scope;
            // see http://stackoverflow.com/questions/14693052/watch-ngmodel-from-inside-directive-using-isolate-scope how to do it then
            scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                lastValid = lastValid || newValue;

                if (newValue != oldValue) {
                    ngModelCtrl.$setViewValue(toUser(newValue));

                    // TODO avoid this causing the focus of the input to be lost..
                    ngModelCtrl.$render();
                }
            }, true); // MUST use objectEquality (true) here, for some reason..

            function fromUser(text) {
                // Beware: trim() is not available in old browsers
                if (!text || text.trim() === '') {
                    return {};
                } else {
                    try {
                        lastValid = angular.fromJson(text);
                        ngModelCtrl.$setValidity('invalidJson', true);
                    } catch (e) {
                        ngModelCtrl.$setValidity('invalidJson', false);
                    }
                    return lastValid;
                }
            }

            function toUser(object) {
                // better than JSON.stringify(), because it formats + filters $$hashKey etc.
                return angular.toJson(object, true);
            }
        }
    };
});




app.controller('QueryFormController', function($scope, Fluro) {
    if($scope.item._id) {
        $scope.previewLink = Fluro.apiURL + '/content/_query/' + $scope.item._id + '?noCache=true';
    }
    /*
    if (!$scope.item.data) {
        $scope.item.data = {}
    }

    $scope.getObjectAsText = function() {

        if ($scope.item.data) {
            $scope.textAreaModel = JSON.stringify($scope.item.data);
        } else {
            $scope.textAreaModel = '{}';
        }
    };


    $scope.$watch('textAreaModel', function(n) {

        if (n) {
            try {
                $scope.item.data = JSON.parse($scope.textAreaModel);
            } catch (err) {
                //Exception handler
                console.log('Exception', err)
            }
        }
    });

    /*
    var aceEditor;

    $scope.aceLoaded = function(_editor) {
        // Options
        //_editor.setReadOnly(true);
        aceEditor = _editor;
        aceEditor.setTheme("ace/theme/tomorrow_night_eighties");
        updateSyntax();
    };

    function updateSyntax() {
        if (aceEditor) {

            var syntaxName
            switch($scope.item.syntax) {
                case 'js':
                    syntaxName = 'javascript';
                break;
                default:
                syntaxName = $scope.item.syntax;
                break;
            }

            if(syntaxName) {
                aceEditor.getSession().setMode("ace/mode/" +syntaxName);
            }
        }
    }

    $scope.$watch('item.syntax', updateSyntax)

    $scope.aceChanged = function(e) {
        //
    };
    */

});