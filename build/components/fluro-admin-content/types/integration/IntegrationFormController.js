app.controller('IntegrationFormController', function($scope, $http, Fluro, Notifications) {



	$scope.authorizeInstagram = function() {


		if(!$scope.item._id || !$scope.item._id.length) {
			Notifications.warning('Please save before attempting to authorize this integration')
		}

		var clientID = $scope.item.publicDetails.clientID;

		var redirectURI = Fluro.apiURL + '/integrate/instagram/' + $scope.item._id;

		if(Fluro.token) {
			redirectURI+= '?access_token=' +Fluro.token;
		}



		if(!clientID || !clientID.length) {
			Notifications.warning('Invalid or missing client ID')
		}

		if(!redirectURI || !redirectURI.length) {
			Notifications.warning('Invalid or missing Redirect URI')
		}

		var url = 'https://api.instagram.com/oauth/authorize/?client_id='+clientID+'&redirect_uri='+redirectURI+ '&response_type=code'

		if(Fluro.token) {
			url+= '&access_token=' +Fluro.token;
		}

		console.log('authorize', url);
		window.open(url);


	}


});
