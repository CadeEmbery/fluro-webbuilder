app.controller('PersonaFormController', function($scope, Session, $state, Fluro, FluroContent, $http, $rootScope, CacheManager, $rootScope, FluroAccess, Notifications) {


    $scope.resendState = 'ready';


    $scope.sendResetEmail = function() {

        $scope.resendState = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;


        if ($scope.item.user) {
            email = $scope.item.user.email;
        }

        /////////////////////////////////////////////////

        // var request = $http.post(Fluro.apiURL + '/user/reinvite', {
        //     email: email
        // });

        var request = $http.post(Fluro.apiURL + '/user/reinvite/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.error(function(err) {
            console.log('ERROR', err);

            $scope.resendState = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending password instructions');
            }
        });

        /////////////////////////////////////////////////

        request.success(function(result) {
            console.log('Send', result);
            $scope.resendState = 'ready';
            Notifications.status('Instructions on how to reset password have been sent to ' + email);
        });




    }


    //////////////////////////////////////////////////
    //////////////////////////////////////////////////
    //////////////////////////////////////////////////




});