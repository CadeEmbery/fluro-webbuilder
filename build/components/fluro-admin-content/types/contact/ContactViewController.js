app.controller('ContactViewController', function($scope, FluroContent, FluroAccess, TypeService, ModalService) {

    FluroContent.endpoint('info/checkins').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.checkins = res;
    });


    FluroContent.endpoint('info/interactions').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {
        $scope.interactions = res;
    });


    FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
        .then(function(res) {
            $scope.details = res;
        });

    FluroContent.endpoint('info/assignments').query({
        contact: $scope.item._id
    }).$promise.then(function(res) {

        var today = new Date();

        $scope.pastassignments = _.filter(res, function(event) {
            return (new Date(event.startDate) < today);
        });

        $scope.upcomingassignments = _.filter(res, function(event) {
            return (new Date(event.startDate) >= today);
        });

    });


    $scope.viewDetail = function(sheet) {
        ModalService.view(sheet);
    }


})