


app.controller('ContactFormController', function($scope, FluroContent, FluroAccess, TypeService, ModalService) {


    if(!$scope.details) {
        $scope.details = [];
    }
    
    /////////////////////////////////////

    $scope.dateOptions = {
        formatYear: 'yy',
        //startingDay: 1,
        showWeeks:false,
    };

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

/*
    //Get all the defined types
    $scope.availableDetails = _.filter(TypeService.definedTypes, function(def) {
        
    });
*/

    /////////////////////////////////////

    /////////////////////////////////////

    $scope.edit = function(item) {
        ModalService.edit(item);
    }

    /////////////////////////////////////

    $scope.getCreateableDetails = function() {
        return _.filter(TypeService.definedTypes, function(def) {
            var correctType = def.parentType == 'contactdetail';
            var exists = _.some($scope.details, function(existing) {
                return existing.definition == def.definitionName;
            })

            return (correctType && !exists);
        });
    }

    /////////////////////////////////////

    $scope.createDetailSheet = function(sheet) {

        var options = {};
        options.template = {
            contact:$scope.item,
        }

        ModalService.create(sheet.definitionName, options, function(result) {
            $scope.details.push(result);
        })
        
    }

    /////////////////////////////////////

    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item);
    }

    /////////////////////////////////////


    if($scope.item._id) {
        FluroContent.endpoint('info/checkins').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {
            $scope.checkins = res;
        });


        FluroContent.endpoint('info/interactions').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {
            $scope.interactions = res;
        });


        FluroContent.endpoint('contact/details/' + $scope.item._id, true, true).query().$promise
        .then(function(res) {
            $scope.details = res;
        });

        FluroContent.endpoint('info/assignments').query({
            contact:$scope.item._id
        }).$promise.then(function(res) {

            var today = new Date();

            $scope.pastassignments = _.filter(res, function(event) {
                return (new Date(event.startDate) < today);
            });

            $scope.upcomingassignments = _.filter(res, function(event) {
                return (new Date(event.startDate) >= today);
            });

        });
    }
   
})