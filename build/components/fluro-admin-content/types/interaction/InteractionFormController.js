app.controller('InteractionFormController', function($scope) {





	//////////////////////////////////////

	$scope.amountPaid = function() {
		var totalPaid = 0;

		if($scope.item.transactions && $scope.item.transactions.length) {
			totalPaid += _.chain($scope.item.transactions)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		if($scope.item.manualPayments && $scope.item.manualPayments.length) {
			totalPaid += _.chain($scope.item.manualPayments)
			.map(function(transaction) {
				return transaction.amount;
			})
			.sum()
			.value();
		}

		return totalPaid;
	}

	//////////////////////////////////////

	$scope.amountDue = function() {

		var originalAmount = $scope.item.amount;
		var amountPaid = $scope.amountPaid();
		
		return originalAmount - amountPaid;
		
	}

});