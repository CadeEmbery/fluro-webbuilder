app.controller('DefinitionFormController', function($scope) {

    if (!$scope.item.definitionName) {
        $scope.$watch('item.title', function(newValue) {
            if (newValue) {
                $scope.item.definitionName = _.camelCase(newValue); //.toLowerCase();
            }
        })
    }

    $scope.$watch('item.definitionName', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.item.definitionName = $scope.item.definitionName.replace(regexp, '');
        }
    })

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    function getFlattenedFields(array, trail) {

        return _.chain(array).map(function(field, key) {
                if (field.type == 'group') {

                    if (field.asObject) {
                        //Push the field key
                        trail.push(field.key);
                    }

                    // console.log('Go down', field.key);
                    var fields = getFlattenedFields(field.fields, trail);

                    trail.pop();
                    //console.log('Go back up')
                    return fields;
                } else {

                    //Push the field key
                    trail.push(field.key);

                    field.trail = trail.slice();
                    trail.pop();

                    // console.log(field.title, trail);
                    return field;
                }
            })
            .flatten()
            .value();
    }

   

    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////

    $scope.$watch('item.fields', function(fields) {

         //Get all the flattened fields
        var flattened = getFlattenedFields($scope.item.fields, []);


        $scope.availableFields = _.map(flattened, function(field) {
            return {
                title:field.title,
                path:field.trail.join('.'),
                type:field.type,
            }
        });
    })

    /////////////////////////////////////////////////

    $scope.addAlternativePaymentMethod = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.paymentMethods) {
            $scope.item.paymentDetails.paymentMethods = [];
        }

        $scope.item.paymentDetails.paymentMethods.push({
            title: 'New Payment Method'
        })
    }


    $scope.removeAlternativePaymentMethod = function(method) {
        _.pull($scope.item.paymentDetails.paymentMethods, method);
    }


    /////////////////////////////////////////////////

    $scope.addPaymentModifier = function() {
        if (!$scope.item.paymentDetails) {
            $scope.item.paymentDetails = {}
        }


        if (!$scope.item.paymentDetails.modifiers) {
            $scope.item.paymentDetails.modifiers = [];
        }

        $scope.item.paymentDetails.modifiers.push({})
    }


    $scope.removeModifier = function(entry) {
        _.pull($scope.item.paymentDetails.modifiers, entry);
    }

})