app.controller('FamilyFormController', function($scope, ModalService) {

    console.log('FAMILY CONTROLLER')

    //$scope.openContact = ModalService.edit(item);


    $scope.addContact = function() {

        console.log('Add Contact', $scope.item);

        if ($scope.item && $scope.item._id) {
            var params = {}
            params.template = {
                family: $scope.item,
                lastName:$scope.item.title,
            }

            ModalService.create('contact', params, function(res) {
                $scope.item.items.push(res);
            })

        }

    }
});