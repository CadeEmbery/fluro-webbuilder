app.directive('adminPlanView', function(ModalService) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=ngModel',
            event: '=ngEvent',
            confirmations:'=ngConfirmations',
        },
        templateUrl: 'fluro-admin-content/types/plan/view.html',
        link:function($scope, $element, $attributes) {
            $scope.canEditPlan = true;

            $scope.editPlan = function(plan) {
                console.log('Plan', plan)
                ModalService.edit($scope.item);
            }
        }


        //controller: 'PlanViewController',
    };
});



app.controller('AdminPlanViewController', function($scope) {

    if($scope.item.event) {
        $scope.event = $scope.item.event;
    }

    $scope.item.showTeamOnSide = true;
    
    /////////////////////////////////////////////

    $scope.contexts = []


    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {


        return _.contains($scope.contexts, val);
    }


    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = (contact._id == confirmation.contact._id);
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })


            var laterTime = new Date(startDate);

            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }



    $scope.isBlankColumn = function(team) {


        var hasContent = _.chain($scope.item.schedules)
            .any(function(schedule) {
                if(!schedule) {
                    return false;
                }

                if (!schedule.notes) {
                    return false;
                }
                if (schedule.notes[team] && schedule.notes[team].length) {
                    return true;
                } else {
                    return false;
                }
            })
            .value();

        return !hasContent;
    }
});