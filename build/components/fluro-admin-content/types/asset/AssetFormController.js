app.controller('AssetFormController', function($scope, Asset, Fluro, $state, $stateParams, Notifications, FileUploader) {


    if($scope.type.path == 'image') {
        if(!$scope.item.width) {
            $scope.needsWidth = true;
        }

        if(!$scope.item.height) {
            $scope.needsHeight = true;
        }
    }

    /////////////////////////////////

    $scope.uploader = new FileUploader({
        url: Fluro.apiURL + '/file/replace',
        //removeAfterUpload: true,
        withCredentials: true,
        formData: {},
    });


    var forceRefresh = false;

    $scope.saveAndUpload = function() {
        console.log('Save and Upload');

        if (!$scope.item || !$scope.item.realms || !$scope.item.realms.length) {
            return Notifications.warning('Please select a realm before attempting to upload');
        }

        $scope.uploader.uploadAll();

    }

    
    if (!$scope.item.assetType && $scope.type.path != 'video' && $scope.type.path != 'audio') {
        $scope.item.assetType = 'upload';
    }
    

    /////////////////////////////////

    function refreshAfterUpload(response) {
        if (response._id == $scope.item._id) {
            $scope.item = response;
        }
    }

    $scope.getImageURL = function() {
        return Asset.getUrl($scope.item._id) + '?w=600&v=' + $scope.item.updated;// + '&forceRefresh=true';
        
    }

    //////////////////////////////////////////////////

    $scope.$watch('item._id + item.assetType', function() {
        if (!$scope.item._id && $scope.item.assetType == 'upload') {
            $scope.uploadSave = true;
        } else {
            $scope.uploadSave = false;
        }
    })

    /////////////////////////////////

    $scope.uploader.onBeforeUploadItem = function(item) {

        // item.removeAfterUpload = false;
        var details = {};

        if ($scope.item._id) {
            details._id = $scope.item._id;
        } else {

            //Add the definition before we send the data
            if ($scope.definition) {
                $scope.item.definition = $scope.definition.definitionName;
            }

            $scope.item.type = $scope.type.path;
            //console.log('Create Item', $scope.type, $scope.item.type);
            details.json = angular.toJson($scope.item);
        }

        console.log('Send details', details)
        item.formData = [details];
    };




    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {

        _.each(response.errors, function(err) {
            if (err.message) {
                Notifications.error(err.message);
            } else {
                Notifications.error(err);
            }
        })

        //Reset File Item and add it back into the queue
        fileItem.isError = false;
        fileItem.isUploaded = false;
        $scope.uploader.queue.push(fileItem);
    };

    /////////////////////////////////

    $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
        switch (status) {
            case 200:
                if ($scope.item._id) {
                    $scope.saveSuccess(response);
                    refreshAfterUpload(response);
                } else {
                    $scope.saveSuccessExit(response);
                }
                break;
            default:
                //Failed
                $scope.saveFail(response);
                break;
        }
    };


});