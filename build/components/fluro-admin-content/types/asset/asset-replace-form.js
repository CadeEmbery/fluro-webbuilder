app.directive('assetReplaceForm', function() {

    return {
        restrict: 'E',
        // Replace the div with our template
        templateUrl: 'fluro-admin-content/types/asset/asset-replace-form.html',
        controller: 'AssetReplaceController',
    };
});


app.controller('AssetReplaceController', function($scope, Fluro) {


    /////////////////////////////////

/*
    if (!$scope.item._id) {
        $scope.replaceDialogShowing = true;
    }
*/

    /////////////////////////////////

    $scope.showReplaceDialog = function() {
        $scope.replaceDialogShowing = true;
    }

    /////////////////////////////////

});