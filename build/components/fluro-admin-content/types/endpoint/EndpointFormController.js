app.controller('EndpointFormController', function($scope, PermissionService, GenericPermissionService) {
    $scope.permissions = PermissionService.permissions;
    $scope.genericPermissions = GenericPermissionService.getPermissions();
});
