app.controller('EventViewController', function($scope, ModalService) {



    $scope.viewEvent = function() {
        ModalService.view(item.event);
    }

    /////////////////////////////////////////////

    $scope.contexts = [];

    /////////////////////////////////////////////

   
    if($scope.extras) {
        $scope.confirmations = $scope.extras.confirmations;
        $scope.unavailableList = _.filter($scope.confirmations, function(con) {
            return con.status == 'denied';
        })

        $scope.confirmedList = _.filter($scope.confirmations, function(con) {
            return con.status == 'confirmed';
        })


    }
    /////////////////////////////////////////////

    $scope.toggleContext = function(val) {

        if (_.contains($scope.contexts, val)) {
            _.pull($scope.contexts, val);
        } else {
            $scope.contexts.push(val)
        }
    }

    $scope.contextIsActive = function(val) {

        
        return _.contains($scope.contexts, val);
    }

    /////////////////////////////////////////////

    $scope.isConfirmed = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'confirmed';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnavailable = function(contact, assignment) {
        return _.some($scope.confirmations, function(confirmation) {
            var correctAssignment = (assignment._id == confirmation.assignment || assignment.title == confirmation.title);
            var correctContact = contact._id == confirmation.contact._id;
            return correctAssignment && correctContact && confirmation.status == 'denied';
        })
    }

    /////////////////////////////////////////////

    $scope.isUnknown = function(contact, assignment) {
        return !$scope.isConfirmed(contact, assignment) && !$scope.isUnavailable(contact, assignment);
    }

    /////////////////////////////////////////////

    $scope.estimateTime = function(startDate, schedules, target) {

        if (target) {
            var total = 0;
            var foundAmount = 0;

            var index = schedules.indexOf(target);

            _.every(schedules, function(item, key) {
                //No item so return running total
                if (!item) {
                    foundAmount = total;
                } else {
                    //Increment the total
                    if (item.duration) {
                        total += parseInt(item.duration);
                        foundAmount = (total - item.duration);
                    } else {
                        foundAmount = total;
                    }
                }

                return (index != key)
            })

           
            var laterTime = new Date(startDate);



            var milliseconds = foundAmount * 60 * 1000;
            laterTime.setTime(laterTime.getTime() + milliseconds);

            return laterTime;
        } else {
            return;
        }
    }


})

