app.controller('EventFormController', function($scope, $stateParams, ModalService, DateTools) {

    /*
	//////////////////////////////////////////

    //Check if the user provided an init date upon creation
    //var initDate = $stateParams.initDate;
    if (!$scope.item._id) {
        if ($scope.definition && $scope.definition.data) {
            if ($scope.definition.data.defaultAssignments) {
                $scope.item.assignments = angular.copy($scope.definition.data.defaultAssignments);
            }
        }
    }

    //////////////////////////////////////////

    */
   
    
    //////////////////////////////////////////

   $scope.confirmations = $scope.extras.confirmations;

    //////////////////////////////////////////

   var currentDate1;
   var currentDate2;
   
    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }


    if (currentDate1 && currentDate2 && (currentDate1.getTime() != currentDate2.getTime())) {
        $scope.useEndDate = true;
    }


    //Make sure the end date is never earlier than the start date
    $scope.$watch('item.startDate + useEndDate', function(data) {

        //Compare the dates
        var date1 = new Date($scope.item.startDate);
        var date2 = new Date($scope.item.endDate);

        //If the user does not want to use an end date set it the same as the start date
        if (!$scope.useEndDate) {
            $scope.item.endDate = $scope.item.startDate;
        }

        //Update the end time if its earlier than the start time
        if (date2.getTime() < date1.getTime()) {
            $scope.item.endDate = date1
        }

    });

    //////////////////////////////////////////
    //////////////////////////////////////////

    $scope.$watch('item.startDate +  item.endDate + item.checkinData.checkinStartOffset + item.checkinData.checkinEndOffset', function() {

        var startDate = $scope.item.startDate;
        var endDate = $scope.item.endDate;

        if(!$scope.item.checkinData) {
            $scope.item.checkinData = {
                checkinStartOffset:0,
                checkinEndOffset:0,
            }
        }

        $scope.checkinStartDate = moment(startDate).subtract($scope.item.checkinData.checkinStartOffset, 'minutes');
        $scope.checkinEndDate = moment(endDate).add($scope.item.checkinData.checkinEndOffset, 'minutes');
    })
    //////////////////////////////////////////


    /*

    //////////////////////////////////////////

    var today = new Date();

    var currentDate1 = new Date(today.getTime());
    var currentDate2 = new Date(today.getTime());

    if ($scope.item.startDate) {
        //Use end date if the dates are different when we open an event
        currentDate1 = new Date($scope.item.startDate);
        currentDate2 = new Date($scope.item.endDate);
    }

    if (currentDate1.getTime() != currentDate2.getTime()) {
        $scope.useEndDate = true;
    }


	//////////////////////////////////////////



    var initDate;

    //If there are modal params
    if ($stateParams.initDate) {
        initDate = $stateParams.initDate;
    }

    if (initDate) {
       
        if (_.isString(initDate)) {
            initDate = new Date(parseInt(initDate)); //Number(initDate);
        }

        var initStart = new Date(initDate);
        var initEnd = new Date(initDate);
        initStart.setMinutes(initStart.getMinutes() + 30);
        initStart.setMinutes(0);

        initEnd.setMinutes(initEnd.getMinutes() + 30);
        initEnd.setMinutes(0);

        $scope.item.startDate = initStart;
        $scope.item.endDate = initEnd;
    }

    //////////////////////////////////////////

    //Include the readable date range function
    $scope.readableDateRange = DateTools.readableDateRange;

/**/


    $scope.createPlanFromTemplate = function() {

        var selection = {};
        var modal = ModalService.browse('plan', selection);
        modal.result.then(addPlan,
            addPlan)

        function addPlan() {
           // console.log('Select Plans', $scope.item);
            if (selection.items && selection.items.length) {

                //Create an array if there isnt one already
                if (!$scope.item.plans || !$scope.item.plans.length) {
                    $scope.item.plans = [];
                }

                //Get the template
                var template = selection.items[0];

               // console.log('Add Plan from template', template)


                  
                ModalService.edit(template, function(copyResult) {
                    if (copyResult) {
                        $scope.item.plans.push(copyResult)
                    }

                    //console.log('After', $scope.item)

                }, function(res) {
                    //console.log('Failed to copy', res)
                }, true, true, $scope);

            }
        }

    }

    /*
    //////////////////////////////////////////////////


    $scope.saveFinished = function(result) {

        if (!$scope.item._id) {
            if (result._id) {

                _.assign($scope.item, result);

                //$scope.item._id = result._id;

                $scope.saveText = 'Save';
                $scope.cancelText = 'Close';
            }

            $scope.closeOnSave = true;
            return true;
        } else {

            console.log('Close like normal')
            if ($scope.$close) {
                return $scope.$close(result)
            } else {
                return false;
            }
        }
    }

    /*
    //Save
    $scope.save = function(options) {

        //TODO Disable Socket     

        if (!options) {
            options = {}
        }
        //////////////////////////////////////////

        //Use save options

        if (options.status) {
            status = options.status;
        }

        if (options.exitOnSave) {
            saveSuccessCallback = exitAfterSave;
        }

        //////////////////////////////////////////

        if ($scope.item._id) {
            //Edit existing user
            FluroContent.resource($scope.definedType).update({
                id: $scope.item._id,
            }, $scope.item, $scope.saveSuccess, $scope.saveFail);
        } else {

            console.log('SAVE SUCCESS STAY');
            //Saving a new event so stay and dont disappear
            FluroContent.resource($scope.definedType).save($scope.item, $scope.saveSuccessStay, $scope.saveFail);
        }


    }


    //////////////////////////////////////////////////

    $scope.saveSuccessStay = function(result) {
        console.log('Stay please')
        //Clear cache
        CacheManager.clear($scope.definedType);

        if (result._id) {
            $scope.item.__v = result.__v;
            $scope.item._id = result._id;
        }


        //TODO Reenable Socket
        Notifications.status(result.title + ' saved successfully')
        
        return false;

       
        if ($scope.$close) {
            return $scope.$close(result)
        } else {
            console.log('Return false modal')
            return false;
        }
        
    }

    */

    //////////////////////////////////////////


})