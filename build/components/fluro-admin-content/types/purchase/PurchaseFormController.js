app.controller('PurchaseFormController', function($scope, $http, Fluro, Notifications) {


    //Save the original email
    $scope.originalEmail = String($scope.item.collectionEmail);



	$scope.$watch('item.product', function(product) {
		$scope.item.title = product.title;
	})


     $scope.originalEmail = String($scope.item.collectionEmail);


     

	//////////////////////////////////////////////

	$scope.resendStatus = 'ready';


	$scope.resendPurchaseInvitation = function() {


		if(!$scope.item || !$scope.item._id) {
			return;
		}

		if(!$scope.item.collectionEmail || !$scope.item.collectionEmail.length) {
			return;
		}



        $scope.resendStatus = 'processing';

        //Start off with the collection email
        var email = $scope.item.collectionEmail;

        /////////////////////////////////////////////////

        var request = $http.post(Fluro.apiURL + '/purchase/resend/' + $scope.item._id);

        /////////////////////////////////////////////////

        request.error(function(err) {
            $scope.resendStatus = 'ready';

            if (err.error) {
                Notifications.error(err.error);
            } else {
                Notifications.error('There was an error sending purchase notification');
            }
        });

        /////////////////////////////////////////////////

        request.success(function(result) {
            $scope.resendStatus = 'ready';
            Notifications.status('Purchase notification sent ' + email);
        });




    }




});