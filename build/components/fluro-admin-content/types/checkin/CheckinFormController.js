app.controller('CheckinFormController', function($scope, $http, Fluro, Notifications) {

	$scope.$watch('item.contact + item.title + item.firstName + item.lastName', function() {
		
		//Watch to see if the contact exists
		if($scope.item.contact) {
			//if(!$scope.item.title || !$scope.item.title.length) {
				$scope.item.title = $scope.item.contact.title;
			//}
		} else {

			var title = '';
			if($scope.item.firstName && $scope.item.firstName.length) {
				title += $scope.item.firstName;
			}

			if($scope.item.lastName && $scope.item.lastName.length) {
				title += ' ' + $scope.item.lastName;
			}

			$scope.item.title = title;
		}

	})
});
