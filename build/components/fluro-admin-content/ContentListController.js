//////////////////////////////////////////////////////////////////////////

app.controller('ContentListController', function($scope, $filter, extras, Notifications, CacheManager, FluroSocket, $rootScope, $controller, $state, $filter, $compile, Fluro, FluroAccess, ModalService, Selection, FluroStorage, Tools, items, definition, definitions, type) {

    //////////////////////////////

    //Add the items to the scope
    $scope.items = items;
    $scope.definitions = definitions;
    $scope.type = type;
    $scope.definition = definition;

    $scope.extras = extras;


    $scope.templates = _.filter(items, function(item) {
        return item.status == 'template';
    })

    
    //var socketEnabled = true;

    FluroSocket.on('content', socketUpdate);


    function socketUpdate(data) {
        if (data.item._type == type.path) {

            if (data.item.definition) {
                CacheManager.clear(data.item.definition);
            } else {
                CacheManager.clear(type.path);
            }
            
            Notifications.warning('Some of this content was just updated by ' + data.user.name);

            //console.log('Refresh list because changes were made')
            //$state.reload();
            //refreshList();
        }
    }
    //////////////////////////////////////////////////

    $scope.$on("$destroy", function() {
        FluroSocket.off('content', socketUpdate);
    });

    //////////////////////////////


    //////////////////////////////

    $scope.updatedTooltip = function(item) {
        if(item.updatedBy) {
            return 'By ' + item.updatedBy + ' ' + $filter('timeago')(item.updated);
        } else {
            return $filter('timeago')(item.updated);
        }

        //return $sce.trustAsHtml('<span ng-if="item.updatedBy">By ' +item.updatedBy+ '</span> {{item.updated | timeago}}');
    }

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.filtersActive = function() {
        return _.keys($scope.search.filters).length;
    }

    switch (type.path) {
        case 'event':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        case 'account':
            $scope.template = 'fluro-admin-content/types/' + type.path + '/list.html';
            break;
        default:
            $scope.template = 'fluro-admin-content/types/list.html';
            break;
    }
    //////////////////////////////

    //Setup Local storage
    var local;
    var session;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;

        local = FluroStorage.localStorage(definition.definitionName)
        session = FluroStorage.sessionStorage(definition.definitionName)
    } else {
        local = FluroStorage.localStorage(type.path)
        session = FluroStorage.sessionStorage(type.path)

        $scope.definedType = type.path;
    }

    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {
                    status: ['active'],
                },
                reverse: false,
        }
    } else {
        $scope.search = session.search;
    }

    //////////////////////////////

    //////////////////////////////

    //Setup Search    
    if (!local.settings) {
        $scope.settings =
            local.settings = {}
    } else {
        $scope.settings = local.settings;
    }

    //////////////////////////////

    switch ($scope.type.path) {
        case 'asset':
        case 'image':
        case 'video':
        case 'audio':
            $scope.uploadType = true;

            $scope.uploadName = $scope.type.path;
            if(definition) {
                $scope.uploadName = definition.definitionName;
            }
            break;

    }
    //////////////////////////////

    $scope.currentViewMode = function(viewMode) {
        if (viewMode == 'default') {
            return (!$scope.settings.viewMode || $scope.settings.viewMode == '' || $scope.settings.viewMode == viewMode);
        } else {
            return $scope.settings.viewMode == viewMode;
        }
    }

    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
        $scope.updateFilters();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        $scope.updateFilters();
    }




    //////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize:8,
        }
    } else {
        $scope.pager = session.pager;
    }

    //////////////////////////////

    $scope.$watch('search', function() {
        $scope.updateFilters();
    }, true);

    ///////////////////////////////////////////////

    $scope.canSelect = function(item) {
        return true;
    }

    ///////////////////////////////////////////////

    $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect);

    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }

    

    ///////////////////////////////////////////////

    $scope.deselectFiltered = function() {
        $scope.selection.deselectMultiple($scope.filteredItems);
    }

    ///////////////////////////////////////////////

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    ///////////////////////////////////////////////

    $scope.viewInModal = function(item) {
        ModalService.view(item);
    }

    ///////////////////////////////////////////////

    $scope.editInModal = function(item) {
        ModalService.edit(item, function(result) {
            $state.reload();
        }, function(result) {
        });
    }

    ///////////////////////////////////////////////

    $scope.createInModal = function() {
        ModalService.create($scope.type, function(result) {
            $state.reload();
        }, function(result) {
            //console.log('Failed to create in modal', result)
        });
    }

    ///////////////////////////////////////////////

    $scope.createFromTemplate = function(template) {

        ModalService.edit(template, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, false, true, $scope);


    }

    ///////////////////////////////////////////////

    $scope.copyItem = function(item) {

        ModalService.edit(item, function(result) {
            //console.log('Copied in modal', result)
            $state.reload();
        }, function(result) {
            //console.log('Failed to copy in modal', result)
        }, true);


    }



    ///////////////////////////////////////////////

    $scope.canCreate = function() {
        var c = FluroAccess.can('create', type.path);

        if (definition) {
            c = FluroAccess.can('create', definition.definitionName);
        }

        return c;
    }


    ///////////////////////////////////////////////

    $scope.canCopy = function(item) {
        var typeName = $scope.type.path;
        if (item.definition) {
            typeName = item.definition;
        }

        return FluroAccess.can('create', typeName);
    }

    ///////////////////////////////////////////////

    
    $scope.canEdit = function(item) {
        return FluroAccess.canEditItem(item, (type.path == 'user'));
    }

    /*function(item) {

        if (!item) {
            return false;
        }

        var canEditAny = FluroAccess.can('edit any', type.path);
        var canEditOwn = FluroAccess.can('edit own', type.path);

        if (definition) {
            canEditAny = FluroAccess.can('edit any', definition.definitionName);
            canEditOwn = FluroAccess.can('edit own', definition.definitionName);
        }

        if (canEditAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canEditOwn;
            }
        }
    }
    */




    ///////////////////////////////////////////////

     $scope.canDelete = function(item) {
        return FluroAccess.canDeleteItem(item, (type.path == 'user'));
    }
    /*function(item) {

        if (!item) {
            return false;
        }

        var canDeleteAny = FluroAccess.can('delete any', type.path);
        var canDeleteOwn = FluroAccess.can('delete own', type.path);

        if (definition) {
            canDeleteAny = FluroAccess.can('delete any', definition.definitionName);
            canDeleteOwn = FluroAccess.can('delete own', definition.definitionName);
        }

        if (canDeleteAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canDeleteOwn;
            }
        }
    }
    */



    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);

            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;


            //Update the current page
            $scope.updatePage();
        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {

        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        //Now break it up into pages
        var pageItems = $filter('startFrom')($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }




    ////////////////////////////////////////////

    switch (type.path) {
        case 'event':
            $controller('EventListController', {
                $scope: $scope
            });
            break;
        case 'user':
            $controller('UserListController', {
                $scope: $scope
            });
            break;
        case 'persona':
            $controller('PersonaListController', {
                $scope: $scope
            });
            break;
        case 'account':
            $controller('AccountListController', {
                $scope: $scope
            });
            break;
    }


    $scope.updateFilters();

    ///////////////////////////////////////////////

    /*
	///////////////////////////////////////

	$scope.$watch('search.terms', function(keywords) {
		return $http.get(Fluro.apiURL + '/search', {
            ignoreLoadingBar: true,
            params: {
                keys: keywords,
                //type: $scope.restrictType,
                limit: 100,
            }
        }).then(function(response) {
           $scope.items = response.data;
        });
	})
*/

    ///////////////////////////////////////


});