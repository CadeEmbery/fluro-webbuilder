app.controller('ContentDeleteController', function($scope, type, definition, $state, FluroContent, Notifications, CacheManager, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;


    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;


    $scope.cancelPath = type.path;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.cancelPath = type.path + ".custom({definition:'" + definition.definitionName + "'})";
    }


    ///////////////////////////////////////////////////

    $scope.delete = function() {

        var definedName = type.path;
        if(definition && definition.definitionName) {
            definedName = definition.definitionName;
        }

		
		if($scope.item._id) {
			//Update existing article
			FluroContent.resource(definedName).delete({
                id: $scope.item._id,
            }, deleteSuccess, deleteFailed);
		}
	}

	//////////////////////////////////////////////////

	function deleteSuccess(data) {
        var title = data.title;
        if(data.name) {
            title = data.name;
        }

        Notifications.status(title + ' was deleted successfully')
		
        if(data.definition && data.definition != data._type) {
            CacheManager.clear(data.definition);
            $state.go(type.path + '.custom', {definition:data.definition});
        } else {
            CacheManager.clear(type.path);
            $state.go(type.path);
        }
	}

	function deleteFailed(data) {

        var title = $scope.item.title;
        if($scope.item.name) {
            title = $scope.item.name;
        }

		 Notifications.error('Failed to delete ' + title)
	}



})