/////////////////////////////////////////////////////////////////

//WE NEED TO UPDATE THIS TO BE NICER (admin-content-select that is)
app.run(function(formlyConfig, $templateCache) {

    formlyConfig.setType({
        name: 'reference-select',
        templateUrl: 'fluro-admin-content/content-formly/reference-select.html',
        controller: function($scope) {


            //Get definition
            var definition = $scope.to.definition;

            if (!definition.params) {
                definition.params = {}
            }

            //Create a config object
            $scope.config = {
                canCreate: true,
                minimum: definition.minimum,
                maximum: definition.maximum,
                type: definition.params.restrictType,
                searchInheritable: definition.params.searchInheritable
            };
        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'date-select',
        templateUrl: 'fluro-admin-content/content-formly/date-select.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'object-editor',
        templateUrl: 'fluro-admin-content/content-formly/object-editor.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    formlyConfig.setType({
        name: 'custom',
        templateUrl: 'fluro-admin-content/content-formly/unknown.html',
        wrapper: ['bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'terms',
        templateUrl: 'fluro-admin-content/content-formly/terms.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfig.setType({
        name: 'code',
        templateUrl: 'fluro-admin-content/content-formly/code.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
        controller: function($scope) {

            var aceEditor;

            $scope.aceLoaded = function(_editor) {
                // Options
                //_editor.setReadOnly(true);
                aceEditor = _editor;

                var syntaxName;

                if ($scope.to.definition.params && $scope.to.definition.params.syntax) {
                    syntaxName = syntaxName = $scope.to.definition.params.syntax;
                }

                if (aceEditor && syntaxName) {
                    aceEditor.getSession().setMode("ace/mode/" + syntaxName);
                }
            }
        }
    });

    formlyConfig.setType({
        name: 'wysiwyg',
        templateUrl: 'fluro-admin-content/content-formly/wysiwyg.html',
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });


    /**
// set templates here
    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-interaction-form/nested.html',
        controller: function($scope) {

            $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    console.log('Create it!')
                    $scope.model[$scope.options.key] = [];
                }

            })

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;

            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                console.log('Only 1!!!', $scope.options)
            } else {
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({});
                    });
                }
            }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        //defaultValue:[],
        //noFormControl: true,
        //template: '<formly-form model="model[options.key]" fields="options.data.fields"></formly-form>'
    });

    /**/

    // set templates here
    formlyConfig.setType({
        name: 'nested',
        templateUrl: 'fluro-admin-content/content-formly/nested.html',
        // defaultOptions: {
        //    noFormControl: true,
        // },
        controller: function($scope) {
            $scope.$watch('model[options.key]', function(keyModel) {
                if (!keyModel) {
                    $scope.model[$scope.options.key] = [];
                }
            })

            $scope.settings = {};

            ////////////////////////////////////

            //Definition
            var def = $scope.to.definition;

            var minimum = def.minimum;
            var maximum = def.maximum;
            var askCount = def.askCount;

            if (!minimum) {
                minimum = 0;
            }

            if (!maximum) {
                maximum = 0;
            }

            if (!askCount) {
                askCount = 0;
            }

            //////////////////////////////////////

            if (minimum && askCount < minimum) {
                askCount = minimum;
            }

            if (maximum && askCount > maximum) {
                askCount = maximum;
            }

            //////////////////////////////////////

            if (maximum == 1 && minimum == 1 && $scope.options.key) {
                //console.log('Only 1 Please!!!!', $scope.options)
            } else {

                //console.log('Fill er up')
                if (askCount && !$scope.model[$scope.options.key].length) {
                    _.times(askCount, function() {
                        $scope.model[$scope.options.key].push({});
                    });
                }
            }

            //////////////////////////////////////
            //////////////////////////////////////

            $scope.firstLine = function(item) {
                var keys = _.chain(item)
                    .keys()
                    .without('active')
                    .filter(function(key) {
                        var i = key.indexOf('$$');
                        if (i == -1) {
                            return true;
                        } else {
                            return false;
                        }
                    })
                    .slice(0, 3)
                    .value();

                return _.at(item, keys).join(',').slice(0, 150);
            }

            $scope.items = function() {
                return $scope.model[$scope.options.key];
            }




            //[{title:'one'},{title:'two'},{title:'three'}]

            ////////////////////////////////////

            //var minimum = $scope.to.definition.minimum;
            //var maximum = $scope.to.definition.maximum;

            // if (minimum && !$scope.model[$scope.options.key].length) {
            //     _.times(minimum, function() {
            //         $scope.model[$scope.options.key].push({});
            //     });
            // }

            ////////////////////////////////////

            $scope.canRemove = function() {
                if (minimum) {
                    if ($scope.model[$scope.options.key].length > minimum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            $scope.remove = function(entry) {
                //model[options.key].splice($index, 1)
                console.log('Pull Entry', entry, $scope.model[$scope.options.key].indexOf(entry));
                _.pull($scope.model[$scope.options.key], entry);
            }

            ////////////////////////////////////

            $scope.canAdd = function() {
                if (maximum) {
                    if ($scope.model[$scope.options.key].length < maximum) {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            ////////////////////////////////////

            if (maximum != 1 && $scope.model && $scope.model[$scope.options.key] && $scope.model[$scope.options.key].length) {
                $scope.settings.active = $scope.model[$scope.options.key][0];
            }
        }
    });




    formlyConfig.setType({
        name: 'list-select',
        templateUrl: 'fluro-admin-content/content-formly/list-select.html',
        controller: function($scope, FluroValidate) {



            //Get the definition
            var definition = $scope.to.definition;

            //Minimum and maximum
            var minimum = definition.minimum;
            var maximum = definition.maximum;

            /////////////////////////////////////////////////////////////////////////


            $scope.multiple = (maximum != 1);

            /////////////////////////////////////////////////////////////////////////

            var to = $scope.to;
            var opts = $scope.options;

            $scope.selection = {
                values: [],
                value: null,
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.contains = function(value) {
                if ($scope.multiple) {
                    //Check if the values are selected
                    return _.contains($scope.selection.values, value);
                } else {
                    return $scope.selection.value == value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.select = function(value) {
                //console.log('SELECTION', $scope.selection);
                if ($scope.multiple) {
                    $scope.selection.values.push(value);
                } else {
                    $scope.selection.value = value;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.deselect = function(value) {
                if ($scope.multiple) {
                    _.pull($scope.selection.values, value);
                } else {
                    $scope.selection.value = null;
                }
            }

            /////////////////////////////////////////////////////////////////////////

            $scope.toggle = function(reference) {
                if ($scope.contains(reference)) {
                    $scope.deselect(reference);
                } else {
                    $scope.select(reference);
                }

                //Update model
                setModel();
            }


            /////////////////////////////////////////////////////////////////////////

            // initialize the checkboxes check property
            $scope.$watch('model', function(newModelValue) {
                var modelValue;

                //If there is properties in the FORM model
                if (_.keys(newModelValue).length) {

                    //Get the model for this particular field
                    modelValue = newModelValue[opts.key];



                    //$scope.$watch('to.options', function(newOptionsValues) {
                    if ($scope.multiple) {
                        if (_.isArray(modelValue)) {
                            $scope.selection.values = angular.copy(modelValue);
                        } else {
                            if (modelValue) {
                                $scope.selection.values = [angular.copy(modelValue)];
                            }
                        }
                    } else {
                        if(modelValue) {
                           $scope.selection.value = angular.copy(modelValue);
                        }
                    }
                }
            }, true);


            /////////////////////////////////////////////////////////////////////////

            function checkValidity() {

                var validRequired;
                var validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);

                //Check if multiple
                if ($scope.multiple) {
                    if ($scope.to.required) {
                        validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0;
                    }
                } else {
                    if ($scope.to.required) {
                        if ($scope.model[opts.key]) {
                            validRequired = true;
                        }
                    }
                }

                $scope.fc.$setValidity('required', validRequired);
                $scope.fc.$setValidity('validInput', validInput);
            }

            /////////////////////////////////////////////////////////////////////////

            function setModel() {
                if ($scope.multiple) {
                    $scope.model[opts.key] = angular.copy($scope.selection.values);
                } else {
                    $scope.model[opts.key] = angular.copy($scope.selection.value);
                }

                $scope.fc.$setTouched();
                checkValidity();
            }

            /////////////////////////////////////////////////////////////////////////

            if (opts.expressionProperties && opts.expressionProperties['templateOptions.required']) {
                $scope.$watch(function() {
                    return $scope.to.required;
                }, function(newValue) {
                    checkValidity();
                });
            }

            /////////////////////////////////////////////////////////////////////////

            if ($scope.to.required) {
                var unwatchFormControl = $scope.$watch('fc', function(newValue) {
                    if (!newValue) {
                        return;
                    }
                    checkValidity();
                    unwatchFormControl();
                });
            }

        },
        wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });
});


////////////////////////////////////////////////////////////////////////


app.directive('contentFormly', function(FluroContent, FluroContentRetrieval, FluroValidate) {
    return {
        restrict: 'E',
        //replace: true,
        transclude: true,
        scope: {
            item: '=ngModel',
            definition: '=',
            agreements:'=?'
        },
        templateUrl: 'fluro-admin-content/content-formly/form.html',
        //controller: 'InteractionFormController',
        //templateUrl: 'fluro-interaction-form/fluro-web-form.html',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {


            if (!$scope.item) {
                $scope.item = {}
            }


            /////////////////////////////////////////////////////////////////

            $scope.$watch('item', function(item) {


                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = item; //$scope.item;

                // The model object that we reference
                // on the  element in index.html
                $scope.vm.model = $scope.item;


                // An array of our form fields with configuration
                // and options set. We make reference to this in
                // the 'fields' attribute on the  element
                $scope.vm.fields = [];

                /////////////////////////////////////////////////////////////////

                //Keep track of the state of the form
                $scope.vm.state = 'ready';

                /////////////////////////////////////////////////////////////////

                //$scope.vm.onSubmit = submitInteraction;

                /////////////////////////////////////////////////////////////////

                function addFieldDefinition(array, fieldDefinition) {

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Create a new field
                    var newField = {};
                    newField.key = fieldDefinition.key;


                    /////////////////////////////

                    //Add the class name if applicable
                    if (fieldDefinition.className && !fieldDefinition.asObject) {
                        newField.className = fieldDefinition.className;
                    }

                    /////////////////////////////

                    //Template Options
                    var templateOptions = {};
                    templateOptions.type = 'text';
                    templateOptions.label = fieldDefinition.title;
                    templateOptions.description = fieldDefinition.description;

                    //Include the definition itself
                    templateOptions.definition = fieldDefinition;

                    /////////////////////////////

                    //Add a placeholder
                    if (fieldDefinition.placeholder && fieldDefinition.placeholder.length) {
                        templateOptions.placeholder = fieldDefinition.placeholder;
                    } else if (fieldDefinition.description && fieldDefinition.description.length) {
                        templateOptions.placeholder = fieldDefinition.description;
                    } else {
                        templateOptions.placeholder = fieldDefinition.title;
                    }

                    /////////////////////////////

                    //Require if minimum is greater than 1 and not a field group
                    templateOptions.required = (fieldDefinition.minimum > 0);

                    /////////////////////////////

                    //Directive or widget
                    switch (fieldDefinition.directive) {
                        case 'value-select':
                        case 'button-select':
                        case 'order-select':
                            newField.type = 'list-select';
                            break;
                        default:
                            newField.type = fieldDefinition.directive;
                            break;
                    }


                    /////////////////////////////

                    //Directive or widget
                    switch (fieldDefinition.type) {
                        case 'reference':
                            //Detour here if we are using a select
                            if (fieldDefinition.directive == 'select' && fieldDefinition.maximum == 1) {

                                //Map Reference objects to strings
                                if ($scope.vm.model[fieldDefinition.key] && $scope.vm.model[fieldDefinition.key]._id) {
                                    $scope.vm.model[fieldDefinition.key] = $scope.vm.model[fieldDefinition.key]._id;
                                }


                            } else {
                                newField.type = 'reference-select';
                            }





                            break;
                        case 'boolean':
                            //Detour here

                            // console.log('GOT PARAMS SORTED', fieldDefinition)
                            if(fieldDefinition.params && fieldDefinition.params.storeData) {
                                newField.type = 'terms';
                                newField.data = {
                                    agreements:$scope.agreements,
                                }
                            } else {
                                 newField.type = 'checkbox';
                            }
                           
                            break;
                        case 'object':
                            //Detour here
                            newField.type = 'object-editor';
                            break;
                        case 'date':
                            //Detour here
                            newField.type = 'date-select';
                            break;
                    }





                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Allowed Options

                    if (fieldDefinition.type == 'reference') {




                        //If we have allowed references specified
                        if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) {
                            templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id,
                                }
                            });
                        } else {

                            //We want to load all the options from the server
                            templateOptions.options = [];

                            if (fieldDefinition.sourceQuery) {

                                //We use the query to find all the references we can find
                                var queryId = fieldDefinition.sourceQuery;
                                if (queryId._id) {
                                    queryId = queryId._id;
                                }


                                /////////////////////////

                                var options = {};

                                //If we need to template the query
                                if (fieldDefinition.queryTemplate) {
                                    options.template = fieldDefinition.queryTemplate;
                                    if (options.template._id) {
                                        options.template = options.template._id;
                                    }
                                }

                                /////////////////////////

                                //Now retrieve the query
                                var promise = FluroContentRetrieval.getQuery(queryId, options);


                                //var promise = FluroContentRetrieval.query(null, null, queryId, true);

                                //Now get the results from the query
                                promise.then(function(res) {

                                    templateOptions.options = _.map(res, function(ref) {
                                        return {
                                            name: ref.title,
                                            value: ref._id,
                                        }
                                    })
                                });


                            } else {

                                if (!fieldDefinition.params) {
                                    fieldDefinition.params = {};
                                }
                                //We want to load all the possible references we can select
                                FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                                    templateOptions.options = _.map(res, function(ref) {
                                        return {
                                            name: ref.title,
                                            value: ref._id,
                                        }
                                    })
                                });
                            }
                        }
                    } else {
                        //Just list the options specified

                        if (fieldDefinition.options && fieldDefinition.options.length) {
                            templateOptions.options = fieldDefinition.options;
                        } else {
                            templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                                return {
                                    name: val,
                                    value: val
                                }
                            });
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //What kind of data type, override for things like checkbox
                    if (fieldDefinition.directive == 'input') {
                        switch (fieldDefinition.type) {
                            case 'boolean':
                                newField.type = 'checkbox';
                                break;
                                /*
                        case 'number':
                        case 'integer':
                        case 'float':
                            templateOptions.type = 'number';
                        break;
                        */
                        }
                    }

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    //Default Options

                    if (fieldDefinition.maximum == 1) {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]; //._id;
                                } else {
                                    newField.defaultValue = fieldDefinition.defaultReferences[0]._id;
                                }
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues[0];
                            }
                        }
                    } else {
                        if (fieldDefinition.type == 'reference') {
                            if (fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length) {

                                if (newField.type == 'reference-select') {
                                    newField.defaultValue = fieldDefinition.defaultReferences;
                                } else {
                                    newField.defaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                                        return ref._id;
                                    });
                                }
                            } else {
                                newField.defaultValue = [];
                            }
                        } else {
                            if (fieldDefinition.defaultValues && fieldDefinition.defaultValues.length) {
                                newField.defaultValue = fieldDefinition.defaultValues;
                            }
                        }
                    }




                    /////////////////////////////

                    //Append the template options
                    newField.templateOptions = templateOptions;

                    /////////////////////////////
                    /////////////////////////////
                    /////////////////////////////

                    newField.validators = {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            var valid = FluroValidate.validate(value, fieldDefinition);
                            //   console.log(scope.name, 'VALID?', value, valid, scope);
                            return valid;
                        }
                    }

                    /////////////////////////////

                    /////////////////////////////

                    //If there are sub items and this is just a group
                    if (fieldDefinition.directive != 'embedded' && fieldDefinition.fields && fieldDefinition.fields.length) {

                        var fieldContainer;

                        if (fieldDefinition.asObject) {
                            newField.type = 'nested';

                            //Check if it's an array or an object
                            newField.defaultValue = [];

                            if (fieldDefinition.key && fieldDefinition.maximum == 1 && fieldDefinition.minimum == 1) {
                                newField.defaultValue = {};
                            }

                            newField.data = {
                                fields: [],
                               // className: fieldDefinition.className,
                            };

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                            newField.extras = {
                                skipNgModelAttrsManipulator: true,
                            }

                            //Link to the definition of this nested object
                            fieldContainer = newField.data.fields;

                        } else {
                            //Start again
                            newField = {
                                fieldGroup: [],
                                className: fieldDefinition.className,
                            }

                            //Link to the sub fields
                            fieldContainer = newField.fieldGroup;
                        }

                        //Loop through each sub field inside a group
                        _.each(fieldDefinition.fields, function(sub) {
                            addFieldDefinition(fieldContainer, sub);
                        });
                    }

                    /////////////////////////////

                    if (fieldDefinition.hideExpression) {
                        newField.hideExpression = fieldDefinition.hideExpression;
                    }

                    /////////////////////////////

                    if (newField.key != '_contact' && newField.type != 'value') {
                        //Push our new field into the array
                        array.push(newField);
                    }
                }

                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////

                if ($scope.definition) {
                    //Loop through each defined field and add it to our form
                    _.each($scope.definition.fields, function(fieldDefinition) {
                        addFieldDefinition($scope.vm.fields, fieldDefinition);
                    });
                }


            });



        },
    };
});