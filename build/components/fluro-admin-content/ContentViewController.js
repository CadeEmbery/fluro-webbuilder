app.controller('ContentViewController', function($scope, extras, $rootScope, $controller, FluroSocket, Notifications, ModalService, $state, FluroAccess, type, definition, item) {

    $scope.item = item;
    $scope.type = type;
    $scope.definition = definition;
    $scope.extras = extras;

    //Template
    $scope.template = 'fluro-admin-content/types/' + type.path + '/view.html';

    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    //Cancel path

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;
        $scope.definedType = definition.definitionName;
    } else {
        $scope.definedType = type.path;
    }


    $scope.canEdit = FluroAccess.canEditItem;


    //////////////////////////////



    var socketEnabled = true;
    /**/
    FluroSocket.on('content', function(data) {

        if (socketEnabled) {
            if (data.item._id == item._id) {
                Notifications.warning('Changes to this ' + $scope.titleSingular + ' were just made by ' + data.user.name);
                //$state.reload();
                if (angular.equals(data.item, item)) {
                    console.log('No change');
                } else {

                    socketEnabled = false;
                    //_.assign($scope.item, data.item);
                    $scope.item = data.item;
                    socketEnabled = true;

                    Notifications.warning('This content was just updated by ' + data.user.name);
                }
            }
        }
    });
    /**/



    //ui-sref="{{type.path}}.edit({id:item._id})"

    //////////////////////////////////////////////////

    $scope.editItem = function() {

        if ($scope.$dismiss) {
            ModalService.edit(item);
            $scope.$dismiss();
            /*
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

       
           
            */
        } else {
            $state.go($scope.item._type + '.edit', {
                id: $scope.item._id,
                definitionName: $scope.definedType,
            });

        }

    }

    //////////////////////////////////////////////////

    $scope.cancel = function() {

        if ($scope.$dismiss) {
            return $scope.$dismiss();
        }

        if ($scope.item._id == $rootScope.user._id) {
            return $state.go('home')
        }

        //Go to the normal path
        if (definition && definition.definitionName) {
            $state.go(type.path + '.custom', {
                definition: definition.definitionName
            });
        } else {
            $state.go(type.path)
        }
    }


    switch ($scope.item._type) {
        case 'contact':
            $controller('ContactViewController', {
                $scope: $scope
            });
            break;
        
    }





})