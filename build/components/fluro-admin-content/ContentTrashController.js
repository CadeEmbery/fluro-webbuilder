app.controller('ContentTrashController', function($scope, $rootScope, $filter, $state, FluroContent, Notifications, CacheManager, Batch, FluroStorage, FluroAccess, Selection, items, definition, definitions, type) {

    //////////////////////////////

    //Add the items to the scope
    $scope.items = items;
    $scope.definitions = definitions;
    $scope.type = type;
    $scope.definition = definition;





    //////////////////////////////

    $scope.titleSingular = type.singular;
    $scope.titlePlural = type.plural;

    //////////////////////////////

    //Setup Local storage
    var local;
    var session;

    if (definition) {
        $scope.titleSingular = definition.title;
        $scope.titlePlural = definition.plural;

        local = FluroStorage.localStorage(definition.definitionName + '-trash')
        session = FluroStorage.sessionStorage(definition.definitionName + '-trash')
    } else {
        local = FluroStorage.localStorage(type.path + '-trash')
        session = FluroStorage.sessionStorage(type.path + '-trash')
    }


    $scope.template = 'fluro-admin-content/types/trash.html';


    //////////////////////////////

    //Setup Search    
    if (!session.search) {
        $scope.search =
            session.search = {
                filters: {},
                reverse: false,
        }
    } else {
        $scope.search = session.search;

        if ($scope.search.filters.status) {
            delete $scope.search.filters.status;
        }
    }








    //////////////////////////////

    //Setup Search    
    if (!local.settings) {
        $scope.settings =
            local.settings = {}
    } else {
        $scope.settings = local.settings;
    }



    console.log($scope, 'SCOPE');
    //////////////////////////////

    $scope.setOrder = function(string) {
        $scope.search.order = string;
        $scope.updateFilters();
    }

    $scope.setReverse = function(bol) {
        $scope.search.reverse = bol;
        $scope.updateFilters();
    }


    //////////////////////////////

    //Setup Selection    
    if (!session.selection) {
        session.selection = []
    }

    //////////////////////////////

    //Setup Pager    
    if (!session.pager) {
        $scope.pager =
            session.pager = {
                limit: 20,
                maxSize:8,
        }
    } else {
        $scope.pager = session.pager;
    }

    //////////////////////////////

    $scope.$watch('search', function() {
        $scope.updateFilters();
    }, true);


    ///////////////////////////////////////////////

    $scope.canSelect = function(item) {
        return true;
    }

    ///////////////////////////////////////////////

    $scope.selection = new Selection(session.selection, $scope.items, $scope.canSelect);

    ///////////////////////////////////////////////

    $scope.selectPage = function() {
        $scope.selection.selectMultiple($scope.pageItems);
    }

    $scope.togglePage = function() {
        if ($scope.pageIsSelected()) {
            $scope.deselectPage();
        } else {
            $scope.selectPage();
        }
    }

    $scope.deselectPage = function() {
        $scope.selection.deselectMultiple($scope.pageItems);
    }

    $scope.pageIsSelected = function() {
        return $scope.selection.containsAll($scope.pageItems);
    }


    ///////////////////////////////////////////////

    $scope.canDestroy = function(item) {

        if (!item) {
            return false;
        }

        var canDestroyAny = FluroAccess.can('destroy any', type.path);
        var canDestroyOwn = FluroAccess.can('destroy own', type.path);

        if (definition) {
            canDestroyAny = FluroAccess.can('destroy any', definition.definitionName);
            canDestroyOwn = FluroAccess.can('destroy own', definition.definitionName);
        }

        if (canDestroyAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canDestroyOwn;
            }
        }
    }

    ///////////////////////////////////////////////

    $scope.canRestore = function(item) {

        if (!item) {
            return false;
        }

        var canRestoreAny = FluroAccess.can('restore any', type.path);
        var canRestoreOwn = FluroAccess.can('restore own', type.path);

        if (definition) {
            canRestoreAny = FluroAccess.can('restore any', definition.definitionName);
            canRestoreOwn = FluroAccess.can('restore own', definition.definitionName);
        }

        if (canRestoreAny) {
            return true;
        }

        if (item.author) {
            var authorID;

            if (_.isObject(item.author)) {
                authorID = item.author._id;
            } else {
                authorID = item.author;
            }

            if (authorID == $rootScope.user._id) {
                return canRestoreOwn;
            }
        }
    }

    ///////////////////////////////////////////////

    //Filter the items by all of our facets
    $scope.updateFilters = function() {

        if ($scope.items) {
            //Start with all the results
            var filteredItems = $scope.items;

            //Filter search terms
            filteredItems = $filter('filter')(filteredItems, $scope.search.terms);

            //Order the items
            filteredItems = $filter('orderBy')(filteredItems, $scope.search.order, $scope.search.reverse);


            if ($scope.search.filters) {
                _.forOwn($scope.search.filters, function(value, key) {

                    if (_.isArray(value)) {
                        _.forOwn(value, function(value) {
                            filteredItems = $filter('reference')(filteredItems, key, value);
                        })
                    } else {
                        filteredItems = $filter('reference')(filteredItems, key, value);
                    }
                });
            }

            ////////////////////////////////////////////

            //Update the items with our search
            $scope.filteredItems = filteredItems;





            //Update the current page
            $scope.updatePage();

        }
    }


    ///////////////////////////////////////////////

    //Update the current page
    $scope.updatePage = function() {



        if ($scope.filteredItems.length < ($scope.pager.limit * ($scope.pager.currentPage - 1))) {
            $scope.pager.currentPage = 1;
        }

        //Now break it up into pages
        var pageItems = $filter('startFrom')($scope.filteredItems, ($scope.pager.currentPage - 1) * $scope.pager.limit);
        pageItems = $filter('limitTo')(pageItems, $scope.pager.limit);

        $scope.pageItems = pageItems;
    }



    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


    $scope.done = function(data) {

        ///////////////////////

        //Find all the caches we need to clear
        var caches = _.chain(data.results)
            .map(function(result) {
                return [result.definition, result._type];
            })
            .flatten()
            .value();

        ///////////////////////

        //Make sure we refresh the current view
        if ($scope.definition) {
            console.log('GET THE DEFINITION');
            caches.push($scope.definition.definitionName);
            caches.push($scope.definition.definitionName + '-trash');
        }

        caches.push($scope.type.path);
        caches.push($scope.type.path + '-trash');

        ///////////////////////

        console.log('Caches to clear', caches);

        caches = _.uniq(caches);
        caches = _.compact(caches);

        _.each(caches, function(t) {
            console.log('Clear cache', t);
            CacheManager.clear(t);
        });

        ///////////////////////

        //Reload the state
        $state.reload();

        ///////////////////////

        //Deselect all
        $scope.selection.deselectAll();
        //Notifications.status('Updated  ' + data.results.length + ' ' + $scope.type.plural);
    }

    ///////////////////////////////////////////////

    /**/
    $scope.restore = function(item) {
        //Edit existing user
        FluroContent.endpoint('content/' + $scope.type.path + '/'+item._id+'/restore').get().$promise.then(function(res) {

            Notifications.status('Restored ' + res.title);
            //Refresh
            $scope.done(res);
        });
    }
   /**

    ////////////////////////////////////////////

    $scope.restore = function(item) {
        console.log('Restore selected item')

        var id = item;
        if(_.isObject(item)) {
            id = item._id;
        }

        Batch.restore({
            ids: [id],
        }, function(err, res) {

            if (err) {
                console.log('DONE ERR', err)
                return Notifications.error(err);
            }

            Notifications.status('Restored ' + res.results.length + ' ' + $scope.type.plural);
            

            $scope.done(res);
        });
    }
    /**/



    ////////////////////////////////////////////

    $scope.destroy = function(item) {
        FluroContent.resource($scope.type.path).delete({
            id: item._id,
            destroy: true,
        }, function(res) {

            Notifications.status('Destroyed ' + res.title);
            //Refresh
            $scope.done(res);
        });
    }

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    $scope.updateFilters();









});