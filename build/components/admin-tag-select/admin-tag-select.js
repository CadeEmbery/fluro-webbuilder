app.directive('tagSelect', function() {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
            type: '=ngType',
        },
        templateUrl: 'admin-tag-select/admin-tag-select.html',
        controller: 'TagSelectController',
    };
});

app.controller('TagSelectController', function($scope, $rootScope, CacheManager, Fluro, $http, FluroContent) {


    if (!$scope.model) {
        $scope.model = [];
    }
    
    $scope.dynamicPopover = {
         templateUrl: 'admin-tag-select/admin-tag-popover.html',
    };


    $scope.proposed = {};

    /////////////////////////////////////

    $scope.add = function(item) {
        $scope.model.push(item);
        $scope.proposed = {};
    }


    $scope.removeTag = function(tag) {
       _.pull($scope.model, tag);
    }

    
    $scope.getTags = function(val) {

        var url = Fluro.apiURL + '/content/tag/search/' + val;
        return $http.get(url, {
            ignoreLoadingBar: true,
            params: {
                limit: 20,
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.model, {
                    '_id': item._id
                });
                if (!exists) {
                    filtered.push(item);
                }
                return filtered;
            }, [])
        });

    };


    $scope.create = function() {

       
        var details = {
            title: $scope.proposed.value,
        }

        //Immediately create a tag   
        FluroContent.resource('tag').save(details, function(tag) {

            //$scope.tags.push(tag);
            $scope.model.push(tag);

            //We need to clear the tag cache now
            CacheManager.clear('tag');

            //Clear
            $scope.proposed = {
            }
        }, function(data) {
            console.log('Failed to create tag', data);
        });

    }
});