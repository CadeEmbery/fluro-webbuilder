app.directive('compileTemplate', function($compile, $rootScope, $templateCache) {
    return {
        restrict: 'A',
        // scope: {
        //     templateName: '@templateName',
        // },
        link: function($scope, $element, $attr) {

            var templateName;

            //Watch to see if the template name changes
            $attr.$observe('compileTemplate', function(newTemplateName) {

                //Clear out the element
                $element.empty();

                templateName = newTemplateName;
            });

            //////////////////////////////////

            $scope.$watch(function() {

                if (templateName) {
                    return $templateCache.get(templateName);
                } else {
                    return;
                }

            }, function(html) {

                //Clear out the element
                $element.empty();

                //If theres a template
                if (html && html.length) {
                    //Compile it and append it
                    var template = $compile(html)($scope);
                    $element.append(template);
                }

            }, true);
        },
    };
});