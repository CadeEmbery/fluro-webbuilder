app.directive('templateManager', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            model: '=ngModel',
           //selection: '=ngSelection',
        },
        // Replace the div with our template
        templateUrl: 'fluro-template-manager/fluro-template-manager.html',
        controller: 'TemplateManagerController',
    };
});


app.controller('TemplateEditorController', function($scope) {


    /////////////////////////////////////////////

    $scope.$watch('selection.item', function(item) {
        if (!item.key || !item.key.length) {
            startWatchingTitle();
        } else {
            stopWatchingTitle();
        }
    })

    /////////////////////////////////////////////

    var watchTitle;

    function stopWatchingTitle() {
        if (watchTitle) {
            watchTitle();
        }
    }

    function startWatchingTitle() {

        if (watchTitle) {
            watchTitle();
        }

        watchTitle = $scope.$watch('selection.item.title', function(newValue) {
            if (newValue) {
                $scope.selection.item.key = _.camelCase(newValue); //.toLowerCase();
            }
        });
    }

    /////////////////////////////////////////////

    $scope.$watch('selection.item.key', function(newValue) {
        if (newValue) {
            var regexp = /[^a-zA-Z0-9-_]+/g;
            $scope.selection.item.key = $scope.selection.item.key.replace(regexp, '');
        }
    });

})

app.controller('TemplateManagerController', function($scope, ObjectSelection) {


    //Setup one route selected at a time
    var selection = new ObjectSelection();
    selection.multiple = false;
    $scope.selection = selection;



    if (!$scope.model) {
        $scope.model = [];
    } else {
        //Select first route
        $scope.selection.select($scope.model[0]);
    }

    //////////////////////////////////////////

    $scope.removeTemplate = function() {
        _.pull($scope.model, $scope.selection.item);
        $scope.selection.deselect();
    }

    //////////////////////////////////////////

    $scope.duplicate = function(template) {

        if (!template) {
            return;
        }

        var index = $scope.model.indexOf(template);

        var newTemplate = angular.copy(template);
        newTemplate.title = newTemplate.title + ' copy';

        if(index != -1) {
            $scope.model.splice(index+1, 0, newTemplate);
        } else {
            $scope.model.push(newTemplate);
        }

        $scope.selection.select(newTemplate);
    }

    //////////////////////////////////////////

    $scope.addTemplate = function() {

        var newTemplate = {
            title: '',
            //includeInMenu:true,
        };

        $scope.model.push(newTemplate);
        $scope.selection.select(newTemplate);
    }

})