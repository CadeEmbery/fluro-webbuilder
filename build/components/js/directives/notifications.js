
app.directive('notifications', function() {
    return {
        restrict: 'E',
        replace: true,
        controller: 'NotificationsController',
        template: '<div class="notifications"><div ng-if="lastMessage" class="notification {{lastMessage.type}}"><i class="fa fa-notification-{{lastMessage.type}}"></i><span>{{lastMessage.text}}</span></div></div>',
    };
})


/////////////////////////////////////////////////////

app.controller('NotificationsController', function($scope, Notifications) {
    //$scope.messages = Notifications.list;

    $scope.$watch(function() {
    	return Notifications.list.length;
    }, function() {
    	$scope.lastMessage = _.last(Notifications.list);
    })
})