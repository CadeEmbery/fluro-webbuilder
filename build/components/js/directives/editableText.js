app.directive('editable', function($compile, $timeout) {
    return {
        restrict: 'A',
        scope: {
            model: '=ngModel',
        },
        link: function(scope, element, attrs) {

            //Active
            scope.active = false;

            //Normal
            var normal = angular.element('<span class="editable-text" ng-hide="active" ng-dblclick="setActive($event)">{{model}}</span>');
            var editor = angular.element('<input class="editable-input" ng-show="active" ng-model="model" ng-blur="active = false"/>');

            $compile(normal)(scope);
            $compile(editor)(scope);

            //Add the elements
            element.append(normal);
            element.append(editor);


            scope.setActive = function(event) {
                scope.active = true;
                $timeout(function() {
                    editor.select();
                }, 10);
            }

            //Listen for keypress
            element.bind("keydown keypress", function(event) {
                console.log('Enter pressed')
                if (event.which === 13) {
                    event.preventDefault();
                    //Set active
                    $timeout(function() {
                        scope.active = false;
                    });
                }

            })



        }
    }
});


// app.directive('selectOnFocus', function($timeout) {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attrs) {
//             var focusedElement = null;

//             element.on('focus', function() {
//                 var self = this;
//                 if (focusedElement != self) {
//                     focusedElement = self;
//                     $timeout(function() {
//                         self.select();
//                     }, 10);
//                 }
//             });

//             element.on('blur', function() {
//                 focusedElement = null;
//             });
//         }
//     }
// });