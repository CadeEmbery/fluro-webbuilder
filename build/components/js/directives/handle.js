app.directive('handle', function() {

    return {
        restrict: 'E',
        replace:true,
        template:'<div class="btn-handle"><i class="fa fa-arrows"></i></div>'
    };
});
