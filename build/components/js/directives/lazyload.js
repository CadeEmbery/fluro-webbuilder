angular.module('fluro.lazyload', []).directive('script', function() {
    return {
        restrict: 'E',
        scope: false,
        link: function(scope, elem, attr) {
            if (attr.type === 'text/lazy') {
                var code = elem.text();
                var f = new Function(code);
                f();
            }
        }
    };
});
