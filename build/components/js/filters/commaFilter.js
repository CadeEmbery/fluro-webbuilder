app.filter('comma', function() {
    return function(array, key, characterLimit) {


        if(!array) {
            return '';
        }


        if (key) {
            var strings = _.map(array, function(item) {
                return item[key];
            })
        } else {
            strings = array;
        }

        if(characterLimit) {
            return strings.join(", ").substring(0, characterLimit);
        } else {
            return strings.join(", ")
        }
    };
});






app.filter('simple', function() {
    return function(data) {


        if(_.isString(data)) {
            return data;
        }

        if(_.isArray(data)) {

            var array = _.map(data, function(item) {
                if(_.isObject(item)) {
                    return item.title;
                } 

                if(_.isString(item)) {
                    return item;
                }
            })

            return array.join(', ');
        }
    };
});