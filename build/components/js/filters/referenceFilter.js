app.filter("reference", function() {
    return function(items, fieldName, id) {


        
        function getProperty(obj, prop) {
            var parts = prop.split('.'),
                last = parts.pop(),
                l = parts.length,
                i = 1,
                current = parts[0];

            if(!parts.length) {
                return obj[prop];
            }

            while ((obj = obj[current]) && i < l) {
                current = parts[i];
                i++;
            }

            if (obj) {
                return obj[last];
            }
        }
       /* */

        //Find where item author is the id specified
        var results = _.filter(items, function(item) {

            var key = getProperty(item, fieldName);

            if (key) {
                if(_.isArray(key)) {
                    var array = key;
                    return _.some(array, function(e) {
                        if(_.isObject(e)) {
                            if(e._id) {
                                return e._id == id;
                            }
                        } else {
                            return e == id;
                        }
                    })
                } else {
                    return (key._id == id || key == id);
                }
            }



            /*
            if (item[fieldName]) {
            	if(_.isArray(item[fieldName])) {

            		var array = item[fieldName];
            		return _.some(array, {'_id': id});
            	} else {
                	return (item[fieldName]._id == id || item[fieldName] == id);
            	}
            }
            */
        });

        return results;



    };
});