app.filter('age', function(){
  return function(date){
      return Number(moment().diff(date, 'years'));
  };
});
