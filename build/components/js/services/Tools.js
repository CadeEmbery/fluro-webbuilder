app.service('Tools', function() {

    var controller = {}

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
   
    controller.getDeepProperty = function(obj, prop) {
        var parts = prop.split('.'),
            last = parts.pop(),
            l = parts.length,
            i = 1,
            current = parts[0];



        if(!parts.length) {
            return obj[prop];
        }


        while ((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }
        
        if (obj) {
            return obj[last];
        }
    }


    /////////////////////////////////////////////////////

    return controller;
});



