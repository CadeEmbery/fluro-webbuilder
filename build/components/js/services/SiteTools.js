app.service('SiteTools', function() {

    var controller = {}

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
   
    controller.getFlattenedRoutes = function(array) {
        return _.chain(array).map(function(route) {
            if (route.type == 'folder') {
                return controller.getFlattenedRoutes(route.routes);
            } else {
                return route;
            }
        }).flatten().compact().value();
    }


    /////////////////////////////////////////////////////

    return controller;
});