app.service('ModalService', function($modal, $rootScope, Fluro, FluroContent, TypeService) {

    var controller = {}


    /////////////////////////////////////////////////////////////////////

    controller.message = function(ids, successCallback, cancelCallback) {

        ///////////////////////////////

        var modalInstance = $modal.open({
            templateUrl: 'fluro-message-center/main.html',
            controller: 'FluroMessageCenterController',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                /**
                    access: function(FluroAccess) {
                        var canCreate;
                        if (definition) {
                            canCreate = FluroAccess.can('create', definition.definitionName);
                        } else {
                            canCreate = FluroAccess.can('create', type.path);
                        }

                        return FluroAccess.resolveIf(canCreate);
                    },
                    /**/
                contacts: function($q) {
                    return FluroContent.endpoint('message/contacts').query({
                        ids: ids,
                    }).$promise;
                }
            }
        });

        modalInstance.result.then(successCallback, cancelCallback);
    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    controller.create = function(typeName, params, successCallback, cancelCallback) {

        //console.log('Create In Modal', params);
        //////////////////////*/

        if (!params) {
            params = {};
        }

        var type = TypeService.getTypeFromPath(typeName);


        if (type.parentType) {

            //Resource
            FluroContent.endpoint('defined/' + type.path).get({}, createModal);

        } else {
            createModal();
        }

        function createModal(definition) {

            ///////////////////////////////

            var modalInstance = $modal.open({
                templateUrl: 'fluro-admin-content/types/form.html',
                controller: 'ContentFormController',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    //Get the definition
                    definition: function() {
                        return definition;
                    },
                    type: function() {
                        if (definition) {
                            return TypeService.getTypeFromPath(definition.parentType);
                        } else {
                            return type;
                        }
                    },
                    access: function(FluroAccess) {
                        var canCreate;
                        if (definition) {
                            canCreate = FluroAccess.can('create', definition.definitionName);
                        } else {
                            canCreate = FluroAccess.can('create', type.path);
                        }

                        return FluroAccess.resolveIf(canCreate);
                    },
                    item: function($q) {
                        var deferred = $q.defer();

                        var newItem = {};

                        //Check if we received any starting data
                        if (params.template) {
                            newItem = angular.copy(params.template);
                        }

                        //console.log('Create new item', newItem);

                        if (definition) {
                            //If the definition exists
                            if (definition.definitionName) {
                                //Send back a new item with the definition set
                                newItem.definition = definition.definitionName;
                                deferred.resolve(newItem);
                            } else {
                                //Just don't resolve
                                deferred.reject();
                            }

                            //return deferred.promise;
                        } else {

                            deferred.resolve(newItem);
                            //return newItem;
                        }

                        return deferred.promise;
                    },
                    extras: function() {
                        return {};
                    }
                }
            });

            modalInstance.result.then(successCallback, cancelCallback);
        }
    }


    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////


    controller.view = function(itemObject, successCallback, cancelCallback) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        //Load the items
        FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {

                var modalInstance = $modal.open({
                    templateUrl: 'fluro-admin-content/types/view.html',
                    controller: 'ContentViewController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {
                            return item;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }
                            var canView = FluroAccess.canViewItem(item, (definitionName == 'user'));
                            return FluroAccess.resolveIf(canView);
                        },
                        extras: function(FluroContent, $q) {

                            var deferred = $q.defer();

                            //Data Object
                            var data = {};

                            switch (type.path) {
                                // case 'process':
                                //     return FluroContent.endpoint('process/' + item._id + '/progress', null, true).query().$promise;
                                //     break;
                                case 'event':
                                case 'plan':

                                    var id;
                                    if (item.event) {
                                        if (item.event._id) {
                                            id = item.event._id;
                                        } else {
                                            id = item.event;
                                        }
                                    } else {
                                        id = item._id;
                                    }

                                    if (id) {
                                        FluroContent.endpoint('confirmations/event/' + id).query().$promise.then(function(res) {
                                            data.confirmations = res;
                                            //console.log('GOT CONFIRMATIONS', res);
                                            deferred.resolve(data);
                                        });
                                    } else {
                                        //console.log('NO ID')
                                        deferred.resolve(data);
                                    }
                                    break;
                                default:
                                    deferred.resolve(data);
                                    break;
                            }

                            return deferred.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }

    controller.edit = function(itemObject, successCallback, cancelCallback, createCopy, createFromTemplate, $scope) {


        var definedName = itemObject._type;
        if (itemObject.definition) {
            definedName = itemObject.definition;
        }

        //Load the items
        FluroContent.resource(definedName).get({
            id: itemObject._id
        }, function(item) {
            var type = TypeService.getTypeFromPath(item._type);

            if (item.definition) {
                FluroContent.endpoint('defined/' + item.definition).get({}, createModal);
            } else {
                createModal();
            }

            //////////////////////

            function createModal(definition) {

                var modalInstance = $modal.open({
                    templateUrl: 'fluro-admin-content/types/form.html',
                    controller: 'ContentFormController',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        type: function() {
                            return type;
                        },
                        item: function() {

                            var newItem = item;

                            if (createCopy || createFromTemplate) {
                                newItem = angular.copy(item);

                                delete newItem._id;
                                delete newItem.slug;
                                delete newItem.author;

                                //Delete API Key
                                delete newItem.apikey;

                                //Clear plan details
                                if (newItem._type == 'event') {
                                    newItem.plans = [];
                                    newItem.assignments = [];
                                }


                                if (newItem._type == 'event' || newItem._type == 'plan') {
                                    var initDate = new Date();

                                    if ($scope) {
                                        if ($scope.stringBrowseDate) {
                                            initDate = $scope.stringBrowseDate();
                                        }

                                        if ($scope.item && $scope.item.startDate) {
                                            initDate = $scope.item.startDate;
                                            //console.log('Use start date of item we are creating for');
                                        }
                                    }

                                    //Reset Dates
                                    newItem.useEndDate = false;
                                    newItem.endDate = initDate;
                                    newItem.startDate = initDate;
                                }

                                //Link a new plan to an event if none provided
                                if (newItem._type == 'plan' && !newItem.event) {
                                    if ($scope.item && $scope.item._id) {
                                        newItem.event = $scope.item;
                                    }
                                }
                            }

                            ////////////////////////////////////////////////

                            //Create Copy
                            if (createCopy) {
                                newItem.title = newItem.title + ' Copy';
                            }

                            ////////////////////////////////////////////////

                            if (createFromTemplate) {
                                //Update the status
                                newItem.status = 'active';

                            }

                            ////////////////////////////////////////////////

                            //Cheat here
                            if (itemObject.event) {
                                newItem.event = itemObject.event;
                                if (newItem._type == 'plan') {
                                    newItem.startDate = newItem.event.startDate;
                                }
                            }

                            ////////////////////////////////////////////////

                            return newItem;
                        },
                        definition: function() {
                            return definition;
                        },
                        access: function(FluroAccess, $rootScope) {
                            var definitionName = type.path;
                            if (definition) {
                                definitionName = definition.definitionName;
                            }

                            //Allow the user to modify themselves
                            var author = false;

                            //If wanting to edit a user
                            if (type.path == 'user') {
                                author = ($rootScope.user._id == item._id);
                                if (author) {
                                    return true;
                                }
                            } else {
                                //Only allow if author of the content
                                if (_.isObject(item.author)) {
                                    author = (item.author._id == $rootScope.user._id);
                                } else {
                                    author = (item.author == $rootScope.user._id);
                                }
                            }

                            /////////////////////////////////////

                            //Check if we can edit
                            var canEdit = FluroAccess.canEditItem(item, (definitionName == 'user'));
                            var canCreate = FluroAccess.can('create', definitionName);

                            if (createCopy) {
                                return FluroAccess.resolveIf(canCreate && canEdit);
                            } else {
                                return FluroAccess.resolveIf(canEdit);
                            }
                        },
                        extras: function(FluroContent, $q) {
                            var deferred = $q.defer();

                            //Data Object
                            var data = {};

                            switch (type.path) {
                                case 'event':
                                    if (itemObject._id && !createCopy && !createFromTemplate) {
                                        FluroContent.endpoint('confirmations/event/' + itemObject._id).query().$promise.then(function(res) {
                                            data.confirmations = res;
                                            //console.log('GOT CONFIRMATIONS', res);
                                            deferred.resolve(data);
                                        });
                                    } else {
                                        //console.log('NO ID')
                                        deferred.resolve(data);
                                    }
                                    break;
                                default:
                                    deferred.resolve(data);
                                    break;
                            }

                            return deferred.promise;
                        }
                    }
                });

                modalInstance.result.then(successCallback, cancelCallback);
            }
        });
    }


    /////////////////////////////////////////////////////////////////////////

    controller.browse = function(type, modelSource, params) {

        if (!params) {
            params = {}
        }

        /////////////////////////////////////////

        //We need an object for our source
        if (!modelSource) {
            modelSource = {};
        }

        /////////////////////////////////////////

        //We need an array to append the items to
        if (!modelSource.items) {
            modelSource.items = [];
        }

        /////////////////////////////////////////

        //Launch the modal
        var modalDetails = {
            template: '<content-browser ng-model="model.items" ng-done="$dismiss" ng-type="type" params="params"></content-browser>',
            controller: function($scope) {
                $scope.type = type;
                $scope.model = modelSource;
                $scope.params = params;
            },
            size: 'lg',
        };
        /*
        if(params.scope) {
            //Isolate scope
            console.log('Isolate Scope');
            modalDetails.scope = params.scope;
        }
        */

        var modalInstance = $modal.open(modalDetails);

        return modalInstance;

        //modalInstance.result.then(successCallback, cancelCallback);

    }

    controller.batch = function(type, definition, ids, callback) {

        //var $scope = $rootScope.$new();

        var modalInstance = $modal.open({
            template: '<batch-editor></batch-editor>',
            backdrop: 'static',
            controller: function($scope) {
                $scope.type = type;
                $scope.definition = definition;
                $scope.ids = ids;
            },
            size: 'lg',
        });

        //modalInstance.result.then(successCallback, cancelCallback);

    }

    ///////////////////////

    return controller;
});